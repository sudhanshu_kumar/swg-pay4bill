<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class sfWidgetFormSchemaFormatterCustom extends sfWidgetFormSchemaFormatter {

    protected
    $errorListFormatInARow = "  <span class=\"error_list\">\n%errors%  </span>\n",
    $errorRowFormatInARow = "    %error%\n",
    $namedErrorRowFormatInARow = "    %name%: %error%\n",
    $rowFormat       = "<tr>\n  <th>%label%</th>\n  <td>%error%%field%%help%%hidden_fields%</td>\n</tr>\n",
    $errorRowFormat  = "<tr><td colspan=\"2\">\n%errors%</td></tr>\n",
    $helpFormat      = '<br />%help%',
    $decoratorFormat = "<table>\n  %content%</table>";

}

?>
