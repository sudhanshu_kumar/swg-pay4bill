<?php

/**
 * AppSessions filter form base class.
 *
 * @package    nisp
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseAppSessionsFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'user_id'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'), 'add_empty' => true)),
      'username'   => new sfWidgetFormFilterInput(),
      'ip_address' => new sfWidgetFormFilterInput(),
      'sess_data'  => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'sess_time'  => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'user_id'    => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('sfGuardUser'), 'column' => 'id')),
      'username'   => new sfValidatorPass(array('required' => false)),
      'ip_address' => new sfValidatorPass(array('required' => false)),
      'sess_data'  => new sfValidatorPass(array('required' => false)),
      'sess_time'  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('app_sessions_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'AppSessions';
  }

  public function getFields()
  {
    return array(
      'sess_id'    => 'Text',
      'user_id'    => 'ForeignKey',
      'username'   => 'Text',
      'ip_address' => 'Text',
      'sess_data'  => 'Text',
      'sess_time'  => 'Number',
    );
  }
}
