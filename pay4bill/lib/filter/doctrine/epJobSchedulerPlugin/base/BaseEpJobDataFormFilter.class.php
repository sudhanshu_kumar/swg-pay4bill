<?php

/**
 * EpJobData filter form base class.
 *
 * @package    nisp
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseEpJobDataFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'output_type'      => new sfWidgetFormFilterInput(),
      'output_text'      => new sfWidgetFormFilterInput(),
      'job_execution_id' => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'output_type'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'output_text'      => new sfValidatorPass(array('required' => false)),
      'job_execution_id' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('ep_job_data_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpJobData';
  }

  public function getFields()
  {
    return array(
      'id'               => 'Number',
      'output_type'      => 'Number',
      'output_text'      => 'Text',
      'job_execution_id' => 'Number',
    );
  }
}
