<?php

/**
 * EpJobRepeatSchedule filter form base class.
 *
 * @package    nisp
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseEpJobRepeatScheduleFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'job_id'       => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EpJob'), 'add_empty' => true)),
      'minutes'      => new sfWidgetFormFilterInput(),
      'hours'        => new sfWidgetFormFilterInput(),
      'day_of_month' => new sfWidgetFormFilterInput(),
      'month'        => new sfWidgetFormFilterInput(),
      'day_of_week'  => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'job_id'       => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('EpJob'), 'column' => 'id')),
      'minutes'      => new sfValidatorPass(array('required' => false)),
      'hours'        => new sfValidatorPass(array('required' => false)),
      'day_of_month' => new sfValidatorPass(array('required' => false)),
      'month'        => new sfValidatorPass(array('required' => false)),
      'day_of_week'  => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('ep_job_repeat_schedule_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpJobRepeatSchedule';
  }

  public function getFields()
  {
    return array(
      'id'           => 'Number',
      'job_id'       => 'ForeignKey',
      'minutes'      => 'Text',
      'hours'        => 'Text',
      'day_of_month' => 'Text',
      'month'        => 'Text',
      'day_of_week'  => 'Text',
    );
  }
}
