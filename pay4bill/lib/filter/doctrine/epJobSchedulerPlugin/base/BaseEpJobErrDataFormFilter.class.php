<?php

/**
 * EpJobErrData filter form base class.
 *
 * @package    nisp
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedInheritanceTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseEpJobErrDataFormFilter extends EpJobDataFormFilter
{
  protected function setupInheritance()
  {
    parent::setupInheritance();

    $this->widgetSchema->setNameFormat('ep_job_err_data_filters[%s]');
  }

  public function getModelName()
  {
    return 'EpJobErrData';
  }
}
