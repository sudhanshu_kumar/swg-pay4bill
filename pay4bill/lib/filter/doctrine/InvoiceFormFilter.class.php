<?php

/**
 * Invoice filter form.
 *
 * @package    nisp
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class InvoiceFormFilter extends BaseInvoiceFormFilter
{
  public function configure()
  {
      

      unset($this['issue_date'],$this['invoice_no'],$this['currency_type'],$this['additional_information'],$this['payment_date'],$this['part_payment'],$this['parent_id_if_recurring'],$this['due_date'],$this['delivery_date'],$this['customer_id'],$this['payment_mode'],$this['id'], $this['amount_paid'], $this['amount_due'], $this['merchant_id'], $this['status'], $this['created_at'], $this['updated_at']);
//      $this->setWidget('issue_date', new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormJQueryDate(), 'to_date' => new sfWidgetFormJQueryDate(), 'with_empty' => false, 'template' => '<div class="clear" style="padding-top:5px;"></div><span style="float: left; width: 37px; padding-left:38px; font-weight:bold;"> From</span> %from_date%<br /><span style="float: left; width: 37px; padding-left:38px; font-weight:bold;"> To</span> %to_date%')));
          $this->widgetSchema['from'] = new sfWidgetFormDateCalInput(array('years' => DateHelper::getDateRanges(), 'format' => '%year%%month%%day%'), array());

  $this->widgetSchema['to'] = new sfWidgetFormDateCalInput(array('years' => DateHelper::getDateRanges(), 'format' => '%year%%month%%day%'), array());
  //Varun : Bug Fix : FS#34688
  $this->widgetSchema->setLabels(array(
                'from'=>'From<font color=red>*</font>',
                'to'    => 'To<font color=red>*</font>',

            ));
  }
}
