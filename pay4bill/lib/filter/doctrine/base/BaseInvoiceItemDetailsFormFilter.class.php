<?php

/**
 * InvoiceItemDetails filter form base class.
 *
 * @package    nisp
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseInvoiceItemDetailsFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'invoice_id'       => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Invoice'), 'add_empty' => true)),
      'item_description' => new sfWidgetFormFilterInput(),
      'no_of_items'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'item_price'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'item_tax'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'subTotal'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'tax_total'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'created_at'       => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'       => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'invoice_id'       => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Invoice'), 'column' => 'id')),
      'item_description' => new sfValidatorPass(array('required' => false)),
      'no_of_items'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'item_price'       => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'item_tax'         => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'subTotal'         => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'tax_total'        => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'created_at'       => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'       => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('invoice_item_details_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'InvoiceItemDetails';
  }

  public function getFields()
  {
    return array(
      'id'               => 'Number',
      'invoice_id'       => 'ForeignKey',
      'item_description' => 'Text',
      'no_of_items'      => 'Number',
      'item_price'       => 'Number',
      'item_tax'         => 'Number',
      'subTotal'         => 'Number',
      'tax_total'        => 'Number',
      'created_at'       => 'Date',
      'updated_at'       => 'Date',
    );
  }
}
