<?php

/**
 * MerchantProfile filter form base class.
 *
 * @package    pay4bill
 * @subpackage filter
 * @author     Aditi
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseMerchantProfileFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'merchant_id'              => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'), 'add_empty' => true)),
      'merchant_name'            => new sfWidgetFormFilterInput(),
      'cac_registration'         => new sfWidgetFormFilterInput(),
      'merchant_contact_address' => new sfWidgetFormFilterInput(),
      'merchant_contact_number'  => new sfWidgetFormFilterInput(),
      'company_url'              => new sfWidgetFormFilterInput(),
      'approved'                 => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'company_logo'             => new sfWidgetFormFilterInput(),
      'tiny_url'                 => new sfWidgetFormFilterInput(),
      'disabled'                 => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'merchant_service_id'      => new sfWidgetFormFilterInput(),
      'merchant_code'            => new sfWidgetFormFilterInput(),
      'merchant_key'             => new sfWidgetFormFilterInput(),
      'object_biller_type'       => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('ObjectType'), 'add_empty' => true)),
      'payment_type'             => new sfWidgetFormChoice(array('choices' => array('' => '', 'o' => 'o', 'b' => 'b'))),
      'unique_attribute_id'      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('AttributeMaster'), 'add_empty' => true)),
      'created_at'               => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'               => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'merchant_id'              => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('sfGuardUser'), 'column' => 'id')),
      'merchant_name'            => new sfValidatorPass(array('required' => false)),
      'cac_registration'         => new sfValidatorPass(array('required' => false)),
      'merchant_contact_address' => new sfValidatorPass(array('required' => false)),
      'merchant_contact_number'  => new sfValidatorPass(array('required' => false)),
      'company_url'              => new sfValidatorPass(array('required' => false)),
      'approved'                 => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'company_logo'             => new sfValidatorPass(array('required' => false)),
      'tiny_url'                 => new sfValidatorPass(array('required' => false)),
      'disabled'                 => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'merchant_service_id'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'merchant_code'            => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'merchant_key'             => new sfValidatorPass(array('required' => false)),
      'object_biller_type'       => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('ObjectType'), 'column' => 'id')),
      'payment_type'             => new sfValidatorChoice(array('required' => false, 'choices' => array('o' => 'o', 'b' => 'b'))),
      'unique_attribute_id'      => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('AttributeMaster'), 'column' => 'id')),
      'created_at'               => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'               => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('merchant_profile_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'MerchantProfile';
  }

  public function getFields()
  {
    return array(
      'id'                       => 'Number',
      'merchant_id'              => 'ForeignKey',
      'merchant_name'            => 'Text',
      'cac_registration'         => 'Text',
      'merchant_contact_address' => 'Text',
      'merchant_contact_number'  => 'Text',
      'company_url'              => 'Text',
      'approved'                 => 'Boolean',
      'company_logo'             => 'Text',
      'tiny_url'                 => 'Text',
      'disabled'                 => 'Boolean',
      'merchant_service_id'      => 'Number',
      'merchant_code'            => 'Number',
      'merchant_key'             => 'Text',
      'object_biller_type'       => 'ForeignKey',
      'payment_type'             => 'Enum',
      'unique_attribute_id'      => 'ForeignKey',
      'created_at'               => 'Date',
      'updated_at'               => 'Date',
    );
  }
}
