<?php

/**
 * Invoice filter form base class.
 *
 * @package    pay4bill
 * @subpackage filter
 * @author     Aditi
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseInvoiceFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'invoice_no'             => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'merchant_id'            => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Merchant'), 'add_empty' => true)),
      'customer_id'            => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Customer'), 'add_empty' => true)),
      'tax_id'                 => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Tax'), 'add_empty' => true)),
      'issue_date'             => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'delivery_date'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'due_date'               => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'currency_type'          => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'additional_information' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'part_payment'           => new sfWidgetFormFilterInput(),
      'total_amount'           => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'amount_paid'            => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'amount_due'             => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'parent_id_if_recurring' => new sfWidgetFormFilterInput(),
      'status'                 => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'payment_date'           => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'payment_mode'           => new sfWidgetFormFilterInput(),
      'regenerated'            => new sfWidgetFormFilterInput(),
      'pay_full_amount'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'object_id'              => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Object'), 'add_empty' => true)),
      'created_at'             => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'             => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'created_by'             => new sfWidgetFormFilterInput(),
      'updated_by'             => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'invoice_no'             => new sfValidatorPass(array('required' => false)),
      'merchant_id'            => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Merchant'), 'column' => 'id')),
      'customer_id'            => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Customer'), 'column' => 'id')),
      'tax_id'                 => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Tax'), 'column' => 'id')),
      'issue_date'             => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'delivery_date'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'due_date'               => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'currency_type'          => new sfValidatorPass(array('required' => false)),
      'additional_information' => new sfValidatorPass(array('required' => false)),
      'part_payment'           => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'total_amount'           => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'amount_paid'            => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'amount_due'             => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'parent_id_if_recurring' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'status'                 => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'payment_date'           => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'payment_mode'           => new sfValidatorPass(array('required' => false)),
      'regenerated'            => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'pay_full_amount'        => new sfValidatorPass(array('required' => false)),
      'object_id'              => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Object'), 'column' => 'id')),
      'created_at'             => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'             => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'created_by'             => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'updated_by'             => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('invoice_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Invoice';
  }

  public function getFields()
  {
    return array(
      'id'                     => 'Number',
      'invoice_no'             => 'Text',
      'merchant_id'            => 'ForeignKey',
      'customer_id'            => 'ForeignKey',
      'tax_id'                 => 'ForeignKey',
      'issue_date'             => 'Date',
      'delivery_date'          => 'Date',
      'due_date'               => 'Date',
      'currency_type'          => 'Text',
      'additional_information' => 'Text',
      'part_payment'           => 'Number',
      'total_amount'           => 'Number',
      'amount_paid'            => 'Number',
      'amount_due'             => 'Number',
      'parent_id_if_recurring' => 'Number',
      'status'                 => 'Number',
      'payment_date'           => 'Date',
      'payment_mode'           => 'Text',
      'regenerated'            => 'Number',
      'pay_full_amount'        => 'Text',
      'object_id'              => 'ForeignKey',
      'created_at'             => 'Date',
      'updated_at'             => 'Date',
      'created_by'             => 'Number',
      'updated_by'             => 'Number',
    );
  }
}
