<?php

/**
 * Bank filter form base class.
 *
 * @package    nisp
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseBankFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'merchant_id'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'), 'add_empty' => true)),
      'bank_name'      => new sfWidgetFormFilterInput(),
      'bank_branch'    => new sfWidgetFormFilterInput(),
      'account_name'   => new sfWidgetFormFilterInput(),
      'account_number' => new sfWidgetFormFilterInput(),
      'sort_code'      => new sfWidgetFormFilterInput(),
      'created_at'     => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'     => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'merchant_id'    => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('sfGuardUser'), 'column' => 'id')),
      'bank_name'      => new sfValidatorPass(array('required' => false)),
      'bank_branch'    => new sfValidatorPass(array('required' => false)),
      'account_name'   => new sfValidatorPass(array('required' => false)),
      'account_number' => new sfValidatorPass(array('required' => false)),
      'sort_code'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'created_at'     => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'     => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('bank_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Bank';
  }

  public function getFields()
  {
    return array(
      'id'             => 'Number',
      'merchant_id'    => 'ForeignKey',
      'bank_name'      => 'Text',
      'bank_branch'    => 'Text',
      'account_name'   => 'Text',
      'account_number' => 'Text',
      'sort_code'      => 'Number',
      'created_at'     => 'Date',
      'updated_at'     => 'Date',
    );
  }
}
