<?php

/**
 * TellerMerchantMapping filter form base class.
 *
 * @package    nisp
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseTellerMerchantMappingFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'teller_profile_id'   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TellerProfile'), 'add_empty' => true)),
      'merchant_profile_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantProfile'), 'add_empty' => true)),
      'is_active'           => new sfWidgetFormChoice(array('choices' => array('' => '', 0 => 0, 1 => 1))),
      'created_at'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'teller_profile_id'   => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('TellerProfile'), 'column' => 'id')),
      'merchant_profile_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('MerchantProfile'), 'column' => 'id')),
      'is_active'           => new sfValidatorChoice(array('required' => false, 'choices' => array(0 => 0, 1 => 1))),
      'created_at'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('teller_merchant_mapping_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'TellerMerchantMapping';
  }

  public function getFields()
  {
    return array(
      'id'                  => 'Number',
      'teller_profile_id'   => 'ForeignKey',
      'merchant_profile_id' => 'ForeignKey',
      'is_active'           => 'Enum',
      'created_at'          => 'Date',
      'updated_at'          => 'Date',
    );
  }
}
