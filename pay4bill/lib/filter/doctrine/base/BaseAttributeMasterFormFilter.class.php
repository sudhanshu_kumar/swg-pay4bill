<?php

/**
 * AttributeMaster filter form base class.
 *
 * @package    nisp
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseAttributeMasterFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'att_name'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'att_label'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'att_type'       => new sfWidgetFormChoice(array('choices' => array('' => '', 'String' => 'String', 'Integer' => 'Integer', 'Alphanumeric' => 'Alphanumeric'))),
      'merchant_id'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantProfile'), 'add_empty' => true)),
      'object_type_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('ObjectType'), 'add_empty' => true)),
      'is_mandatory'   => new sfWidgetFormChoice(array('choices' => array('' => '', 0 => 0, 1 => 1))),
      'is_unique'      => new sfWidgetFormChoice(array('choices' => array('' => '', 0 => 0, 1 => 1))),
      'is_active'      => new sfWidgetFormChoice(array('choices' => array('' => '', 0 => 0, 1 => 1))),
      'created_at'     => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'     => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'att_name'       => new sfValidatorPass(array('required' => false)),
      'att_label'      => new sfValidatorPass(array('required' => false)),
      'att_type'       => new sfValidatorChoice(array('required' => false, 'choices' => array('String' => 'String', 'Integer' => 'Integer', 'Alphanumeric' => 'Alphanumeric'))),
      'merchant_id'    => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('MerchantProfile'), 'column' => 'id')),
      'object_type_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('ObjectType'), 'column' => 'id')),
      'is_mandatory'   => new sfValidatorChoice(array('required' => false, 'choices' => array(0 => 0, 1 => 1))),
      'is_unique'      => new sfValidatorChoice(array('required' => false, 'choices' => array(0 => 0, 1 => 1))),
      'is_active'      => new sfValidatorChoice(array('required' => false, 'choices' => array(0 => 0, 1 => 1))),
      'created_at'     => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'     => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('attribute_master_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'AttributeMaster';
  }

  public function getFields()
  {
    return array(
      'id'             => 'Number',
      'att_name'       => 'Text',
      'att_label'      => 'Text',
      'att_type'       => 'Enum',
      'merchant_id'    => 'ForeignKey',
      'object_type_id' => 'ForeignKey',
      'is_mandatory'   => 'Enum',
      'is_unique'      => 'Enum',
      'is_active'      => 'Enum',
      'created_at'     => 'Date',
      'updated_at'     => 'Date',
    );
  }
}
