<?php

/**
 * EpP4mResponse filter form base class.
 *
 * @package    nisp
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseEpP4mResponseFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'request_id'          => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EpP4mRequest'), 'add_empty' => true)),
      'payment_status'      => new sfWidgetFormChoice(array('choices' => array('' => '', 0 => '0', 1 => '1'))),
      'payment_mode'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'validation_number'   => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'total_amount'        => new sfWidgetFormFilterInput(),
      'currency'            => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'payment_date'        => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'bank_name'           => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'branch_name'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'payment_description' => new sfWidgetFormFilterInput(),
      'created_at'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'request_id'          => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('EpP4mRequest'), 'column' => 'id')),
      'payment_status'      => new sfValidatorChoice(array('required' => false, 'choices' => array(0 => '0', 1 => '1'))),
      'payment_mode'        => new sfValidatorPass(array('required' => false)),
      'validation_number'   => new sfValidatorPass(array('required' => false)),
      'total_amount'        => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'currency'            => new sfValidatorPass(array('required' => false)),
      'payment_date'        => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'bank_name'           => new sfValidatorPass(array('required' => false)),
      'branch_name'         => new sfValidatorPass(array('required' => false)),
      'payment_description' => new sfValidatorPass(array('required' => false)),
      'created_at'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('ep_p4m_response_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpP4mResponse';
  }

  public function getFields()
  {
    return array(
      'id'                  => 'Number',
      'request_id'          => 'ForeignKey',
      'payment_status'      => 'Enum',
      'payment_mode'        => 'Text',
      'validation_number'   => 'Text',
      'total_amount'        => 'Number',
      'currency'            => 'Text',
      'payment_date'        => 'Date',
      'bank_name'           => 'Text',
      'branch_name'         => 'Text',
      'payment_description' => 'Text',
      'created_at'          => 'Date',
      'updated_at'          => 'Date',
    );
  }
}
