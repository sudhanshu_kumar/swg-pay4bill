<?php

/**
 * EpP4mRequest filter form base class.
 *
 * @package    nisp
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseEpP4mRequestFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'item_number'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'merchant_service_id' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'transaction_number'  => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'currency'            => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'payment_type'        => new sfWidgetFormFilterInput(),
      'buyer_ip_address'    => new sfWidgetFormFilterInput(),
      'payment_description' => new sfWidgetFormFilterInput(),
      'total_amount'        => new sfWidgetFormFilterInput(),
      'created_at'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'item_number'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'merchant_service_id' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'transaction_number'  => new sfValidatorPass(array('required' => false)),
      'currency'            => new sfValidatorPass(array('required' => false)),
      'payment_type'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'buyer_ip_address'    => new sfValidatorPass(array('required' => false)),
      'payment_description' => new sfValidatorPass(array('required' => false)),
      'total_amount'        => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'created_at'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('ep_p4m_request_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpP4mRequest';
  }

  public function getFields()
  {
    return array(
      'id'                  => 'Number',
      'item_number'         => 'Number',
      'merchant_service_id' => 'Number',
      'transaction_number'  => 'Text',
      'currency'            => 'Text',
      'payment_type'        => 'Number',
      'buyer_ip_address'    => 'Text',
      'payment_description' => 'Text',
      'total_amount'        => 'Number',
      'created_at'          => 'Date',
      'updated_at'          => 'Date',
    );
  }
}
