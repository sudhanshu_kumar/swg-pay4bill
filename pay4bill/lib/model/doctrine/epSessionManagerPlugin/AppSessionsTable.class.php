<?php


class AppSessionsTable extends PluginAppSessionsTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('AppSessions');
    }
}