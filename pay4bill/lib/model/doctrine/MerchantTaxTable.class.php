<?php


class MerchantTaxTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('MerchantTax');
    }

    /* Varun
     * Function : EntryForMerchantTax
     * Function to get Tax details for a merchant
     */
       public function findEntryForMerchantTax($tax_id,$merchant_id)
    {
        $q = Doctrine_Query::create()
        ->from('merchanttax mtx');
        $q->andWhere('mtx.tax_id = ?', $tax_id);
        $q->andWhere('mtx.merchant_id = ?', $merchant_id);
         
        return $q->execute();

    }
}