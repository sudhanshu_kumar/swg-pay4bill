<?php

class RecurrentBillingTable extends Doctrine_Table {

    public static function getInstance() {
        return Doctrine_Core::getTable('RecurrentBilling');
    }

    public function updateOnRecurringBillGeneration($old_invoice_id, $new_invoice_id) {
        $query = Doctrine_Query::create()->update('RecurrentBilling i')->set('i.invoice_id', '?', $new_invoice_id)
                        ->where('i.invoice_id= ?', $old_invoice_id);
        $query->execute();
    }

}