<?php

class InvoiceTable extends Doctrine_Table {

    public static function getInstance() {
        return Doctrine_Core::getTable('Invoice');
    }

    public function isValidInvoiceNumberForThsMerchant($invoice_no, $merchantServiceId) {

        $q = Doctrine_Query::create()
                        ->from('Invoice i')
                        ->leftJoin('i.Merchant mp')
                        ->where('i.invoice_no= ?', $invoice_no)
                        ->addWhere('i.status=?', 0)
                        ->addWhere('mp.merchant_service_id= ?', $merchantServiceId)
                        ->addWhere('mp.disabled = 0')
                        ->addWhere("mp.payment_type ='b'")
                        ->andwhere('mp.company_url is NOT NULL')
                        ->addWhere('mp.approved= 1 ')
                        ->addWhere('mp.merchant_code != "" ')
                        ->addWhere('mp.merchant_key != "" ');

        if ($q->execute()->count() > 0) {
            return $q->execute()->getFirst();
        } else {
            return false;
        }
    }

    public function returnDistinctInvoiceNumer() {
        $proposed_no = 0;
        $found = false;
        while (!$found) {
            $proposed_no = rand(1, 9999999999);
            $q = Doctrine_Query::create()
                            ->from('Invoice i')->where('invoice_no=?', $proposed_no)->count();
            if ($q == 0) {
                $found = true;
            }
        }
        return $proposed_no;
    }

    public function returnInvoiceObjectsFromIds($id_array) {
        $q = Doctrine_Query::create()
                        ->from('Invoice i');

//        $q->leftJoin('i.Merchant mp');


        $q->andWhereIn('i.id', $id_array);


        return $q->execute();
    }

    public function chk_Invoice_number($no) {

        $q = Doctrine_Query::create()
                        ->from('Invoice i')->where('invoice_no=?', $no)->count();
        if ($q == 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getInvoiceForCustomer($customerId, $offset=null, $limit=null, $getExpired=null, $com_name=null, $invoice_no=null, $fromDate=null, $toDate=null, $date_category=null, $email_address=null, $customerProfile_customerId) {

        $q = Doctrine_Query::create()
                        ->from('Invoice i');
        $q->leftJoin('i.InvoiceItemDetails id');
        $q->leftJoin('i.Merchant mp');
        $q->leftJoin('i.Customer cp');
        $q->leftJoin('mp.sfGuardUser sfgu'); //@Saurabh to match the email and get the vale from sfguard table for email
        $q->addSelect('i.*,mp.payment_type as payment_type,sum(id.subtotal) as subtotal,sum(id.tax_total) as tax,mp.merchant_name as merchant_name');
        $q->groupBy('id.invoice_id');
        if ($offset) {
            $q->offset($offset);
        }
        if ($limit) {
            $q->limit($limit);
        }
//        $q->andWhere('i.customer_id = ?', $customerId);

        /*
         * @Saurabh in 16 Aug 2012
         * Customer will see all the invoices in which he/she is linked with his/her username.
         *
         */

        $cusIdArray = explode(",", $customerId);
        $q->andWhereIn('i.customer_id', $cusIdArray);


        if ($getExpired == 'true') {
            $q->andWhere("(mp.payment_type='o' and i.due_date < '" . date('Y-m-d') . "' ) "); /* Aditi:- This line commented for bug FS#36138 */
            $q->andWhere('i.status <>1 and i.status<> 2');
        } else {
            $q->andWhere('mp.disabled = ?', 0);
            $q->andWhere("i.delivery_date <= '" . date('Y-m-d') . "'");
            $q->andWhere("mp.payment_type='b' or (mp.payment_type='o' and i.due_date >= '" . date('Y-m-d') . "' ) "); /* Aditi:- This line commented for bug FS#36138 */
            $q->andWhere('i.status = 0 or i.status=3');
        }
        if (isset($email_address) && $email_address != '') {
            $q->addWhere("sfgu.email_address LIKE '$email_address%'");
        }

        if (isset($invoice_no) && $invoice_no != '') {
            $q->addWhere("i.invoice_no LIKE '$invoice_no%'");
        }
        if (isset($com_name) && $com_name != '') {
            $q->addWhere("mp.merchant_name LIKE '$com_name%'");
        }
        if (!is_null($fromDate) && !is_null($toDate)) {
            $q->andWhere("i." . $date_category . " >= '" . $fromDate . "'");
            $q->andWhere("i." . $date_category . " <= '" . $toDate . "'");
        }
        if (isset($customerProfile_customerId) && $customerProfile_customerId !== '') {
            $q->addWhere("cp.customer_id LIKE '$customerProfile_customerId%'");
        }


        $q->orderBy('i.due_date');
        return $q->execute();
    }

    /*
     * @Saurabh  on 22June,2012
     * To find all invoices which status 0 and 2 when the tellers log in
     */

    public function getInvoiceForTeller($customerId, $offset=null, $limit=null, $getExpired=null, $com_name=null, $invoice_no=null, $fromDate=null, $toDate=null, $date_category=null, $email_address=null) {

        $q = Doctrine_Query::create()
                        ->from('Invoice i');
        $q->leftJoin('i.InvoiceItemDetails id');
        $q->leftJoin('i.Merchant mp');
        $q->leftJoin('mp.TellerMerchantMapping tmp');
        $q->leftJoin('mp.sfGuardUser sfgu');
        $q->addWhere('tmp.teller_profile_id=?', sfContext::getInstance()->getUser()->getGuardUser()->getTellerUser()->getId());
        $q->addSelect('i.*,sum(id.subtotal) as subtotal,sum(id.tax_total) as tax,mp.merchant_name as merchant_name');
        $q->groupBy('id.invoice_id');
        if ($offset) {
            $q->offset($offset);
        }
        if ($limit) {
            $q->limit($limit);
        }
        $q->andWhere('i.customer_id = ?', $customerId);
        if ($getExpired == 'true') { // Will never execute as teller will not be shown this expired invoices
//            $q->andWhere("i.due_date < '" . date('Y-m-d') . "'");
//            $q->andWhere('i.status <>1 and i.status<> 2');
        } else {

            $q->andWhere('mp.disabled = ?', 0);
            // $q->andWhere("i.delivery_date <= '" . date('Y-m-d') . "'");
            // $q->andWhere("i.due_date >= '" . date('Y-m-d') . "'");
            $q->andWhere('i.status = 0'); // For only 0 Only pending invoice
        }
        if (isset($email_address) && $email_address != '') {
            $q->addWhere("sfgu.email_address LIKE '$email_address%'");
        }
        if (isset($invoice_no) && $invoice_no != '') {
            $q->addWhere("i.invoice_no LIKE '$invoice_no%'");
        }
        if (isset($com_name) && $com_name != '') {
            $q->addWhere("mp.merchant_name LIKE '$com_name%'");
        }
        if (!is_null($fromDate) && !is_null($toDate)) {
            $q->andWhere("i." . $date_category . " >= '" . $fromDate . "'");
            $q->andWhere("i." . $date_category . " <= '" . $toDate . "'");
        }


        $q->orderBy('i.due_date');

        return $q->execute();
    }

    /*
     * @Saurabh on 21 June
     * Function to show all invocies for a customer
     */

    public function getAllInvoicesForCustomer($customerId, $offset=null, $limit=null) {

        $q = Doctrine_Query::create()
                        ->from('Invoice i');
        $q->leftJoin('i.InvoiceItemDetails id');
        $q->leftJoin('i.Merchant mp');
        $q->addSelect('i.*,sum(id.subtotal) as subtotal,sum(id.tax_total) as tax,mp.merchant_name as merchant_name');
        $q->groupBy('id.invoice_id');
        if ($offset) {
            $q->offset($offset);
        }
        if ($limit) {
            $q->limit($limit);
        }
        $q->andWhere('i.customer_id = ?', $customerId);
        $q->andWhere('mp.disabled = ?', 0);

//        $q->andWhere("i.delivery_date <= '" . date('Y-m-d') . "'");
//        $q->andWhere("i.due_date >= '" . date('Y-m-d') . "'");

        $q->andWhere('i.status = 0 or i.status=2');
        $q->orderBy('i.due_date');
        return $q->execute();
    }

    public function getAllInvoiceForMerchantReport($merchantId, $invoice_no, $fromDate, $toDate, $date_category, $cust_name=null, $customer_id, $status, $expired, $merchant_name, $offset=null, $limit=null) {
        $q = Doctrine_Query::create()
                        ->from('Invoice i')
                        ->leftJoin('i.Customer cust');

        $q->addSelect('i.*,cust.*');
        if (isset($merchant_name) && $merchant_name != '') {
            $q->leftJoin('i.Merchant mp');
        }

        if (isset($invoice_no) && $invoice_no != '') {
            $q->andWhere("i.invoice_no like '$invoice_no%'");
        }

        if (isset($status) && ($status >= 0) && trim($status) !== '') {
            if (sfContext::getInstance()->getUser()->hasAttribute('allUnpaid') && sfContext::getInstance()->getUser()->getAttribute('allUnpaid') == 1) {
                $q->andWhere("(i.status=0 or i.status=3)");
            } else if (sfContext::getInstance()->getUser()->hasAttribute('allPaid') && sfContext::getInstance()->getUser()->getAttribute('allPaid') == 1) {

                $q->andWhere('(i.status=2 or i.status=3)');
            } else {
                if (!$expired) {
                    $q->andWhere("i.status=?", $status);
                } if ($status == 0 && !$expired) {
                    $q->andWhere("i.due_date >= '" . date('Y-m-d') . "'");
                }
                if ($status == 3 && !$expired) {
                    $q->andWhere("i.due_date >= '" . date('Y-m-d') . "'");
                }
            }
        }

        if (!is_null($merchantId) || !is_null($fromDate) && !is_null($toDate) || isset($customer_id) && $customer_id != '' || isset($cust_name) && $cust_name != '' || isset($merchant_name) && $merchant_name != '') {
            if (!is_null($merchantId)) {

                $q->andWhere('i.merchant_id = ?', $merchantId);
            }
            if (!is_null($fromDate) && !is_null($toDate)) {
                $q->andWhere("i." . $date_category . " >= '" . $fromDate . "'");
                $q->andWhere("i." . $date_category . " <= '" . $toDate . "'");
            }
            //Varun : Condition added for email address and cust name
            if (isset($customer_id) && $customer_id != '') {
                $q->andWhere("cust.customer_id LIKE '$customer_id%'");
            }
            if (isset($cust_name) && $cust_name != '') {
                $q->andWhere("cust.name LIKE '$cust_name%'");
            }
            if (isset($merchant_name) && $merchant_name != '') {
                $q->andWhere("mp.merchant_name LIKE '$merchant_name%'");
            }
        }
        if ($expired) {
            $q->andWhere('(i.status=0 or i.status=3)');
            $q->andWhere("i.due_date < '" . date('Y-m-d') . "'");
        }
        if ($offset) {
            $q->offset($offset);
        }
        if ($limit) {
            $q->limit($limit);
        }
        $q->orderBy('i.due_date');

        return $q->execute();
    }

    public function getInvoiceForMerchant($merchantId, $offset=null, $limit=null, $email_address=null, $cust_name=null, $invoice_no=null, $fromDate=null, $toDate=null, $date_category=null, $customer_id=null) {

        $q = Doctrine_Query::create()
                        ->from('Invoice i')
                        ->leftJoin('i.Customer cp')
                        ->leftJoin('i.RecurrentBilling'); // added by santosh on 2nd May 12
//                        ->leftJoin('cp.sfGuardUser sfgu');
        if ($offset) {
            $q->offset($offset);
        }
        if ($limit) {
            $q->limit($limit);
        }
        $q->addWhere('i.merchant_id = ?', $merchantId);
        $q->addWhere('i.status <> ?', 2);
        //Varun : Query modified to pass search criteria variables
        if (isset($invoice_no) && $invoice_no != '') {
            $q->addWhere("i.invoice_no LIKE '$invoice_no%'");
        }
        if (isset($cust_name) && $cust_name != '') {
            $q->addWhere("cp.name LIKE '$cust_name%'");
        }
        if (isset($customer_id) && $customer_id != '') {//Added By Aditi Malhotra to add enable customer id search on username
            $q->addWhere("cp.customer_id LIKE '$customer_id%'");
        }

        if (!is_null($fromDate) && !is_null($toDate)) {
            $q->andWhere("i." . $date_category . " >= '" . $fromDate . "'");
            $q->andWhere("i." . $date_category . " <= '" . $toDate . "'");
        }
        $q->andWhere("i.due_date >='" . date('Y-m-d') . "'");
        $q->andWhere('i.regenerated =?', '0');
        $q->orderBy('i.due_date');

        //echo  $q->getSQLQuery();die;

        return $q->execute();
    }

    public function getInvoiceForDashBoard($merchantId) {



        $q = Doctrine_Query::create()
                        ->from('Invoice i');
        $q->addWhere('i.merchant_id = ?', $merchantId);
        $q->addWhere('i.status = ?', 2);
//      $q->andWhere("i.due_date <='". date('Y-m-d')."'");
        $q->addSelect('SUM(i.amount_paid) as sum, i.payment_date as payment_date');
        $q->groupBy('i.payment_date');


        if ($q->execute()->count() > 30) {
            $q->offset($q->execute()->count() - 30);
        }

        return $q->execute();
    }

    public function getInvoiceDetailsForPaymentStatusUpdates($id, $merchant_id=null) {

        $q = Doctrine_Query::create()
                        ->from('Invoice i');
        $q->leftJoin('i.InvoiceItemDetails id');
        $q->leftJoin('i.RecurrentBilling rb');
        $q->addSelect('i.*,sum(id.subtotal) as subtotal,sum(id.tax_total) as tax,rb.*');
        $q->groupBy('id.invoice_id');
        $q->addWhere('i.id = ?', $id);
        if ($merchant_id != '' && isset($merchant_id)) {
            $q->addWhere('i.merchant_id = ?', $merchant_id);
        }
        return $q->execute()->toArray();
    }

    public function partlyPaidForCustomer($customerProfileId, $offset=null, $limit=null, $com_name=null, $invoice_no=null, $customerIdSearch) {


        $q = Doctrine_Query::create()
                        ->from('Invoice i')
                        ->leftJoin('i.Customer cp');
        $q->leftJoin('i.Merchant mp');

        if (isset($customerIdSearch) && $customerIdSearch != '') {
            $q->addWhere("cp.customer_id LIKE '$customerIdSearch%'");
        }

        $custArray = explode(",", $customerProfileId);
        $q->andWhereIn('i.customer_id', $custArray);


        $q->andWhere("i.status=3");
        if ($offset) {
            $q->offset($offset);
        }
        if ($limit) {
            $q->limit($limit);
        }

        if (isset($invoice_no) && $invoice_no != '') {
            $q->addWhere("i.invoice_no LIKE '$invoice_no%'");
        }
        if (isset($com_name) && $com_name != '') {
            $q->addWhere("mp.merchant_name LIKE '$com_name%'");
        }

        $q->orderBy('i.payment_date');

        return $q->execute();
    }

    public function fullyPaidForCustomer($customerId, $offset=null, $limit=null, $com_name=null, $invoice_no=null, $customerIdSearch) {


        $q = Doctrine_Query::create()
                        ->from('Invoice i')->leftJoin('i.Customer cp')->leftJoin('i.Merchant mp');
        if ($offset) {
            $q->offset($offset);
        }
        if ($limit) {
            $q->limit($limit);
        }

        $custArray = explode(",", $customerId);
        $q->andWhereIn('i.customer_id', $custArray);

        $q->andWhere('i.status=2');
        $q->orderBy('i.due_date');

        if (isset($invoice_no) && $invoice_no != '') {
            $q->addWhere("i.invoice_no LIKE '$invoice_no%'");
        }
        if (isset($com_name) && $com_name != '') {
            $q->addWhere("mp.merchant_name LIKE '$com_name%'");
        }
        if (isset($customerIdSearch) && $customerIdSearch != '') {
            $q->addWhere("cp.customer_id LIKE '$customerIdSearch%'");
        }

//echo "<pre>";
//print_r($q->execute()->toArray());
//exit;
        return $q->execute();
    }

    public function paymentHistoryForCustomer($customerProfileId, $offset=null, $limit=null, $com_name=null, $invoice_no=null, $customerIdSearch) {

        $q = Doctrine_Query::create()
                        ->from('Invoice i')
                        ->leftJoin('i.Customer cp');
        $q->leftJoin('i.Merchant mp');

        if (isset($customerIdSearch) && $customerIdSearch != '') {
            $q->addWhere("cp.customer_id LIKE '$customerIdSearch%'");
        }

        $custArray = explode(",", $customerProfileId);
        $q->andWhereIn('i.customer_id', $custArray);


        $q->andWhere("(i.status = 2 or i.status=3)");
        if ($offset) {
            $q->offset($offset);
        }
        if ($limit) {
            $q->limit($limit);
        }

        if (isset($invoice_no) && $invoice_no != '') {
            $q->addWhere("i.invoice_no LIKE '$invoice_no%'");
        }
        if (isset($com_name) && $com_name != '') {
            $q->addWhere("mp.merchant_name LIKE '$com_name%'");
        }

        $q->orderBy('i.payment_date');

        return $q->execute();
    }

    // Varun : regenerated field set to 1 in case Invoice is regenerated
    public function updateRegenerated($id) {
        $query = Doctrine_Query::create()->update('Invoice i')->set('i.regenerated', '?', '1')
                        ->where('i.id= ?', $id);
        $query->execute();
    }

    /**
     * @author:  Manu Datta
     * @param <type> $type
     * @purpose:    To get invoice details
     */
    public function getInvoices($type) {
        $q = Doctrine_Query::create()
                        ->select('i.id,i.amount_due as amount_due,i.due_date as due_date,i.issue_date as issue_date,i.invoice_no as invoice_no,cp.name as customer_name,sfgu.email_address as email_address,mp.merchant_name as company_name,mp.tiny_url as tiny_url,mp.company_url as company_url')
                        ->from('Invoice i')
                        ->leftJoin('i.Customer cp')
                        ->leftJoin('cp.sfGuardUser sfgu')
                        ->leftJoin('i.Merchant mp')
                        ->whereIn('i.status', array(0, 3));
        if ($type == 'monthly') {
            $q->andWhere('i.due_date >=?', date('Y-m-d', strtotime("+1 month", strtotime(date('Y-m-d')))));
        } else if ($type == 'weekly') {
            $q->andWhere(" i.due_date BETWEEN '" . date('Y-m-d', strtotime("+1 week", strtotime(date('Y-m-d')))) . "' AND '" . date('Y-m-d', strtotime("+1 month", strtotime(date('Y-m-d')))) . "'");
        } else if ($type == 'daily') {
            $q->andWhere(" i.due_date BETWEEN '" . date('Y-m-d', strtotime("+1 day", strtotime(date('Y-m-d')))) . "' AND '" . date('Y-m-d', strtotime("+1 week", strtotime(date('Y-m-d')))) . "'");
        }
        return $q->execute()->toArray();
    }

    /*
     * Purpose to set the variable  that full amunt is paid
     * 05 Apr 2012
     * Santosh Kumar
     *
     */

    public function updateFullAmount($id, $fullAmtStatus) {
        $query = Doctrine_Query::create()->update('Invoice i')->set('i.pay_full_amount', '?', $fullAmtStatus)
                        ->where('i.id= ?', $id);
        $query->execute();
    }

    /**
     * Bug fix : FS#34885
     * @param <type> $invoice_number
     * @return <type>
     */
    public function findPaidInvoiceNo($invoice_number) {
        $array = array(2, 3);
        $q = Doctrine_Query::create()
                        ->from('Invoice i');
        $q->addSelect('i.*');
        $q->andWhere('i.invoice_no =? ', $invoice_number);
        $q->andWhereIn('i.status', $array); // For paid and partially paid Bug fix : FS#34885
        return $q->execute()->toArray();
    }

    /*
     * Varun : Update amount due
     */

    public function updateAmountDue($id, $amtDue) {
        $query = Doctrine_Query::create()->update('Invoice i')->set('i.amount_due', '?', $amtDue)
                        ->where('i.id= ?', $id);
        $query->execute();
    }

    /*
     * To get the max invoice no
     *  17 may 2012
     *  Santosh Kumar
     */

    public function getMaxInvoiceNo($merchantId) {
        return $q = Doctrine_Query::create()
                ->select('invoice_no as maxInvno')
                ->from('Invoice i')->where('merchant_id=?', $merchantId)
                ->limit(1)
                ->orderBy('i.invoice_no Desc')
                ->execute()
                ->toArray();
    }

    /*
     * To get the Merchant current or future invoice
     *  28 may 2012
     *  Santosh Kumar
     */

    public function getInvoiceByMerchant($merchantId) {
        return $q = Doctrine_Query::create()
                ->select('id')
                ->from('Invoice i')
                ->where('merchant_id=?', $merchantId)
                ->andWhere('due_date >= ?', date('Y-m-d'))
                ->andWhere('status <> ?', 2)
                ->limit(1)
                ->execute()->count();
    }

    /*
     * @Saurabh on 25 July
     * Return the id of the last invoice if matched
     */

    public function getLastInvoice($invoice_number) {
        return $q = Doctrine_Query::create()
                ->select('id')
                ->from('Invoice i')
                ->where('i.invoice_no=?', $invoice_number)
                ->orderBy('i.id Desc')
                ->limit(1)
                ->execute(array(), Doctrine::HYDRATE_ARRAY);
    }

}