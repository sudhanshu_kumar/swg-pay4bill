<?php

class TellerMerchantMappingTable extends Doctrine_Table {

    public static function getInstance() {
        return Doctrine_Core::getTable('TellerMerchantMapping');
    }

    public function getEnabledMerchantsMappedWithTeller($teller_profile_id) {

        return Doctrine_Query::create()
                ->from('TellerMerchantMapping tm')
                ->leftJoin('tm.MerchantProfile mp')
                ->addWhere('tm.teller_profile_id=?', $teller_profile_id)
                ->andWhere('mp.disabled=?', 0)
                ->andwhere('mp.approved = ?', 1)->execute();
    }

}