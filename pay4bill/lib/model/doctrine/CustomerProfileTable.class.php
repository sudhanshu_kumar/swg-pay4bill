<?php

class CustomerProfileTable extends Doctrine_Table {

    public static function getInstance() {
        return Doctrine_Core::getTable('CustomerProfile');
    }

    public function getMaxCustomerId($merchantId) {
        return $q = Doctrine_Query::create()
                ->select('cp.customer_id')
                ->from('CustomerProfile cp')->where('merchant_profile_id=?', $merchantId)
                ->limit(1)
                ->orderBy('cp.id Desc')
                ->execute()
                ->getFirst();
    }

    public static function getCustomers_for_merchant($merchantId, $customerId='') {
        $q = Doctrine_Query::create()
                        ->from('CustomerProfile cp');

        $q->where('cp.merchant_profile_id =? ', $merchantId);
        if (!empty($customerId))
            $q->andWhere('cp.id=?', $customerId);
        $q->andWhere('cp.is_active=?', 1); // @saurabh on 1 Jun 2012 : only those customers who are active by merchant
        $q->orderBy('cp.name');

        //echo $q->getSqlQuery();die;
        return $q;
    }

    /**
     * @author:  Manu Datta
     * @param <type> $merchant_id
     * @param <type> $offset
     * @param <type> $limit
     * @purpose:    To retrieve Existing Customer List
     */
    public function getList($merchant_id, $offset, $limit, $email_address=null, $cust_name=null) {
        $q = Doctrine_Query::create()
                        ->from('CustomerProfile cp');
        $q->leftJoin('cp.MerchantCustomer mc'); //Manu : Remove Merchant Id from customer table and mapping the customer and merchant MerchantCustomer table
        $q->leftJoin('cp.sfGuardUser sfgu');
        if ($offset) {
            $q->offset($offset);
        }
        if ($limit) {
            $q->limit($limit);
        }
//  Varun : Query midified to add search criteria variables.
        if (isset($email_address) && $email_address != '') {
            $q->addWhere("sfgu.email_address LIKE '$email_address%'");
        }
        if (isset($cust_name) && $cust_name != '') {
            $q->addWhere("cp.name LIKE '$cust_name%'");
        }
        $q->addWhere('mc.merchant_id = ?', $merchant_id);
        $q->orderBy('cp.name');
        return $q->execute();
    }

    /**
     * @author:  Manu Datta
     * @param <type> $merchant_id
     * @param <type> $customerId
     * @purpose:    To retrieve Valid Customer information
     */
    public function isValidCustomerForMerchant($merchant_id, $customerId) {

        $q = Doctrine_Query::create()
                        ->from('CustomerProfile cp');
        $q->andWhere('cp.merchant_profile_id = ?', $merchant_id);
        $q->andWhere('cp.customer_id = ?', $customerId);
        $q->andWhere('cp.is_active = ?', 1);
        return $q->execute();
    }

    /**
     * @author:  Manu Datta
     * @param <type> $name
     * @param <type> $email
     * @param <type> $phone_number
     * @param <type> $offset
     * @param <type> $limit
     * @purpose:    To retrieve Customer information
     */
    public function getCustomerInfo($name='', $email='', $gender='', $phone_number='', $merchantId='', $offset=null, $limit=null) {

        $q = Doctrine_Query::create()
                        ->from('CustomerProfile cp');
        $q->leftJoin('cp.sfGuardUser user');
        $q->leftJoin('cp.MerchantCustomer mc');
        $q->where('mc.merchant_id !=? ', $merchantId);
        $q->andWhere('mc.customer_id Not In (select customer_id from tbl_merchant_customer where merchant_id =' . $merchantId . ')');
        if ($name != '') {
            $q->andWhere('cp.name like "' . $name . '%"');
        }
        if ($gender != '') {
            $q->andWhere('cp.sex = ?', $gender);
        }
        if ($phone_number != '') {
            $q->andWhere('cp.phone_number = ?', $phone_number);
        }
        if ($email != '') {
            $q->andWhere('user.username = ?', $email);
        }
        if ($offset) {
            $q->offset($offset);
        }
        if ($limit) {
            $q->limit($limit);
        }
        return $q->execute();
    }

    /**
     * @author:  Manu Datta
     * @param <type> $customerId
     * @purpose:    To retrieve Customer information
     */
    public function getCustomerInfoById($customerId='') {
        if ($customerId != '') {
            $q = Doctrine_Query::create()
                            ->from('CustomerProfile cp');
//            $q->leftJoin('cp.sfGuardUser user');
            $q->andWhere('cp.id = ?', $customerId);
            return $q->execute(array(), Doctrine::HYDRATE_ARRAY);
        } else {
            return false;
        }
    }

    /**
     * @author:  Saurabh
     * @param <type> $customerId
     * @purpose:    To retrieve Customer information based on user_id
     */
    public function getCustomerInfoByUserId($customerUserId='') {
        if ($customerUserId != '') {
            $q = Doctrine_Query::create()
                            ->from('CustomerProfile cp');
            $q->leftJoin('cp.sfGuardUser user');
            $q->andWhere('cp.user_id = ?', $customerUserId);
            return $q->execute(array(), Doctrine::HYDRATE_ARRAY);
        } else {
            return false;
        }
    }

    /**
     * @author:  Saurabh
     * @param <type> $customerId
     * @purpose:   To retrieve total information of the customer based on user_id
     */
    public function getAllCustomerDetails($customerUserId='') {
        if ($customerUserId != '') {
            $q = Doctrine_Query::create()
                            ->from('CustomerProfile cp');
            $q->addselect('cp.*,mp.id,ob.id,attval.id,am.att_label,am.id,mp.merchant_name as merchant_name,attval.att_value, am.att_name');
            $q->leftJoin('cp.Object ob');
            $q->leftjoin('cp.MerchantProfile mp');
            $q->leftjoin('ob.AttributeValue attval');
            $q->leftjoin('attval.AttributeMaster am');
            $q->andWhere('cp.id = ?', $customerUserId);

            return $q->execute(array(), Doctrine::HYDRATE_ARRAY);
        }
    }

    /**
     * @author:  Manu Datta
     * @param <type> $email
     * @purpose:    To retrieve Customer information
     */
    public function customerExistence($email='') {
        $q = Doctrine_Query::create()
                        ->from('CustomerProfile cp');
        $q->leftJoin('cp.sfGuardUser user');
        if ($email != '') {
            $q->andWhere('user.email_address = ?', $email);
        }
        return $q->execute(array(), Doctrine::HYDRATE_ARRAY);
    }

    /*
     * to remove the customer
     * 21 May 2012
     * @ Santosh Kumar
     */

    public function unsetCustomer($intCustomerId) {
        return Doctrine_Query::create()->delete()->from('CustomerProfile')->where('id=?', $intCustomerId)->execute();
    }

    /*
     * to get the list of Customers for more than one merchant
     * 20 June 2012
     * @ Aditi M
     */

    public function getCustomersOfMerchants($merchantArray, $offset=0, $limit=0) {

        $q = Doctrine_Query::create()
                        ->from('CustomerProfile cp')
                        ->leftJoin('cp.MerchantProfile mprofile')
                        ->addSelect('cp.id,cp.name,cp.address,cp.phone_number,mprofile.merchant_name as mName');
        // Added condition for teller
        if (isset($_SESSION['logedin_user_group']) && $_SESSION['logedin_user_group'] == 'teller') {
            $q->andwhere("cp.is_active = '1'");
        }

        $q->whereIn('cp.merchant_profile_id', $merchantArray);
        if ($offset) {
            $q->offset($offset);
        }
        if ($limit) {
            $q->limit($limit);
        }
        return $q->execute();
    }

    /*
     * to get the list of Customers with searxh on name,address,email,phone_number,merchant_id,dynamic_sttributes
     * 20 June 2012
     * @ Aditi M
     */

    public function getCustomersListWithAttributeSearch($merchantsArray, $name='', $customer_id='', $email_address='', $address='', $phoneNumber='', $dynamic_attributes_array=array(), $offset=null, $limit=null) {
        $name = trim($name);
        $customer_id = trim($customer_id);
        $email_address = trim($email_address);
        $address = trim($address);
        $phoneNumber = trim($phoneNumber);


        $query = Doctrine_Query::create()
                        ->from('CustomerProfile cp')
                        ->leftJoin('cp.MerchantProfile mprofile');
        $query->addSelect('cp.id,cp.name,cp.customer_id,cp.address,cp.phone_number,mprofile.merchant_name as mName');

        if (count($merchantsArray) > 0) {
            $query->whereIn('cp.merchant_profile_id', $merchantsArray);
        }
        if ($name != "") {

            $query->addWhere("cp.name LIKE '" . $name . "%'");
        }

        // Only show those which are active customer against a merchant - FOR TELLER ONLY   @Saurabh 24 July 2012
        if (isset($_SESSION['logedin_user_group']) && $_SESSION['logedin_user_group'] == 'teller') {
            $query->addWhere("cp.is_active = '1'");
        }

        if ($customer_id != "") {
            $query->addWhere("cp.customer_id LIKE '" . $customer_id . "%'");
            $query->addSelect('cp.customer_id as cust_id');
        }
        if ($email_address != "") {
            $query->addWhere("cp.email_address LIKE '" . $email_address . "%'");
            $query->addSelect('cp.email_address');
        }
        if ($address != "") {
            $query->addWhere("cp.address LIKE '%" . $address . "%'");
        }
        if ($phoneNumber != "") {
            $query->addWhere("cp.phone_number LIKE '" . $phoneNumber . "%'");
        }

        if (count($dynamic_attributes_array) > 0 && count($merchantsArray) == 1) {

            $joinWithAttributeValue = false;
            foreach ($dynamic_attributes_array as $attVal) {

                if (trim($attVal) != "" && !$joinWithAttributeValue) {
                    $joinWithAttributeValue = true;
                }
            }

            if ($joinWithAttributeValue) {

                $query->leftJoin('cp.Object ob');
                $query->leftJoin('ob.AttributeValue av');
                $query->leftJoin('av.AttributeMaster am');
                $query->addWhere("am.object_type_id = ?", 1);
                $query->whereIn('am.merchant_id', $merchantsArray);
                $i = 0;
                foreach ($dynamic_attributes_array as $attName => $attVal) {

                    $attVal = trim($attVal);
//                echo $attVal;
                    if ($attVal != "") {
                        if ($i > 0) {
                            $query->orWhere(" (am.att_name ='" . $attName . "' and av.att_value LIKE '" . $attVal . "%')");
//
                        } else {

                            $query->addWhere("(am.att_name ='" . $attName . "' and av.att_value LIKE '" . $attVal . "%')");
//                        $query->addWhere("av.att_value LIKE '%" . $attVal . "%'");
                        }
//                     echo $attName . "<br>";
                        $query->addSelect('ob.id,am.id,av.id,am.*,av.*');
                        ++$i;
                    }
                }
                $query->orderBy('am.id ASC');
            }
        }

        if ($offset) {
            $query->offset($offset);
        }
        if ($limit) {

            $query->limit($limit);
        }
        return $query->execute();
    }

    /**
     * @author:  Santosh Kumar
     * @param <type> $customerId
     * @purpose:    To retrieve Customer information
     */
    public function getCustomerMerchantInfoByCustomerId($customerId='') {
        if ($customerId != '') {
            $q = Doctrine_Query::create()
                            ->select('cp.name,cp.customer_id as customer_id, cp.merchant_profile_id as merchant_profile_id')
                            ->from('CustomerProfile cp')
//                            ->leftJoin('cp.sfGuardUser user')
//                            ->leftJoin('cp.MerchantCustomer mc')
                            ->andWhere('cp.id = ?', $customerId);
            return $q->execute(array(), Doctrine::HYDRATE_ARRAY);
        } else {
            return false;
        }
    }

    public function getCustomersWithUnpaidInvoicesInInterval($merchant_id='', $from_date='', $to_date='') {
        $q = Doctrine_Query::create()
                        ->from('CustomerProfile cp')
                        ->leftJoin('cp.Invoice inv')
                        ->leftJoin('cp.MerchantCustomer mc');

        if ($merchant_id != '') {

            $q->addWhere('mc.merchant_id=?', $merchant_id);
        }
        if ($from_date != '') {

            $q->addWhere('inv.delivery_date>=?' . $from_date);
        }
        if ($to_date != '') {

            $q->addWhere('inv.delivery_date<=?' . $to_date);
        }
        $q->addSelect('sum(inv.amount_due)');
        if ($offset) {
            $q->offset($offset);
        }
        if ($limit) {
            $q->limit($limit);
        }
        $q->addWhere('inv.status=?', 0);
        return $q->execute();
    }

    /*
     *
     * Aditi- 22 Aug 2012
     * To get Total Amount Paid by Customers
     */

    public function getTotalAmountPaidByCustomer($customer_name='', $customer_id='', $from_date='', $to_date='', $merchant_name, $offset=0, $limit=0) {
        $q = Doctrine_Query::create()
                        ->from('CustomerProfile cp')
                        ->leftJoin('cp.Invoice inv');
        $q->addSelect('cp.customer_id,cp.name,max(inv.payment_date) as payment_date');
        if (pay4billHelper::checkIfMerchantUser(sfContext::getInstance()->getUser()->getGuardUser()->getGroups()->toArray())) {


            $q->addWhere('cp.merchant_profile_id=?', sfContext::getInstance()->getUser()->getGuardUser()->getMerchantUser()->getId());
        } else { {
                $q->leftJoin('cp.MerchantProfile mp');
                $q->addSelect('mp.merchant_name');
                if ($merchant_name != '') {
                    $q->addWhere("mp.merchant_name  LIKE '$merchant_name%'");
                }
            }
        }


        if ($customer_id != '') {
            $q->addWhere("cp.customer_id  LIKE '$customer_id%'");
        }
        if ($customer_name != '') {
            $q->addWhere("cp.name LIKE '$customer_name%'");
        }
        if ($from_date != '') {

            $q->addWhere('inv.payment_date>=?', $from_date);
        }
        if ($to_date != '') {

            $q->addWhere('inv.payment_date<=?', $to_date);
        }
        $q->addSelect('sum(inv.amount_paid) as amount_paid');
        $q->addWhere('cp.is_active=?', 1);
        $q->andWhere('(inv.status=2 or inv.status=3)');
        $q->orderBy('amount_paid desc');
        $q->groupBy('inv.customer_id');

        if ($offset) {
            $q->offset($offset);
        }

        if ($limit) {
            $q->limit($limit);
        }
        return $q->execute();
    }

    /*
     * To check whether customer can be disable or not based on :
     * Customer would not be disabled if due date is greater than equal to todays date
     * And status is 2
     * @ Saurabh Kumar from MerchantCustomer to here on 19 July
     */

    public function canDisable($customerID, $merchantId) {

        if(Doctrine::getTable('MerchantProfile')->find($merchantId) != false){        
            switch (Doctrine::getTable('MerchantProfile')->find($merchantId)->getPaymentType()) {
                case 'o':
                    $q = Doctrine_Query::create()->from('invoice')
                                    ->andWhere('due_date >= ?', date("Y-m-d"))
                                    ->andWhere('merchant_id=?', $merchantId)
                                    ->andWhere('customer_id=?', $customerID)
                                    ->andWhere('(status=0 or status=3)');
                    break;
                case 'b':
                    $q = Doctrine_Query::create()->from('invoice')
                                    ->andWhere('(status=0 or status=3)')
                                    ->andWhere('merchant_id=?', $merchantId)
                                    ->andWhere('customer_id=?', $customerID);
                    break;
            }
            return $q->execute()->count();
        } else {
            return '';
        }       
    }

    public function customerIdAuthenticationWithPin($merchantProfileId, $customerId, $pin) {
        return Doctrine_Query::create()->from('CustomerProfile cp')
                ->leftJoin('cp.UserCustomerMapping ucm')
                ->where('cp.merchant_profile_id=?', $merchantProfileId)->addWhere('cp.customer_id=?', $customerId)
                ->addWhere('cp.pin=?', cryptString::encrypt($pin))->execute();
    }

    public function customerIdAuthenticationWithValidationNumber($merchantProfileId, $customerId, $validation_number) {


        return Doctrine_Query::create()->from('CustomerProfile cp')
                ->leftJoin('cp.UserCustomerMapping ucm')
                ->leftJoin('cp.BillPaymentRequest bpr')
                ->leftJoin('bpr.EpP4mRequest req')
                ->leftJoin('req.EpP4mResponse res')
                ->where('cp.merchant_profile_id= ?', $merchantProfileId)
                ->addWhere('cp.customer_id=?', $customerId)
                ->addWhere('res.validation_number=?', $validation_number)->execute();
    }

    /*     * ********
     *  Aditi- To view Customer Profiles related to logged in customer user
     *
     */

    public function getCustomerProfilesOfLoggedInUser($userId, $customerId='', $merchantName='', $offset=0, $limit=0) {

        $q = Doctrine_Query::create()
                        ->from('CustomerProfile cp')
                        ->leftJoin('cp.UserCustomerMapping ucm')
                        ->where('ucm.user_id=?', $userId)
                        ->orderBy('cp.name Asc');
        if ($customerId != '') {
            $q->addWhere("cp.customer_id LIKE '$customerId%'");
        }
        if ($merchantName != '') {
            $q->leftJoin('cp.MerchantProfile mp');
            $q->addWhere("mp.merchant_name LIKE '$merchantName%'");
        }
        if ($offset) {
            $q->offset($offset);
        }
        if ($limit) {
            $q->limit($limit);
        }
        return $q->execute();
    }

    /*
     *
     * Aditi- 22 Aug 2012
     * To get Total Amount Paid by Customers
     */

    public function getCustomersWithPendingInvoices($customer_name='', $customer_id='', $from_date='', $to_date='', $merchant_name, $offset=0, $limit=0) {
        $sql_where = '';

        if ($customer_id != '') {
            $sql_where.=" AND t.customer_id LIKE '" . $customer_id . "%'";
        }
        if ($customer_name != '') {
            $sql_where.=" AND t.name LIKE '" . $customer_name . "%'";
        }
        if ($from_date != '') {
            $sql_where.=" AND t2.delivery_date >='" . $from_date . "'";
        }
        if ($to_date != '') {
            $sql_where.=" AND t2.delivery_date <='" . $to_date . "'";
        }
        $sql_where.=" AND t.is_active ='1'";



        if (pay4billHelper::checkIfMerchantUser(sfContext::getInstance()->getUser()->getGuardUser()->getGroups()->toArray())) {

            $sql_where .=" AND t.merchant_profile_id = " . sfContext::getInstance()->getUser()->getGuardUser()->getMerchantUser()->getId();
        } else {

            $sql = "select customer_id,name,max(payment_date) as payment_date,sum(amount_due) as total_amount_due,merchant_name from
            (SELECT t.customer_id AS customer_id, t.name as name, MAX(t2.payment_date) AS payment_date,tmp.merchant_name as merchant_name,
            t2.status AS invoice_status, SUM(t2.total_amount) AS amount_due FROM tbl_customer_profile t
            LEFT JOIN tbl_invoice t2 ON t.id = t2.customer_id LEFT JOIN tbl_merchant_profile tmp ON t.merchant_profile_id = tmp.id WHERE t2.status = 0 " . $sql_where . " union all SELECT t.customer_id AS customer_id, t.name AS name,
            MAX(t2.payment_date) AS payment_date,tmp.merchant_name as merchant_name,t2.status AS invoice_status, SUM(t2.amount_due) AS amt_due FROM tbl_customer_profile t
            LEFT JOIN tbl_invoice t2 ON t.id = t2.customer_id LEFT JOIN tbl_merchant_profile tmp ON t.merchant_profile_id = tmp.id
            WHERE t2.status = 3 " . $sql_where . ") ";



            if ($merchant_name != '') {
                $sql_where.=" AND tmp.merchant_name LIKE '" . $merchant_name . "%'";
            }
        }
        $sql_where.=" group by t.customer_id";
        if (pay4billHelper::checkIfMerchantUser(sfContext::getInstance()->getUser()->getGuardUser()->getGroups()->toArray())) {

            $sql = "select customer_id,name,max(payment_date) as payment_date,sum(amount_due) as total_amount_due from
            (SELECT t.customer_id AS customer_id, t.name as name, MAX(t2.payment_date) AS payment_date,
            t2.status AS invoice_status, SUM(t2.total_amount) AS amount_due FROM tbl_customer_profile t
            LEFT JOIN tbl_invoice t2 ON t.id = t2.customer_id WHERE t2.status = 0 " . $sql_where . " union all SELECT t.customer_id AS customer_id, t.name AS name,
            MAX(t2.payment_date) AS payment_date,t2.status AS invoice_status, SUM(t2.amount_due) AS amt_due FROM tbl_customer_profile t
            LEFT JOIN tbl_invoice t2 ON t.id = t2.customer_id
            WHERE t2.status = 3 " . $sql_where . ") ";
        } else {
            if ($merchant_name != '') {


                $sql_where.=" AND tmp.merchant_name LIKE '" . $merchant_name . "%'";
            }
            $sql = "select customer_id,name,max(payment_date) as payment_date,sum(amount_due) as total_amount_due,merchant_name from
            (SELECT t.customer_id AS customer_id, t.name as name, MAX(t2.payment_date) AS payment_date,tmp.merchant_name as merchant_name,
            t2.status AS invoice_status, SUM(t2.total_amount) AS amount_due FROM tbl_customer_profile t
            LEFT JOIN tbl_invoice t2 ON t.id = t2.customer_id LEFT JOIN tbl_merchant_profile tmp ON t.merchant_profile_id = tmp.id WHERE t2.status = 0 " . $sql_where . " union all SELECT t.customer_id AS customer_id, t.name AS name,
            MAX(t2.payment_date) AS payment_date,tmp.merchant_name as merchant_name,t2.status AS invoice_status, SUM(t2.amount_due) AS amt_due FROM tbl_customer_profile t
            LEFT JOIN tbl_invoice t2 ON t.id = t2.customer_id LEFT JOIN tbl_merchant_profile tmp ON t.merchant_profile_id = tmp.id
            WHERE t2.status = 3 " . $sql_where . ") ";
        }
        $sql.=" as d group by customer_id";
        if ($limit) {
            $sql.=" LIMIT " . $limit;
        }
        if ($offset) {
            $sql.=" OFFSET " . $offset;
        }

        return Doctrine_Manager::getInstance()->getCurrentConnection()->execute($sql)->fetchAll();
    }

}
