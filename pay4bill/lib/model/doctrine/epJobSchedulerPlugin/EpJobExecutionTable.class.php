<?php


class EpJobExecutionTable extends PluginEpJobExecutionTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('EpJobExecution');
    }
}