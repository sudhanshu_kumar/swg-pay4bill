<?php

class TellerProfileTable extends Doctrine_Table {

    public static function getInstance() {
        return Doctrine_Core::getTable('TellerProfile');
    }

    public function getTellersList($username ='', $email_address='',$bankname,$offset=0, $limit=0) {

        $q = Doctrine_Query::create()->from('TellerProfile t')->leftJoin('t.sfGuardUser s');
        
        if(isset($username) && $username <> ''){            
            $q->andWhere("s.username like '" . $username . "%'");            
        }    
        if(isset($email_address) && $email_address <> ''){            
            $q->andWhere("s.email_address like '" . $email_address . "%'");
        }

        if(isset($bankname) && $bankname <> ''){
            $q->andWhere("t.bankname like '" .$bankname . "%'");
        }
        if ($offset) {
            $q->offset($offset);
        }
        if ($limit) {
            $q->limit($limit);
        }
        return $q->execute();
    }

   /**
     * @author:  Saurabh
     * @param <type> $tellerId
     * @purpose:    To retrieve teller information based on the ID
     */
    public function getTellerInfoById($tellerId='') {
        if ($tellerId != '') {
            $q = Doctrine_Query::create()
                            ->from('TellerProfile tp');
            $q->andWhere('tp.user_id = ?', $tellerId);
            return $q->execute(array(), Doctrine::HYDRATE_ARRAY);
        } else {
            return false;
        }
    }

}