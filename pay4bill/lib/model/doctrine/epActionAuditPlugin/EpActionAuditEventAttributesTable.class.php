<?php


class EpActionAuditEventAttributesTable extends PluginEpActionAuditEventAttributesTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('EpActionAuditEventAttributes');
    }
}