<?php


class EpActionAuditEventTable extends PluginEpActionAuditEventTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('EpActionAuditEvent');
    }
}