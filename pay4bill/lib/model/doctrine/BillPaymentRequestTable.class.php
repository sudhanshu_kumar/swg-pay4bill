<?php

class BillPaymentRequestTable extends Doctrine_Table {

    public static function getInstance() {
        return Doctrine_Core::getTable('BillPaymentRequest');
    }

    /** Check if Invoice for this customer and merchant combination exists  -@Author-Aditi Malhotra* */
    public function findLatestInvoiceOfCustomer($customerId, $merchantId) {
        return Doctrine_Query::create()->
                from("BillPaymentRequest bpr")
                ->leftJoin("bpr.MerchantProfile mp")
                ->leftJoin('bpr.CustomerProfile cp')
                ->addWhere("customer_id=" . $customerId)
                ->andWhere("merchant_id=" . $merchantId)
                ->andWhere("invoice_id is NOT NULL")->orderBy("bpr.created_at DESC")->execute();
    }

    public function totalAmountCollectedPerBank($from_date='', $to_date='', $bankName='', $offset=0, $limit=0) {


        $sql = "SELECT res.bank_name AS bank_name, COUNT(res.id) AS no_of_payments,MAX(date(invoice.payment_date)) AS last_payment_date, 
                SUM(res.total_amount) AS totalamount 
                FROM tbl_bill_payment_request bpr 
                INNER JOIN ep_p4m_request req  ON bpr.id = req.item_number 
                INNER JOIN ep_p4m_response res ON req.id = res.request_id 
                INNER JOIN tbl_invoice invoice ON bpr.invoice_id = invoice.id 
                WHERE (res.payment_status = '0' AND (invoice.status = '2' or invoice.status = '3') AND res.bank_name is not null AND res.bank_name != ''";
        if (pay4billHelper::checkIfMerchantUser(sfContext::getInstance()->getUser()->getGuardUser()->getGroups()->toArray())) {
            $sql.= " and invoice.merchant_id='" . sfContext::getInstance()->getUser()->getGuardUser()->getMerchantUser()->getId() . "'";
        }
        if ($from_date != "") {
            $sql.= " and invoice.payment_date>='" . $from_date . "'";
        }
        if ($to_date != "") {
            $sql.= " and invoice.payment_date<='" . $to_date . "'";
        }
        if ($bankName != "") {
            $sql.= "and res.bank_name like '$bankName%'";
        }

        $sql.= ") GROUP BY res.bank_name ORDER BY res.bank_name";


        if ($limit) {
            $sql.=" LIMIT " . $limit;
        }
        if ($offset) {
            $sql.=" OFFSET " . $offset;
        }

        return Doctrine_Manager::getInstance()->getCurrentConnection()->execute($sql);
    }

    public function allMerchantPerBank($from_date='', $to_date='', $bankName='', $merchant_name ='', $offset=0, $limit=0) {
        
        $sql = "SELECT mp.merchant_name, bpr.*, res.bank_name as bank_name, COUNT(res.id) AS no_of_payments,MAX(date(invoice.payment_date)) 
                AS last_payment_date,sum(res.total_amount) as totalamount 
                FROM tbl_bill_payment_request bpr 
                INNER JOIN ep_p4m_request req       ON bpr.id = req.item_number 
                INNER JOIN ep_p4m_response res      ON req.id = res.request_id 
                INNER JOIN tbl_invoice invoice      ON bpr.invoice_id = invoice.id 
                INNER JOIN tbl_merchant_profile mp  ON bpr.merchant_id= mp.id 
                WHERE (res.payment_status = '0' AND (invoice.status = '2' or invoice.status = '3') AND res.bank_name is not null AND res.bank_name != ''";
        
        if ($from_date != "")       $sql.= " and invoice.payment_date>='" . $from_date . "'";
        if ($to_date != "")         $sql.= " and invoice.payment_date<='" . $to_date . "'";
        if ($bankName != "")        $sql.= "and res.bank_name like '$bankName%'";
        if ($merchant_name != "")   $sql.= "and mp.merchant_name like '$merchant_name%'";
        
        $sql.= ") GROUP BY bpr.merchant_id ORDER BY sum(res.total_amount)";
        
        if ($limit) $sql.=" LIMIT " . $limit;
        if ($offset)$sql.=" OFFSET " . $offset;
        
        return Doctrine_Manager::getInstance()->getCurrentConnection()->execute($sql);
    }

}