<?php

class TaxTable extends Doctrine_Table {

    public static function getInstance() {
        return Doctrine_Core::getTable('Tax');
    }

    /**
     * Function : TaxList
     * Parameter : Offset, limit, flag
     * Varun : Function to get details for tax list
     */
    public function findTaxList($offset=null, $limit=null, $flag=null) {
        $q = Doctrine_Query::create()
        ->select('DISTINCT tx.id, tx.tax_name, tx.description, tx.status') // fixed paging bug on 3rd May 2012:Santosh Kumar
        ->from('tax tx');
        $q->leftJoin('tx.MerchantTax mtx');

        if ($offset) {
            $q->offset($offset);
        }
        if ($limit) {
            $q->limit($limit);
        }

        if ($flag) {
            $q->andWhere("tx.status = ?", "1");
        }
        $q->orderBy('tx.id DESC'); //Varun : Bug fix :FS#35009
//        echo $q->getSqlQuery();
        return $q->execute();
    }

    /*
     *   Function : EntryForTax
     *   Varun : Function to check tax existence.
     */

    public function findEntryForTax($tax, $id=null) {
        $q = Doctrine_Query::create()
                        ->from('tax tx');
        $q->andWhere('tx.tax_name = ?', $tax);
        if ($id != '' && isset($id)) {
            $q->andWhere('tx.id != ?', $id);
        }
        return $q->execute()->count();
    }

    /* Varun
     * Function : TaxDetailsOfMerchant
     * Function to find tax details for a merchant
     */

    public function findTaxDetailsOfMerchant($merchant_id) {
        $tax_value = array();
        $q = Doctrine_Query::create()
                        ->from('tax tx');
        $q->leftJoin('tx.MerchantTax mtx');
        $q->andWhere('tx.status = ?', '1');
        $q->andWhere('mtx.merchant_id = ?', $merchant_id);
        $taxInfo = $q->execute(array(), Doctrine::HYDRATE_ARRAY);
        //Manu : Change variable name resolve encription issue
        if (!empty($taxInfo)) {
            foreach ($taxInfo as $value) {
                $tax_value[] = $value['id']." - ".$value['tax_name'] . " - " . $value['MerchantTax']['tax_value'];
            }
            return $tax_value;
        }
    }
        /*
     *   Function : F
     *   Varun : Function to check tax existence.
     */

    public function findTax($id) {
        $q = Doctrine_Query::create()
                        ->from('tax tx');
        if ($id != '' && isset($id)) {
            $q->andWhere('tx.id = ?', $id);
        }
        return $q->execute();
    }

}