<?php

class MerchantCustomerTable extends Doctrine_Table {

    public static function getInstance() {
        return Doctrine_Core::getTable('MerchantCustomer');
    }

    /*
     * to remove the customer
     * 21 May 2012
     * @ Santosh Kumar
     */
    public function unsetMerchantAsCustomer($intMerchantId, $intCustomerId) {
        return Doctrine_Query::create()->delete()->from('MerchantCustomer')->where('merchant_id=?', $intMerchantId)->andWhere('customer_id=?', $intCustomerId)->execute();
    }

    /*
     * To enable the customer
     * 1 Jun 2012
     * @ Saurabh Kumar
     */
    public function setActivateCustomer($customerID, $merchantId){
        $q = Doctrine_Query::create()->update('MerchantCustomer MC')->set('is_active','?', '1')->where('merchant_id=?',$merchantId)
            ->andWhere('customer_id=?',$customerID);
//       echo $q->getSqlQuery();die;
       return $q->execute();
    }
    
    /*
     * To disable the customer
     * 1 Jun 2012
     * @ Saurabh Kumar
     */
    
    public function setDisableCustomer($customerID, $merchantId){
        $q = Doctrine_Query::create()->update('MerchantCustomer MC')->set('is_active','?', '0')->where('merchant_id=?',$merchantId)
            ->andWhere('customer_id=?',$customerID);
       return $q->execute();
    }

    /*
     * To check whether customer can be disable or not based on :
     * Customer would not be disabled if due date is greater than equal to todays date
     * And status is 2
     * @ Saurabh Kumar
     */
    public function canDisable($customerID, $merchantId){
        $q = Doctrine_Query::create()->from('invoice')
        ->where('due_date >= date(now()) and status != 2')
        ->andWhere('merchant_id=?',$merchantId)
        ->andWhere('customer_id=?',$customerID);
       return $q->execute()->count();
     
    }

    /*
     * @Saurabh 21 June 2012
     * Get all merchant from tbl_teller_merchant table
     *
     */
    public function getAllMerchantFromTellerMerchant($id) {
        $q = Doctrine_Query::create()
                        ->from('TellerMerchantMapping tm')
                        ->leftJoin('tm.MerchantProfile mp')
                        ->addselect("tm.*, mp.merchant_name as merchant_name")
                        ->where('tm.teller_profile_id = ?', $id)
                        ->andwhere('mp.approved = ?', 1)
                        ->andwhere('mp.disabled = ?', 0);
//                        ->orderby('mp.merchant_name asc');


          //echo "<pre>";print_r($q->execute(array(), Doctrine::HYDRATE_ARRAY));die;

        return $q->execute();

    }


}