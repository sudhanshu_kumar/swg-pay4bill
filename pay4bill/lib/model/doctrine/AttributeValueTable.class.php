<?php

class AttributeValueTable extends Doctrine_Table {

    public static function getInstance() {
        return Doctrine_Core::getTable('AttributeValue');
    }

    public function checkIfUniqueValueOfAttribute($object_id, $master_attr_id, $att_value, $object_type_id=1) {//$object_type_id=1 for customer
        $q = Doctrine_Query::create()->from('AttributeValue av')
                        ->leftJoin('av.Object ob')
                        ->where('av.att_master_id = ?', $master_attr_id)
                        ->andWhere('av.att_value LIKE "' . $att_value . '"')
                        ->andWhere('ob.object_type_id= ?', $object_type_id)
                        ->andWhere('av.object_id!='. $object_id);
        
        if ($q->execute()->count() > 0) {

            return false;
        } else {
            return true;
        }
    }

}