<?php


class sfGuardUserGroupTable extends PluginsfGuardUserGroupTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('sfGuardUserGroup');
    }
    //Varun : Delete previous enrty for user id details in sf guard user group
    public function deletePrev($id)
    {
     $deleted = Doctrine_Query::create()
      ->delete()
      ->from('sfGuardUserGroup')
      ->andWhere('user_id =?', $id)
      ->execute();

  }
}