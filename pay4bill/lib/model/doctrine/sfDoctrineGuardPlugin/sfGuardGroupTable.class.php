<?php

class sfGuardGroupTable extends PluginsfGuardGroupTable {

    public static function getInstance() {
        return Doctrine_Core::getTable('sfGuardGroup');
    }

    public function getGrpIdsFromGroupNames($grpNames_array) {

        return Doctrine_Query::create()->from('sfGuardGroup')->whereIn('name', $grpNames_array)->execute();
    }

}