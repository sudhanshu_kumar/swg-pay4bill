<?php


class sfGuardUserTable extends PluginsfGuardUserTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('sfGuardUser');
    }
//    Varun: Function for info by Id
    public function getInfoById($id)
    {
        $q = Doctrine_Query::create()
        ->from('sfGuardUser sf');
        $q->addWhere('sf.id = ?', $id);
        return $q->execute();
    }
    /*
     * To get the value based on the ID
     * Created by Saurabh
     */
     public function getSelectedInfoById($id)
    {
       if(!empty($id))
       return Doctrine_Query::create()->from('sfGuardUser u')->andWhere('u.id = ?', $id)->execute(array(),Doctrine::HYDRATE_ARRAY);
    }
//    Varun : Function to check email address existence.
    public function findEntryForEmailAddress($emailAddress,$id=null)
    {
        $q = Doctrine_Query::create()
        ->from('sfGuardUser sf');
        $q->andWhere('sf.email_address = ?', $emailAddress);
         if($id!='' && isset($id)){
            $q->andWhere('sf.id != ?', $id);
        }
        return $q->execute()->count();

    }
    //    Varun : Function to check username existence.
    public function findEntryForUserName($username,$id=null)
    {
        $q = Doctrine_Query::create()
        ->from('sfGuardUser sf');
        $q->andWhere('sf.username = ?', $username);
        if($id!='' && isset($id)){
            $q->andWhere('sf.id != ?', $id);
        }
        return $q->execute()->count();

    }
    // Varun : Function to get details for admin list
    public function findAdminList($offset=null,$limit=null)
    {
        $q = Doctrine_Query::create()
           ->from('sfGuardUser sf');
        $q->leftJoin('sf.sfGuardUserGroup sfg');
        $q->addSelect('sf.*,sfg.*');
         if ($offset) {
            $q->offset($offset);
        }
        if ($limit) {
            $q->limit($limit);
        }
//        $q->andWhere('sfg.group_id!= ?', '1');
//        $q->andWhere('sfg.group_id!= ?', '2');
//        $q->andWhere('sfg.group_id!= ?', '3');
        $q->andWhere('sfg.group_id = ?', '4');
        $q->orWhere('sfg.group_id = ?', '5');
        
        $q->orderBy('sf.id DESC');
        return $q->execute();

    }
    // Varun : Function to get details for admin list
    public function getPermissions($group_id)
    {
        $q = Doctrine_Query::create()
           ->select('sfp.name')
           ->from('sfGuardPermission sfp')
           ->leftJoin('sfp.sfGuardGroupPermission sfg')
           ->where('sfg.group_id =?',$group_id);
           return $q->execute(array(),Doctrine::HYDRATE_SINGLE_SCALAR);

    }
    /* function getSubAdminPermissions
     * Purpose: to get all permission of subadmin
     *  Author: Santosh Kumar
     *  Date: 02 May 2012
     */
    public function getSubAdminPermissions($group_id)
    {
        $q = Doctrine_Query::create()
           ->select('sfp.name')
           ->from('sfGuardPermission sfp')
           ->leftJoin('sfp.sfGuardGroupPermission sfg')
           ->whereIn('sfg.group_id',$group_id);
           return $q->execute(array(),Doctrine::HYDRATE_SINGLE_SCALAR);

    }
}