<?php

class MerchantProfileTable extends Doctrine_Table {

    public static function getInstance() {
        return Doctrine_Core::getTable('MerchantProfile');
    }

    /*
     * @Saurabh 29 Jun 2012
     * To retrieve only merchant with service code and
     * whose profile has been created
     */

    public function getBillerMerchantWithServiceCodeKey($merchant_name='') {
        $q= Doctrine_Query::create()
                ->from('MerchantProfile mp')
                ->leftJoin('mp.sfGuardUser sgu')->addWhere('mp.merchant_service_id != "" ')
                ->addWhere('sgu.is_active = 1 ')
                ->addWhere('mp.disabled = 0')
                ->addWhere("mp.payment_type ='b'")
                ->andwhere('mp.company_url is NOT NULL')
                ->addWhere('mp.approved= 1 ')
               ->addWhere('mp.merchant_code != "" ')->addWhere('mp.merchant_key != "" ');
        if($merchant_name!='')
        {
           return $q->andwhere("mp.merchant_name like '".$merchant_name."'")->execute();
             
        }
    return $q;
}

    public function detailedMerchantProfile($offset=null, $limit=null, $email_address=null, $cac_reg=null, $mer_name=null) {
        $q = Doctrine_Query::create()
                        ->from('MerchantProfile mp')
                        ->leftJoin('mp.sfGuardUser sfgu')
                        ->where('mp.company_url is NOT NULL');

        if (isset($email_address) && $email_address != '' || isset($mer_name) && $mer_name != '' || isset($cac_reg) && $cac_reg != '') {
            if (isset($email_address) && $email_address != '') {
                $q->addWhere("sfgu.email_address LIKE '$email_address%'");
            }
            if (isset($mer_name) && $mer_name != '') {
                $q->addWhere("mp.merchant_name LIKE '$mer_name%'");
            }
            if (isset($cac_reg) && $cac_reg != '') {
                $q->addWhere("mp.cac_registration LIKE '$cac_reg%'");
            }
        }


        if ($offset) {
            $q->offset($offset);
        }
        if ($limit) {
            $q->limit($limit);
        }
        //Varun : Query commented to display all merchants 
//        $q->addWhere('sfgu.is_active=?',1);
        //$q->andWhere('mp.disabled=?',0);
        $q->andWhere('mp.approved!= 0');
        $q->orderBy('mp.merchant_name');
        return $q->execute();
    }

    public function findLogo($merchantId, $setCompanyLogo) {
        $q = Doctrine_Query::create()
                        ->update('MerchantProfile mp')
                        ->set('mp.company_logo', '?', $setCompanyLogo)
                        ->where('mp.merchant_id =?', $merchantId);
        $res = $q->execute();
        if ($res) {
            return true;
        } else {
            return false;
        }
    }

    public function validateBillerRequest($merchantServiceId, $value) {

        $q = Doctrine_Query::create()
                        ->from('MerchantProfile mp')
                        ->leftJoin("mp.AttributeMaster am")
                        ->leftJoin("am.AttributeValue av")
                        ->addWhere('mp.merchant_service_id=' . $merchantServiceId)
                        ->andWhere("av.att_value='" . $value . "'");

        if ($q->count() > 0) {


            return $q->execute()->getFirst();
        } else {
            return false;
        }
    }

    public function removeLogo($merchantId) {
        $q = Doctrine_Query::create()
                        ->update('MerchantProfile mp')
                        ->set('mp.company_logo', '?', 'NULL')
                        ->where('mp.merchant_id =?', $merchantId);
        $res = $q->execute();
        if ($res) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @author:  Manu Datta
     * @param <type> $merchantId
     * @purpose:    To retrieve Customer information
     */
    public function getMerchantInfoById($merchantId='') {
        if ($merchantId != '') {
            $q = Doctrine_Query::create()
                            ->from('MerchantProfile mp');
            $q->andWhere('mp.id = ?', $merchantId);
            return $q->execute(array(), Doctrine::HYDRATE_ARRAY);
        } else {
            return false;
        }
    }

    /**
     * @author:  Saurabh
     * @param <type> $merchantId
     * @purpose:    To retrieve merchant information based on the ID
     */
    public function getMerchantInfoByMerchantId($merchantId='') {
        if ($merchantId != '') {
            $q = Doctrine_Query::create()
                            ->from('MerchantProfile mp');
            $q->andWhere('mp.merchant_id = ?', $merchantId);
            return $q->execute(array(), Doctrine::HYDRATE_ARRAY);
        } else {
            return false;
        }
    }

    /*
     * Purpose: to getthe list of pending approval of merchant
     * 21 may 2012
     * Santosh kumar
     */

    public function getPendingApprovalMerchant($offset=null, $limit=null) {
        $q = Doctrine_Query::create()
                        ->from('MerchantProfile mp')
                        ->leftJoin('mp.sfGuardUser sfgu')
                        ->where('mp.approved=?', 0);
        if ($offset) {
            $q->offset($offset);
        }
        if ($limit) {
            $q->limit($limit);
        }
        $q->orderBy('sfgu.email_address');
        return $q->execute();
    }

    public function getMerchantCodeKeyfromServiceId($merchantServiceId) {


        return Doctrine_Query::create()
                ->from('MerchantProfile mp')
                ->addWhere('mp.merchant_service_id=' . $merchantServiceId)->execute()->getFirst();
    }
    
    public function getFullyConfiguredActiveMerchants() {
        
        $q= Doctrine_Query::create()
                ->from('MerchantProfile mp')
                ->leftJoin('mp.sfGuardUser sgu')->addWhere('mp.merchant_service_id != "" ')
                ->addWhere('sgu.is_active = 1 ')
                ->addWhere('mp.disabled = 0')
                ->andwhere('mp.company_url is NOT NULL')
                ->addWhere('mp.approved= 1 ')
               ->addWhere('mp.merchant_code != "" ')->addWhere('mp.merchant_key != "" ');
       
    return $q;
}

}