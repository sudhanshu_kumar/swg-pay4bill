<?php

class BankTable extends Doctrine_Table {

    public static function getInstance() {
        return Doctrine_Core::getTable('Bank');
    }

    /**
     * Varun : Update details for merchant bank
     */
    public function updateById($id, $bank_name, $bank_branch, $account_name, $account_number, $sort_code) {

        $query = Doctrine_Query::create()->update('Bank b')
                        ->set('b.bank_name', '?', $bank_name)
                        ->set('b.bank_branch', '?', $bank_branch)
                        ->set('b.account_name', '?', $account_name)
                        ->set('b.account_number', '?', $account_number)
                        ->set('b.sort_code', '?', $sort_code)
                        ->where('b.id= ?', $id);
        $query->execute();
    }

    /**
     * Manu : To Get All banks
     */
    public function findAllBank($offset, $limit, $ban_name = null, $com_name = null) {
        $q = Doctrine_Query::create()
                        ->from('Bank b')
                        ->leftjoin('b.sfGuardUser g')
                        ->leftjoin('g.MerchantUser mp');

        if (isset($com_name) && $com_name != '') {


            $q->addWhere("mp.merchant_name LIKE '$com_name%'");
        }
        if (isset($ban_name) && $ban_name != '') {
            $q->addWhere("b.bank_name LIKE '$ban_name%'");
        }


        if ($offset) {
            $q->offset($offset);
        }
        if ($limit) {
            $q->limit($limit);
        }
        $q->orderBy('b.id');
//
//        echo $q->getSqlQuery();
        return $q->execute();
    }

    public function findMerchantId($id, $offset, $limit) {
        $q = Doctrine_Query::create()
                        ->from('Bank b');
        if ($offset) {
            $q->offset($offset);
        }
        if ($limit) {
            $q->limit($limit);
        }
        $q->addWhere('b.merchant_id = ?', $id);
// Post dated and Past invoice display
//        $q->andWhere("i.delivery_date <='" . date('Y-m-d') . "'");
//        $q->andWhere("i.due_date >='" . date('Y-m-d') . "'");
        $q->orderBy('b.id');

        return $q->execute();
    }

    /**
     * Varun : To check exist for merchant
     */
    public function findBankForMerchantId($merchantId, $bank_name, $id=null) {
        $q = Doctrine_Query::create()
                        ->from('Bank b');
        $q->andWhere('b.merchant_id = ?', $merchantId);
        $q->andWhere('b.bank_name = ?', $bank_name);
        if ($id != '' && isset($id)) {
            $q->andWhere('b.id != ?', $id);
        }
        $q->orderBy('b.id');
        return $q->execute()->count();
    }

}