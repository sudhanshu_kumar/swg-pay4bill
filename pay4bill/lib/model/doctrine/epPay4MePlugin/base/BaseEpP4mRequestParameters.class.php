<?php

/**
 * BaseEpP4mRequestParameters
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $id
 * @property integer $request_id
 * @property string $parameter_name
 * @property string $parameter_value
 * @property EpP4mRequest $EpP4mRequest
 * 
 * @method integer                getId()              Returns the current record's "id" value
 * @method integer                getRequestId()       Returns the current record's "request_id" value
 * @method string                 getParameterName()   Returns the current record's "parameter_name" value
 * @method string                 getParameterValue()  Returns the current record's "parameter_value" value
 * @method EpP4mRequest           getEpP4mRequest()    Returns the current record's "EpP4mRequest" value
 * @method EpP4mRequestParameters setId()              Sets the current record's "id" value
 * @method EpP4mRequestParameters setRequestId()       Sets the current record's "request_id" value
 * @method EpP4mRequestParameters setParameterName()   Sets the current record's "parameter_name" value
 * @method EpP4mRequestParameters setParameterValue()  Sets the current record's "parameter_value" value
 * @method EpP4mRequestParameters setEpP4mRequest()    Sets the current record's "EpP4mRequest" value
 * 
 * @package    nisp
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseEpP4mRequestParameters extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('ep_p4m_request_parameters');
        $this->hasColumn('id', 'integer', 4, array(
             'type' => 'integer',
             'primary' => true,
             'autoincrement' => true,
             'length' => 4,
             ));
        $this->hasColumn('request_id', 'integer', null, array(
             'type' => 'integer',
             'notnull' => true,
             ));
        $this->hasColumn('parameter_name', 'string', 30, array(
             'type' => 'string',
             'notnull' => true,
             'length' => 30,
             ));
        $this->hasColumn('parameter_value', 'string', 30, array(
             'type' => 'string',
             'notnull' => true,
             'length' => 30,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('EpP4mRequest', array(
             'local' => 'request_id',
             'foreign' => 'id'));
    }
}