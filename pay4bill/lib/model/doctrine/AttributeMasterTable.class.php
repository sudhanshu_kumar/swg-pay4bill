<?php

class AttributeMasterTable extends Doctrine_Table {

    public static function getInstance() {
        return Doctrine_Core::getTable('AttributeMaster');
    }

    /*
     * @Saurabh on 11 June 2012
     * To get the attribute against a Merchant
     * with Object type 'Customer'
     * and attribute is active
     */

    public function getAttributeNameByObjectId($merchantId, $object_type) {
        $q = Doctrine_Query::create()
                        ->from('AttributeMaster am');

        $q->andWhere("am.merchant_id = $merchantId");
        $q->andWhere("am.object_type_id =  $object_type"); // In case of object type customer
        $q->andWhere("am.is_active =  '1'");
        $q->orderBy('is_unique asc'); // unique column will come first

        $users = $q->fetchArray();
        $arr = array();
        foreach ($users as $k) {
            $arr[$k['att_name']] = $k['att_label'];
        }
        return $arr;
    }

    /*
     * @Saurabh on 13 June 2012
     * To get the attribute against a Merchant
     * with Object type 'Customer'
     * and attribute is active
     */

    public function getAllAttributeDetails($merchantId, $object_type, $objId=0) {
        $q = Doctrine_Query::create()
                        ->from('AttributeMaster am');
        
        $q->andWhere("am.merchant_id = $merchantId");
        $q->andWhere("am.object_type_id =  $object_type"); // In case of object type customer
        $q->andWhere("am.is_active =  '1'");
        
        $q->leftJoin('am.AttributeValue av');
        if ($objId) {
        
            $q->andWhere("av.object_id = $objId");
        }
        $q->orderBy('is_unique asc'); // unique column will come first
        return $q->execute(array(), Doctrine::HYDRATE_ARRAY);
    }

     /*
     * @Saurabh on 20 June 2012
     * To get the attribute with value
     */
    public function getAllAttributeDetailsWithValue($merchantId, $object_type, $object_id){

        $q = Doctrine_Query::create()
        ->select ('av.att_value as att_value, am.*')
              ->from('AttributeMaster am')
              ->innerJoin('am.AttributeValue av')
               ->where ('object_id = ?',$object_id);


         $q->andWhere("am.merchant_id = $merchantId");
         $q->andWhere("am.object_type_id =  $object_type"); // In case of object type customer
         $q->andWhere("am.is_active =  '1'");
         $q->orderBy('is_unique asc'); // unique column will come first
         return $q->execute(array(), Doctrine::HYDRATE_ARRAY);
    }

    /*
     * @Saurabh on 11 June 2012
     * Get the datatype and mandatory value of a field
     * which should be active
     */

    public function getAttributeType($merchantId, $object_name, $object_type) {

        $q = Doctrine_Query::create()
                        ->select('am.att_type, am.is_mandatory, am.id')
                        ->from('AttributeMaster am');

        $q->andWhere('am.merchant_id = ?', $merchantId);
        $q->andWhere("replace(am.att_label,' ','') =  ?", $object_name);
        $q->andWhere("am.is_active =  '1'");
        $q->andWhere("am.object_type_id =  '" . $object_type . "'");// In case of object type customer
        $q->orderBy('is_unique asc'); // unique column will come first

        return $q->execute(array(), Doctrine::HYDRATE_ARRAY);
    }

    /*
     * @Saurabh on 12 June 2012
     * Return the unique field against a merchant and object_type
     * 
     */

    public function getUniqueAttribute($merchantId, $object_type) {

        $q = Doctrine_Query::create()
                        ->from('AttributeMaster am');

        $q->andWhere('am.merchant_id = ?', $merchantId);
        $q->andWhere("am.object_type_id =  $object_type");
        $q->andWhere("am.is_active =  '1'");
        $q->andWhere("am.is_unique =  '1'");

        return $q->execute(array(), Doctrine::HYDRATE_ARRAY);
    }

    /*
     * @Saurabh on 12 June 2012
     * Check if data for the unique attribute id is already exists in
     * the database against master attribute id
     */

    public function isUniqueDataAlreadyExist($attribute_master_id, $value) {

        $q = Doctrine_Query::create()
                        ->from('AttributeValue av');

        $q->andWhere('av.att_master_id = ?', $attribute_master_id);
        $q->andWhere("av.att_value =  '" . $value . "'");
        return $q->execute()->count();
    }

}