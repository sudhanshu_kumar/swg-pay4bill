<?php


class SupportUserProfileTable extends Doctrine_Table
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('SupportUserProfile');
    }
    public function getSupportUserList($username ='', $email_address='',$offset=0, $limit=0) {
        $q = Doctrine_Query::create()->from('SupportUserProfile t')->leftJoin('t.sfGuardUser s');
       
        if(isset($username) && $username <> ''){
            $q->andWhere("s.username like '" . $username . "%'");            
        }
        if(isset($email_address) && $email_address <> '') {
            $q->andWhere("s.email_address like '" . $email_address . "%'");
        }    
        if($offset) $q->offset($offset);
        if($limit)  $q->limit($limit);
        return $q->execute();
    }
     public function getSupportUserInfoById($userId='') {
        if ($userId != '') {
            $q = Doctrine_Query::create()
                            ->from('SupportUserProfile su');
            $q->andWhere('su.user_id = ?', $userId);
            return $q->execute(array(), Doctrine::HYDRATE_ARRAY);
        } else {
            return false;
        }
    }

}