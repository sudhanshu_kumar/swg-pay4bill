<?php

class InvoiceItemDetailsTable extends Doctrine_Table {

    public static function getInstance() {
        return Doctrine_Core::getTable('InvoiceItemDetails');
    }

    public function updateOnRecurringBillGeneration($old_invoice_id, $new_invoice_id) {
        $query = Doctrine_Query::create()->update('InvoiceItemDetails i')->set('i.invoice_id', '?', $new_invoice_id)
                        ->where('i.invoice_id= ?', $old_invoice_id);
        $query->execute();
    }

    public function getItemDetailsForInvoices($array_of_invoices) {

        $query = Doctrine_Query::create()->from('InvoiceItemDetails i');
        $query->whereIn('invoice_id', $array_of_invoices)
                ->addSelect('sum(i.subtotal) as partial_sum,sum(i.tax_total) as tax,invoice_id')
                ->groupBy("invoice_id");
        return $query->execute();
    }
    // Varun : get list of invoice item details for specific invoice number.
    public function getByInvoiceId($id) {
        $q = Doctrine_Query::create()
        ->from('InvoiceItemDetails i');
        $q->addWhere('i.invoice_id = ?', $id);
        $q->orderBy('i.id');

        return $q->execute();
    }
    /*
     * Varun : Function to set values for new created invoice
     */
    public function saveDetails($ob,$itemTax,$itemQuantity,$itemDescription,$itemUnitPrice,$amountDue)
    {   
            $invoiceItem = new InvoiceItemDetails();
            $invoiceItem->setInvoice($ob);
            if ($itemTax != null) {
                $invoiceItem->setItemTax($itemTax);
            } else {
                $invoiceItem->setItemTax('0.00');
            }
            if ($itemQuantity != null) {
                $invoiceItem->setNoOfItems($itemQuantity);
            }
            if ($itemDescription != null) {
                $invoiceItem->setItemDescription($itemDescription);
            }
            if ($itemUnitPrice != null) {
                $invoiceItem->setItemPrice($itemUnitPrice);
            }

            $invoiceItem->setSubTotal(round($invoiceItem->getItemPrice() * $invoiceItem->getNoOfItems(), 2));
            $invoiceItem->setTaxTotal(round(($invoiceItem->getSubTotal() * $invoiceItem->getItemTax() / 100), 2));
            
            if($amountDue!=null)
            {
                $amountDue =$invoiceItem->getSubTotal() + $invoiceItem->getTaxTotal() + $amountDue;
            } else {
                $amountDue = $invoiceItem->getSubTotal() + $invoiceItem->getTaxTotal();
            }
            $invoiceItem->save();
            return $amountDue;
    }

}