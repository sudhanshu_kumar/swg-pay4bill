<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EpAuditEvent
 *
 * @author saurabhk
 */
class EpAuditEvent {

    //category
    public static $CATEGORY_SECURITY = 'security';
    public static $CATEGORY_TRANSACTION = 'transaction';
    // subcategory for CATEGORY_SECURITY
    public static $SUBCATEGORY_SECURITY_INCORRECT_USERNAME = 'Incorrect Username';
    public static $SUBCATEGORY_SECURITY_LOGIN = 'Login';
    public static $SUBCATEGORY_SECURITY_LOGOUT = 'Logout';
    public static $SUBCATEGORY_SECURITY_FORGOTPASS_AND_SEC_ANSWER = 'Incorrect Credentials';
    public static $SUBCATEGORY_SECURITY_PASSWORD_CHANGE = 'Password Change';
    public static $SUBCATEGORY_SECURITY_RESET_PASSWORD = 'Password Reset';
    // These are attribute names
    public static $ATTR_USERINFO = 'UserDetail';
    public static $ATTR_TELLERINFO = 'TellerDetail';
    public static $MSG_SUBCATEGORY_TELLER_PASSWORD_RESET = "Teller password has been reset successfully by support user";
    public static $MSG_SUBCATEGORY_SECURITY_RESET_PASSWORD_FORCEFULLY = 'Teller password has been changed forcefully by teller user';

    public static function getFomattedMessage($message, $replaceVars) {
        $formattedMessage = $message;
        foreach ($replaceVars as $key => $value) {
            $formattedMessage = str_replace('{' . $key . '}', $value, $formattedMessage);
        }
        return $formattedMessage;
    }
}
?>
