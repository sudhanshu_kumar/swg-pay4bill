<?php
/**
 * Project Template formating helper.
 * You may wonder why the helpers are named according to the underscore syntax rather than the camelCase convention,
 * used everywhere else in symfony. This is because helpers are functions, and all the core PHP functions use the
 * underscore syntax convention. *
 * @package    template objects Formating helpers
 * @path       /lib/Widget/templateHelper.php
 */

/**
 *
 * @param <string> $content
 * @param <array> $attr
 * @param <boolen> $empty
 */
function ePortal_legend($content,array $attr= array(), $empty =false) {
  $html ='';
  $attributes ='';
  $class= 'legend';
  if(empty($content) && !$empty) {return;}
  if(!empty($attr)){
    foreach($attr as $k=>$v){
      if($k=='class'){$class .=' '.$v ;continue;}
      $attributes .="{$k}='{$v}' ";
    }
  }
  $attributes .='class=" '.$class.'"';
  $html .= "<legend {$attributes}> {$content}</legend>";
  return $html;
}
/**
 *
 * @param <type> $content
 * @param <type> $attr
 * @param <type> $empty
 * @return <type> page heading in admin
 */
function ePortal_pagehead($content,array $attr= array(), $empty =false) {
  $html ='';
  $attributes ='';
  $class= '';
  //if(empty($content) && !$empty) {return;}
  if(!empty($attr)){
    foreach($attr as $k=>$v){
      if($k=='class'){$class .=' '.$v ;continue;}
      $attributes .="{$k}='{$v}' ";
    }
  }
  
  $attributes .=' class="'.$class.'"' ;
  
  //$html .= "<div id='pageHead'><h2 {$attributes}>{$content}</h2></div><br />";
  //echo "<pre>"; print_r($attr); echo "INR-".key_exists('showFlash',$attr)."-".str($attr['showFlash'], 'false');die;
  if(key_exists('showFlash',$attr) && (strcasecmp($attr['showFlash'], 'false') == 0) ) {
    return $html;
  }
  $sfU =sfContext::getInstance()->getUser();
  if ($sfU->hasFlash('notice')){
    $html.='<div id="flash_notice" class="msg_serverside"><span style="margin:0px;">';
    $html .= nl2br($sfU->getFlash('notice'));
    $html .='</span></div><br />';
  }
  if ($sfU->hasFlash('error')){
    $html .= '<div id="flash_error" class="error_serverside">
          <span>'.nl2br($sfU->getFlash('error')).
          '</span>
        </div><br />';
  }
  return $html;
}

/**
 * Display hiligheted blocks
 * @param <type> $heading
 * @param <type> $content
 * @param <type> $attr
 * @param <type> $empty
 * @return <type>
 */
function ePortal_highlight($content, $heading='', array $attr= array(), $empty=false) {
  $html ='';
  $attributes ='';
  $class= 'highlight';
  if(empty($content) && $empty) {return;}
  if(!empty($attr)){
    foreach($attr as $k=>$v){
      if($k=='class'){$class .=' '.$v ;continue;}
      $attributes .="{$k}='{$v}' ";
    }
  }
  $attributes .='class="'.$class.'"' ;
  $html .= "<div {$attributes}>";
  $html .= (!empty($heading))?"<h3>{$heading}</h3>":'' ;
  $html .= "<p>{$content}</p></div>";

  return $html;
}

/**
 *
 * @param <array> $thead  array of heading to be display
 * @param <array> $tbody  Recordset to be displayed
 * @param <array> $attr   settings required for the table
 * @return <type>
 * $attr['display']   <array>   ('1','5');
 * $attr['no_record'] <string>  No Records found.;
 * $attr['sno']       <boolean> true;
 */
function ePortal_tGrid(array $thead,$tbody, $attr=array()) {
  if(get_class($tbody) !=''){$tbody = $tbody->getRawValue();}
  $sno =true;
  $display = array();
  $no_record = 'No records found.';
  $empty = false;
  $sno_index = 1;
  $link = array();

  # overwrite default values with user defined values
  extract($attr,EXTR_OVERWRITE); //extract($attr, EXTR_PREFIX_SAME,'user');

  $rows = count($tbody);
  # if no record found and $empty attribute set to true return blank block;
  if(empty($rows) && $empty){return;}

  $r1 = ($rows)?$tbody[0]:0; # record #1
  if($r1 && empty($attr['display'])){
    $cols = count($r1);
    $display = array_combine(array_keys($r1),array_keys($r1));
    $headings = array_keys($r1);
  }else{
    $cols = count($thead);
    $headings = array_keys($thead);
  }
  //else{
  //    $cols = count($thead);
  //    $headings = array_keys($thead);
  //  }



  $html ="<table class='tGrid'> ";
  # add table headings
  $html .='<thead><tr>';
  if(isset($sno) && !empty($sno)) {
    $html .='<th style="width:35px;">S.No.</th>';
  }

  foreach($display as $k=>$v){
    if(in_array($k,$headings)){ //echo $v;

      $style='';
      $css = explode('|',$v);

      if(is_array($css)){ @list($align,$width) = $css ; }
      if(!empty($width) && $width!='auto'){$style .= "width: {$width};";}
      if(!empty($align) && $align!='auto'){$style .= "text-align: {$align};";}
      //$html .="<th style='{$style}'>".(@$thead[$k])." ({$k})</th>" ;
      $html .="<th style='{$style}'>".(@$thead[$k])." </th>" ;
    }
  }
  $html .='</tr></thead>' ;

  # add table body contents
  $html .='<tbody>';
  if(empty($rows)){
    $html .="<tr><td colspan='".(($sno)?count($display)+1:count($display))."'><center class='XY20'>{$no_record}</center></td></tr>" ;
  }else {

    //echo "<h1>".print_r($tbody)." <br>: ".print_r($display)."</h1>" ;
    foreach($tbody as $data ){

      $html .="<tr>";
      if(isset($sno) && !empty($sno)) {
        $html .="<td>".$sno_index++."</td>"  ;
      }

      foreach($display as $k=>$v){

        if(!empty($display) && in_array($k,$headings)){
          $style='';
          $css = explode('|',$v);
          if(is_array($css)){ @list($align,$width) = $css ; }
          //if(!empty($width) && $width!='auto'){$style .= "width: {$width};";}
          if(!empty($align) && $align!='auto'){$style .= "text-align: {$align};";}

          $html .="\r\n<td style='{$style}'>";
          //echo"<h1>".print_r($link)."</h1>" ;
          //print_r($link.":".$k."<br>");
          if(!empty($link) && array_key_exists($k,$link)){
            //echo "<h1>LINK</h1>";print_r($headings);
            //  $parse = array('%default%'=>$data[$k]);
            foreach($headings as $headKey){
              $parse["%{$headKey}%"] = @$data[$headKey];
            }
            //$context = strtr($link[$k]['context'], $parse);
            //$context = strtr(implode(';echo ',$link[$k]['context']), $parse);
            $context ='';
            foreach ($link[$k]['context'] as $linkP ){
              $context .= "\$html .= ' '.".strtr($linkP, $parse).";";
            }

            //print_r($context);echo "<br>";
            eval($context) ;
            //$html .=(!empty($link[$k]['text']))? link_to($link[$k]['text'],$link['path']):link_to($data[$k],$link[$k]['path']);
          }else{
            $html .= $data[$k];
          }
          $html .="</td>" ;

        }
      }
      $html .="</tr>" ;
    }
  }
  $html .='</tbody>';

  # add table footer
  $html .="<tfoot><tr><td colspan='".((!empty($display))?count($display)+(($sno)?1:0):count($thead))."'></td></tr></tfoot>";

  $html .= "\r\n</table>";

  return $html;
}
//Function for making http url to https url
//its,useful when you want to make forcefully your url with ssl(like http://example.com to https://example.com)
function secure_url_for($internal_uri) {
  $context = sfContext::getInstance();
  $request = $context->getRequest();
  $host = $request->getHost();
  $uri = url_for($internal_uri);
  $surl = sprintf('%s://%s%s', 'https', $host, $uri);
  return $surl;
}

/**
 * @param <type> $label
 * @param <type> $value
 * @param <type> $empty      if flase it
 * @param <type> $empty_text
 * @return <type>
 */
function ePortal_renderFormRow($label,$value,$empty=true,$empty_text=''){

  if(!$empty && empty($value)){
    if(empty($empty_text)){return;}
    $value=$empty_text;

  }
  $html = <<<EOF
<dl>
  <dt><label>{$label}</label></dt>
  <dd>{$value}</dd>
</dl>
EOF;

  return $html;
}

/**
 *
 * @param <type> $fName
 * @param <type> $mName
 * @param <type> $lName
 * @return <type> returns formatted full name as SURNAME FIRSTNAME LASTNAME
 */
function ePortal_displayName($title="", $fName="",$mName="",$lName=""){

  return ucwords (strtolower($title." ".$lName." ".$fName." ".$mName));
}

function ePortal_popup($title,$content) {
  $popBlock = <<<EOF
<div class='popWrapper'><div id='mainDiv' class='parentDisable'><table border='0' id='popup' width='400' height='400'><tr><td><div style='padding:50px;'><h2>{$title}</h2><p>{$content}</p><p><a href='#' onClick='return hide()'><b>[Close]</b></a></p></div></td></tr></table></div></div>
EOF;
  $html = <<<EOF
<style>

.pageWrapper {z-index:0}
.popWrapper {position:absolute;z-index:999;left:0px;top:0px;}
.parentDisable {
z-index:99;
width:100%;
height:100%;
display:block;
position:fixed;
top:0px;
left:0px;
background-color: #ccc;
color: #aaa;
opacity: .98;
filter: alpha(opacity=98);
}
#popup {
width:400px;
height:200px;
position:relative;
margin:0px auto;
margin-top:50px;
color: #000;
background-color: #fff;
}
</style>
<script>
showPop = false ;
function pop() {
showPop =true ;
  $('#mainDiv').css('display','block');
  return false
}
function hide() {
  $('#mainDiv').css('display','none');
  return false
}
$('body').prepend("{$popBlock}");
/*$('document').ready(function(){
  $('body').prepend("{$popBlock}");
 if(showPop){ pop();}
});*/
wHeight =$(document).height() ;
wWidth = $(document).width() ;
$('#mainDiv').css('height',wHeight).css('width',wWidth);

</script>
EOF;

  return $html;
}

function ePortal_visa_permanent_address($addressId='')
{
  $addressText='';
  if(isset($addressId) && $addressId!="")
  {
    $addressArray = Doctrine::getTable('VisaPermanentAddress')->getFullAddress($addressId);
    if(is_array($addressArray) && count($addressArray)>0)
    {
      if(is_array($addressArray) && count($addressArray)>0)
      {
        $addressArray =$addressArray['VisaPermanentAddress_0'];
        if(isset($addressArray['address_1']) && $addressArray['address_1']!="")
        $addressText .= $addressArray['address_1'].'&nbsp;';
        if(isset($addressArray['address_2']) && $addressArray['address_2']!="")
        $addressText .= ','.$addressArray['address_2'];
        if(isset($addressArray['city']) && $addressArray['city']!="")
        $addressText .= '<br> '.$addressArray['city'].'&nbsp;';
        if(isset($addressArray['postcode']) && $addressArray['postcode']!="")
        $addressText .= '-'.$addressArray['postcode'];
        if(isset($addressArray['state']) && $addressArray['state']!="")
        $addressText .= '<br> '.$addressArray['state'].'&nbsp;,';
        if(isset($addressArray['Country']['country_name']) && $addressArray['Country']['country_name']!="")
        $addressText .= ' '.$addressArray['Country']['country_name'];
      }
    }
  }
  return $addressText;
}

function ePortal_reentryvisa_permanent_address($addressId='')
{
  $addressText='';
  if(isset($addressId) && $addressId!="")
  {
    $addressArray = Doctrine::getTable('ReEntryVisaAddress')->getFullAddress($addressId);

    if(is_array($addressArray) && count($addressArray)>0)
    {
      $addressArray =$addressArray['ReEntryVisaAddress_0'];
      if(isset($addressArray['address_1']) && $addressArray['address_1']!="")
      $addressText .= $addressArray['address_1'].'&nbsp;';
      if(isset($addressArray['address_2']) && $addressArray['address_2']!="")
      $addressText .= ','.$addressArray['address_2'];
      if(isset($addressArray['city']) && $addressArray['city']!="")
      $addressText .= '<br> '.$addressArray['city'].'&nbsp;';
      if(isset($addressArray['postcode']) && $addressArray['postcode']!="")
      $addressText .= '-'.$addressArray['postcode'];
      if(isset($addressArray['state']) && $addressArray['state']!="")
      $addressText .= '<br> '.$addressArray['state'].'&nbsp;,';
      if(isset($addressArray['Country']['country_name']) && $addressArray['Country']['country_name']!="")
      $addressText .= ' '.$addressArray['Country']['country_name'];

    }
  }
  return $addressText;
}

function ePortal_ecowas_residential_address($addressId='')
{
  $addressText='';
  if(isset($addressId) && $addressId!="")
  {
    $addressArray = Doctrine::getTable('EcowasResidentialAddress')->getFullAddress($addressId);
    if(is_array($addressArray) && count($addressArray)>0)
    {
      $addressArray =$addressArray['EcowasResidentialAddress_0'];
      if(isset($addressArray['address_1']) && $addressArray['address_1']!="")
      $addressText .= $addressArray['address_1'].'&nbsp;';
      if(isset($addressArray['address_2']) && $addressArray['address_2']!="")
      $addressText .= ','.$addressArray['address_2'];
      if(isset($addressArray['city']) && $addressArray['city']!="")
      $addressText .= '<br> '.$addressArray['city'].'&nbsp;';
      if(isset($addressArray['LGA']['lga']) && $addressArray['LGA']['lga']!="")
      $addressText .= '<br> '.$addressArray['LGA']['lga'].'&nbsp;';
      if(isset($addressArray['district']) && $addressArray['district']!="")
      $addressText .= '<br> '.$addressArray['district'].'&nbsp;';
      if(isset($addressArray['postcode']) && $addressArray['postcode']!="")
      $addressText .= '-'.$addressArray['postcode'];
      if(isset($addressArray['State']['state_name']) && $addressArray['State']['state_name']!="")
      $addressText .= '<br> '.$addressArray['State']['state_name'].'&nbsp;,';
      if(isset($addressArray['Country']['country_name']) && $addressArray['Country']['country_name']!="")
      $addressText .= ' '.$addressArray['Country']['country_name'];
    }
  }
  return $addressText;
}

function ePortal_ecowas_kin_address($addressId='')
{
  $addressText='';
  if(isset($addressId) && $addressId!="")
  {
    $addressArray = Doctrine::getTable('EcowasKinAddress')->getFullAddress($addressId);

    if(is_array($addressArray) && count($addressArray)>0)
    {
      $addressArray =$addressArray['EcowasKinAddress_0'];
      if(isset($addressArray['address_1']) && $addressArray['address_1']!="")
      $addressText .= $addressArray['address_1'].'&nbsp;';
      if(isset($addressArray['address_2']) && $addressArray['address_2']!="")
      $addressText .= ','.$addressArray['address_2'];
      if(isset($addressArray['city']) && $addressArray['city']!="")
      $addressText .= '<br> '.$addressArray['city'].'&nbsp;';
      if(isset($addressArray['LGA']['lga']) && $addressArray['LGA']['lga']!="")
      $addressText .= '<br> '.$addressArray['LGA']['lga'].'&nbsp;';
      if(isset($addressArray['district']) && $addressArray['district']!="")
      $addressText .= '<br> '.$addressArray['district'].'&nbsp;';
      if(isset($addressArray['postcode']) && $addressArray['postcode']!="")
      $addressText .= '-'.$addressArray['postcode'];
      if(isset($addressArray['State']['state_name']) && $addressArray['State']['state_name']!="")
      $addressText .= '<br> '.$addressArray['State']['state_name'].'&nbsp;,';
      if(isset($addressArray['Country']['country_name']) && $addressArray['Country']['country_name']!="")
      $addressText .= ' '.$addressArray['Country']['country_name'];

    }
  }
  return $addressText;
}

function ePortal_passport_permanent_address($addressId='')
{
  $addressText='';
  if(isset($addressId) && $addressId!="")
  {
    $addressArray = Doctrine::getTable('PassportPermanentAddress')->getFullAddress($addressId);

    if(is_array($addressArray) && count($addressArray)>0)
    {
      $addressArray =$addressArray['PassportPermanentAddress_0'];
      if(isset($addressArray['address_1']) && $addressArray['address_1']!="")
      $addressText .= $addressArray['address_1'].'&nbsp;';
      if(isset($addressArray['address_2']) && $addressArray['address_2']!="")
      $addressText .= ','.$addressArray['address_2'];
      if(isset($addressArray['city']) && $addressArray['city']!="")
      $addressText .= '<br> '.$addressArray['city'].'&nbsp;';
      if(isset($addressArray['LGA']['lga']) && $addressArray['LGA']['lga']!="")
      $addressText .= '<br> '.$addressArray['LGA']['lga'].'&nbsp;';
      if(isset($addressArray['district']) && $addressArray['district']!="")
      $addressText .= '<br> '.$addressArray['district'].'&nbsp;';
      if(isset($addressArray['postcode']) && $addressArray['postcode']!="")
      $addressText .= '-'.$addressArray['postcode'];
      if(isset($addressArray['State']['state_name']) && $addressArray['State']['state_name']!="")
      $addressText .= '<br> '.$addressArray['State']['state_name'].'&nbsp;,';
      if(isset($addressArray['Country']['country_name']) && $addressArray['Country']['country_name']!="")
      $addressText .= ' '.$addressArray['Country']['country_name'];

    }
  }
  return $addressText;
}

function ePortal_passport_kin_address($addressId='')
{
  $addressText='';
  if(isset($addressId) && $addressId!="")
  {
    $addressArray = Doctrine::getTable('PassportKinAddress')->getFullAddress($addressId);

    if(is_array($addressArray) && count($addressArray)>0)
    {
      $addressArray =$addressArray['PassportKinAddress_0'];
      if(isset($addressArray['address_1']) && $addressArray['address_1']!="")
      $addressText .= $addressArray['address_1'].'&nbsp;';
      if(isset($addressArray['address_2']) && $addressArray['address_2']!="")
      $addressText .= ','.$addressArray['address_2'];
      if(isset($addressArray['city']) && $addressArray['city']!="")
      $addressText .= '<br> '.$addressArray['city'].'&nbsp;';
      if(isset($addressArray['LGA']['lga']) && $addressArray['LGA']['lga']!="")
      $addressText .= '<br> '.$addressArray['LGA']['lga'].'&nbsp;';
      if(isset($addressArray['district']) && $addressArray['district']!="")
      $addressText .= '<br> '.$addressArray['district'].'&nbsp;';
      if(isset($addressArray['postcode']) && $addressArray['postcode']!="")
      $addressText .= '-'.$addressArray['postcode'];
      if(isset($addressArray['State']['state_name']) && $addressArray['State']['state_name']!="")
      $addressText .= '<br> '.$addressArray['State']['state_name'].'&nbsp;,';
      if(isset($addressArray['Country']['country_name']) && $addressArray['Country']['country_name']!="")
      $addressText .= ' '.$addressArray['Country']['country_name'];

    }
  }
  return $addressText;
}

/**
* link_to with additionnal param : $app
*
* @param string name of the link, i.e. string to appear between the <a> tags
* @param string 'module/action' or '@rule' of the action
* @param array additional HTML compliant <a> tag parameters
* @param string name of the application
* @return string XHTML compliant <a href> tag
*/
function link_to_app($name = '', $internal_uri = '', $options = array(),$app = SF_APP){
  //  echo "<pre>";
  //  $context = sfContext::getInstance();
  //  $request = $context->getRequest();
  //  print_r(get_class_methods(sfConfig));
  //  print_r(sfConfig::getAll());
  //  echo "</pre>";
  //  die();
  $request = sfContext::getInstance()->getRequest();
  $root = $request->getRelativeUrlRoot();

  define('SF_ENVIRONMENT',sfConfig::get('sf_environment'));

  $environment=(SF_ENVIRONMENT=='prod') ? "" : "_".SF_ENVIRONMENT;
  $protocole=(sfContext::getInstance()->getRequest()->isSecure()) ? "https" : "http";
  $url=$protocole."://".$_SERVER["SERVER_NAME"].$root."/".strtolower($app).$environment. ".php/".$internal_uri;
  return link_to($name, $url, $options);
}
