<?php
    /**
     *@author:  Manu Datta
     *@param <type> $value
     *@purpose:    To upload validator
     */
class upload_validator
{
    static $valid=0;
    static $invalid=0;
    static $validArray=array();
    static $invalidArray=array();

    public function valid_record($content_arr){
        self::validate('VALID',$content_arr);
    }
    public function invalid_record($content_arr,$errorTxt){
        self::validate('INVALID',$content_arr, $errorTxt);
    }
    private function validate($status,$content_arr,$errorTxt=''){
        $content_arr_val=array_values($content_arr);
        switch($status){
            case 'VALID':
                self::$validArray[]=$content_arr_val;
                self::$valid++;
                break;
            case 'INVALID':
                $content_arr_val[]=$errorTxt;
                self::$invalidArray[]=$content_arr_val;
                self::$invalid++;
                break;
        }
    }
    public function getValues(){
        return array('validArray'=>self::$validArray,'invalidArray'=>self::$invalidArray,'validRows'=>self::$valid,'invalidRows'=>self::$invalid);
    }
}
?>
