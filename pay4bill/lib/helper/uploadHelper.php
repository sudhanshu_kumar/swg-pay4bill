<?php
class uploadTemplate{
    public function  upload($upload_file,$head) {
        $this->upload_file=$upload_file;
        $this->head=explode(',',$head);
    }
    /**
     *@author:  Manu Datta
     *@purpose:    To write uploaded files (valid and invalid)
     */
    public function process(){
        $this->uploaded_file_name = $this->upload_file['name'];
        $ext = substr($this->uploaded_file_name, strrpos($this->uploaded_file_name, '.') + 1);
        $this->ext=$ext;
        $pos=strpos($this->uploaded_file_name ,$ext);
        $result=substr($this->uploaded_file_name,0,$pos-1);
        $dateTime=date("dmY_H_i_s", time());
        $c = exec('file '.$this->upload_file['tmp_name']);
        $fileExt = '';        
        
        $upload_on = sfContext::getInstance()->getUser()->getAttribute('upload_on');
        if(strlen($upload_on)> 0 && $upload_on == 'invoice') // Set in Invoice action function: executeUpload 
            $allowedext = sfConfig::get('app_applicant_file_ext_for_invoice'); 
        else 
            $allowedext = sfConfig::get('app_applicant_file_ext_for_others');
        // removal of session variable which was set on invoice action file
        if(sfContext::getInstance()->getUser()->getAttribute('upload_on'))
        sfContext::getInstance()->getUser()->getAttributeHolder()->remove('upload_on');
        
        switch($ext){
            case 'csv':
                $data = stripos($c,'ASCII text');
                $this->validFile=$result.'_valid_'.$dateTime.'.csv';
                $this->invalidFile=$result.'_invalid_'.$dateTime.'.csv';
                $fileExt = 'csv';
                break;
            case 'xls':
                $data = pay4billHelper::checkXlsMimeType($c);
                $this->validFile=$result.'_valid_'.$dateTime.'.xls';
                $this->invalidFile=$result.'_invalid_'.$dateTime.'.xls';
                $fileExt = 'xls';
                break;
        }
        if(empty($this->uploaded_file_name) && empty($data)){
            throw new Exception("Please upload file of type ".$allowedext);
        }
        // comment the file name with special char name validation
//        if(!pay4billHelper::checkFileName($this->uploaded_file_name)){
//            throw new Exception("File Name should not contain Special Characters");
//        }
        $this->uploaded_file_tmpname = $this->upload_file['tmp_name'];
        if(!in_array($ext, explode("/",$allowedext))){ //the extension allowed is defined in app.yml
            throw new Exception("Please upload ".$allowedext." type file only.");
        }
        
        /* Added on 2 Nov 2012 
         * By Saurabh Kumar
         * For XLS to count the number of rows in xls sheet 
         * Fix: Bug #36064
         */
        if($fileExt == 'xls'){
            $excel = new Spreadsheet_Excel_Reader();
            $excel->read($this->uploaded_file_tmpname);
            $file_count = $excel->sheets[0]['numRows'];
        } else if($fileExt == 'csv'){        
            $file_count = count(file($this->uploaded_file_tmpname));
        }
        
        if($file_count>451) {
            throw new Exception("CSV length cannot be more than 450");
        }
        $columns=count($this->head);
        switch($ext){
            case 'csv':
                $content_arr_list = pay4billHelper::readCsv($this->uploaded_file_tmpname , $columns);
                break;
            case 'xls':
                $content_arr_list = pay4billHelper::readXls($this->uploaded_file_tmpname , $columns);
                break;
        }
        $content_head = $content_arr_list[0];

        if($this->head!==$content_head){
            throw new Exception("Pattern is not matched with uploaded CSV file");
        }
        $this->content_arr_list=$content_arr_list;
        return $this->content_arr_list;
    }
    /**
     *@author:  Manu Datta
     *@param <type> $validArray
     *@param <type> $invalidArray
     *@param <type> $path
     *@purpose:    To write file
     */
    public function writeFiles($validArray,$invalidArray,$path)
    {
        $validFile=$this->validFile;
        $invalidFile=$this->invalidFile;
        $ext=$this->ext;
        
        $fieldHeadingsvalid = $fieldHeadingsInvalid = $this->head;
        $fieldHeadingsInvalid[] = 'Reason';
        switch($ext){
            case 'csv':
                //valid
                if(count($validArray)>0){
                    $content_arr=array();
                    $content_arr[]=$this->head;
                    $content_arr=array_merge($content_arr, $validArray);
                    pay4billHelper::createCsv($path.'/'.$validFile, $content_arr);
                    $this->validFile = cryptString::encrypt($validFile) ;
                }
                //invalid
                if(count($invalidArray)>0){
                    $content_arr=array();
                    $content_arr[]=$fieldHeadingsInvalid;
                    $content_arr=array_merge($content_arr, $invalidArray);
                    pay4billHelper::createCsv($path.'/'.$invalidFile, $content_arr);
                    $this->invalidFile = cryptString::encrypt($invalidFile) ;
                }
                break;
            case 'xls':
                //valid
                if(count($validArray)>0){
                    $content_arr=array();
                    $content_arr[]=$this->head;
                    $content_arr=array_merge($content_arr, $validArray);
                    pay4billHelper::createCsv($path.'/'.$validFile, $content_arr);
                    $this->validFile = cryptString::encrypt($validFile) ;
                }
                //invalid
                if(count($invalidArray)>0){
                    $content_arr=array();
                    $content_arr[]=$fieldHeadingsInvalid;
                    $content_arr=array_merge($content_arr, $invalidArray);
                    pay4billHelper::createCsv($path.'/'.$invalidFile, $content_arr);
                    $this->invalidFile = cryptString::encrypt($invalidFile) ;
                }
                break;
        }
        return array($this->validFile,$this->invalidFile);
        
    }
}

?>
