<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
function calculatePenalty($penalty_array=array(),$sec_fee=0,$uploaded_month=null,$uploaded_year=null)
{

/* Formula
 * (Flat Rate + MPR+ Penalty Percentage * (SEC Fees Due) + Number of Days of
Default)/Divisor
 *
 */
  $penalty=0;
 // print_r($penalty_array);
  $flat_rate= isset($penalty_array['flat_rate'])?$penalty_array['flat_rate'] : 0;
  $monetary_policy_rate= isset($penalty_array['monetary_policy_rate'])?$penalty_array['monetary_policy_rate'] : 0;
  $penalty_percentage= isset($penalty_array['penalty_percentage'])?$penalty_array['penalty_percentage']/100 : 0;
  $divisor= isset($penalty_array['divisor'])?$penalty_array['divisor'] : 365;
  $penalty_enforcement_day= isset($penalty_array['penalty_enforcement_day'])?$penalty_array['penalty_enforcement_day'] : 16;

  $number_of_days= 0;

if($uploaded_month==null || $uploaded_year==null)
 {
      $penalty=0;
 }
else
 {
    $current_date=strtotime(date('Y-m-d'));

    $expected_date=strtotime (date('Y-m-d',mktime(0,0,0, $uploaded_month+1,$penalty_enforcement_day-1,$uploaded_year)));
    $number_of_days=($current_date-$expected_date)/ (60 * 60 * 24);
    if($number_of_days>0)
    {

        $penalty= ($flat_rate +$monetary_policy_rate+ ($penalty_percentage * $sec_fee ) + $number_of_days ) / $divisor;
    }



 }
 return  $penalty=sprintf("%0.".sfConfig::get('app_precision')."f",($penalty)?$penalty:0);


}
?>
