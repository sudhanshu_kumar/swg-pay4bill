<?php
class pay4meServices {
  public function test()
  {
    $browser= $this->getBrowser();

    $uri = "http://spark.swgindia.com/~vdeshmane/sec/web/index.php/fee_payment/notification";

    $xmlData = '<?xml version="1.0"?>
<payment-notification xmlns="http://www.pay4me.com/schema/pay4meorder/v1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.pay4me.com/schema/pay4meorder/v1 payformeorder.xsd">
 <merchant-service id="63">
   <item number="3">
     <transaction-number>3</transaction-number>
     <validation-number>9961211257</validation-number>
     <payment-information>
       <amount>205000</amount>
       <currency>naira</currency>
       <payment-date>2010-06-19T15:07:44</payment-date>
       <mode>bank</mode>
       <bank>
         <name>Intercontinental Bank</name>
         <branch>Intercontinental Bank Wuse II</branch>
       </bank>
       <status>
         <code>0</code>
         <description>Payment successfully done</description>
       </status>
     </payment-information>
   </item>
 </merchant-service>
</payment-notification>';

    $header = array();
    $merchantAuthString = "228520468:1609284751";
    $merchantAuth = base64_encode($merchantAuthString) ;
    $header['Authorization'] = "Basic $merchantAuth";
    $header['Content-Type'] =  "application/xml;charset=UTF-8";
    $header['Accept'] = "application/xml;charset=UTF-8";
    
    $browser->post($uri, $xmlData, $header);

    if($browser->getResponseCode()==200)
    {
      $xdoc = new DomDocument;

      $isLoaded = $xdoc->LoadXML($browser->getResponseText());

      if($isLoaded)
      {
          if($xdoc->getElementsByTagName('error')->length)
          {
            $redirectURL = 'fee_payment/fail?case=1';

            if($xdoc->getElementsByTagName('error-message')->length)
            {
                sfContext::getInstance()->getUser()->setFlash('error', $xdoc->getElementsByTagName('error-message')->item(0)->nodeValue);
            }
            else
            {
                sfContext::getInstance()->getUser()->setFlash('error', "Unexpected Error!");
            }
          }
          elseif($xdoc->getElementsByTagName('redirect-url')->length)
          {
            sfContext::getInstance()->getUser()->setFlash('error', "Unexpected Error!");
            $redirectURL = $xdoc->getElementsByTagName('redirect-url')->item(0)->nodeValue ;
          }
          else
          {
              $redirectURL = 'fee_payment/fail?case=2';
              sfContext::getInstance()->getUser()->setFlash('error', "Invalid response!");
          }
      }
      else
      {
          sfContext::getInstance()->getUser()->setFlash('error', $browser->getResponseText());
          $redirectURL = 'fee_payment/fail?case=3';
      }
    }
    elseif($browser->getResponseCode()==400)
    {
        sfContext::getInstance()->getUser()->setFlash('error', $browser->getResponseText());
        $redirectURL = 'fee_payment/fail?case=4';
    }
    else
    {
        $error = 'Error: receiving server response code- '.$browser->getResponseCode()."\n".$browser->getResponseText();
        sfContext::getInstance()->getUser()->setFlash('error', $error);
        $redirectURL = 'fee_payment/fail?case=5';
    }

    return $redirectURL;
  }
  
  function integratePay4Me($trans_no, $item, $serviceId, $params, $amount, $description, $fullName)
  {
      $obj = $this->buildPaymentObject($trans_no, $item, $serviceId, $params, $amount, $description, $fullName);      
      return $this->postXML($obj);
  }

  private function buildPaymentObject($trans_no, $item, $serviceId, $params, $amount, $description, $fullName)
  {
      
    $this->defaultXmlNs = "http://www.pay4me.com/schema/pay4meorder/v1";
    $xml_data = new gc_XmlBuilder();
    $xml_data->Push('order', array('xmlns' => $this->defaultXmlNs));
    $xml_data->Push('merchant-service',  array('id'=>$serviceId));
    $xml_data->Push('item', array('number' => $item));
    $xml_data->Element('transaction-number', $trans_no);
    $xml_data->Element('name', $fullName);
    $xml_data->Element('description', $description);
    $xml_data->Element('price', $amount);
    $xml_data->Element('currency', 'naira');
    $xml_data->Push('parameters');
    foreach($params as $key => $value)
    {
        $xml_data->Element('parameter', $value, array('name' => $key));
    }

    $xml_data->Pop('parameters');
    $xml_data->Pop('item');
    $xml_data->Pop('merchant-service');
    $xml_data->Pop('order');
    $xmlContant = $xml_data->GetXML();
    $this->createLogData($xmlContant, "InitiateRequest");

    return $xmlContant;
  }

  private function postXML($xmlData)
  {
    $logger = sfContext::getInstance()->getLogger();
    $browser = $this->getBrowser();

    $merchantCode = sfConfig::get('app_merchant_code');
    $merchantKey  = sfConfig::get('app_merchant_key');
    $merchantAuthString = $merchantCode .":" .$merchantKey;
    $logger->info("auth string: $merchantAuthString");
    $merchantAuth = base64_encode($merchantAuthString) ;

    $header['Authorization'] = "Basic $merchantAuth";
    $header['Content-Type'] =  "application/xml;charset=UTF-8";
    $header['Accept'] = "application/xml;charset=UTF-8";

    $uri = sfConfig::get('app_merchant_pay4me_url');

    $payProcessVersion = sfConfig::get('app_pay_process_version');

    //echo $uri; exit;
    
    $logger->info("Sending To:$uri");
    
    $browser->post($uri, $xmlData, $header);

    if($browser->getResponseCode()==200)
    {
      $xdoc = new DomDocument;

      $isLoaded = $xdoc->LoadXML($browser->getResponseText());      
      if($isLoaded)
      {
          if($xdoc->getElementsByTagName('error')->length)
          {
            $redirectURL = 'fee_payment/fail?case=6';

            if($xdoc->getElementsByTagName('error-message')->length)
            {
                sfContext::getInstance()->getUser()->setFlash('error', $xdoc->getElementsByTagName('error-message')->item(0)->nodeValue);
            }
            else
            {
                sfContext::getInstance()->getUser()->setFlash('error', "Unexpected Error!");
            }
          }
          elseif($xdoc->getElementsByTagName('redirect-url')->length)
          {
            $redirectURL = $xdoc->getElementsByTagName('redirect-url')->item(0)->nodeValue ;
            $logger->info("XML data from payforme as notification:".$browser->getResponseText());
          }
          else
          {
              $redirectURL = 'fee_payment/fail?case=7';
              sfContext::getInstance()->getUser()->setFlash('error', "Invalid response!");
          }
      }
      else
      {
          sfContext::getInstance()->getUser()->setFlash('error', $browser->getResponseText());
          $redirectURL = 'fee_payment/fail?case=8';
      }
    }
    elseif($browser->getResponseCode()==400)
    {
        sfContext::getInstance()->getUser()->setFlash('error', $browser->getResponseText());
        $redirectURL = 'fee_payment/fail?case=9';
    }
    else
    {
        $error = 'Error: receiving server response code- '.$browser->getResponseCode()."\n".$browser->getResponseText();
        $logger->err($error);
        sfContext::getInstance()->getUser()->setFlash('error', $error);
        $redirectURL = 'fee_payment/fail?case=10';
    }

    return $redirectURL;
  }

  public function getBrowser() {
  if(!@$this->browserInstance) {
    $this->browserInstance = new sfWebBrowser(array(), 'sfCurlAdapter',
      array('SSL_VERIFYPEER' => false, 'SSL_VERIFYHOST' => false));
  }
  return $this->browserInstance;
 }


    public function createLogData($xmlData,$nameFormate)
    {
        $path = $this->getLogPath();

        $file_name = $path."/".($nameFormate.'-'.date('Y_m_d_H:i:s')).".txt";
        $i=1;
        while(file_exists($file_name)) {
            $file_name = $path."/".($nameFormate.'-'.date('Y_m_d_H:i:s-')).$i.".txt";
            $i++;
        }
        @file_put_contents($file_name, $xmlData);
    }

    public function getLogPath()
    {
        $logPath = sfConfig::get('sf_web_dir').'/log/';
        $logPath = $logPath.'/'.date('Y-m-d');

        if(is_dir($logPath)=='')
        {
            $dir_path=$logPath."/";
            mkdir($dir_path,0777,true);
            chmod($dir_path, 0777);
        }
        return $logPath;
    }
}
?>