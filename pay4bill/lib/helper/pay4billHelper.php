<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of pay4billHelper
 *
 * @author manu
 */
class pay4billHelper {

    /**
     * @author:  Manu Datta
     * @param <type> $merchantId
     * @param <type> $merchantName
     * @purpose:   Get the Invoice number 
     */
    public static function getInvoiceNo($merchantId='', $merchantName='') {
        // change the function to stop the duplicate invoice genertaion on 17 May 2012
        $invoiceNo = NULL;
        if ($merchantId != '') {
            $strMaxInvNo = Doctrine::getTable('Invoice')->getMaxInvoiceNo($merchantId);
//            if (!empty($strMaxInvNo)) {
            if (isset($strMaxInvNo['0']['maxInvno']) && $strMaxInvNo['0']['maxInvno'] != '') {
                $lastInvoice = $strMaxInvNo['0']['maxInvno'];
                $invoiceNo1 = str_pad(substr($lastInvoice, 3) + 1, 7, "0", STR_PAD_LEFT);
                $a = substr($lastInvoice, 0, 3);
                $invoiceNo = strtoupper($a) . $invoiceNo1;
            } else {
                // Removing in between space - Saurabh
                $merchantName = str_replace(" ", "", $merchantName);
                $prefix = $merchantName;
                $a = substr($prefix, 0, 3);
                $invoiceNo = strtoupper($a) . '0000001';
            }
//            }
        }
        return $invoiceNo;
    }

    /**
     * @author:  Manu Datta
     * @param <type> $filename
     * @param <type> $content_arr
     * @purpose:   To create CSV
     */
    public static function createCsv($filename, $content_arr) {
        $fp = fopen($filename, 'w+');
        foreach ($content_arr as $row) {
            fputcsv($fp, $row);
        }
        fclose($fp);
    }

    /**
     * @author:  Manu Datta
     * @param <type> $filename
     * @param <type> $columns
     * @param <type> $sheet_number
     * @purpose:   To read XLS
     */
    public static function readXls($filename, $columns, $sheet_number=0) {
        $fp = new Spreadsheet_Excel_Reader();
        $fp->setOutputEncoding('CP1251');
        $fp->read($filename, 'r');
        $content_arr_list = array();
        for ($i = 1; $i <= $fp->sheets[$sheet_number]['numRows']; $i++) {
            $content_arr_list_temp = array_slice($fp->sheets[$sheet_number]['cells'][$i], 0, $columns, true);
            $content_arr_cell = array();
            for ($j = 1; $j <= $columns; $j++) {
                $content_arr_cell[] = @$content_arr_list_temp[$j];
            }
            $content_arr_list[] = $content_arr_cell;
        }
        return $content_arr_list;
    }

    /**
     * @author:  Manu Datta
     * @param <type> $filename
     * @param <type> $columns
     * @purpose:   To read CSV
     */
    public static function readCsv($filename, $columns) {
        $fp = fopen($filename, 'r');
        while ($content_arr = fgetcsv($fp)) {
            $content_arr_list[] = array_slice($content_arr, 0, $columns);
        }
        return $content_arr_list;
    }

    /**
     * @author:  Manu Datta
     * @param <type> $c
     * @purpose:   To check xls mime type
     */
    public static function checkXlsMimeType($c) {
        $data='';
        $arrFileType = sfConfig::get('app_file_type');
        for ($i = 0; $i < count($arrFileType); $i++) {
            $data = stripos($c, $arrFileType[$i]);
            if ($data != '') {
                break;
            }
        }
        return $data;
    }

    /**
     * @author:  Manu Datta
     * @param <type> $filename
     * @purpose:   To check file name
     */
    public static function checkFileName($filename) {
        return (preg_match('/^[a-z0-9-_.]+$/i', $filename));
    }

    /**
     * @author:  Manu Datta
     * @param <type> $filename
     * @purpose:   To downloads valid and invalid files
     */
    public static function downloads($upFileTitle=false) {
        if ($upFileTitle) {
            $filePath = addslashes(sfConfig::get('sf_upload_dir') . '/assets/' . $upFileTitle);
        }
        if ($upFileTitle) {
            if (file_exists($filePath)) {
                sfConfig::set('sf_web_debug', false);
                $fileinfo = pathinfo($filePath);
                // Adding the file to the Response object
                sfContext::getInstance()->getResponse()->setHttpHeader('Content-type', 'application/force-download');
                sfContext::getInstance()->getResponse()->setHttpHeader('Content-Disposition', "attachment; filename={$fileinfo['basename']}", TRUE);
                sfContext::getInstance()->getResponse()->sendHttpHeaders();
                sfContext::getInstance()->getResponse()->setContent(file_get_contents($filePath));
                return true;
            }
            return false;
        }
    }

    /**
     * @author: Aditi Malhotra

     * @purpose:   To create Customer Id for Customer
     */
    static function generateCustomerId($merchantId, $customerName) {
        $customerId = NULL;
        if ($merchantId != '' && $customerName != '') {
            $cpObj = Doctrine::getTable('CustomerProfile')->getMaxCustomerId($merchantId);
            if ($cpObj) {
                $lastCustomerId = $cpObj->getCustomerId();

                $key = str_pad((int) substr($lastCustomerId, -6) + 1, 6, "0", STR_PAD_LEFT);
            } else {
                $key = '000001';
            }
            $mpObj = Doctrine::getTable('MerchantProfile')->find($merchantId);
            if ($mpObj) {
                $merchantName = $mpObj->getMerchantName();
            } else {
                return NULL;
            }
            //There should not be any space in between the name - Saurabh
            $merchantName = str_replace(" ", "", $merchantName);
            $customerName = str_replace(" ", "", $customerName);

            $first3chactersOfMerchantName = substr($merchantName, 0, 3);
            $first3chactersOfCustomerName = substr($customerName, 0, 3);
            $customerId = $first3chactersOfMerchantName . $first3chactersOfCustomerName . $key;
            $found = false;
            while (!$found) {

                $customer_id_collection = Doctrine::getTable('CustomerProfile')->findBy('customer_id', $customerId);
                if ($customer_id_collection->count() == 0) {
                    $found = true;
                } else {
                    $key = str_pad((int) substr($lastCustomerId, -6) + 1, 6, "0", STR_PAD_LEFT);
                    $customerId = $first3chactersOfMerchantName . $first3chactersOfCustomerName . $key;
                }
            }
        }
        return $customerId;
    }

    /**
     * @author: Aditi Malhotra

     * @purpose:   To generate Pin Id for Customer
     */
    static function generatePin() {
        $pin = '';
        while (strlen($pin) != 8) {
            $asciiCode = rand(48, 90);
            if ($asciiCode < 58 || $asciiCode > 64) {
                $pin.=chr($asciiCode);
            }
        }

        return cryptString::encrypt($pin);
    }

    static function regexPatternforAttType($attType) {

        switch ($attType) {
            case "String":
                return "/^[a-zA-Z\s]+$/";
            case "Integer":
                return "/^[0-9]+$/";
            case "Alphanumeric":
                return "/^[a-zA-Z0-9\s]+$/";
        }
    }

    static function validationMsgforAttType($attType) {

        switch ($attType) {
            case "String":
                return "Please enter alphabets [A-Za-z] only";
            case "Integer":
                return "Please enter digits [0-9] only";
            case "Alphanumeric":
                return "Please enter [A-Za-z0-9] only";
        }
    }

    static function createArrayForDynamicAttrForm($dynamicAttrData, $object_id=0) {

        $array_to_return = array();
        foreach ($dynamicAttrData as $attMasterRecord) {
            $array_to_return[$attMasterRecord['att_name']] = array('validator' => array('is_mandatory' => $attMasterRecord['is_mandatory'],
                    'is_unique' => $attMasterRecord['is_unique'],
                    'att_type' => $attMasterRecord['att_type']), 'label' => $attMasterRecord['att_label']);
            if ($object_id != 0) {
                foreach ($attMasterRecord['AttributeValue'] as $attValObjs) {


                    if ($attValObjs['object_id'] == $object_id) {

                        $array_to_return[$attMasterRecord['att_name']]['val'] = $attValObjs['att_value'];
                    }
                }
            }
        }

        return $array_to_return;
    }

    /**
     * @author: Saurabh Kumar
     * To check the user security at the time of given username
     * on 12 July
     */
    public static function isUserNameIgnore($username, $arr_restricted_users) {
        return false;
        //In case to disable this function, simply -- return false
        if (in_array($username, $arr_restricted_users))
            return true; else
            return false;
    }

    /**
     * @author: Saurabh Kumar
     * To check the user security at the time of given password
     * on 12 July
     */
    public static function isPasswordInSecure($password) {
        return false;
        //In case to disable this function, simply -- return false
        //Password must contain atleast 1 alphabet, 1 specialchar and 1 number
        $regex = "/(?=^.{6,20}$)(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[\W_])^.*/";
        if (preg_match($regex, $password))
            return false; else
            return true;
    }

    /* @Saurabh on 13 July 2012 */

    public static function executeDownloadCustomerSample($merchant_id, $newDynamicArray) {

        function removeSpaceFromString(&$val, $key) {
            $val = str_replace(" ", "", $val);
        }

        $arrayHeadings1 = Doctrine::getTable('AttributeMaster')->getAttributeNameByObjectId($merchant_id, 1); // 1 : Customer

        array_walk(&$arrayHeadings1, 'removeSpaceFromString');

        /* Check for the dynamic atribute type - string, alphanumeric, numeric
         * created a new array
         */
        $sampleType = array();
        foreach ($arrayHeadings1 as $v) {
            $arrType = Doctrine::getTable('AttributeMaster')->getAttributeType($merchant_id, $v, 1); // 1 : Customer 
            $type = strtolower($arrType[0]['att_type']);
            switch ($type) {
                case 'string': $sampleType[] = 'Alphabets [A-Za-z] only';
                    break;
                case 'alphanumeric': $sampleType[] = 'Alphanumeric [A-Za-z0-9] only';
                    break;
                case 'integer': $sampleType[] = 'Digits [0-9] only';
                    break;
            }
        }

        $fileName = '';
        $fileName = 'cus_sample_csv_' . cryptString::encrypt($merchant_id) . '.csv';

        /* Creation of heading for CSV */
        $arrayHeadings2 = array('Name', 'Sex', 'Address', 'PhoneNumber', 'EmailAddress');
        $validate['arrayHeading'][0] = array_merge($arrayHeadings2, $arrayHeadings1);
        $newDynamicArray = $validate['arrayHeading'][0];
        /* End of Creation of heading for CSV */


        /* Creation of sample data */
        $sampleArr1 = array('SampleCustomerName', 'Male or Female', '15, Rockwell Street', '123456789', 'test@abc.com');
        $sampleArr2 = $sampleType;
        $sampleArr = array_merge($sampleArr1, $sampleArr2);
        /* End of Creation of sample data */

        $validate['arrayHeading'][] = $sampleArr;

        pay4billHelper::createCsv(sfConfig::get('sf_upload_dir') . '/assets/' . $fileName, $validate['arrayHeading']);
        return $fileName;
    }

    /* @Saurabh on 13 July 2012 */

    public static function executeDownloadInvoiceSample($merchant_id) {
        // lines are commented because there is no concept of dynamic attribute at the time of invoice
        // $arrayHeadings1 = Doctrine::getTable('AttributeMaster')->getAttributeNameByObjectId($merchant_id, 2); // 2 : Invoice
        $fileName = '';

        $fileName = 'invoice_sample_csv_' . cryptString::encrypt($merchant_id) . '.csv';

        // @Saurabh on 27 July 2012
        // Check if the merchant is online or biller
        // And based on that, add/remove fields from the CSV
        $data = Doctrine::getTable('MerchantProfile')->find($merchant_id);
        if ($data->getPaymentType() == 'o') {
            $validate['arrayHeading'][0] = array('CustomerId', 'IssueDate', 'DeliveryDate', 'DueDate', 'AdditionalInformation', 'CurrencyType', 'PartPaymentPercentage', 'RecurrentBilling', 'ItemTax', 'ItemQuantity1', 'ItemDescription1', 'ItemUnitPrice1', 'ItemQuantity2', 'ItemDescription2', 'ItemUnitPrice2', 'ItemQuantity3', 'ItemDescription3', 'ItemUnitPrice3', 'ItemQuantity4', 'ItemDescription4', 'ItemUnitPrice4');
            $validate['arrayHeading'][1] = array('CustomerId of the Customer', 'yyyy-mm-dd(Should not be a Past Date)', 'yyyy-mm-dd(Should be greater than or equal to Issue Date)', 'yyyy-mm-dd(Should be greater than or equal to Delivery Date)', 'Alphanumeric text', 'naira', '0.1 to 99.99 ', 'Either weekly, monthly, yearly, or custom|number of days', 'Tax Name', 'Between 1 to 999', 'Alphanumeric text', 'Maximum 99999999', 'Between 1 to 999', 'Alphanumeric text', 'Maximum 99999999', 'Between 1 to 999', 'Alphanumeric text', 'Maximum 99999999', 'Between 1 to 999', 'Alphanumeric text', 'Maximum 99999999');
        } else if ($data->getPaymentType() == 'b') {
            $validate['arrayHeading'][0] = array('CustomerId', 'IssueDate', 'DeliveryDate', 'DueDate', 'AdditionalInformation', 'CurrencyType', 'ItemTax', 'ItemQuantity1', 'ItemDescription1', 'ItemUnitPrice1', 'ItemQuantity2', 'ItemDescription2', 'ItemUnitPrice2', 'ItemQuantity3', 'ItemDescription3', 'ItemUnitPrice3', 'ItemQuantity4', 'ItemDescription4', 'ItemUnitPrice4');
            $validate['arrayHeading'][1] = array('CustomerId of the Customer', 'yyyy-mm-dd(Should not be a Past Date)', 'yyyy-mm-dd(Should be greater than or equal to Issue Date)', 'yyyy-mm-dd(Should be greater than or equal to Delivery Date)', 'Alphanumeric text', 'naira', 'Tax Name', 'Between 1 to 999', 'Alphanumeric text', 'Maximum 99999999', 'Between 1 to 999', 'Alphanumeric text', 'Maximum 99999999', 'Between 1 to 999', 'Alphanumeric text', 'Maximum 99999999', 'Between 1 to 999', 'Alphanumeric text', 'Maximum 99999999');
        }

//        $validate['arrayHeading'][0] = array('CustomerId', 'IssueDate', 'DeliveryDate', 'DueDate', 'AdditionalInformation', 'CurrencyType', 'PartPaymentPercentage', 'RecurrentBilling', 'ItemTax', 'ItemQuantity1', 'ItemDescription1', 'ItemUnitPrice1', 'ItemQuantity2', 'ItemDescription2', 'ItemUnitPrice2', 'ItemQuantity3', 'ItemDescription3', 'ItemUnitPrice3', 'ItemQuantity4', 'ItemDescription4', 'ItemUnitPrice4');
//        $validate['arrayHeading'][0] = array_merge($arrayHeadings2, $arrayHeadings1);

        pay4billHelper::createCsv(sfConfig::get('sf_upload_dir') . '/assets/' . $fileName, $validate['arrayHeading']);
        return $fileName;
    }

    /* @Saurabh on 16 July 2012 */
    
//    Whole function commented. File will not create on the fly: on 10 Aug 2012
//    public static function executeDownloadTellerSample() {
//
//        $fileName = '';
//        $fileName = 'teller_sample_csv.csv';
//
//        $arrayHeadings = array('Name', 'Sex', 'Address', 'PhoneNumber', 'EmailAddress', 'MerchantNameSeparatedByPipe', 'BankName', 'UserName', 'Password');
//        $validate[0] = $arrayHeadings;
//        $validate[1] = array('SampleName', 'Male or Female', 'lagos,Nigeria', '123456789', 'abc@abc.com', 'Merchant1|Merchant2|Merchant3', 'Abc Bank', 'bankteller.abc', 'atLeast6characters');
//
//        pay4billHelper::createCsv(sfConfig::get('sf_upload_dir') . '/assets/' . $fileName, $validate);
//        return $fileName;
//    }

    /**
     * @author:     Saurabh Kumar
     * @date:       16 July 2012
     * @purpose:    To trim array values
     * Aditi- Modified on 2 Aug 2012:- Changed for multidimansional arrays
     */
    public function trimArray($arr) {
        return $this->trimVal(&$arr);
    }

    /* Aditi- 2 Aug 2012 - Trimming multidimensional Arrays with recursive calls-- */

    private function trimVal(&$array) {


        foreach ($array as $key => &$val) {

            if (is_array($val)) {

                $this->trimVal($val); //// <--calling itself.
            } else {
                $val = trim($val);
            }
        }
        return $array;
    }

    /**
     * @author: Aditi
     * To check merchant Payment Type
     * on 23 July
     */
    public function checkMerchantPaymentType($merchantProfileId) {
        if (!is_null($merchantProfileId)) {
            return Doctrine::getTable('MerchantProfile')->find($merchantProfileId)->getPaymentType();
        } else {
            return 'o';
        }
    }

    public static function getGroupIdsArrayFromGroupNames($groupNames_array) {
        $resultObjs = Doctrine::getTable('sfGuardGroup')->getGrpIdsFromGroupNames($groupNames_array);
        $array_to_return = array();
        foreach ($resultObjs as $groupObj) {
            array_push($array_to_return, $groupObj->getId());
        }
        return $array_to_return;
    }

    // Check the user is teller or not
    public static function checkIfTellerUser($groupsArray) {
        foreach ($groupsArray as $record => $recordDetails)
            if ($recordDetails['name'] == 'teller') {
                return true;
            }
        return false;
    }

    // Check the user is admin
    public static function checkIfAdminUser($groupsArray) {
        foreach ($groupsArray as $record => $recordDetails)
            if ($recordDetails['name'] == 'administrator') {
                return true;
            }
        return false;
    }

    public static function checkIfMerchantUser($groupsArray) {
        foreach ($groupsArray as $record => $recordDetails)
            if ($recordDetails['name'] == 'merchant' && $_SESSION['logedin_user_group'] == 'merchant') {
                return true;
            }
        return false;
    }

    public static function checkIfCustomerUser($groupsArray) {
        foreach ($groupsArray as $record => $recordDetails)
            if ($recordDetails['name'] == 'customer' && $_SESSION['logedin_user_group'] == 'customer') {
                return true;
            }
        return false;
    }

    public static function checkIfSubAdminReportUser($groupsArray) {
        foreach ($groupsArray as $record => $recordDetails)
            if ($recordDetails['name'] == 'report') {
                return true;
            }
        return false;
    }

    public static function checkIfSubAdminMerchantReportUser($groupsArray) {
        foreach ($groupsArray as $record => $recordDetails)
            if ($recordDetails['name'] == 'merchant_report') {
                return true;
            }
        return false;
    }
    public static function checkIfSupportUser($groupsArray) {
        foreach ($groupsArray as $record => $recordDetails)
            if ($recordDetails['name'] == 'support' && $_SESSION['logedin_user_group'] == 'support') {
                return true;
            }
        return false;
    }

    public static function getCustomerProfilesForUserObj($sfGuardUserObj) {
        $id='';
        $varObjUserCustomer = $sfGuardUserObj->getUserCustomerMapping();
            $totalRecord = count($varObjUserCustomer);
            foreach ($varObjUserCustomer as $k => $ob) {
                $id .= $ob['customer_profile_id'] . '';
                if ($k < ($totalRecord - 1)) {
                    $id .= ',';
                }
            }
            return $id;
    }

}

?>
