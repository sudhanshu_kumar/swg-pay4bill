<?php

/**
 * Description of SEC
 * @author ahskehar
 */
class SecHelper {

    /**
     * function getCmoDetails()
     * Purpose : To get CMO details
     * @param :$IntCmoAppId
     * @return : Resultset
     * Last Changed : 02/06/11
     * Contributor : ashekhar
     */
    public static function getCmoDetails($intCmoId='', $arrStatus='', $searchString = '') {
        try {
            $strQuery = Doctrine_Query::create()
                            ->select('c.*,cp.firstname,cp.cmo_reg_type,cp.si_reg_type,cp.cmo_id,s.id,s.application_status,ot.name')
                            ->from('TblCmo c')
                            ->leftJoin('c.TblCmoFs2PersonalInfo cp')
                            ->leftJoin('cp.TblOperatorType1 ot')
                            ->leftJoin('c.TblCmoStatus s');
            if (!empty($arrStatus)) {
                $strQuery->whereIn("c.status", $arrStatus);
            }
            if (!empty($intCmoId)) {
                $strQuery->andwhere("c.id=?", $intCmoId);
            }
            if (!empty($searchString)) {
                $strQuery->andwhere("c.cmo_app_id=?", $searchString);
            }
            return $strQuery->execute();
            //return $strQuery->toArray(true);
        } catch (Exception $e) {
            echo 'problem found' . $e->getMessage() . "\n";
            die;
        }
    }

    public static function getCmoDetailsQuery($application_status=array(), $start_date='', $end_date='', $application_id = '') {
        try {
            $strQuery = Doctrine_Query::create()
                            ->select('c.*,cp.firstname,cp.cmo_reg_type,cp.si_reg_type,cp.cmo_id,s.id,s.application_status,ot.name')
                            ->from('TblCmo c')
                            ->leftJoin('c.TblCmoFs2PersonalInfo cp')
                            ->leftJoin('cp.TblOperatorType1 ot')
                            ->leftJoin('c.TblCmoStatus s')
                            ->where("c.payment_status=?", "Paid")
                            ->orderBy('c.paid_date DESC');
            if (!empty($application_id)) {
                $strQuery->andwhere("c.cmo_app_id =?", $application_id);
            }
            if (!empty($application_status)) {
                $strQuery->whereIn("c.status", $application_status);
            }

            if ($start_date != '') {
                // $strQuery->andwhere("date(paid_date) between'".$start_date."' AND '".$end_date."'");
                $strQuery->andwhere("date(paid_date)>='" . $start_date . "'");
            }
            if ($end_date != '') {
                // $strQuery->andwhere("date(paid_date) between'".$start_date."' AND '".$end_date."'");
                $strQuery->andwhere("date(paid_date)<='" . $end_date . "'");
            }
            return $strQuery;
        } catch (Exception $e) {
            echo 'problem found' . $e->getMessage() . "\n";
            die;
        }
    }

    /*
     * For Date format
     * Dinesh Kumar
     */

    public static function dateFormat($date) {
        return date('d-m-Y', strtotime($date));
    }

    public static function secRegNumber($id, $type) {

        switch ($type) {
            case '1' :                  // Corporate
                $number = '1904' + $id;
                $preFixText = 'SEC00';
                $length = '9';
                break;
            case '2' :      // Individual
                $number = '3000' + $id;
                $preFixText = 'SI00';
                $length = '8';
                break;
            case '3' :              // Withdraw Application Id Corporate
                $number = $id;
                $preFixText = 'CRAPPID00000';
                $length = '12';
                break;
            case '4' :              // Withdraw Application Id Individual
                $number = $id;
                $preFixText = 'CRAPPID00000';
                $length = '12';
                break;
        }

        $regLength = strlen($preFixText . $number);
        if ($regLength > $length) {
            $trimLetter = $regLength - $length;
            $preFixText = substr($preFixText, 0, -$trimLetter);
        }
        return $preFixText . $number;
    }

    /**
     * Function For encyprition  and deycription
     * Dinesh Singh
     */
    static function encrypt($string) {
        $Len_Simbolos = strlen(sfConfig::get('app_inscrypt_key'));
        if ($Len_Simbolos < 26) {
            echo 'ERROR';
            die;
        }
        $Len_Str_Message = strlen($string);
        $Str_Encoded_Message = "";
        for ($Position = 0; $Position < $Len_Str_Message; $Position++) {
            $Byte_To_Be_Encoded = substr($string, $Position, 1);
            $Ascii_Num_Byte_To_Encode = ord($Byte_To_Be_Encoded);
            $Rest_Modulo_Simbolos = ($Ascii_Num_Byte_To_Encode + $Len_Simbolos) % $Len_Simbolos;
            $Plus_Modulo_Simbolos = (int) ($Ascii_Num_Byte_To_Encode / $Len_Simbolos);
            $Encoded_Byte = substr(sfConfig::get('app_inscrypt_key'), $Rest_Modulo_Simbolos, 1);
            if ($Plus_Modulo_Simbolos == 0) {
                $Str_Encoded_Message .= $Encoded_Byte;
            } else {
                $Str_Encoded_Message .= $Plus_Modulo_Simbolos . $Encoded_Byte;
            }
        }
        return base64_encode($Str_Encoded_Message);
    }

    static function decrypt($string) {
        $Len_Simbolos = strlen(sfConfig::get('app_inscrypt_key'));
        if ($Len_Simbolos < 26) {
            echo 'ERROR';
            die;
        }
        $string = base64_decode($string);
        $Len_Str_Message = strlen($string);
        $Str_Decoded_Message = "";
        for ($Position = 0; $Position < $Len_Str_Message; $Position++) {
            $Plus_Modulo_Simbolos = 0;
            $Byte_To_Be_Decoded = substr($string, $Position, 1);
            if ($Byte_To_Be_Decoded > 0) {
                $Plus_Modulo_Simbolos = $Byte_To_Be_Decoded;
                $Position++;
                $Byte_To_Be_Decoded = substr($string, $Position, 1);
            }
            //finding the position in the string
            // $SIMBOLOS
            $Byte_Decoded = 0;
            for ($SecondPosition = 0; $SecondPosition < $Len_Simbolos; $SecondPosition++) {
                $Byte_To_Be_Compared = substr(sfConfig::get('app_inscrypt_key'), $SecondPosition, 1);
                if ($Byte_To_Be_Decoded == $Byte_To_Be_Compared) {
                    $Byte_Decoded = $SecondPosition;
                }
            }
            $Byte_Decoded = ($Plus_Modulo_Simbolos * $Len_Simbolos) + $Byte_Decoded;
            $Ascii_Num_Byte_To_Decode = chr($Byte_Decoded);
            $Str_Decoded_Message .= $Ascii_Num_Byte_To_Decode;
        }
        return $Str_Decoded_Message;
    }

    /**
     * Returns action for  searchCmoApplicants action page.
     * Created by: Ganesh Pasarkar
     * Created date:9th June 2011 10:00 AM
     * Modified by: Ganesh Pasarkar
     * Modified date: 9th June 2011 10:00 AM
     * Reason: UC 4.10
     */
    public static function searchCmoApplicants($intCmoAppId='', $application_status=array(), $start_date='', $end_date='') {
        try {
            $strQuery = Doctrine_Query::create()
                            ->select('c.*,cp.firstname,cp.cmo_reg_type,cp.si_reg_type,cp.cmo_id,s.id,s.application_status,s.processing_status,s.legal_status,ot.name')
                            ->from('TblCmo c')
                            ->leftJoin('c.TblCmoFs2PersonalInfo cp')
                            ->leftJoin('cp.TblOperatorType1 ot')
                            ->leftJoin('c.TblCmoStatus s')
                            ->where("c.payment_status=?", "Paid");
            if (!empty($intCmoAppId)) {
                $param = "c.cmo_app_id LIKE '%" . $intCmoAppId . "%'";
                $strQuery->andwhere($param);
            }
            if (!empty($application_status)) {
                $strQuery->whereIn("c.status", $application_status);
            }
            if ($start_date != '') {
                // $strQuery->andwhere("date(paid_date) between'".$start_date."' AND '".$end_date."'");
                $strQuery->andwhere("date(paid_date)>='" . $start_date . "'");
            }
            if ($end_date != '') {
                // $strQuery->andwhere("date(paid_date) between'".$start_date."' AND '".$end_date."'");
                $strQuery->andwhere("date(paid_date)<='" . $end_date . "'");
            }
            if ($start_date != '' && $end_date != '') {
                $strQuery->andwhere("date(paid_date) between'" . $start_date . "' AND '" . $end_date . "'");
            }
            return $strQuery;
        } catch (Exception $e) {
            echo 'problem found' . $e->getMessage() . "\n";
            die;
        }
    }

    /*
     * For generate Registration No
     * Manoj Kumar
     */

    public static function generateRegistrationNo($id = '') {
        $cmo_reg_number = '';
        $cmo_reg_number = Doctrine::getTable('TblCmo')->find($id)->getCmoRegNumber();
        $reg_no = '';
        if (!$cmo_reg_number) {
            $obj = Doctrine::getTable('TblCmo')
                            ->createQuery()
                            ->select('id,cmo_reg_number')
                            ->where('cmo_reg_number IS NOT NULL')->orderBy('cmo_reg_number desc')
                            ->execute();

            if ($obj) {
                $old_reg_no = $obj->getFirst()->getCmoRegNumber();
                $len = strlen($old_reg_no);
                $number = substr($old_reg_no, -($len - 3));
                $prefix_no = ($number + 1);
                $prefix_no_len = strlen($prefix_no);
                if ($prefix_no_len < 6)
                    $prefix_no = str_pad($prefix_no, 6, '0', STR_PAD_LEFT);
                $new_reg_no = 'SEC' . $prefix_no;
            }else {
                $new_reg_no = sfConfig::get('app_sec_reg_no');
            }
            return $new_reg_no;
        } else {
            return $cmo_reg_number;
        }
    }
    /**
     * Purpose : Generate unique application Id for Raise Change Request
     * @param <integer> $id
     * @return <string> return Application Id
     */
    public static function getRcrAppId($id = '') { 
        $appId = '';
        if(!empty ($id))
          $appId = Doctrine::getTable('TblCrAmendmentApplicationInfo')->find($id)->getApplicationId();
        
        $reg_no = '';
        if (!$appId) {
            $obj = Doctrine::getTable('TblCrAmendmentApplicationInfo')
                            ->createQuery()
                            ->select('id,application_id')
                            ->where('application_id IS NOT NULL')->orderBy('application_id desc')
                            ->execute();
            
            if (count($obj)) {
                $old_reg_no = $obj->getFirst()->getApplicationId();

                $len = strlen($old_reg_no);
                $number = substr($old_reg_no, -($len - 8));
                
                $prefix_no = ($number + 1);

                $prefix_no_len = strlen($prefix_no);
                
                if ($prefix_no_len < 6){
                    $prefix_no = str_pad($prefix_no, 6, '0', STR_PAD_LEFT);
                }
                    
                $new_app_id = ' CRAPPID' . $prefix_no;
            }else {
                $prefix_no = str_pad(1, 6, '0', STR_PAD_LEFT);
                $new_app_id = ' CRAPPID' . $prefix_no;
            }           
            return $new_app_id;

        } else {
            return $appId;
        }
    }
    /**
     * Purpose : Generate unique application Id for Raise Change Request
     * @param <integer> $id
     * @return <string> return Application Id
     */
    public static function getTraAppId($id = '') {
        $appId = '';
        if(!empty ($id))
          $appId = Doctrine::getTable('TblCrTransferSponsoredIndividuals')->find($id)->getApplicationId();

        $reg_no = '';
        if (!$appId) {
            $obj = Doctrine::getTable('TblCrTransferSponsoredIndividuals')
                            ->createQuery()
                            ->select('id,application_id')
                            ->where('application_id IS NOT NULL')->orderBy('application_id desc')
                            ->execute();

            if (count($obj)) {
                $old_reg_no = $obj->getFirst()->getApplicationId();

                $len = strlen($old_reg_no);
                $number = substr($old_reg_no, -($len - 8));

                $prefix_no = ($number + 1);

                $prefix_no_len = strlen($prefix_no);

                if ($prefix_no_len < 6){
                    $prefix_no = str_pad($prefix_no, 6, '0', STR_PAD_LEFT);
                }

                $new_app_id = ' TRAPPID' . $prefix_no;
            }else {
                $prefix_no = str_pad(1, 6, '0', STR_PAD_LEFT);
                $new_app_id = ' TRAPPID' . $prefix_no;
            }
            return $new_app_id;

        } else {
            return $appId;
        }
    }

    /*
     * For generate Identification No
     * Manoj Kumar
     */

    public static function generateIdentificationNo($id) {
        $id_number = '';
        $id_number = Doctrine::getTable('TblCmoFs2PersonalInfo')->createQuery()
                        ->select("max(id_no) as max_id_no")
                        ->fetchOne()
                        ->getMaxIdNo();
        $reg_no = '';

        if ($id_number) {
            $id_no = '';

            $old_id_no = $id_number;
            $len = strlen($old_id_no);
            $number = substr($old_id_no, -($len - 3));
            $prefix_no = ($number + 1);
            $prefix_no_len = strlen($prefix_no);
            if ($prefix_no_len < 5)
                $prefix_no = str_pad($prefix_no, 5, '0', STR_PAD_LEFT);
            $new_id_no = 'SI' . $prefix_no;
            return $new_id_no;
        }
        else {
            return sfConfig::get('app_sec_id_no');
            ;
        }
    }

    /**
     * function getCmoDetails()
     * Purpose : To get CMO Sponsored details
     * @param :$IntCmoAppId
     * @return : Resultset
     * Last Changed : 14/06/11
     * Contributor : Santosh
     */
    public static function getCmoSponsoredDetails($intCmoId='', $arrStatus='', $searchString = '') {
        try {
            $strQuery = Doctrine_Query::create()
                            ->select('c.*,cp.firstname,cp.cmo_reg_type,cp.si_reg_type,cp.cmo_id,s.id,s.application_status,ot.name, cs.*')
                            ->from('TblCmo c')
                            ->leftJoin('c.TblCmoFs2PersonalInfo cp')
                            ->leftJoin('cp.TblCmoSponsoredChecklist cs')
                            ->leftJoin('cp.TblOperatorType1 ot')
                            ->leftJoin('c.TblCmoStatus s');
            if (!empty($arrStatus)) {
                $strQuery->whereIn("c.status", $arrStatus);
            }
            if (!empty($intCmoId)) {
                $strQuery->andwhere("c.id=?", $intCmoId);
            }
            if (!empty($searchString)) {
                $strQuery->andwhere("c.cmo_app_id=?", $searchString);
            }
            return $strQuery->execute();
            //return $strQuery->toArray(true);
        } catch (Exception $e) {
            echo 'problem found' . $e->getMessage() . "\n";
            die;
        }
    }

    /**
     * function getCheckListDetails()
     * Purpose : To get Sponsored details
     * @param :$IntCmoAppId
     * @return : Resultset
     * Last Changed : 01/07/11
     * Contributor : Santosh
     */
    public static function getCheckListDetails($intCmoId) {
        try {
            $strQuery = Doctrine_Query::create()
                            ->select('cp.id, cp.firstname as firstname, cp.firstname as firstname,cp.cmo_reg_type,cp.si_reg_type,cp.cmo_id, cs.id, cs.previous_employer as previous_employer
                ,cs.police_clearance as police_clearance, cs.referees as referees, cs.bankers as bankers, cs.curriculum_vitae as curriculum_vitae,
                 cs.credentials as credentials, cs.practicing_fees as practicing_fees, cs.forms as forms,  cs.missing_gap_comment as missing_gap_comment')
                            // ->from('TblCmo c')
                            ->from('TblCmoFs2PersonalInfo cp')
                            ->leftJoin('cp.TblCmoSponsoredChecklist cs')
                            ->andwhere("cp.cmo_id=?", $intCmoId);

            return $strQuery->execute()->toArray(true);
            //return $strQuery->toArray(true);
        } catch (Exception $e) {
            echo 'problem found' . $e->getMessage() . "\n";
            die;
        }
    }

    /* Created By : Shagupta Faras       On: 4 July 2011
     *  Purpose : To return online top 10 trades details in array from the nigerian web side
     */

    public static function getTopTrades() {
        try {
            $result = '';

            $b = new sfWebBrowser(array(), 'sfCurlAdapter', array('cookies' => true));
            $b->post(sfConfig::get('app_trading_link'), array());
            if (!$b->responseIsError()) {
                // Successful response (eg. 200, 201, etc)
                $htmlText = $b->getResponseBody();
                $resultArray = SecHelper::parseHtmlContent($htmlText);
                return $resultArray;
            } else {
                return 'Found No Trading Result';
            }
        } catch (Exception $e) {
            echo 'problem found --' . $e->getMessage() . "\n";
            die;
            return 'Result not found';
        }
    }

    /* Created By : Shagupta Faras       On: 4 July 2011
     * purpose : To parse Html Content in to array & return required form
     */

    public static function parseHtmlContent($htmlText) {
        // $document should contain an HTML document.
        // This will remove HTML tags, javascript sections
        // and white space. It will also convert some
        // common HTML entities to their text equivalent.
        $resultArray = array();

        $search = array("'<script[^>]*?>.*?</script>'si", // Strip out javascript
            "'<[/!]*?[^<>]*?>'si", // Strip out HTML tags
            "'([rn])[s]+'", // Strip out white space
            "'&(quot|#34);'i", // Replace HTML entities
            "'&(amp|#38);'i",
            "'&(lt|#60);'i",
            "'&(gt|#62);'i",
            "'&(nbsp|#160);'i",
            "'&(iexcl|#161);'i",
            "'&(cent|#162);'i",
            "'&(pound|#163);'i",
            "'&(copy|#169);'i",
            "'&#(d+);'e");                    // evaluate as php

        $replace = array("",
            "",
            "\1",
            "\"",
            "&",
            "<",
            ">",
            " ",
            chr(161),
            chr(162),
            chr(163),
            chr(169),
            "chr(\1)");


        $parser = new htmlParser($htmlText);
        $ar = $parser->toArray();

        $ar = $ar[0]['childNodes'];
        //  echo "<pre>";
        //print_r($ar);

        $j = 0;
        for ($i = 0; $i < count($ar); $i++) {
            $father = trim($ar[$i]['father']);
            $tag = trim($ar[$i]['tag']);
            $content = trim($ar[$i]['innerHTML']);
            if (isset($ar[$i]['childNodes']))
                $childNode = $ar[$i]['childNodes'];
            else
                $childNode=array();


            $text = preg_replace($search, $replace, $content);

            if ($tag == 'center') { //echo $content;
                if ($text != '') {
                    $part = explode(' ', $text);
                    if (!empty($part)) {
                        $resultArray[$j]['symbol'] = $part[1];
                        $resultArray[$j]['volume'] = isset($part[2]) ? $part[2] : 0.00;
                        $resultArray[$j]['value'] = isset($part[3]) ? $part[3] : 0.00;
                        $j = $j + 1;
                    }
                }
            } else {
                if ($father == '-{}-6-0-{}-' && $tag == 'td' && $content != '' && !empty($childNode) && $text != '') {


                    $childTag = trim($childNode[0]['tag']);
                    $childevel = trim($childNode[0]['level']);

                    if ($childTag == 'font' && $childevel != 0) {
                        // echo $i."***".$father."**".$childNode."****";
                        //  echo $content;
                    } else {
                        if ($childTag == 'a') {

                            $resultArray[$j]['symbol'] = $text;
                        } else {
                            if (isset($resultArray[$j]['volume'])) {
                                $resultArray[$j]['value'] = $text;
                                $j++;
                            } else {
                                $resultArray[$j]['volume'] = $text;
                            }
                        }
                        //  echo $i."***".$father."**".$childNode."****";
                        // echo $content;
                    }
                }//if($father=='-{}-6-0-{}-' && $tag=='td' && $content!='' && !empty($childNode) && $text!='')
            }//elseif($tag=='center')
        }


        unset($resultArray[0]);

        return $resultArray;
    }
    /**
     * Purpose : set excel file header
     * @param <PHPExcel Object> $objPHPExcel
     * @param <array> $cellArr
     * @return <PHPExcel Object>
     */
    public static function setExcelHeader($objPHPExcel,$cellArr){ 
      $styleArray = array(
                    'font' => array(
                      'bold' => true,
                      'color' => array('argb' => '00000000'),
            )
        );
        
      foreach($cellArr as $cell){
        $objPHPExcel->getActiveSheet()->getStyle($cell)->applyFromArray($styleArray);
      }

      return $objPHPExcel;
    }

    /**
     * Purpose : set excel file cell witdh
     * @param <PHPExcel Object> $objPHPExcel
     * @param <array> $cellArr
     * @return <PHPExcel Object>
     */
    public static function setExcelCellWidth($objPHPExcel,$rowArr){
      
      foreach($rowArr as $row){
        $objPHPExcel->getActiveSheet()->getColumnDimension($row)->setAutoSize(true);
      }

      return $objPHPExcel;
    }

     /**
      *
      * Purpose:  Display payment type name at frontend
      * @param <type> $payment type
      * create: 28-07-2011
      * update: 28-07-2011
      */

    public function getPaymentType($paymentType)
    {
        $paymentTypeName = '';
        switch ($paymentType)
        {
            case 'cmo_reg':
                $paymentTypeName = 'Initiate Registration';
                break;
            case 'up_bill':
                $paymentTypeName = 'Uplaod Bills';
                break;
            case 'sec_fees':
                $paymentTypeName = 'SEC Fees';
                break;
            case 'cmo_renewal':
                $paymentTypeName = 'Renewal of Registration';
                break;
            case 'change_request':
                $paymentTypeName = 'Change Request';
                break;
            case 'new_si':
                break;
            case 'trans_si':
                break;
            case 'renew_crop':
                break;
            default :
                break;
        }
        return $paymentTypeName;
    }
    
    static public function calculate_day_difference($endDate, $beginDate)
    {
        $date_parts1=explode("-", $beginDate);
        $date_parts2=explode("-", $endDate);
        $start_date=gregoriantojd($date_parts1[1], $date_parts1[2], $date_parts1[0]);
        $end_date=gregoriantojd($date_parts2[1], $date_parts2[2], $date_parts2[0]);
        return $end_date - $start_date;
    }

    static public function getOperatorTypeArr($opr){
      $operator = Doctrine::getTable('TblOperatorType')->getOperator($opr);
      $operaArr = Array();     

      foreach($operator as $key=>$val){
        $operaArr[$val->getId()]=$val->getName();
      }
      return $operaArr;
    }
}


?>
