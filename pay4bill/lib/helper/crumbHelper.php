<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
require_once dirname(__FILE__).'/FormHelper.php';

function generateCrumb($module_name, $action_name)
{    
    $pattern = '/([A-Z]+)/';
    $replacement = ' ${1}';

    $module_name = str_replace('_', ' ', $module_name);

    $action_name = str_replace('_form', 'New', $action_name);

    if($module_name == 'individualcertification')
        $module_name = 'individualCertification';

    if($module_name == 'companycertification')
        $module_name = 'companyCertification';

    if($action_name == 'searchCompany Applicants')
        $action_name = 'searchCorporate Applicants';

    if($module_name == 'corporatecertification')
        $module_name = 'corporateCertification';

    if($module_name == 'companyverification')
        $module_name = 'companyVerification';

    if($module_name == 'corporateverification')
        $module_name = 'corporateVerification';

    if($module_name == 'individualverification')
        $module_name = 'individualVerification';

    return ucfirst(preg_replace($pattern, $replacement, $module_name)).' &raquo; '.ucfirst(preg_replace($pattern, $replacement, $action_name));
}

function returnLogoPath($host='')
{
   
   if(!$host)
   {
        $server = $_SERVER['HTTP_HOST'];
   }
   else
   {
        return "http://".$host."/web/images/img/logo-securityex.png";
   }

   if($server == 'spark')
        $server = 'spark.swgindia.com';
        
   $var_path = 'http://'.$server;

   $a = explode('web', $_SERVER['SCRIPT_NAME']);
   //return $var_login_path = $var_path.$a[0].'web/images/img/logo-securityex.png';

   $url = sfContext::getInstance()->getController()->genUrl('images/',true).'img/logo-securityex.png';
   $url = str_replace("frontend_dev.php/", "", $url);
   $url = str_replace("frontend.php/", "", $url);
   $url = str_replace("index.php/", "", $url);
   return $url;
}
?>
