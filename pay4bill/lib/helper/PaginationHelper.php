<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

function pager_navigation($pager, $uri)
{
  $navigation = '';

  if ($pager->haveToPaginate())
  {
    $uri .= (preg_match('/\?/', $uri) ? '&' : '?').'page=';

    // First and previous page
    if ($pager->getPage() != 1)
    {
      $navigation .= link_to(image_tag('/sf/sf_admin/images/first.png', 'align=absmiddle'), $uri.'1');
      $navigation .= link_to(image_tag('/sf/sf_admin/images/previous.png', 'align=absmiddle'), $uri.$pager->getPreviousPage()).' ';
    }

    // Pages one by one
    $links = array();
    foreach ($pager->getLinks() as $page)
    {
      $links[] = link_to_unless($page == $pager->getPage(), $page, $uri.$page);
    }
    $navigation .= join('  ', $links);

    // Next and last page
    if ($pager->getPage() != $pager->getLastPage())
    {
      $navigation .= ' '.link_to(image_tag('/sf/sf_admin/images/next.png', 'align=absmiddle'), $uri.$pager->getNextPage());
      $navigation .= link_to(image_tag('/sf/sf_admin/images/last.png', 'align=absmiddle'), $uri.$pager->getLastPage());
    }

  }

  return $navigation;
}

function ajax_pager_navigation($pager, $uri, $divId)
{
  $navigation = '';

  if ($pager->haveToPaginate())
  {
    $uri .= (preg_match('/\?/', $uri) ? '&' : '?').'page=';

    // First and previous page
    if ($pager->getPage() != 1)
    {
      $FirstUri = $uri . '1' ;
      $PreviousUri = $uri.$pager->getPreviousPage() ;
      $navigation .=  "<span style='cursor:pointer;color:blue;' onclick=\"ajax_paginator('$divId', '$FirstUri')\" ><img src='../sf/sf_admin/images/first.png' /></span>" ;
      $navigation .=  "<span style='cursor:pointer;color:blue;' onclick=\"ajax_paginator('$divId', '$PreviousUri')\" ><img src='../sf/sf_admin/images/previous.png' /></span>" ;
    }

    // Pages one by one
    $links = array();
    $currPage = $pager->getPage() ;
    foreach ($pager->getLinks() as $page)
    {
      $styleUnderscore = "" ;
      ($currPage == $page)? "" : $styleUnderscore = "text-decoration:underline" ; // For style underscore property
      $pagenoUri = $uri.$page ;
      $links[] =  "<span style='cursor:pointer;color:blue;$styleUnderscore' onclick=\"ajax_paginator('$divId', '$pagenoUri')\" >$page</span>" ;

     // link_to_unless($page == $pager->getPage(), $page, "#", array('onclick' => "ajax_paginator('$divId', '$pagenoUri')"));


     // $links[] = link_to_unless($page == $pager->getPage(), $page, $uri.$page);
    }


    $navigation .= join('  ', $links);

    // Next and last page
    if ($pager->getPage() != $pager->getLastPage())
    {
      $lastUri = $uri.$pager->getLastPage() ;
      $nextUri = $uri.$pager->getNextPage() ;
      //$navigation .= ' '.link_to(image_tag('/sf/sf_admin/images/next.png', 'align=absmiddle'), $uri.$pager->getNextPage());
      $navigation .= "<span style='cursor:pointer;color:blue;' onclick=\"ajax_paginator('$divId', '$nextUri')\" ><img src='../sf/sf_admin/images/next.png' /></span>" ;
     // $navigation .= link_to(image_tag('/sf/sf_admin/images/last.png', 'align=absmiddle'), $uri.$pager->getLastPage());
      $navigation .= "<span style='cursor:pointer;color:blue;' onclick=\"ajax_paginator('$divId', '$lastUri')\" ><img src='../sf/sf_admin/images/last.png' /></span>" ;
    }

  }

  return $navigation;
}
function getQueryString($sf_request, $include_page=false) {
    $params = array();
    foreach($sf_request->getParameterHolder()->getAll() as $key => $val)
    {
        if(is_array($val)) {
            foreach($val as $k1 => $v1) {
                if(is_array($v1)) {
                    foreach($v1 as $k2 => $v2) {
                        if(is_array($v2)) {
                            continue;
                        } else {
                            if(!in_array($k2, array('action','module','page')) && trim($v2)!='') {
                                $params[] = $key.'['.$k1.']['.$k2.']='.urlencode($v2);
                            }
                        }
                    }
                } else {
                    if(!in_array($k1, array('action','module','page')) && trim($v1)!='') {
                        $params[] = "$key[$k1]=".urlencode($v1);
                    }
                }
            }

            continue;
        }

        if(!in_array($key, array('action','module','page')) && trim($val)!='') {
            $params[] = $key.'='.urlencode(trim($val));
        }

        if($include_page && $key == 'page') {

            if(!$val) {
                $val = 1;
            }
            $params[] = $key.'='.urlencode(trim($val));
        }
    }
    if(count($params)) {
        $params = '?'.implode("&", $params);
    } else {
        $params = '?1=1';
    }
    return $params;
}