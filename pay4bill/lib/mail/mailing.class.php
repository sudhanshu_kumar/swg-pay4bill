<?php

//require_once(sfConfig::get('sf_lib_dir').'/vendor/swiftmailer/swift_required.php') ;

class Mailing {
/**
* Send a mail to user/s and/or all users of a group/s using secure mail.
*
*@author  Anurag
* @param  string                     $partialName           A Partial Name which will send an HTML email
* @param  array                      $partialVars           An Associative array with the name of variables which will be used in partial
* @param  array                      $mailingOptions        An Associative array with keys as : ['mailFrom']:string, ['mailTo']:array, ['mailToGroup']:array, ['subject']:string
*
* @return Boolean
* @example
* <pre>
* $partialVars = array('name' => 'Jhone Doe', 'age' => 27, 'marital_status' => 'Single') ;
* $mailTo = array('jhonDoe@abc.com', 'JaneDoe@xyz.com') ;
* $mailToGroup = array('admin', 'operation', 'business') ; // sfGuardUser Group
* $mailSubject = "Send the confirmation" ;
* $mailingOptions = array('mailFrom' => 'jerryWilliams@bce.com','mailTo' => $mailTo,'mailToGroup' =>  $mailToGroup, 'mailSubject' => $subject ) ;
* $sendMailObj = new Mailing() ;
* $sendMailObj->sendMail('xyzModule/abcPartial', $partialVars, $mailingOptions )     *
* </pre>
* @example /examples/example.php
*/
 public function sendMail($partialName, $partialVars, $mailingOptions, $mailServerSettings=null) 
 {

   $partialVars["html"] = true ;
   $currentAction = sfContext::getInstance()->getActionStack()->getLastEntry()->getActionInstance();

   try {
       
     $subject = $mailingOptions['mailSubject'] ;
     // Create the mailer and message objects
     if(isset($mailServerSettings['connectionType'])) {
       $connectionType = $mailServerSettings['connectionType'] ;
     }
     else {
       $connectionType = 'smtp';
     }
     
     
     if($connectionType == 'smtp')  // If secured connection to be used
     {
       if($mailServerSettings) {
         $server = $mailServerSettings['server'] ;
         $port = $mailServerSettings['port'] ;
         $encryption = $mailServerSettings['encryption'] ;
         $username = $mailServerSettings['username'] ;
         $password = $mailServerSettings['password'] ;
       } else {
           
         if(sfConfig::get('app_host')=='localhost'){
           $server = sfConfig::get('app_email_local_settings_server');
           $port = sfConfig::get('app_email_local_settings_port');
           $encryption = sfConfig::get('app_email_local_settings_encryption');
           $username = sfConfig::get('app_email_local_settings_username');
           $password = sfConfig::get('app_email_local_settings_password');
                      
           if((sfConfig::has('app_email_local_settings_bcc'))) {
              $bcc_email_addresses = sfConfig::get('app_email_local_settings_bcc');
           }
         }
         else if(sfConfig::get('app_host') == "production"){
           $server = sfConfig::get('app_email_production_settings_server');
           $port = sfConfig::get('app_email_production_settings_port');
           $username = sfConfig::get('app_email_production_settings_username');
           $password = sfConfig::get('app_email_production_settings_password');
           if((sfConfig::has('app_email_production_settings_bcc'))) {
              $bcc_email_addresses = sfConfig::get('app_email_production_settings_bcc');
           }
         }
       }
       // Create Connection
       if(sfConfig::get('app_host') == "production"){
          $connection = Swift_SmtpTransport::newInstance($server, $port);
       }
       else{

          $connection = Swift_SmtpTransport::newInstance($server, $port, $encryption)->setUsername($username)->setPassword($password);
       }

       $mailer = new Swift_Mailer($connection);

     }
     elseif($connectionType == 'native') {
       $mailer = new Swift(new Swift_Connection_NativeMail());
     } else {
       throw Exception("unsupported connection type: $connectionType");
     }
     
     $mailFrom = $mailingOptions['mailFrom'] ;
     if(array_key_exists('mailTo', $mailingOptions)) {
       $mailTo = $mailingOptions['mailTo'] ;
       $message = new Swift_Message($subject) ; //, $mailBody, 'text/html');
       
       $message->setFrom($mailFrom);
       $message->setTo($mailTo) ;

      
         if(isset($bcc_email_addresses) && ($bcc_email_addresses != "")) {
           $email_addresses = explode(",",$bcc_email_addresses);
           $message->setBcc($email_addresses) ;
         }
         
       $mailBody = $currentAction->getPartial($partialName, $partialVars);

       
       $message->setBody($mailBody, 'text/html') ;
       if(array_key_exists('attachements', $mailingOptions)) {

          $attch_array = $mailingOptions['attachements'];

           $attFile = $attch_array['filepath'];//'http://localhost/payforme_ewallet/web/uploads/Pay4Me_20100211_1.csv';
           $attMime = $attch_array['mime'];// 'text/plain';
           $attObj = Swift_Attachment::fromPath($attFile,$attMime);
           $message->attach($attObj);
       }

       $isSent = $mailer->send($message) ; 
     } 
     if(array_key_exists('mailToGroup', $mailingOptions)) {
       $mailToGroup = $mailingOptions['mailToGroup'] ;
       foreach($mailToGroup as $groupName) {
         $usersList =  $this->groupUser($groupName) ;
         if(is_array($usersList)) {
           foreach($usersList as $user) {
             if(count($user['UserDetail']) > 0) {

               $userEmail = $user['UserDetail']['0']['email'] ;

               $userFullName = $user['UserDetail']['0']['name'];
               if($userEmail) {
                 $message = new Swift_Message($subject) ; //, $mailBody, 'text/html');
                 $message->setFrom($mailFrom);
                 $message->setTo(array($userEmail)) ;
                 $partialVars['name'] = $userFullName ;
                 //print_r($partialVars) ;
                 $mailBody = $currentAction->getPartial($partialName, $partialVars);
                 $message->setBody($mailBody, 'text/html') ;
                 if(array_key_exists('attachements', $mailingOptions)) {
                   $attch_array = $mailingOptions['attachements'];
                   foreach ($attch_array as $k=>$attachement) {
                      $attFile = $attch_array['filepath'];
                      $attMime = $attch_array['mime'];
                     $attObj = Swift_Attachment::fromPath($attFile,$attMime);
                     $message->attach($attObj);
                   }
                 }
                 $mailer->send($message) ;
                 //$mailer->send($message, $userEmail, $mailFrom);
               }
             }

           }

         }
       }
     }
     
     //  $mailer->disconnect();
   }
   catch (Exception $e)
   {

     $logger=sfContext::getInstance()->getLogger();

     $logger->info('could not send mail');
     //$mailer->disconnect();
     return false ;
     // handle errors here
   }
   return true ;
 }


 public function groupUser($groupName) {

   if($groupName) {
     $userListQuery = Doctrine_Query::create()
     ->select('u.*, ud.*')
     ->from('sfGuardUser u')
     ->leftJoin('u.sfGuardUserGroup ug')
     ->leftJoin('ug.sfGuardGroup g')
     ->leftJoin('u.UserDetail ud')
     ->where('g.name=?',$groupName);

     $userList =  $userListQuery->execute(array(),Doctrine::HYDRATE_ARRAY) ;
     return $userList ;
   }
   return false ;
 }


}


?>
