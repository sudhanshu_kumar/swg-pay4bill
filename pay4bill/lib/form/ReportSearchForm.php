<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MerchantRegistrationForm
 *
 * @author amalhotra
 */
class ReportSearchForm extends sfForm {

    public function configure() {
        
        $arrPassFromAction = $this->getOptions();
        
        if(!isset($arrPassFromAction['report'])){        
            $this->widgetSchema['cust_name'] = new sfWidgetFormInput(array('label' => 'Customer Name'), array('maxlength' => 50, 'title' => 'Customer Name'));
            $this->widgetSchema['customer_id'] = new sfWidgetFormInput(array('label' => 'Customer Id'), array('maxlength' => 50, 'title' => 'Customer Id'));
            if (pay4billHelper::checkIfAdminUser(sfContext::getInstance()->getUser()->getGuardUser()->getGroups()->toArray())) {
                $this->widgetSchema['merchant_name'] = new sfWidgetFormInput(array('label' => 'Merchant Name'), array('maxlength' => 50, 'title' => 'Merchant Name'));
            }
        } else {
            $this->widgetSchema['merchant_name'] = new sfWidgetFormInput(array('label' => 'Merchant Name'), array('maxlength' => 50, 'title' => 'Merchant Name'));
            $this->widgetSchema['bank_name'] = new sfWidgetFormInput(array('label'=> 'Bank Name'), array('maxlength' => 50, 'title' => 'Bank Name'));
        }
        $this->widgetSchema['from'] = new sfWidgetFormDateCalInput(array('years' => DateHelper::getDateRanges(), 'format' => '%day% %month% %year%'), array());

        $this->widgetSchema['to'] = new sfWidgetFormDateCalInput(array('years' => DateHelper::getDateRanges(), 'format' => '%day% %month% %year%'), array());
        $this->widgetSchema->setLabels(array(
            'from' => 'From<font color=red>*</font>',
            'to' => 'To<font color=red>*</font>',
        ));
    }

}
?>