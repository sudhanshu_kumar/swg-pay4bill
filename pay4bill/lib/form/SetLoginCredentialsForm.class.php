<?php

/**
 * sfGuardFormSignin for sfGuardAuth signin action
 *
 * @package    sfDoctrineGuardPlugin
 * @subpackage form
 * @author     Fabien Potencier <fabien.potencier@symfony-project.com>
 * @version    SVN: $Id: sfGuardFormSignin.class.php 23536 2009-11-02 21:41:21Z Kris.Wallsmith $
 */
class SetLoginCredentialsForm extends sfForm {

    /**
     * @see sfForm
     */
    public function configure() {

        $this->widgetSchema['username'] = new sfWidgetFormInputText(array('label' => 'Username<span class="redLbl">*</span>'), array('class' => 'FormInput9'));
        $this->widgetSchema['password'] = new sfWidgetFormInputPassword(array('label' => 'Password<span class="redLbl">*</span>'), array('class' => 'FormInput9'));
        $this->widgetSchema['email_address'] = new sfWidgetFormInputText(array('label' => 'Email Address<span class="redLbl">*</span>'), array('class' => 'FormInput9'));
        $this->widgetSchema['password'] = new sfWidgetFormInputPassword(array('label' => 'Password<span class="redLbl">*</span>'), array('class' => 'FormInput9'));
        $this->widgetSchema['confirm_password'] = new sfWidgetFormInputPassword(array('label' => 'Confirm Password<span class="redLbl">*</span>'), array('class' => 'FormInput9'));
        $this->validatorSchema['username'] = new sfValidatorRegex(array('pattern' => '/^[a-z0-9._@]+$/', 'min_length' => 6,
                    'required' => true), array('min_length' => 'Username can\'t be less than 6 characters', 'required' => 'Please enter Username', 'invalid' => 'Please enter valid Username'));

//        new sfValidatorString(array('required' => true, 'min_length' => '6', 'max_length' => '20', 'trim' => true), array('required' => 'Please enter Username', 'min_length' => "Username can't be less than 6 characters", 'max_length' => "Username can't be more than 20 characters"));
        $this->validatorSchema['password'] = new sfValidatorString(array('max_length' => 20, 'min_length' => 6),
                        array('min_length' => 'Password can\'t be less than 6 characters', 'max_length' => 'Password can\'t be more than 20 characters',
                            'required' => 'Please enter Password'));

        $this->validatorSchema['confirm_password'] = new sfValidatorString(array('max_length' => 20, 'min_length' => 6),
                        array('min_length' => 'Password can\'t be less than 6 characters', 'max_length' => 'Password can\'t be more than 20 characters',
                            'required' => 'Please enter Confirm Password'));
        $this->validatorSchema['email_address'] = new sfValidatorEmail(array('max_length' => 40, 'required' => true),
                        array('invalid' => 'Email address is invalid', 'max_length' => 'Email address can not be more than 40 characters', 'required' => 'Please enter Email Address'));

        $r = $this->validatorSchema->setPostValidator(
                        new sfValidatorSchemaCompare('password', sfValidatorSchemaCompare::EQUAL, 'confirm_password',
                                array(),
                                array('invalid' => "Passwords don\'t match")));
        $this->widgetSchema['customerId'] = new sfWidgetFormInputhidden();
        if (!$this->hasDefault('customerId')) {

            $this->widgetSchema['customerId']->setDefault($this->getOption('customerId'));
        }

        $this->validatorSchema->setPostValidator(new sfValidatorAnd(array(
                    new sfValidatorCallback(array('callback' => array($this, 'checkUsernameAvailablity'))))));
        $this->validatorSchema->setOption('allow_extra_fields', true);
        $this->widgetSchema->setNameFormat('SetUserNamePassword[%s]');
    }

    public function checkUsernameAvailablity($validator, $values) {
        $orginalValues = $values; // If there is any error in any field which is validated from any validator, the value for that field will be blank
        $values = $this->getTaintedValues();
        $customerProfileobj = Doctrine::getTable('CustomerProfile')->findBy('customer_id', $values['customerId'])->getFirst();
        if (is_object($customerProfileobj)) {
            if ($customerProfileobj->getIsActive() == 0) {


                $error[$this->getName()] = new sfValidatorError($validator, 'This Customer<b>[' . $values['customerId'] . ']</b> has been disabled');
            }
            if ($customerProfileobj->getMerchantProfile()->getDisabled() == 1) {
                $error[$this->getName()] = new sfValidatorError($validator, 'Merchant of this Customer<b>[' . $values['customerId'] . ']</b> has been disabled');
            }
        } else {
            $error[$this->getName()] = new sfValidatorError($validator, 'Customer<b>[' . $values['customerId'] . ']</b> has been deleted');
        }

        if (array_key_exists('username', $orginalValues) && $orginalValues['username'] != "") {
            $collectionObj = Doctrine::getTable('sfGuardUser')->findBy('username', $orginalValues['username']);
            if ($collectionObj->count() > 0) {
                $error['username'] = new sfValidatorError($validator, 'Username Already Exists');
            }
        }

        if (array_key_exists('password', $values) && $values['password'] != "" && $values['confirm_password'] != "") {
            if ($values['password'] != $values['confirm_password']) {
                $error['confirm_password'] = new sfValidatorError($validator, "Passwords don't match");
            }
        }
        if (array_key_exists('email_address', $values) && $values['email_address'] != "") {

            $collectionObj = Doctrine::getTable('sfGuardUser')->findBy('email_address', $values['email_address']);

            if ($collectionObj->count() > 0) {
                $error['email_address'] = new sfValidatorError($validator, 'Email Address already exists');
            }
        }
        if (array_key_exists('customerId', $values) && $values['customerId'] != "" && is_object($customerProfileobj)) {

            $collectionObj = Doctrine::getTable('UserCustomerMapping')->findBy('customer_profile_id', $customerProfileobj->getId());

            if ($collectionObj->count() > 0) {
                $error[$this->getName()] = new sfValidatorError($validator, 'This Customer<b>[' . $values['customerId'] . ']</b> already has Login Credentials');
            }
        }
        if (!empty($error))
            throw new sfValidatorErrorSchema($validator, $error);
        return $values;
    }

    public function updateUserDetails($userInput) {
        try {

            $con = Doctrine::getTable('sfGuardUser')->getConnection();
            $con->beginTransaction();
            $ob = new sfGuardUser();
            $ob->setUsername($userInput['username']);
            $ob->setEmailAddress($userInput['email_address']);
            $ob->setPassword($userInput['password']);
            $ob->setIsActive(1);
            $ob->link('Groups', array(2));
            $ucm = new UserCustomerMapping();

            $ucm->setCustomerProfile(Doctrine::getTable('CustomerProfile')->findBy('customer_id', $userInput['customerId'])->getFirst());

            $ucm->setSfGuardUser($ob);
            $ob->save();
            $ucm->save();
            $con->commit();
        } catch (Exception $e) {
            $con->rollback();

//            echo "<pre>";
            print_r($e->getMessage());
//            exit();
        }
    }

}
