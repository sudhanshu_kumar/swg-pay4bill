<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MerchantRegistrationForm
 *
 * @author amalhotra
 */
class CustomerIdAuthenticationForm extends sfform {

    public function configure() {

        $this->widgetSchema['activeMerchants'] = new sfWidgetFormDoctrineChoice(array('model' => 'MerchantProfile', 'add_empty' => 'Please Select', 'order_by' => array('MerchantName' => 'asc'), 'table_method' => 'getFullyConfiguredActiveMerchants', 'label' => 'Merchant<span class="redLbl">*</span>'), array('class' => 'FormInput9'));
        $this->widgetSchema['customerId'] = new sfWidgetFormInputText(array('label' => 'Customer Id<span class="redLbl">*</span>'), array('class' => 'FormInput9'));
        $this->widgetSchema['pin'] = new sfWidgetFormInputText(array('label' => 'PIN<span class="redLbl">*</span>'), array('class' => 'FormInput9'));
        $this->widgetSchema['validation_number'] = new sfWidgetFormInputText(array('label' => 'Validation Number<span class="redLbl">*</span>'), array('class' => 'FormInput9'));
        $ob = Doctrine::getTable('MerchantProfile')->getInstance();
        $this->validatorSchema['activeMerchants'] = new sfValidatorDoctrineChoice(array('model' => 'MerchantProfile', 'query' => $ob->getFullyConfiguredActiveMerchants(), 'required' => true), array('invalid' => 'Please select valid Merchant', 'required' => 'Please select Merchant'));

        $this->validatorSchema['customerId'] = new sfValidatorRegex(array('pattern' => '/^[A-Za-z0-9]+$/', 'max_length' => 12, 'min_length' => 8,
                    'required' => true), array('max_length' => 'Customer Id can\'t be more than 12 characters',
                    'min_length' => 'Customer Id can\'t be less than 8 characters', 'required' => 'Please enter Customer Id', 'invalid' => 'Please enter valid Customer Id'));

        $this->validatorSchema['pin'] = new sfValidatorRegex(array('pattern' => '/^[A-Z0-9]+$/', 'max_length' => 8, 'min_length' => 8,
                    'required' => false), array('max_length' => 'PIN can\'t be more than 8 characters',
                    'min_length' => 'PIN can\'t be less than 8 characters', 'invalid' => 'Please enter valid PIN'));

        $this->validatorSchema['validation_number'] = new sfValidatorRegex(array('pattern' => '/^[0-9]+$/', 'min_length' => 6,
                    'required' => false), array('min_length' => 'Validation Number can\'t be less than 6 digits', 'invalid' => 'Please enter valid Validation Number'));

        $this->validatorSchema->setPostValidator(new sfValidatorAnd(array(
                    new sfValidatorCallback(array('callback' => array($this, 'validateMerchantCustomerIdCombination'))))));
        $this->validatorSchema->setOption('allow_extra_fields', true);
        $this->widgetSchema->setNameFormat('CustomerIdAuthentication[%s]');
    }

    public function validateMerchantCustomerIdCombination($validator, $values) {

        $values = $this->getTaintedValues();
        $mpOb = Doctrine::getTable('MerchantProfile')->find($values['activeMerchants']);
        
        /* @Saurabh 3 Sep, 2012
         * Check for disable customer at the time of  registration if cutomer has been disabled by merchant
         */
            $isActive = Doctrine::getTable('CustomerProfile')->findBy('customer_id',$values['customerId'])->getFirst();
        
            if ($values['pin'] != '' && $mpOb->getPaymentType() == 'o') {
                $resultSet = Doctrine::getTable('CustomerProfile')->customerIdAuthenticationWithPin($values['activeMerchants'], $values['customerId'], $values['pin']);

                if ($resultSet->count() == 0) {
                    $error['CustomerIdAuthentication'] = new sfValidatorError($validator, 'Merchant/ Customer Id/ PIN is invalid');
                } else if ($resultSet->getFirst()->getUserCustomerMapping()->count() != 0) {
                    $error['CustomerIdAuthentication'] = new sfValidatorError($validator, 'This Customer already has Login Credentials');
                } else if($isActive->getIsActive() == 0){
                    $error['CustomerIdAuthentication'] = new sfValidatorError($validator, 'This customer has been disabled');
                }
            }
            if ($values['validation_number'] != '' && $mpOb->getPaymentType() == 'b') {
                $resultSet = Doctrine::getTable('CustomerProfile')->customerIdAuthenticationWithValidationNumber($values['activeMerchants'], $values['customerId'], $values['validation_number']);
                if ($resultSet->count() == 0) {
                    $error['CustomerIdAuthentication'] = new sfValidatorError($validator, 'Merchant/ Customer Id/ Validation Number is invalid');
                } else if ($resultSet->getFirst()->getUserCustomerMapping()->count() != 0) {
                    $error['CustomerIdAuthentication'] = new sfValidatorError($validator, 'This Customer already has Login Credentials');
                } else if($isActive->getIsActive() == 0){
                    $error['CustomerIdAuthentication'] = new sfValidatorError($validator, 'This customer has been disabled');
                }
            }
       
        if ($values['validation_number'] == '' && $values['pin'] == '') {
            if ($mpOb->getPaymentType() == 'b') {
                $error['validation_number'] = new sfValidatorError($validator, 'Please enter Validation Number');
            } else {
                $error['pin'] = new sfValidatorError($validator, 'Please enter PIN');
            }
        }
        if (!empty($error))
            throw new sfValidatorErrorSchema($validator, $error);
        return $values;
    }

}

?>
