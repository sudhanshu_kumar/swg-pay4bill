<?php

/**
 * MerchantTax form.
 *
 * @package    nisp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class MerchantTaxForm extends BaseMerchantTaxForm
{
  public function configure()
  {
       unset(
      $this['created_at'],$this['updated_at'],$this['id'],$this['tax_id'],$this['merchant_id']
    );
       $this->validatorSchema['tax_value'] = new sfValidatorString(array('max_length' => 5, 'required' => true),
                        array('invalid' => 'Tax Value is invalid.', 'max_length' => 'Tax Value can not be more than 5 numbers.', 'required' => 'Tax Value is required'));
  }
}
