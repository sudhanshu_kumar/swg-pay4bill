<?php

/**
 * MerchantProfile form.
 *
 * @package    nisp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class MerchantProfileForm extends BaseMerchantProfileForm {

    public function configure() {
        if ($this->getOption('valid_cac') == 0) {
            unset($this['payment_type'], $this['disabled'], $this['approved'], $this['created_at'], $this['tiny_url'], $this['updated_at'], $this['company_url'], $this['merchant_contact_address'], $this['merchant_id'], $this['id'], $this['merchant_service_id'],
                    $this['merchant_code'],
                    $this['merchant_key'],
                    $this['object_biller_type'],
                    $this['unique_attribute_id']);
//            $this->widgetSchema['merchant_name']->setLabel('Company Name');
            $this->widgetSchema['merchant_name']->setLabel('Company Name<span class="redLbl">*</span>');
            $this->widgetSchema['cac_registration']->setLabel('CAC Registration<span class="redLbl">*</span>');
            $this->validatorSchema['cac_registration'] = new sfValidatorString(array('required' => true),
                            array('required' => 'CAC Registration  is required'));
            $this->validatorSchema['merchant_contact_number'] = new sfValidatorRegex(array('pattern' => '/^[+]{0,1}[0-9-]*$/', 'max_length' => 20, 'min_length' => 6,
                        'required' => true), array('max_length' => 'Contact Phone number can not  be more than 20 digits.',
                        'min_length' => 'Contact Phone number is too short(minimum 6 digits).',
                        'invalid' => 'Contact Phone number is invalid', 'required' => 'Contact Phone number is required.'));
            $this->widgetSchema['merchant_contact_number']->setLabel('Contact Number<span class="redLbl">*</span>');
            $this->validatorSchema['company_logo'] = new sfValidatorFile(array(
                        'required' => false,
                        'path' => sfConfig::get('sf_upload_dir') . '/merchant_logo',
                        'mime_types' => array(
                            'image/jpeg',
                            'image/png',
                            'image/gif'),
                        'max_size' => '410623',
                            ), array('required' => "Please upload identity proof.", "mime_types" => "Please upload JPG/GIF/PNG images only.", "max_size" => "Merchant Logo can not be more than 400 KB."));
            $this->widgetSchema['company_logo']->setLabel('Merchant Logo');

            $this->widgetSchema['company_logo'] = new sfWidgetFormInputFile(array(), array('class' => 'txt-input', 'size' => '31', 'maxlength' => 100, 'min_length' => 3));
        } else {
            unset($this['approved'], $this['created_at'], $this['updated_at'], $this['merchant_id'], $this['id'], $this['payment_type'],
                    $this['merchant_service_id'],
                    $this['merchant_code'],
                    $this['merchant_key'],
                    $this['object_biller_type'],
                    $this['unique_attribute_id']);
            $this->widgetSchema['merchant_name']->setLabel('Company Name');
            $this->widgetSchema['merchant_name']->setAttribute('readonly', 'readonly');
            $this->widgetSchema['cac_registration']->setAttribute('readonly', 'readonly');
            $this->widgetSchema['merchant_contact_number']->setAttribute('readonly', 'readonly');
        }
    }

    public function updateObject($values = null) {


        $this->getObject()->setApproved('false');
//     $this->getObject()->setIsActive(0);
//echo "<pre>";
//echo "jkhjk";
//print_r($this->getValues());
//exit;
        parent::updateObject($values);
    }

}
