<?php

/**
 * CustomerProfile form.
 *
 * @package    nisp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class CustomerProfileForm extends BaseCustomerProfileForm {

    public function configure() {
        unset($this['id'], $this['pin'], $this['object_id'], $this['is_active'], $this['merchant_profile_id'], $this['customer_id'], $this['created_at'], $this['updated_at'], $this['created_by'], $this['updated_by']);

        $attribute_master_name = '';

        $formArray = $this->getOptions();

        $email_address_mandatary = '';

        if (isset($formArray['email_address_mandatary'])) {
            $email_address_mandatary = $formArray['email_address_mandatary'];
        }

//        if (count($this->getOptions()) > 0) {
//
//            foreach ($this->getOptions() as $k => $v) {
//
//                /* Takes the value if the customer is allowed to login */
//
//                if (count($v) > 0 && is_array($v)) {
//                    foreach ($v as $key => $value) {
//
////                        if ($k == 'email_address_mandatary') {
////                              $email_address_mandatary = $value;
////                        }
//                        if ($k == 'misc' && $key == 'unique_field_id') {
//                            $attribute_master_id = $value; // To know which field have unique attribute
//                        }
//                        if ($k == 'misc' && $key == 'unique_field_name') {
//                            $attribute_master_name = $value; // To have the unique field name
//                        }
//                        if ($key == 'att_name')
//                            $arr[$k]['att_name'] = $value;
//                         if ($key == 'att_label')
//                            $arr[$k]['att_label'] = $value;
//                        if ($key == 'att_type')
//                            $arr[$k]['att_type'] = $value;
//                        if ($key == 'is_mandatory')
//                            $arr[$k]['is_mandatory'] = $value;
//                        if ($key == 'is_unique')
//                            $arr[$k]['is_unique'] = $value;
//                        if ($key == 'att_value')
//                            $arr[$k]['att_value'] = $value;
//                    }
//                }
//            }
//        }
//
//
//        if (isset($arr) && count($arr) > 0) {
//            foreach ($arr as $va) {
//                $va = array_reverse($va, true);
//
//                if (count($va) > 0) {
//                    foreach ($va as $k => $v) {
//
//                        if ($k == 'att_label')
//                            $atlabel = $v;
//
//                        if ($k == 'att_type')
//                            $attype = $v;
//                        if ($k == 'is_mandatory')
//                            $mandat = $v;
//                        if ($k == 'att_value')
//                            $attval = $v;
//
//                        if ($k == 'att_name') {
//                            $t = $atlabel; // Will be used if the field are having spaces ni between: For Labeling only
//                            $v = str_replace(" ", "", $v);
//
//                            $this->widgetSchema[$v] = new sfWidgetFormInputText(array('label' => $t . '<span class="redLbl">*</span>'));
//
//                            if (isset($attval) && $attval <> '')
//                                $this->setDefault($v, $attval);
//                            // $this->widgetSchema[$v] =  new sfWidgetFormInputText(array(),array('value'=>$attval));
//
//                            if (strtolower($attype) == 'integer') {
//                                if ($mandat == 1) {
//                                    $this->validatorSchema[$v] = new sfValidatorRegex(array('required' => true, 'pattern' => '/^[0-9]*$/i'), array('required' => 'Please enter ' . $t, 'invalid' => pay4billHelper::validationMsgforAttType($attype)));
//                                } else {
//                                    $this->widgetSchema[$v]->setLabel($t . '<span class="redLbl"> </span>');
//                                    $this->validatorSchema[$v] = new sfValidatorRegex(array('required' => false, 'pattern' => '/^[0-9]*$/i'), array('invalid' => pay4billHelper::validationMsgforAttType($attype)));
//                                }
//                            } else if (strtolower($attype) == 'string') {
//                                if ($mandat == 1) {
//                                    $this->validatorSchema[$v] = new sfValidatorRegex(array('required' => true, 'pattern' => '/^[a-z]*$/i'), array('required' => 'Please enter ' . $t, 'invalid' => pay4billHelper::validationMsgforAttType($attype)));
//                                } else {
//                                    $this->widgetSchema[$v]->setLabel($t . '<span class="redLbl"> </span>');
//                                    $this->validatorSchema[$v] = new sfValidatorRegex(array('required' => false, 'pattern' => '/^[a-z]*$/i'), array('invalid' => pay4billHelper::validationMsgforAttType($attype)));
//                                }
//                            } else if (strtolower($attype) == 'alphanumeric') {
//                                if ($mandat == 1) {
//                                    $this->validatorSchema[$v] = new sfValidatorRegex(array('required' => true, 'pattern' => '/^[a-z0-9]*$/i'), array('required' => 'Please enter ' . $t, 'invalid' => pay4billHelper::validationMsgforAttType($attype)));
//                                } else {
//                                    $this->widgetSchema[$v]->setLabel($t . '<span class="redLbl"> </span>');
//                                    $this->validatorSchema[$v] = new sfValidatorRegex(array('required' => false, 'pattern' => '/^[a-z0-9]*$/i'), array('invalid' => pay4billHelper::validationMsgforAttType($attype)));
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        }


        if ($email_address_mandatary == 1) {
            $this->widgetSchema['email_address']->setLabel('Email Address<span class="redLbl">*</span> ');
            $this->validatorSchema['email_address'] = new sfValidatorEmail(array('max_length' => 40, 'required' => true),
                            array('invalid' => 'Please enter valid email address', 'max_length' => 'Email address can not be more than 40 characters', 'required' => 'Please enter Email Address'));
        } else {
            $this->widgetSchema['email_address']->setLabel('Email Address<span class="redLbl"> </span> ');
            $this->validatorSchema['email_address'] = new sfValidatorEmail(array('max_length' => 40, 'required' => false),
                            array('invalid' => 'Please enter valid email address', 'max_length' => 'Email address can not be more than 40 characters', 'required' => 'Please enter Email Address'));
        }

        if ($this->getoption('email_address') != '') {
            $this->setDefault('email_address', $this->getoption('email_address'));
            $this->setDefault('', '');
        }

        /*
         * @Saurabh on 21 June
         * Set merchant id in hidden in case merchant coming from teller
         * by selecting the merchant fromm dropdown
         */

        $this->widgetSchema['merchant_id'] = new sfWidgetFormInputHidden();
        $this->setDefault('merchant_id', $this->getOption('merchant_id'));

        /*
         * @Saurabh - 14June12
         * Creating hidden fields for call back functions to acces
         */

//        $this->widgetSchema['attribute_master_id'] = new sfWidgetFormInputHidden();
//        $this->setDefault('attribute_master_id', $attribute_master_id);
//
//        $this->validatorSchema['attribute_master_id'] = new sfValidatorInteger(array('required' => false));
//
//        $this->widgetSchema['attribute_master_name'] = new sfWidgetFormInputHidden();
//        $this->setDefault('attribute_master_name', $attribute_master_name);
//        $this->validatorSchema['attribute_master_name'] = new sfValidatorString(array('required' => false));





        $this->widgetSchema['name']->setLabel('Name<span class="redLbl">*</span> ');
        //@Saurabh 13-June-2012: Adding validation
        $this->validatorSchema['name'] = new sfValidatorRegex(array('required' => true, 'pattern' => '/^[a-z\s\.]*$/i', 'trim' => true, 'max_length' => 50), array('required' => 'Please enter Name', 'invalid' => 'Please enter valid Name', 'max_length' => 'Name can\'t be more than 50 characters'));
        $this->widgetSchema['sex'] = new sfWidgetFormChoice(array('choices' => array('' => 'Please Select', 'Male' => 'Male', 'Female' => 'Female')));
        //@Saurabh 13-June-2012: Adding validation
        $this->validatorSchema['sex'] = new sfValidatorString(array('required' => true), array('required' => 'Please select Sex'));
        $this->widgetSchema['sex']->setLabel('Sex<span class="redLbl">*</span> ');
        $this->widgetSchema['address']->setLabel('Address<span class="redLbl">*</span> ');
        //@Saurabh 13-June-2012: Adding validation
        $this->validatorSchema['address'] = new sfValidatorString(array('required' => true, 'max_length' => 256, 'trim' => true), array('required' => 'Please enter Address', 'max_length' => 'More than 256 characters not allowed'));
        $this->widgetSchema['phone_number']->setLabel('Phone Number<span class="redLbl">*</span> ');
        //@Saurabh 13-June-2012: Adding validation
        $this->validatorSchema['phone_number'] = new sfValidatorRegex(array('pattern' => '/^[+]{0,1}[0-9-]{6,20}$/', 'max_length' => 20, 'min_length' => 6,
                    'required' => true), array('max_length' => 'Phone Number can not  be more than 20 digits.',
                    'min_length' => 'Phone Number cannot be less than 6 digits', 'required' => 'Please enter Phone Number', 'invalid' => 'Please enter valid Phone Number'));
        if ($this->getOption('optionForDynAttr')) {
            $this->embedForm('attrForm', new DynamicAttrFormOnCustomerEdit(array(), array('configuration' => $this->getOption('optionForDynAttr'), 'object_id' => $this->getOption('object_id'))));
        }

        $this->validatorSchema->setOption('allow_extra_fields', true);
    }

}

