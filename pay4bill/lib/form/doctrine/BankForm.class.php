<?php

/**
 * Bank form.
 *
 * @package    nisp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class BankForm extends BaseBankForm
{
  public function configure() {

        unset($this['merchant_id'],$this['created_at'], $this['updated_at']);
        $this->setWidgets(array(
            'id' => new sfWidgetFormInputHidden(),
            'merchant_id' => new sfWidgetFormInputHidden(),
            'bank_name' => new sfWidgetFormInputText(array(), array('maxlength' => 40,'minlength' => 5)),
            'bank_branch' => new sfWidgetFormInputText(array(), array('maxlength' => 50,'minlength' => 5)),
            'account_name' => new sfWidgetFormInputText(array(), array('maxlength' => 100,'minlength' => 5)),
            'account_number' => new sfWidgetFormInputText(array(), array('maxlength' => 20,'minlength' => 5)),
            'sort_code' => new sfWidgetFormInputText(array(), array('maxlength' => 7,'minlength' => 1)),
        ));
        $this->setValidators(array(
            'merchant_id' => new sfValidatorNumber(array('required'=>false)),
            'bank_name' => new sfValidatorString(array('min_length' => 5, 'max_length' => 40,'required'=>true), array('invalid' => 'Invalid Bank Name', 'min_length' => 'Bank Name should be greater than 5 characters', 'max_length' => 'Bank Name should be less than or equal to 40 characters')),
            'bank_branch' => new sfValidatorString(array('min_length' => 5, 'max_length' => 50,'required'=>true), array('invalid' => 'Invalid Bank Branch', 'min_length' => 'Bank Branch should be greater than 5 characters', 'max_length' => 'Bank Branch should be less than or equal to 50 characters')),
            'account_name' => new sfValidatorString(array('min_length' => 5, 'max_length' => 100,'required'=>true), array('invalid' => 'Invalid Account Name', 'min_length' => 'Account Name should be greater than 5 characters', 'max_length' => 'Account Name should be less than or equal to 100 characters')),
            'account_number' => new sfValidatorString(array('min_length' => 5, 'max_length' => 20,'required'=>true), array('invalid' => 'Invalid Account Number', 'min_length' => 'Account Numner should be greater than 5 characters', 'max_length' => 'Account Number should be less than or equal to 20 characters')),
            'sort_code' => new sfValidatorString(array('min_length' => 1, 'max_length' => 7,'required'=>true), array('invalid' => 'Invalid Sort Code', 'min_length' => 'Sort Code should be greater than 1 characters', 'max_length' => 'Sort Code should be less than or equal to 7 characters')),

//            'cart_amount_capacity' => new sfValidatorNumber(array('min' => 0, 'max' => 99999,'required'=>false), array('invalid' => 'Invalid Cart Amount Capacity', 'min' => 'Cart Amount Capacity should be greater than 0', 'max' => 'Cart Amount Capacity should be less than or equal to 99999')),
//            'number_of_transaction' => new sfValidatorInteger(array('min' => 0, 'max' => 99,'required'=>false), array('invalid' => 'Invalid Number of Transaction(s)', 'min' => 'Number of Transaction(s) should be greater than 0', 'max' => 'Number of Transaction(s) should be less than or equal to 99')),
//            'transaction_period' => new sfValidatorInteger(array('min' => 0, 'max' => 9999999,'required'=>false), array('invalid' => 'Invalid Transaction Period', 'min' => 'Transaction Period should be greater than 0', 'max' => 'Transaction Period should be less than or equal to 999999')),
        ));

        $this->widgetSchema->setLabels(array(
            'bank_name' => 'Bank Name<sup><font color="red">*</font></sup>',
            'bank_branch' => 'Bank Branch<sup><font color="red">*</font></sup>',
            'account_name' => 'Account Name<sup><font color="red">*</font></sup>',
            'account_number' => 'Account Number<sup><font color="red">*</font></sup>',
            'sort_code' => 'Sort Code<sup><font color="red">*</font></sup>',
//            'cart_amount_capacity' => 'Cart Amount capacity',
//            'number_of_transaction' => 'Allowed Number of Transaction(s)',
//            'transaction_period' => 'Transaction Period',
        ));

    }
}
