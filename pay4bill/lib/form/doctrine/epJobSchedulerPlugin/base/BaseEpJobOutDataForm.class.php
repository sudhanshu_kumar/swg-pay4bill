<?php

/**
 * EpJobOutData form base class.
 *
 * @method EpJobOutData getObject() Returns the current form's model object
 *
 * @package    nisp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedInheritanceTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseEpJobOutDataForm extends EpJobDataForm
{
  protected function setupInheritance()
  {
    parent::setupInheritance();

    $this->widgetSchema->setNameFormat('ep_job_out_data[%s]');
  }

  public function getModelName()
  {
    return 'EpJobOutData';
  }

}
