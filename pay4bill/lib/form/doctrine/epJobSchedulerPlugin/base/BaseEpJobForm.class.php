<?php

/**
 * EpJob form base class.
 *
 * @method EpJob getObject() Returns the current form's model object
 *
 * @package    nisp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseEpJobForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                    => new sfWidgetFormInputHidden(),
      'name'                  => new sfWidgetFormInputText(),
      'url'                   => new sfWidgetFormInputText(),
      'sf_application'        => new sfWidgetFormInputText(),
      'start_time'            => new sfWidgetFormDateTime(),
      'end_time'              => new sfWidgetFormDateTime(),
      'schedule_type'         => new sfWidgetFormChoice(array('choices' => array('once' => 'once', 'repeated' => 'repeated'))),
      'state'                 => new sfWidgetFormChoice(array('choices' => array('active' => 'active', 'suspended' => 'suspended', 'finished' => 'finished'))),
      'last_execution_status' => new sfWidgetFormChoice(array('choices' => array('notexecuted' => 'notexecuted', 'pass' => 'pass', 'failed' => 'failed'))),
      'max_retry_attempts'    => new sfWidgetFormInputText(),
      'execution_attempts'    => new sfWidgetFormInputText(),
      'created_at'            => new sfWidgetFormDateTime(),
      'updated_at'            => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                    => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'name'                  => new sfValidatorString(array('max_length' => 40, 'required' => false)),
      'url'                   => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'sf_application'        => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'start_time'            => new sfValidatorDateTime(array('required' => false)),
      'end_time'              => new sfValidatorDateTime(array('required' => false)),
      'schedule_type'         => new sfValidatorChoice(array('choices' => array(0 => 'once', 1 => 'repeated'), 'required' => false)),
      'state'                 => new sfValidatorChoice(array('choices' => array(0 => 'active', 1 => 'suspended', 2 => 'finished'), 'required' => false)),
      'last_execution_status' => new sfValidatorChoice(array('choices' => array(0 => 'notexecuted', 1 => 'pass', 2 => 'failed'), 'required' => false)),
      'max_retry_attempts'    => new sfValidatorInteger(array('required' => false)),
      'execution_attempts'    => new sfValidatorInteger(array('required' => false)),
      'created_at'            => new sfValidatorDateTime(),
      'updated_at'            => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('ep_job[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpJob';
  }

}
