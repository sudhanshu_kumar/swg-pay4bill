<?php

/**
 * AttributeMaster form base class.
 *
 * @method AttributeMaster getObject() Returns the current form's model object
 *
 * @package    nisp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseAttributeMasterForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'             => new sfWidgetFormInputHidden(),
      'att_name'       => new sfWidgetFormInputText(),
      'att_label'      => new sfWidgetFormInputText(),
      'att_type'       => new sfWidgetFormChoice(array('choices' => array('String' => 'String', 'Integer' => 'Integer', 'Alphanumeric' => 'Alphanumeric'))),
      'merchant_id'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantProfile'), 'add_empty' => true)),
      'object_type_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('ObjectType'), 'add_empty' => true)),
      'is_mandatory'   => new sfWidgetFormChoice(array('choices' => array(0 => 0, 1 => 1))),
      'is_unique'      => new sfWidgetFormChoice(array('choices' => array(0 => 0, 1 => 1))),
      'is_active'      => new sfWidgetFormChoice(array('choices' => array(0 => 0, 1 => 1))),
      'created_at'     => new sfWidgetFormDateTime(),
      'updated_at'     => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'             => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'att_name'       => new sfValidatorString(array('max_length' => 100)),
      'att_label'      => new sfValidatorString(array('max_length' => 50)),
      'att_type'       => new sfValidatorChoice(array('choices' => array(0 => 'String', 1 => 'Integer', 2 => 'Alphanumeric'), 'required' => false)),
      'merchant_id'    => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantProfile'), 'required' => false)),
      'object_type_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('ObjectType'), 'required' => false)),
      'is_mandatory'   => new sfValidatorChoice(array('choices' => array(0 => 0, 1 => 1), 'required' => false)),
      'is_unique'      => new sfValidatorChoice(array('choices' => array(0 => 0, 1 => 1), 'required' => false)),
      'is_active'      => new sfValidatorChoice(array('choices' => array(0 => 0, 1 => 1), 'required' => false)),
      'created_at'     => new sfValidatorDateTime(),
      'updated_at'     => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('attribute_master[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'AttributeMaster';
  }

}
