<?php

/**
 * TellerMerchantMapping form base class.
 *
 * @method TellerMerchantMapping getObject() Returns the current form's model object
 *
 * @package    nisp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseTellerMerchantMappingForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                  => new sfWidgetFormInputHidden(),
      'teller_profile_id'   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TellerProfile'), 'add_empty' => false)),
      'merchant_profile_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantProfile'), 'add_empty' => false)),
      'is_active'           => new sfWidgetFormChoice(array('choices' => array(0 => 0, 1 => 1))),
      'created_at'          => new sfWidgetFormDateTime(),
      'updated_at'          => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                  => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'teller_profile_id'   => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('TellerProfile'))),
      'merchant_profile_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantProfile'))),
      'is_active'           => new sfValidatorChoice(array('choices' => array(0 => 0, 1 => 1), 'required' => false)),
      'created_at'          => new sfValidatorDateTime(),
      'updated_at'          => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('teller_merchant_mapping[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'TellerMerchantMapping';
  }

}
