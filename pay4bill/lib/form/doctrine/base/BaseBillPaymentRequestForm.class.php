<?php

/**
 * BillPaymentRequest form base class.
 *
 * @method BillPaymentRequest getObject() Returns the current form's model object
 *
 * @package    nisp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseBillPaymentRequestForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'          => new sfWidgetFormInputHidden(),
      'merchant_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantProfile'), 'add_empty' => false)),
      'customer_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('CustomerProfile'), 'add_empty' => false)),
      'invoice_id'  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Invoice'), 'add_empty' => true)),
      'created_at'  => new sfWidgetFormDateTime(),
      'updated_at'  => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'          => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'merchant_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantProfile'))),
      'customer_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('CustomerProfile'))),
      'invoice_id'  => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Invoice'), 'required' => false)),
      'created_at'  => new sfValidatorDateTime(),
      'updated_at'  => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('bill_payment_request[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'BillPaymentRequest';
  }

}
