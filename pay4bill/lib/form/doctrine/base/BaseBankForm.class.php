<?php

/**
 * Bank form base class.
 *
 * @method Bank getObject() Returns the current form's model object
 *
 * @package    nisp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseBankForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'             => new sfWidgetFormInputHidden(),
      'merchant_id'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'), 'add_empty' => false)),
      'bank_name'      => new sfWidgetFormInputText(),
      'bank_branch'    => new sfWidgetFormInputText(),
      'account_name'   => new sfWidgetFormInputText(),
      'account_number' => new sfWidgetFormInputText(),
      'sort_code'      => new sfWidgetFormInputText(),
      'created_at'     => new sfWidgetFormDateTime(),
      'updated_at'     => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'             => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'merchant_id'    => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'))),
      'bank_name'      => new sfValidatorString(array('max_length' => 40, 'required' => false)),
      'bank_branch'    => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'account_name'   => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'account_number' => new sfValidatorString(array('max_length' => 30, 'required' => false)),
      'sort_code'      => new sfValidatorInteger(array('required' => false)),
      'created_at'     => new sfValidatorDateTime(),
      'updated_at'     => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('bank[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Bank';
  }

}
