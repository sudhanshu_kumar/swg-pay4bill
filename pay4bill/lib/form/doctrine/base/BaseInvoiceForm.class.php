<?php

/**
 * Invoice form base class.
 *
 * @method Invoice getObject() Returns the current form's model object
 *
 * @package    pay4bill
 * @subpackage form
 * @author     Aditi
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseInvoiceForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                     => new sfWidgetFormInputHidden(),
      'invoice_no'             => new sfWidgetFormInputText(),
      'merchant_id'            => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Merchant'), 'add_empty' => false)),
      'customer_id'            => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Customer'), 'add_empty' => false)),
      'tax_id'                 => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Tax'), 'add_empty' => false)),
      'issue_date'             => new sfWidgetFormDate(),
      'delivery_date'          => new sfWidgetFormDate(),
      'due_date'               => new sfWidgetFormDate(),
      'currency_type'          => new sfWidgetFormInputText(),
      'additional_information' => new sfWidgetFormInputText(),
      'part_payment'           => new sfWidgetFormInputText(),
      'total_amount'           => new sfWidgetFormInputText(),
      'amount_paid'            => new sfWidgetFormInputText(),
      'amount_due'             => new sfWidgetFormInputText(),
      'parent_id_if_recurring' => new sfWidgetFormInputText(),
      'status'                 => new sfWidgetFormInputText(),
      'payment_date'           => new sfWidgetFormDate(),
      'payment_mode'           => new sfWidgetFormInputText(),
      'regenerated'            => new sfWidgetFormInputText(),
      'pay_full_amount'        => new sfWidgetFormInputText(),
      'object_id'              => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Object'), 'add_empty' => true)),
      'created_at'             => new sfWidgetFormDateTime(),
      'updated_at'             => new sfWidgetFormDateTime(),
      'created_by'             => new sfWidgetFormInputText(),
      'updated_by'             => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'                     => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'invoice_no'             => new sfValidatorString(array('max_length' => 44)),
      'merchant_id'            => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Merchant'))),
      'customer_id'            => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Customer'))),
      'tax_id'                 => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Tax'))),
      'issue_date'             => new sfValidatorDate(),
      'delivery_date'          => new sfValidatorDate(),
      'due_date'               => new sfValidatorDate(),
      'currency_type'          => new sfValidatorString(array('max_length' => 45)),
      'additional_information' => new sfValidatorString(array('max_length' => 100)),
      'part_payment'           => new sfValidatorNumber(array('required' => false)),
      'total_amount'           => new sfValidatorNumber(),
      'amount_paid'            => new sfValidatorNumber(),
      'amount_due'             => new sfValidatorNumber(),
      'parent_id_if_recurring' => new sfValidatorInteger(array('required' => false)),
      'status'                 => new sfValidatorInteger(),
      'payment_date'           => new sfValidatorDate(array('required' => false)),
      'payment_mode'           => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'regenerated'            => new sfValidatorInteger(array('required' => false)),
      'pay_full_amount'        => new sfValidatorPass(),
      'object_id'              => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Object'), 'required' => false)),
      'created_at'             => new sfValidatorDateTime(),
      'updated_at'             => new sfValidatorDateTime(),
      'created_by'             => new sfValidatorInteger(array('required' => false)),
      'updated_by'             => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('invoice[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Invoice';
  }

}
