<?php

/**
 * TellerProfile form base class.
 *
 * @method TellerProfile getObject() Returns the current form's model object
 *
 * @package    nisp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseTellerProfileForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                       => new sfWidgetFormInputHidden(),
      'user_id'                  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'), 'add_empty' => true)),
      'name'                     => new sfWidgetFormInputText(),
      'sex'                      => new sfWidgetFormInputText(),
      'address'                  => new sfWidgetFormTextarea(),
      'bankName'                 => new sfWidgetFormInputText(),
      'phone_number'             => new sfWidgetFormInputText(),
      'change_password_required' => new sfWidgetFormInputText(),
      'created_at'               => new sfWidgetFormDateTime(),
      'updated_at'               => new sfWidgetFormDateTime(),
      'created_by'               => new sfWidgetFormInputText(),
      'updated_by'               => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'                       => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'user_id'                  => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'), 'required' => false)),
      'name'                     => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'sex'                      => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'address'                  => new sfValidatorString(array('max_length' => 256, 'required' => false)),
      'bankName'                 => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'phone_number'             => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'change_password_required' => new sfValidatorInteger(array('required' => false)),
      'created_at'               => new sfValidatorDateTime(),
      'updated_at'               => new sfValidatorDateTime(),
      'created_by'               => new sfValidatorInteger(array('required' => false)),
      'updated_by'               => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('teller_profile[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'TellerProfile';
  }

}
