<?php

/**
 * MerchantProfile form base class.
 *
 * @method MerchantProfile getObject() Returns the current form's model object
 *
 * @package    pay4bill
 * @subpackage form
 * @author     Aditi
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseMerchantProfileForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                       => new sfWidgetFormInputHidden(),
      'merchant_id'              => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'), 'add_empty' => false)),
      'merchant_name'            => new sfWidgetFormInputText(),
      'cac_registration'         => new sfWidgetFormInputText(),
      'merchant_contact_address' => new sfWidgetFormTextarea(),
      'merchant_contact_number'  => new sfWidgetFormInputText(),
      'company_url'              => new sfWidgetFormInputText(),
      'approved'                 => new sfWidgetFormInputCheckbox(),
      'company_logo'             => new sfWidgetFormInputText(),
      'tiny_url'                 => new sfWidgetFormInputText(),
      'disabled'                 => new sfWidgetFormInputCheckbox(),
      'merchant_service_id'      => new sfWidgetFormInputText(),
      'merchant_code'            => new sfWidgetFormInputText(),
      'merchant_key'             => new sfWidgetFormInputText(),
      'object_biller_type'       => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('ObjectType'), 'add_empty' => true)),
      'payment_type'             => new sfWidgetFormChoice(array('choices' => array('o' => 'o', 'b' => 'b'))),
      'unique_attribute_id'      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('AttributeMaster'), 'add_empty' => true)),
      'created_at'               => new sfWidgetFormDateTime(),
      'updated_at'               => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                       => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'merchant_id'              => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'))),
      'merchant_name'            => new sfValidatorString(array('max_length' => 40, 'required' => false)),
      'cac_registration'         => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'merchant_contact_address' => new sfValidatorString(array('max_length' => 256, 'required' => false)),
      'merchant_contact_number'  => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'company_url'              => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'approved'                 => new sfValidatorBoolean(array('required' => false)),
      'company_logo'             => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'tiny_url'                 => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'disabled'                 => new sfValidatorBoolean(array('required' => false)),
      'merchant_service_id'      => new sfValidatorInteger(array('required' => false)),
      'merchant_code'            => new sfValidatorInteger(array('required' => false)),
      'merchant_key'             => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'object_biller_type'       => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('ObjectType'), 'required' => false)),
      'payment_type'             => new sfValidatorChoice(array('choices' => array(0 => 'o', 1 => 'b'), 'required' => false)),
      'unique_attribute_id'      => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('AttributeMaster'), 'required' => false)),
      'created_at'               => new sfValidatorDateTime(),
      'updated_at'               => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('merchant_profile[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'MerchantProfile';
  }

}
