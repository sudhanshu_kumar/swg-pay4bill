<?php

/**
 * AttributeValue form base class.
 *
 * @method AttributeValue getObject() Returns the current form's model object
 *
 * @package    nisp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseAttributeValueForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'            => new sfWidgetFormInputHidden(),
      'att_value'     => new sfWidgetFormInputText(),
      'att_master_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('AttributeMaster'), 'add_empty' => false)),
      'object_id'     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Object'), 'add_empty' => false)),
      'created_at'    => new sfWidgetFormDateTime(),
      'updated_at'    => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'            => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'att_value'     => new sfValidatorString(array('max_length' => 100)),
      'att_master_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('AttributeMaster'))),
      'object_id'     => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Object'))),
      'created_at'    => new sfValidatorDateTime(),
      'updated_at'    => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('attribute_value[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'AttributeValue';
  }

}
