<?php

/**
 * CustomerProfile form base class.
 *
 * @method CustomerProfile getObject() Returns the current form's model object
 *
 * @package    nisp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseCustomerProfileForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                  => new sfWidgetFormInputHidden(),
      'merchant_profile_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantProfile'), 'add_empty' => false)),
      'customer_id'         => new sfWidgetFormInputText(),
      'name'                => new sfWidgetFormInputText(),
      'sex'                 => new sfWidgetFormInputText(),
      'address'             => new sfWidgetFormTextarea(),
      'email_address'       => new sfWidgetFormInputText(),
      'pin'                 => new sfWidgetFormInputText(),
      'phone_number'        => new sfWidgetFormInputText(),
      'is_active'           => new sfWidgetFormChoice(array('choices' => array(0 => 0, 1 => 1))),
      'object_id'           => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Object'), 'add_empty' => true)),
      'created_at'          => new sfWidgetFormDateTime(),
      'updated_at'          => new sfWidgetFormDateTime(),
      'created_by'          => new sfWidgetFormInputText(),
      'updated_by'          => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'                  => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'merchant_profile_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('MerchantProfile'))),
      'customer_id'         => new sfValidatorString(array('max_length' => 20)),
      'name'                => new sfValidatorString(array('max_length' => 50)),
      'sex'                 => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'address'             => new sfValidatorString(array('max_length' => 256, 'required' => false)),
      'email_address'       => new sfValidatorString(array('max_length' => 40, 'required' => false)),
      'pin'                 => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'phone_number'        => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'is_active'           => new sfValidatorChoice(array('choices' => array(0 => 0, 1 => 1), 'required' => false)),
      'object_id'           => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Object'), 'required' => false)),
      'created_at'          => new sfValidatorDateTime(),
      'updated_at'          => new sfValidatorDateTime(),
      'created_by'          => new sfValidatorInteger(array('required' => false)),
      'updated_by'          => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('customer_profile[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CustomerProfile';
  }

}
