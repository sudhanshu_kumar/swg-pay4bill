<?php

/**
 * InvoiceItemDetails form base class.
 *
 * @method InvoiceItemDetails getObject() Returns the current form's model object
 *
 * @package    nisp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseInvoiceItemDetailsForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'               => new sfWidgetFormInputHidden(),
      'invoice_id'       => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Invoice'), 'add_empty' => false)),
      'item_description' => new sfWidgetFormInputText(),
      'no_of_items'      => new sfWidgetFormInputText(),
      'item_price'       => new sfWidgetFormInputText(),
      'item_tax'         => new sfWidgetFormInputText(),
      'subTotal'         => new sfWidgetFormInputText(),
      'tax_total'        => new sfWidgetFormInputText(),
      'created_at'       => new sfWidgetFormDateTime(),
      'updated_at'       => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'               => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'invoice_id'       => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Invoice'))),
      'item_description' => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'no_of_items'      => new sfValidatorInteger(),
      'item_price'       => new sfValidatorNumber(),
      'item_tax'         => new sfValidatorNumber(),
      'subTotal'         => new sfValidatorNumber(),
      'tax_total'        => new sfValidatorNumber(),
      'created_at'       => new sfValidatorDateTime(),
      'updated_at'       => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('invoice_item_details[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'InvoiceItemDetails';
  }

}
