<?php

/**
 * Tax form.
 *
 * @package    nisp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class TaxForm extends BaseTaxForm
{
  public function configure()
  {
      unset(
      $this['created_at'],$this['updated_at'],$this['id']
    );
    $this->validatorSchema['tax_name'] = new sfValidatorString(array('max_length' => 40, 'required' => true),
                        array('invalid' => 'Tax Name is invalid.', 'max_length' => 'Tax Name can not be more than 40 characters.', 'required' => 'Tax Name is required'));
    $this->validatorSchema['description'] = new sfValidatorString(array('max_length' => 255, 'required' => true),
                        array('invalid' => 'Description is invalid.', 'max_length' => 'Description can not be more than 255 characters.', 'required' => 'Description is required'));
    


  }
}
