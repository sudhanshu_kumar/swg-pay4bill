<?php

/**
 * AttributeMaster form.
 *
 * @package    nisp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class AttributeMasterForm extends BaseAttributeMasterForm {

    public function configure() {
        unset($this['created_at'], $this['updated_at'], $this['object_type_id'], $this['is_mandatory'], $this['is_unique'], $this['is_active']);
        $this->setWidget('att_name', new sfWidgetFormInputText(array(), array( 'name' => 'attr_name_' . $this->getOption('count'))));
        $this->setWidget('att_type', new sfWidgetFormChoice(array('choices' => array('integer' => 'Integer', 'string' => 'String'), 'expanded' => true), array('id' => 'attr_type_id_' . $this->getOption('count'), 'name' => 'attr_type_' . $this->getOption('count'))));
        $this->setWidget('merchant_id',new sfWidgetFormInputHidden(array('default'=>$this->getOption('merchant-id')), array()));
    }

}
