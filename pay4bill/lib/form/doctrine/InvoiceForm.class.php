<?php

/**
 * Invoice form.
 *
 * @package    nisp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class InvoiceForm extends BaseInvoiceForm {

    public function configure() {
        /*         * ---------------------Invoice number generate Start---------------------------------- */
        if ($_SESSION['logedin_user_group'] == 'teller') { // added by santosh
            $merchantInfo = $this->getOptions();
            $merchantName = $merchantInfo['merchant_name'];
            $merchantId = $merchantInfo['merchant_id'];
            $customerId = $merchantInfo['customer_id'];
            unset($this['total_amount'], $this['payment_mode'], $this['id'], $this['amount_paid'], $this['amount_due'], $this['status'], $this['created_at'], $this['updated_at'], $this['tax_id']);
        } else {
            $merchantName = sfContext::getInstance()->getUser()->getGuardUser()->MerchantUser->getMerchantName();
            $merchantId = sfContext::getInstance()->getUser()->getGuardUser()->MerchantUser->getId();
            unset($this['total_amount'], $this['payment_mode'], $this['id'], $this['merchant_id'], $this['amount_paid'], $this['amount_due'], $this['status'], $this['created_at'], $this['updated_at'], $this['tax_id']);
        }
        // comment the line for fixing the production issue of duplicate invoice number
//        $invoiceNo = pay4billHelper::getInvoiceNo($merchantId, $merchantName);

        /*         * ---------------------Invoice number generate End---------------------------------- */


        //Issue Date
        $this->widgetSchema['issue_date'] = new sfWidgetFormDateCalInput(array('years' => DateHelper::getDateRanges(), 'format' => '%day% %month% %year%'), array());
        $this->widgetSchema['issue_date']->setLabel('Issue Date<span class="redLbl">*</span> ');
        $this->validatorSchema['issue_date']->setMessage('required', 'Please select the Issue Date');
        //End of Issue Date

        if ($_SESSION['logedin_user_group'] == 'teller') { // added by santosh
            $this->widgetSchema['customer_id'] = new sfWidgetFormDoctrineChoice(array('model' => 'CustomerProfile', 'query' => CustomerProfileTable::getCustomers_for_merchant($merchantId, $customerId, null)));
            $this->widgetSchema['merchant_id'] = new sfWidgetFormInputHidden();
            $this->setDefault('merchant_id', $merchantId);
            $this->validatorSchema['merchant_id'] = new sfValidatorInteger(array('required' => false));

            // Validation for customer Id in case at the time of creation of invoice, someone deactivated the customer.
            $this->validatorSchema['customer_id'] = new sfValidatorDoctrineChoice(array('model' => 'CustomerProfile', 'query' => CustomerProfileTable::getCustomers_for_merchant($merchantId, $customerId, null), 'required' => true));
        } else {
            $this->widgetSchema['customer_id'] = new sfWidgetFormDoctrineChoice(array('model' => 'CustomerProfile', 'add_empty' => 'Please Select', 'query' => CustomerProfileTable::getCustomers_for_merchant(sfContext::getInstance()->getUser()->getGuardUser()->MerchantUser->getId(), null, null)));
        }
        $this->validatorSchema['customer_id']->setMessage('required', 'Please select the customer');

        $this->widgetSchema['customer_id']->setLabel('Customer<span class="redLbl">*</span> ');
        $this->widgetSchema['due_date'] = new sfWidgetFormDateCalInput(array('years' => DateHelper::getDateRanges(), 'format' => '%day% %month% %year%'), array());
        $this->widgetSchema['due_date']->setLabel('Due Date<span class="redLbl">*</span> ');
        $this->widgetSchema['delivery_date'] = new sfWidgetFormDateCalInput(array('years' => DateHelper::getDateRanges(), 'format' => '%day% %month% %year%'), array());

        $this->validatorSchema['delivery_date']->setMessage('required', 'Please select the Delivery Date');
        $this->validatorSchema['due_date']->setMessage('required', 'Please select the Due Date');

        $this->widgetSchema['currency_type'] = new sfWidgetFormChoice(array(
                    'choices' => array('naira' => 'Naira')
                ));


        $this->widgetSchema['invoice_no'] = new sfWidgetFormInputHidden();
        $this->validatorSchema['invoice_no'] = new sfValidatorString(array('required' => false), array());
        // comment the line for fixing the production issue of duplicate invoice number
//        if ($this->isNew()) {
//            $this->setDefault('invoice_no', $invoiceNo);
//        }

        $this->widgetSchema['additional_information']->setLabel('Additional Information<span class="redLbl">*</span> ');
        $this->validatorSchema['additional_information']->setMessage('required', 'Please enter the additional information');
        $this->validatorSchema['additional_information'] = new sfValidatorRegex(array('pattern' => '/^[a-zA-Z\s\.0-9-,]+$/', 'max_length' => 100,
                    'required' => true, 'trim' => true), array('max_length' => 'Not more Than 100 Characters', 'required' => 'Please enter Additional Information', 'invalid' => 'Please enter valid Additional Information'));
        $this->widgetSchema['currency_type']->setLabel('Currency Type<span class="redLbl">*</span> ');
        $this->widgetSchema['delivery_date']->setLabel('Delivery Date<span class="redLbl">*</span> ');

        $this->validatorSchema->setPostValidator(new sfValidatorAnd(array(
                    /* new sfValidatorSchemaCompare('delivery_date', sfValidatorSchemaCompare::GREATER_THAN_EQUAL, 'issue_date',
                      array(),
                      array('invalid' => 'Delivery Date should be greater than or equal to Issue Date')),
                      new sfValidatorSchemaCompare('due_date', sfValidatorSchemaCompare::GREATER_THAN_EQUAL, 'delivery_date',
                      array(),
                      array('invalid' => 'Due Date should be greater than or equal to Delivery Date')), */
                    new sfValidatorCallback(array('callback' => array($this, 'checkFutureDate')), array()),
                    new sfValidatorCallback(array('callback' => array($this, 'checkDeliveryDateOption')), array()),
                    new sfValidatorCallback(array('callback' => array($this, 'checkDueDateOption')), array()),
//                  new sfValidatorDoctrineUnique(array('model' => 'Invoice', 'column' => array('invoice_no')), array('invalid' => 'Invoice Number Already Exists'))
                )));
    }

    public function checkDeliveryDateOption($validator, $values) {

        $today = strtotime(date("Y/n/d "));
        $delivery_date = strtotime($values['delivery_date']);

        if ((strtotime($values['delivery_date']) < $today) && ($values['delivery_date'] != '')) {
            $error['delivery_date'] = new sfValidatorError($validator, 'Delivery Date cannot be Past Date');
        } else if (($delivery_date < strtotime($values['issue_date'])) && $values['delivery_date'] != '' && $values['issue_date'] != '') {
            $error['delivery_date'] = new sfValidatorError($validator, 'Delivery Date should be greater than or equal to Issue Date');
        }
        if (!empty($error))
            throw new sfValidatorErrorSchema($validator, $error);
        return $values;
    }

    public function checkDueDateOption($validator, $values) {

        $today = strtotime(date("Y/n/d "));
        $due_date = strtotime($values['due_date']);

        if ((strtotime($values['due_date']) < $today) && ($values['due_date'] != '')) {
            $error['due_date'] = new sfValidatorError($validator, 'Due Date cannot be Past Date');
        } else if (($due_date < strtotime($values['delivery_date'])) && $values['due_date'] != '' && $values['delivery_date'] != '') {
            $error['due_date'] = new sfValidatorError($validator, 'Due Date should be greater than or equal to Delivery Date');
        }
        if (!empty($error))
            throw new sfValidatorErrorSchema($validator, $error);
        return $values;
    }

    public function checkFutureDate($validator, $values) {

        $today = strtotime(date("Y/n/d "));
        if ((strtotime($values['issue_date']) < $today) && $values['issue_date'] != '') {
            $error['issue_date'] = new sfValidatorError($validator, 'Issue Date cannot be Past Date');
        }
//        if ((strtotime($values['due_date']) < $today)&& ($values['due_date'] !='')){
//            $error['due_date'] = new sfValidatorError($validator, 'Due Date cannot be Past Date');
//        }
        if (!empty($error))
            throw new sfValidatorErrorSchema($validator, $error);
        return $values;
    }

    public function updateObject($values = null) {

        $ob = Doctrine::getTable('sfGuardUser')->findBy('username', sfContext::getInstance()->getUser()->getUsername())->getFirst();
        $this->getObject()->setMerchantId($ob->MerchantUser->getId());
        $this->getObject()->setStatus(0);
        $this->getObject()->setAmountPaid(0);
        $this->getObject()->setAmountDue(0);
        parent::updateObject($values);
    }

}

