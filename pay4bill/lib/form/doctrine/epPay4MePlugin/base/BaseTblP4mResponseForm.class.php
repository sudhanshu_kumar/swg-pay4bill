<?php

/**
 * TblP4mResponse form base class.
 *
 * @method TblP4mResponse getObject() Returns the current form's model object
 *
 * @package    nisp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseTblP4mResponseForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                  => new sfWidgetFormInputHidden(),
      'request_id'          => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TblP4mRequest'), 'add_empty' => true)),
      'payment_status'      => new sfWidgetFormChoice(array('choices' => array(0 => '0', 1 => '1'))),
      'item_number'         => new sfWidgetFormInputText(),
      'transaction_number'  => new sfWidgetFormInputText(),
      'payment_mode'        => new sfWidgetFormInputText(),
      'validation_number'   => new sfWidgetFormInputText(),
      'total_amount'        => new sfWidgetFormInputText(),
      'currency'            => new sfWidgetFormInputText(),
      'payment_date'        => new sfWidgetFormDateTime(),
      'payment_description' => new sfWidgetFormInputText(),
      'created_at'          => new sfWidgetFormDateTime(),
      'updated_at'          => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                  => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'request_id'          => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('TblP4mRequest'), 'required' => false)),
      'payment_status'      => new sfValidatorChoice(array('choices' => array(0 => '0', 1 => '1'), 'required' => false)),
      'item_number'         => new sfValidatorInteger(),
      'transaction_number'  => new sfValidatorString(array('max_length' => 30)),
      'payment_mode'        => new sfValidatorString(array('max_length' => 150, 'required' => false)),
      'validation_number'   => new sfValidatorString(array('max_length' => 30)),
      'total_amount'        => new sfValidatorNumber(array('required' => false)),
      'currency'            => new sfValidatorString(array('max_length' => 30)),
      'payment_date'        => new sfValidatorDateTime(array('required' => false)),
      'payment_description' => new sfValidatorPass(array('required' => false)),
      'created_at'          => new sfValidatorDateTime(),
      'updated_at'          => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('tbl_p4m_response[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'TblP4mResponse';
  }

}
