<?php

/**
 * EpP4mResponse form base class.
 *
 * @method EpP4mResponse getObject() Returns the current form's model object
 *
 * @package    nisp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseEpP4mResponseForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                  => new sfWidgetFormInputHidden(),
      'request_id'          => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EpP4mRequest'), 'add_empty' => false)),
      'payment_status'      => new sfWidgetFormChoice(array('choices' => array(0 => '0', 1 => '1'))),
      'payment_mode'        => new sfWidgetFormInputText(),
      'validation_number'   => new sfWidgetFormInputText(),
      'total_amount'        => new sfWidgetFormInputText(),
      'currency'            => new sfWidgetFormInputText(),
      'payment_date'        => new sfWidgetFormDateTime(),
      'bank_name'           => new sfWidgetFormInputText(),
      'branch_name'         => new sfWidgetFormInputText(),
      'payment_description' => new sfWidgetFormInputText(),
      'created_at'          => new sfWidgetFormDateTime(),
      'updated_at'          => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                  => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'request_id'          => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('EpP4mRequest'))),
      'payment_status'      => new sfValidatorChoice(array('choices' => array(0 => '0', 1 => '1'), 'required' => false)),
      'payment_mode'        => new sfValidatorString(array('max_length' => 150)),
      'validation_number'   => new sfValidatorString(array('max_length' => 30)),
      'total_amount'        => new sfValidatorNumber(array('required' => false)),
      'currency'            => new sfValidatorString(array('max_length' => 30)),
      'payment_date'        => new sfValidatorDateTime(array('required' => false)),
      'bank_name'           => new sfValidatorString(array('max_length' => 50)),
      'branch_name'         => new sfValidatorString(array('max_length' => 50)),
      'payment_description' => new sfValidatorPass(array('required' => false)),
      'created_at'          => new sfValidatorDateTime(),
      'updated_at'          => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('ep_p4m_response[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpP4mResponse';
  }

}
