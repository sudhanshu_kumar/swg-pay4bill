<?php

/**
 * TblP4mRequest form base class.
 *
 * @method TblP4mRequest getObject() Returns the current form's model object
 *
 * @package    nisp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseTblP4mRequestForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                  => new sfWidgetFormInputHidden(),
      'item_number'         => new sfWidgetFormInputText(),
      'transaction_number'  => new sfWidgetFormInputText(),
      'payment_type'        => new sfWidgetFormInputText(),
      'buyer_ip_address'    => new sfWidgetFormInputText(),
      'payment_description' => new sfWidgetFormInputText(),
      'total_amount'        => new sfWidgetFormInputText(),
      'created_at'          => new sfWidgetFormDateTime(),
      'updated_at'          => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                  => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'item_number'         => new sfValidatorInteger(),
      'transaction_number'  => new sfValidatorString(array('max_length' => 30)),
      'payment_type'        => new sfValidatorInteger(),
      'buyer_ip_address'    => new sfValidatorString(array('max_length' => 255)),
      'payment_description' => new sfValidatorString(array('max_length' => 255)),
      'total_amount'        => new sfValidatorNumber(array('required' => false)),
      'created_at'          => new sfValidatorDateTime(),
      'updated_at'          => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('tbl_p4m_request[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'TblP4mRequest';
  }

}
