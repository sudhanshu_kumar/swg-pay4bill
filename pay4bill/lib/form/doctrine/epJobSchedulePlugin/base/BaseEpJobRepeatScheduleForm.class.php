<?php

/**
 * EpJobRepeatSchedule form base class.
 *
 * @method EpJobRepeatSchedule getObject() Returns the current form's model object
 *
 * @package    nisp
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseEpJobRepeatScheduleForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'           => new sfWidgetFormInputHidden(),
      'job_id'       => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('EpJob'), 'add_empty' => true)),
      'minutes'      => new sfWidgetFormInputText(),
      'hours'        => new sfWidgetFormInputText(),
      'day_of_month' => new sfWidgetFormInputText(),
      'month'        => new sfWidgetFormInputText(),
      'day_of_week'  => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'           => new sfValidatorDoctrineChoice(array('model' => $this->getModelName(), 'column' => 'id', 'required' => false)),
      'job_id'       => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('EpJob'), 'required' => false)),
      'minutes'      => new sfValidatorString(array('max_length' => 200, 'required' => false)),
      'hours'        => new sfValidatorString(array('max_length' => 200, 'required' => false)),
      'day_of_month' => new sfValidatorString(array('max_length' => 200, 'required' => false)),
      'month'        => new sfValidatorString(array('max_length' => 200, 'required' => false)),
      'day_of_week'  => new sfValidatorString(array('max_length' => 200, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('ep_job_repeat_schedule[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'EpJobRepeatSchedule';
  }

}
