<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MerchantRegistrationForm
 *
 * @author amalhotra
 */
class DynamicAttrFormOnCustomerEdit extends sfForm {

    public function configure() {
       
        foreach ($this->getOption('configuration') as $widgetName => $widgetDetails) {

            $required = $widgetDetails['validator']['is_mandatory'] == 1 ? true : false;
            
            if ($required) {
                $label = $widgetDetails['label'] . '<span class="redLbl">*</span>';
            } else {
                $label = $widgetDetails['label'];
            }

            $this->setWidget($widgetName, new sfWidgetFormInput(array('label' => $label), array('id' => $widgetName . "_id")));
            if (is_array($widgetDetails) && array_key_exists('val', $widgetDetails)) {
                $this->setDefault($widgetName, $widgetDetails['val']);
            }
            $this->setValidator($widgetName, new sfValidatorRegex(array('required' => $required, 'trim' => true, 'max_length' => 50, 'pattern' => pay4billHelper::regexPatternforAttType($widgetDetails['validator']['att_type'])),
                            array('invalid' => pay4billHelper::validationMsgforAttType($widgetDetails['validator']['att_type']), 'max_length' => $widgetDetails['label'] . ' can\'t be more than 50 characters', 'required' => 'Please enter ' . $widgetDetails['label'])));
        }


        $this->validatorSchema->setPostValidator(
                new sfValidatorCallback(array
                    ('callback' => array($this, 'isAttValueUniqueForCustomer')))
        );
        $this->validatorSchema->setOption('allow_extra_fields', true);
    }

    public function isAttValueUniqueForCustomer($validator, $values) {
        foreach ($values as $attName => $attVal) {
            if(strlen($attVal) > 0){
                $attribute_master_obj = Doctrine::getTable('AttributeMaster')->findByAttName($attName)->getFirst();
                $newValue = $attVal;
                if ($attribute_master_obj->getIsUnique() == 1) {
                    $attribute_master_id = $attribute_master_obj->getId();

                    $unique = Doctrine::getTable('AttributeValue')->checkIfUniqueValueOfAttribute($this->getOption('object_id'), $attribute_master_id, $attVal);
                    if (!$unique) {
                        $error[$attName] = new sfValidatorError($validator, "<b>" . $newValue . "</b> value of <i>" . $attribute_master_obj->getAttLabel() . "</i> already exists");
                    }
                }
            }
        }
        if (!empty($error))
            throw new sfValidatorErrorSchema($validator, $error);
        return $values;
    }

}

?>
