<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MerchantRegistrationForm
 *
 * @author amalhotra
 */
class TellerCreationForm extends sfForm {

    var $arr_restricted_users;

    public function configure() {
//        echo sfContext::getInstance()->getUser()->getAttribute('existing_email_address');

        $this->arr_restricted_users = sfConfig::get('app_ignore_users');

        $this->setWidget('name', new sfWidgetFormInputText(array('label' => "Name"), array()));
        $this->setWidget('email_address', new sfWidgetFormInputText(array('label' => "Email Address"), array()));
        $this->setWidget('sex', new sfWidgetFormChoice(array('choices' => array("" => 'Please Select', 'male' => 'Male', 'female' => 'Female')), array()));
        $this->setWidget('address', new sfWidgetFormTextarea(array(), array('style' => 'height:80px;width:192px;margin-bottom:10px')));
        $this->setWidget('bankname', new sfWidgetFormInputText(array('label' => "Bank Name"), array()));
        $this->setWidget('phone_number', new sfWidgetFormInputText(array('label' => "Phone Number"), array()));

        $this->setWidget('merchants', new sfWidgetFormDoctrineChoice(array('model' => 'MerchantProfile', 'table_method' => 'getBillerMerchantWithServiceCodeKey', 'add_empty' => 'Please select Merchant', 'multiple' => true), array()));


        $this->disableCSRFProtection();
        $this->validatorSchema['name'] = new sfValidatorRegex(array('pattern' => '/^[a-zA-Z\s\.]+$/', 'max_length' => 50, 'min_length' => 3,
                    'required' => true, 'trim' => true), array('max_length' => 'Name can\'t be more than 50 characters',
                    'min_length' => 'Name can\'t be less than 3 characters', 'required' => 'Please enter Name', 'invalid' => 'Please enter valid Name'));

        $this->validatorSchema['email_address'] = new sfValidatorEmail(array('required' => true,'max_length'=>70), array('required' => 'Please enter Email Address', 'invalid' => 'Please enter valid  Email Address', 'max_length'=>'Email address cannot be more than 70 characters.'));
        $this->validatorSchema['sex'] = new sfValidatorString(array('required' => true), array('required' => 'Please select Sex'));
        $this->validatorSchema['address'] = new sfValidatorString(array('required' => true, 'max_length' => 255, 'trim' => true), array('required' => 'Please enter Address', 'max_length' => 'Address can\'t be more than 255 characters'));
        $this->validatorSchema['bankname'] = new sfValidatorRegex(array('pattern' => '/^[a-zA-Z\s\.]+$/', 'max_length' => 30, 'min_length' => 3,
                    'required' => true, 'trim' => true), array('max_length' => 'Bank Name can\'t be more than 30 characters',
                    'min_length' => 'Bank Name can\'t be less than 3 characters', 'required' => 'Please enter Bank Name', 'invalid' => 'Please enter valid Bank Name'));
        $this->validatorSchema['phone_number'] = new sfValidatorRegex(array('pattern' => '/^[+]{0,1}[0-9-]{6,20}$/', 'max_length' => 20, 'min_length' => 6,
                    'required' => true), array('max_length' => 'Phone Number can not  be more than 20 digits.',
                    'min_length' => 'Phone Number can\'t be less than 6 digits', 'required' => 'Please enter Phone Number', 'invalid' => 'Please enter valid Phone Number'));
        $this->validatorSchema['merchants'] = new sfValidatorDoctrineChoice(array('model' => 'MerchantProfile', 'multiple' => true, 'required' => true), array('invalid' => 'Please select at least one Merchant', 'required' => 'Please Select at least one Merchant'));
        if (count($this->getOptions()) == 0) {
            $this->setWidget('password', new sfWidgetFormInputPassword(array(), array()));
            $this->setWidget('username', new sfWidgetFormInputText(array(), array()));
            $this->widgetSchema['confirm_password'] = new sfWidgetFormInputPassword();
            $this->widgetSchema['confirm_password']->setLabel('Confirm Password<span class="redLbl">*</span>');

//            $this->validatorSchema['username'] = new sfValidatorString(array('required' => true, 'min_length' => '6', 'max_length' => '70', 'trim' => true), array('required' => 'Please enter Username', 'min_length' => "Username can't be less than 6 characters", 'max_length' => "Username can't be more than 70 characters"));


$this->validatorSchema['username'] = new sfValidatorRegex(array('pattern' => '/^[-_a-zA-Z0-9\.\@]+$/', 'max_length' => 70, 'min_length' => 6,
                    'required' => true, 'trim' => true), array('required' => 'Please enter Username', 'max_length' => 'Username can\'t be more than 70 characters',
                    'min_length' => 'Username can\'t be less than 6 characters','invalid' => 'Please enter valid Username'));


            $this->validatorSchema['password'] = new sfValidatorString(array('max_length' => 20, 'min_length' => 6),
                            array('min_length' => 'Password can\'t be less than 6 characters', 'max_length' => 'Password can\'t be more than 20 characters',
                                'required' => 'Please enter Password'));

            $this->validatorSchema['confirm_password'] = new sfValidatorString(array('max_length' => 20, 'min_length' => 6),
                            array('min_length' => 'Password can\'t be less than 6 characters', 'max_length' => 'Password can\'t be more than 20 characters',
                                'required' => 'Please enter Confirm Password'));

            $r = $this->validatorSchema->setPostValidator(
                            new sfValidatorSchemaCompare('password', sfValidatorSchemaCompare::EQUAL, 'confirm_password',
                                    array(),
                                    array('invalid' => "Password and Confirm Password do not match")));


            $this->mergePostValidator(new sfValidatorCallback(array
                        ('callback' => array($this, 'isIgnoreUsers'))));

            $this->mergePostValidator(new sfValidatorCallback(array
                        ('callback' => array($this, 'passwordCheckInsecurity'))));
        } else {
            $this->getWidget('name')->setDefault($this->getOption('name'));
            $this->getWidget('sex')->setDefault($this->getOption('sex'));
            $this->getWidget('address')->setDefault($this->getOption('address'));
            $this->getWidget('email_address')->setDefault($this->getOption('email_address'));
            $this->getWidget('phone_number')->setDefault($this->getOption('phone_number'));
            $this->getWidget('bankname')->setDefault($this->getOption('bankname'));
            $this->getWidget('merchants')->setDefault($this->getOption('merchants'));
        }
 $this->validatorSchema->setPostValidator(new sfValidatorAnd(array(
                        new sfValidatorCallback(array('callback' => array($this, 'checkUsernamrAvailablity'))))));
        $this->validatorSchema->setOption('allow_extra_fields', true);
    }

    public function checkUsernamrAvailablity($validator, $values) {

        if (array_key_exists('username', $values) && $values['username'] != "") {
            $collectionObj = Doctrine::getTable('sfGuardUser')->findBy('username', $values['username']);
            if ($collectionObj->count() > 0) {
                $error['username'] = new sfValidatorError($validator, 'Username Already Exists');
            }
        }
        if (array_key_exists('email_address', $values) && $values['email_address'] != "") {

            $collectionObj = Doctrine::getTable('sfGuardUser')->findBy('email_address', $values['email_address']);

            $email = sfContext::getInstance()->getUser()->hasAttribute('existing_email_address')?sfContext::getInstance()->getUser()->getAttribute('existing_email_address'):NULL;

            if ($collectionObj->count() > 0 && $values['email_address'] <> $email) {
                $error['email_address'] = new sfValidatorError($validator, 'Email Address already exists');
            }
        }
        if (array_key_exists('password', $values) && $values['password'] != "" && $values['confirm_password'] != "") {
            if ($values['password'] != $values['confirm_password']) {
                $error['confirm_password'] = new sfValidatorError($validator, "Password and Confirm Password do not match");
            }
        }
        if (!empty($error))
            throw new sfValidatorErrorSchema($validator, $error);
        return $values;
    }

    /*
     * @Saurabh on 12 July
     * To check the user security at the time of given username by Admin to teller
     * Bug#35504
     */

    public function isIgnoreUsers($validator, $values) {

        if (pay4billHelper::isUserNameIgnore($values['username'], $this->arr_restricted_users)) {
            $error['username'] = new sfValidatorError($validator, 'Restricted username. Please enter another username.');
        }
        if (!empty($error))
            throw new sfValidatorErrorSchema($validator, $error);
        return $values;
    }

    /*
     * @Saurabh on 12 July
     * To check the user security at the time of given password
     * Bug#35504
     */

    public function passwordCheckInsecurity($validator, $values) {

        if (strlen($values['password']) > 0 && pay4billHelper::isPasswordInSecure($values['password'])) {
            $error['password'] = new sfValidatorError($validator, 'Password must contain atleast 1 alphabet, 1 specialchar and 1 number.');
        }
        if (!empty($error))
            throw new sfValidatorErrorSchema($validator, $error);
        return $values;
    }

}

?>
