<?php
class ResetUserForm extends sfform {
    public function configure() {

     
        
        $this->widgetSchema['username']      = new sfWidgetFormInputText(array(),array('maxlength'=>70));
		$this->widgetSchema['username']->setAttribute('class','FormInput2');
        $this->validatorSchema['username']   = new sfValidatorAnd(array(
                                                          new sfValidatorString(array('required' => true)),
                                                          new sfValidatorRegex(array('pattern' => '/[^\s+]/'),array('invalid' => 'White spaces are not allowed.',))),
                                                            array('halt_on_error' => false,'required'=>true),array('required'=>'User name is required'));

//       $this->widgetSchema['email_address'] = new sfWidgetFormInputText(array(),array('title '=>'Email Address','maxlength'=>50));
//       $this->validatorSchema['email_address'] =          new sfValidatorAnd(array(
//                                                          new sfValidatorString(array('max_length' => 50,'required' => true),array('required'=>'Eamil Address is required')),
//                                                          new sfValidatorRegex(array('pattern' => '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/'),array('invalid' => 'Invalid email address.',))    ),
//                                                            array('halt_on_error' => true,'required' => true),array('required'=>'Email address is required'));


        $this->widgetSchema->setLabels(array(
                'username'=>'Email Address<span class="redLbl">*</span>'
        ));
        $this->widgetSchema->setNameFormat('sf_guard_user[%s]');
        $this->disableCSRFProtection();
    }
}
