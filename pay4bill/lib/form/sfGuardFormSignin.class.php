<?php

/**
 * sfGuardFormSignin for sfGuardAuth signin action
 *
 * @package    sfDoctrineGuardPlugin
 * @subpackage form
 * @author     Fabien Potencier <fabien.potencier@symfony-project.com>
 * @version    SVN: $Id: sfGuardFormSignin.class.php 23536 2009-11-02 21:41:21Z Kris.Wallsmith $
 */
class sfGuardFormSignin extends BasesfGuardFormSignin {

    /**
     * @see sfForm
     */
    public function configure() {
        unset($this['remember']);
        $this->widgetSchema['username']->setLabel('Username<span class="redLbl">*</span>');

        //$this->widgetSchema['username']->se
        $this->validatorSchema['username'] = new sfValidatorString(array('required' => true),
                        array('required' => 'Please enter Username'));
        $this->validatorSchema['password'] = new sfValidatorString(array('required' => true),
                        array('required' => 'Please enter Password'));
        $this->widgetSchema['username']->setAttribute('class', 'FormInput1');
        $this->widgetSchema['password']->setAttribute('class', 'FormInput1');
        $this->widgetSchema['password']->setLabel('Password<span class="redLbl">*</span>');
    }

}
