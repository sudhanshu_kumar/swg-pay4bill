<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MerchantRegistrationForm
 *
 * @author amalhotra
 */
class AdminRegistrationForm extends PluginsfGuardUserForm {

    var $arr_restricted_users;
    
    public function configure() {
        
        $this->arr_restricted_users = sfConfig::get('app_ignore_users');
        
        
        $this->useFields(array('email_address', 'password','username','first_name'));
        $this->widgetSchema['email_address']->setLabel('Email Address<span class="redLbl">*</span> ');
        $this->widgetSchema['username']->setLabel('Username <span class="redLbl">*</span> ');
        $this->widgetSchema['first_name']->setLabel('Administrator Name<span class="redLbl">*</span> ');
        $this->widgetSchema['password'] = new sfWidgetFormInputPassword(array('label' => 'Password<span class="redLbl">*</span>'));
        $this->widgetSchema['password2'] = new sfWidgetFormInputPassword(array('label' => 'Confirm Password<span class="redLbl">*</span>'));
//        $customertProfile = new CustomerProfileForm($this->getObject()->getCustomerUser());
//        $this->embedForm("Details", $customertProfile);

        $this->validatorSchema['email_address'] = new sfValidatorEmail(array('max_length' => 70, 'required' => true),
                        array('invalid' => 'Email address is invalid.', 'max_length' => 'Email address can not be more than 70 characters.', 'required' => 'Email is required'));

        $this->validatorSchema['password'] = new sfValidatorString(array('max_length' => 20, 'required' => true), array('max_length' => 'Confirm password can not  be more than 20 characters.', 'required' => 'Password is required'));
        $this->validatorSchema['password2'] = new sfValidatorString(array('max_length' => 20, 'required' => true), array('max_length' => 'Confirm password can not  be more than 20 characters.', 'required' => 'Confirm password is required.'));
//        $this->validatorSchema['username'] = new sfValidatorString(array('max_length' => 70, 'required' => true), array('max_length' => 'Username can not  be more than 70 characters.', 'required' => 'Username is required'));
        $this->validatorSchema['username'] = new sfValidatorString(array('required' => true, 'min_length' => '6','max_length' => '20', 'trim'=>true), array('required' => 'Please enter Username', 'min_length' => "At least 6 Characters Required", 'max_length' => "More than 20 characters are not allowed"));
        $this->validatorSchema['first_name'] = new sfValidatorString(array('max_length' => 50, 'required' => true), array('max_length' => 'Name can not  be more than 20 characters.', 'required' => 'Name is required'));
        $this->validatorSchema->setPostValidator(
                new sfValidatorSchemaCompare('password', sfValidatorSchemaCompare::EQUAL, 'password2',
                        array(),
                        array('invalid' => 'Password do not match, please try again')
        ));
        $this->validatorSchema->setPostValidator(new sfValidatorDoctrineUnique(array('model' => 'sfGuardUser', 'column' => array('email_address')), array('invalid' => 'Email Address already registered.')));
        $this->validatorSchema->setOption('allow_extra_fields', true);
        
        $this->mergePostValidator(new sfValidatorCallback(array
                    ('callback' => array($this, 'isIgnoreUsers'))));
        
        $this->mergePostValidator(new sfValidatorCallback(array
                    ('callback' => array($this, 'passwordCheckInsecurity'))));
        
    }

    public function updateObject($values = null) {
        $values = $this->getValues();
        $this->getObject()->setUsername($values['email_address']);
        //Remove Merchant Id from customer table
//        $ob = Doctrine::getTable('sfGuardUser')->findBy('username', sfContext::getInstance()->getUser()->getUsername())->getFirst();
//        $this->getObject()->getCustomerUser()->setMerchantId($ob->MerchantUser->getId());
//        $this->getObject()->getCustomerUser()->setAddress($values['Details']['address']);
//        $this->getObject()->getCustomerUser()->setSex($values['Details']['sex']);
//        $this->getObject()->getCustomerUser()->setPhoneNumber($values['Details']['phone_number']);

        $this->validatorSchema->setOption("allow_extra_fields", true);
        parent::updateObject($values);
    }
    /* 
     * @Saurabh on 12 July 
     * To check the user security at the time of given username by Admin to sub admin
     * Bug#35504
     */
    
    public function isIgnoreUsers($validator, $values) {
       
       if (pay4billHelper::isUserNameIgnore($values['username'],$this->arr_restricted_users)) {
            $error['username'] = new sfValidatorError($validator, 'Restricted username. Please enter another username.');
       }
       if (!empty($error))
            throw new sfValidatorErrorSchema($validator, $error);
        return $values;
    }
    /* 
     * @Saurabh on 12 July 
     * To check the user security at the time of given password
     * Bug#35504
     */
    
    public function passwordCheckInsecurity($validator, $values) {
       
        if (strlen($values['password']) > 0 && pay4billHelper::isPasswordInSecure($values['password'])) {
            $error['password'] = new sfValidatorError($validator, 'Password must contain atleast 1 alphabet, 1 specialchar and 1 number.');
        }
       if (!empty($error))
            throw new sfValidatorErrorSchema($validator, $error);
        return $values;
    }

}

?>
