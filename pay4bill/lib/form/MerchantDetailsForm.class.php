<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MerchantRegistrationForm
 *
 * @author amalhotra
 */
class MerchantDetailsForm extends PluginsfGuardUserForm {

    public function configure() {
        $this->useFields(array('email_address'));
        $merchantProfile = new MerchantProfileForm($this->getObject()->getMerchantUser(), array('valid_cac' => $this->getOption('valid_cac')));
        $this->validatorSchema['email_address'] = new sfValidatorEmail(array('max_length' => 70, 'required' => true),
                        array('invalid' => 'Email address is invalid.', 'max_length' => 'Email address can not be more than 70 characters.', 'required' => 'Email is required'));
        $this->widgetSchema['email_address']->setLabel('Email Address<span class="redLbl">*</span> ');
        $this->embedForm("Profile", $merchantProfile);
        $this->validatorSchema->setOption('allow_extra_fields', true);
        $this->validatorSchema->setPostValidator(new sfValidatorDoctrineUnique(array('model' => 'sfGuardUser', 'column' => array('email_address')), array('invalid' => 'User already exists')));        
    }

    public function updateObject($values = null) {
        $values = $this->getValues();

        $this->getObject()->setUsername($values['email_address']);

        $this->getObject()->setPassword('password');
        $this->getObject()->setIsActive(0);
        $this->getObject()->getMerchantUser()->setTinyUrl(str_replace(" ", "-", strtolower($values['Profile']['merchant_name'])));

        parent::updateObject($values);
    }

}

?>
