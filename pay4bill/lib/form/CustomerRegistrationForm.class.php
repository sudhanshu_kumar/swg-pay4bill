<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MerchantRegistrationForm
 *
 * @author amalhotra
 */
class CustomerRegistrationForm extends PluginsfGuardUserForm {

    public function configure() {

        $this->useFields(array('email_address', 'password'));

        /*
         * @Saurabh - 13 June
         * Dynamic attribute field addition
         * with validation
         *
         */

        $attribute_master_name = '';
        if (count($this->getOptions()) > 0) {
           
            foreach ($this->getOptions() as $k => $v) {

                /* Takes the value if the customer is allowed to login */
//               echo "<pre>";
//               print_r($v);
//               exit;
                if (count($v) > 0 && is_array($v)) {
                    foreach ($v as $key => $value) {
                        if ($k == 'email_address_mandatary') {
                            $email_address_mandatary = $value;
                        }
                        if ($k == 'misc' && $key == 'unique_field_id') {
                            $attribute_master_id = $value;
                        }
                        if ($k == 'misc' && $key == 'unique_field_name') {
                            $attribute_master_name = $value;
                        }
                        if ($key == 'att_name')
                            $arr[$k]['att_name'] = $value;
                        if ($key == 'att_type')
                            $arr[$k]['att_type'] = $value;
                        if ($key == 'is_mandatory')
                            $arr[$k]['is_mandatory'] = $value;
                        if ($key == 'is_unique')
                            $arr[$k]['is_unique'] = $value;
                        if ($key == 'att_value')
                            $arr[$k]['att_value'] = $value;
                    }
                }
            }
        }

        if (isset($arr) && count($arr) > 0) {
            foreach ($arr as $va) {
                $va = array_reverse($va, true);
                if (count($va) > 0) {
                    foreach ($va as $k => $v) {
                        if ($k == 'att_type')
                            $attype = $v;
                        if ($k == 'is_mandatory')
                            $mandat = $v;
                        if ($k == 'att_value')
                            $attval = $v;

                        if ($k == 'att_name') {
                            $t = $v; // Will be used if the field are having spaces ni between: For Labeling only
                            $v = str_replace(" ", "", $v);

                            $this->widgetSchema[$v] = new sfWidgetFormInputText(array('label' => $t . '<span class="redLbl">*</span>'));

                            if (isset($attval) && $attval <> '')
                                $this->setDefault($v, $attval);
                            // $this->widgetSchema[$v] =  new sfWidgetFormInputText(array(),array('value'=>$attval));

                            if (strtolower($attype) == 'integer') {
                                if ($mandat == 1) {
                                    $this->validatorSchema[$v] = new sfValidatorRegex(array('required' => true, 'pattern' => '/^[0-9]*$/i'), array('required' => 'Please enter ' . $t, 'invalid' => pay4billHelper::validationMsgforAttType($attype)));
                                } else {
                                    $this->widgetSchema[$v]->setLabel($t . '<span class="redLbl"> </span>');
                                    $this->validatorSchema[$v] = new sfValidatorRegex(array('required' => false, 'pattern' => '/^[0-9]*$/i'), array('invalid' => pay4billHelper::validationMsgforAttType($attype)));
                                }
                            } else if (strtolower($attype) == 'string') {
                                if ($mandat == 1) {
                                    $this->validatorSchema[$v] = new sfValidatorRegex(array('required' => true, 'pattern' => '/^[a-z]*$/i'), array('required' => 'Please enter ' . $t, 'invalid' => pay4billHelper::validationMsgforAttType($attype)));
                                } else {
                                    $this->widgetSchema[$v]->setLabel($t . '<span class="redLbl"> </span>');
                                    $this->validatorSchema[$v] = new sfValidatorRegex(array('required' => false, 'pattern' => '/^[a-z]*$/i'), array('invalid' => pay4billHelper::validationMsgforAttType($attype)));
                                }
                            } else if (strtolower($attype) == 'alphanumeric') {
                                if ($mandat == 1) {
                                    $this->validatorSchema[$v] = new sfValidatorRegex(array('required' => true, 'pattern' => '/^[a-z0-9]*$/i'), array('required' => 'Please enter ' . $t, 'invalid' => pay4billHelper::validationMsgforAttType($attype)));
                                } else {
                                    $this->widgetSchema[$v]->setLabel($t . '<span class="redLbl"> </span>');
                                    $this->validatorSchema[$v] = new sfValidatorRegex(array('required' => false, 'pattern' => '/^[a-z0-9]*$/i'), array('invalid' => pay4billHelper::validationMsgforAttType($attype)));
                                }
                            }
                        }
                    }
                }
            }
        }
        /* end of ..13 June */

        if ($email_address_mandatary == 1) {
            $this->widgetSchema['email_address']->setLabel('Email Address<span class="redLbl">*</span> ');
            $this->validatorSchema['email_address'] = new sfValidatorEmail(array('max_length' => 70, 'required' => true),
                            array('invalid' => 'Email address is invalid', 'max_length' => 'Email address can not be more than 70 characters', 'required' => 'Please enter Email Address'));
        } else {
            $this->widgetSchema['email_address']->setLabel('Email Address<span class="redLbl"> </span> ');
            $this->validatorSchema['email_address'] = new sfValidatorEmail(array('max_length' => 70, 'required' => false),
                            array('invalid' => 'Please enter correct email address', 'max_length' => 'Email address can not be more than 70 characters', 'required' => 'Please enter Email Address'));
        }

        if ($this->getoption('email_address') != '') {
            $this->setDefault('email_address', $this->getoption('email_address'));
            $this->setDefault('', '');
        }

        /*
         * @Saurabh on 21 June
         * Set merchant id in hidden in case merchant coming from teller
         * by selecting the merchant fromm dropdown
         */
        $this->widgetSchema['merchant_id'] = new sfWidgetFormInputHidden();
        $this->setDefault('merchant_id', $this->getOption('merchant_id'));

        /*
         * @Saurabh - 14June12
         * Creating hidden fields for call back functions to acces
         */

        $this->widgetSchema['attribute_master_id'] = new sfWidgetFormInputHidden();
        $this->setDefault('attribute_master_id', $attribute_master_id);

        $this->validatorSchema['attribute_master_id'] = new sfValidatorInteger(array('required' => false));

        $this->widgetSchema['attribute_master_name'] = new sfWidgetFormInputHidden();
        $this->setDefault('attribute_master_name', $attribute_master_name);
        $this->validatorSchema['attribute_master_name'] = new sfValidatorString(array('required' => false));



        $this->widgetSchema['password'] = new sfWidgetFormInputPassword(array('label' => 'Password<span class="redLbl">*</span>'));
        $this->widgetSchema['password2'] = new sfWidgetFormInputPassword(array('label' => 'Confirm Password<span class="redLbl">*</span>'));

        // $customertProfile = new CustomerProfileForm($this->getObject()->getCustomerUser());

        /*
         * @Saurabh 20-Jun-2012
         * Pass array value to CustomerProfileForm to fill up the value on edit
         *
         */
        $customertProfile = new CustomerProfileForm($this->getObject()->getCustomerUser(), array('customer' => $this->getoption('customer'), 'id' => $this->getoption('id')));
        $this->embedForm("Details", $customertProfile);


//        $this->validatorSchema['password'] = new sfValidatorString(array('max_length' => 20, 'required' => true), array('max_length' => 'Password can not  be more than 20 characters', 'required' => 'Please enter Password'));
        $this->validatorSchema['password'] = new sfValidatorString(array('max_length' => 20, 'min_length' => 6),
                            array('max_length' => 'Password length must be between 6 and 20', 'min_length' => 'Password length must be between 6 and 20.',
                                'required' => 'Please enter Password'));
        $this->validatorSchema['password2'] = new sfValidatorString(array('max_length' => 20, 'required' => true), array('max_length' => 'Confirm password can not  be more than 20 characters', 'required' => 'Please enter Confirm password'));
        $this->validatorSchema->setPostValidator(
                new sfValidatorCallback(array
                    ('callback' => array($this, 'passwordNotMatched')))
        );

        if ($is_customer_login == 1) {
            $this->mergePostValidator(new sfValidatorDoctrineUnique(array('model' => 'sfGuardUser', 'column' => array('email_address')), array('invalid' => 'Email Address already registered')));
        }


        $this->mergePostValidator(new sfValidatorCallback(array
                    ('callback' => array($this, 'isAttValueUniqueForCustomer'))));

        $this->mergePostValidator(new sfValidatorCallback(array
                    ('callback' => array($this, 'passwordCheckInsecurity'))));

        $this->validatorSchema->setOption('allow_extra_fields', true);
    }

    /*
     * @Saurabh 14June12
     * To validate attribute value against a merchant
     * and that attribute value is unique or not
     */

    public function isAttValueUniqueForCustomer($validator, $values) {

        $attName = $values['attribute_master_name'];
        $attribute_master_id = $values['attribute_master_id'];

        if(isset($values[$attName]) &&  $values[$attName] != ''){
            $newValue = $values[$attName];

            $isExist = Doctrine::getTable('AttributeMaster')->isUniqueDataAlreadyExist($attribute_master_id, $newValue);

            if ($isExist > 0) {
                $error[$attName] = new sfValidatorError($validator, "<b>" . $newValue . "</b> value of ".$attName." already exists");
            }
        }

        if (!empty($error))
            throw new sfValidatorErrorSchema($validator, $error);
        return $values;
    }

    /*
     * @Saurabh 14June12
     * To validate attribute value against a merchant
     * and that attribute value is unique or not
     */

    public function passwordNotMatched($validator, $values) {
        $values = $this->getTaintedValues();
        $attName = $values['attribute_master_name'];
        $attribute_master_id = $values['attribute_master_id'];
        
        if(isset($values[$attName]) &&  $values[$attName]!= ''){
            $newValue = $values[$attName];

            $isExist = Doctrine::getTable('AttributeMaster')->isUniqueDataAlreadyExist($attribute_master_id, $newValue);

            if (($values['password'] != $values['password2']) && $values['password2'] != '') {
                $error['password2'] = new sfValidatorError($validator, 'Password do not match.');
            }
           }
      

        if (!empty($error))
            throw new sfValidatorErrorSchema($validator, $error);
        return $values;
    }
    
    /* 
     * @Saurabh on 13 July 
     * To check the user security at the time of given password
     * Bug#35504
     */
    
    public function passwordCheckInsecurity($validator, $values) {
       
          
        if (strlen($values['password']) >=6 &&  strlen($values['password']) <=20 && pay4billHelper::isPasswordInSecure($values['password'])) {
            $error['password'] = new sfValidatorError($validator, 'Password must contain atleast 1 alphabet, 1 specialchar and 1 number.');
        }
       if (!empty($error))
            throw new sfValidatorErrorSchema($validator, $error);
        return $values;
    }

    public function updateObject($values = null) {
        $values = $this->getTaintedValues();


        $this->getObject()->setUsername($values['username']);
        //Remove Merchant Id from customer table
//        $ob = Doctrine::getTable('sfGuardUser')->findBy('username', sfContext::getInstance()->getUser()->getUsername())->getFirst();
//        $this->getObject()->getCustomerUser()->setMerchantId($ob->MerchantUser->getId());
        $this->getObject()->getCustomerUser()->setAddress($values['Details']['address']);
        $this->getObject()->getCustomerUser()->setSex($values['Details']['sex']);
        $this->getObject()->getCustomerUser()->setPhoneNumber($values['Details']['phone_number']);

        $this->validatorSchema->setOption("allow_extra_fields", true);
        parent::updateObject($values);
    }

}

?>
