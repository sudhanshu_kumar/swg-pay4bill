<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MerchantRegistrationForm
 *
 * @author amalhotra
 */
class InvoiceSearchForm extends sfForm {

    public function configure() {
        $this->widgetSchema['cust_name'] = new sfWidgetFormInput(array(), array('maxlength' => 50, 'title' => 'Customer Name'));

        $this->validatorSchema['cust_name'] = new sfValidatorString(array('max_length' => 50), array('max_length' => 'Customer Name can not be more than 50 characters.'));


        $this->widgetSchema->setLabels(array('cust_name' => 'Customer Name',
        ));

        $this->widgetSchema['invoice_no'] = new sfWidgetFormInput(array(), array('maxlength' => 10, 'title' => 'Invoice Number'));

        $this->validatorSchema['invoice_no'] = new sfValidatorString(array('max_length' => 10), array('max_length' => 'Invoice Number can not be more than 10 characters.'));


        $this->widgetSchema->setLabels(array('invoice_no' => 'Invoice Number',
        ));

        /*
         * @Saurabh at 22June, 2012
         * add from and to in search
         */

        $this->widgetSchema['from'] = new sfWidgetFormDateCalInput(array('years' => DateHelper::getDateRanges(), 'format' => '%day% %month% %year%'), array());

        $this->widgetSchema['to'] = new sfWidgetFormDateCalInput(array('years' => DateHelper::getDateRanges(), 'format' => '%day% %month% %year%'), array());
        $this->widgetSchema->setLabels(array(
            'from' => 'From<font color=red>*</font>',
            'to' => 'To<font color=red>*</font>',
        ));
      
    }

}

?>
