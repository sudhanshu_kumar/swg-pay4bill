<?php

/**
 * CurrencyMaster form.
 *
 * @package    form
 * @subpackage CurrencyMaster
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 6174 2007-11-27 06:22:40Z fabien $
 */
class ChangeUserForm extends BasesfGuardUserForm {
    public static function userVerifyPasswordCallBack($validator, $value, $arguments) {
        // this is my logged in user
        $user = sfContext::getInstance()->getUser();
        // TODO verify if following usage of == is correct
        if ($user == null) {
            throw new sfValidatorError($validator, 'invalid');
        }
        // TODO handle not logged in user properly
        if (!$user->isAuthenticated()) {
            throw new sfValidatorError($validator, 'invalid');
        }
        if (!$user->checkPassword($value)) {
            // password don't match
            throw new sfValidatorError($validator, 'invalid');
        }
        return $value;
    }

    public function configure() {
        unset(
                $this['algorithm'],$this['salt'],$this['is_super_admin'],
                $this['last_login'],$this['updated_at'],$this['created_at'],
                $this['groups_list'],$this['permissions_list'],$this['first_name'],$this['last_name'],$this['email_address'],$this['mobile_no'],$this['employee_id'],$this['department_id'],
                $this['is_active'],$this['username'],$this['updated_by'],$this['created_by'],$this['role'],$this['department_id'],$this['division'],$this['unit'],$this['reg_committee_member']
        );

        $this->widgetSchema['old_password'] = new sfWidgetFormInputPassword();
        $this->widgetSchema['old_password']->setLabel('Old Password<span class="redLbl">*</span> ');
        $this->validatorSchema['old_password'] = new sfValidatorCallback(array(
                        'callback'  => 'ChangeUserForm::userVerifyPasswordCallBack'));
        $this->validatorSchema['old_password']->setOption('required',true);
        //$this->validatorSchema['old_password']->setMessage('required','Old password is required');
        $this->validatorSchema['old_password']->setMessage('required','Please enter old password');
        $this->validatorSchema['old_password']->setMessage('invalid','Old password is incorrect');

        //working with changing the passwords
        $this->widgetSchema['password']= new sfWidgetFormInputPassword();
//        $this->widgetSchema['password']->setLabel('New Password<span class="redLbl">*</span> ');
        $this->widgetSchema['confirm_password']= new sfWidgetFormInputPassword();
        $this->widgetSchema['confirm_password']->setLabel('Confirm New Password<span class="redLbl">*</span>');

        //Varun : Changes for maxlength validation
        
        $this->validatorSchema['password'] = new sfValidatorString(array('max_length' => 20,'min_length' => 6),
                array('max_length' => 'New password length must be between 6 and 20','min_length' => 'New password length must be between 6 and 20.',
                        'required' => 'Please enter new password'));

        $this->validatorSchema['confirm_password'] = new sfValidatorString(array('max_length' => 20,'min_length' => 6),
                array('max_length' => 'Confirm password length must be between 6 and 20.','min_length' => 'Confirm password length must be between 6 and 20.',
                        'required' => 'Please enter confirm new password'));

//  $this->validatorSchema->setPostValidator(new sfValidatorAnd(array(
//          new sfValidatorCallback(array('callback' => array($this, 'checkIssueDate'))),new sfValidatorCallback(array('callback' => array($this, 'checkMultiPleEntries'))))
//      ));
    $r = $this->validatorSchema->setPostValidator(
                new sfValidatorSchemaCompare('password', sfValidatorSchemaCompare::EQUAL, 'confirm_password',
                array(),
                array('invalid' => 'New password and confirm password should be same')
        ));

            $this->validatorSchema->setPostValidator(new sfValidatorAnd(array(
          new sfValidatorCallback(array('callback' => array($this, 'passwordValidate'))))));

            $this->mergePostValidator(new sfValidatorCallback(array
                    ('callback' => array($this, 'passwordCheckInsecurity'))));
            

        /* $this->mergePostValidator(
                new sfValidatorSchemaCompare('password', sfValidatorSchemaCompare::NOT_EQUAL, 'old_password',
                array(),
                array('invalid' => 'Old Password and New Password should not be same')
                )
        );*/
        
        $this->widgetSchema->setLabels(array_merge($this->widgetSchema->getLabels(),
                array('password'=>'New Password<span class="redLbl">*</span>')));

        $this->disableCSRFProtection();
    }

     public function passwordValidate($validator, $values) {

       if ($values['old_password'] == $values['password'] && ($values['password']!='' && $values['old_password']!='')) {
            $error = new sfValidatorError($validator, 'Old password and new password should not be same');
            throw new sfValidatorErrorSchema($validator,array('password'=>$error));
          }
       if ($values['confirm_password'] != $values['password'] && ($values['confirm_password']!='' && $values['password']!='')) {
            $error = new sfValidatorError($validator, 'New password and confirm new password should be the same');
            throw new sfValidatorErrorSchema($validator,array('password'=>$error));
          }
        return $values;
      }

     /* 
     * @Saurabh on 12 July 
     * To check the user security at the time of given password
     * Bug#35504
     */
    
    public function passwordCheckInsecurity($validator, $values) {
       
        if (strlen($values['password']) > 0 && pay4billHelper::isPasswordInSecure($values['password']) && ($values['old_password'] != $values['password']) && ($values['password'] == $values['confirm_password'])) {
            $error['password'] = new sfValidatorError($validator, 'Password must contain atleast 1 alphabet, 1 specialchar and 1 number.');
        }
       if (!empty($error))
            throw new sfValidatorErrorSchema($validator, $error);
        return $values;
    }


    public function save($conn = null) {
        sfContext::getInstance()->getUser()->setPassword($this->values['password']);
    }
}