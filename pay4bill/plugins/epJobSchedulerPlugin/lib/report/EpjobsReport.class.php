<?php

/**
 * Description of EpjobsStatus
 *
 * @author apundir
 */
class EpjobsReport {
  /* what format the report is required in */

  public static $FORMAT_CSV = 1;
  public static $FORMAT_HTML = 2;
  public static $FORMAT_TXT = 4;

  /* What is required in the report */
  public static $OPTION_FAILED = 1;
  public static $OPTION_PASSED = 2;
  public static $OPTION_SUMMARY = 4;
  public static $OPTION_DETAILED = 8;
  public static $OPTION_PASS_IN_MULTI_ATTEMPT = 16;
  public static $OPTION_SOURCE_JOB = 32;
  public static $OPTION_SOURCE_EXECUTION = 64;

  public function generateStatus($startTime, $endTime, $options=0, $format=0) {
    $statusFile = $this->generateStatusFile($startTime, $endTime, $options, $format);
    $status = file_get_contents($statusFile);
    unlink($statusFile);
    return $status;
  }

  public function generateStatusFile($startTime, $endTime, $options=0, $format=0, $file=null) {
    $execStartTime = time();
    if (!$file) {
      $file = tempnam(sys_get_temp_dir(), 'epjobs.report.');
    }
    if (!$format) {
      $format = self::$FORMAT_CSV;
    }
    $detailed = $options & self::$OPTION_DETAILED;
    $data = $this->getData($startTime, $endTime, $options, $detailed);
    $this->printStatusFile($file, $data, $format, $detailed, $execStartTime #
            , $startTime, $endTime, $options);
    return $file;
  }

  protected function getReportHeaderLines($repStart, $repEnd, $options) {
    $data = "Epjobs Report generated at " . date('d-M-Y h:ia T') . PHP_EOL;
    $data .= "From(Start): $repStart, To(End): $repEnd, Options: ";
    $optTxt = array();
    if ($options & self::$OPTION_PASSED) {
      $optTxt[] = ' Pass ';
    }
    if ($options & self::$OPTION_PASS_IN_MULTI_ATTEMPT) {
      $optTxt[] = ' Pass-multi ';
    }
    if ($options & self::$OPTION_FAILED) {
      $optTxt[] = ' Failed ';
    }
    $data .= join('|', $optTxt);
    return $data;
  }

  protected function printStatusFile($file, $data, $format, $detailed, #rest options for header only
          $execStartTime, $repStart, $repEnd, $options) {
    $formatter = AbstractEpjobsReportFormat::getReportFormatter($format, $file);
    $formatter->startReport($this->getReportHeaderLines($repStart, $repEnd, $options));
    $formatter->startSummaryTable("Execution Summary:");
    foreach ($data as $job) {
      $formatter->printSummaryLine($job);
    }
    $formatter->finishSummaryTable();
    //$formatter->finishSummaryTable();
    
    foreach ($data as $job) {
      $formatter->startParameterTable("Parameters for Job Id: ".$job->getId());
      $formatter->printParameters($job);
      $formatter->finishParameterTable();
      if ($detailed) {
        $executions = $job->getEpJobExecution();
        $totalExecutions = $executions->count();
        $currentIndex = 1;
        foreach ($executions as $execution) {
          $formatter->printDetailedData($execution, $currentIndex++, $totalExecutions);
        }
      }
    }
    $formatter->closeReport("Report Finished in " . (time() - $execStartTime) . " seconds.");
  }

  /**
   *
   * @param type $startTime
   * @param type $endTime
   * @param type $options
   * @return type 
   */
  protected function getData($startTime, $endTime, $options) {
    $jobQuery = Doctrine_Query::create()->from('EpJob jb');
    if (($options & self::$OPTION_PASS_IN_MULTI_ATTEMPT) && (!($options & self::$OPTION_PASSED))) {
      // we need to have more than one time execution status
      if ($options & self::$OPTION_FAILED) {
        $jobQuery->where('(jb.last_execution_status = ? and jb.execution_attempts > ?)
          or jb.last_execution_status = ?', array(EpJobConstants::$JOB_LAST_EXECUTION_STATE_PASS, 1,
            EpJobConstants::$JOB_LAST_EXECUTION_STATE_FAIL));
      } else {
        $jobQuery->where('jb.last_execution_status = ? and jb.execution_attempts > ?', array(
            EpJobConstants::$JOB_LAST_EXECUTION_STATE_PASS, 1));
      }
    } elseif ($options & self::$OPTION_PASSED) {
      if ($options & self::$OPTION_FAILED) {
        // we need both pass and failed 
        $jobQuery->where('jb.last_execution_status != ?', array(
            EpJobConstants::$JOB_LAST_EXECUTION_STATE_NOTEXECUTED));
      } else {
        // we need plain pass
        $jobQuery->where('jb.last_execution_status = ?', array(
            EpJobConstants::$JOB_LAST_EXECUTION_STATE_PASS));
      }
    } elseif ($options & self::$OPTION_FAILED) {
      // only failed jobs required !!
      $jobQuery->where('jb.last_execution_status = ?', array(
          EpJobConstants::$JOB_LAST_EXECUTION_STATE_FAIL));
    } else {
      // no status option is provided !!
      $jobQuery->where('jb.last_execution_status != ?', array(
          EpJobConstants::$JOB_LAST_EXECUTION_STATE_NOTEXECUTED));
    }
    if ($options & self::$OPTION_DETAILED) {
      $jobQuery->leftJoin('jb.EpJobExecution jbexe');
      $jobQuery->orderBy('jb.last_execution_status asc, jb.execution_attempts desc, jbexe.id desc');
      // TODO - create joins with job out data as well
    } else {
      $jobQuery->orderBy('jb.last_execution_status asc, jb.execution_attempts desc');
    }

    if ($options & self::$OPTION_SOURCE_EXECUTION) {
        if (!($options & self::$OPTION_DETAILED)) {
            $jobQuery->leftJoin('jb.EpJobExecution jbexe');
            $jobQuery->orderBy('jb.last_execution_status asc, jb.execution_attempts desc, jbexe.id desc');
        }
      $jobQuery->andWhere('jbexe.updated_at > ? and jbexe.updated_at < ?', array($startTime, $endTime));
    }
    else {
      $jobQuery->andWhere('jb.updated_at > ? and jb.updated_at < ?', array($startTime, $endTime));
    }

    $res = $jobQuery->execute();
    $jobQuery->free();
    return $res;
  }
}
