<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EpjobsReportCsv
 *
 * @author apundir
 */
class EpjobsReportCsv extends AbstractEpjobsReportFormat {

  protected function getCloseReportText($closingText=null) {
    if ($closingText) {
      return array($closingText);
    }
    return array();
  }

  protected function getDetailedHeaderText(EpJobExecution $execution, $index, $total) {
    $jobName = $execution->getEpJob()->getName();
    $startTime = strtotime($execution->getStartTime());
    $endTime = strtotime($execution->getEndTime());
    $spentSec = $endTime - $startTime;
    $spentTime = $this->sec2hms($spentSec);
    $data = array();
    $data[0] = array('Jobid', 'Execution ID', 'Name', 'Exit Code'
        , 'Started', 'Finished', 'Time Spent');
    $data[1] = array($execution->getJobId(), $execution->getId() #
        , $jobName, $execution->getExitCode(), $execution->getStartTime()
        , $execution->getEndTime(), $spentTime);
    return $data;
  }

  protected function getDetailedOutputText(EpJobData $jobData) {
    $type = "STDERR";
    if ($jobData->getOutputType() == 1) {
      $type = "STDOUT";
    }
    $data = array();
    $data[] = $type . ' Output:';
    $data[] = $jobData->getOutputText();
    return $data;
  }

  protected function getFinishSummaryTableText() {
    return array();
  }

  protected function getStartReportText($reportHeader) {
    return array($reportHeader);
  }

  protected function getStartSummaryTableText() {
    $data = array('Job-id', 'Name', 'URL', 'Current-State', 'LastExecReport', 'ExecAttempts');
    return $data;
  }

  protected function getSummaryHeaderText($summaryHeader) {
    if (!empty($summaryHeader)) {
      return array($summaryHeader);
    }
    return array();
  }

  protected function getSummaryLineText(EpJob $job) {
    $data = array($job->getId(), $job->getName(), $job->getUrl(), # 
        $job->getState(), $job->getLastExecutionStatus(), $job->getExecutionAttempts());
    return $data;
  }

  protected function write($data) {
    if ((count($data) > 0) && (is_array($data[0]))) {
      foreach ($data as $dataLine) {
        $this->write($dataLine);
      }
      return;
    }
    fputcsv($this->getReportFileHandle(), $data);
  }

  protected function getParameterHeaderText($parameterHeader) {
    if (!empty($parameterHeader)) {
      return array($parameterHeader);
    }
    return array();
  }

  protected function getStartParameterTableText() {
    $data = array('Parameter Name', 'Parameter Value');
    return $data;
  }

  protected function getFinishParameterTableText() {
    return array();
  }

  protected function getParameterLineText(EpJobParameters $parameter) {
    $data = array($parameter->getName(), $parameter->getValue());
    return $data;
  }

}