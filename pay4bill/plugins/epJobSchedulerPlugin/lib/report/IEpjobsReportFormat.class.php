<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * An interface that facilitate multiple format support for epjobs report.
 * 
 * @author apundir
 */
interface IEpjobsReportFormat {
  public function startReport($reportHeader);
  
  public function startSummaryTable($summaryHeader);
  
  public function printSummaryLine(EpJob $job);
  
  public function finishSummaryTable();
  
  public function printDetailedData(EpJobExecution $execution, $index, $total);
  
  public function closeReport($closingText=null);

  public function startParameterTable($parameterHeader);

  public function printParameters(EpJob $job);

  public function finishParameterTable();
}
