<?php

class EpJobUtils {

  /**
   * Creates EpJobExecution instances for given queue execution instance.
   * The execution instance will have all details populated in.
   * The job is re-scheduled IF the exit code of the taks is NOT ZERO.
   * The res-scheduling is based upon exponentail backoff suggested by
   * method addInQueue.
   *
   *
   * @param EpJobQueue $queueObj  The Object from Job Queue
   * @param string $startTime     Start time of this task
   * @param integer $exitCode     Exit code of the task
   * @param string $outData       Std-out data from the task
   * @param string $errData       Std-err data from the task
   * @param string $nextStartTime Next time at which the task shall be scheduled
   * @see function addInQueue
   */
  public static function updateJobOutputData(EpJobQueue $queueObj, $startTime, $exitCode, $outData, $errData, $nextStartTime=null) {
    $job = $queueObj->getEpJob();
    $job->setExecutionAttempts($job->getExecutionAttempts() + 1);
    $jobExecution = new EpJobExecution();
    $jobExecution->setJobId($job->getId());
    $jobExecution->setStartTime($startTime);
    $jobExecution->setEndTime(date('Y-m-d H:i:s'));
    $jobExecution->setExitCode($exitCode);
    // add stdout data and stderr data now;
    if (strlen($outData)) {
      $jobExecution->StdOutData->output_text = $outData;
    }
    if (strlen($errData)) {
      $jobExecution->StdErrData->output_text = $errData;
    }

    $conn = Doctrine_Manager::connection();
    $conn->beginTransaction();
    $queueObj->delete($conn);
    if ($exitCode != 0) {
      $job->setLastExecutionStatus(EpJobConstants::$JOB_LAST_EXECUTION_STATE_FAIL);
      self::addInQueue($job, $conn, $nextStartTime);
    } else {
      $job->setLastExecutionStatus(EpJobConstants::$JOB_LAST_EXECUTION_STATE_PASS);
      if ($job->getScheduleType() == EpJobConstants::$JOB_SCHEDULE_TYPE_ONCE) {
        $job->setState(EpJobConstants::$JOB_STATE_FINISHED);
      } else {
        //To do
        $job->setExecutionAttempts(0);
        self::addRepeatedJobInQueue($job, $conn);
      }
    }
    $job->save($conn);
    $jobExecution->save($conn);
    $conn->commit();
  }

  /**
   * Add the given job in the jobqueue. This method shall be called
   * when a job has failed and it needs to be re-scheduled. The
   * re-scheduling algorigtham is loosly based on exponential back-off
   * policy where with each failure, the next scheduled time is incremented
   * by twice the duration. Minimum re-schedule duration is 5 minutes.
   *
   * @param EpJob $job The job object that shall be added in queue
   * @param Doctrine_Connection $conn DB Connection to use while storing
   */
  public static function addInQueue(EpJob $job, Doctrine_Connection $conn, $startTime=null) {
    if ($job->getMaxRetryAttempts() >= $job->getExecutionAttempts()) {
      $jobQueue = new EpJobQueue();
      $jobQueue->setJobId($job->getId());
      if ($startTime == null) {
        $jobQueue->setScheduledStartTime(self::getRescheduleTime($job->getExecutionAttempts()));
      } else {
        $jobQueue->setScheduledStartTime($startTime);
      }
      $jobQueue->setCurrentStatus(EpJobConstants::$JOB_QUEUE_STATUS_SCHEDULED);
      $jobQueue->save($conn);
    } else {
      $job->setState(EpJobConstants::$JOB_STATE_FINISHED);
    }
  }

  /**
   * Add the given repeated job in the jobqueue. This method shall be called
   * when a job has finished successfully and it needs to be re-scheduled. The
   * re-scheduling algorigtham is loosly based on exponential back-off
   * policy where with each failure, the next scheduled time is incremented
   * by twice the duration. Minimum re-schedule duration is 5 minutes.
   *
   * @param EpJob $job The job object that shall be added in queue
   * @param Doctrine_Connection $conn DB Connection to use while storing
   */
  public static function addRepeatedJobInQueue(EpJob $job, Doctrine_Connection $conn) {
    try {
      $jobQueue = new EpJobQueue();
      $jobQueue->setJobId($job->getId());
      $jobQueue->setScheduledStartTime($job->EpJobRepeatSchedule[0]->getNextScheduleTime());
      $jobQueue->setCurrentStatus(EpJobConstants::$JOB_QUEUE_STATUS_SCHEDULED);
      $jobQueue->save($conn);
    } catch (EpJobExpiredException $e) {
      $job->setState(EpJobConstants::$JOB_STATE_FINISHED);
    }
  }

  /**
   * Calculates and returns the time at which a task shall be re-scheduled.
   * This is where all calculations about the minimum/maximum time and
   * exponential back-off policies are being attempted.
   *
   * @param integer $reScheduleAttempt which rescheduling attempt is it.
   * @return string timestamp in DB format on which re-scheduling
   *                  shall be done.
   */
  public static function getRescheduleTime($reScheduleAttempt) {
    $resheduleDurationMin = sfConfig::get(
                    'app_epjob_reschedule_duration_min', EpJobConstants::$CFG_RESCHEDULE_DURATION_MIN);
    $rescheduleWindowTimeSec = $resheduleDurationMin * 60;
    $minimumReschTimeSec = 60;
    $maxReschTimeSec = $rescheduleWindowTimeSec;
    $nextTime = time() + (pow(2, $reScheduleAttempt - 1) * $rescheduleWindowTimeSec) +
            rand($minimumReschTimeSec, $maxReschTimeSec);
    return date('Y-m-d H:i:s', $nextTime);
  }

  /**
   * Protectects a job from malicious web execution. Call this API in your action that you
   * intend to be invoked from scheduler only. This API will ensure that the execution
   * is allowed if and only if the invocation of the API is through the a symfony
   * command line task. For any web request, exception will be thrown aborting the
   * execution of the task.
   *
   * @param boolean $allowDevEnv Set it to trur if you want at least dev environment
   *            to allow execution of tasks through web. Defaults to false.
   */
  public static function protectWebExecution($allowDevEnv=false) {
    $currentEnviroment = sfConfig::get('sf_environment');
    if (array_key_exists('_', $_SERVER)) {
      // being executed from CLI, all allowed
      return;
    }
    // it's not CLI execution for sure. Let us see if dev is allowed !!
    if ($allowDevEnv) {
      if ($currentEnviroment != sfConfig::get('app_epjob_dev_env_name', EpJobConstants::$DEV_ENV_NAME)) {
        throw new Exception("only Schedular can request this page !");
      }
      // it's a dev environment !!
      return;
    }
    throw new Exception ("Protected scheduled task! execution not allowed from web.");
  }

}