<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EpjobReportTask
 *
 * @author apundir
 */
class EpjobReportTask extends sfDoctrineBaseTask {

  protected function configure() {
    $this->namespace = 'epjobs';
    $this->name = 'report';
    $this->briefDescription = 'Create execution report';
    $this->detailedDescription = <<<EOF
        The [epjobs:report|INFO] task creates job execution stats report.
Call it with:

  [php symfony epjobs:report "2009-12-19 16:00:00" \
        "2009-12-22 16:00:00"|INFO]

[format|COMMENT] specifies the report format, possible values are:
[csv|INFO]      Generate CSV report (default)
[html|INFO]     Generate HTML report
[txt|INFO]      Generate Text report

To generate a detailed report which includes the task execution output
streams, include --detailed option:
  [php symfony epjobs:report --detailed "2009-12-19 16:00:00" \
        "2009-12-22 16:00:00"|INFO]

To send a text format DETAILED email for past 24 hours, use following:
  [symfony epjobs:report --pass-multi --failed --format=txt \
        --detailed --mailto=avnish.pundir@tekmindz.com|INFO]


EOF;
    $this->addArgument('start-dateNtime', sfCommandArgument::OPTIONAL, #
            'Report from (date and time)', date('Y-m-d H:i:s', time() - (24 * 60 * 60)));
    $this->addArgument('end-dateNtime', sfCommandArgument::OPTIONAL, #
            'Report to (date and time)', date('Y-m-d H:i:s'));
    $this->addOption('memory_limit', null, sfCommandOption::PARAMETER_REQUIRED, 'Max memory to consume', '128M');
    $this->addOption('detailed', null, sfCommandOption::PARAMETER_NONE, 'Add execution details to report');
    $this->addOption('outfile', null, sfCommandOption::PARAMETER_REQUIRED, 'Create report in this file');
    $this->addOption('format', null, sfCommandOption::PARAMETER_REQUIRED, 'Report format', 'csv');
    $this->addOption('mailto', null, sfCommandOption::PARAMETER_REQUIRED, 'Send report in email "TO" addresses, separate by comma');
    $this->addOption('pass', null, sfCommandOption::PARAMETER_NONE, 'Include Pass jobs in report');
    $this->addOption('failed', null, sfCommandOption::PARAMETER_NONE, 'Include Failed jobs in report');
    $this->addOption('pass-multi', null, sfCommandOption::PARAMETER_NONE, #
            'Report Pass jobs with exec attempts more than one');
    $this->addOption('application', null, sfCommandOption::PARAMETER_REQUIRED, # 
            'The application name', EpJobConstants::$CFG_DEFAULT_APPLICATION);
    $this->addOption('env', null, sfCommandOption::PARAMETER_REQUIRED, # 
            'The environment', EpJobConstants::$CFG_DEFAULT_ENVIRONMENT);
    $this->addOption('source', null, sfCommandOption::PARAMETER_REQUIRED, 
            'Source of data; specify "execution" in case detailed execution stats
                  from ep_job_execution table are required', 'job');
    $this->addOption('attach-file', null, sfCommandOption::PARAMETER_REQUIRED,
            'Attach outfile to mail only if its size is less than specified[in bytes]; default is 1MB', '1048576');//by default attachments upto 1MB allowed
  }

  protected function execute($arguments = array(), $options = array()) {
    $databaseManager = new sfDatabaseManager($this->configuration);
    sfContext::createInstance($this->configuration);

    $format = EpjobsReport::$FORMAT_CSV;
    $filext = '.csv';
    $mime = 'text/csv';
    if ($options['format'] == 'html') {
      $format = EpjobsReport::$FORMAT_HTML;
      $filext = '.html';
      $mime = 'text/html';
    } elseif ($options['format'] == 'txt') {
      $format = EpjobsReport::$FORMAT_TXT;
      $filext = '.txt';
      $mime = 'text/plain';
    }

    $rep_options = 0;
    if ($options['pass']) {
      $rep_options |= EpjobsReport::$OPTION_PASSED;
    }
    if ($options['failed']) {
      $rep_options |= EpjobsReport::$OPTION_FAILED;
    }
    if ($options['pass-multi']) {
      $rep_options |= EpjobsReport::$OPTION_PASS_IN_MULTI_ATTEMPT;
    }
    if ($options['detailed']) {
      $rep_options |= EpjobsReport::$OPTION_DETAILED;
    }
    if ($options['source'] && $options['source']=="execution") {
      $rep_options |= EpjobsReport::$OPTION_SOURCE_EXECUTION;
    }
    else {
      $rep_options |= EpjobsReport::$OPTION_SOURCE_JOB;
    }
    // see how much memory is assigned for the job!!
    ini_set('memory_limit',$options['memory_limit']);

    $statCalculator = new EpjobsReport();

    if ($options['mailto']) {
      $outfile = tempnam(sys_get_temp_dir(), 'epjobs-report-');
      @unlink($outfile);
      $outfile .= $filext;
      $statCalculator->generateStatusFile($arguments['start-dateNtime'], $arguments['end-dateNtime'], #
              $rep_options, $format, $outfile);
      $fileSize = filesize($outfile);
      $mailer = EpjobsMailerFactory::getMailer();
      $mailer->setTo(explode(',', $options['mailto']));
      $mailer->setSubject("Epjobs execution stats report");
      $mailer->setBody(file_get_contents($outfile), $mime);
      if(is_numeric($options['attach-file']) && ($fileSize <= $options['attach-file'])) {
        $mailer->addAttachmentFromFile($outfile);
      }
      $mailer->send();
      unlink($outfile);
      return;
    }
    if ($options['outfile']) {
      echo 'Generating report file ...: ';
      echo $statCalculator->generateStatusFile($arguments['start-dateNtime'], $arguments['end-dateNtime'], #
              $rep_options, $format, $options['outfile']);
      echo "\n";
    } else {
      $stats = $statCalculator->generateStatus($arguments['start-dateNtime'], $arguments['end-dateNtime'], #
              $rep_options, $format);
      echo ($stats);
    }
  }

}