<?php
//EpJobExecuteTask.class.php

class EpJobExecuteTask extends sfDoctrineBaseTask {
  protected function configure() {
    $this->namespace        = 'epjobs';
    $this->name             = 'execute-job';
    $this->briefDescription = 'Execute a job';
    $this->detailedDescription = <<<EOF
        The [epjobs:execute-job|INFO] task executes a scheduled job.
Call it with:

  [php symfony epjobs:execute-job <job-queue-id>|INFO]

EOF;
    $this->addArgument('jobqueueid',
      sfCommandArgument::REQUIRED, 'id of the job in jobqueue');
    $this->addOption('application', null,
      sfCommandOption::PARAMETER_REQUIRED, 'The application name', EpJobConstants::$CFG_DEFAULT_APPLICATION);
    $this->addOption('env', null,
      sfCommandOption::PARAMETER_REQUIRED, 'The environment', EpJobConstants::$CFG_DEFAULT_ENVIRONMENT);
  }

  protected function execute($arguments = array(), $options = array()) {
    $databaseManager = new sfDatabaseManager($this->configuration);
    $this->context = sfContext::createInstance($this->configuration);
    $queueObj = Doctrine_Query::create()->from('EpJobQueue q')
      ->leftJoin('q.EpJob ep')->where('q.id = ?', $arguments['jobqueueid'])
      ->leftJoin('ep.EpJobParameters prm')->fetchOne();
    if(!$queueObj) {
      exit(EpJobConstants::$EXIT_CODE_JOB_NOT_FOUND_IN_QUEUE);
    }
    if($queueObj->getCurrentStatus() == EpJobConstants::$JOB_QUEUE_STATUS_RUNNING) {
      exit(EpJobConstants::$EXIT_CODE_JOB_ALREADY_RUNNING);
    }
    $jobStartTime= date('Y-m-d H:i:s');
    $queueObj->setStartTime($jobStartTime);
    $queueObj->setCurrentStatus(EpJobConstants::$JOB_QUEUE_STATUS_RUNNING);
    $queueObj->save();
    $exitCode = 255;
    $outText = '';
    $errorText = '';
    $jobContext = EpjobsContext::getInstance();
    try {
      $job = $queueObj->getEpJob();
      $jobContext->setCurrentJob($job);
      $param = $job->getParameters();
      $hostName = $param[EpJobConstants::$JOB_SCHEDULE_HOST_NAME];
      $schemaName = $param[EpJobConstants::$JOB_SCHEDULE_SCHEMA_NAME];
      unset($param[EpJobConstants::$JOB_SCHEDULE_HOST_NAME]);
      unset($param[EpJobConstants::$JOB_SCHEDULE_SCHEMA_NAME]);
      $browser = new sfBrowser();
      $url2Fetch = trim($job->getUrl());
      if (strpos($url2Fetch,'@') === 0) {
        // this is a route name
        $url2Fetch = $this->routeToUrl($url2Fetch);
      }
      if ($schemaName == "HTTPS") {
        $browser->setVar("HTTPS", "on");
      }
      $browser->setHttpHeader("Host", $hostName);
      $browser->post($url2Fetch,$param);
      $outText = $browser->getResponse()->getContent();
      $respCode = $browser->getResponse()->getStatusCode();
      // if $respCode is 200 than the request has passed, it has failed otherwise.
      if($respCode == 200) {
        if($jobContext->isTaskReportedFailed()) {
          $exitCode = EpJobConstants::$EXECUTION_REPORTED_FAILURE;
        } else {
          $exitCode=0;
        }
      } else {
        $exitCode=$respCode;
        //$outText = "HTTP Status Code: ".$respCode.PHP_EOL.$outText;
      }
    } catch (Exception $ex) {
      $errorText = "Exception during Job Execution: ".$ex->getMessage()
      .PHP_EOL.$ex->getTraceAsString();
      $this->log($errorText);
      $exitCode = EpJobConstants::$EXIT_CODE_JOB_BROWSER_EXCEPTION;
    }
    $nextScheduleTime = $jobContext->getNextRescheduleTime();
    EpJobUtils::updateJobOutputData($queueObj, $jobStartTime,
      $exitCode, $outText, $errorText, $nextScheduleTime);
  }

  protected function printJobs(Doctrine_Collection $jobs) {
    foreach ($jobs as $aJob) {
      echo "Queue ID: ".$aJob->getId().", Scheduled Start Time: "
          .$aJob->getScheduledStartTime().", Status: "
          .$aJob->getCurrentStatus().PHP_EOL;
    }
  }

  protected function deleteJobQueue($queueId){
    $deleted = Doctrine_Query::create()
    ->delete('EpJobQueue ep')
    ->where('ep.id = ?', $queueId)->execute();
  }

  protected function routeToUrl($route) {
    $this->context->getConfiguration()->loadHelpers('Url');
    $url = url_for($route);
    ## The url is ./symfony/symfony/<actual>/<route> - strip off
    ## first two symfony now to get a legitimate url
    $pos=strripos($url,'symfony');
    if ($pos === false) {
      throw new Exception ('Unable to convert route '.
        $route.' into URL, url_for returned: '.$url);
    }
    return substr($url,$pos+7);
  }

}