<?php

class EpJobRunScheduleTask extends sfDoctrineBaseTask {
  /** @var array Array which holds the data related to executing task */
  protected $handleJobMap = array();
  /**  @var Doctrine_Collection Jobs to execute in this schedule */
  protected $jobsToExecute = null;
  /** @var array The default CLI build for the task execution */
  protected $jobCmdCli = array();
  /** @var array The default CLI build for the task execution */
  protected $symfonyCmdCli = array();
  /** @var integer How many task handler process have been executed */
  protected $executedProcesses = 0;
  /** @var integer My own ip address*/
  protected $myPid = 0;
  /**
   * @var integer how long (in milliseconds) should be waited before
   *              re-checking the processes for their status.
   */
  protected $procChkSleepTimeMillis = 100;

  protected function configure() {
    $this->namespace        = 'epjobs';
    $this->name             = 'run-schedule';
    $this->briefDescription = 'Run all ready jobs as per schedule';
    $this->detailedDescription = <<<EOF
The [epjobs:run-schedule|INFO] task executes all jobs which are ready
as per already schedule available in the job queue.

You can execute a selective job by providing the job-queue-id

Call it with:

  [php symfony epjobs:run-schedule <job-queue-id>|INFO]

EOF;
    $this->addArgument('jobqueueid',
      sfCommandArgument::OPTIONAL, 'id of the job in jobqueue');

    $this->addOption('application', null,
      sfCommandOption::PARAMETER_OPTIONAL, 'The application name', EpJobConstants::$CFG_DEFAULT_APPLICATION);
    $this->addOption('env', null,
      sfCommandOption::PARAMETER_OPTIONAL, 'The environment', EpJobConstants::$CFG_DEFAULT_ENVIRONMENT);
  }

  protected function execute($arguments = array(), $options = array()) {
    $this->myPid = getmypid();
    $this->log('schedule execution started');
    $databaseManager = new sfDatabaseManager($this->configuration);
    $queyTable = Doctrine::getTable('EpJobQueue');
    $this->jobsToExecute = $queyTable->getReadyJobs(true,true);

    $this->executeBatch();
    $this->monitorBatch();
    $this->log('schedule execution finished');
  }

  protected function monitorBatch() {
    while (1) {
      $allQueuIds = array_keys($this->handleJobMap);
      if(!count($allQueuIds)) {
        //$this->log("All processes Finished ...");
        break;
      }
      // $this->log('Checking '.count($allQueuIds).' processes...');
      foreach ($allQueuIds as $aId) {
        $ps = proc_get_status($this->handleJobMap[$aId]['handle']);
        if ($ps['running']) {
        } else {
          $this->processFinishedJob($ps,$aId);
          //echo "Process ".$ps['pid']." Finished, exit code:".$ps['exitcode']."\n";
          // TODO - close all the pipes associated with this proc
        }
      }
      usleep ($this->procChkSleepTimeMillis * 1000);
    }
  }

  protected function processFinishedJob($pStatus,$queId) {
    $this->log('Job with queueid: '.$queId.' Finished execution');
    $exitCode = $pStatus['exitcode'];
    /*
     * It's assumed that the child process itself captures
     * and populates the job data into jobexecution.
     *
     * If and ONLY if the exit code is non zero, I'll update
     * the output data into the jobexecution instance
     */
    if($exitCode != 0) {
      // see if the task was really executed or not
      if ($exitCode == EpJobConstants::$EXIT_CODE_JOB_ALREADY_RUNNING) {
        $this->log("Job with queueid: $queId was already Running!! Wasn't executed this time");
      } elseif ($exitCode == EpJobConstants::$EXIT_CODE_JOB_NOT_FOUND_IN_QUEUE) {
        $this->log("Job with queueid: $queId NOT FOUND in the queue. Is it executed and finished already?");
      } else {
        $this->updateExecution($exitCode, $queId);
      }
    } else {
      /*
       * For symfony tasks which are scheduled, they wouldn't be doing
       * any logging by themselves thus it's imperative that we do
       * the logging ourselves even if the exit code is 0 from these tasks
       */
      $job = $this->handleJobMap[$queId]['queueobj']->getEpJob();
      if ($job->isSymfonyCommand()) {
        $this->updateExecution($exitCode, $queId);
      }
    }
    // delete the stdout and stderr files now
    if(file_exists($this->handleJobMap[$queId]['outfile'])) {
      unlink($this->handleJobMap[$queId]['outfile']);
    }
    if(file_exists($this->handleJobMap[$queId]['errfile'])) {
      unlink($this->handleJobMap[$queId]['errfile']);
    }
    // remove it from the handle map now
    unset($this->handleJobMap[$queId]);
    // add new task to pool if any exists
    $this->spawnTaskFromQueue();
  }

  protected function updateExecution($exitCode,$queId) {
    $jobid = $this->handleJobMap[$queId]['jobid'];
    $queueObj = $this->handleJobMap[$queId]['queueobj'];
    $outData = @file_get_contents($this->handleJobMap[$queId]['outfile']);
    $errData = @file_get_contents($this->handleJobMap[$queId]['errfile']);
    EpJobUtils::updateJobOutputData($queueObj, $this->handleJobMap[$queId]['starttime'], $exitCode, $outData, $errData);
  }

  protected function executeBatch() {
    $php = $this->getPhpPath();
    $symCli = $this->getSymfonyCliPath();
    $exeParam[] = $php;
    if ($php != $symCli) {
      $exeParam[] = $symCli;
    }
    // this is what will be used for executing symfony commands
    $this->symfonyCmdCli=$exeParam;

    // this is what'll be used to execute jobs through our task
    $this->jobCmdCli=$exeParam;
    $this->jobCmdCli[]='epjobs:execute-job';

    $max_children_process = sfConfig::get('app_epjobs_max_task_processes', EpJobConstants::$CFG_MAX_TASK_PROCESSES);
    $spawned_processes = 0;

    while($spawned_processes<$max_children_process) {
      if (!$this->spawnTaskFromQueue()) {
        // no more in queue
        break;
      }
      $spawned_processes++;
    }
  }

  /**
   * Attempt to execute a task from the queue as background process.
   * Return true if succesfull, false if there's no task available in
   * queue.
   *
   * @return boolean True if there was a task available in queue that was
   *                 handed over to a child process. False if there is no
   *                 no more task in the queue
   */
  protected function spawnTaskFromQueue() {
    if ($this->executedProcesses >= $this->jobsToExecute->count()) {
      return false;
    }
    $queJob = $this->jobsToExecute->get($this->executedProcesses);
    $quid = $queJob->getId();
    $jobExeParam = $this->getExcutionParameters($queJob);
    $this->log("Executing job with queue id: ".$quid);
    $this->executeJob($queJob,$jobExeParam);
    $this->executedProcesses++;
    return true;
  }

  protected function getExcutionParameters($queJob) {
    $job = $queJob->getEpJob();
    $jobUrl = $job->getUrl();
    $jobAppName = $job->getSfApplication();
    // see if this job is a symfony command or not
    if ((strpos($jobUrl, ':') === false) && (!$job->isSymfonyCommand())) {
      $quid = $queJob->getId();
      $jobExeParam = $this->jobCmdCli;
      $jobExeParam[] = '--application='.$jobAppName;
      $jobExeParam[] = "--trace";
      $jobExeParam[] = $quid;
      return $jobExeParam;
    }
    $quid = $queJob->getId();
    $jobExeParam = $this->symfonyCmdCli;
    // command name
    $jobExeParam[] = $jobUrl;
    // now add all the parameters
    $params = $job->getParameters();
    $noOfParams = $params[EpJobConstants::$JOB_SCHEDULE_SYMFONY_CMD_PARAM_COUNT];
    for($i=0;$i<$noOfParams;$i++) {
      $jobExeParam[] = $params[EpJobConstants::$JOB_SCHEDULE_SYMFONY_CMD_PARAM_PREFIX.$i];
    }
    $jobExeParam[] = "--trace";
    return $jobExeParam;
  }

  protected function executeJob($queueJob, $params) {
    $queueId = $queueJob->getId();
    $jobId = $queueJob->getJobId();
    //echo "Wil be executing".implode($params, ' ');
    $stdOutFile = $this->getTempFile($queueId);
    $stdErrFile = $this->getTempFile($queueId,true);
    $descriptorspec = array(
         1 => array("file", $stdOutFile, "a"),
         2 => array("file", $stdErrFile, "a")
    );
    $handle = proc_open(implode($params, ' '), $descriptorspec, $pip);
    $this->handleJobMap[$queueId] = array (
      'handle' => $handle,
      'queueobj' => $queueJob,
      'queueid' => $queueId,
      'jobid' => $jobId,
      'outfile' => $stdOutFile,
      'errfile' => $stdErrFile,
      'starttime' => date('Y-m-d H:i:s')
    );
  }

  protected function getTempFile($jobQueueId, $isForErr=false) {
    $tmpPath = sys_get_temp_dir();
    $myPid = getmypid();
    $fileName='EP_';
    if($isForErr) {
      $fileName.='ERR_';
    } else {
      $fileName.='OUT_';
    }
    $fileName.="$myPid_$jobQueueId.txt";
    return $tmpPath.DIRECTORY_SEPARATOR.$fileName;
  }

  public function log($messages) {
    $messages = '['.$this->myPid.']['.date('Y-m-d H:i:s T').']: '.$messages;
    parent::log($messages);
  }

  protected function getPhpPath() {
    return $_SERVER['_'];
  }

  protected function getSymfonyCliPath() {
    return $_SERVER['SCRIPT_FILENAME'];
  }
}