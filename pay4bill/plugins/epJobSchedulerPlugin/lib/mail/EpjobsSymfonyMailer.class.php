<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EpjobsSymfonyMailer
 *
 * @author apundir
 */
class EpjobsSymfonyMailer implements IEpjobsMailer {

  /**
   *
   * @var Swift_Mime_Message actual message from swift pkg.
   */
  private $message = null;

  public function EpjobsSymfonyMailer() {
    $this->message = Swift_Message::newInstance();
    if ($this->getDefaultFrom() != null) {
      $this->message->setFrom(explode(',', $this->getDefaultFrom()));
    }
  }

  public function addAttachment($attachment, $mime = null) {
    $this->message->attach(Swift_Attachment::newInstance($attachment, $mime));
  }

  public function addAttachmentFromFile($attachmentFile, $mime = null) {
    $this->message->attach(Swift_Attachment::fromPath($attachmentFile, $mime));
  }

  public function send() {
    $mailer = sfContext::getInstance()->getMailer();
    $mailer->send($this->message);
  }

  public function setBcc($bccList) {
    $this->message->setBcc($bccList);
  }

  public function setBody($body, $mime = null) {
    $this->message->setBody($body, $mime);
  }

  public function setCc($ccList) {
    $this->message->setCc($ccList);
  }

  public function setFrom($fromAddress) {
    $this->message->setFrom($fromAddress);
  }

  public function setSubject($subject) {
    $this->message->setSubject($subject);
  }

  public function setTo($toList) {
    $this->message->setTo($toList);
  }

  protected function getDefaultFrom() {
    return sfConfig::get('app_epjob_mail_from');
  }

}