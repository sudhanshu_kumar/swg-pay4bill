<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of pay4MeGatewayManagerFactory
 *
 * @author srikanth reddy
 */
/*  Merchant Code Merchant Key now Dyanamic-12June,2102  */
class Pay4meGatewayManagerFactory {

    public static function getIntegrator($version, $merchant_code, $merchant_key) {

        if ($version != "") {
            $pay4MeManager = "Pay4me" . ($version) . "Client";
            $pay4MeManagerObj = new $pay4MeManager(
                            $merchant_code,
                            $merchant_key,
                            sfConfig::get("app_pay4me_payment_url"));
            return $pay4MeManagerObj;
        } else {
            return 'INVALID_VERSION';
        }
    }

}

?>
