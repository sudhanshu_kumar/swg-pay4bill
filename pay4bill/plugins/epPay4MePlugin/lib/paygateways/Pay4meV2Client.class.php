<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Pay4meV2Client extends AbstractPay4meClient {

    protected $merchant_code = null;
    protected $merchant_key = null;
    protected $payment_url = null;
    protected $version = "v2";
    protected $defaultXmlNs = "http://www.pay4me.com/schema/pay4meorder/v2";
    protected $logger;
    protected $merchant_service_id;

    function Pay4meV2Client($merchant_code, $merchant_key, $payment_url) {
        $this->merchant_code = $merchant_code;
        $this->merchant_key = $merchant_key;
        $this->payment_url = $payment_url;
        $this->logger = new Pay4meLog("L_ALL");
    }

    protected function generateRequestXML($item) {
        $version = $this->version;
        $merchant_service_id = $this->merchant_service_id;
        $xml_data = new gc_XmlBuilder();
        $xml_data->Push('order', array('xmlns' => $this->defaultXmlNs));
        $xml_data->Push('merchant-service', array('id' => $merchant_service_id));
        if (!isset($item) || count($item) == 0) {
            return false;
        }

        if (isset($item['item_number']))
            $xml_data->Push('item', array('number' => $item['item_number']));
        else
            $xml_data->Push('item', array('number' => ''));
        if (isset($item['transaction_number']))
            $xml_data->Element('transaction-number', $item['transaction_number']);
        if (isset($item['name']))
            $xml_data->Element('name', $item['name']);
        if (isset($item['description']))
            $xml_data->Element('description', $item['description']);

        $xml_data->Push('parameters');
        if (isset($item['parameters'])) {
            foreach ($item['parameters'] as $paramkey => $parameter) {
                $xml_data->Element('parameter', $parameter, array('name' => $paramkey));
            }
        }

        $xml_data->Pop('parameters');
        if (isset($item['transaction-location'])) {
            $xml_data->Element('transaction-location', $item['transaction-location']);
        }
        $xml_data->Push('payment');
        if (isset($item['payment'])) {
            foreach ($item['payment'] as $paymentkey => $payment) {
                if (isset($payment['code']))
                    $xml_data->Push('currency', array('code' => $payment['code']));
                $xml_data->Push('payment-modes');
                if (isset($payment['payment-modes'])) {
                    foreach ($payment['payment-modes'] as $paymentmode) {
                        $xml_data->Element('payment-mode', $paymentmode);
                    }
                }
                $xml_data->Pop('payment-modes');
                if (isset($payment['deduct-at-source'])) {
                    $xml_data->Element('deduct-at-source', $payment['deduct-at-source']);
                } else {
                    $xml_data->Element('deduct-at-source', false);
                }
                if (isset($payment['price']))
                    $xml_data->Element('price', $payment['price']);

                //                $xml_data->Push('revenue-share',array('fixed-share' =>
                //                                                $payment['revenue-share']['fixed-share']));
                //
                //                $xml_data->Push('other-shares');
                //                foreach($payment['revenue-share']['other-shares'] as $othersharekey=>$othershare){
                //                     $xml_data->Element('other-share', $othershare,
                //                                array('merchant-account-number' => $othersharekey));
                //                }
                //                $xml_data->Pop('other-shares');
                //                $xml_data->Pop('revenue-share');
                $xml_data->Pop('currency');
            }
        }

        $xml_data->Pop('payment');
        $xml_data->Pop('item');

        $xml_data->Pop('merchant-service');
        $xml_data->Pop('order');

        return $xml_data->GetXML();
    }

    protected function saveResponse($payment_notification) {//Added Bank name and branch name to be saved-12June,2102

        $data = array(
            'payment_status' =>
            $payment_notification['item']['payment-information']['status']['code']['VALUE'],
            'item_number' =>
            $payment_notification['item']['number'],
            'transaction_number' =>
            $payment_notification['item']['transaction-number']['VALUE'],
            'merchant_service_id' =>
            $payment_notification['id'],
            'validation_number' =>
            $payment_notification['item']['validation-number']['VALUE'],
            'bank_name' =>
            $payment_notification['item']['payment-information']['bank']['name']['VALUE'],
            'branch_name' =>
            $payment_notification['item']['payment-information']['bank']['branch']['VALUE'],
            'payment_mode' =>
            $payment_notification['item']['payment-information']['payment-mode']['VALUE'],
            'total_amount' =>
            $payment_notification['item']['payment-information']['amount']['VALUE'],
            'currency' =>
            $payment_notification['item']['payment-information']['currency']['code'],
            'payment_date' =>
            $payment_notification['item']['payment-information']['payment-date']['VALUE'],
            'description' =>
            $payment_notification['item']['payment-information']['status']['description']['VALUE']);

        $requestStatus = DbApiFactory::SaveResponse($this->version,$data);
        
    }

    protected function savePaymentRequest($itemArray) {

        $item = $itemArray['item_details'];
        $data = array(
            'merchant_service_id' => $itemArray['merchant_service_id'],
            'item_number' => $item['item_number'],
            'transaction_number' => (isset($item['transaction_number'])) ? $item['transaction_number'] : '',
            'payment_type' => (isset($item['payment_type'])) ? $item['payment_type'] : '',
            'buyer_ip_address' => (isset($item['buyer_ip_address'])) ? $item['buyer_ip_address'] : '',
            'description' => (isset($item['description'])) ? $item['description'] : '',
            'price' => $item['price']);
        $merchantApiObj = new MerchantDbApi();
        $merchantApiObj->saveRequest($data);
    }

}

?>