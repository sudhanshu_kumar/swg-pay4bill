<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MerchantDbApiclass
 *
 * @author spandey
 */
class MerchantDbApi {

    public function saveRequest($data) {

        $itemNo = $data['item_number'];
        $status = true;
        $checkStatus = Doctrine::getTable('EpP4mRequest')->checkRequestDetailByItemNumber($itemNo, $data['merchant_service_id']);
        if (!$checkStatus) {
            $tblP4mRequestObj = new EpP4mRequest();
            $tblP4mRequestObj->setItemNumber($data['item_number']);
            $tblP4mRequestObj->setMerchantServiceId($data['merchant_service_id']);
            $tblP4mRequestObj->setTransactionNumber($data['transaction_number']);
            if (isset($data['payment_type']))
                $tblP4mRequestObj->setPaymentType($data['payment_type']);
            if (isset($data['buyer_ip_address']))
                $tblP4mRequestObj->setBuyerIpAddress($data['buyer_ip_address']);
            if (isset($data['description']))
                $tblP4mRequestObj->setPaymentDescription($data['description']);
            if (isset($data['currency']))
                $tblP4mRequestObj->setCurrency($data['currency']);
            if (isset($data['price']))
                $tblP4mRequestObj->setTotalAmount($data['price']);
            $tblP4mRequestObj->save();
            return true;
        } else {
            return $status;
        }
    }

    public function saveResponse($data) {
        $itemNo = $data['item_number'];

        $checkStatus = Doctrine::getTable('EpP4mRequest')->checkRequestDetailByItemNumber($itemNo, $data['merchant_service_id']);

        if ($checkStatus) {
            $requestObj = Doctrine::getTable('EpP4mRequest')->findByItemNumber($itemNo);
            $requestId = $requestObj->getFirst()->getId();
            $tblP4mResponseObj = new EpP4mResponse();
            $tblP4mResponseObj->setRequestId($requestId);
            $tblP4mResponseObj->setPaymentStatus($data['payment_status']);
            if (isset($data['payment_mode']))
                $tblP4mResponseObj->setPaymentMode($data['payment_mode']);
            if (isset($data['validation_number']))
                $tblP4mResponseObj->setValidationNumber($data['validation_number']);
            if (isset($data['total_amount']))
                $tblP4mResponseObj->setTotalAmount($data['total_amount']);
            if (isset($data['currency']))
                $tblP4mResponseObj->setCurrency($data['currency']);
            if (isset($data['bank_name']))
                $tblP4mResponseObj->setBankName($data['bank_name']);
            if (isset($data['branch_name']))
                $tblP4mResponseObj->setBranchName($data['branch_name']);
            if (isset($data['payment_date']))
                $tblP4mResponseObj->setPaymentDate($data['payment_date']);
            if (isset($data['description']))
                $tblP4mResponseObj->setPaymentDescription($data['description']);
            $tblP4mResponseObj->save();
           
            return true;
        } else {
            return false;
        }
    }

}

?>