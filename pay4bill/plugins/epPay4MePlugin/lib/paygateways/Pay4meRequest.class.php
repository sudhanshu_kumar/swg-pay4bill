<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
define('ENTER', "\r\n");
define('DOUBLE_ENTER', ENTER.ENTER);
class Pay4meRequest {

    private $merchant_code;
    private $merchant_key;
    private $payment_url;
    private $version;
    private $timeout;

    public function Pay4meRequest($merchant_code,$merchant_key,$payment_url,$version,$timeout=30) {

        $this->merchant_code = $merchant_code;
        $this->merchant_key = $merchant_key;
        $this->version = $version;
        $this->timeout = $timeout;
        //do logging of url
        $this->payment_url = $payment_url."/payprocess/$version/PID/$merchant_code";
        $this->logger = new Pay4meLog("L_ALL");

    }


    public  function sendRequest($postargs,$timeout=false) {
      $url = $this->payment_url;
      $header_arr = $this->GetAuthenticationHeaders();
      // Get the curl session object
      $session = curl_init($url);
      // Set the POST options.
      curl_setopt($session, CURLOPT_POST, true);
      curl_setopt($session, CURLOPT_HTTPHEADER, $header_arr);
      curl_setopt($session, CURLOPT_POSTFIELDS, $postargs);
      curl_setopt($session, CURLOPT_HEADER, true);
      curl_setopt($session, CURLOPT_RETURNTRANSFER, true);

      if(!empty($this->certPath) && file_exists($this->certPath)) {
        curl_setopt($session, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($session, CURLOPT_CAINFO, $this->certPath);
      }
      else {
        curl_setopt($session, CURLOPT_SSL_VERIFYPEER, false);
      }

      if(isset($this->proxy) && is_array($this->proxy) && count($this->proxy)) {
         curl_setopt($session, CURLOPT_PROXY,
                            $this->proxy['host'] . ":" . $this->proxy['port']);
      }
      if($timeout != false){
        curl_setopt($session, CURLOPT_TIMEOUT, $timeout);

      }
      // Do the POST and then close the session
     $response = curl_exec($session);


      if (curl_errno($session)) {
         $this->logger->LogError(curl_error($session));
        return array("CURL_ERR", curl_error($session));
      } else {
        curl_close($session);
      }

      $heads = $this->parse_headers($response);
      $body = "";
     $body_xml = $this->get_body_x($response);
      $file = "/RedirectXml-".date('Y-m-d_h:i:s').".txt";
      $this->logger->LogData($response,$file);
      $this->xml_parser = new gc_XmlParser($body_xml);
      $this->root = $this->xml_parser->GetRoot();
      $this->data = $this->xml_parser->GetData();

      $code =0;
      if($this->root== 'order-redirect'){
        $body = $this->data['order-redirect']['redirect-url']['VALUE'];
      } elseif($this->root=='error'){
        $body = $this->data['error']['error-message']['VALUE'];
        $code = $this->data['error']['error-code']['VALUE'];

      }
      // Get HTTP Status code from the response
      $status_code = array();
      preg_match('/\d\d\d/', $heads[0], $status_code);

      // Check for errors
      switch( $status_code[0] ) {
        case 200:
          // Success
           // $this->log->LogResponse($response);
            return array(200, $body,$code,$this->root);
          break;
        case 503:
           // $this->log->LogError($response);
            return array(503, htmlentities($body),$code,$this->root);
          break;
        case 403:
           // $this->log->LogError($response);
            return array(403, htmlentities($body),$code,$this->root);
          break;
        case 400:
            //$this->log->LogError($response);
            return array(400, htmlentities($body),$code,$this->root);
          break;
        default:
           // $this->log->LogError($response);
            return array("ERR", htmlentities($body),$code,$this->root);
          break;
      }
    }

    // Private functions
// Function to get HTTP headers,
// will also work with HTTP 200 status added by some proxy servers
    /**
     * @access private
     */
    private function parse_headers($message) {
      $head_end = strpos($message, DOUBLE_ENTER);
      $headers = $this->get_headers_x(substr($message,0,
                                             $head_end + strlen(DOUBLE_ENTER)));
      if(!is_array($headers) || empty($headers)){
        return null;
      }
      if(!preg_match('%[HTTP/\d\.\d] (\d\d\d)%', $headers[0], $status_code)) {
        return null;
      }
      switch( $status_code[1] ) {
        case '200':
          $parsed = $this->parse_headers(substr($message,
                                             $head_end + strlen(DOUBLE_ENTER)));
          return is_null($parsed)?$headers:$parsed;
        break;
        default:
          return $headers;
        break;
      }
    }
    /**
     * @access private
     */
    function get_headers_x($heads, $format=0) {
      $fp = explode(ENTER, $heads);
      foreach($fp as $header){
        if($header == "") {
          $eoheader = true;
          break;
        } else {
          $header = trim($header);
        }

        if($format == 1) {
          $key = array_shift(explode(':',$header));
          if($key == $header) {
            $headers[] = $header;
          } else {
            $headers[$key]=substr($header,strlen($key)+2);
          }
          unset($key);
        } else {
         $headers[] = $header;
        }
      }
      return $headers;
    }

    /**
     * @access private
     */
    function get_body_x($heads){
        list($headers,$content) = explode(DOUBLE_ENTER,$heads,2);
        return $content;
    }
    /**
     * @access private
     */
    private function GetAuthenticationHeaders() {

      //do headers logging
      $headers = array();
      $headers[] = "Authorization: Basic ".base64_encode(
          $this->merchant_code.':'.$this->merchant_key);
      $headers[] = "Content-Type: application/xml; charset=UTF-8";
      $headers[] = "Accept: application/xml; charset=UTF-8";
      return $headers;
    }


}
?>
