<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class DbApiFactory {

 private static function getDbManager($version='') {
     $MerchantDbApiManager = "MerchantDbApi".strtoupper($version);
     $MerchantDbApiObj = new $MerchantDbApiManager();
     return $MerchantDbApiObj;
 }

 public static function SaveRequest($version,$arrayDetails){
     $status = true;
     //check if data to be saved to db or not
     $database_operations = sfConfig::get('app_pay4me_database_operations');

     if($database_operations){
        $ObjManager = self::getDbManager($version);
        $status = $ObjManager->saveRequest($arrayDetails);
     }
     return $status;
 }

 public static function SaveResponse($version,$arrayDetails){
     $status = true;
     $database_operations = sfConfig::get('app_pay4me_database_operations');
     //check if data to be saved to db or not
     if($database_operations){
        $ObjManager = self::getDbManager($version);
        $status = $ObjManager->saveResponse($arrayDetails);
     }
     return $status;
 }
}

?>
