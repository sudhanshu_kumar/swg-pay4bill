<?php
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
*/

/**
* Description of MerchantDbApiclass
*
* @author spandey
*/
class MerchantDbApiV2 {

    private function getConnection() {
        return Property::getConnection();
    }

    public function saveRequest($itemArray){

//  CREATE TABLE IF NOT EXISTS `ep_p4m_request` (
//  `id` int(11) NOT NULL AUTO_INCREMENT,
//  `item_number` int(11) NOT NULL,
//  `item_name` varchar(255) NOT NULL,
//  `merchant_service_id` int(10) NOT NULL,
//  `transaction_number` varchar(30) NOT NULL,
//  `payment_type` int(11) NOT NULL,
//  `buyer_ip_address` varchar(255) NOT NULL,
//  `payment_description` varchar(255) NOT NULL
//  PRIMARY KEY (`id`)
//) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;
        $item = $itemArray['item_details'];
        $itemNo = $item['item_number'];

        
        $status = true;
        $Last_Request_Id = "";
        $checkStatus = Doctrine::getTable('EpP4mRequest')->checkRequestDetailByItemNumber($itemNo,$itemArray['merchant_service_id']);
       
        if (!$checkStatus) {
            $tblP4mRequestObj = new EpP4mRequest();
            $tblP4mRequestObj->setItemNumber($item['item_number']);
            $tblP4mRequestObj->setMerchantServiceId($itemArray['merchant_service_id']);
            $tblP4mRequestObj->setTransactionNumber($item['transaction_number']);
            if(isset($item['payment_type']))
            $tblP4mRequestObj->setPaymentType($item['payment_type']);
            if(isset($item['buyer_ip_address']))
            $tblP4mRequestObj->setBuyerIpAddress($item['buyer_ip_address']);
            if(isset($item['description']))
            $tblP4mRequestObj->setPaymentDescription($item['description']);
            if(isset($item['price']))
            $tblP4mRequestObj->setTotalAmount($item['price']);

            $tblP4mRequestObj->save();
            $Last_Request_Id = $tblP4mRequestObj->getId();
        } else {
            return false;
        }
                                     
        if($Last_Request_Id){
            foreach($item['parameters'] as $paramkey=>$parameter){
                $this->saveRequestParameters($Last_Request_Id,$paramkey,$parameter);
            }
        
            foreach($item['payment'] as $paymentkey => $payment){

                $requestCurrencyId = $this->saveRequestCurrency($Last_Request_Id,
                            $payment['code'],
                $payment['price'],(
                        isset($payment['deduct-at-source'])) ? $payment['deduct-at-source']:false);

                foreach($payment['payment-modes'] as $paymentmode){
                    $this->saveRequestCurrencyPaymentMode($requestCurrencyId,$paymentmode);
                }
            }
        }                    
        return $status;
    }


    private function saveRequestParameters($requestId,$paramkey,$parameter){
//        CREATE TABLE `ep_p4m_request_parameters` (
//        `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
//        `request_id` INT NOT NULL ,
//        `parameter_name` VARCHAR( 255 ) NOT NULL ,
//        `parameter_value` VARCHAR( 255 ) NOT NULL ,
//        INDEX ( `request_id` )
//        ) ENGINE = InnoDB;
            $tblP4mReqParObj = new EpP4mRequestParameters();
            $tblP4mReqParObj->setRequestId($requestId);
            $tblP4mReqParObj->setParameterName($paramkey);
            $tblP4mReqParObj->setParameterValue($parameter);
            $tblP4mReqParObj->save();
    }

    private function saveRequestCurrency($requestId,$currency_code,$price,$deduct_at_source){
//        CREATE TABLE `ep_p4m_request_currency` (
//    `id` INT( 10 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
//    `request_id` INT( 10 ) NOT NULL ,
//    `currency_code` INT( 4 ) NOT NULL ,
//    `price` FLOAT( 30, 2 ) NOT NULL ,
//    `deduct_at_source` boolean NOT NULL ,
//    INDEX ( `request_id` )
//    ) ENGINE = InnoDB;
            $tblP4mReqCurObj = new EpP4mRequestCurrency();
            $tblP4mReqCurObj->setRequestId($requestId);
            $tblP4mReqCurObj->setCurrencyCode($currency_code);
            $tblP4mReqCurObj->setPrice($price);
            $tblP4mReqCurObj->setDeductAtSource($deduct_at_source);
            $tblP4mReqCurObj->save();
            return $tblP4mReqCurObj->getId();
    }

    private function saveRequestCurrencyPaymentMode($requestCurrencyId,$paymentMode){
//        CREATE TABLE `ep_p4m_currency_payment_mode` (
//    `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
//    `request_currency_id` INT NOT NULL ,
//    `payment_mode` INT NOT NULL ,
//    INDEX ( `request_currency_id` )
//    ) ENGINE = InnoDB;
        $tblP4mReqCurPayObj = new EpP4mRequestCurrencyPaymentMode();
        $tblP4mReqCurPayObj->setRequestCurrencyId($requestCurrencyId);
        $tblP4mReqCurPayObj->setPaymentMode($paymentMode);
        $tblP4mReqCurPayObj->save();
    }



    public function saveResponse($payment_notification){
//      create your table with the help of this query
//          CREATE TABLE IF NOT EXISTS `ep_p4m_response` (
//          `id` int(11) NOT NULL AUTO_INCREMENT,
//          `request_id` int(11) NOT NULL,
//          `payment_status` enum('0','1') DEFAULT '0',
//          `payment_mode` varchar(150) NOT NULL,
//          `validation_number` varchar(255) NOT NULL,
//          `total_amount` float(30,2) DEFAULT NULL,
//          `currency` integer(4) NOT NULL,
//          `payment_date` datetime NOT NULL,
//          `payment_description` text,
//          PRIMARY KEY (`id`),
//          KEY `request_id` (`request_id`)
//        ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;
       
        $itemNo = $payment_notification['item_number'];
        $checkStatus = Doctrine::getTable('EpP4mRequest')->checkRequestDetailByItemNumber($itemNo,$payment_notification['merchant_service_id']);
         if ($checkStatus) {
            $requestObj = Doctrine::getTable('EpP4mRequest')->findByItemNumber($itemNo);
            $requestId  = $requestObj->getFirst()->getId();
            $tblP4mResponseObj = new EpP4mResponse();
            $tblP4mResponseObj->setRequestId($requestId);
            $tblP4mResponseObj->setPaymentStatus($payment_notification['payment_status']);
            if(isset($payment_notification['payment_mode']))
            $tblP4mResponseObj->setPaymentMode($payment_notification['payment_mode']);
            if(isset($payment_notification['validation_number']))
            $tblP4mResponseObj->setValidationNumber($payment_notification['validation_number']);
            if(isset($payment_notification['total_amount']))
            $tblP4mResponseObj->setTotalAmount($payment_notification['total_amount']);
            if(isset($payment_notification['currency']))
            $tblP4mResponseObj->setCurrency($payment_notification['currency']);
            if(isset($payment_notification['payment_date']))
            $tblP4mResponseObj->setPaymentDate($payment_notification['payment_date']);
            if(isset($payment_notification['description']))
            $tblP4mResponseObj->setPaymentDescription($payment_notification['description']);
            $tblP4mResponseObj->save();
            return true;

        } else {
            return false;
        }

    }

}
?>