<?php

  // Log levels
  define("L_OFF", 0); // No log
  define("L_ERR", 1); // Log Errors
  define("L_RQST", 2); // Log Request
  define("L_RESP", 4); // Log Resoponse
  define("L_ERR_RQST", L_ERR | L_RQST);
  define("L_RQST_RESP", L_RQST | L_RESP);
  define("L_ALL", L_ERR | L_RQST | L_RESP);
class Pay4meLog {
    private $LogFile;
 // L_ALL (err+requests+responses), L_ERR, L_RQST, L_RESP, L_OFF
    private $logLevel = L_ERR_RQST;
    private $logParentDir;
  /**
   * SetLogFiles
   */
  function Pay4meLog($logLevel=L_ERR_RQST, $die=false){
    $this->logParentDir = property::getLogPath();
    $this->CreateLogPath($this->logParentDir);
    $this->logLevel = $logLevel;
    if($logLevel == L_OFF) {
      $this->logLevel = L_OFF;
    } else {
      $this->LogFile = $this->logParentDir."/pay4meLog".date('Y-m-d_h:i:s').".txt";
      if (!$this->LogFile = @fopen($this->LogFile, "a")) {
        header('HTTP/1.0 500 Internal Server Error');
        $log = "Cannot open " . $this->LogFile . " file.\n" .
                    "Logs are not writable, set them to 777";
        LogError($log);
        } else {
            $this->writeLogtoFile($this->LogFile,"");
        }
      }
  }

  private function writeLogtoFile($log_message_file,$log){
    $fp = fopen($log_message_file,"a");
    fwrite($fp,$log);
    fclose($fp);
  }

  private  function CreateLogPath($parentLogFolder){

    $logPath =$parentLogFolder;
    if(is_dir($logPath)=='')
    {
      $dir_path=$logPath."/";
      mkdir($dir_path,0777,true);
      chmod($dir_path, 0777);
    }
    return $logPath;

  }

  public function LogError($log){
    if($this->LogFile ){
      $this->LogErrorFile = $this->logParentDir."/pay4meErrorLog".date('Y-m-d_h:i:s').".txt";
      $this->writeLogtoFile($this->LogErrorFile,$log);
      return true;
    }
    return false;
  }

  public function LogMessage($log){
    if($this->LogFile){
      $this->writeLogtoFile($this->LogFile,$log);
      return true;
    }
    return false;
  }

  public function LogData($log,$file=''){
    if($file!=""){
        $log_message_file =  $this->logParentDir.$file;
    } else {
        $log_message_file = $this->LogFile;
    }
    $this->writeLogtoFile($log_message_file,$log);
    return false;
  }

}
?>