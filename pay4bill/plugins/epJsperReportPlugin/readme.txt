Requirements
-------------
To use the plugin, you will need:
 * A web server that supports php5
 * SOAP pear package (http://pear.php.net/package/SOAP/)
 * Net_Dime pear package (http://pear.php.net/package/Net_DIME/)
 * Symfony 1.2.X or above 
	
See the php documentation for information about installing the pear packages. 
On Debian/Ubuntu installing the "php-soap" package is sufficient.

Install the plugin
-------------------
* Plugin zip file extract into symfony plugins folder and add following lines to config/ProjectConfiguration.class.php

 $this->enablePlugins('epJsperReportPlugin');

* run command "php symfony plugin:publish-assets"
* apps/frontend/config/settings.yml file
pa
all:
  .settings:
    enabled_modules:        [jasperReport]

Plugin Configuration
---------------------
a) The plugin assumes you are running JasperReports Server instance at:

http://localhost:8080/jasperserver/services/repository
  
If you need to change this URL (for example, if you are running JasperReports Server 
Professional), you must change the variable in 

epJsperReportPlugin/config/app.yml

jasperserverurl: http://localhost:8080/jasperserver/services/
webservicesuri:  http://localhost:8080/jasperserver/services/repository

the use the following URL:

http://localhost:8080/jasperserver-pro/services/repository

b) Plugin needs jasper server login credential to access the reports
   Use a regular JasperServer account (such as jasperadmin/jasperadmin).

If you have any other, Change the Below variable values in epJsperReportPlugin/config/app.yml

username: jasperadmin
password: jasperadmin

c) copy  epJsperReportPlugin/others/jasperCustomPaging.jar file into

Path: "/jasperreports-server-cp-4.2.1/apache-tomcat/webapps/jasperserver/WEB-INF/lib"

Where your jasperserver beign installed and restart your jasperserver.




Note: If your report performing any actions like edit and delete, then while
     creating jaspereport you must give a parameter name as "_show_actions_" and
     include this in your "Action" column.


Pre-candition
---------------
All reports should be uploaded into jasperserver.

Plugin Uses Example
-------------------
a) Your action file function:

 public function executeIndex(sfWebRequest $request) {
        $this->callBack = array($this, "replaceVal");  // This will replace "_show_actions_" parameter with provided option.
        $this->formats = array('pdf',"xls");           // In this array you can give download options.
        $this->reportPath = '/reports/Dashboard';     // Report Path of the jasperserver.
    }


 public function replaceVal($key, $val){ // Call back function
        return "<a href=edit?id=$val&key=$key>edit</a>";
    }

b) In the indexSuccess page include this component and pass the desire parameters.

include_component('jasperReport', 'printReport', array('actionsGenerator' => $callBack, 'exportFormats' => $formats, 'reportPath' => $reportPath));



For paging issue
jasperreports-server-cp-4.2.1/apache-tomcat/webapps/jasperserver/WEB-INF/applicationContext-web-services.xml

instead of "<bean id="managementServiceImpl" class="com.jaspersoft.jasperserver.ws.axis2.ManagementServiceImpl">"
put "<bean id="managementServiceImpl" class="com.tekmindz.jasperserver.ManagementServiceImpl">"
