<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of jasperLogclass
 *
 * @author asingh
 */
class jasperLog {

    public static function getLogPath(){
        return  sfConfig::get('sf_log_dir')."/jasperError/".date('Y-m-d');

    }

    public static function  writeErrorLog($log){
        $logParentDir = self::getLogPath();
        $LogFile= self::CreateLogPath($logParentDir);
        $LogFile= $LogFile.'/'.date('Y-m-d_h:i:s').".txt";

        self::writeLogtoFile($LogFile,$log);

    }
    private function writeLogtoFile($log_message_file,$log){
        $fp = fopen($log_message_file,"a");
        fwrite($fp,$log);
        fclose($fp);
    }

    private  function CreateLogPath($parentLogFolder){

        $logPath =$parentLogFolder;
        if(is_dir($logPath)=='')
        {
            $dir_path=$logPath."/";
            mkdir($dir_path,0777,true);
            chmod($dir_path, 0777);
        }
        return $logPath;

    }
}
?>
