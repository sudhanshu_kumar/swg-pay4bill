<?php
// TODO - see how to handle this 
error_reporting(0);
class jasperReport{

    public static $RUN_OUTPUT_FORMAT_PDF = "PDF";
    public static $RUN_OUTPUT_FORMAT_HTML = "HTML";
    public static $RUN_OUTPUT_FORMAT_XLS = "XLS";
    public static $RUN_OUTPUT_FORMAT_XML = "XML";
    public static $RUN_OUTPUT_FORMAT_CSV = "CSV";
    public static $RUN_OUTPUT_FORMAT_RTF = "RTF";
    public static $RUN_OUTPUT_IMAGES_URI = "IMAGES_URI";

    private $callBackFunction = null;
    private $availableExportOptions= array('PDF','HTML','XLS','CSV','RTF','XML') ;

    private $requiredExportOptions = array();

    public function jasperReport ($actionsCallback = null, $exportOptions = null) {

        $this->requiredExportOptions = $exportOptions;
        $this->callBackFunction = $actionsCallback;
    }

    function  getJasperReport($currentUri, $format, $exportOption=null){
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Asset', 'Tag'));
        $request = sfContext::getInstance()->getRequest();
        $jsperClientObj=  new jasperClient();
        // 1 Get the ReoportUnit ResourceDescriptor...
        $reportData= array();
        $result = $jsperClientObj->ws_get($currentUri);

        if (get_class((object)$result) == 'SOAP_Fault')
        {
            $errorMessage = $result->getFault()->faultstring;
            jasperLog::writeErrorLog("Error executing report (SOAP Fault): ". $errorMessage);
            throw new Exception("Error executing report (SOAP Fault): ". $errorMessage);
        }
        else
        {
            $folders = $jsperClientObj->getResourceDescriptors($result);
        }

        if (count($folders) != 1 || $folders[0]['type'] != 'reportUnit')
        {
            jasperLog::writeErrorLog("Invalid RU ($currentUri)");
            throw new Exception("Invalid RU ($currentUri)");
        }

        $reportUnit = $folders[0];

        // 2. Prepare the parameters array looking in the $_GET for params
        // starting with PARAM_ ...
        //

        $report_params = array();
        if ( $format == RUN_OUTPUT_FORMAT_HTML) {
            $report_params[_show_actions_] = "1";
        }

        // $moveToPage = "executeReport.php?uri=$currentUri";
        $moveToPage ='';
        $exportparam='';
        $count=1;
        $paramHolder = $request->getParameterHolder();

        foreach (array_keys($paramHolder->getAll()) AS $param_name)
        {
            if (in_array($param_name, array('module','action'))) {
                continue;
            }
            if (strncmp("PARAM_", $param_name,6) == 0)
            {
                $report_params[substr($param_name,6)] = $request->getParameter($param_name);

            } if ($param_name != "page" && $param_name!='format')
            {
                if($count==1){
                    $moveToPage .= "?".urlencode($param_name)."=". urlencode($request->getParameter($param_name));
                }else{
                    $moveToPage .= "&".urlencode($param_name)."=". urlencode($request->getParameter($param_name));
                }
                $count++;
            }

        }

        $exportparam= $moveToPage;

        // TODO - fix it so that we are no longer dependent on _GET super global
        if((count($_GET)==1 and isset($_GET['page'])) || count($_GET)==0){
            $moveToPage.="?page=";
        }else{
            $moveToPage.="&page=";
        }



        // 3. Execute the report
        $output_params = array();
        $output_params[RUN_OUTPUT_FORMAT] = $format;

        if ( $format == RUN_OUTPUT_FORMAT_HTML)
        {
            $page = 0;
            if ($request->hasParameter('page')) {
                $page = $request->getParameter('page');
            }
            $output_params[RUN_OUTPUT_PAGE] = $page;
        }

        $result = $jsperClientObj->ws_runReport($currentUri, $report_params,  $output_params, $attachments);

        // 4.
        if (get_class((object)$result) == 'SOAP_Fault')
        {
            $errorMessage = $result->getFault()->faultstring;
            jasperLog::writeErrorLog("<pre>".print_r($paramHolder,true)."</pre>".$errorMessage);
            throw new Exception($errorMessage);



        }

        $operationResult = $jsperClientObj->getOperationResult($result);
        if ($operationResult['returnCode'] != '0')
        {
            // throw new Exception( "Error executing the report:$operationResult[returnMessage]");
            jasperLog::writeErrorLog("<pre>".print_r($paramHolder,true)."</pre>"."Error executing the report:$operationResult[returnMessage]");
            return 0;


        }

        if (is_array($attachments))
        {

            if ($format == RUN_OUTPUT_FORMAT_PDF)
            {
                return $reportData[]= array('report'=>$attachments["cid:report"], //#
                    'content-type'=> 'application/pdf', //#
                    'content-disposition' => 'attachment; filename="report.pdf"');
            }
            else if ($format == RUN_OUTPUT_FORMAT_XLS)
            {


                return $reportData[]= array('report'=>$attachments["cid:report"], //#
                    'content-type'=> 'application/xls',  //#
                    'content-disposition' => 'attachment; filename="report.xls"');
            }
            // TODO - add remaining mime type here
            else if ($format == 'HTML')
            {
                // 1. Save attachments....

                // 2. Print the report....


                // 		header ( "Content-type: text/html");

                foreach (array_keys($attachments) as $key)
                {
                    if ($key != "cid:report")
                    {
                        $f = fopen("images/".substr($key,4),"w");
                        fwrite($f, $attachments[$key]);
                        fclose($f);
                    }
                }

                $totalPage= strtok( $operationResult['returnMessage'],'TotalPages:');
                $pagingPrv='';
                $pagingNxt='';
                $prevpage = ($page > 0) ? $page-1 : 0;
                $nextpage = $page+1;
                $pagingPrv= $moveToPage.$prevpage;
                $firstPage= $moveToPage.'0';
                $lastPage= $moveToPage.($totalPage-1);

                if($nextpage < $totalPage){
                    $pagingNxt= $moveToPage.$nextpage;
                }else{
//                    $pagingNxt='';
                      $pagingNxt=$moveToPage.'0';
                }

                $pageCount= $page+1;

                $paging = "<div id=pageId><a href=$firstPage >".image_tag("../epJsperReportPlugin/images/first.png",array('alter'=>'First Page','align'=>'top','border'=>0))."</a> <a href=$pagingPrv>".image_tag("../epJsperReportPlugin/images/prv.png",array('alter'=>'Prv Page','align'=>'top','border'=>0)). "</a> Page $pageCount of $totalPage
                             <a href=$pagingNxt>".image_tag("../epJsperReportPlugin/images/next.png",array('alter'=>'Next Page', 'align'=>'top','border'=>0)). "</a> <a href=$lastPage>".image_tag("../epJsperReportPlugin/images/last.png",array('alter'=>'Last Page', 'align'=>'top','border'=>0)). " </a> </div>";



                $exportPopupHtml='';
                $reportHtmlData = $attachments["cid:report"];
                if ($this->callBackFunction) {
                    $reportHtmlData = $this->replaceContents($reportHtmlData);
                }

                if(count($this->requiredExportOptions) >0){
                    $exportPopupHtml= $this->exportContent($exportparam);

                }
                return $reportData[]= array('paging'=>$paging,'report'=>$reportHtmlData,'export'=>$exportPopupHtml);


            }


        } else{
            jasperLog::writeErrorLog("<pre>".print_r($paramHolder,true)."</pre>"."No attachment found!");
            throw new Exception("No attachment found!");

        }

    }

    private function replaceContents($data) {
        return preg_replace_callback( "/%([a-zA-Z0-9]+)%([a-zA-Z0-9]+)%/",
            array($this, "replaceCallback"), $data);
    }

    private function replaceCallback($foundText) {
        return call_user_func($this->callBackFunction, $foundText[1], $foundText[2]);
    }

    private function exportContent($exportparam) {

        $this->requiredExportOptions= array_map(strtoupper, array_unique($this->requiredExportOptions));
        $commanArray= array_intersect($this->availableExportOptions,$this->requiredExportOptions);

        $download='';
        if(empty($exportparam)){
            $format='?format=';
        }else{
            $format='&format=';
        }
        if(count($commanArray) >0){
//            $download.= "<a href=javascript:showDownload() id='jsperDownloadId'>".image_tag('../epJsperReportPlugin/images/download-button.png',array('title'=>'Download'))."</a>";
//            $download.= "<table id=jasper_exportId>";
//            foreach($commanArray as $options){
//                $download.= "<tr><td> <a href=$exportparam$format$options>$options</a></td></tr>";
//            }
//            $download.= "</table>";
            $download.= '<div style="float:right;padding-right:10px;">';
            $download.= "<table id=jasper_exportId1>";
            $downloadimages = array('pdf_icon.png','export_icon.png');
            $i = 0;
            $download .= '<tr><td>';
            
            $title = 'Download';
            foreach($commanArray as $options){
                if($downloadimages[$i] == 'export_icon.png') $title = 'Export to XLS';
                if($downloadimages[$i] == 'pdf_icon.png') $title = 'Export to PDF';
                $download.= "<a href=$exportparam$format$options>".image_tag('../epJsperReportPlugin/images/'.$downloadimages[$i],array('title'=>$title,'alt'=>$title, 'border'=>0))."</a>&nbsp;";
                $i++;
            }
            $download .= '</tr></td>';
            $download.= "</table>";
            $download.= '</div>';
        }

        return $download;
    }
}

?>
