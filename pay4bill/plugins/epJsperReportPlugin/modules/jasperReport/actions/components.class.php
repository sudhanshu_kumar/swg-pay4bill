<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of jasperReportComponentsclass
 *
 * @author asingh
 */
class jasperReportComponents extends sfComponents {

    public function executePrintReport() {
        $reportPath = $this->getVar('reportPath');
        if (empty($reportPath)) {
            throw new exception ('No report path specified');
        }
        $callBack = null;
        $exportFormats = null;
        if ($this->getVar('actionsGenerator')) {
            $callBack = $this->getVar('actionsGenerator');
        }
        if ($this->getVar('exportFormats')) {
            $exportFormats = $this->getVar('exportFormats');
        }
        $jasperObj= new jasperReport($callBack,$exportFormats);
        $getFormat='';
        $reportFormat = '';
        $reportFormat=jasperReport::$RUN_OUTPUT_FORMAT_HTML;

        if($this->hasRequestParameter('format')){
            $getFormat= $this->getRequestParameter('format');
            if($getFormat=='PDF'){
                $reportFormat=jasperReport::$RUN_OUTPUT_FORMAT_PDF;
            }elseif($getFormat=='XLS'){
                $reportFormat=jasperReport::$RUN_OUTPUT_FORMAT_XLS;
            }
        }
        $result= $jasperObj->getJasperReport($reportPath, $reportFormat);
        // see if this report is supposed to be downloaded
        if (array_key_exists('content-type', $result)) {
            header ( 'Content-type: '.$result['content-type'] );
            header ( 'Content-Disposition: '.$result['content-disposition']);
            echo $result['report'];die;
        }
        $this->includeResources();
        $this->htmlView = $result;
        $this->finalReport=  $this->getReportBody($result['report']);
    }

    private function getReportBody($repoData) {
        $mr = preg_match('/<body[^>]+>(.*)<\/body>/ims',$repoData,$matches);
        return $matches[1];
    }

    private function includeResources() {
        $basePath = sfConfig::get('sf_epJsperReportPlugin_web_dir', '/epJsperReportPlugin');
        $jasperCss = $basePath . '/css/jasper_main.css';
        $jasperJs = $basePath.'/js/jasper.js';
        $this->addCss = $jasperCss;
        $this->addJs = array($jasperJs);
    }
}