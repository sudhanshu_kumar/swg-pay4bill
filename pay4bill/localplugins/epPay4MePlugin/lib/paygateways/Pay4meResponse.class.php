<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Pay4meResponse {

    private $redirect_url;
    private $merchant_code;
    private $merchant_key;

    public function Pay4meResponse($merchant_code,$merchant_key,$version='v1'){
        $this->merchant_code = $merchant_code;
        $this->merchant_key = $merchant_key;
        $this->logger = new Pay4meLog("L_ALL");
        $this->schema_url = 'http://www.pay4me.com/schema/pay4meorder/'.$version;
    }

    public function pay4mePaymentResponse($reponse_arr,$items){
        $ParsedResponse_arr = $reponse_arr;

        switch( $reponse_arr[0] ) {
        case 200:
          // Success
           if(isset($ParsedResponse_arr[0]) && $ParsedResponse_arr[0]== 'ERR'){
                $this->redirect_url = 'home/error404';
            } elseif(isset($ParsedResponse_arr[3]) &&
                    $ParsedResponse_arr[3]=='order-redirect') {
                $this->redirect_url = $ParsedResponse_arr[1];
            } elseif(isset($ParsedResponse_arr[3]) && $ParsedResponse_arr[3]== 'error'){
                  if(isset($ParsedResponse_arr[2]) && $ParsedResponse_arr[2]==2){
                      $this->redirect_url = 'home/error404?message=waitingResponse';
                  } else {
                      $this->redirect_url  = 'home/error404?message=paymentError';
                  }
            } else {
                $this->redirect_url = 'home/error404?message=paymentError';
            }
          break;
        default:
            $this->redirect_url = 'home/error404?message=paymentError';
          break;
      }

        $fileName = "/OrderRedirect_".date('Y-m-d_h:i:j')."_".
                $items['item_number']."_".$items['transaction_number'].".txt";
        return $this->redirect_url;


    }

    /**
     * @access private
     */
    public function GetParsedXML($response){
      if(!is_null($response)) {

        $this->response = $response;

        $this->xml_parser = new gc_XmlParser($this->response);
        $this->root = $this->xml_parser->GetRoot();
        $this->data = $this->xml_parser->GetData();
      }
      return array($this->root, $this->data);
    }

    /**
     * Set the response header indicating an erroneous authentication from
     * Pay4me
     *
     * @param string $msg the message to log
     */
    private function SendFailAuthenticationStatus($msg="401 Unauthorized Access",
                                                                   $die=true) {

      header('WWW-Authenticate: Basic realm="Pay4me Authentication Failure"');
      header('HTTP/1.0 401 Unauthorized');
      if($die) {
       die($msg);
      } else {
        echo $msg;
      }
    }

    /**
     * Set the response header indicating a malformed request from Pay4me
     *
     * @param string $msg the message to log
     */
    public function SendBadRequestStatus($msg="400 Bad Request", $die=true) {
      $this->logger->logError($msg);
      header('HTTP/1.0 400 Bad Request');
      if($die) {
       die($msg);
      } else {
      echo $msg;
      }
    }

    /**
     * Set the response header indicating that an internal error ocurred and
     * the notification sent by Pay4me can't be processed
     *
     * @param string $msg the message to log
     */
    public function SendServerErrorStatus($msg="500 Internal Server Error",
                                                                   $die=true) {
      $this->logger->logError($msg);
      header('HTTP/1.0 500 Internal Server Error');
      if($die) {
       die($msg);
      } else {
        echo $msg;
      }
    }

    /**
     * Send an acknowledgement in response to Merchant request
     * @param string $serial serial number of notification for acknowledgement
     */
    public function SendAck($serial=null, $die=true) {
      $this->SendOKStatus();
      $acknowledgment = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" .
                        "<notification-acknowledgment xmlns=\"" .
                        $this->schema_url."\"";
      if(isset($serial)) {
        $acknowledgment .=" serial-number=\"" . $serial."\"";
      }
      $acknowledgment .= " />";
      $logfile = "/Pay4meNotificationResponse-".date('Y-m-d_h:i:s').".txt";
      $this->logger->LogData($acknowledgment,$logfile);
      if($die) {
        die($acknowledgment);
      } else {
        echo $acknowledgment;
      }
    }

    /**
     * Verifies that the authentication sent by Pay4me matches the
     * merchant id and key
     *
     * @param string $headers the headers from the request
     */
    public function HttpAuthentication($headers=null, $die=true) {
      if(!is_null($headers)) {
        $_SERVER = $headers;
      }
      if(isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW'])) {
        $compare_mer_id = $_SERVER['PHP_AUTH_USER'];
        $compare_mer_key = $_SERVER['PHP_AUTH_PW'];
      }
  //  IIS Note::  For HTTP Authentication to work with IIS,
  // the PHP directive cgi.rfc2616_headers must be set to 0 (the default value).
      else if(isset($_SERVER['HTTP_AUTHORIZATION'])){
        list($compare_mer_id, $compare_mer_key) = explode(':',
            base64_decode(substr($_SERVER['HTTP_AUTHORIZATION'],
            strpos($_SERVER['HTTP_AUTHORIZATION'], " ") + 1)));
      } else if(isset($_SERVER['Authorization'])) {
        list($compare_mer_id, $compare_mer_key) = explode(':',
            base64_decode(substr($_SERVER['Authorization'],
            strpos($_SERVER['Authorization'], " ") + 1)));
      } else {
        $this->SendFailAuthenticationStatus(
              "Failed to Get Basic Authentication Headers",$die);
        return false;
      }
      if($compare_mer_id != $this->merchant_code
         || $compare_mer_key != $this->merchant_key) {
        $this->SendFailAuthenticationStatus("Invalid Merchant Id/Key Pair",$die);
        return false;
      }
      return true;
    }

    private function SendOKStatus() {
        header('HTTP/1.0 200 OK');
    }
}
?>
