<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Pay4meV1Client extends AbstractPay4meClient {

    protected $merchant_code = null;
    protected $merchant_key = null;
    protected $payment_url = null;
    protected $version = "v1";
    protected $defaultXmlNs = "http://www.pay4me.com/schema/pay4meorder/v1";
    protected $logger;
    protected $merchant_service_id;

    function Pay4meV1Client($merchant_code, $merchant_key, $payment_url) {
        $this->merchant_code = $merchant_code;
        $this->merchant_key = $merchant_key;
        $this->payment_url = $payment_url;
        $this->logger = new Pay4meLog("L_ALL");
    }

    protected function generateRequestXML($item) {
        $version = $this->version;
        $merchant_service_id = $this->merchant_service_id;
        //require_once(dirname(__FILE__).'/xml-processing/gc_xmlbuilder.php');
        $xml_data = new gc_XmlBuilder();
        $xml_data->Push('order', array('xmlns' => $this->defaultXmlNs));
        $xml_data->Push('merchant-service', array('id' => $merchant_service_id));
        if (!isset($item) || count($item) == 0) {
            return false;
        }

        if (isset($item['item_number']))
            $xml_data->Push('item', array('number' => $item['item_number']));
        else
            $xml_data->Push('item', array('number' => ''));
        if (isset($item['transaction_number']))
            $xml_data->Element('transaction-number', $item['transaction_number']);
        if (isset($item['name']))
            $xml_data->Element('name', $item['name']);
        if (isset($item['description']))
            $xml_data->Element('description', $item['description']);
        if (isset($item['price']))
            $xml_data->Element('price', $item['price']);
        if (isset($item['currency']))
            $xml_data->Element('currency', $item['currency']);
        $xml_data->Push('parameters');
        if (isset($item['parameters'])) {
            foreach ($item['parameters'] as $paramkey => $parameter) {
                $xml_data->Element('parameter', $parameter, array('name' => $paramkey));
            }
        }
        $xml_data->Pop('parameters');
        $xml_data->Pop('item');

        $xml_data->Pop('merchant-service');
        $xml_data->Pop('order');
        return $xml_data->GetXML();
    }

    protected function saveResponse($payment_notification) {

        $notificationResponseArray =
                array(
                    'payment_status' =>
                    $payment_notification['item']['payment-information']['status']['code']['VALUE'],
                    'item_number' =>
                    $payment_notification['item']['number'],
                    'transaction_number' =>
                    $payment_notification['item']['transaction-number']['VALUE'],
                    'merchant_service_id' =>
                    $payment_notification['id'],
                    'validation_number' =>
                    $payment_notification['item']['validation-number']['VALUE'],
                    'bank_name' =>
                    $payment_notification['item']['payment-information']['bank']['name']['VALUE'],
                    'branch_name' =>
                    $payment_notification['item']['payment-information']['bank']['branch']['VALUE'],
                    'payment_mode' =>
                    $payment_notification['item']['payment-information']['mode']['VALUE'],
                    'total_amount' =>
                    $payment_notification['item']['payment-information']['amount']['VALUE'],
                    'currency' =>
                    $payment_notification['item']['payment-information']['currency']['VALUE'],
                    'payment_date' =>
                    $payment_notification['item']['payment-information']['payment-date']['VALUE'],
                    'description' =>
                    $payment_notification['item']['payment-information']['status']['description']['VALUE']);
        $merchantApiObj = new MerchantDbApi();
        $merchantApiObj->saveResponse($notificationResponseArray);
    }

    protected function savePaymentRequest($itemArray) {

        $item = $itemArray['item_details'];
        $itemArray =
                array(
                    'merchant_service_id' => $itemArray['merchant_service_id'],
                    'item_number' => $item['item_number'],
                    'transaction_number' => (isset($item['transaction_number'])) ? $item['transaction_number'] : '',
                    'currency' => (isset($item['currency'])) ? $item['currency'] : '',
                    'payment_type' => (isset($item['payment_type'])) ? $item['payment_type'] : '',
                    'buyer_ip_address' => (isset($item['buyer_ip_address'])) ? $item['buyer_ip_address'] : '',
                    'description' => (isset($item['description'])) ? $item['description'] : '',
                    'price' => (isset($item['price'])) ? $item['price'] : '');
        $merchantApiObj = new MerchantDbApi();
        return $merchantApiObj->saveRequest($itemArray);
    }

}

?>
