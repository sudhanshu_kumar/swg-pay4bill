Please find the corresponding names of the report:
						Reports						Sub Reports
						=======						===========
Admin/SubAdmin:
==============
Invoice 					a) Admin_Invoice.jrxml       			b) Admin_Invoice_Subreport.jrxml
Payment By Customer				a) Admin_PaymentByCustomer.jrxml       		b) Admin_PaymentByCustomer_Subreport.jrxml
Payment By Customer(View Invoices)		a) Admin_ViewInvoice_PaymentByCustomer.jrxml  	b) Admin_ViewInvoice_PaymentByCustomer_Subreport.jrxml
Customer With Pending Invoices			a) Admin_CustomerWithPendingInvoice.jrxml       b) Admin_CustomerWithPendingInvoice_Subreport.jrxml
Customer With Pending Invoices(ViewInvoice)	a) Admin_ViewInvoice.jrxml       		b) Admin_ViewInvoice_Subreport.jrxml
Bank Collection					a) Admin_BankCollection.jrxml       		b) Admin_BankCollection_Subreport.jrxml
View Merchants(Bank Collection)			a) Admin_AllMerchantPerBank.jrxml		b) Admin_AllMerchantPerBank_Subreport.jrxml


Merchant:
========
Invoice 					a) Merchant_Invoice.jrxml       		b) Merchant_Invoice_Subreport.jrxml
Payment By Customer				a) Merchant_PaymentByCustomer.jrxml       	b) Merchant_PaymentByCustomer_Subreport.jrxml
Payment By Customer(View Invoices)		a) Merchant_ViewInvoice_PaymentByCustomer.jrxml b) Merchant_ViewInvoice_PaymentByCustomer_Subreport.jrxml
Customer With Pending Invoices			a) Merchant_CustomerWithPendingInvoice.jrxml    b) Merchant_CustomerWithPendingInvoice_Subreport.jrxml
Customer With Pending Invoices(ViewInvoice)	a) Merchant_ViewInvoice.jrxml       		b) Merchant_ViewInvoice_Subreport.jrxml
Bank Collection					a) Merchant_BankCollection.jrxml       		b) Merchant_BankCollection_Subreport.jrxml

