<?php $item_info_obs = Doctrine::getTable('InvoiceItemDetails')->findBy('invoice_id', $invoice_id); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Untitled Document</title>
    </head>
    <body>
        <table align="center" cellpadding="0" cellspacing="0" style="width:650px;">
            <tr>
                <td style="padding-bottom:5px;">
                   <?php include_partial('global/pay4bill_logo', array('path' => $path));    ?> 
                </td>
            </tr>
            <tr>
                <td>
            <table cellspacing="0" cellpadding="10" border="0" align="center" bgcolor="#f0f0f0" style="width:650px; border-collapse:collapse; border:1px solid #dedcdc; color:#646464; font-family:Arial, Helvetica, sans-serif; font-size:14px;">
                <tr>
                    <td>Dear  <span><?php echo $customerName; ?></span>,</td>
                </tr>
                <tr>
                    <td>
                        You have a bill waiting for payment at
                        <a href="www.pay4bill.com" style="color:#0c5c89;">www.pay4bill.com</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        The Invoice Detail is as follows:
                    </td>
                </tr>
                <tr>
                    <td><strong style="color:#000">Customer Id: </strong><?php echo $item_info_obs->getFirst()->getInvoice()->getCustomer()->getCustomerId(); ?></td>
                </tr>
                <tr>
                    <td><strong style="color:#000">Issue Date: </strong><?php echo $issueDate; ?></td>
                </tr>
                <tr>
                    <td><strong style="color:#000">Delivery Date: </strong><?php echo $deliveryDate; ?></td>
                </tr>
                <tr>
                    <td><strong style="color:#000">Due Date: </strong><?php echo $dueDate; ?></td>
                </tr>
                <tr>
                    <td><strong style="color:#000">Invoice Number: </strong><?php echo $invoiceNo; ?></td>
                </tr>
                <tr>
                    <td>
                        <table border="0" cellpadding="1" cellspacing="1" style="background:#8d8d8d;" width="50%">
                            <tr><td style='background:#f0f0f0'><b>Quantity</b></td><td style='background:#f0f0f0'><b>Description</b></td><td style='background:#f0f0f0'><b>Cost</b></td></tr>
                            <?php
                            $r = new InvoiceItemDetails();

                            $subttl = 0;
                            $ttl_tax = 0;
                            foreach ($item_info_obs as $item_ob) {
                                echo "<tr>";
                                echo "<td style='background:#f0f0f0'>" . $item_ob->getNoOfItems() . "</td>";
                                echo "<td style='background:#f0f0f0'>" . $item_ob->getItemDescription() . "</td>";
                                echo "<td style='background:#f0f0f0' align='right'>" . number_format($item_ob->getItemPrice(),'2','.',',') . "</td>";
                                echo "</tr>";
                                $subttl+=$item_ob->getSubTotal();
                                $ttl_tax+=$item_ob->getTaxTotal();
                            }
                            echo "</table>";
                            echo "<br><table>
                            <tr>
                            <td>Sub-Total:</td>
                            <td>NGN</td>
                            <td align='right'>".number_format($subttl,'2','.',',')."</td>
                            </tr>
                            <tr>
                            <td>Total Tax:</td>
                            <td>NGN</td>
                            <td align='right'>".number_format($ttl_tax,'2','.',',')."</td>
                            </tr>
                            <tr>
                            <td>Total:</td>
                            <td>NGN</td>
                            <td align='right'>" .number_format(($subttl + $ttl_tax),'2','.',',') . "</td>
                                </tr>
                            </table>";
                            ?>
                            <tr>
                                <td><strong style="color:#000">Additional Information: &nbsp;</strong><?php echo $additionalInfo; ?></td>
                            </tr>
                            <?php if($item_info_obs->getFirst()->getInvoice()->getMerchant()->getPaymentType() == 'b') { ?>
                            <tr>
                                <td>Please go to one of the registered banks to make payment of your invoice.</td>
                            </tr>
                            <?php } elseif($item_info_obs->getFirst()->getInvoice()->getMerchant()->getPaymentType() == 'o') { ?>
                            <tr>
                                <td>Please click on the link below and login to make payment:</td>
                            </tr>
                            <tr>
                                <td><a href="<?php echo $completeUrl; ?>" style="color:#0c5c89;"><?php echo $completeUrl; ?></a></td>
                            </tr>
                            <?php } ?>
                            <tr><td>Regards,<br />The Pay4bill Team</td></tr>

                            <tr><td style="background-color:#18648e; color:#ffffff; padding:10px; font:14px Arial, Helvetica, sans-serif;">If you experience any problem, write to us at <a href="mailto:support@pay4bill.com" style="color:#fff">
                                        support@pay4bill.com</a></td></tr>

                        </table>
                    </td></tr>

                        </table>
 </td></tr>
</table>
                        </body>
                        </html>