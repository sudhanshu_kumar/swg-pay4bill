<?php
if (!isset($hidePaybutton)) {

    $hidePaybutton = false;
}
?>
<script language="javascript" >

    $(document).ready(function(){

        $('#<?php echo $highlighted_menu; ?>').addClass('active');
    }
);
</script>

<div id="pgMenu">
    <div id="wrappernew">
        <div class="pgTitle">
            <h2>Invoice - <?php echo $invoice->getInvoiceNo() ?></h2>
        </div>
        <!-- <div class="pgNav"> </div>-->

    </div>

</div>

<div id="wrappernew">
    <div id="content">
        <br clear="all"/>

        <div id="address" >
            <div class="customer"><h4> Customer Details</h4>
                <br  clear="all"/>

                <div style="text-transform:uppercase; font-weight:bold;"><?php echo $invoice->Customer->getName(); ?></div>


                <div class="clear_new"></div>
                <div style="font-weight:normal"><?php echo wordwrap($invoice->Customer->getAddress(), 45, "<br />\n", true); ?></div>
            </div>
            <div class="merchant">   <h4> Merchant Details </h4>
                <br  clear="all"/>
                <div style="text-transform:uppercase; font-weight:bold;"><?php echo $invoice->Merchant->getMerchantName(); ?></div> <div class="clear_new"></div>
                <div style="font-weight:normal;"><?php echo $invoice->Merchant->getMerchantContactAddress(); ?></div></div>
<?php
//Varun : Helper added for proper amount format
$helperOb = new csvHelper();
$logo = $invoice->Merchant->getCompanyLogo();
if (file_exists(sfConfig::get('sf_upload_dir') . '/merchant_logo/' . $logo) && !empty($logo)) {
?>
            <div style=" float:right; margin-bottom: 25px;"> <img src="<?php echo _compute_public_path($invoice->Merchant->getCompanyLogo(), 'uploads/merchant_logo', '', true); ?>" style="height: 75px; max-width:120px;" alt="Merchant Logo" class="" /> </div>
            <?php } ?>
            <div style="clear:both"></div>
        </div>
        <div class="TableForm">
            <table border="0" cellspacing="0" cellpadding="0" class="print_table">

                <tr>

                    <th>Invoice Number                        </th>
                    <th>Delivery Date                           </th>
                    <th>Due Date                      </th>
                    <th>Currency                      </th>
<?php if ($invoice->getPartPayment() != 0 && $invoice->getPayFullAmount() != 'Y') { ?>
                    <th>Part Payment %</th>
<?php } ?>
                    <?php if ($recurring_plan) {
                    ?><th>Recurring Plan</th>
<?php } ?>
                    <?php if ($invoice->getStatus() != 2) {
 ?> <th>Amount Due                      </th><?php } ?>
                    <th>Amount Paid                      </th>



<?php if ($invoice->getStatus() == 2 or $invoice->getStatus() == 3) { ?> <th>Payment Date                     </th><?php } ?>
                    <th>Status                      </th>
                </tr>


                <tr>
                    <td><?php echo $invoice->getInvoiceNo() ?></td>
                    <td><?php echo date("d-M-Y", strtotime($invoice->getDeliveryDate())) ?></td>
                    <td><?php echo date("d-M-Y", strtotime($invoice->getDueDate())) ?></td>
                    <td><?php echo ucfirst($invoice->getCurrencyType()) ?></td>
<?php if ($invoice->getPartPayment() != 0 && $invoice->getPayFullAmount() != 'Y') { ?>
                        <td align="center"><?php echo $invoice->getPartPayment(); ?></td>
<?php } ?>
<?php
                    if ($recurring_plan) {
?><td ><?php echo ucfirst($recurring_plan->getType()); ?><?php if ($recurring_plan->getValue() != '') { ?>-<?php echo $recurring_plan->getValue() . " Days";
                        } ?></td>
                    <?php
                    }
                    ?>
                    <?php if (!($invoice->getStatus() == 2 )) {
                    ?>  <td align="center"><?php echo $helperOb->price_format($total - $invoice->getAmountPaid()); ?></td><?php } ?>

                    <td align="center"><?php echo $helperOb->price_format($invoice->getAmountPaid()); ?></td>



                        <?php if ($invoice->getStatus() == 2 or ($invoice->getStatus() == 3)) { ?> <td align="center"><?php
                            echo date("d-M-Y", strtotime($invoice->getPaymentDate()));
                        ?></td>
<?php } ?>   <td> <?php
                        /* In case of teller: Pending need to show */
                        if (isset($_SESSION['logedin_user_group']) && $_SESSION['logedin_user_group'] == 'teller') {
                            echo image_tag('pending.png', array('alt' => "pending", 'title' => "pending"));
                        } else {
                            if ((strtotime($invoice->getDueDate()) < strtotime(date("Y-m-d"))) && ($invoice->getStatus() != 2 && $invoice->getStatus() != 3) && $invoice->getMerchant()->getPaymentType()=='o') {
                                echo image_tag('expired.png', array('alt' => "Expired", 'title' => "Expired"));
                            }elseif ((strtotime($invoice->getDueDate()) < strtotime(date("Y-m-d"))) && ($invoice->getStatus() != 2 && $invoice->getStatus() != 3) && $invoice->getMerchant()->getPaymentType()=='b') {
                               echo image_tag('pending.png', array('alt' => "pending", 'title' => "pending"));
                            } else if ((strtotime($invoice->getDueDate()) < strtotime(date("Y-m-d"))) && $invoice->getStatus() == 3) {
                                echo image_tag('partiallypaid_expired.png', array('alt' => "PartiallyPaid/Expired", 'title' => "PartiallyPaid/Expired"));
                            } else {
                                if ($invoice->getStatus() == 0) {
                                    echo image_tag('pending.png', array('alt' => "pending", 'title' => "pending"));
                                } else if ($invoice->getStatus() == 3) {
                                    echo image_tag('partially-paid.png', array('alt' => "Partially Paid", 'title' => "Partially Paid"));
                                } else if ($invoice->getStatus() == 2) {
                                    echo image_tag('paid.png', array('alt' => "Paid", 'title' => "Paid"));
                                } else if ($invoice->getStatus() == 1) {
                                    echo image_tag('cancel.png', array('alt' => "Cancelled", 'title' => "Cancelled"));
                                }
                            }
                        }
?></td>
                </tr>
            </table>

        </div>

        <div class="TableForm">
            <table border="0" cellspacing="0" cellpadding="0" class="print_table">
                <thead>
                <th >Item Description</th>
                <th >Number of Item</th>
                <th >Item Price</th>
                <th >Subtotal</th>
                <th >Tax(%)</th>
                <th >Tax Amount</th>
                </thead>

                <tbody><?php
//                                                             echo "<pre>";
//                                                                    print_r($items_details);
//                                                                    exit;
                        foreach ($items_details as $listofItems) {
?>
                        <tr>
                            <td align="right">
<?php echo $listofItems['item_description']; ?>
                            </td>
                            <td align="right">
                    <?php echo $listofItems['no_of_items']; ?>
                                </td>
                                <td align="right">
                    <?php echo $helperOb->price_format($listofItems['item_price']); ?>
                                </td>
                                <td align="right">
                            <?php echo $helperOb->price_format($listofItems['subTotal']); ?>
                        </td>
                        <td align="right">
                            <?php echo $listofItems['item_tax']; // Varun : Bug Fix : FS#34883   ?>
                        </td>
                        <td align="right">
                            <?php echo $helperOb->price_format($listofItems['tax_total']); ?>
                        </td>
                    </tr>
                            <?php
                            //
                        }
                            ?></tbody>
            </table>

        </div>

        <div class="clear_new">&nbsp;</div>
        <div class="total_new_table">
            <div class="total">
                <p>Sub-Total: <?php echo $helperOb->price_format($subtotal); ?></p>

                        <p>Total Tax: <?php echo $helperOb->price_format($taxes); ?></p>
                        <p>Total: <?php echo $helperOb->price_format($total); ?></p></div>





<?php if ($customerLogin == '1') { // Varun : Check for customer login ?>
<?php if ($invoice->getDueDate() >= date('Y-m-d') && $invoice->getAmountDue() != 0 && $hidePaybutton == 0) { ?>
                            <h3>Payable Amount in the Transaction: ₦<span id="visAmount"><?php echo number_format($invoice->getAmountDue(), 2, '.', ','); ?></span>  </h3>
<?php if ($invoice->getPartPayment() != 0 && $invoice->getStatus() == 0) { ?><br> <!-- Varun :Bug : FS#35003-->
                                <b>Pay Full Amount</b> &nbsp;&nbsp;<input type="checkbox" onClick="return setAmount(this);" name="full_payment" value="<?php echo number_format($total, 2, ".", ""); ?>" />
<?php
                                    }
                                }
                            }
?>
                        </div>
                        <div class="clear_new">&nbsp;</div>
                        <div style="float:left; margin-top:10px; width:960px;" class="clearfix">
            <?php if ($invoice->getDueDate() >= date('Y-m-d') && $invoice->getStatus() != 1 && $invoice->getStatus() != 2 && $customerLogin == '1' && $paymenttype == 'o' && $hidePaybutton == 0) {
            ?>
                                <form action="<?php echo url_for('payment/payment'); ?>" method="post" name="frmPayment" id="frmPayment">
                                    <input name="instant_pay" type="submit" class="blueBtn" value="Pay" style="float:left" />

                                    <input type="hidden" id="amount" name="amount" value="<?php echo number_format($invoice->getAmountDue(), 2, ".", ""); ?>">



                                    <input type="hidden" name="item_number" value="<?php echo $item_number; ?>">
                                    <input type="hidden" name="invoice_id" value="<?php echo $invoice->getId(); ?>">
                                    <input type="hidden" name="customer_name" value="<?php echo $invoice->Customer->getName(); ?>">
                                    <input type="hidden" id="part_amount" name="part_amount" value="N">
                                    <input type="hidden" name="merchant_id" value="<?php echo $invoice->Merchant->getId(); ?>">
                                    <input type="hidden" name="receipt_number" value="<?php echo $invoice->getInvoiceNo(); ?>">
                                    <input type="hidden" name="merchant_name" value="<?php echo $invoice->Merchant->getMerchantName(); ?>">


                                </form>
<?php } ?><input name="back" type="button" class="blueBtn" value="Back" style="float:left" onclick="javascript:history.go(-1)"/>
                <!--            <input name="print" type="button" class="blueBtn" value="Print" onclick="javascript:window.open('<?php //echo url_for('invoice/print?id=' . $invoice->getId())    ?>','PrintPage','width=750,height=700,scrollbars=1');" />-->
                            <input name="print" type="button" class="blueBtn" value="Print" onclick="javascript:window.print();" />
                        </div>
                    </div>

                </div>
                <p>&nbsp;</p>
                </body>
                </html>

                <script>
                    /*
                     *Purpose: To post full amount while invoice is part payment type
                     * function setAmount
                     *  *  param: chkFullAmount
                     *  Date: 04 Mar 2012
                     */
                    function setAmount(chkFullAmount)
                    {
                        if(chkFullAmount.checked)
                        {
                            document.getElementById('amount').value = <?php echo number_format($total, 2, ".", ""); ?>;
                            document.getElementById('part_amount').value = "Y";
                            document.getElementById('visAmount').innerHTML = '<?php echo number_format($total, 2, ".", ","); ?>'; //Varun : Bug Fix : FS#35005
                        }
                        else
                        {
                            document.getElementById('amount').value =  <?php echo number_format($invoice->getAmountDue(), 2, ".", ""); ?>;
                            document.getElementById('visAmount').innerHTML =  '<?php echo number_format($invoice->getAmountDue(), 2, ".", ","); ?>'; //Varun : Bug Fix : FS#35005
            document.getElementById('part_amount').value = "N";

        }
    }
</script>