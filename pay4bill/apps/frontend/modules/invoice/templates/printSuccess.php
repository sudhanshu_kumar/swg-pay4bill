<style type="text/css">
@media print {
input#close {
display: none;
}
input#print {
display: none;
}
}
</style>
<script language="javascript" >

    $(document).ready(function(){

        $('#<?php echo $highlighted_menu; ?>').addClass('active');
    }
);
</script>

<div id="pgMenu">
    <div id="wrappernew">
        <div class="pgTitle">
            <h2>Invoice - <?php echo $invoice->getInvoiceNo() ?></h2>
        </div>
        <!-- <div class="pgNav"> </div>-->

    </div>

</div>

<div id="wrappernew">
    <div id="content">
        <br clear="all"/>

        <div id="address" >
            <div class="customer" style="width:350px; float:left;"><h4> Customer Details</h4>
                <br  clear="all"/>

                <div style="text-transform:uppercase; font-weight:bold;"><?php echo $invoice->Customer->getName(); ?></div>


                <div class="clear_new"></div>
                <div style="font-weight:normal;"><?php echo $invoice->Customer->getAddress(); ?></div>
            </div>
            <div class="merchant" style="width:300px; float:left;">   <h4> Merchant Details </h4>
                <br  clear="all"/>
                <div style="text-transform:uppercase; font-weight:bold;"><?php echo $invoice->Merchant->getMerchantName(); ?></div> <div class="clear_new"></div>
                <div style="font-weight:normal;"><?php echo $invoice->Merchant->getMerchantContactAddress(); ?></div></div>
                  <div class="pos_logo"><?php
            $logo = $invoice->Merchant->getCompanyLogo();
            if (file_exists(sfConfig::get('sf_upload_dir') . '/merchant_logo/' . $logo) && !empty($logo)) { ?>
            <div style="margin-left: 600px; margin-bottom: 25px;"> <img src="<?php echo _compute_public_path($invoice->Merchant->getCompanyLogo(), 'uploads/merchant_logo', '', true); ?>" style="height: 75px; max-width:120px;" alt="Merchant Logo" class="" /> </div>
                    <?php } ?></div>
        </div>

        <div class="tableHolder">
            <table border="0" cellspacing="0" cellpadding="0">

                <tr>

                    <th>Invoice Number                        </th>
                    <th>Delivery Date                           </th>
                    <th>Due Date                      </th>
                    <th>Currency                      </th>
                    <?php
                    $helperOb = new csvHelper();
                    if ($invoice->getPartPayment() != 0) {
                    ?>
                        <th>Part Payment %</th>
                    <?php } ?>
                    <?php if ($recurring_plan) {
                    ?><th>Recurring Plan</th>
                    <?php } ?>
                    <?php if ($invoice->getStatus() != 2) {
                    ?> <th>Amount Due                      </th><?php } ?>
                    <th>Amount Paid                      </th>

                    <?php if ($invoice->getStatus() == 2 or ((strtotime($invoice->getDueDate()) >= strtotime(date("Y-m-d"))) && $invoice->getStatus() == 3)) {
                    ?> <th>Payment Date                     </th><?php } ?>
                    <th>Status                      </th>
                </tr>


                <tr>
                    <td><?php echo $invoice->getInvoiceNo() ?></td>
                    <td><?php echo date("d-M-Y", strtotime($invoice->getDeliveryDate())) ?></td>
                    <td><?php echo date("d-M-Y", strtotime($invoice->getDueDate())) ?></td>
                    <td><?php echo ucfirst($invoice->getCurrencyType()) ?></td>
                    <?php if ($invoice->getPartPayment() != 0) {
                    ?>
                        <td align="center"><?php echo $invoice->getPartPayment(); ?></td>
                    <?php } ?>
                    <?php
                    if ($recurring_plan) {
                    ?><td ><?php echo ucfirst($recurring_plan->getType()); ?><?php if ($recurring_plan->getValue() != '') {
                    ?>-<?php echo $recurring_plan->getValue() . " Days";
                        } ?></td>
                    <?php
                    }
                    ?>
                        <?php if (!($invoice->getStatus() == 2 )) {
 ?>  <td align="center"><?php echo $helperOb->price_format($total - $invoice->getAmountPaid()); ?></td><?php } ?>

                    <td align="center"><?php echo $helperOb->price_format($invoice->getAmountPaid()); ?></td>



                    <?php if ($invoice->getStatus() == 2 or ((strtotime($invoice->getDueDate()) >= strtotime(date("Y-m-d"))) && $invoice->getStatus() == 3)) {
                    ?> <td align="center"><?php echo date("d-M-Y", strtotime($invoice->getPaymentDate())); ?></td>
                        <?php } ?>   <td> <?php
                        if ((strtotime($invoice->getDueDate()) < strtotime(date("Y-m-d"))) && $invoice->getStatus() != 2 && (isset($_SESSION['logedin_user_group']) && $_SESSION['logedin_user_group'] != 'teller')) {
                            echo image_tag('expired.png', array('alt' => "Expired", 'title' => "Expired"));
                        } else {
                            if ($invoice->getStatus() == 0) {
                                echo image_tag('pending.png', array('alt' => "pending", 'title' => "pending"));
                            } else if ($invoice->getStatus() == 3) {
                                echo image_tag('partially-paid.png', array('alt' => "Partially Paid", 'title' => "Partially Paid"));
                            } else if ($invoice->getStatus() == 2) {
                                echo image_tag('paid.png', array('alt' => "Paid", 'title' => "Paid"));
                            } else if ($invoice->getStatus() == 1) {
                                echo image_tag('cancel.png', array('alt' => "Cancelled", 'title' => "Cancelled"));
                            }
                        }
                        ?></td>
                </tr>
            </table>

        </div>

        <div class="tableHolder">
            <table border="0" cellspacing="0" cellpadding="0">
                <thead>
                <th >Item Description</th>
                <th >Number of Item</th>
                <th >Item Price</th>
                <th >Subtotal</th>
                <th >Tax(%)</th>
                <th >Tax Amount</th>
                </thead>

                <tbody><?php
//                                                             echo "<pre>";
//                                                                    print_r($items_details);
//                                                                    exit;
                        foreach ($items_details as $listofItems) {
                        ?>
                            <tr>
                                <td align="right">
<?php echo $listofItems['item_description']; ?>
                        </td>
                        <td align="right">
<?php echo $listofItems['no_of_items']; ?>
                        </td>
                        <td align="right">
<?php $itemPrice = $helperOb->price_format($listofItems['item_price']); echo $itemPrice; ?>
                        </td>
                        <td align="right">
<?php $subTot = $helperOb->price_format($listofItems['subTotal']);echo $subTot; ?>
                        </td>
                        <td align="right">
<?php $itemTax = $helperOb->price_format($listofItems['item_tax']);echo $itemTax; ?>
                        </td>
                        <td align="right">
<?php $taxTot = $helperOb->price_format($listofItems['tax_total']);echo $taxTot; ?>
                        </td>
                    </tr>
                    <?php
                            //
                        }
                    ?></tbody>
                </table>

            </div>

            <div class="clear_new">&nbsp;</div>
            <div class="total_new_table">
                <div class="total">
                    <h3>Sub-Total: <?php echo $helperOb->price_format($subtotal); ?></h3>

                    <h3>Total Tax: <?php echo $helperOb->price_format($taxes); ?></h3>
                    <h3>Total: <?php echo $helperOb->price_format($total); ?></h3></div>





            <?php if (!$merchnatLogin && !$adminLogin) {
 ?>
<?php if ($invoice->getDueDate() >= date('Y-m-d') && $invoice->getAmountDue() != 0) {
?>
                                <h3>Payable Amount in the Transaction: <?php echo number_format($invoice->getAmountDue(), 2, ".", ""); ?> NGN</h3>
            <?php }
                        } ?></div>
            <div class="hidebuttons"><input name="close" id="close" type="button" class="blueBtn" value="Close" style="float:left" onclick="javascript:window.close()"/>
<input name="print" type="button" class="blueBtn" id="print" value="Print" onclick="javascript:window.print()" /></div>

    </div>

</div>
<p>&nbsp;</p>


</body>
</html>