<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>
<?php
if ( isset($getMerchantTaxDetails) && count($getMerchantTaxDetails) > 0) {
    $getMerchantTaxDetails = $sf_data->getRaw('getMerchantTaxDetails');
}
function validateItems($fieldName){

    if($fieldName == 'item_no0'){
        if(isset($_POST[$fieldName]) && $_POST[$fieldName] == ''){
            return 'Please enter Number of Item';
        }
        if(isset($_POST[$fieldName]) && !is_numeric($_POST[$fieldName])){
            return 'Please enter valid Number of Item';
        }
         if(isset($_POST[$fieldName]) && ($_POST[$fieldName] == 0)){
            return 'Please enter valid Number of Item';
        }
    }
    if($fieldName == 'item_desc0'){
        if(isset($_POST[$fieldName]) && $_POST[$fieldName] == ''){
            return 'Please enter Item Description';
        }
    }
    if($fieldName == 'item_price0'){
        if(isset($_POST[$fieldName]) && $_POST[$fieldName] == ''){
            return 'Please enter Item Unit Price';
        }
        if(isset($_POST[$fieldName]) && !is_numeric($_POST[$fieldName])){
            return 'Please enter valid Item Unit Price';
        }
    }
}

?>
<div id="Formwraper">
<form action="<?php echo url_for('invoice/' . (($form->getObject()->isNew() && $id=='')  ? 'create' : 'update') . ((!$form->getObject()->isNew() || $id!='') ? '?id=' .$id : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?> name="create_invoice">
  <?php if (!$form->getObject()->isNew()): ?>
  <input type="hidden" name="sf_method" value="put" />
  <?php endif; ?>
  <h3>Create Invoice</h3>
  <div class="sform">
    <h3>Customer Information</h3>
    <div class="TableForm">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td width="35%" align="right"><?php echo $form['customer_id']->renderLabel() ?></td>
          <td><?php echo $form['customer_id']->render() ?>
                  <div style="color:red" id="err_customer">&nbsp;<?php echo $form['customer_id']->renderError(); ?></div></td>
        </tr>
      </table>
    </div>
    <h3>Invoice Date Information</h3>
    <div class="TableForm">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td width="35%" align="right"><?php echo $form['issue_date']->renderLabel() ?></td>
          <td><?php echo $form['issue_date']->render() ?> <span id="dispErr_date" style="display: none;"></span>
            <label for="invoice_issue_date_day" generated="false" class="error" id="label_error"></label>
            <div style="color:red" id="err_issue_date">&nbsp;<?php echo $form['issue_date']->renderError(); ?></div> </td>
        </tr>
        <tr>
          <td align="right"><?php echo $form['delivery_date']->renderLabel() ?> </td>
          <td><?php echo $form['delivery_date']->render() ?>
                  <div style="color:red" id="err_delivery_date">&nbsp;<?php echo $form['delivery_date']->renderError(); ?></div> <span id="dispErr_date2" style="display: none;"></span>
            <label for="invoice_delivery_date_day" generated="false" class="error" id="label_error"></label>
          </td>
        </tr>
        <tr>
          <td align="right"><?php echo $form['due_date']->renderLabel() ?></td>
          <td><?php echo $form['due_date']->render() ?>
                  <div style="color:red" id="err_due_date">&nbsp;<?php echo $form['due_date']->renderError(); ?> </div><span id="dispErr_date3" style="display: none;"></span>
            <label for="invoice_due_date_day" generated="false" class="error" id="label_error"></label>
          </td>
        </tr>
        <tr>
          <td align="right"><?php echo $form['currency_type']->renderLabel() ?></td>
          <td><?php echo $form['currency_type']->render() ?><?php echo $form['currency_type']->renderError(); ?></td>
        </tr>
      </table>
    </div>
    <div class="clear_new"></div>




     <div class="TableForm bdrPd">

      <table width="100%" border="0" cellpadding="0" cellspacing="0" id="tableId" class="invoiceTb" style="border-left:0px solid;margin-top:0px" >
        <tr>
            <th >Number of Item<span class="redLbl">*</span> </th>
            <th >Item Description<span class="redLbl">*</span> </th>
            <th >Item Unit Price<span class="redLbl">*</span> </th>
            <th>Action</th>
        </tr>
          <?php if (isset($invoiceDetails) && $invoiceDetails != '') { ?>
          <?php $i=0;

                   $newArray = $invoiceDetails->toArray();

                   if(isset($newArray[3]) &&  $newArray[3]['id'] <> '') {
                       $Epls3 = '<a class="addRows" href="javascript:void(0)">'.image_tag('add-icon.png', array("id"=>"add3", "style"=>"display:none")).'</a>';
                       $Epls0 = '<a class="addRows" href="javascript:void(0)">'.image_tag('add-icon.png', array("id"=>"add0", "style"=>"display:none")).'</a>';
                       $Epls1 = '<a class="addRows" href="javascript:void(0)">'.image_tag('add-icon.png', array("id"=>"add1", "style"=>"display:none")).'</a>';
                       $Epls2 = '<a class="addRows" href="javascript:void(0)">'.image_tag('add-icon.png', array("id"=>"add2", "style"=>"display:none")).'</a>';
                   } else if(isset($newArray[2]) &&  $newArray[2]['id'] <> '') {
                       $Epls2 = '<a class="addRows" href="javascript:void(0)">'.image_tag('add-icon.png', array("id"=>"add2")).'</a>';
                       $Epls0 = '<a class="addRows" href="javascript:void(0)">'.image_tag('add-icon.png', array("id"=>"add0", "style"=>"display:none")).'</a>';
                       $Epls1 = '<a class="addRows" href="javascript:void(0)">'.image_tag('add-icon.png', array("id"=>"add1", "style"=>"display:none")).'</a>';
                   } else if(isset($newArray[1]) &&  $newArray[1]['id'] <> '') {
                       $Epls1 = '<a class="addRows" href="javascript:void(0)">'.image_tag('add-icon.png', array("id"=>"add1")).'</a>';
                       $Epls0 = '<a class="addRows" href="javascript:void(0)">'.image_tag('add-icon.png', array("id"=>"add0", "style"=>"display:none")).'</a>';
                   } else if(isset($newArray[0]) &&  $newArray[0]['id'] <> '') {
                       $Epls0 = '<a class="addRows" href="javascript:void(0)">'.image_tag('add-icon.png', array("id"=>"add0")).'</a>';
                   } else {
                       $Epls0 = '<a class="addRows" href="javascript:void(0)">'.image_tag('add-icon.png', array("id"=>"add0")).'</a>';
                   }


    
           foreach ($invoiceDetails as $invoiceDetails): ?>
        <tr id="invoiceRow<?php echo $i?>" >
            <td style="vertical-align:top;"><input type="text" onblur="javascript: onBlurTxtBox('item_no<?php echo $i ?>')" name="item_no<?php echo $i ?>" id="item_no<?php echo $i ?>" value="<?php echo $invoiceDetails->getNoOfItems(); ?>" maxlength="3" onkeydown="calTotal1()" onkeyup="calTotal1();" onchange="calTotal1();" />
                <div style="color:red;clear:both" id="err_item_no<?php echo $i ?>"></div>
            </td>
            <td style="vertical-align:top;"><input type="text" onblur="javascript: onBlurTxtBox('item_desc<?php echo $i ?>')" name="item_desc<?php echo $i ?>" id="item_desc<?php echo $i ?>"  value="<?php echo $invoiceDetails->getItemDescription(); ?>"  maxlength="36"/>
                <div style="color:red;clear:both" id="err_item_desc<?php echo $i ?>"></div>
            </td>
            <td style="vertical-align:top;"><input type="text" onblur="javascript: onBlurTxtBox('item_price<?php echo $i ?>')" name="item_price<?php echo $i ?>" id="item_price<?php echo $i ?>" value="<?php echo $invoiceDetails->getItemPrice(); ?>" maxlength="8" onkeydown="calTotal1();" onkeyup="calTotal1();" onchange="calTotal1();" />
                <div style="color:red;clear:both" id="err_item_price<?php echo $i ?>"></div>
            </td>
            <td>
                <?php if($i <> 0){ ?>
                   <a href="javascript:void(0)"><?php echo image_tag('delete.png', array("alt"=>"Delete", "class"=>"deleteRow", "title"=>"Delete entry", "width"=>"16", "id"=>$i))?></a>
                <?php  }
                 $btn = 'Epls'.$i;echo $$btn;
                 ?>
                
            </td>
        </tr>
        <?php $i++; endforeach; ?>
          <?php } else {

              //Condition for Plus button
                   if(isset($arr['item_no3']) &&  $arr['item_no3'] <> '') {
                       $pls3 = '<a class="addRows" href="javascript:void(0)">'.image_tag('add-icon.png', array("id"=>"add3", "style"=>"display:none")).'</a>';
                       $pls0 = '<a class="addRows" href="javascript:void(0)">'.image_tag('add-icon.png', array("id"=>"add0", "style"=>"display:none")).'</a>';
                       $pls1 = '<a class="addRows" href="javascript:void(0)">'.image_tag('add-icon.png', array("id"=>"add1", "style"=>"display:none")).'</a>';
                       $pls2 = '<a class="addRows" href="javascript:void(0)">'.image_tag('add-icon.png', array("id"=>"add2", "style"=>"display:none")).'</a>';
                   } else if(isset($arr['item_no2']) &&  $arr['item_no2'] <> '') {
                       $pls2 = '<a class="addRows" href="javascript:void(0)">'.image_tag('add-icon.png', array("id"=>"add2")).'</a>';
                       $pls0 = '<a class="addRows" href="javascript:void(0)">'.image_tag('add-icon.png', array("id"=>"add0", "style"=>"display:none")).'</a>';
                       $pls1 = '<a class="addRows" href="javascript:void(0)">'.image_tag('add-icon.png', array("id"=>"add1", "style"=>"display:none")).'</a>';
                   } else if(isset($arr['item_no1']) &&  $arr['item_no1'] <> '') {
                       $pls1 = '<a class="addRows" href="javascript:void(0)">'.image_tag('add-icon.png', array("id"=>"add1")).'</a>';
                       $pls0 = '<a class="addRows" href="javascript:void(0)">'.image_tag('add-icon.png', array("id"=>"add0", "style"=>"display:none")).'</a>';
                   } else if(isset($arr['item_no0']) &&  $arr['item_no0'] <> '') {
                       $pls0 = '<a class="addRows" href="javascript:void(0)">'.image_tag('add-icon.png', array("id"=>"add0")).'</a>';
                   } else {
                       $pls0 = '<a class="addRows" href="javascript:void(0)">'.image_tag('add-icon.png', array("id"=>"add0")).'</a>';
                   }

          ?>
                  <tr id="invoiceRow0" >
                    <td style="vertical-align:top;"><input type="text" onblur="javascript: onBlurTxtBox('item_no0')"  name="item_no0" value="<?php  if(isset($_REQUEST['item_no0'])) echo $_REQUEST['item_no0'];?>" id="item_no0" value="" maxlength="3" onkeydown="calTotal1()" onkeyup="calTotal1();" onchange="calTotal1();" />
                      <?php echo '<div style="color:red;clear:both" id="err_item_no0">'.validateItems('item_no0').'</div>' ?></td>
                    <td style="vertical-align:top;"><input type="text" onblur="javascript: onBlurTxtBox('item_desc0')" name="item_desc0" id="item_desc0" value="<?php if(isset($_REQUEST['item_desc0'])) echo $_REQUEST['item_desc0'];?>"  maxlength="36"/>
                      <?php echo '<div style="color:red;clear:both" id="err_item_desc0">'.validateItems('item_desc0').'</div>';?></td>
                    <td style="vertical-align:top;"><input type="text" onblur="javascript: onBlurTxtBox('item_price0')" name="item_price0" id="item_price0" value="<?php if(isset($_REQUEST['item_price0'])) echo $_REQUEST['item_price0']?>" maxlength="8" onkeydown="calTotal1();" onkeyup="calTotal1();" onchange="calTotal1();" />
                      <?php echo '<div style="color:red;clear:both" id="err_item_price0">'.validateItems('item_price0').'</div>';?></td>
                    <td><a href="javascript:void(0)" class="addRows"><?php echo $pls0; ?></a></td>

                  </tr>
                  <?php if(isset($arr['item_no1']) &&  $arr['item_no1'] <> ''){?>

                  <tr id="invoiceRow1" >
                    <td style="vertical-align:top;"><input type="text" onblur="javascript: onBlurTxtBox('item_no1')"  name="item_no1" value="<?php  if(isset($_REQUEST['item_no1'])) echo $_REQUEST['item_no1'];?>" id="item_no1" value="" maxlength="3" onkeydown="calTotal1()" onkeyup="calTotal1();" onchange="calTotal1();" />
                      <?php echo '<div style="color:red;clear:both" id="err_item_no1">'.validateItems('item_no1').'</div>' ?></td>
                    <td style="vertical-align:top;"><input type="text" onblur="javascript: onBlurTxtBox('item_desc1')" name="item_desc1" id="item_desc1" value="<?php if(isset($_REQUEST['item_desc1'])) echo $_REQUEST['item_desc1'];?>"  maxlength="36"/>
                      <?php echo '<div style="color:red;clear:both" id="err_item_desc1">'.validateItems('item_desc1').'</div>';?></td>
                    <td style="vertical-align:top;"><input type="text" onblur="javascript: onBlurTxtBox('item_price1')" name="item_price1" id="item_price1" value="<?php if(isset($_REQUEST['item_price1'])) echo $_REQUEST['item_price1']?>" maxlength="8" onkeydown="calTotal1();" onkeyup="calTotal1();" onchange="calTotal1();" />
                      <?php echo '<div style="color:red;clear:both" id="err_item_price1">'.validateItems('item_price1').'</div>';?></td>
                    <td><img src="<?php echo image_path('delete.png');?>"  id='1' alt="Delete" title="Delete entry" width="16" class="deleteRow" style="cursor:pointer"/>&nbsp;<?php echo $pls1; ?></td>
                  </tr>

                  <?php } ?>
                  <?php if(isset($arr['item_no2']) &&  $arr['item_no2'] <> ''){?>

                  <tr id="invoiceRow2" >
                    <td style="vertical-align:top;"><input type="text" onblur="javascript: onBlurTxtBox('item_no2')"  name="item_no2" value="<?php  if(isset($_REQUEST['item_no2'])) echo $_REQUEST['item_no2'];?>" id="item_no2" value="" maxlength="3" onkeydown="calTotal1()" onkeyup="calTotal1();" onchange="calTotal1();" />
                      <?php echo '<div style="color:red;clear:both" id="err_item_no2">'.validateItems('item_no2').'</div>' ?></td>
                    <td style="vertical-align:top;"><input type="text" onblur="javascript: onBlurTxtBox('item_desc2')" name="item_desc2" id="item_desc2" value="<?php if(isset($_REQUEST['item_desc2'])) echo $_REQUEST['item_desc2'];?>"  maxlength="36"/>
                      <?php echo '<div style="color:red;clear:both" id="err_item_desc2">'.validateItems('item_desc2').'</div>';?></td>
                    <td style="vertical-align:top;"><input type="text" onblur="javascript: onBlurTxtBox('item_price2')" name="item_price2" id="item_price2" value="<?php if(isset($_REQUEST['item_price2'])) echo $_REQUEST['item_price2']?>" maxlength="8" onkeydown="calTotal1();" onkeyup="calTotal1();" onchange="calTotal1();" />
                      <?php echo '<div style="color:red;clear:both" id="err_item_price2">'.validateItems('item_price2').'</div>';?></td>
                    <td><img src="<?php echo image_path('delete.png');?>"  id='2' alt="Delete" title="Delete entry" width="16" class="deleteRow" style="cursor:pointer"/>&nbsp;<?php echo $pls2; ?></td>
                  </tr>

                  <?php } ?>
                  <?php if(isset($arr['item_no3']) &&  $arr['item_no3'] <> ''){?>

                  <tr id="invoiceRow3" >
                    <td style="vertical-align:top;"><input type="text" onblur="javascript: onBlurTxtBox('item_no3')"  name="item_no3" value="<?php  if(isset($_REQUEST['item_no3'])) echo $_REQUEST['item_no3'];?>" id="item_no3" value="" maxlength="3" onkeydown="calTotal1()" onkeyup="calTotal1();" onchange="calTotal1();" />
                      <?php echo '<div style="color:red;clear:both" id="err_item_no3">'.validateItems('item_no3').'</div>' ?></td>
                    <td style="vertical-align:top;"><input type="text" onblur="javascript: onBlurTxtBox('item_desc3')" name="item_desc3" id="item_desc3" value="<?php if(isset($_REQUEST['item_desc3'])) echo $_REQUEST['item_desc3'];?>"  maxlength="36"/>
                      <?php echo '<div style="color:red;clear:both" id="err_item_desc3">'.validateItems('item_desc3').'</div>';?></td>
                    <td style="vertical-align:top;"><input type="text" onblur="javascript: onBlurTxtBox('item_price3')" name="item_price3" id="item_price3" value="<?php if(isset($_REQUEST['item_price3'])) echo $_REQUEST['item_price3']?>" maxlength="8" onkeydown="calTotal1();" onkeyup="calTotal1();" onchange="calTotal1();" />
                      <?php echo '<div style="color:red;clear:both" id="err_item_price3">'.validateItems('item_price3').'</div>';?></td>
                    <td><img src="<?php echo image_path('delete.png');?>"  id='3' alt="Delete" title="Delete entry" width="16" class="deleteRow" style="cursor:pointer"/>&nbsp;<?php echo $pls3; ?></td>
                  </tr>

                  <?php } ?>

          <?php } ?>
        </table>


    <br />
    <br />

      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <th>Item Tax(%) </th>
  	    <th>Subtotal (Excluding Tax) </th>
            <th>Total (Including Tax)</th>

        </tr>
         <tr>
          <td>

              <select id="item_tax0" name="item_tax0" onclick="calTotal1();" onchange="calTotal1();">
              <option id="item_tax1" value=""  onclick="calTotal1();" onkeydown="calTotal1();" onkeyup="calTotal1();" onchange="calTotal1();" >Please select a Item Tax</option>
              <?php $i = 1;
                            if(!empty($getMerchantTaxDetails)) {
                            foreach ($getMerchantTaxDetails as $val): ?>
              <?php
                                $tax_value = explode(' - ', $val);
                                $i++;
                            ?>
              <option id="item_tax<?php echo $i; ?>" <?php if(!empty($getTaxDetail) && $getTaxDetail[0]['item_tax']==$tax_value[2]) { ?> onload="calTotal1();" selected="selected" <?php } else if(isset($arr['item_tax0']) && ($arr['item_tax0'] == ($tax_value[0].'/'.$tax_value[2]))){ echo 'selected'; } ?> value="<?php echo $tax_value[0].'/'.$tax_value[2] ?>"  onclick="calTotal1();" onkeydown="calTotal1();" onkeyup="calTotal1();" onchange="calTotal1();" ><?php echo $tax_value[1].' - '.$tax_value[2]; ?></option>
              <?php endforeach; } ?>
            </select>
               </td>
  		<td ><input type="text" class="FormInput10" id="mysubtotal" name="mysubtotal" value="<?php if(isset($arr['mysubtotal'])){ echo $arr['mysubtotal'];}?>"  readonly /></td>
                <td ><input type="text" class="FormInput10" id="mytotal" name="mytotal"  value="<?php if(isset($arr['mytotal'])){ echo $arr['mytotal'];}?>" readonly /></td>

        </tr>

        </table>
    </div>
    <h3>Additional Information</h3>
    <div class="TableForm">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td width="35%" align="right"> <?php echo $form['additional_information']->renderLabel() ?></td>
          <td> <?php echo $form['additional_information']->render() ?>
                  <div style="color:red" id="err_additional_information">&nbsp;<?php echo $form['additional_information']->renderError(); ?></div></td>
        </tr>
      </table>
    </div>

    <div class="invoiceTb1" width="100%" style="display:none">
      <div class="addBtn"  id="addRowBtn1"><span title="Add Row"> Add Item</span></div>
      <div style="clear:both; padding:20px 0 0 2px;">
        <div class="FormRepeatArea">
          <div class="FormLabel">
            <label for=">Item_Tax(%)">Item Tax(%)</label>
          </div>
          <div class="FromInputLabe2">
            
          </div>
          <div class="clear_new"></div>
        </div>
      </div>
      <div class="total" style="padding-right:10px;">
        <!--set the right alignment only this page-->
        <h3>Subtotal (Excluding Tax) <span class="currency_type">N</span><span id="mysubtotal1">0.00</span></h3>
        <h3>Total (Including Tax) <span class="currency_type">N</span><span id="mytotal1">0.00</span></h3>
      </div>
    </div>

<?php if ($showRecurringPartPaymentInputs) { // added by santosh ?>
    <div class="clear_new"></div>

      <h3>Recurring Setup</h3>
    <div class="TableForm">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td width="35%" align="right" style="vertical-align:top;">Recurring Bill</td>
          <td><div class="Nwradio">
        <input <?php if(isset($recurring) && $recurring!=''){ ?><?php }?> class="recurr" type="checkbox" value="yes" name="recurring" id="recurring" >
        Recurring Setup <br clear="all"/>
        <div  class="rOpt_new" >
          <ul>
            <li>
              <input type="radio" <?php if(isset($recurring_plan) && $recurring_plan['type']=='weekly') { ?> checked <?php } ?> name="recurring_plan" value="weekly"  />
              Weekly Recurring Bill</li>
            <li>
              <input type="radio" <?php if(isset($recurring_plan) && $recurring_plan['type']=='monthly') { ?> checked <?php } ?> name="recurring_plan" value="monthly"  />
              Monthly Recurring Bill</li>
            <li>
              <input type="radio" <?php if(isset($recurring_plan) && $recurring_plan['type']=='yearly') { ?> checked <?php } ?> name="recurring_plan" value="yearly"  />
              Yearly Recurring Bill</li>
            <li>
              <input type="radio" <?php if(isset($recurring_plan) && $recurring_plan['type']=='quaterly') { ?> checked <?php } ?> name="recurring_plan" value="quarterly"  />
              Quarterly Recurring Bill</li>
            <li><span style="float:left">
              <input type="radio" id="custom_recurring_plan" name="recurring_plan"<?php if(isset($recurring_plan) && $recurring_plan['type']=='custom') { ?> checked <?php } ?> value="custom"  />
              Custom Recurring Bill (days)</span>
              <div class="clear_new"></div>
              <?php if(isset($recurring_plan) && $recurring_plan['value']!='') { ?>
              <input type="text" id="custom_recurring_bill_days" name="custom_recurring_bill" maxlength="3" value="<?php echo $recurring_plan['value']; ?>" style="border:1px #999 solid; width:120px; height:20px; padding:0px; margin:0px; margin-right:10px;margin-left:10px; margin-bottom:10px; float:left" />
              <?php } else { ?>
              <input type="text" id="custom_recurring_bill_days" name="custom_recurring_bill" maxlength="3" value="" style="border:1px #999 solid; width:120px; height:20px; padding:0px; margin:0px; margin-right:10px;margin-left:10px; margin-bottom:10px; float:left;display:none" />
              <?php } ?>
            </li>
          </ul>
        </div>
      </div>
      <label class="error" for="recurring_plan" generated="false"></label></td>
        </tr>
      </table>
    </div>

     <h3>Part Payment Setup</h3>
    <div class="TableForm">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td width="35%" align="right" style="vertical-align:top;">Part Payment</td>
           <td><div class="Nwradio">
        <input class="app" type="checkbox" value="yes" name="part_payment">
        Allow part payment
        <div class="pMore">
          <div style="float:left">Set Percentage(%)</div>
          <input type="text" id="app_textbox" maxlength="5" style="border: 1px solid #ccc; width: 120px; height: 20px; padding: 0px; margin: 0px 0px 10px 0px;" value="<?php if(isset($part_payment) && $part_payment!='') { echo $part_payment; }?>" name="part_payment_percent">
        </div>
      </div>
      <label class="error" for="app_textbox" generated="false"></label>
      <!--          &nbsp;<a href="<?php echo url_for('invoice/index') ?>">Back to list</a>--></td>
        </tr>
      </table>

</div>
     <?php } else {?>
     <input type="hidden" id="app_textbox" maxlength="5" style="border: 1px solid #ccc; width: 120px; height: 20px; padding: 0px; margin: 0px 0px 10px 0px;" value="<?php if(isset($part_payment) && $part_payment!='') { echo $part_payment; }?>" name="part_payment_percent">
     <input type="hidden" id="custom_recurring_bill_days" name="custom_recurring_bill" maxlength="3" value="" style="border:1px #999 solid; width:120px; height:20px; padding:0px; margin:0px; margin-right:10px;margin-left:10px; margin-bottom:10px; float:left;display:none" />
     <?php }?>
      <br clear="all"/>
      </span> <?php echo $form->renderHiddenFields(false) ?> <?php echo $form['_csrf_token']; ?>
       <br clear="all"/>
    <?php //if (!$form->getObject()->isNew()):  ?>
    &nbsp;
    <?php //echo link_to('Delete', 'invoice/delete?id='.$form->getObject()->getId(), array('method' => 'delete', 'confirm' => 'Are you sure?'))                ?>
    <?php //endif;  ?>
    <div class="right">
        <input type="submit" value="Create Invoice" class="blueBtn"  name="Submit" onclick="return callOnSubmit()">
    </div>
       <br clear="all"/>
  </div>
  <?php //echo $form  ?>
</form>
</div>
<script>
                                // Varun : tax calculated on load in case pagfe is re-directed from regenerate link
<?php if (isset($invoiceDetails) && $invoiceDetails != '') { ?>
                                window.onload = calTotal1();
<?php } ?>
</script>
