<script language="javascript" >
    $(document).ready(function(){

        $('#Invoice').addClass('active');
        $('#date_search').bind('click', function (){
            if($(this).attr('checked')){
                displayDateSearch();
            }else {
                hideDateSearch();
            }
        });
       
        function displayDateSearch(){
            $('#date_type').css('display', 'inline');
            $('#date_type').addClass('date_type');
            $("#search_form").validate({
                rules: {
                    'date_category':
                        {
                        required: true
                    }
                },
                messages:{
                    'date_category':
                        {
                        required: "Please enter Date Type"
                    }
                }
            });
            $('#date_selection').css('display', 'inline');
            $('#to_day').rules("add", "diff_between_dates");
        }

        function hideDateSearch(){
            $('#date_type').css('display', 'none');
            $('#date_type').addClass('date_type');
            $('#date_selection').css('display', 'none');
            $('input[name="date_category"]').rules('remove');
            $('#to_day').rules('remove');
            $('input [name=date_category]').val('');
            $('#from_date').val('');
            $('#to_date').val('');
            $('label[for=date_category]').hide();
        }
      
     


<?php if (!is_null($from_date)) { ?>
            $('#from_date').val('<?php echo $from_date; ?>');
            $('#date_selection').css('display', 'inline');
            $("#from_year").val('<?php echo date('Y', strtotime($from_date)) ?>');
            $("#from_month").val('<?php echo date('n', strtotime($from_date)) ?>');
            $("#from_day").val('<?php echo date('j', strtotime($from_date)) ?>');
<?php } else { ?>
            $('#from_date').val('');
<?php } ?>
            
<?php if (!is_null($to_date)) { ?>$('#to_date').val('<?php echo $to_date; ?>');
            $('#date_selection').css('display', 'inline');
            $("#to_year").val('<?php echo date('Y', strtotime($to_date)) ?>');
            $("#to_month").val('<?php echo date('n', strtotime($to_date)) ?>');
            $("#to_day").val('<?php echo date('j', strtotime($to_date)) ?>');
<?php } else { ?>
            $('#to_date').val('');
<?php } ?>

<?php if (!is_null($date_cat)) { ?>$('#<?php echo $date_cat; ?>').attr('checked','checked');
            $('#date_search').attr('checked','checked');
            $('#date_type').css('display', 'inline');
            $('#date_type').addClass('date_type');
<?php } ?>
    });

    function resetForm(){
        $('#email_address').val('');
        $('#cust_name').val('');
        $('#com_name').val('');
        $('#invoice_no').val('');
        $('#issue_date').attr('checked',true);
        $('input [name=date_category]').val('');
        $('#from_date').val('');
        $('#to_date').val('');
        $('label[for=date_category]').hide();
        $("#to_year").val('');
        $("#to_month").val('');
        $("#to_day").val('');
        $("#from_year").val('');
        $("#from_month").val('');
        $("#from_day").val('');
            

    }

    function displayDateSearch1(){
        var checked = $('input[type=checkbox]').is(':checked');
        if(checked){
            $('#date_selection').css('display', 'inline');
            return checkifDateBlank();
            // $('#to_day').rules("add", "diff_between_dates");
        }
    }

    function validateSearch(){
        var err  = 0;
        var invoice_number = jQuery.trim($('#invoice_no').val());
        var email_address = jQuery.trim($('#email_address').val());
        var cust_name = jQuery.trim($('#cust_name').val());
        var com_name = jQuery.trim($('#com_name').val());


        var reg = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i; //Email validation
        var regcheck = /^[a-zA-Z0-9 .&]*$/; //Invoice Number  and Customer Name validation


        if(displayDateSearch1() == false){
            err = 1;
        }

        if(email_address!='')
        {
            if(reg.test(email_address) == false) {
                $('#email_address_error').html("<font color='red'>Please enter correct Email Address.</font>");
                $('#email_address').focus();
                err = err + 1;
            } else {
                $('#email_address_error').html("");
            }
        }
        if(cust_name!=''){
            if(cust_name.length >50 ) {
                $('#cust_name_error').html("<font color='red'>Customer Name cannot be more than 50 characters.</font>");
                $('#cust_name').focus();
                err = err + 1;
            } else {
                $('#cust_name_error').html("");
            }
            if(regcheck.test(cust_name) == false ) {
                $('#cust_name_error').html("<font color='red'>Please enter valid Customer Name.</font>");
                $('#cust_name').focus();
                err = err + 1;
            } else {
                $('#cust_name_error').html("");
            }
        }

        if(com_name!=''){
            if(com_name.length >50 ) {
                $('#com_name_error').html("<font color='red'>Merchant Name cannot be more than 50 characters.</font>");
                $('#com_name').focus();
                err = err + 1;
            } else {
                $('#com_name_error').html("");
            }
            if(regcheck.test(com_name) == false ) {
                $('#com_name_error').html("<font color='red'>Please enter valid Merchant Name.</font>");
                $('#com_name').focus();
                err = err + 1;
            } else {
                $('#com_name_error').html("");
            }
        }
        
        if(invoice_number!=''){
            //            if(invoice_number.length >10 ) {
            //                $('#invoice_no_error').html("<font color='red'>Invoice Number cannot be more than 10 characters.</font>");
            //                $('#invoice_no').focus();
            //                err = err + 1;
            //            } else {
            //                $('#invoice_no_error').html("");
            //            }
            if(regcheck.test(invoice_number) == false ) {
                $('#invoice_no_error').html("<font color='red'>Please enter valid Invoice Number.</font>");
                $('#invoice_no').focus();
                err = err + 1;
            } else {
                $('#invoice_no_error').html("");
            }
        }

        if(err>0) return false;
        return true;
    }
   
    // Manu : User should select one option to use for search either via dropdown
    function populateSearch()
    {
        return false;
        var settings = $('#userForm').validate().settings;
        var email_address = $('#email_address1');
        var customer_name = $('#customer_name');
        var company_name = $('#company_name');
        var invoice_number = $('#invoice_number');

        var id = $('#search_par').val();

        $('#email_address1').hide();
        $('#customer_name').hide();
        $('#company_name').hide();
        $('#invoice_number').hide();

        if(id == 'email'){
                    
            $('#cust_name').val('');
            $('#com_name').val('');
            $('#invoice_no').val('');
            $('#invoice_no_error').html("");
            $('#cust_name_error').html("");
            $('#com_name_error').html("");

            customer_name.css("display", "none");
            company_name.css("display", "none");
            invoice_number.css("display", "none");
            email_address.css("display", "block");

        } else  if(id == 'customer'){
                    
            $('#email_address').val('');
            $('#invoice_no').val('');
            $('#com_name').val('');
            $('#email_address_error').html("");
            $('#invoice_no_error').html("");
            $('#com_name_error').html("");

            email_address.css("display", "none");
            invoice_number.css("display", "none");
            customer_name.css("display", "block");
        } else  if(id == 'invoice'){

            $('#email_address').val('');
            $('#cust_name').val('');
            $('#cust_name_error').html("");
            $('#com_name').val('');
            $('#com_name_error').html('');
            $('#email_address_error').html("");


            customer_name.css("display", "none");
            company_name.css("display", "none");
            email_address.css("display", "none");
            invoice_number.css("display", "block");
        } else  if(id == 'company'){
                   
            $('#email_address').val('');
            $('#cust_name').val('');
            $('#cust_name_error').html("");
            $('#email_address_error').html("");
            $('#invoice_no').val('');
            $('#invoice_no_error').html("");

            customer_name.css("display", "none");
            company_name.css("display", "block");
            email_address.css("display", "none");
            invoice_number.css("display", "none");
        }else{
            customer_name.css("display", "none");
            email_address.css("display", "none");
            invoice_number.css("display", "none");
            company_name.css("display", "none");
        }
    }
    
</script>
<script language="javascript" >

    function checkifDateBlank(){
        
        value = $('#to_day').val();
        if(value!='')
        {
            if($('#from_date').val()==''){
                /**
                 * Manu : Search date display in case of page refresh
                 */
                $("#dispErr_date2").html('Please enter From Date');
                $("#dispErr_date2").addClass('error');
                $('#dispErr_date2').css('display', 'inline');
                $("#dispErr_date3").html('');
                $("#dispErr_date3").addClass('error');
                $('#dispErr_date3').css('display', 'inline');
                return false;
            } else{
                var month=$("#from_month").val();
                if(parseInt($("#from_month").val())<10) {
                    var month="0"+$("#from_month").val();
                }

                var day=parseInt($("#from_day").val());
                
                if(day<10){
                    var day="0"+day;
                }
                
                var d = new Date($("#from_year").val()+"/"+month+"/"+day);
                d.setHours(0, 0, 0, 0);

                var delivery_month=$("#to_month").val();
                if(parseInt($("#to_month").val())<10){
                    var delivery_month="0"+$("#to_month").val();
                }

                var delivery_day=parseInt($("#to_day").val());
                if(delivery_day<10){
                    var delivery_day="0"+delivery_day;
                }
                var delivery_date = new Date($("#to_year").val()+"/"+delivery_month+"/"+delivery_day);
                delivery_date.setHours(0,0,0,0);

                if(d.getTime().valueOf() > delivery_date.getTime().valueOf()){
                    $("#dispErr_date3").html('"To" Date Cannot be Less than "From" Date');
                    $("#dispErr_date3").addClass('error');
                    $('#dispErr_date3').css('display', 'inline');
                    $("#dispErr_date2").html('');
                    $("#dispErr_date2").addClass('error');
                    $('#dispErr_date2').css('display', 'inline');
                    return false;
                } else {
                    $('#dispErr_date3').css('display', 'none');
                    return true;
                }
            }
        } else {
            /**
             * Manu : Search date display in case of page refresh
             */
            if($('#to_date').val()==''&& $('#from_date').val()==''){
                $("#dispErr_date3").html('Please enter To Date');
                $("#dispErr_date3").addClass('error');
                $('#dispErr_date3').css('display', 'inline');
                $("#dispErr_date2").html('Please enter From Date');
                $("#dispErr_date2").addClass('error');
                $('#dispErr_date2').css('display', 'inline');
                return false;
            }else if($('#to_date').val()==''&& $('#from_date').val()!=''){
                /**
                 * Manu : Search date display in case of page refresh
                 */
                $("#dispErr_date3").html('Please enter To Date');
                $("#dispErr_date3").addClass('error');
                $('#dispErr_date3').css('display', 'inline');
                $("#dispErr_date2").html('');
                $("#dispErr_date2").addClass('error');
                $('#dispErr_date2').css('display', 'inline');
                return false;
            }else if($('#to_date').val()!='' && $('#from_date').val()==''){
                /**
                 * Manu : Search date display in case of page refresh
                 */
                $("#dispErr_date2").html('Please enter From Date');
                $("#dispErr_date2").addClass('error');
                $('#dispErr_date2').css('display', 'inline');
                $("#dispErr_date3").html('');
                $("#dispErr_date3").addClass('error');
                $('#dispErr_date3').css('display', 'inline');
                return false;
            } else {
                return true;
            }
        }
    }

        
    $(document).ready(function(){
 
        //    $.validator.addMethod('diff_between_dates', function(value, element) {
        //            if(value!='')
        //            {
        //                if($('#from_date').val()==''){
        //                    /**
        //                    * Manu : Search date display in case of page refresh
        //                    */
        //                    $("#dispErr_date2").html('Please enter From Date');
        //                    $("#dispErr_date2").addClass('error');
        //                    $('#dispErr_date2').css('display', 'inline');
        //                    $("#dispErr_date3").html('');
        //                    $("#dispErr_date3").addClass('error');
        //                    $('#dispErr_date3').css('display', 'inline');
        //                    return false;
        //                } else{
        //                    var month=$("#from_month").val();
        //                    if(parseInt($("#from_month").val())<10) {
        //                        var month="0"+$("#from_month").val();
        //                    }
        //
        //                    var day=parseInt($("#from_day").val());
        //                    if(value<10){
        //                        var day="0"+value;
        //                    }
        //                    var d = new Date($("#from_year").val()+"/"+month+"/"+day);
        //
        //                    d.setHours(0, 0, 0, 0);
        //
        //                    var delivery_month=$("#to_month").val();
        //                    if(parseInt($("#to_month").val())<10){
        //                        var delivery_month="0"+$("#to_month").val();
        //                    }
        //
        //                    var delivery_day=parseInt($("#to_day").val());
        //                    if(delivery_day<10){
        //                        var delivery_day="0"+delivery_day;
        //                    }
        //
        //                    var delivery_date = new Date($("#to_year").val()+"/"+delivery_month+"/"+delivery_day);
        //                    delivery_date.setHours(0,0,0,0);
        //
        //                    if(d.getTime().valueOf() > delivery_date.getTime().valueOf()){
        //
        //                        $("#dispErr_date3").html('"To" Date Cannot be Less than "From" Date');
        //                        $("#dispErr_date3").addClass('error');
        //                        $('#dispErr_date3').css('display', 'inline');
        //                        $("#dispErr_date2").html('');
        //                        $("#dispErr_date2").addClass('error');
        //                        $('#dispErr_date2').css('display', 'inline');
        //                        return false;
        //                    } else {
        //                        $('#dispErr_date3').css('display', 'none');
        //                        return true;
        //                    }
        //                }
        //            } else {
        //              /**
        //                * Manu : Search date display in case of page refresh
        //                */
        //                if($('#to_date').val()==''&& $('#from_date').val()==''){
        //                    $("#dispErr_date3").html('Please enter To Date');
        //                    $("#dispErr_date3").addClass('error');
        //                    $('#dispErr_date3').css('display', 'inline');
        //                    $("#dispErr_date2").html('Please enter From Date');
        //                    $("#dispErr_date2").addClass('error');
        //                    $('#dispErr_date2').css('display', 'inline');
        //                    return false;
        //                }else if($('#to_date').val()==''&& $('#from_date').val()!=''){
        //                    /**
        //                    * Manu : Search date display in case of page refresh
        //                    */
        //                    $("#dispErr_date3").html('Please enter To Date');
        //                    $("#dispErr_date3").addClass('error');
        //                    $('#dispErr_date3').css('display', 'inline');
        //                    $("#dispErr_date2").html('');
        //                    $("#dispErr_date2").addClass('error');
        //                    $('#dispErr_date2').css('display', 'inline');
        //                    return false;
        //                }else if($('#to_date').val()!='' && $('#from_date').val()==''){
        //                    /**
        //                    * Manu : Search date display in case of page refresh
        //                    */
        //                        $("#dispErr_date2").html('Please enter From Date');
        //                        $("#dispErr_date2").addClass('error');
        //                        $('#dispErr_date2').css('display', 'inline');
        //                    $("#dispErr_date3").html('');
        //                    $("#dispErr_date3").addClass('error');
        //                    $('#dispErr_date3').css('display', 'inline');
        //                    return false;
        //                } else {
        //                    return true;
        //                }
        //            }
        //        },'');


        //populateSearch();
        $('#Invoice').addClass('active');

<?php if (isset($msg) && $msg != "") { ?>
            $('#if_error').html('<?php echo html_entity_decode($msg); ?>');
            $('#if_error').addClass('msg_serverside');
<?php } else { ?>
            $('#if_error').html();
            $('#if_error').removeClass('msg_serverside');
<?php } ?>
    });
</script>
<script language="javascript" >
    $(document).ready(function(){
<?php if (isset($expired) && $expired == 'false') { ?>
            $('#active_invoices').addClass('on');
            $('#expired_invoices').removeClass('on');
<?php
} else {
?>$('#expired_invoices').addClass('on');
            $('#active_invoices').removeClass('on');
<?php } ?>

    }
);
</script>

<?php
if ($merchnatLogin || $tellerLogin) {
    if (!isset($customerTellerId))
        $customerTellerId = '';
    if ($merchnatLogin == true) {
        $loginMerchantTeller = 1;
    } elseif ($tellerLogin == true) {
        $loginMerchantTeller = 2;
    }

    if (isset($addClassTo)) {
        include_partial('subHeader', array('addClassTo' => $addClassTo, 'loginType' => $loginMerchantTeller, 'customerTellerId' => sfContext::getInstance()->getUser()->getAttribute('customerTellerId')));
    } else {
        include_partial('subHeader', array('addClassTo' => 'new', 'loginType' => $loginMerchantTeller, 'customerTellerId' => $customer_id));
    }
} else {
?>

    <div id="pgMenu">
        <div id="wrappernew">
            <div class="pgTitle">
                <h2>Invoices</h2>
            </div>
        </div>
    </div>
<?php
}
?>
<!-- Varun : Flash messaged added -->
<?php
$sf = sfContext::getInstance()->getUser();
if ($sf->hasFlash('notice')) {
?>
    <div id="flash_notice" class="msg_serverside" >
    <?php
    echo nl2br($sf->getFlash('notice'));
    ?>
</div>
<?php } ?>
<div id="mainnew">
    <div id="wrappernew">
        <div id="content">
            <div id="Formwraper">
                <?php //if(isset($merchant_key) && $merchant_key!='') {  ?>
                <!-- Varun : Search section added to display search div-->
                <h3>Search Invoice</h3>
                <div style="clear:both"></div>
                <div class="TableForm" id="search">
                    <form action="<?php echo url_for($formAction); ?>" method="post" id="userForm"  > <!--onsubmit="return validateSearch()"-->
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <?php /*
                              <tr>
                              <td width="17.5%"><?php echo "Search" ?> </td>
                              <td  width="35%">
                              <select name="search_par" id="search_par" onchange="return populateSearch()">
                              <option value="">Please Select</option>
                              <?php if($merchnatLogin ){ ?>
                              <option value="email" <?php if(isset($_POST['search_par']) && $_POST['search_par']=='email') { echo "selected"; } else if($search_par=='email'){ echo "selected"; } else { echo ""; }  ?> >Email Address</option>
                              <option value="customer" <?php if(isset($_POST['search_par']) && $_POST['search_par']=='customer') { echo "selected"; } else if($search_par=='customer'){ echo "selected"; } else { echo ""; }   ?> >Customer Name</option>
                              <?php } else { ?>
                              <option value="company" <?php if(isset($_POST['search_par']) && $_POST['search_par']=='company') { echo "selected"; } else if($search_par=='company'){ echo "selected"; } else { echo ""; }   ?> >Company Name</option>
                              <?php } ?>
                              <option value="invoice" <?php if(isset($_POST['search_par']) && $_POST['search_par']=='invoice') { echo "selected"; } else if($search_par=='invoice'){ echo "selected"; } else { echo ""; }  ?> >Invoice Number</option>
                              </select>
                              </td>
                              <td><div id="email_address1">
                              <div class="search_text"><?php echo $form['email_address']->renderLabel() ?><font color="red">*</font></div>

                              <input type="text" id="email_address" name="email_address" maxlength="70" value="<?php echo $_SESSION['email_address']?>">
                              <div class="clear_new"></div>
                              <div class="red redlt Errormrg" id="email_address_error"> <?php echo $form['email_address']->renderError(); ?></div>
                              </div>
                              <?php if($merchnatLogin ){ ?>
                              <div  id="customer_name">
                              <div class="search_text"> <?php echo $form['cust_name']->renderLabel() ?><font color="red">*</font></div>
                              <input type="text" id="cust_name" name="cust_name" maxlength="50" value="<?php echo $_SESSION['cust_name']?>">
                              <div class="clear_new"></div>
                              <div class="red redlt Errormrg" id="cust_name_error"> <?php echo $form['cust_name']->renderError(); ?></div>

                              </div>
                              <?php } else { ?>
                              <div id="company_name">

                              <div class="search_text">Company Name <font color="red">*</font></div>
                              <input type="text" id="com_name" name="com_name"  maxlength="50" value="<?php echo $_SESSION['com_name']?>">
                              <div class="clear_new"></div>
                              <div class="red redlt Errormrg" id="com_name_error"></div>
                              </div>
                              <?php } ?>
                              <div class="" id="invoice_number">
                              <div class="search_text"> <?php echo $form['invoice_no']->renderLabel() ?><font color="red">*</font></div>

                              <input type="text" id="invoice_no" name="invoice_no" maxlength="10" value="<?php echo $_SESSION['invoice_no']?>">
                              <div class="clear_new"></div>
                              <div class="red redlt Errormrg" id="invoice_no_error"> <?php echo $form['invoice_no']->renderError(); ?></div>

                              <div class="clear_new"></div>
                              </div></td>
                              </tr>
                              <?php */ ?>

                            <!-- New Search with text box on 22Jun, 2012 -->


                            <tr>
                                <td>&nbsp;Customer Id<font color="red"></font></td>
                                <td>
                                    <input type="text" id="customer_id" name="customer_id"  maxlength="50" value="<?php echo $customer_id; ?>">
                                    <div class="red redlt" id="customer_id_error"></div>
                                </td>
                            </tr>
                            <?php if ($merchnatLogin) {
                            ?>
                                <tr>
                                    <td><?php echo $form['cust_name']->renderLabel() ?><font color="red"></font></td>
                                    <td>
                                        <input type="text" id="cust_name" name="cust_name" maxlength="50" value="<?php echo $cust_name ?>">
                                        <div class="red redlt" id="cust_name_error"> <?php echo $form['cust_name']->renderError(); ?></div>
                                    </td>
                                </tr>
                            <?php
                            } else {

                                if (!$addCreateInvoiceButton) {
                            ?>
                                    <tr>
                                        <td>&nbsp;Merchant Name <font color="red"></font></td>
                                        <td width="84%">
                                            <input type="text" id="com_name" name="com_name"  maxlength="50" value="<?php echo $com_name; /* if (isset($_SESSION['com_name'])) echo $_SESSION['com_name']; */ ?>">
                                            <div class="red redlt" id="com_name_error"></div>
                                        </td>
                                    </tr>
                            <?php } ?>
                            <?php } ?>
                            <?php
                            if ($addCreateInvoiceButton) {
                                echo "<input type=hidden name=id value='$customerTellerId'>";
                            }
                            ?>
                            <tr>
                                <td><?php echo $form['invoice_no']->renderLabel() ?><font color="red"></font></td>
                                <td>
                                    <input type="text" id="invoice_no" name="invoice_no"  value="<?php echo $invoice_no ?>">
                                    <div class="red redlt" id="invoice_no_error"> <?php echo $form['invoice_no']->renderError(); ?></div>
                                </td>
                            </tr>

                            <!-- Date Search -->
                            <tr>
                                <td colspan="3" >
                                    <div style="padding:5px 0 0 5px; float:left; font-weight:bold;">
                                        <input type="checkbox" id="date_search" class="date_f">Search Date</div>
                                    <div style="clear:both; display:inline; margin:15px 0 5px 0;">
                                        <div id="date_type" style="display:none">
                                            <?php if (isset($tellerLogin) && !$tellerLogin) {
                                            ?>     <input type="radio" id="issue_date" name="date_category" checked value="issue_date">Issue Date<?php } ?>
                                            <input type="radio" name="date_category" id="delivery_date" <?php
                                            if ($tellerLogin) {
                                                echo "checked";
                                            }
                                            ?> value="delivery_date">Delivery Date
                                            <input type="radio" name="date_category" id="due_date" value="due_date">Due Date
                                            <!--input type="radio" name="date_category" id="payment_date" value="payment_date">Payment Date-->
                                            (<font color="red">*</font>Please select one option)
                                            <label for="date_category" generated="false" class="error"></label>
                                        </div>
                                    </div>
                                    <div id="date_selection" style="display:none">
                                        <div class="FormRepeatArea form_no_height" style="clear:left;">
                                            <div style="margin-bottom:5px; display:inline-block;">
                                                <div style="float:left;width:80px;"><?php echo $form['from']->renderLabel() ?></div>
                                                <div class="FromInputLabe2 select_size"> <?php echo $form['from']->render() ?></div>
                                                <span id="dispErr_date2" style="display: none;"></span>
                                            </div>
                                            <div class="FormRepeatArea form_no_height">
                                                <div style="float:left;width:80px;"> <?php echo $form['to']->renderLabel() ?></div>
                                                <div class="FromInputLabe2 select_size"> <?php echo $form['to']->render() ?></div><label for="to_day" generated="false" class="error" id="label_error"></label>
                                                <span id="dispErr_date3" style="display: none;"></span>
                                                <div class="clear_new"></div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <!-- Date Search -->
                            <!-- New Search with text box -->

                            <tr>
                                <td colspan="3" >
                                    <div class="Errormrg"><input type="submit" value="Search" class="blueBtn"  onclick="return validateSearch()" >
                                        <input type="button" class="blueBtn" value="Reset" onClick="window.location='<?php
                                                   echo url_for('invoice/index');
                                                   if ($addCreateInvoiceButton) {
                                                       echo '?id=' . $customerTellerId;
                                                   }
                                            ?>'"/>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </form>
                    </div>

                <?php
                                                   //}
                                                   if (!$merchnatLogin) {
                                                       if ($expired == 'false') {
                ?>
                                                           <h3>Current Invoices</h3>
                <?php
                                                           if ($addCreateInvoiceButton) {

                                                               echo "<h3>Customer:$customerName</h3>";
                                                           }
                                                       } else {
                ?>
                                                           <h3>Expired Invoices</h3>
                <?php } ?>
                <?php } else {
                ?>
                                                       <h3>Current Invoices</h3>
                                                       <div id="if_error" ></div>
                <?php } ?>
                <?php if (!$merchnatLogin && $_SESSION['logedin_user_group'] != 'teller') {
                ?>
                                                       <div class="TableForm">
                                                           <table>
                                                               <tr>
                                                                   <td>Search By  :&nbsp;
                                                                       <a href="<?php echo url_for('invoice/index') . '?' . $queryStringForStatusSearch . 'offset=0&getExpired=false'; ?>" <?php if (isset($class) && $class == '1') { ?>  style="font-weight:bold" <?php } ?>>Current Invoices</a>&nbsp;
                                                                       | <a href="<?php echo url_for('invoice/index') . '?' . $queryStringForStatusSearch . 'offset=0&getExpired=true'; ?>" <?php if (isset($class) && $class == '0') { ?>  style="font-weight:bold" <?php } ?>>Past Due Bills</a></td>
                                                               </tr>
                                                           </table>
                                                       </div>
                <?php } ?>
                                                   <div style="clear:both"></div>
                                                   <div class="TableForm">
                                                       <table>
                                                           <thead>
                                                               <tr>
                                <?php
                                                   //Varun : Helper added for proper amount format
                                                   $helperOb = new csvHelper();
                                                   if ($merchnatLogin) {
                                ?>
                                                       <th>Invoice Number</th>
                                                       <th>Customer Name</th>
                                                       <th>Customer Id</th>
                                                       <th>Issue Date</th>
                                                       <th>Delivery Date</th>
                                                       <th>Due Date</th>
                                                       <th>Status</th>
                                                       <th>Action</th>
                                <?php } else {
 ?>
                                                       <th>Invoice no</th>

                                <?php if (isset($tellerLogin) && $tellerLogin) {
                                ?><th>Delivery Date</th>
<?php } else { ?><th>Merchant Name</th><?php } ?>
                                                       <th>Customer Id</th><th>Due Date</th>
                                                       <th>Currency</th>
                                                       <th>Invoice Amount</th>
<?php if ($expired == 'false') { ?>
                                                           <th>Amount Paid</th>
                                                           <th>Amount Due</th>
<?php } ?>
                                                       <th>Status</th>
                                <?php
                                                       /* @Saurabh At 22June, 2012
                                                        *  In case of teller, action would not be displayed
                                                        */
                                                       if (isset($_SESSION['logedin_user_group']) && $_SESSION['logedin_user_group'] != 'teller') {
                                ?>
                                                           <th>Action</th>
<?php } /**/ ?>

<?php } ?>
                                               </tr>
                                           </thead>
                                           <tbody>
                            <?php
                                                   $i = 0;
                                                   if (count($invoices) > 0) {

                                                       $index_for_getting_invoice_amount = 0;
                            ?>
                            <?php
                                                       foreach ($invoices as $invoice):

                                                           $i++;
                            ?>
                            <?php if ($i % 2 != 0) {
 ?>
                                                               <tr class="odd">
                                <?php } else {
                                ?>
                                                           <tr class="">
                                <?php } ?>
                                <?php if ($merchnatLogin) {
                                ?>
                                                               <!-- View the invoice details 23-Feb-2012 -->
                                                               <!-- Varun : Encrypted id  -->
                                                               <td><a href="<?php echo url_for('invoice/show?id=' . cryptString::encrypt($invoice->getId())); ?>"><?php echo $invoice->getInvoiceNo(); ?></a></td>
                                                               <td><?php echo $invoice->Customer->getName() ?></td>
                                                               <td><?php echo $invoice->getCustomer()->getCustomerId(); ?></td>
                                                               <td><?php echo $invoice->getIssueDate() ?></td>
                                                               <td><?php echo $invoice->getDeliveryDate() ?></td>
                                                               <td><?php echo $invoice->getDueDate() ?></td>
                                                               <td><?php
                                                               //Varun : Check whether entry is expired or not
                                                               if (date('Y-m-d') > $invoice->getDueDate() && $invoice->getMerchant()->getPaymentType() == 'o') {
                                                                   echo image_tag('expired.png', array('alt' => "Expired", 'title' => "Expired"));
                                                               } elseif (date('Y-m-d') > $invoice->getDueDate() && $invoice->getMerchant()->getPaymentType() == 'b') {
                                                                   echo image_tag('expired.png', array('alt' => "Expired", 'title' => "Expired"));
                                                               } else if ($invoice->getStatus() == 3) {
                                                                   echo image_tag('pending.png', array('alt' => "pending", 'title' => "pending"));
                                                               } else if ($invoice->getStatus() == 0) {
                                                                   echo image_tag('pending.png', array('alt' => "pending", 'title' => "pending"));
                                                               } else if ($invoice->getStatus() == 1) {
                                                                   echo image_tag('cancel.png', array('alt' => "Cancel", 'title' => "Cancel"));
                                                               }
                                ?></td>
                                                           <td><?php
                                                               /* below code written for regenrate invoce recuring and part payment section handling
                                                                * by Santosh Kumar
                                                                *  on 02 May 2012
                                                                */
                                                               $urlParam = '';
                                                               if (!empty($invoice['RecurrentBilling']['0'])) {
                                                                   $urlParam = '&recurring=' . cryptString::encrypt(1);
                                                               }
                                                               if ($invoice->getPartPayment() != '' && $invoice->getPartPayment() != 0) {

                                                                   $part_payment = $invoice->getPartPayment();
                                                                   $urlParam .='&part_payment=' . cryptString::encrypt($part_payment);
                                                               }

                                                               if ($invoice->getStatus() == 0) {
                                ?>
                                                                   <!-- Varun : Encrypted id  -->
                                                                   <a href="<?php echo url_for('invoice/toggleInvoiceStatus?id=' . cryptString::encrypt($invoice->getId())); ?> "><?php echo image_tag('tdDelete.png', array('alt' => "Cancel", 'title' => "Cancel")); ?></a>
<?php } elseif ($invoice->getStatus() != 3) {
?>
                                                                   <a href="<?php echo url_for('invoice/new?id=' . cryptString::encrypt($invoice->getId()) . $urlParam); ?> "><?php echo image_tag('tdtick.png', array('alt' => "Regenerate Invoice", 'title' => "Regenerate Invoice")); ?></a>
<?php } ?></td>
                                    <?php
                                                           } else {
                                    ?>
                                <?php
                                                               // Added @Saurabh on 25 June, 2012
                                                               if (isset($_SESSION['logedin_user_group']) && $_SESSION['logedin_user_group'] == 'teller') {
                                ?>
                                                                   <td><a href="<?php echo url_for('invoice/show?id=' . cryptString::encrypt($invoice->getId())); ?>"><?php echo $invoice->getInvoiceNo() ?></a></td>
<?php } else {
?>
                                                                   <td><?php echo $invoice->getInvoiceNo() ?></td>
<?php } ?>

<?php if (isset($tellerLogin) && $tellerLogin) { ?><td><?php echo $invoice->getDeliveryDate(); ?></td> <?php } else {
?><td><?php echo ucfirst($invoice->getMerchantName()) ?></td><?php } ?>
                                                               <td><?php echo $invoice->getCustomer()->getCustomerId(); ?></td>
                                                               <td><?php echo $invoice->getDueDate() ?></td>
                                                               <td ><?php echo ucfirst($invoice->getCurrencyType()) ?></td>
                                                               <!-- Varun : Amount format corrected-->
                                                               <td align="left"><?php $totAmt = $helperOb->price_format($invoice['subtotal'] + $invoice['tax']);
                                                               echo $totAmt; //echo $invoiceAmountArray[$index_for_getting_invoice_amount++];           ?></td>
                                    <?php if ($expired == 'false') {
 ?>
                                                                   <td align="left"><?php $amtPaid = $helperOb->price_format($invoice['amount_paid']);
                                                                   echo $amtPaid; ?></td>
                                                               <td align="left"><?php $dedAmt = $helperOb->price_format($invoice['subtotal'] + $invoice['tax'] - $invoice['amount_paid']);
                                                                   echo $dedAmt; ?></td>
                                <?php } ?>
                                                               <td><?php
                                                               if ($expired == 'false') {
                                                                   if ($invoice->getStatus() == 3) {
                                                                       echo image_tag('partially-paid.png', array('alt' => "Partially Paid", 'title' => "Partially Paid"));
                                                                   } else {
                                                                       echo image_tag('pending.png', array('alt' => "Pending", 'title' => "Pending"));
                                                                   }
                                                               } else {
                                                                   if ($invoice->getStatus() == 3) {
                                                                       echo image_tag('partiallypaid_expired.png', array('alt' => "PartiallyPaid/Expired", 'title' => "PartiallyPaid/Expired"));
                                                                   } else {
                                                                       echo image_tag('expired.png', array('alt' => "Expired", 'title' => "Expired"));
                                                                   }
                                ?>
                                    <?php } ?></td>

                                <?php
                                                               /* @Saurabh At 22June, 2012
                                                                *  In case of teller, action would not be displayed
                                                                */
                                                               if (isset($_SESSION['logedin_user_group']) && $_SESSION['logedin_user_group'] != 'teller') {
                                ?>
                                                                   <td>
                                                                       <a href="<?php echo url_for('invoice/show?id=' . cryptString::encrypt($invoice->getId())); ?>">
                                        <?php if ($expired == 'false' && $invoice->getPaymentType() == 'o') {
 ?>Make Payment
                                        <?php } else {
                                        ?>View Invoice
<?php } ?>
                                                               </a>
                                                           </td>
<?php } /**/ ?>


<?php } ?>
                                                       </tr>
                            <?php endforeach; ?>
                            <?php
                                                       } else {
                                                           echo "<tr><td colspan='10' align='center'>No Invoice Found</td></tr>";
                                                       }
                            ?>
                                                   </tbody>
                                               </table>
                    <?php
                                                       if ($addCreateInvoiceButton) {

                                                           echo "<input type='button' class='blueBtn' value='Back' onclick=window.location='" . url_for("custMgt/listingForTeller") . "'>";
                                                       }
                    ?>
                                                   </div>
                                                   <div class="nofoList">
                    <?php
                                                       include_partial('global/records_number', array('amount_footer' => false, 'current_offset' => isset($current_offset) ? $current_offset : null, 'offset_next' => isset($offset_next) ? $offset_next : null, 'total_no_of_records' => isset($total_no_of_records) ? $total_no_of_records : $i, 'no_of_records_per_page' => isset($no_of_records_per_page) ? $no_of_records_per_page : null, 'from' => '', 'total_amount_of_all_transactions' => isset($total_amount_of_all_transactions) ? $total_amount_of_all_transactions : null));
                    ?>
                                                   </div>
                                                   <div align="right">
                    <?php
                                                       if ($setPagination) {
                                                           if ($merchnatLogin) {
//  $array_for_pagination = pay4billHelper::trimArray(array('moduleName' => $module, 'amount_footer' => false, 'actionName' => $actionTobeCalled, 'current_offset' => $current_offset, 'offset_next' => $offset_next, 'offset_prev' => $offset_prev, 'offset_last' => $offset_last, 'search' => '', 'from' => '', 'to' => '', 'date_cat' => '', 'email_address' => $email_address, 'cust_name' => $cust_name, 'invoice_no' => $invoice_no));
//                                                       include_partial('global/pagination', $array_for_pagination);

                                                               include_partial('global/pagination', array('moduleName' => $module, 'amount_footer' => false, 'actionName' => $actionTobeCalled, 'current_offset' => $current_offset, 'offset_next' => $offset_next, 'offset_prev' => $offset_prev, 'offset_last' => $offset_last, 'from' => $from_date, 'to' => $to_date, 'customer_id' => $customer_id, 'date_cat' => $date_cat, 'email_address' => $email_address, 'cust_name' => $cust_name, 'invoice_no' => $invoice_no));
                                                           } else {

                                                               include_partial('cust_pagination', array('amount_footer' => false, 'expired' => $expired, 'moduleName' => $module, 'current_offset' => $current_offset, 'offset_next' => $offset_next, 'offset_prev' => $offset_prev, 'offset_last' => $offset_last, 'com_name' => $com_name, 'invoice_no' => $invoice_no, 'from' => $from_date, 'to' => $to_date, 'customer_id' => $customer_id, 'date_cat' => $date_cat));
                                                           }
                                                       }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
