<script>
    
    function onBlurTxtBox(fieldName){

        formErr = true;
        for(i=0;i<4;i++){

            if(fieldName == 'item_no'+i){
                $('#err_item_no'+i).html('');
                if($('#item_no' + i).val() == ''){
                    $('#err_item_no'+i).html('Please enter Number of Item.');
                    formErr = false;
                } else
                    if(!$('#item_no' + i).val().match(/^([1-9]){1,}[0-9]{0,2}$/)){
                        $('#err_item_no'+i).html('Please enter valid Number of Item.');
                        formErr = false;
                    }
            }
            if(fieldName == 'item_desc'+i){
                $('#err_item_desc'+i).html('');
                if($('#item_desc' + i).val() == ''){
                    $('#err_item_desc'+i).html('Please enter Item Description.');
                    formErr = false;
                }
            }
            if(fieldName == 'item_price'+i){
                $('#err_item_price'+i).html('');
                if($('#item_price' + i).val() == ''){
                    $('#err_item_price'+i).html('Please enter Item Unit Price.');
                    formErr = false;
                } else
                    value = $('#item_price' + i).val();
                if(value.match(/^[1-9]+([0-9])*(?:\.[0-9]{1,3})?$/) || value.match(/^(0){1}(\.){1}([1-9]){1,3}$/) || value.match(/^(0){1}(\.){1}(0){0,2}([1-9])$/))
                {
                } else {
                    $('#err_item_price'+i).html('Please enter valid Item Unit Price.');
                    formErr = false;
                }
            }
        }
    }

    function callOnSubmit(){
        retrn = true
        $('#err_customer').html('');
        $('#err_issue_date').html('');
        $('#err_delivery_date').html('');
        $('#err_due_date').html('');
        $('#err_additional_information').html('');

        if($('#invoice_customer_id').val() == ''){
            $('#err_customer').html('&nbsp;Please select the Customer.');
            retrn = false;
        }

        if($('#invoice_issue_date_date').val() == ''){
            $('#err_issue_date').html('&nbsp;Please select the Issue Date.');
            retrn = false;
        }
        if($('#invoice_delivery_date_date').val() == ''){
            $('#err_delivery_date').html('&nbsp;Please select the Delivery Date.');
            retrn = false;
        }
        if($('#invoice_due_date_date').val() == ''){
            $('#err_due_date').html('&nbsp;Please select the Due Date.');
            retrn = false;
        }

        if($('#invoice_additional_information').val() == ''){
            $('#err_additional_information').html('&nbsp;Please enter the Additional Information .');
            retrn = false;
        }

        for(k=0;k<=5;k++){
            if($('#item_no'+k).length > 0 && $('#item_no'+k).val() == ''){
                $('#err_item_no'+k).html('Please enter Number of Item.');
                retrn = false;

            }
            if($('#item_desc'+k).length > 0 && $('#item_desc'+k).val() == ''){
                $('#err_item_desc'+k).html('Please enter Item Description.');
                retrn = false;
            }
            if($('#item_price'+k).length > 0 && $('#item_price'+k).val() == ''){
                $('#err_item_price'+k).html('Please enter Item Unit Price.');
                retrn = false;
            }
        }
        return retrn;
    }
    
    var myWidth = 0, myHeight = 0;
    if( typeof( window.innerWidth ) == 'number' ) {
        //Non-IE
        myWidth = window.innerWidth;
        myHeight = window.innerHeight;
    } else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
        //IE 6+ in 'standards compliant mode'
        myWidth = document.documentElement.clientWidth;
        myHeight = document.documentElement.clientHeight;
    } else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
        //IE 4 compatible
        myWidth = document.body.clientWidth;
        myHeight = document.body.clientHeight;
    }

    function getNewRowdId(j)
    {
        rowChange =1;
        for(incrRow=1;incrRow<4;incrRow++) {
            $('#tableId tr').each(function(){
                if(($(this).attr('id'))!='')
                {
                    //var str = $(this).attr('id').substr(-1);
                    var str = $(this).attr('id').substr($(this).attr('id').length - 1);

                    if(incrRow!=str && rowChange==1) {
                        j = incrRow;
                        rowChange++;
                    }
                    else if(incrRow==str){
                        j = incrRow+1;
                        rowChange++;
                    }
                }
            })
            if(rowChange==2) {
                break;
            }
        }
        if(j==4) {
            j=2;
        }
        return j;
    }
    
    $(document).ready(function(){

        moreOptions('.recurr' , '.rOpt_new'  );
        moreOptions('.app' , '.pMore'  );
        closeAlert();
        $('.invoiceTb tr:odd').addClass('alt');
        $('.tableHolder tr:odd').addClass('alt');

        setInvoiceTable ();
        //calTotal1();
        var i=1;
        $('#Invoice').addClass('active');
        $(".error").css('display','none');



        $('.addRows').live('click' ,  function ()
        {
            
            var tRow = $("#tableId tr").size();
            if ($.browser.msie){
                //var maxRowCount=6;
                var maxRowCount=5;
            }
            else
            {
                var maxRowCount=5
            }

            
            /* if(tRow < maxRowCount) {
                // var newRow = ('<tr><td><input name="item_no[]" type="text" maxlength="3" onkeydown="calTotal1();" onkeyup="calTotal1();" onchange="calTotal1();"/></td><td><input name="item_desc[]" type="text" /></td><td><input name="item_price[]" type="text" maxlength="10" onkeydown="calTotal1();" onkeyup="calTotal1();" onchange="calTotal1();"/></td><td><input name="item_tax[]" type="text" maxlength="6" onkeydown="calTotal1();" onkeyup="calTotal1();" onchange="calTotal1();"/></td><td><img src="' + get_base_url() + 'assets/images/delete.png"  alt="Delete" title="Delete entry" class="deleteRow"/></td></tr>');
                var newRow = ('<tr id="invoiceRow'+i+'" ><td style="vertical-align:top;"><input name="item_no'+i+'" id="item_no'+i+'" type="text" maxlength="3" onkeydown="calTotal1();" onkeyup="calTotal1();" onchange="calTotal1();"/></td>\n\
 <td style="vertical-align:top;"><input name="item_desc'+i+'" id="item_desc'+i+'"  type="text" /></td>\n\
<td style="vertical-align:top;"><input name="item_price'+i+'" id="item_price'+i+'" type="text"  onkeydown="calTotal1();" onkeyup="calTotal1();" onchange="calTotal1();"/></td>\n\
<td style="vertical-align:top;"><img src="../../images/delete.png"  id='+ i+' alt="Delete" title="Delete entry" class="deleteRow"/></td></tr>');
             */

            j = tRow - 1;
            j= getNewRowdId(j);
            if(tRow < maxRowCount) {

                var newRow = ('<tr id="invoiceRow'+j+'" ><td style="vertical-align:top;"><input onblur="javascript: onBlurTxtBox(\'item_no'+j+'\')" class="FormInput9" name="item_no'+j+'" id="item_no'+j+'" type="text" maxlength="3" onkeydown="calTotal1();" onkeyup="calTotal1();" onchange="calTotal1();"/>\n\
<div style="color:red;clear:both" id="err_item_no'+j+'"></div></td>\n\
 <td style="vertical-align:top;"><input  onblur="javascript: onBlurTxtBox(\'item_desc'+j+'\')" class="FormInput9" name="item_desc'+j+'" id="item_desc'+j+'"  type="text" />\n\
<div style="color:red;clear:both" id="err_item_desc'+j+'"></div></td>\n\
 <td style="vertical-align:top;"><input onblur="javascript: onBlurTxtBox(\'item_price'+j+'\')" class="FormInput9" name="item_price'+j+'" id="item_price'+j+'" type="text"  onkeydown="calTotal1();" onkeyup="calTotal1();" onchange="calTotal1();"/>\n\
<div style="color:red;clear:both" id="err_item_price'+j+'"></div>\n\
</td>\n\
 <td style="vertical-align:top;"><img src="<?php echo image_path('delete.png'); ?>"  id='+ j+' alt="Delete" title="Delete entry" width="16" class="deleteRow" style="cursor:pointer"/>&nbsp;\n\
<a href="javascript:void(0)" class="addRows"><img src="<?php echo image_path('add-icon.png'); ?>"  id="add'+ j+'" alt="Add" style="cursor:pointer"/></a></td></tr>');



                 if($('#add0').length > 0)  $('#add0').hide();
                 if($('#add1').length > 0)  $('#add1').hide();
                 if($('#add2').length > 0)  $('#add2').hide();
                 if($('#add3').length > 0)  $('#add3').hide();


                 var lastTr = 	$('.invoiceTb tbody  tr:last-child');
                 //                alert($("#tableId tr").attr('id'));
                 //        alert(lastTr);
                 lastTr.after(newRow);
                
                 if(tRow==4) { $('#add3').hide();  /*$('#add0').show();*/        }
                
                 //lastTr.addClass('test');
                 var fresh = $('.invoiceTb table tr:last-child');
                 fresh.fadeTo(0,0);
                 fresh.fadeTo('slow',1);
                 setInvoiceTable ();
                 $('.invoiceTb tr').removeClass('alt');
                 $('.invoiceTb tr:odd').addClass('alt');
                 //        alert('will add rules');
                 //                $('#item_no'+i).rules('add','required');
                 //                $('#item_no'+i).rules('add','isValidWholeNo_zeroNotAllowed');
                 //                $('#item_desc'+i).rules('add','required');
                 //                $('#item_price'+i).rules('add','required');
                 //                $('#item_price'+i).rules('add','isValidPrice');
                 //remove item tax
                 //                $('#item_tax'+i).rules('add','required');
                 //                //        $('#item_tax'+i).rules('add','isValidTaxValue');
                 //                $('#item_tax'+i).rules('add','isValidTaxValue');
                 i++;
             }else{
                 alert('You have reached at maximum limit!');
             }
         });


         $('.app').bind('click' ,  function ()
         {

             if($(this).attr('checked')){
                 $('#app_textbox').rules("add", "required");
                 $('#app_textbox').rules("add", "isValidTaxValue");
             }
             else
             {
                 $('label[for=app_textbox]').html('');
                 $('#app_textbox').rules('remove');
                 $('#app_textbox').val('');

             }

         });
         $('.recurr').bind('click' ,  function ()
         {

             if($(this).attr('checked')){
                 $('input[name=recurring_plan]').rules("add", "required");
                 $('input[name=recurring]').addClass('redLbl');
                 //                $('input[name=recurring_plan]').rules("add", "isValidnumber");
             }
             else
             {

                 //alert($('label[for=recurring_plan]'));
                 $('label[for=recurring_plan]').html('');
                 $('input[name=recurring_plan]').rules('remove');
                 $('input[name=recurring_plan]').val('');


             }

         });
         $('input[name=recurring_plan]').bind('click' ,  function ()
         {
             $('#custom_recurring_bill_days').css('display', 'none');
             //                alert($('label[for=custom_recurring_bill_days]'));
             $('label[for=custom_recurring_bill_days]').hide();
             $('#custom_recurring_bill_days').rules('remove');
         });
         $('#custom_recurring_plan').bind('click' ,  function ()
         {

             if($(this).attr('checked')){
                 $('#custom_recurring_bill_days').rules("add", "required");
                 $('#custom_recurring_bill_days').css('display', 'inline');
                 $('#custom_recurring_bill_days').rules("add", "isValidWholeNo_zeroNotAllowed");
             }
             else
             {
                 $('#custom_recurring_bill_days').css('display', 'none');
                 $('#custom_recurring_bill_days').rules('remove');
                 $('#custom_recurring_bill_days').val('');


             }

         });

         $('form').attr('id','division_frm');


         jQuery.validator.addMethod('isValidTaxValue', function(value, element) {
             valuesplit = value.split('/');
             value = valuesplit[1];
             var error=true;
             // Varun : Check for value existence , if value is null calculation and validations will not be done.
             if(value=='')
             {
                 error=false;
             } else {
                 if(value.match(/^[1-9]{1,}([0-9])*(?:\.[0-9]{1,3})?$/) || value.match(/^(0){1}(\.){1}([0-9]){1,2}$/) )
                 {

                     error=false;
                 }
                 if(!error)
                 {
                     //                        alert(parseFloat(value));
                     if(parseFloat(value)<100)
                     {
                         error=false;
                     }
                     else
                     {
                         error=true;
                     }
                 } }
             return !error;
         },'Invalid(Max 99.99)');
         jQuery.validator.addMethod('isValidPrice', function(value, element) {
             var error=true;

             if(value.match(/^[1-9]+([0-9])*(?:\.[0-9]{1,3})?$/) || value.match(/^(0){1}(\.){1}([1-9]){1,3}$/) || value.match(/^(0){1}(\.){1}(0){0,2}([1-9])$/) )
             {

                 error=false;
             }
             if(!error)
             {
                 //                        alert(parseFloat(value));
                 if(parseFloat(value)<=99999999)
                 {
                     error=false;
                 }
                 else
                 {
                     error=true;
                 }
             }
             return !error;

         },'Invalid(Max 99999999)');
         jQuery.validator.addMethod('isValidnumber', function(value, element) {

             return this.optional(element) || value.match(/^[1-9]+(?:\.[0-9]{1,3})?$/) || value.match(/^(0){1}(\.){1}([1-9]){1,3}$/) || value.match(/^(0){1}(\.){1}(0){0,2}([1-9])$/);
         },'Invalid');
         jQuery.validator.addMethod('isValidWholeNumber', function(value, element) {

             return this.optional(element) || value.match(/^[0-9]*$/);
         },'Invalid');

         jQuery.validator.addMethod('isValidWholeNo_zeroNotAllowed', function(value, element) {
             //            alert();
             return this.optional(element) || value.match(/^([1-9]){1,}[0-9]{0,2}$/);
         },'Invalid');





         //        jQuery.validator.addMethod('isValidInvoiceNumber', function(value, element) {
         //
         //            return this.optional(element) || value.match(/[^\s+]/);
         //        },'Invalid Invoice Number(Only Alfa Numeric)');
         $("#division_frm").validate({
             //            rules: {
             //                //                'invoice[invoice_no]': {
             //                //                    required: true,
             //                //                    maxlength: 10,
             //                //                    isValidInvoiceNumber: true
             //                //
             //                //                },
             //                'invoice[issue_date][day]': {
             //                    future_chk_issue_date: true
             //
             //                }, 'invoice[delivery_date][day]': {
             //                    future_chk_delivery_date: true
             //                }, 'invoice[due_date][day]': {
             //
             //                    future_chk_due_date: true
             //                }, 'item_no0': {
             //                    required: true,
             //                    isValidWholeNo_zeroNotAllowed: true
             //
             //                }, 'item_desc0': {
             //                    required: true
             //
             //                }, 'item_tax0': {
             //                    required: false, //Varun: Tax field is Not Mandatory , customer requirement
             //                    isValidTaxValue: true
             //                    //                    max: 99.99
             //
             //
             //
             //                }, 'item_price0': {
             //                    required: true,
             //                    isValidPrice: true
             //
             //
             //                },
             //                'invoice[additional_information]':
             //                    {
             //                    required: true
             //
             //                }
             //            },
             //
             //            messages: {
             //                //                'invoice[invoice_no]':
             //                //                    {
             //                //                    required: "Invoice Number is Required",
             //                //                    maxlength: "Maximum 10 Characters"
             //                //
             //                //                },
             //                'item_no0':
             //                    {
             //                    required: "Required"
             //
             //                },
             //                'item_desc0':
             //                    {
             //                    required: "Required"
             //
             //                },
             //                'item_tax0':
             //                    {
             //                    required: "Required"
             //
             //                },
             //                'item_price0':
             //                    {
             //                    required: "Required"
             //
             //                }
             //                ,
             //
             //                'invoice[additional_information]':
             //                    {
             //                    required: "Additional Information is Required"
             //
             //                },
             //
             //                'custom_recurring_bill':
             //                    {
             //                    required: "Days Required"
             //
             //                }
             //                ,
             //
             //                'part_payment_percent':
             //                    {
             //                    required: "Percentage is Required"
             //
             //                },
             //                'recurring_plan':
             //                    {
             //
             //                    required: "Recurring Plan is Required"
             //                }
             //
             //
             //            }


         })

     });
     function setInvoiceTable ()
     {



         $('.invoiceTb .deleteRow').bind('click' ,function()
         {
             var numRow =  0 ;
             $('.invoiceTb tbody tr ').each(function(index)
             {
                 numRow ++;
             });
             //            alert(numRow);
             if(numRow >  1)
             {
                 var row_to_be_removed=parseInt(this.id);
                 var temp = $(parent.document.getElementById("invoiceRow"+row_to_be_removed));
                 //                           alert(row_to_be_removed);
                 $('#item_no'+row_to_be_removed).rules('remove');
                 $('#item_desc'+row_to_be_removed).rules('remove');
                 //remove item tax
                 //                $('#item_tax'+row_to_be_removed).rules('remove');
                 $('#item_price'+row_to_be_removed).rules('remove');
                 $(temp).animate({
                     opacity: 0
                 } ,
                 {
                     duration: 'slow',
                     complete: function()
                     {
                         $(temp). remove();
                         $('.invoiceTb tr').removeClass('alt');
                         $('.invoiceTb tr:odd').addClass('alt');

                         //getLastIdForPlus = $('#tableId tr:last').attr("id").substr(-1)
                         getLastIdForPlus = $('#tableId tr:last').attr("id").substr($('#tableId tr:last').attr("id").length - 1);
                         //                         alert(getLastIdForPlus);
                         $('#add'+ getLastIdForPlus).show();
                         
                         $(this).unbind("click");

                         calTotal1();//function to calculate the total of invoice

                     }
                 } );
             }

         }

     );
         $('.dialog').animate({
             left :( myWidth / 2 )- 150
         } ,0);
         $('.dialog').animate({
             top :300
         } ,0);
         $('.dialog').fadeTo(0,0);
         $('.dialog').fadeTo(1100,1);

     }
     function  moreOptions( recur  , opt)
     {
         /* below code written for regenrate invoce recuring and part payment section handling
          * by Santosh Kumar
          *  on 02 May 2012
          */
         if(recur=='.recurr'){
<?php if (isset($recurring) && $recurring != '') { ?>
                            $(recur).attr({
                                checked: "checked"
                            });
<?php } else { ?>
                            $(opt).hide();
<?php } ?>
                    }

                    if(recur=='.app'){
<?php if (isset($part_payment) && $part_payment != '') { ?>
                            $(recur).attr({
                                checked: "checked"
                            });
<?php } else { ?>
                            $(opt).hide();
<?php } ?>
                    }

                    $(recur).bind('click',

                    function()
                    {
                        var temp = $(recur).attr('checked');

                        if( temp)
                        {
                            $(opt).slideDown();

                            $(opt).animate({
                                opacity: 1
                            }, 'slow');


                        }
                        if( temp  == false)
                        {


                            $(opt).animate({
                                opacity: 0
                            } ,  {
                                complete :  function()

                                {
                                    $(this).slideUp('slow');

                                }

                            });
                        }

                    });

                }
                function closeAlert()
                {
                    $(".closeBtn").bind('click',  function()
                    {
                        $(this).parent().animate({
                            opacity: 0
                        },{
                            duration:'slow',
                            complete : function ()

                            {
                                $(this).slideUp("slow");
                            }
                        });

                    });

                }
                function calTotal1()
                {

                    var tRow = $("#tableId tr").size();

                    var myItem = 0;
                    var myPrice = 0;
                    var myTax = 0;
                    var subTax = 0;


                    //pwinning = total = counter = stake = 0;
                    var total = 0;
                    var total_tax = 0;
                    var subtotal = 0;

                    //    alert(item_num.length);
                    //
                    //    alert(item_num.length);
                    //
                    if(tRow == 1)
                    {
                        myItem = $('#item_no0').val()
                        myPrice = parseFloat($('#item_price0').val());
                        myTaxsplit = $('#item_tax0').val();
                        myTaxsplit = myTaxsplit.split('/');
                        myTax = myTaxsplit[1];

                        if((isNumeric(myItem) && isValidFloat(myPrice)) || isValidFloat(myTax))
                        {
                            if(!isValidFloat(myTax) || (myTax > 100) || (myTax < 0))
                                myTax = 0;

                            if(!isNumeric(myItem) || (myItem < 0))
                                myItem = 0;

                            if(!isValidFloat(myPrice) || (myPrice < 0))
                                myPrice = 0;

                            //alert(myTax);

                            subtotal = myItem * myPrice;
                            total = subtotal + ((myTax/100) * subtotal);

                            document.getElementById("mytotal").value = total.toFixed(2);
                            document.getElementById("mysubtotal").value = subtotal.toFixed(2);
                        }
                        else
                        {
                            document.getElementById("mytotal").value = "0.00";
                            document.getElementById("mysubtotal").value = "0.00";
                        }


                    }
                    else
                    {	//alert(item_num.length)
                        for(i=0; i < tRow; i++)
                        {
                            //            myItem = parseFloat(item_num[i].value);
                            //            myPrice = parseFloat(item_price[i].value);
                            //            myTax = parseFloat(item_tax[i].value);
                            myItem = parseFloat($('#item_no'+i).val());
                            myPrice = parseFloat($('#item_price'+i).val());
                            //                myTax = parseFloat($('#item_tax'+i).val());//Item tax is single entry
                            myTaxsplit = $('#item_tax0').val();
                            myTaxsplit = myTaxsplit.split('/');
                            myTax = myTaxsplit[1];
                            if((isNumeric(myItem) && isValidFloat(myPrice)) || isValidFloat(myTax))
                            {
                                if(!isValidFloat(myTax) || (myTax > 100) || (myTax < 0))
                                    myTax = 0;

                                if(!isNumeric(myItem) || (myItem < 0))
                                    myItem = 0;

                                if(!isValidFloat(myPrice) || (myPrice < 0))
                                    myPrice = 0;

                                //alert(myTax);

                                subtotal = subtotal + (myItem * myPrice);
                                subTax = subTax + ((myTax/100) * (myItem * myPrice));
                            }
                        }

                        total = subtotal + subTax;

                        document.getElementById("mytotal").value = total.toFixed(2);
                        document.getElementById("mysubtotal").value = subtotal.toFixed(2);

                    }

                }


</script>

<?php
if (isset($addClassTo)) {
    include_partial('subHeader', array('addClassTo' => $addClassTo, 'loginType' => 1));
} else {
    include_partial('subHeader', array('addClassTo' => 'new', 'loginType' => 1));
}
?>
<div class="clear_new"></div>
<?php if (isset($getMerchantTaxDetails) && $getMerchantTaxDetails != '') {
?>
<?php include_partial('form', array('form' => $form, 'invoiceDetails' => $invoiceDetails, 'getMerchantTaxDetails' => $getMerchantTaxDetails, 'recurring' => $recurring, 'part_payment' => $part_payment, 'recurring_plan' => $recurring_plan, 'getTaxDetail' => $getTaxDetail, 'merchant_id' => $merchant_id, 'id' => $id, 'showRecurringPartPaymentInputs' => $showRecurringPartPaymentInputs)) ?>
<?php } else {
 ?>
<?php include_partial('form', array('form' => $form, 'arr' => $invoiceDetails, 'merchant_id' => $merchant_id, 'id' => $id, 'recurring' => $recurring, 'part_payment' => $part_payment, 'recurring_plan' => $recurring_plan, 'showRecurringPartPaymentInputs' => $showRecurringPartPaymentInputs)) ?>
<?php } ?>
