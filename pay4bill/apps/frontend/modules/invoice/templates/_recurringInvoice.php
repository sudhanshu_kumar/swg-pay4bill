<?php
$item_info_obs = Doctrine::getTable('InvoiceItemDetails')->findBy('invoice_id', $invoice_id);
$recur_ob = Doctrine::getTable('RecurrentBilling')->findBy('invoice_id', $invoice_id)->getFirst();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Untitled Document</title>
    </head>
    <body>
        <table align="center" cellpadding="0" cellspacing="0" style="width:650px;">
            <tr>
                <td style="padding-bottom:5px;"><?php include_partial('global/pay4bill_logo', array('path' => $path)); ?></td>
           </tr>
            <table cellspacing="0" cellpadding="10" border="0" align="center" bgcolor="#f0f0f0" style="width:650px; border-collapse:collapse; border:1px solid #dedcdc; color:#646464; font-family:Arial, Helvetica, sans-serif; font-size:14px;">
                <!--tr><td>Dear  <span><?php echo $customerName; ?></span>,</td></tr>
                <tr>
                    <td align="justify">
                        <?php /* echo "Your " . $recur_ob->getType() . " payment for " . $companyName . " is almost due.In
    order to meet up with your payment , please ensure to pay your bill by " . date("d-M-Y", strtotime($dueDate)) . ". If you fail to do so you will have to contact " . $companyName . " directly to pay and to issue a new recurring bill for future payments.";
                       */ ?>     
                        
                    </td>
                </tr-->
                <tr>
                    <td>Dear  <span><?php echo $customerName; ?></span>,</td>
                </tr>
                <tr>
                    <td>
                        You have a bill waiting for payment at
                        <a href="www.pay4bill.com" style="color:#0c5c89;">www.pay4bill.com</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        The Invoice Detail is as follows:
                    </td>
                </tr>
                <!--tr>
                    <td><strong style="color:#000"> <?php //echo $companyName; ?></strong></td>
                </tr>
                <tr>
                    <td><strong style="color:#000">  <?php //echo $company_url; ?></strong></td>
                </tr-->
                 <tr>
                    <td><strong style="color:#000">Customer Id: </strong><?php echo $item_info_obs->getFirst()->getInvoice()->getCustomer()->getCustomerId(); ?></td>
                </tr>
                <tr>
                    <td><strong style="color:#000">Issue Date: </strong><?php echo $issueDate; ?></td>
                </tr>
                <tr>
                    <td><strong style="color:#000">Delivery Date: </strong><?php echo $deliveryDate; ?></td>
                </tr>
                <tr>
                    <td><strong style="color:#000">Due Date: </strong><?php echo $dueDate; ?></td>
                </tr>
                <tr>
                    <td><strong style="color:#000">  Invoice Number: </strong><?php echo $invoiceNo; ?></td>
                </tr>
                
                <!--tr>
                    <td><strong style="color:#000">Next Invoice Date: </strong><?php echo $next_invoice; ?></td>
                </tr>
                <tr>
                    <td><strong style="color:#000">  Invoice Items:</strong></td>
                </tr-->
                <tr>
                    <td>
                        <table border="0" cellpadding="1" cellspacing="1" style="background:#8d8d8d;" width="50%">
                            <tr><td style='background:#f0f0f0'><b>Quantity</b></td><td style='background:#f0f0f0'><b>Description</b></td><td style='background:#f0f0f0'><b>Cost</b></td></tr>
                            <?php
                            $r = new InvoiceItemDetails();

                            $subttl = 0;
                            $ttl_tax = 0;
                            foreach ($item_info_obs as $item_ob) {
                                echo "<tr>";
                                echo "<td style='background:#f0f0f0'>" . $item_ob->getNoOfItems() . "</td>";
                                echo "<td style='background:#f0f0f0'>" . $item_ob->getItemDescription() . "</td>";
                                echo "<td style='background:#f0f0f0' align='right'>" . number_format($item_ob->getItemPrice(),'2','.',','). "</td>";
                                echo "</tr>";
                                $subttl+=$item_ob->getSubTotal();
                                $ttl_tax+=$item_ob->getTaxTotal();
                            }
                            echo "</table>";
                            echo "<br><table>
                                <tr>
                                <td>Sub-Total:</td>
                                <td>NGN</td>
                                <td align='right'>".number_format($subttl,2,'.',',')."</td>
                                </tr>
                                <tr>
                                <td>Total Tax:</td>
                                <td>NGN</td>
                                <td align='right'>".number_format($ttl_tax,2,'.',',')."</td>
                                </tr>
                                <tr><td>Total:</td>
                                <td>NGN</td><td align='right'>" . number_format(($subttl + $ttl_tax),2,'.',',') . "</td></tr>
                                </table>";
                            ?>
                            <tr>
                                <td><strong style="color:#000">Additional Information: &nbsp;</strong><?php echo $additionalInfo; ?></td>
                            </tr>
                             <tr>
                                <td>Please click on the link below and login to make payment:</td>
                            </tr>
                            <tr>
                                <td><a href="<?php echo $completeUrl; ?>" style="color:#0c5c89;"><?php echo $completeUrl; ?></a></td>
                            </tr>
                            <tr>
                                <td>Regards,<br />The Pay4bill Team</td>
                            </tr>
                             <tr>
                    <td style="background-color:#18648e; color:#ffffff; padding:10px; font:14px Arial, Helvetica, sans-serif;">If you experience any problem, write to us at <a href="mailto:support@pay4bill.com" style="color:#fff">support@pay4bill.com</a></td>
                </tr>
                 </table>
                    </td></tr>
               
            </table>
        </table>
    </body>
</html>
<?php
//                            $emailText = "<b>" . $companyName . "</b><br>" . $company_url . "<br><br><b>Invoice Number:</b>" . $invoiceNo . "<br><b>Due Date:</b>" . date("d-M-Y", strtotime($dueDate)) . "<br><b>Issue Date: </b>" . date("d-M-Y", strtotime($issueDate)) . "<br><b>Next Invoice Date: </b>" . $next_invoice;
//                            $emailText.="<br><br><b>Invoice Items</b><br>";
//
//                            $emailText.="<table border=1>";
//                            $emailText.="<tr><td><b>Quantity</b></td><td><b>Description</b></td><td><b>Cost</b></td></tr>";
//
//                            $item_info_obs = Doctrine::getTable('InvoiceItemDetails')->findBy('invoice_id', $invoice_id);
//                            $recur_ob = Doctrine::getTable('RecurrentBilling')->findBy('invoice_id', $invoice_id)->getFirst();
////
//////$emailText.=var_dump($item_info_obs);
//////        $r=new InvoiceItemDetails();
//////        $r->
//                            $subttl = 0;
//                            $ttl_tax = 0;
//                            foreach ($item_info_obs as $item_ob) {
//                                $emailText.="<tr>";
//                                $emailText.="<td>" . $item_ob->getNoOfItems() . "</td>";
//                                $emailText.="<td>" . $item_ob->getItemDescription() . "</td>";
//                                $emailText.="<td>" . $item_ob->getItemPrice() . "</td>";
//                                $emailText.="</tr>";
//                                $subttl+=$item_ob->getSubTotal();
//                                $ttl_tax+=$item_ob->getTaxTotal();
////
//                            }
//                            $emailText.="</table>";
//                            $emailText.="<table><tr><td>Sub-Total:</td><td>$subttl NGN</td></tr><tr><td>Total-Tax:</td><td>$ttl_tax NGN</td></tr>
//<tr><td>Total-NGN:</td><td>" . ($subttl + $ttl_tax) . "NGN</td></tr>
//
//</table>";
//                            $emailText.="Dear " . $customerName . ",<br><br><br>Your " . $recur_ob->getType() . " payment for " . $companyName . " is almost due.In
//    order to meet up with your payment , please ensure to pay your bill by " . date("d-M-Y", strtotime($dueDate)) . ". If you fail to do so you will have to contact " . $companyName . " directly to pay and to issue a new recurring bill for future payments.
//        <br><br>";
//                            $emailText.="<br>If you have any questions regarding your account, please feel free to contact us at:<br><br>info@pay4bill.com<br>
//    <br>Thank You!<br><br>The Pay4bill Team";
//echo $emailText;



                            