<?php
if ($loginType == 2) {
    $customerId = '?cust=' . $customerTellerId;
    $customerIdConcat = '?id=' . $customerTellerId;
?>
    <div id="pgMenu">
        <div id="wrappernew">
            <div class="pgTitle">

            <h2>Invoices</h2>
        </div>
        <div class="pgNav">
            <ul>
                <li <?php if ($addClassTo == 'index') { ?>class="current" <?php } ?>><a href="<?php echo url_for('invoice/index' . $customerIdConcat) ?>">View Invoices</a></li>
                <li <?php if ($addClassTo == 'new') { ?>class="current" <?php } ?>><a href="<?php echo url_for('invoice/new' . $customerId) ?>">Create Invoice</a></li>

            </ul>
        </div>
    </div>
</div>
<?php } else { ?>
            <div id="pgMenu">
                <div id="wrappernew">
                    <div class="pgTitle">
                        <h2>Invoices</h2>
        </div>
        <div class="pgNav">
            <ul>
                <li <?php if ($addClassTo == 'index') { ?>class="current" <?php } ?>><a href="<?php echo url_for('invoice/index?offset=0') ?>">View Invoices</a></li>
                <li <?php if ($addClassTo == 'new') { ?>class="current" <?php } ?>><a href="<?php echo url_for('invoice/new') ?>">Create Invoice</a></li>
                <li <?php if ($addClassTo == 'upload') { ?>class="current" <?php } ?>><a href="<?php echo url_for('invoice/upload') ?>">Upload CSV/XLS</a></li>

            </ul>
        </div>
    </div>
</div>
<?php } ?>
