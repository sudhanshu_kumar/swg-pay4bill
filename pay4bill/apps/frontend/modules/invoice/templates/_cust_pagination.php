<?php
// Added by Saurabh on 31May
$var = '';
if (isset($from) && $from != '') {
        $var .= '&from=' . $from;
    }

    if (isset($to) && $to != '') {
        $var .= '&to=' . $to;
    }
    if (isset($date_cat) && $date_cat != '') {
        $var .= '&date_cat=' . $date_cat;
    }
    if (isset($email_address) && $email_address != '') {
        $var .= '&email_address=' . cryptString::encrypt($email_address);
    }
    if (isset($cust_name) && $cust_name != '') {
        $var .= '&cust_name=' . $cust_name;
    }


    if (isset($mer_name) && $mer_name != '') {
        $var .= '&merchant_name=' . $mer_name;
    }
    if (isset($com_name) && $com_name != '') {
        $var .= '&com_name=' . $com_name;
    }
    if (isset($ban_name) && $ban_name != '') {
        $var .= '&ban_name=' . cryptString::encrypt($ban_name);
    }
    if (isset($invoice_no) && $invoice_no != '') {
        $var .= '&invoice_no=' . $invoice_no;
    }
    if (isset($cac_reg) && $cac_reg != '') {
        $var .= '&cac_registration=' . $cac_reg;
    }
    if (isset($merchant_id) && $merchant_id != '') {
        $var .= '&merchant_id=' . $merchant_id;
    }
    if (isset($name) && $name != '') {
        $var .= '&name=' . $name;
    }
    if (isset($emailaddress) && $emailaddress != '') {
        $var .= '&emailaddress=' . $emailaddress;
    }
    if (isset($customer_id) && $customer_id != '') {
        $var .= '&customer_id=' . $customer_id;
    }
    if (isset($user_name) && $user_name != '') {
        $var .= '&user_name=' . $user_name;
    }
    if (isset($phone_number) && $phone_number != '') {
        $var .= '&phone_number=' . $phone_number;
    }
    if (isset($invoice_status) && $invoice_status != '') {
        $var .= '&status=' . $invoice_status;
    }
    
    if (isset($both) && $both != '') {
        $var .= '&both=' . $both;
    }
     if (isset($partlyPaid) && $partlyPaid != '') {
        $var .= '&partlypaid=' . $partlyPaid;
    }

    if (isset($dynamicAttr) && !empty($dynamicAttr)) {
        if (!is_string($dynamicAttr) && is_object($dynamicAttr)) {
            foreach ($dynamicAttr as $attName => $val) {
                $var .= '&dynamicAttr[' . $attName . ']=' . $val;
            }
//            $var .= '&dynamicAttr=' . $dynamicAttr;
        }
    }
if (($current_offset / sfConfig::get('app_records_per_page')) != 0)
    $pageNo = $current_offset / sfConfig::get('app_records_per_page') + 1;
else
    $pageNo = 1;
?>

<a href="<?php
if ($current_offset != 0) {
    echo url_for($moduleName . '/index?offset=0&getExpired=' . $expired . $var);
} else {
    echo '#';
} ?>"><?php echo image_tag('pagination_1_first.png', array('title' => "First")); ?></a>
   <?php if ($current_offset != 0) {
   ?>
    <a href="<?php echo url_for($moduleName . '/index?offset=' . $offset_prev . '&getExpired=' . $expired . $var);
   ?>"><?php echo image_tag('pagination_1_previous.png', array('title' => "Previous")); ?></a>



<?php } ?>
<?php echo '<span style="color:#19658F;top:-3px; position:relative;">' . $pageNo . "</span>"; ?>



<?php if ($offset_next != $current_offset) {
?>
       <a href="<?php echo url_for($moduleName . '/index?offset=' . $offset_next . '&getExpired=' . $expired . $var); ?>"><?php echo image_tag('pagination_1_next.png', array('title' => "Next")); ?></a>
<?php } ?>
   <a href="<?php
   if ($offset_next != $current_offset) {
       echo url_for($moduleName . '/index') . '?offset=' . $offset_last . '&getExpired=' . $expired . $var;
   } else {
       echo "#";
   }
?>"><?php echo image_tag('pagination_1_last.png', array('title' => "Last")); ?></a>












