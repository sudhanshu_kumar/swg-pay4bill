<?php

/**
 * invoice actions.
 *
 * @package    nisp
 * @subpackage invoice
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class invoiceActions extends sfActions {

    public function executeIndex(sfWebRequest $request) {

        $this->addCreateInvoiceButton = pay4billHelper::checkIfTellerUser($this->getUser()->getGuardUser()->getGroups()->toArray());
        if ($this->addCreateInvoiceButton) {
            $this->customerTellerId = $customerTellerId = $request->hasParameter('id') ? $request->getParameter('id') : $this->getUser()->getAttribute('customerTellerId');

            $this->getUser()->setAttribute('customerTellerId', $this->customerTellerId);
        } else {

            $this->getUser()->getAttributeHolder()->remove('customerTellerId');
        }
        // Varun :Merchant key for section to be displayed to Merchant, variables for search result .
        $parameters = $request->getParameterHolder()->getAll();
        $helperObj = new pay4billHelper();
        $parameters = $helperObj->trimArray($parameters);

        //echo "<pre>";print_r($parameters);
        $this->merchant_key = array_key_exists('merchant_id', $parameters) ? $parameters['merchant_id'] : $this->getUser()->getGuardUser()->getMerchantUser()->getId();
        $this->form = new InvoiceSearchForm();

        /* Unset below session varibale which have not been set Customer -> Payment History
         * Function Name:executeAllPaymentsForCustomer && executePartlyPaidForCustomer on
         * date: 31/05/2012
         */
        $_SESSION['com_name_payment_history'] = NULL;
        $_SESSION['invoice_no_payment_history'] = NULL;
        $_SESSION['search_par_payment_history'] = NULL;
        /* End of unset below session varibale which have not been set Customer -> Payment History */

        $email_address = '';
        $cust_name = '';
        $com_name = '';
        $invoice_no = '';
        $search_par = '';
        $formAction = '';
        // Varun : Bug Fix FS#34870
        $this->class = '1';

        if ($request->hasParameter('getExpired')) {
            if ($request->getParameter('getExpired') == 'true') {
                $this->class = '0';
                $this->formAction = 'invoice/index?getExpired=true';
            } else {
                $this->formAction = 'invoice/index';
            }
        } else {
            $this->formAction = 'invoice/index';
        }

        if (array_key_exists('from', $parameters) && is_array($parameters['from']) && isset($parameters['from']['day']) && ($parameters['from']['day'] != '')) {
            if ($parameters['to']['day'] != '') {
                $to_date = date("Y-m-d", strtotime($parameters['to']['year'] . "-" . $parameters['to']['month'] . "-" . $parameters['to']['day']));
            }
            if ($parameters['from']['day'] != '') {
                $fromdate = date("Y-m-d", strtotime($parameters['from']['year'] . "-" . $parameters['from']['month'] . "-" . $parameters['from']['day']));
            }
            $dateCat = $parameters['date_category'];
        } elseif (array_key_exists('to', $parameters) && !is_array($parameters['to']) && $parameters['to'] != '') {
            $to_date = date("Y-m-d", strtotime($parameters['to']));

            $dateCat = $parameters['date_cat'];
            $fromdate = date("Y-m-d", strtotime($parameters['from']));
        }

        //Varun : Offset value to unset session
        $offset = $request->getParameter('offset');
        // Varun : Variables added and defined for search
//        if (!isset($_SESSION['email_address'])) {
//            $_SESSION['email_address'] = '';
//        }
//        if (!isset($_SESSION['cust_name'])) {
//            $_SESSION['cust_name'] = '';
//        }
//        if (!isset($_SESSION['com_name'])) {
//            $_SESSION['com_name'] = '';
//        }


        if (array_key_exists('email_address', $parameters) && $parameters['email_address'] != '') {
//            if ($request->getParameter('offset') > 0) {
//                $email_address = cryptString::decrypt($request->getParameter('email_address'));
//            } else {
            $email_address = $request->getParameter('email_address');
//            }
        }
        if (array_key_exists('cust_name', $parameters) && $parameters['cust_name'] != '') {
//            if ($request->getParameter('offset') > 0) {
//                $cust_name = cryptString::decrypt($parameters['cust_name']);
//            } else {
            $cust_name = $parameters['cust_name'];
//            }
        }

        if (array_key_exists('com_name', $parameters) && $parameters['com_name'] != '') {
//            if ($request->getParameter('offset') > 0) {
//                $com_name = cryptString::decrypt($parameters['com_name']);
//            } else {
            $com_name = $parameters['com_name'];
//            }
        }

        if (array_key_exists('invoice_no', $parameters) && $parameters['invoice_no'] != '') {
//            if ($request->getParameter('offset') > 0) {
//                $invoice_no = cryptString::decrypt($request->getParameter('invoice_no'));
//            } else {
            $invoice_no = $parameters['invoice_no'];
//            }
        }


        //creating session
//        if ($request->isMethod('post')) {
//
//            $_SESSION['email_address'] = $email_address;
//            $_SESSION['cust_name'] = $cust_name;
//            $_SESSION['com_name'] = $com_name;
//
//            $_SESSION['search_par'] = $search_par;
//        } else {
//
////Varun :Session will be unset for the first time page opens
//            if (!isset($offset)) {
//                $_SESSION['email_address'] = NULL;
//                $_SESSION['cust_name'] = NULL;
//                $_SESSION['com_name'] = NULL;
//                $_SESSION['invoice_no'] = NULL;
//                $_SESSION['search_par'] = NULL;
//            } else {
//                $email_address = (isset($_SESSION['email_address'])) ? $_SESSION['email_address'] : '';
//                $cust_name = (isset($_SESSION['cust_name'])) ? $_SESSION['cust_name'] : '';
//                $com_name = (isset($_SESSION['com_name'])) ? $_SESSION['com_name'] : '';
////                $invoice_no = (isset($_SESSION['invoice_no'])) ? $_SESSION['invoice_no'] : '';
//                $search_par = (isset($_SESSION['search_par'])) ? $_SESSION['search_par'] : '';
//            }
//        }

        $this->email_address = (isset($email_address)) ? $email_address : null;
        $this->cust_name = $request->hasParameter('cust_name') ? $parameters['cust_name'] : '';
        $this->com_name = (isset($com_name)) ? $com_name : null;
        $this->queryStringForStatusSearch = '';
        if (isset($this->com_name) && strlen($this->com_name) > 0) {
            $this->queryStringForStatusSearch.='com_name=' . $this->com_name . '&';
        }
        $this->invoice_no = (isset($invoice_no)) ? $invoice_no : null;
        if (isset($this->invoice_no) && strlen($this->invoice_no) > 0) {
            $this->queryStringForStatusSearch.='invoice_no=' . $this->invoice_no . '&';
        }

//        $this->search_par = (isset($search_par)) ? $search_par : null;
        $this->customer_id = $request->hasParameter('customer_id') ? $parameters['customer_id'] : '';

        $this->to_date = (isset($to_date)) ? $to_date : null;
        $this->from_date = (isset($fromdate)) ? $fromdate : null;
        $this->date_cat = (isset($dateCat)) ? $dateCat : null;
        $this->tellerLogin = false;
        if (pay4billHelper::checkIfMerchantUser($this->getUser()->getGuardUser()->getGroups()->toArray())) {
            $this->invoices = Doctrine::getTable('Invoice')->getInvoiceForMerchant($this->getUser()->getGuardUser()->getMerchantUser()->getId(), '', '', $email_address, $cust_name, $invoice_no, $this->from_date, $this->to_date, $this->date_cat, $this->customer_id);
            $this->merchnatLogin = true;
        } else {

            if (pay4billHelper::checkIfTellerUser($this->getUser()->getGuardUser()->getGroups()->toArray())) {

                $this->tellerLogin = true;
                // IN case on search, customer id from teller page could not lost

                $this->customerName = Doctrine::getTable('CustomerProfile')->find(cryptString::decrypt($this->getUser()->getAttribute('customerTellerId')))->getName();
                $this->invoices = Doctrine::getTable('Invoice')->getInvoiceForTeller(cryptString::decrypt($this->getUser()->getAttribute('customerTellerId')), null, null, $request->getParameter('getExpired'), $com_name, $invoice_no, $this->from_date, $this->to_date, $this->date_cat, $email_address);
                if ($request->hasParameter('getExpired')) {
                    $this->expired = $request->getParameter('getExpired');
                } else {
                    $this->expired = "false";
                }
            } else {
                // Customer User only
                $id = '';
                $varObjUserCustomer = $this->getUser()->getGuardUser()->getUserCustomerMapping();
                $totalRecord = count($varObjUserCustomer);
                if (count($varObjUserCustomer) > 0) {
                    foreach ($varObjUserCustomer as $k => $ob) {
                        $id .= $ob['customer_profile_id'] . '';
                        if ($k < ($totalRecord - 1)) {
                            $id .=',';
                        }
                    }
                }
                $allCustomerId = $id;

//
//                echo "<pre>";
//                print_r($this->customer_id);
//                exit;
//                $this->invoices = Doctrine::getTable('Invoice')->getInvoiceForCustomer($this->getUser()->getGuardUser()->getCustomerUser()->getId(), null, null, $request->getParameter('getExpired'), $com_name, $invoice_no, $this->from_date, $this->to_date, $this->date_cat, $email_address);
                $this->invoices = Doctrine::getTable('Invoice')->getInvoiceForCustomer($allCustomerId, null, null, $request->getParameter('getExpired'), $com_name, $invoice_no, $this->from_date, $this->to_date, $this->date_cat, $email_address, $this->customer_id);
                if ($request->hasParameter('getExpired')) {
                    $this->expired = $request->getParameter('getExpired');
                } else {
                    $this->expired = "false";
                }
                $this->tellerLogin = false;
            }
            $invoices_array = $this->invoices->toArray();

            $this->merchnatLogin = false;
        }
        $total_no_of_records = count($this->invoices);
        if ($total_no_of_records > sfConfig::get('app_records_per_page')) {
            $this->actionTobeCalled = $this->getActionName();
            $this->total_no_of_records = $total_no_of_records;
            $this->no_of_records_per_page = sfConfig::get('app_records_per_page');
            $this->setPagination = true;
            if (!$request->hasParameter('offset')) {
                $offset = 0;
            } else {

                $this->getUser()->getAttributeHolder()->remove('msg_invoice');
                $offset = $request->getParameter('offset');
            }
            $this->current_offset = $offset;
            if ($this->merchnatLogin) {
                $this->invoices = Doctrine::getTable('Invoice')->getInvoiceForMerchant($this->getUser()->getGuardUser()->getMerchantUser()->getId(), $offset, sfConfig::get('app_records_per_page'), $email_address, $cust_name, $invoice_no, $this->from_date, $this->to_date, $this->date_cat, $this->customer_id);
            } else {

                if (pay4billHelper::checkIfTellerUser($this->getUser()->getGuardUser()->getGroups()->toArray())) {
                    $this->tellerLogin = true;
                    // IN case on search, customer id from teller page could not lost

                    if (isset($customerTellerId) && !empty($customerTellerId)) {
                        $_SESSION['customerTellerId'] = $customerTellerId;
                    }

                    $this->invoices = Doctrine::getTable('Invoice')->getInvoiceForTeller(cryptString::decrypt($this->getUser()->getAttribute('customerTellerId')), $offset, sfConfig::get('app_records_per_page'), $request->getParameter('getExpired'), $com_name, $invoice_no, $this->from_date, $this->to_date, $this->date_cat, $email_address);
                } else {
//                    $this->invoices = Doctrine::getTable('Invoice')->getInvoiceForCustomer($this->getUser()->getGuardUser()->getCustomerUser()->getId(), $offset, sfConfig::get('app_records_per_page'), $this->expired, $com_name, $invoice_no);
                    $this->invoices = Doctrine::getTable('Invoice')->getInvoiceForCustomer($allCustomerId, $offset, sfConfig::get('app_records_per_page'), $this->expired, $com_name, $invoice_no, $this->from_date, $this->to_date, $this->date_cat, $email_address, $this->customer_id);
                }
// Varun : Bug Fix FS#34870
//                $this->class = '0';
            }
            $this->actionTobeCalled = $this->getActionName();
            $this->module = $this->getModuleName();
            $this->offset_last = (($total_no_of_records % sfConfig::get('app_records_per_page') == 0 ? ($total_no_of_records - sfConfig::get('app_records_per_page') ) : $total_no_of_records - ($total_no_of_records % sfConfig::get('app_records_per_page'))));

            $this->offset_next = (($offset + sfConfig::get('app_records_per_page')) > $this->offset_last ? $this->offset_last : ($offset + sfConfig::get('app_records_per_page')));

            $this->offset_prev = (($offset - sfConfig::get('app_records_per_page')) < 0 ? 0 : $offset - sfConfig::get('app_records_per_page'));
        } else {
            $this->setPagination = false;
        }
        $this->addClassTo = $this->getActionName();
        if ($this->getUser()->hasAttribute('msg_invoice')) {
            $this->msg = $this->getUser()->getAttribute('msg_invoice');
        }        
    }

    /**
     * @author:  Manu Datta
     * @param <type> $request
     * @purpose:    To upload customers
     */
    public function executeUpload(sfWebRequest $request) {
        ini_set('max_execution_time', '5000');
        ini_set('memory_limit', '512M');
        $this->valid = 0;
        $this->invalid = 0;
        $this->success = false;
        $this->titleXLS = false;
        $this->validFile = false;
        $this->invalidFile = false;
        $dateTime = date("dmY_H_i_s", time());
        $this->merchnatLogin = true;
        $this->getUser()->setAttribute('msg', '');
        $this->title = 'Bulk Invoice Uploader';
        $this->action = $this->getModuleName() . "/upload";
        $this->uploadButtonText = 'Upload Invoices';
        $this->addClassTo = $this->getActionName();

        // @Saurabh To get the name of the downloaded file i.e., cus_sample_csv.csv
        // self::executeDownloadSample($request); -- Function moved to pay4billhelper on 13 July 2012
        //$this->sample_link_file_name = pay4billHelper::executeDownloadInvoiceSample($this->getUser()->getGuardUser()->MerchantUser->getId());
        $data = Doctrine::getTable('MerchantProfile')->find($this->getUser()->getGuardUser()->MerchantUser->getId());
        if ($data->getPaymentType() == 'o') {
            $this->sample_link_file_name = 'invoice_sample_online.csv';
            $this->sample_link_file_name_xls = 'invoice_sample_online.xls';
        } else if ($data->getPaymentType() == 'b') {
            $this->sample_link_file_name = 'invoice_sample_biller.csv';
            $this->sample_link_file_name_xls = 'invoice_sample_biller.xls';
        }

        // @Saurabh 22 Nov 2012
        // Application have uploading on teller, customer, invoice for bulk upload
        // we are adding below session to get the upload type when uploading data 
        $this->getUser()->setAttribute('upload_on', 'invoice');
        //end

        // self::executeDownloadSample($request);
        //$this->sample_link_file_name = $this->fileName;
        // End of function

        if ($request->isMethod('POST')) {
            try {
                $uploadTemplate = new uploadTemplate();
                $upload_file = $request->getFiles('file');
                // @Saurabh on 27 July 2012
                // Check if the merchant is online or biller
                $merchant_id = $this->getUser()->getGuardUser()->getMerchantUser()->getId();
                $data = Doctrine::getTable('MerchantProfile')->find($merchant_id);
                $paymentType = $data->getPaymentType();
                if ($paymentType == 'o') {
                    $uploadTemplate->upload($upload_file, 'CustomerId,IssueDate,DeliveryDate,DueDate,AdditionalInformation,CurrencyType,PartPaymentPercentage,RecurrentBilling,ItemTax,ItemQuantity1,ItemDescription1,ItemUnitPrice1,ItemQuantity2,ItemDescription2,ItemUnitPrice2,ItemQuantity3,ItemDescription3,ItemUnitPrice3,ItemQuantity4,ItemDescription4,ItemUnitPrice4');
                } else if ($paymentType == 'b') {
                    $uploadTemplate->upload($upload_file, 'CustomerId,IssueDate,DeliveryDate,DueDate,AdditionalInformation,CurrencyType,ItemTax,ItemQuantity1,ItemDescription1,ItemUnitPrice1,ItemQuantity2,ItemDescription2,ItemUnitPrice2,ItemQuantity3,ItemDescription3,ItemUnitPrice3,ItemQuantity4,ItemDescription4,ItemUnitPrice4');
                }

//                $uploadTemplate->upload($upload_file, 'CustomerId,IssueDate,DeliveryDate,DueDate,AdditionalInformation,CurrencyType,PartPaymentPercentage,RecurrentBilling,ItemTax,ItemQuantity1,ItemDescription1,ItemUnitPrice1,ItemQuantity2,ItemDescription2,ItemUnitPrice2,ItemQuantity3,ItemDescription3,ItemUnitPrice3,ItemQuantity4,ItemDescription4,ItemUnitPrice4');

                $content_arr_list = $uploadTemplate->process();
                //////////////////////Validate CSV///////////////////////////////////////////////////////////////////////////////
                $host = $request->getUriPrefix();
                $url_root = $request->getPathInfoPrefix();
                $complete_url = $host . $url_root;

                /*
                 * @Saurabh on 27 July 2012
                 * Creating a new array which will not have 'PartPaymentPercentage,RecurrentBilling' for biller
                 * Using the below Foreach loop we are corrected the index (index [6],[7] will be removed from the new array)
                 * Leaving the rest of the attribute at their place with the same index number as it is in case of Online.
                 * Do, after removing the 'PartPaymentPercentage,RecurrentBilling', new index would be 0 1 2 3 4 5 8 9 and so on
                 */
                if ($paymentType == 'b') {
                    foreach ($content_arr_list as $m => $val) {
                        foreach ($val as $k => $v) {
                            if ($k >= 6)
                                $k = $k + 2;
                            $content_biller[$m][$k] = $v;
                        }
                    }
                    $content_arr_list = $content_biller;
                }

                $validate = $this->validate($content_arr_list, $complete_url, $paymentType);
                //////////////////////Creating Valid and Invlid files CSV////////////////////////////////////////////////////////
                if ($validate['validRows'] == '0' && $validate['invalidRows'] == '0') {
                    $this->msg = 'CSV/XLS is Empty.';
                    $this->error = true;
                } else {
                    $createdFiles = $uploadTemplate->writeFiles($validate['validArray'], $validate['invalidArray'], sfConfig::get('sf_upload_dir') . '/assets/');
                    $this->validFile = $createdFiles[0];
                    $this->invalidFile = $createdFiles[1];
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    $this->success = true;
                    if ($validate['validRows'] > 0 || $validate['invalidRows'] > 0) {
                        $this->valid = $validate['validRows'];
                        $this->invalid = $validate['invalidRows'];
                    }
                }
            } catch (Exception $e) {
                $this->getUser()->setFlash('notice', $e->getMessage(), false);
            }
        }
    }

    /*
     * @Saurabh
     * Function to add fields for download
     */

    public function executeDownloadSample(sfWebRequest $request) {

        $merchant_id = $this->getUser()->getGuardUser()->MerchantUser->getId();
        $arrayHeadings1 = Doctrine::getTable('AttributeMaster')->getAttributeNameByObjectId($merchant_id, 2); // 2 : Invoice
        $fileName = '';

        $this->fileName = 'invoice_sample_csv_' . cryptString::encrypt($merchant_id) . '.csv';

        $arrayHeadings2 = array('CustomerEmailAddress', 'IssueDate', 'DeliveryDate', 'DueDate', 'AdditionalInformation', 'CurrencyType', 'PartPaymentPercentage', 'RecurrentBilling', 'ItemTax', 'ItemQuantity1', 'ItemDescription1', 'ItemUnitPrice1', 'ItemQuantity2', 'ItemDescription2', 'ItemUnitPrice2', 'ItemQuantity3', 'ItemDescription3', 'ItemUnitPrice3', 'ItemQuantity4', 'ItemDescription4', 'ItemUnitPrice4');
        $validate['arrayHeading'][0] = array_merge($arrayHeadings2, $arrayHeadings1);

        pay4billHelper::createCsv(sfConfig::get('sf_upload_dir') . '/assets/' . $this->fileName, $validate['arrayHeading']);
        return $this->fileName;
    }

    /*
     * Aditi
     * Check if Additional Information is Alphanumeric
     *
     */

    private function IsAlphaNumeric($text) {

        preg_match("/^[a-zA-Z\s\.0-9-,]+$/", $text, $matches);
        if (!count($matches)) {

            return false;
        } else {
            return true;
        }
    }

    /**
     * @author:  Manu Datta
     * @param <type> $content_arr_list
     * @param <type> $complete_url
     * @purpose:    To validate and save uploaded invoices
     */
    public function validate($content_arr_list, $complete_url, $paymentType='') {
        $upload_validator = new upload_validator();
        $merchant_id = $this->getUser()->getGuardUser()->MerchantUser->getId();
        foreach ($content_arr_list as $key => $content_arr_new) {
            if ($key != 0) {
                if (trim(strlen($content_arr_new[0]) == '')) {
                    $upload_validator->invalid_record($content_arr_new, 'Customer Id is Required');
                } else if ($content_arr_new[1] == '') {
                    $upload_validator->invalid_record($content_arr_new, 'Issue Date is Required');
                } else if (is_string($this->chkDate($content_arr_new[1], "Issue Date"))) {
                    $error = $this->chkDate($content_arr_new[1], "Issue Date");
                    $upload_validator->invalid_record($content_arr_new, $error);
                } else if (is_string($this->chkDate($content_arr_new[2], "Delivery Date"))) {
                    $error = $this->chkDate($content_arr_new[2], "Delivery Date");
                    $upload_validator->invalid_record($content_arr_new, $error);
                } else if (is_string($this->chkDate($content_arr_new[3], "Due Date"))) {
                    $error = $this->chkDate($content_arr_new[3], "Due Date");
                    $upload_validator->invalid_record($content_arr_new, $error);
                } else if (strlen($content_arr_new[4]) == 0) {
                    $upload_validator->invalid_record($content_arr_new, 'Additional Information is Required');
                } else if ($content_arr_new[4] != '' && !$this->IsAlphaNumeric($content_arr_new[4])) {
                    $upload_validator->invalid_record($content_arr_new, 'Only Alphanumeric Additional Information Accepted');
                } else if (strlen($content_arr_new[5]) == '') {
                    $upload_validator->invalid_record($content_arr_new, 'Currency Type is Required');
                } else if ($this->chkCurrency($content_arr_new[5])) {
                    $upload_validator->invalid_record($content_arr_new, 'Invalid Currency Type (Only Naira Accepted)');
                } else if (isset($content_arr_new[6]) && !$this->chkPartPaymentPercentage($content_arr_new[6]) && $content_arr_new[6] != '' && $paymentType == 'o') {
                    $upload_validator->invalid_record($content_arr_new, 'Invalid Part Payment Percentage(Max 99.99)');
                } else if (isset($content_arr_new[7]) && is_string($this->chkRecurrentBilling($content_arr_new[7])) && !is_array($this->chkRecurrentBilling($content_arr_new[7])) && $content_arr_new[7] != '' && $paymentType == 'o') {
                    $error = $this->chkRecurrentBilling($content_arr_new[7]);
                    $upload_validator->invalid_record($content_arr_new, $error);
                }
                //varun : new columns added , validation added for new columns
                else if (is_string($this->chkTaxName($content_arr_new[8]))) {
                    $error = $this->chkTaxName($content_arr_new[8]);
                    $upload_validator->invalid_record($content_arr_new, $error);
                } //varun : bug fix :FS#35007
                else if (!$this->chkItemTaxFromDb($content_arr_new[8]) && $content_arr_new[8] != '') {
                    $upload_validator->invalid_record($content_arr_new, 'Please enter valid Tax Name');
                } else if (strlen($content_arr_new[9]) == '') {
                    $upload_validator->invalid_record($content_arr_new, 'Item Quantity is Required');
                } else if (is_string($this->chkItemQuantity($content_arr_new[9]))) {
                    $error = $this->chkItemQuantity($content_arr_new[9]);
                    $upload_validator->invalid_record($content_arr_new, $error);
                } else if (strlen($content_arr_new[10]) == '') {
                    $upload_validator->invalid_record($content_arr_new, 'Item Description is Required');
                } else if (is_string($this->chkItemDescription($content_arr_new[10]))) {
                    $error = $this->chkItemDescription($content_arr_new[10]);
                    $upload_validator->invalid_record($content_arr_new, $error);
                } else if (strlen($content_arr_new[11]) == '') {
                    $upload_validator->invalid_record($content_arr_new, 'Item Unit Price is Required');
                } else if (is_string($this->chkItemUnitPrice($content_arr_new[11]))) {
                    $error = $this->chkItemUnitPrice($content_arr_new[11]);
                    $upload_validator->invalid_record($content_arr_new, $error);
                } else if (is_string($this->chkItemQuantity($content_arr_new[12]))) {
                    $error = $this->chkItemQuantity($content_arr_new[12]);
                    $upload_validator->invalid_record($content_arr_new, $error);
                } else if (is_string($this->chkItemDescription($content_arr_new[13]))) {
                    $error = $this->chkItemDescription($content_arr_new[13]);
                    $upload_validator->invalid_record($content_arr_new, $error);
                } else if (is_string($this->chkItemUnitPrice($content_arr_new[14]))) {
                    $error = $this->chkItemUnitPrice($content_arr_new[14]);
                    $upload_validator->invalid_record($content_arr_new, $error);
                } else if (is_string($this->chkItemQuantity($content_arr_new[15]))) {
                    $error = $this->chkItemQuantity($content_arr_new[15]);
                    $upload_validator->invalid_record($content_arr_new, $error);
                } else if (is_string($this->chkItemDescription($content_arr_new[16]))) {
                    $error = $this->chkItemDescription($content_arr_new[16]);
                    $upload_validator->invalid_record($content_arr_new, $error);
                } else if (is_string($this->chkItemUnitPrice($content_arr_new[17]))) {
                    $error = $this->chkItemUnitPrice($content_arr_new[17]);
                    $upload_validator->invalid_record($content_arr_new, $error);
                } else if (is_string($this->chkItemQuantity($content_arr_new[18]))) {
                    $error = $this->chkItemQuantity($content_arr_new[18]);
                    $upload_validator->invalid_record($content_arr_new, $error);
                } else if (is_string($this->chkItemDescription($content_arr_new[19]))) {
                    $error = $this->chkItemDescription($content_arr_new[19]);
                    $upload_validator->invalid_record($content_arr_new, $error);
                } else if (is_string($this->chkItemUnitPrice($content_arr_new[20]))) {
                    $error = $this->chkItemUnitPrice($content_arr_new[20]);
                    $upload_validator->invalid_record($content_arr_new, $error);
                } else {
                    //Manu :Check valid customer
                    $if_valid_customer = Doctrine::getTable('CustomerProfile')->isValidCustomerForMerchant($merchant_id, $content_arr_new[0]);
                    $customer_array = $if_valid_customer->toArray();
                    if (count($if_valid_customer->toArray()) == 1) {

                        $is_part_payment_applicable = false;
                        $amountDue = 0;
                        $subject = "Regular Invoice";
                        $partial_name = 'emailContent';
                        $next_invoice = null;
                        //Manu :Get Merchant Details
                        $merchantInfo = Doctrine::getTable('MerchantProfile')->find($merchant_id, Doctrine::HYDRATE_ARRAY);
                        $email = $content_arr_new[0];
                        $customer_name = $customer_array[0]['name'];
                        $company_name = $merchantInfo['merchant_name'];
                        $tiny_url = $merchantInfo['tiny_url'];
                        //Manu :Invoice generating
                        $invoiceNo = pay4billHelper::getInvoiceNo($merchant_id, $merchantInfo['merchant_name']);
                        $ob = new Invoice();
                        $ob->setMerchantId($merchant_id);
                        $ob->setCustomerId($customer_array[0]['id']);

                        $ob->setInvoiceNo($invoiceNo);
                        $ob->setIssueDate($content_arr_new[1]);
                        $ob->setDeliveryDate($content_arr_new[2]);
                        $ob->setDueDate($content_arr_new[3]);
                        $ob->setCurrencyType($content_arr_new[5]);
                        $ob->setAdditionalInformation($content_arr_new[4]);
                        //Manu :Part Payment Percentage check
                        if (isset($content_arr_new[6]) && $content_arr_new[6] != '' && $paymentType == 'o') {
                            $ob->setPartPayment($content_arr_new[6]);
                            $is_part_payment_applicable = true;
                        }
                        $amountDue = null;
                        //Varun : Bug fix : FS#35007
                        //Item tax is gethered from the Tax name provided in invoice upload field , if not found then set to null
                        $itemTax = null;
                        if (isset($content_arr_new[8]) && $content_arr_new[8] != '') {
                            $itemTaxDetails = Doctrine::getTable('Tax')->findByTaxName($content_arr_new[8]);
                            $tax_id = $itemTaxDetails[0]['id'];
                            $merchantId = $this->getUser()->getGuardUser()->getId();
                            $itemTaxInfo = Doctrine::getTable('MerchantTax')->findEntryForMerchantTax($tax_id, $merchantId);
                            if (isset($itemTaxInfo[0]['tax_value']) && $itemTaxInfo[0]['tax_value'] != '') {
                                $itemTax = $itemTaxInfo[0]['tax_value'];
                            } else {
                                $itemTax = null;
                            }
                            $ob->setTaxId($tax_id);
                        }

                        //Item 1 Validation
                        if (isset($content_arr_new[9]) && $content_arr_new[9] != '' && isset($content_arr_new[10]) && $content_arr_new[10] != '' && isset($content_arr_new[11]) & $content_arr_new[11] != '') {
                            $invoiceItemDetail = Doctrine::getTable('InvoiceItemDetails')->saveDetails($ob, $itemTax, $content_arr_new[9], $content_arr_new[10], $content_arr_new[11], $amountDue);
                        }

                        $amountDue = $invoiceItemDetail;

                        //Item 2 Validation
                        if (isset($content_arr_new[12]) && $content_arr_new[12] != '' && isset($content_arr_new[13]) && $content_arr_new[13] != '' && isset($content_arr_new[14]) & $content_arr_new[14] != '') {
                            $invoiceItemDetail = Doctrine::getTable('InvoiceItemDetails')->saveDetails($ob, $itemTax, $content_arr_new[12], $content_arr_new[13], $content_arr_new[14], $amountDue);
                        }
                        $amountDue = $invoiceItemDetail;

                        //Item 3 Validation
                        if (isset($content_arr_new[15]) && $content_arr_new[15] != '' && isset($content_arr_new[16]) && $content_arr_new[16] != '' && isset($content_arr_new[17]) & $content_arr_new[17] != '') {
                            $invoiceItemDetail = Doctrine::getTable('InvoiceItemDetails')->saveDetails($ob, $itemTax, $content_arr_new[15], $content_arr_new[16], $content_arr_new[17], $amountDue);
                        }
                        $amountDue = $invoiceItemDetail;


                        //Item 4 Validation
                        if (isset($content_arr_new[18]) && $content_arr_new[18] != '' && isset($content_arr_new[19]) && $content_arr_new[19] != '' && isset($content_arr_new[20]) & $content_arr_new[20] != '') {
                            $invoiceItemDetail = Doctrine::getTable('InvoiceItemDetails')->saveDetails($ob, $itemTax, $content_arr_new[18], $content_arr_new[19], $content_arr_new[20], $amountDue);
                        }

                        // Final Amount due
                        $amountDue = $invoiceItemDetail;
                        $ob->setTotalAmount($amountDue);

                        //Manu :Invoice Item Details checking
//                            $result = $this->chkInvoiceItemDetails($content_arr_new[4]);
//                            if (is_array($result)) {
//                                foreach ($result as $eachDetail) {
//                                    $details_ob = new InvoiceItemDetails();
//                                    $details_ob->setInvoice($ob);
//                                    $details_ob->setNoOfItems($eachDetail['no']);
//                                    $details_ob->setItemDescription($eachDetail['desc']);
//                                    $details_ob->setItemPrice($eachDetail['price']);
//                                    $details_ob->setItemTax($eachDetail['tax']);
//                                    $details_ob->setSubTotal(round($details_ob->getItemPrice() * $details_ob->getNoOfItems(), 2));
//                                    $details_ob->setTaxTotal(round(($details_ob->getSubTotal() * $details_ob->getItemTax() / 100), 2));
//                                    $amountDue +=$details_ob->getSubTotal() + $details_ob->getTaxTotal();
//                                    $details_ob->save();//Manu :Save Item Details
//                                }
//                            }
                        if ($is_part_payment_applicable == true) {
                            $ob->setAmountDue($ob->getPartPayment() * $amountDue / 100);
                        } else {
                            $ob->setAmountDue($amountDue);
                        }
                        if ($this->check_Issue_Delivery_Due_date_validation($ob)) {
                            $error = $this->check_Issue_Delivery_Due_date_validation($ob);
                            $upload_validator->invalid_record($content_arr_new, $error);
                            $ob->setAmountPaid(0);
                        } else {
                            $ob->save(); //Manu :Save Invoice
                            //Manu :In case of Recurrent Billing
                            if (isset($content_arr_new[7]) && $content_arr_new[7] != '' && $paymentType == 'o') {
                                $result = $this->chkRecurrentBilling($content_arr_new[7]);
                                if (is_array($result)) {
                                    $recurring_obj = new RecurrentBilling();
                                    $recurring_obj->setInvoice($ob);
                                    $recurring_obj->setType($result['plan']);

                                    $recurring_obj->setStatus('live');

                                    switch ($recurring_obj->getType()) {

                                        case 'weekly':
                                            $nextInvoice = date("d-M-Y", strtotime($ob->getDueDate() . "+1 week"));
                                            break;
                                        case 'monthly':
                                            $nextInvoice = date("d-M-Y", strtotime($ob->getDueDate() . "+1 month"));
                                            break;
                                        case 'yearly':
                                            $nextInvoice = date("d-M-Y", strtotime($ob->getDueDate() . "+1 year"));
                                            break;
                                        case 'quarterly':
                                            $nextInvoice = date("d-M-Y", strtotime($ob->getDueDate() . "+3 months"));
                                            break;
                                        case 'custom':
                                            $recurring_obj->setValue($result['value']);
                                            $nextInvoice = date("d-M-Y", strtotime($ob->getDueDate() . "+" . $result['value'] . " days"));
                                            break;
                                    }
                                    $recurring_obj->save(); //Manu :Save Recurrent Billing
                                    $next_invoice = $nextInvoice;
                                    $partial_name = 'recurringInvoice';
//                                    $subject = "Re : " . $company_name . " Invoice Reminder";
                                    $subject = "Pay4bill-Recurring Bill: ". $company_name ." invoice due on ".str_replace("/","-",$content_arr_new[3]);
                                }
                            }
                            $company_url = $ob->Merchant->getCompanyUrl();
                            $upload_validator->valid_record($content_arr_new);
                            if (!is_null($ob->getCustomer()->getEmailAddress())) {
                                sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));

//                                $taskId = EpjobsContext::getInstance()->addJob('InvoiceCreatedByBulkUpload', $this->moduleName . "/sendEmail", array('email' => $ob->getCustomer()->getEmailAddress(), 'company_url' => $company_url, 'customer_name' => $customer_name, 'next_invoice' => $next_invoice, 'company_name' => $company_name, 'subject' => $subject, 'invoiceNo' => $invoiceNo, 'deliveryDate' => str_replace("/","-",$content_arr_new[2]), 'due_date' => str_replace("/","-",$content_arr_new[3]), 'invoice_ob_id' => $ob->getId(), 'additional_information' => $content_arr_new[4], 'partial_name' => $partial_name,'image_tag'=>public_path(sfConfig::get('app_logo_path'),true), 'complete_url' => $complete_url . "/custMgt/login/" . $tiny_url,'issue_date' => str_replace("/","-",$content_arr_new[1])), '-1', 'frontend');
                                $taskId = EpjobsContext::getInstance()->addJob('InvoiceCreatedByBulkUpload', $this->moduleName . "/sendEmail", array('email' => $ob->getCustomer()->getEmailAddress(), 'company_url' => $company_url, 'customer_name' => $customer_name, 'next_invoice' => $next_invoice, 'company_name' => $company_name, 'subject' => $subject, 'invoiceNo' => $invoiceNo, 'deliveryDate' => str_replace("/","-",$content_arr_new[2]), 'due_date' => str_replace("/","-",$content_arr_new[3]), 'invoice_ob_id' => $ob->getId(), 'additional_information' => $content_arr_new[4], 'partial_name' => $partial_name,'image_tag'=>public_path(sfConfig::get('app_logo_path'),true), 'complete_url' => $complete_url,'issueDate' => str_replace("/","-",$content_arr_new[1])), '-1', 'frontend');
                                $this->logMessage("scheduled mail job with id: $taskId", 'debug');
                            }
                        }
                    } else {
                        $upload_validator->invalid_record($content_arr_new, 'Invalid Customer Id');
                    }
                }
            }
        }//End Of Foreech
        return $upload_validator->getValues();
    }

    /*
     * Varun : Invoice upload via CSV-new column added
     * 20 Apr 2012
     */

    private function chkItemTax($value) {
        $info = '';
        $pattern5 = "/^[1-9]+([0-9])*(?:\.[0-9]{1,3})?$/";
        $pattern6 = "/^(0){1}(\.){1}([1-9]){1,3}$/";

        preg_match($pattern5, $value, $matches5);
        preg_match($pattern6, $value, $matches6);
        if (count($matches6) == 0 && count($matches5) == 0) {
            $array_values ['tax'] = 0;
            //Manu : Tax is optional not mendatory
//                                        $info.= "<li>Invalid Tax % for Details(" . ($main_key + 1) . ") String at Row:" . $row_no . "</li>";
        } else {
            if (floatval($value) > 99.99) {
                $info.= "Invalid Tax%(Max 99.99)"; //Varun : Bug FIx : FS#34918
            } else {
                $array_values['tax'] = $value;
            }
        }
        if ($info == '') {
            return false;
        } else {
            return $info;
        }
    }

    /*
     * Varun : Invoice upload via CSV-new column added
     * 20 Apr 2012
     */

    private function chkItemQuantity($value) {

        $pattern1 = "/^([1-9]){1}([0-9]){0,2}$/";
        $info = '';
        if (isset($value) && $value != '') {
            preg_match($pattern1, $value, $matches1);
            //Varun : Bug FIx : FS#34918
            if (count($matches1) == 0) {
                $info.= "Invalid Item Number(Max 999)";
            } else {
                if (intval($value) > 999) {
                    $info.= "Item Number(Max 999)"; //Varun : Bug FIx : FS#34918
                } else {
                    $array_values['no'] = $value;
                }
            }
        }

        if ($info == '') {
            return false;
        } else {
            return $info;
        }
    }

    /*
     * Varun : Invoice upload via CSV-new column added
     * 20 Apr 2012
     */

    private function chkItemDescription($value) {
        $pattern2 = "/^.+$/";
        $info = '';
        if (isset($value) && $value != '') {
            preg_match($pattern2, $value, $matches2);
            //Varun : Bug fix : FS#34912
            if (count($matches2) == 0) {
                $info.= "Invalid Item Description "; //Varun : Bug FIx : FS#34918
            } else {
                if (strlen($value) > 50) {
                    $info.= "Item Description cannot be more than 50 characters ";
                } else {
                    $array_values['desc'] = $value;
                }
            }
        }
        if ($info == '') {
            return false;
        } else {
            return $info;
        }
    }

    /*
     * Varun : Invoice upload via CSV tax name checked
     * 30 Apr 2012
     */

    private function chkTaxName($value) {
        $pattern2 = "/^.+$/";
        $info = '';
        if (isset($value) && $value != '') {
            preg_match($pattern2, $value, $matches2);
            //Varun : Bug fix : FS#34912
            if (count($matches2) == 0) {
                $info.= "Invalid Tax Name"; //Varun : Bug FIx : FS#34918
            } else {
                if (strlen($value) > 40) {
                    $info.= "Tax Name cannot be more than 40 characters ";
                } else if (strlen($value) < 5) {
                    $info.= "Tax Name cannot be less than 5 characters ";
                } else {
                    $array_values['desc'] = $value;
                }
            }
        }
        if ($info == '') {
            return false;
        } else {
            return $info;
        }
    }

    /*
     * Varun : Invoice upload via CSV-new column added
     * 20 Apr 2012
     */

    private function chkItemUnitPrice($value) {
        $pattern3 = "/^[1-9]+([0-9])*(?:\.[0-9]{1,3})?$/";
        $pattern4 = "/^(0){1}(\.){1}([1-9]){1,3}$/";
        $info = '';
        if (isset($value) && $value != '') {
            preg_match($pattern4, $value, $matches4);
            preg_match($pattern3, $value, $matches3);
            if (count($matches3) == 0 && count($matches4) == 0) {
                $info.= "Invalid Item Price ";
            } else {
                //varun : Bug Fix : FS#34915
                if (floatval($value) > 99999999) {
                    $info.= "Invalid Item Price(Max 99999999) "; //Varun : Bug FIx : FS#34918
                } else if (floatval($value) <= 0) { //Bug : FS#34913
                    $info.= "Invalid Item Price should be grater than 0 "; //Varun : Bug FIx : FS#34918
                } else {
                    $array_values['price'] = $value;
                }
            }
        }
        if ($info == '') {
            return false;
        } else {
            return $info;
        }
    }

    /**
     * @author:  Manu Datta
     * @param <type> $value
     * @purpose:    To check Invoice Item Details valid or not
     */
    private function chkInvoiceItemDetails($value) {

        $no_of_item_details = explode(";", substr(ltrim(rtrim($value)), 0, strlen(ltrim(rtrim($value)))));
        if (count($no_of_item_details) < 6) {
            $main_array_to_return = array();
            $info = "";
            foreach ($no_of_item_details as $main_key => $items_string) {

                if (count($items_string) != 0) {
                    $individual_values = explode("|", substr($items_string, 1, strlen($items_string) - 2));

                    if (count($individual_values) == 4 && substr(ltrim(rtrim($value)), 0, 1) == "{" && substr(ltrim(rtrim($value)), strlen(ltrim(rtrim($value))) - 1, 1) == "}") {

                        foreach ($individual_values as $key => $string) {

                            switch ($key) {
                                case 0:
                                    $pattern1 = "/^([1-9]){1}([0-9]){0,2}$/";

                                    preg_match($pattern1, $string, $matches1);
                                    if (count($matches1) == 0) {
                                        $info.= "Invalid Item Number(Max 999) for Details(" . ($main_key + 1) . ") String";
                                    } else {
                                        if (intval($string) > 999) {
                                            $info.= "Item Number(Max 999) for Details(" . ($main_key + 1) . ") String";
                                        } else {
                                            $array_values['no'] = $string;
                                        }
                                    }

                                    break;
                                case 1:
                                    $pattern2 = "/^.+$/";

                                    preg_match($pattern2, $string, $matches2);
                                    if (count($matches2) == 0) {
                                        $info.= "Invalid Item Description for Details(" . ($main_key + 1) . ") String";
                                    } else {
                                        $array_values['desc'] = $string;
                                    }
                                    break;

                                case 2:
                                    $pattern3 = "/^[1-9]+([0-9])*(?:\.[0-9]{1,3})?$/";
                                    $pattern4 = "/^(0){1}(\.){1}([1-9]){1,3}$/";

                                    preg_match($pattern4, $string, $matches4);
                                    preg_match($pattern3, $string, $matches3);
                                    if (count($matches3) == 0 && count($matches4) == 0) {
                                        $info.= "Invalid Item Price  for Details(" . ($main_key + 1) . ") String";
                                    } else {
                                        if (floatval($string) > 99999999) {
                                            $info.= "Invalid Item Price(Max 99999999) for Details(" . ($main_key + 1) . ") String";
                                        } else {
                                            $array_values['price'] = $string;
                                        }
                                    }
                                    break;
                                case 3:
                                    $pattern5 = "/^[1-9]+([0-9])*(?:\.[0-9]{1,3})?$/";
                                    $pattern6 = "/^(0){1}(\.){1}([1-9]){1,3}$/";

                                    preg_match($pattern5, $string, $matches5);
                                    preg_match($pattern6, $string, $matches6);
                                    if (count($matches6) == 0 && count($matches5) == 0) {
                                        $info.= "Invalid Tax % for Details(" . ($main_key + 1) . ") String";
                                    } else {
                                        if (floatval($string) > 99.99) {
                                            $info.= "Invalid Tax%(Max 99.99) for Details(" . ($main_key + 1) . ") String";
                                        } else {
                                            $array_values['tax'] = $string;
                                        }
                                        break;
                                    }
                            }
                        }
                    } else {
                        $info = "Incomplete/Malformed Item Details";
                    }
                    if ($info == '') {
                        $main_array_to_return[] = $array_values;
                        unset($array_values);
                    }
                }
            }
        } else {
            $info = "Maximum 5 Item Details String Accepted. Check Row:";
        }
        if ($info == '') {
            return $main_array_to_return;
        } else {
            return $info;
        }
    }

    /**
     * @author:  Manu Datta
     * @param <type> $ob
     * @purpose:    To check Issue Delivery Due date validation valid or not
     */
    private function check_Issue_Delivery_Due_date_validation($ob) {

        $issueDate = strtotime($ob->getIssueDate());
        $deliveryDate = strtotime($ob->getDeliveryDate());
        $due_date = strtotime($ob->getDueDate());
        $checkFurther = true;
        $info = "";
        if (is_null($issueDate) && empty($info)) {
            $info = "Issue Date Should be less than" . date("Y-m-d", strtotime($ob->getIssueDate() . "+10 years"));
            $checkFurther = false;
        }
        if (is_null($deliveryDate) && empty($info)) {
            $checkFurther = false;
            $info = "Delivery Date Should be less than" . date("Y-m-d", strtotime($ob->getIssueDate() . "+10 years"));
        }
        if (is_null($due_date) && empty($info)) {
            $checkFurther = false;
            $info = "Due Date Should be less than" . date("Y-m-d", strtotime($ob->getIssueDate() . "+10 years"));
        }
        if ($checkFurther) {

            if (($issueDate > $deliveryDate) && empty($info)) {
                $info = "Delivery  Date Should be greater than or equal to Issue Date ";
            }
            if (($deliveryDate > $due_date) && empty($info)) {
                $info = "Due  Date Should be greater than or equal to Delivery Date";
            }
        }

        if ($info == '') {
            return false;
        } else {
            return $info;
        }
    }

    /**
     * @author:  Manu Datta
     * @param <type> $value
     * @purpose:    To check Part Payment Percentage no valid or not
     */
    private function chkPartPaymentPercentage($value) {
        preg_match("/^[.0-9]{1,}([0-9])*(?:\.[0-9]{1,3})?$/", $value, $matches);
        if (count($matches) && floatval($value) < 100) {
            return true;
        } else {
            preg_match("/^[.0-9]{1,}([0-9])*(?:\.[0-9]{1,3})?$/", $value, $matches);
            if (count($matches) && floatval($value) < 100) {
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * @author:  Varun Chandwani : FS#35007
     * @param <type> $value
     * @purpose:    To check Tax value from database valid or not
     */
    private function chkItemTaxFromDb($value) {
        $merchant_id = $this->getUser()->getGuardUser()->getId();
        $getMerchantTaxDetails = Doctrine::getTable('tax')->findTaxDetailsOfMerchant($merchant_id);
        $i = 1;
        $array = array();
        if (!empty($getMerchantTaxDetails)) {
            foreach ($getMerchantTaxDetails as $val) {
                $tax_value = explode('- ', $val);
                $i++;
                $array[] = trim($tax_value[1]);
            }
        }

        if (in_array($value, $array)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @author:  Manu Datta
     * @param <type> $value
     * @purpose:    To check chkCurrency valid or not
     */
    private function chkCurrency($value) {
        if (strtolower($value) == 'naira') {
            return false;
        } else {
            return true;
        }
    }

    /**
     * @author:  Manu Datta
     * @param <type> $value
     * @param <type> $str
     * @purpose:    To check date valid or not
     */
    private function chkDate($value, $str) {
        if ($this->is_date($value)) {
            if (strtotime($value) < strtotime(date('Y-m-d'))) {
                $return = $str . " Can't be Past Date ";
            } else {
                $return = true;
            }
        } else {
            $return = "Invalid " . $str;
        }
        return $return;
    }

    /**
     * @author:  Manu Datta
     * @param <type> $str
     * @purpose:    To check date formate
     */
    private function is_date($str) {

        $stamp = strtotime(ltrim(rtrim($str)));

        if (!is_numeric($stamp)) {
            return false;
        }
        $month = date('m', $stamp);
        $day = date('d', $stamp);
        $year = date('Y', $stamp);

        if (checkdate($month, $day, $year)) {
            return $this->checkDateFormat($str);
        }

        return false;
    }

    /*
     * @Saurabh on 30July2012
     * Check the date format (yyyy-mm-dd)
     */

    public function checkDateFormat($date) {
        //match the format of the date
        if (preg_match("/^([0-9]{4})\/([0-9]{2})\/([0-9]{2})$/", $date)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @author:  Manu Datta
     * @param <type> $value
     * @purpose:    To check Email no valid or not
     */
    private function chkEmail($value) {
        preg_match("/^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i", $value, $matches);
        if (count($matches)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @author:  Manu Datta
     * @param <type> $value
     * @purpose:    To check chkRecurrentBilling valid or not
     */
    private function chkRecurrentBilling($value) {
        $info = "";
        if (substr(ltrim(rtrim($value)), 0, 1) == "{" && substr(ltrim(rtrim($value)), strlen(ltrim(rtrim($value))) - 1, 1) == "}") {
            $individual_values = explode("|", substr(ltrim(rtrim($value)), 1, strlen(ltrim(rtrim($value))) - 2));

            if (count($individual_values) == 2 && $individual_values[0] == 'custom') {
                $pattern1 = "/^([1-9]){1,}[0-9]{0,2}$/";

                preg_match($pattern1, $individual_values[1], $matches1);
                if (count($matches1) == 0) {
                    $info.= "Invalid No of Days for Recurring Plan (Max 999)";
                } else {
                    if (intval($individual_values[1]) > 999) {
                        $info.= "Invalid No of Days for Recurring Plan (Max 999)";
                    } else {
                        $array_values['plan'] = $individual_values[0];
                        $array_values['value'] = $individual_values[1];
                    }
                }
            } else {


                switch ($individual_values[0]) {
                    case "weekly":

                        $array_values['plan'] = $individual_values[0];
                        $valid = true;
                        break;
                    case "yearly":
                        $array_values['plan'] = $individual_values[0];
                        break;
                    case "quarterly":
                        $array_values['plan'] = $individual_values[0];
                        break;
                    case "monthly":
                        $array_values['plan'] = $individual_values[0];
                        break;
                    default:
                        $info = "Invalid Recurring Plan";
                        break;
                }
            }
            if ($info == '') {
                return $array_values;
            } else {
                return $info;
            }
        } else {
            return "Incomplete/Malformed Recurring Plan Details";
        }
    }

    /**
     * @author:  Manu Datta
     * @param <type> $value
     * @purpose:    To check Phone no valid or not
     */
    private function chkPhoneNumber($value) {
        preg_match("/^[0-9]*$/", $value, $matches);
        if (count($matches) && strlen($value) == 10) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @author:  Manu Datta
     * @param <type> $request
     * @purpose:    To downloads
     */
    public function executeDownloads(sfWebRequest $request) {
        $title = cryptString::decrypt($request->getParameter('file'));
        if (pay4billHelper::downloads($title)) {
            $this->setLayout(false);
            return sfView::NONE;
        } else {
            $this->getUser()->setFlash('notice', "Download Template Does Not Exist In Directory.", false);
            $this->forward('invoice', 'upload');
        }
    }

    public function executeShow(sfWebRequest $request) {

        if (isset($_SESSION['logedin_user_group']) && $_SESSION['logedin_user_group'] == 'merchant') {
            $this->merchnatLogin = true;
            $this->adminLogin = false;
            $this->customerLogin = false; // Varun : Customer login variable added
            $this->highlighted_menu = 'Report';
        } else if (in_array('administrator', $this->getUser()->getGuardUser()->getGroupNames()) != false || in_array('report', $this->getUser()->getGuardUser()->getGroupNames()) != false) {

            $this->adminLogin = true;
            $this->customerLogin = false;
            $this->merchnatLogin = false;
            $this->highlighted_menu = 'Report';
        } else if (isset($_SESSION['logedin_user_group']) && $_SESSION['logedin_user_group'] == 'customer') {
            $this->customerLogin = true;
            $this->merchnatLogin = false;
            $this->adminLogin = false;
        } else {
            $this->customerLogin = false;
            $this->adminLogin = false;
            $this->merchnatLogin = false;
        }
        $this->getUser()->setAttribute('msg_invoice', '');
        $allParams = ($request->getParameterHolder()->getAll());

        if (!$this->merchnatLogin && !$this->adminLogin) {
            if (array_key_exists('report', $allParams)) {
                $this->highlighted_menu = 'PaymentHistory';
//            echo "coming here";
            } else {
                $this->highlighted_menu = 'Invoice';
            }
        }
        $this->hidePaybutton = $request->hasParameter('hidePaybutton') ? $request->getParameter('hidePaybutton') : 0;
        //Varun : Decrypt id
        // Addded condition to run in jasper
        // so that we integer is coming in url as an id, it should not decrypt the string 
        // otherwise decrypt
        if(ctype_digit($request->getParameter('id')) && $request->getParameter('id') > 0){
            $this->invoice = Doctrine::getTable('Invoice')->find(array($request->getParameter('id')));
        } else {
            $this->invoice = Doctrine::getTable('Invoice')->find(array(cryptString::decrypt($request->getParameter('id'))));
        }
        
//      $this->invoice = Doctrine::getTable('Invoice')->find(array(cryptString::decrypt($request->getParameter('id'))));
        $merchant_object = Doctrine::getTable('MerchantProfile')->find($this->invoice->getMerchantId());
        $this->paymenttype = $merchant_object->getPaymentType();

        $this->recurring_plan = Doctrine::getTable('RecurrentBilling')->findBy('invoice_id', $this->invoice->getId())->getFirst();

        $childInvOb = Doctrine::getTable('Invoice')->findBy('parent_id_if_recurring', $this->invoice->getId())->getLast();

//        if ($childInvOb) {
//            $this->items_details = Doctrine::getTable('InvoiceItemDetails')->findBy('invoice_id', $childInvOb->getId())->toArray();
//        } else {
        
        // Addded condition to run in jasper
        // so that we integer is coming it should not decrypt the string otherwise decrypt
        if(ctype_digit($request->getParameter('id')) && $request->getParameter('id') > 0){
            $this->items_details = Doctrine::getTable('InvoiceItemDetails')->findBy('invoice_id', array($request->getParameter('id')))->toArray();
        } else {
            $this->items_details = Doctrine::getTable('InvoiceItemDetails')->findBy('invoice_id', array(cryptString::decrypt($request->getParameter('id'))))->toArray();
        }

        $subtotals = array();
        $taxes = 0;
        $subtotal = 0;
        foreach ($this->items_details as $listofItems) {
            $subtotal+=$listofItems['subTotal'];
            $taxes+=$listofItems['tax_total'];
        }
        $this->subtotal = $subtotal;
        $this->taxes = $taxes;
        $this->total = $this->subtotal + $this->taxes;

        $this->item_number = Doctrine::getTable('BillPaymentRequest')->findBy('invoice_id', $this->invoice->id)->getLast()->getId();

        $this->forward404Unless($this->invoice);
    }

    public function executeShowPaid(sfWebRequest $request) {

        $allParams = ($request->getParameterHolder()->getAll());

        $this->invoice = Doctrine::getTable('Invoice')->find(array($request->getParameter('id')));
        $this->recurring_plan = Doctrine::getTable('RecurrentBilling')->findBy('invoice_id', $this->invoice->getId())->getFirst();

        $childInvOb = Doctrine::getTable('Invoice')->findBy('parent_id_if_recurring', $this->invoice->getId())->getLast();
        if ($childInvOb) {
            $this->items_details = Doctrine::getTable('InvoiceItemDetails')->findBy('invoice_id', $childInvOb->getId())->toArray();
        } else {
            $this->items_details = Doctrine::getTable('InvoiceItemDetails')->findBy('invoice_id', array($request->getParameter('id')))->toArray();
        }
        $subtotals = array();
        $taxes = 0;
        $subtotal = 0;
        foreach ($this->items_details as $listofItems) {
            $subtotal+=$listofItems['subTotal'];
            $taxes+=$listofItems['tax_total'];
        }
        $this->subtotal = $subtotal;
        $this->taxes = $taxes;
        $this->total = $this->subtotal + $this->taxes;

        $this->item_number = Doctrine::getTable('BillPaymentRequest')->findBy('invoice_id', $this->invoice->id)->getLast()->getId();

        $this->forward404Unless($this->invoice);
    }

    /**
     * @author:  Manu Datta
     * @param <type> $request
     * @purpose:    To Print Invoice
     */
    public function executePrint(sfWebRequest $request) {
//        if (in_array('merchant', $this->getUser()->getGuardUser()->getGroupNames()) != false) {
        if (isset($_SESSION['logedin_user_group']) && $_SESSION['logedin_user_group'] == 'merchant') {
            $this->merchnatLogin = true;
            $this->adminLogin = false;
            $this->highlighted_menu = 'Report';
        } else if (in_array('administrator', $this->getUser()->getGuardUser()->getGroupNames()) != false) {

            $this->adminLogin = true;
            $this->merchnatLogin = false;
            $this->highlighted_menu = 'Report';
        } else {
            $this->adminLogin = false;
            $this->merchnatLogin = false;
        }
        $this->getUser()->setAttribute('msg_invoice', '');
        //getting all request parameters
        $allParams = ($request->getParameterHolder()->getAll());

        if (!$this->merchnatLogin && !$this->adminLogin) {
            if (array_key_exists('report', $allParams)) {
                $this->highlighted_menu = 'PaymentHistory';
            } else {
                $this->highlighted_menu = 'Invoice';
            }
        }
        //getting invoice data by Id
        $this->invoice = Doctrine::getTable('Invoice')->find(array($request->getParameter('id')));
        $this->recurring_plan = Doctrine::getTable('RecurrentBilling')->findBy('invoice_id', $this->invoice->getId())->getFirst();
        //getting invoice data by recurring id
        $childInvOb = Doctrine::getTable('Invoice')->findBy('parent_id_if_recurring', $this->invoice->getId())->getLast();
        if ($childInvOb) {
            $this->items_details = Doctrine::getTable('InvoiceItemDetails')->findBy('invoice_id', $childInvOb->getId())->toArray();
        } else {
            $this->items_details = Doctrine::getTable('InvoiceItemDetails')->findBy('invoice_id', array($request->getParameter('id')))->toArray();
        }
        $subtotals = array();
        $taxes = 0;
        $subtotal = 0;
        foreach ($this->items_details as $listofItems) {
            $subtotal+=$listofItems['subTotal'];
            $taxes+=$listofItems['tax_total'];
        }
        $this->subtotal = $subtotal;
        $this->taxes = $taxes;
        $this->total = $this->subtotal + $this->taxes;
        //getting item number
        $this->item_number = Doctrine::getTable('BillPaymentRequest')->findBy('invoice_id', $this->invoice->id)->getLast()->getId();

        $this->setTemplate('print');
        $this->setLayout('layout_for_print');
    }

    /**
     * @author:  Varun Chandwani
     * @param <type> $request
     * @purpose:    To Print Paid / Partially Paid Invoice
     */
    public function executePaidPrint(sfWebRequest $request) {
        $this->getUser()->setAttribute('msg_invoice', '');
        //getting all request parameters
        $allParams = ($request->getParameterHolder()->getAll());


        //getting invoice data by Id
        $this->invoice = Doctrine::getTable('Invoice')->find(array($request->getParameter('id')));
        $this->recurring_plan = Doctrine::getTable('RecurrentBilling')->findBy('invoice_id', $this->invoice->getId())->getFirst();
        //getting invoice data by recurring id
        $childInvOb = Doctrine::getTable('Invoice')->findBy('parent_id_if_recurring', $this->invoice->getId())->getLast();
        if ($childInvOb) {
            $this->items_details = Doctrine::getTable('InvoiceItemDetails')->findBy('invoice_id', $childInvOb->getId())->toArray();
        } else {
            $this->items_details = Doctrine::getTable('InvoiceItemDetails')->findBy('invoice_id', array($request->getParameter('id')))->toArray();
        }
        $subtotals = array();
        $taxes = 0;
        $subtotal = 0;
        foreach ($this->items_details as $listofItems) {
            $subtotal+=$listofItems['subTotal'];
            $taxes+=$listofItems['tax_total'];
        }
        $this->subtotal = $subtotal;
        $this->taxes = $taxes;
        $this->total = $this->subtotal + $this->taxes;
        //getting item number
        $this->item_number = Doctrine::getTable('BillPaymentRequest')->findBy('invoice_id', $this->invoice->id)->getLast()->getId();

        $this->setTemplate('paidPrint');
        $this->setLayout('layout_for_print');
    }

    private function getLatestChildObj_if_any($invoice) {
//        foreach ($invoices as $invoice_record) {

        if (is_null($invoice_record->getParentIdIfRecurring())) {

            $childInvOb = Doctrine::getTable('Invoice')->findBy('parent_id_if_recurring', $invoice_record->getId())->getLast();
            if ($childInvOb) {

                $id_Items_invoiceId[$invoice_record->getId()] = ($childInvOb->getId());
            } else {
                $id_Items_invoiceId[$invoice_record->getId()] = ($invoice_record->getId());
            }
        } else {

            $id_Items_invoiceId[$invoice_record->getParentIdIfRecurring()] = ($invoice_record->getId());
            $id_Items_invoiceId[$invoice_record->getId()] = ($invoice_record->getId());
        }

        return $id_Items_invoiceId;
    }

    public function executeToggleInvoiceStatus(sfWebRequest $request) {
        $this->getUser()->setAttribute('msg_invoice', '');
        //Varun : Id decrypted
        $invoice = Doctrine::getTable('Invoice')->find(array(cryptString::decrypt($request->getParameter('id'))));
        if ($invoice->getStatus() == 0) {
            $invoice->setStatus(1);
        } else {
            $invoice->setStatus(0);
        }
        $invoice->save();
        $this->redirect('invoice/index');
    }

    public function executeNew(sfWebRequest $request) {
        // added for teller invoice creation
        if ($_SESSION['logedin_user_group'] == 'teller') { // added by santosh
            $this->tellerLogin = true;
            $this->merchnatLogin = false;
            // get customer id
            $cust = cryptString::decrypt($request->getParameter('cust'));
            // get merchant id through customer id
            $arrCustomerInfo = Doctrine::getTable('CustomerProfile')->getCustomerMerchantInfoByCustomerId($cust);
            $this->merchant_id = $PKMerchantProfile = $arrCustomerInfo[0]['merchant_profile_id'];
            $this->merchant_user_id = Doctrine::getTable('MerchantProfile')->find($PKMerchantProfile)->getMerchantId();
            $this->customer_id = $cust;
            $this->customerTellerId = $customerTellerId = cryptString::encrypt($cust);
            if (isset($customerTellerId) && !empty($customerTellerId)) {
                $_SESSION['customerTellerId'] = $customerTellerId;
            }

            // get merchant info by mercahnt id
            $arrMerchantInfo = Doctrine::getTable('MerchantProfile')->find(array($this->merchant_id));
        } else {
            $this->merchant_user_id = $this->getUser()->getGuardUser()->getId();
            $this->merchant_id = $PKMerchantProfile = $this->getUser()->getGuardUser()->getMerchantUser()->getId();
            //$uid = Doctrine::getTable('MerchantProfile')->findOneByMerchantId($this->merchant_id);
            //$PKMerchantProfile = $uid['id'];
            $this->tellerLogin = false;
            $this->merchnatLogin = true;
        }
        // Varun : Id parameter check in case of re-generated invoice
        $id = cryptString::decrypt($request->getParameter('id'));
        $this->id = $request->getParameter('id');
        $this->getMerchantTaxDetails = Doctrine::getTable('tax')->findTaxDetailsOfMerchant($this->merchant_user_id);
        $this->getUser()->setAttribute('msg', '');
        if ($id != '' && isset($id)) {
            //Varun : Recurrent details
            $this->invoiceArray = Doctrine::getTable('Invoice')->find(array($id));
            $this->recurring_plan = Doctrine::getTable('RecurrentBilling')->findBy('invoice_id', $this->invoiceArray->getId())->getFirst();
            /* below code written for regenrate invoce recuring and part payment section handling
             * by Santosh Kumar
             *  on 02 May 2012
             */
            $this->recurring = '';
            if ($request->getParameter('recurring') != '')
                $this->recurring = cryptString::decrypt($request->getParameter('recurring'));
            $this->part_payment = '';
            if ($request->getParameter('part_payment') != '')
                $this->part_payment = cryptString::decrypt($request->getParameter('part_payment'));
            // end here
            $this->getTaxDetail = Doctrine::getTable('InvoiceItemDetails')->findBy('invoice_id', $id);
            $this->invoice = Doctrine::getTable('Invoice')->find($id);
            $this->invoiceDetails = Doctrine::getTable('InvoiceItemDetails')->getByInvoiceId($id);
            $this->form = new InvoiceForm($this->invoice);
        } else {
            if ($_SESSION['logedin_user_group'] == 'teller') { // added by santosh
                $this->form = new InvoiceForm(array(), array('merchant_name' => $arrMerchantInfo->getMerchantName(), 'merchant_id' => $this->merchant_id, 'customer_id' => $this->customer_id));
            } else {
                $this->form = new InvoiceForm();
            }
        }
        //Aditi-To check whether to show Recurring Billing and Part Payment Options while creating new Invoice
        $helper = new pay4billHelper();
        $result = $helper->checkMerchantPaymentType($PKMerchantProfile);

        switch ($result) {
            case 'o':
                $this->showRecurringPartPaymentInputs = true;
                break;
            case 'b':
                $this->showRecurringPartPaymentInputs = false;
                break;
        }

        $this->addClassTo = $this->getActionName();
    }

    public function executeCreate(sfWebRequest $request) {

        $this->forward404Unless($request->isMethod(sfRequest::POST));

        // added for teller invoice creation
        if ($_SESSION['logedin_user_group'] == 'teller') { // added by santosh
            $arrPostParam = $request->getPostParameters();
            $this->merchant_id = $merchantProfileId = $arrPostParam['invoice']['merchant_id'];
            $this->merchant_user_id = Doctrine::getTable('MerchantProfile')->find($this->merchant_id)->getMerchantId();
            $this->customer_id = isset($arrPostParam['invoice']['customer_id']) ? $arrPostParam['invoice']['customer_id'] : '';
            if (empty($this->customer_id)) {
                $this->getUser()->setFlash('notice', 'Customer has been deactivated or deleted.');
                $this->redirect('custMgt/listingForTeller');
            }
            // get merchant info by mercahnt id
            $arrMerchantInfo = Doctrine::getTable('merchantProfile')->find(array($this->merchant_id));
            $this->tellerLogin = true;
            $this->merchnatLogin = false;
            $this->customerTellerId = $customerTellerId = cryptString::encrypt($this->customer_id);
            if (isset($customerTellerId) && !empty($customerTellerId)) {
                $_SESSION['customerTellerId'] = $customerTellerId;
            }
        } else {
            // Varun : Find details for merchant tax list for item tax feild
            $this->merchant_user_id = $this->merchant_id = $this->getUser()->getGuardUser()->getId();
            $merchantProfileId = $this->getUser()->getGuardUser()->getMerchantUser()->getId();
            $this->tellerLogin = false;
            $this->merchnatLogin = true;
        }
        $this->getMerchantTaxDetails = Doctrine::getTable('tax')->findTaxDetailsOfMerchant($this->merchant_user_id);
        if ($_SESSION['logedin_user_group'] == 'teller') { // added by santosh
            $this->form = new InvoiceForm(array(), array('merchant_name' => $arrMerchantInfo->getMerchantName(), 'merchant_id' => $this->merchant_id, 'customer_id' => $this->customer_id));
        } else {
            $this->form = new InvoiceForm();
        }
//        echo "<pre>";print_r($_POST);echo "</pre>";

        $mytotal = $request->getParameter('mytotal');
        $mysubtotal = $request->getParameter('mysubtotal');

        $item_no1 = $request->getParameter('item_no1');
        $item_desc1 = $request->getParameter('item_desc1');
        $item_price1 = $request->getParameter('item_price1');

        $item_no2 = $request->getParameter('item_no2');
        $item_desc2 = $request->getParameter('item_desc2');
        $item_price2 = $request->getParameter('item_price2');

        $item_no3 = $request->getParameter('item_no3');
        $item_desc3 = $request->getParameter('item_desc3');
        $item_price3 = $request->getParameter('item_price3');

        $item_tax0 = $request->getParameter('item_tax0');

        //Setting up an array to pass to the form partial
        $this->arr = array("mytotal" => $mytotal, "mysubtotal" => $mysubtotal, "item_no1" => $item_no1, "item_desc1" => $item_desc1, "item_price1" => $item_price1, "item_no2" => $item_no2, "item_desc2" => $item_desc2, "item_price2" => $item_price2, "item_no3" => $item_no3, "item_desc3" => $item_desc3, "item_price3" => $item_price3, "item_tax0" => $item_tax0);

        $this->processForm($request, $this->form);
//Aditi-To check whether to show Recurring Billing and Part Payment Options while creating new Invoice

        $helper = new pay4billHelper();
        $result = $helper->checkMerchantPaymentType($merchantProfileId);

        switch ($result) {
            case 'o':
                $this->showRecurringPartPaymentInputs = true;
                break;
            case 'b':
                $this->showRecurringPartPaymentInputs = false;
                break;
        }


        $this->setTemplate('new');
    }

    public function executeEdit(sfWebRequest $request) {
        $this->forward404Unless($invoice = Doctrine::getTable('Invoice')->find(array($request->getParameter('id'))), sprintf('Object invoice does not exist (%s).', $request->getParameter('id')));
        $this->form = new InvoiceForm($invoice);
    }

    public function executeUpdate(sfWebRequest $request) {
        // Varun : invoice tabel status set to "1" for regenerated field
        $id = cryptString::decrypt($request->getParameter('id'));
        $this->id = $request->getParameter('id');
        // $updateRegenerated = Doctrine::getTable('Invoice')->updateRegenerated($id);
        $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
        $this->forward404Unless($invoice = Doctrine::getTable('Invoice')->find(array($id)), sprintf('Object invoice does not exist (%s).', $id));
        //Variable added
        $this->merchant_id = $this->getUser()->getGuardUser()->getId();
        $this->getMerchantTaxDetails = Doctrine::getTable('tax')->findTaxDetailsOfMerchant($this->merchant_id);
        $this->invoiceArray = Doctrine::getTable('Invoice')->find(array($id));
        $this->recurring_plan = Doctrine::getTable('RecurrentBilling')->findBy('invoice_id', $this->invoiceArray->getId())->getFirst();
        $this->recurring = '';
        if ($request->getParameter('recurring') != '')
            $this->recurring = $request->getParameter('recurring');
        $this->part_payment = '';
        if ($request->getParameter('part_payment_percent') != '')
            $this->part_payment = $request->getParameter('part_payment_percent');
        $this->merchant_id = $this->getUser()->getGuardUser()->getId();
        $this->getTaxDetail = Doctrine::getTable('InvoiceItemDetails')->findBy('invoice_id', $id);
        $this->invoice = Doctrine::getTable('Invoice')->find($id);
        $this->invoiceDetails = Doctrine::getTable('InvoiceItemDetails')->getByInvoiceId($id);
        $this->form = new InvoiceForm();

        $this->processForm($request, $this->form);

        //Aditi-To check whether to show Recurring Billing and Part Payment Options while creating new Invoice

        $helper = new pay4billHelper();
        $result = $helper->checkMerchantPaymentType($this->getUser()->getGuardUser()->getMerchantUser()->getId());

        switch ($result) {
            case 'o':
                $this->showRecurringPartPaymentInputs = true;
                break;
            case 'b':
                $this->showRecurringPartPaymentInputs = false;
                break;
        }
        $this->setTemplate('edit');
    }

    public function executeDelete(sfWebRequest $request) {
        $request->checkCSRFProtection();

        $this->forward404Unless($invoice = Doctrine::getTable('Invoice')->find(array($request->getParameter('id'))), sprintf('Object invoice does not exist (%s).', $request->getParameter('id')));
        $invoice->delete();

        $this->redirect('invoice/index');
    }

    protected function processForm(sfWebRequest $request, sfForm $form) {
        $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));

        if ($form->isValid()) {
            $allParams = $request->getParameterHolder()->getAll();

            /* @Saurabh on 25 July
             * Check if invoice already exist with the same invoice number
             * set Regenerated to 1
             */
            $alldata = Doctrine::getTable('Invoice')->getLastInvoice($allParams['invoice']['invoice_no']);
            if (isset($alldata[0]['id']) && !empty($alldata[0]['id'])) {
                $updateRegenerated = Doctrine::getTable('Invoice')->updateRegenerated($alldata[0]['id']);
            }

            $invoice = $form->save();
            if (isset($allParams['item_tax0']) && $allParams['item_tax0'] != '') {
                // Item tax calculated for change in tax
                $tax_id_detail = explode('/', $allParams['item_tax0']);
                $tax_id = $tax_id_detail[0];
                $statusCheck = Doctrine::getTable('tax')->findTax($tax_id);
                if ($statusCheck[0]['status'] == '1') {
                    $merchant_id = $invoice->getMerchantId();
                    $merchant_user_id = $invoice->getMerchant()->getMerchantId();
                    $taxExist = Doctrine::getTable('MerchantTax')->findEntryForMerchantTax($tax_id, $merchant_user_id);
                    $item_tax = $taxExist[0]['tax_value'];
                } else {
                    $item_tax = '0.00';
                }
            } else {
                $tax_id = null;
                $item_tax = '0.00';
            }
            $invoice->setPartPayment($allParams['part_payment_percent']);
            $invoice->setTaxId($tax_id);
            $invoice->save();
            //Santosh :- Add the 2 below lines for fixing the production issue of duplicate invoice number
            if ($allParams['invoice']['invoice_no'] == '') {
                $arrMerchantInfo = Doctrine::getTable('MerchantProfile')->find($invoice->getMerchantId());
                $invoiceNo = pay4billHelper::getInvoiceNo($invoice->getMerchantId(), $arrMerchantInfo->getMerchantName());
                $invoice->setInvoiceNo($invoiceNo);
                $invoice->save();
            }
            $pattern = '/item_no/';
            preg_match_all($pattern, implode(",", array_keys($allParams)), $matches, PREG_OFFSET_CAPTURE);


            $row_index_capture = array();
            foreach ($matches[0] as $matchNo => $matchDetails) {


                array_push($row_index_capture, substr(implode(",", array_keys($allParams)), $matchDetails[1] + strlen($pattern) - 2, 1));
            }
            $pattern = '/item_/';
            preg_match_all($pattern, implode(",", array_keys($allParams)), $matches);
            $amountDue = 0;
            foreach ($row_index_capture as $index => $available_row_index) {
                $details_ob = new InvoiceItemDetails();
                $details_ob->setInvoiceId($invoice->id);
                $details_ob->setNoOfItems($allParams['item_no' . $available_row_index]);
                $details_ob->setItemDescription($allParams['item_desc' . $available_row_index]);
                $details_ob->setItemPrice($allParams['item_price' . $available_row_index]);
                //remove item tax
                $details_ob->setItemTax($item_tax);
                $details_ob->setSubTotal(round($details_ob->getItemPrice() * $details_ob->getNoOfItems(), 2));
                $details_ob->setTaxTotal(round(($details_ob->getSubTotal() * $details_ob->getItemTax() / 100), 2));
                $amountDue +=$details_ob->getSubTotal() + $details_ob->getTaxTotal();
                $details_ob->save();
            }
            $invoice->setTotalAmount($amountDue);
            if (array_key_exists('part_payment', $allParams)) {
                $invoice->setAmountDue($allParams['part_payment_percent'] * $amountDue / 100);
//                $invoice->setAmountPaid(0);
            } else {
//                echo "coming here";
                $invoice->setAmountDue($amountDue);
            }
            $invoice->save();
            $partial_name = 'emailContent';
            $subject = "Regular Invoice";
            $nextInvoice = null;
            if (array_key_exists('recurring', $allParams) && $allParams['recurring'] == 'yes') {
                if ($allParams['recurring'] == 'yes' && isset($allParams['recurring_plan'])) {
                    $recurringOb = new RecurrentBilling();
                    $recurringOb->setInvoiceId($invoice->id);
                    $recurringOb->setType($allParams['recurring_plan']);
                    $recurringOb->setValue($allParams['custom_recurring_bill']);
                    $recurringOb->setStatus('live');
                    $recurringOb->save();

                    switch ($recurringOb->getType()) {

                        case 'weekly':
                            $nextInvoice = date("d-M-Y", strtotime($invoice->getDueDate() . "+1 week"));
                            break;
                        case 'monthly':
                            $nextInvoice = date("d-M-Y", strtotime($invoice->getDueDate() . "+1 month"));
                            break;
                        case 'yearly':
                            $nextInvoice = date("d-M-Y", strtotime($invoice->getDueDate() . "+1 year"));
                            break;
                        case 'quarterly':
                            $nextInvoice = date("d-M-Y", strtotime($invoice->getDueDate() . "+3 months"));
                            break;
                        case 'custom':
                            $nextInvoice = date("d-M-Y", strtotime($invoice->getDueDate() . "+" . $allParams['custom_recurring_bill'] . " days"));
                            break;
                    }
                    $partial_name = 'recurringInvoice';
//                    $partial_name = 'emailContent';
//                    $subject = "Re : " . $invoice->Merchant->getMerchantName() . " Invoice Reminder";
                    $subject = "Pay4bill-Recurring Bill: ". $invoice->Merchant->getMerchantName() ." invoice due on ".$invoice->getDueDate();
                }
            } else {
                $partial_name = 'emailContent';
                $subject = "Regular Invoice";
                $nextInvoice = null;
            }
            if (!is_null($invoice->Customer->getEmailAddress())) {
                $host = $request->getUriPrefix();
                $url_root = $request->getPathInfoPrefix();
                $complete_url = $host . $url_root;
                sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
                $taskId = EpjobsContext::getInstance()->addJob('InvoiceCreated', $this->moduleName . "/sendEmail", array('email' => $invoice->Customer->getEmailAddress(), 'customer_name' => $invoice->Customer->getName(), 'next_invoice' => $nextInvoice, 'company_url' => $invoice->Merchant->getCompanyUrl(), 'company_name' => $invoice->Merchant->getMerchantName(), 'subject' => $subject, 'invoiceNo' => $invoice->getInvoiceNo(), 'deliveryDate' => $invoice->getDeliveryDate(), 'due_date' => $invoice->getDueDate(), 'invoice_ob_id' => $invoice->getId(), 'additional_information' => $invoice->getAdditionalInformation(), 'partial_name' => $partial_name, 'complete_url' => $complete_url,'image_tag'=>public_path(sfConfig::get('app_logo_path'),true), 'issueDate' => $invoice->getIssueDate()), '-1', 'frontend');
                $this->logMessage("scheduled mail job with id: $taskId", 'debug');
            }
//            $this->getUser()->setAttribute('msg_invoice', 'Invoice Created');
            $this->getUser()->setFlash('notice', 'Invoice created successfully');
            if (pay4billHelper::checkIfTellerUser($this->getUser()->getGuardUser()->getGroups()->toArray())) {

                $this->redirect('invoice/index?id=' . cryptString::encrypt($allParams['invoice']['customer_id']));
            } else {

                $this->redirect('invoice/index');
            }
        } else {
            /*
             * @Saurabh on 30 July
             * In case if the user deleted while creating his invoice from invoice page. Bug#35634
             * If customer is deleted during the invoice genration a validation message reflect the deletion of 
             * customer should be displayed and application should be navigate to listing page.
             */
//            foreach ($this->form->getFormFieldSchema() as $name => $formField) {
//              if ($formField->getName() =='customer_id' && strlen($formField->getError())>0) {}
//            }

            if ($form->getFormFieldSchema()->offsetGet('customer_id')->hasError()) {
                $this->getUser()->setFlash('notice', 'Customer has been deactivated or deleted.');
                if (pay4billHelper::checkIfTellerUser($this->getUser()->getGuardUser()->getGroups()->toArray()))
                    $this->redirect('custMgt/listingForTeller');
                else if (pay4billHelper::checkIfMerchantUser($this->getUser()->getGuardUser()->getGroups()->toArray()))
                    $this->redirect('invoice/index');
            }
        }
    }

    public function executeSendEmail(sfWebRequest $request) {

        $email = $request->getParameter('email');
        $customerName = $request->getParameter('customer_name');
        $additionalInfo = $request->getParameter('additional_information');
        $invoiceNo = $request->getParameter('invoiceNo');
        $deliveryDate = $request->getParameter('deliveryDate');
        $invoice_id = $request->getParameter('invoice_ob_id');
        $completeUrl = $request->getParameter('complete_url');
        $companyName = $request->getParameter('company_name');
        $issueDate = $request->getParameter('issueDate');
        $dueDate = $request->getParameter('due_date');
        $subject = $request->getParameter('subject');
        $next_invoice = $request->getParameter('next_invoice');
        $company_url = $request->getParameter('company_url');
        $partialName = $request->getParameter('partial_name');

        /* Mail firing code starts */
        $mailTo = $email;
        $mailToGroup = array();

        $mailFrom = sfConfig::get('app_sender');
        $signature = sfConfig::get('app_sender');

        $partialVars = array('path'=>  $request->getParameter('image_tag'),'company_url' => $company_url, 'next_invoice' => $next_invoice, 'dueDate' => $dueDate, 'companyName' => $companyName, 'customerName' => $customerName, 'invoiceNo' => $invoiceNo, 'completeUrl' => $completeUrl, 'invoice_id' => $invoice_id, 'deliveryDate' => $deliveryDate, 'additionalInfo' => $additionalInfo, 'issueDate'=>$issueDate);
        $mailingOptions = array('mailFrom' => $mailFrom, 'mailTo' => $mailTo, 'mailSubject' => $subject);

        $mailObj = new Mailing();
        $mailInfo = $mailObj->sendMail($partialName, $partialVars, $mailingOptions);

        return $this->renderText($mailInfo);
    }

    /**
     * @author:  Manu Datta
     * @param <type> $request
     * @purpose:    To Add Cron
     */
    public function executeAddCron(sfWebRequest $request) {
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
        EpjobsContext::getInstance()->addJobForEveryDay('SendDailyReminderEmail', 'invoice/sendReminderDaily', "2020-3-12 12:12:12", array('username' => 'appAdmin','image_tag'=>public_path(sfConfig::get('app_logo_path'),true)), null, null, null, sfConfig::get('app_cron_setting_daily_minute'), sfConfig::get('app_cron_setting_daily_hour'));
        EpjobsContext::getInstance()->addRepetableJob('SendWeeklyReminderEmail', 'invoice/sendReminderWeekly', "2020-3-12 12:12:12", array('username' => 'appAdmin','image_tag'=>public_path(sfConfig::get('app_logo_path'),true)), null, null, null, sfConfig::get('app_cron_setting_weekly_minute'), sfConfig::get('app_cron_setting_weekly_hour'), null, "4");
        EpjobsContext::getInstance()->addJobForEveryMonth('SendMonthlyReminderEmail', 'invoice/sendReminderDaily', "2020-3-12 12:12:12", array('username' => 'appAdmin','image_tag'=>public_path(sfConfig::get('app_logo_path'),true)), null, null, null, sfConfig::get('app_cron_setting_monthly_minute'), sfConfig::get('app_cron_setting_monthly_hour'), 1);
        $this->setLayout(NULL);
    }

    /**
     * @author:  Manu Datta
     * @param <type> $request
     * @purpose:    To Send Reminder Monthly
     */
    public function executeSendReminderMonthly(sfWebRequest $request) {
        $getInvoices = Doctrine::getTable('Invoice')->getInvoices('monthly');
        foreach ($getInvoices as $eachRecordArray) {
            $host = $request->getUriPrefix();
            $url_root = $request->getPathInfoPrefix();
            $complete_url = $host . $url_root;
            $taskId = EpjobsContext::getInstance()->addJob('SendMailNotification', $this->moduleName . "/sendEmailReminder", array('email' => $eachRecordArray['email_address'], 'customer_name' => $eachRecordArray['customer_name'], 'company_name' => $eachRecordArray['company_name'], 'invoice_no' => $eachRecordArray['invoice_no'], 'issue_date' => $eachRecordArray['issue_date'], 'due_date' => $eachRecordArray['due_date'], 'complete_url' => $complete_url), '-1', 'frontend');
            $this->logMessage("scheduled mail job with id: $taskId", 'debug');
        }
        $this->renderText('Send Reminder Succesfully');
    }

    /**
     * @author:  Manu Datta
     * @param <type> $request
     * @purpose:    To Send Reminder Weekly
     */
    public function executeSendReminderWeekly(sfWebRequest $request) {
        $getInvoices = Doctrine::getTable('Invoice')->getInvoices('weekly');
        foreach ($getInvoices as $eachRecordArray) {
            $host = $request->getUriPrefix();
            $url_root = $request->getPathInfoPrefix();
            $complete_url = $host . $url_root;
            $taskId = EpjobsContext::getInstance()->addJob('SendMailNotification', $this->moduleName . "/sendEmailReminder", array('email' => $eachRecordArray['email_address'], 'customer_name' => $eachRecordArray['customer_name'], 'company_name' => $eachRecordArray['company_name'], 'invoice_no' => $eachRecordArray['invoice_no'], 'issue_date' => $eachRecordArray['issue_date'], 'due_date' => $eachRecordArray['due_date'], 'complete_url' => $complete_url . "/custMgt/login/" . $eachRecordArray['tiny_url']), '-1', 'frontend');
            $this->logMessage("scheduled mail job with id: $taskId", 'debug');
        }
        $this->renderText('Send Reminder Succesfully');
    }

    /**
     * @author:  Manu Datta
     * @param <type> $request
     * @purpose:    To Send Reminder Daily
     */
    public function executeSendReminderDaily(sfWebRequest $request) {
        $getInvoices = Doctrine::getTable('Invoice')->getInvoices('daily');
        foreach ($getInvoices as $eachRecordArray) {
            $host = $request->getUriPrefix();
            $url_root = $request->getPathInfoPrefix();
            $complete_url = $host . $url_root;
            $taskId = EpjobsContext::getInstance()->addJob('SendMailNotification', $this->moduleName . "/sendEmailReminder", array('email' => $eachRecordArray['email_address'], 'customer_name' => $eachRecordArray['customer_name'], 'company_name' => $eachRecordArray['company_name'], 'invoice_no' => $eachRecordArray['invoice_no'], 'issue_date' => $eachRecordArray['issue_date'], 'due_date' => $eachRecordArray['due_date'], 'complete_url' => $complete_url . "/custMgt/login/" . $eachRecordArray['tiny_url']), '-1', 'frontend');
            $this->logMessage("scheduled mail job with id: $taskId", 'debug');
        }
        $this->renderText('Send Reminder Succesfully');
    }

    /**
     * @author:  Manu Datta
     * @param <type> $request
     * @purpose:    To Send the email
     */
    public function executeSendEmailReminder(sfWebRequest $request) {
        $email = $request->getParameter('email');
        $customerName = $request->getParameter('customer_name');
        $merchantName = $request->getParameter('company_name');
        $complete_url = $request->getParameter('complete_url');
        $invoiceNo = $request->getParameter('invoice_no');
        $issue_date = $request->getParameter('issue_date');
        $due_date = $request->getParameter('due_date');
        $subject = 'Payment Reminder';

        $partialName = 'sendReminder';

        /* Mail firing code starts */
        $mailTo = $email;
        $mailToGroup = array();

        $mailFrom = sfConfig::get('app_sender');
        $signature = sfConfig::get('app_sender');

        $partialVars = array('customerName' => $customerName, 'complete_url' => $complete_url, 'merchantName' => $merchantName, 'due_date' => $due_date, 'issue_date' => $issue_date, 'invoiceNo' => $invoiceNo);
        $mailingOptions = array('mailFrom' => $mailFrom, 'mailTo' => $mailTo, 'mailSubject' => $subject);

        $mailObj = new Mailing();
        $mailInfo = $mailObj->sendMail($partialName, $partialVars, $mailingOptions);

        return $this->renderText($mailInfo);
    }

    /*
     * Below code written for teller to create a the invoice
     *  date 21 Jun 2012
     * Santosh Kumar
     */

    public function executeNewInvoice(sfWebRequest $request) {
        // get customer id
        $cust = cryptString::decrypt($request->getParameter('cust'));
        // get merchant id through customer id
        $arrCustomerInfo = Doctrine::getTable('CustomerProfile')->getCustomerMerchantInfoByCustomerId($cust);
        $this->merchant_id = $arrCustomerInfo['0']['merchant_id'];
        $this->customer_id = $cust;
        // get merchant info by mercahnt id
        $arrMerchantInfo = Doctrine::getTable('merchantProfile')->find(array($this->merchant_id));


        $this->getMerchantTaxDetails = Doctrine::getTable('tax')->findTaxDetailsOfMerchant($this->merchant_id);
        $this->form = new InvoiceForm(array(), array('merchant_name' => $arrMerchantInfo->getMerchantName(), 'merchant_id' => $this->merchant_id));
        $this->addClassTo = $this->getActionName();
    }

    public function executeCreateInvoice(sfWebRequest $request) {

        $this->forward404Unless($request->isMethod(sfRequest::POST));
        $this->merchant_id = $request->getPostParameter('merchant_id'); //$this->getUser()->getGuardUser()->getId();
        // get merchant info by mercahnt id
        $arrMerchantInfo = Doctrine::getTable('merchantProfile')->find(array($this->merchant_id));
        $this->getMerchantTaxDetails = Doctrine::getTable('tax')->findTaxDetailsOfMerchant($this->merchant_id);
        $this->form = new InvoiceForm(array(), array('merchant_name' => $arrMerchantInfo->getMerchantName(), 'merchant_id' => $this->merchant_id));

//        echo "<pre>";print_r($_POST);echo "</pre>";

        $mytotal = $request->getParameter('mytotal');
        $mysubtotal = $request->getParameter('mysubtotal');

        $item_no1 = $request->getParameter('item_no1');
        $item_desc1 = $request->getParameter('item_desc1');
        $item_price1 = $request->getParameter('item_price1');

        $item_no2 = $request->getParameter('item_no2');
        $item_desc2 = $request->getParameter('item_desc2');
        $item_price2 = $request->getParameter('item_price2');

        $item_no3 = $request->getParameter('item_no3');
        $item_desc3 = $request->getParameter('item_desc3');
        $item_price3 = $request->getParameter('item_price3');

        $item_tax0 = $request->getParameter('item_tax0');

        //Setting up an array to pass to the form partial
        $this->arr = array("mytotal" => $mytotal, "mysubtotal" => $mysubtotal, "item_no1" => $item_no1, "item_desc1" => $item_desc1, "item_price1" => $item_price1, "item_no2" => $item_no2, "item_desc2" => $item_desc2, "item_price2" => $item_price2, "item_no3" => $item_no3, "item_desc3" => $item_desc3, "item_price3" => $item_price3, "item_tax0" => $item_tax0);

        $this->processInvoiceForm($request, $this->form);



        $this->setTemplate('new');
    }

    public function executeEditInvoice(sfWebRequest $request) {
        $this->forward404Unless($invoice = Doctrine::getTable('Invoice')->find(array($request->getParameter('id'))), sprintf('Object invoice does not exist (%s).', $request->getParameter('id')));
        $this->form = new InvoiceForm($invoice);
    }

    public function executeUpdateInvoice(sfWebRequest $request) {
        // Varun : invoice tabel status set to "1" for regenerated field
        $id = cryptString::decrypt($request->getParameter('id'));
        $this->id = $request->getParameter('id');
        // $updateRegenerated = Doctrine::getTable('Invoice')->updateRegenerated($id);
        $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
        $this->forward404Unless($invoice = Doctrine::getTable('Invoice')->find(array($id)), sprintf('Object invoice does not exist (%s).', $id));
        //Variable added
        $this->merchant_id = $this->getUser()->getGuardUser()->getId();
        $this->getMerchantTaxDetails = Doctrine::getTable('tax')->findTaxDetailsOfMerchant($this->merchant_id);
        $this->invoiceArray = Doctrine::getTable('Invoice')->find(array($id));
        $this->recurring_plan = Doctrine::getTable('RecurrentBilling')->findBy('invoice_id', $this->invoiceArray->getId())->getFirst();
        $this->recurring = '';
        if ($request->getParameter('recurring') != '')
            $this->recurring = $request->getParameter('recurring');
        $this->part_payment = '';
        if ($request->getParameter('part_payment_percent') != '')
            $this->part_payment = $request->getParameter('part_payment_percent');
        $this->merchant_id = $this->getUser()->getGuardUser()->getId();
        $this->getTaxDetail = Doctrine::getTable('InvoiceItemDetails')->findBy('invoice_id', $id);
        $this->invoice = Doctrine::getTable('Invoice')->find($id);
        $this->invoiceDetails = Doctrine::getTable('InvoiceItemDetails')->getByInvoiceId($id);
        $this->form = new InvoiceForm();

        $this->processInvoiceForm($request, $this->form);

        $this->setTemplate('edit');
    }

    protected function processInvoiceForm(sfWebRequest $request, sfForm $form) {

//       echo $updateId = cryptString::decrypt($request->getParameter('id'));


        $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));

        if ($form->isValid()) {

//            try{
//        $conn = Doctrine::getTable('Invoice')->getConnection();
//        $conn->beginTransaction();
            $allParams = $request->getParameterHolder()->getAll();

            if (isset($allParams['item_tax0']) && $allParams['item_tax0'] != '') {
                // Item tax calculated for change in tax
                $tax_id_detail = explode('/', $allParams['item_tax0']);
                $tax_id = $tax_id_detail[0];
                $statusCheck = Doctrine::getTable('tax')->findTax($tax_id);
                if ($statusCheck[0]['status'] == '1') {
                    $merchant_id = $allParams['merchant_id'];
                    $taxExist = Doctrine::getTable('MerchantTax')->findEntryForMerchantTax($tax_id, $merchant_id);
                    $item_tax = $taxExist[0]['tax_value'];
                } else {
                    $item_tax = '0.00';
                }
            } else {
                $tax_id = null;
                $item_tax = '0.00';
            }

            $invoice = $form->save();
            $invoice->setPartPayment($allParams['part_payment_percent']);
            $invoice->setTaxId($tax_id);
            $invoice->save();
            $pattern = '/item_no/';
            preg_match_all($pattern, implode(",", array_keys($allParams)), $matches, PREG_OFFSET_CAPTURE);


            $row_index_capture = array();
            foreach ($matches[0] as $matchNo => $matchDetails) {


                array_push($row_index_capture, substr(implode(",", array_keys($allParams)), $matchDetails[1] + strlen($pattern) - 2, 1));
            }
            $pattern = '/item_/';
            preg_match_all($pattern, implode(",", array_keys($allParams)), $matches);
            $amountDue = 0;
            foreach ($row_index_capture as $index => $available_row_index) {
                $details_ob = new InvoiceItemDetails();
                $details_ob->setInvoiceId($invoice->id);
                $details_ob->setNoOfItems($allParams['item_no' . $available_row_index]);
                $details_ob->setItemDescription($allParams['item_desc' . $available_row_index]);
                $details_ob->setItemPrice($allParams['item_price' . $available_row_index]);
                //remove item tax
                $details_ob->setItemTax($item_tax);
                $details_ob->setSubTotal(round($details_ob->getItemPrice() * $details_ob->getNoOfItems(), 2));
                $details_ob->setTaxTotal(round(($details_ob->getSubTotal() * $details_ob->getItemTax() / 100), 2));
                $amountDue +=$details_ob->getSubTotal() + $details_ob->getTaxTotal();
                $details_ob->save();
            }

            if (array_key_exists('part_payment', $allParams)) {
                $invoice->setAmountDue($allParams['part_payment_percent'] * $amountDue / 100);
//                $invoice->setAmountPaid(0);
            } else {
//                echo "coming here";
                $invoice->setAmountDue($amountDue);
            }
            $invoice->save();
            $partial_name = 'emailContent';
            $subject = "Regular Invoice";
            $nextInvoice = null;
            if (array_key_exists('recurring', $allParams) && $allParams['recurring'] == 'yes') {
                if ($allParams['recurring'] == 'yes' && isset($allParams['recurring_plan'])) {
                    $recurringOb = new RecurrentBilling();
                    $recurringOb->setInvoiceId($invoice->id);
                    $recurringOb->setType($allParams['recurring_plan']);
                    $recurringOb->setValue($allParams['custom_recurring_bill']);
                    $recurringOb->setStatus('live');
                    $recurringOb->save();

                    switch ($recurringOb->getType()) {

                        case 'weekly':
                            $nextInvoice = date("d-M-Y", strtotime($invoice->getDueDate() . "+1 week"));
                            break;
                        case 'monthly':
                            $nextInvoice = date("d-M-Y", strtotime($invoice->getDueDate() . "+1 month"));
                            break;
                        case 'yearly':
                            $nextInvoice = date("d-M-Y", strtotime($invoice->getDueDate() . "+1 year"));
                            break;
                        case 'quarterly':
                            $nextInvoice = date("d-M-Y", strtotime($invoice->getDueDate() . "+3 months"));
                            break;
                        case 'custom':
                            $nextInvoice = date("d-M-Y", strtotime($invoice->getDueDate() . "+" . $allParams['custom_recurring_bill'] . " days"));
                            break;
                    }
                    $partial_name = 'recurringInvoice';
//                    $partial_name = 'emailContent';
//                    $subject = "Re : " . $invoice->Merchant->getMerchantName() . " Invoice Reminder";
                    $subject = "Pay4bill-Recurring Bill: ". $invoice->Merchant->getMerchantName() ." invoice due on ".$invoice->getDueDate();
                }
            } else {
                $partial_name = 'emailContent';
                $subject = "Regular Invoice";
                $nextInvoice = null;
            }
            $host = $request->getUriPrefix();
            $url_root = $request->getPathInfoPrefix();
            $complete_url = $host . $url_root;
            sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
            $taskId = EpjobsContext::getInstance()->addJob('InvoiceCreation', $this->moduleName . "/sendEmail", array('email' => $invoice->Customer->getCustomerId(), 'customer_name' => $invoice->Customer->getName(), 'next_invoice' => $nextInvoice, 'company_url' => $invoice->Merchant->getCompanyUrl(), 'company_name' => $invoice->Merchant->getMerchantName(), 'subject' => $subject, 'invoiceNo' => $invoice->getInvoiceNo(), 'deliveryDate' => $invoice->getDeliveryDate(), 'due_date' => $invoice->getDueDate(), 'invoice_ob_id' => $invoice->getId(), 'additional_information' => $invoice->getAdditionalInformation(), 'partial_name' => $partial_name, 'complete_url' => $complete_url,'image_tag'=>public_path(sfConfig::get('app_logo_path'),true),'issue_date' => $invoice->getIssueDate()
), '-1', 'frontend');

            $this->logMessage("scheduled mail job with id: $taskId", 'debug');
//            $this->getUser()->setAttribute('msg_invoice', 'Invoice Created');
            $this->getUser()->setFlash('notice', 'Invoice created successfully with Invoice Number:' . $invoice->getInvoiceNo());

            $this->redirect('invoice/index');
        }
//        catch (Exception $e) {
//            $exception=true;
//        }
//         if (!$exception) {
//                            $conn->commit();
//
//
//                        } else {
//                            $conn->rollback(); /* --------Else rollback----- */
//                        }
//        }
    }

}
