<?php use_helper('ePortal') ?>
<script language="javascript" >
    $(document).ready(function(){
        $('#Settings').addClass('active');
    });
</script>
<div class="form_section">
    <div id="pgMenu">
        <div id="wrappernew">
            <div class="pgTitle">
                <h2>Settings</h2>
            </div>
            <div class="pgNav">
                <ul>
                    <li class=""><a href="<?php echo url_for('userAdmin/changePassword') ?>">Change Password</a></li>
                    <?php if($merchantLogin||$adminLogin) { ?>
                    <li class=""><a href="<?php echo url_for('userAdmin/viewBank') ?>">View Bank</a></li>
                    <?php }
                    if($merchantLogin) { ?>
                    <li class=""><a href="<?php echo url_for('merchantRegistration/uploadMerchantLogo') ?>">Upload Merchant Logo</a></li>
                    <li ><a href="<?php echo url_for('custMgt/addProfile') ?>">Add Customer Profile</a></li>
                    <?php } ?>

                    <li class="current"><a href="<?php echo url_for('userAdmin/viewDetail') ?>">View Details</a></li>
                    
                </ul>
            </div>
        </div>
    </div>
    
    <?php //echo ePortal_pagehead("",array('class'=>'msg_serverside')); /*echo "<pre>";print_r($allDetails); */ ?><br/>

    <?php if($merchantLogin){?>
     <div class="TableForm">
         
                <h3>View Details</h3>
               <table>
            <tr>
                <td width="35%"align="right">Name</td>
                <td><?php if(isset($allDetails[0]['merchant_name'])) echo $allDetails[0]['merchant_name'];?></td>
            </tr>
            <tr>
                <td align="right">Email</td>
                <td><?php if(isset($allDetails[0]['email'])) echo $allDetails[0]['email'];?></td>
            </tr>
            <tr>
                <td align="right">CAC Registration</td>
                <td><?php if(isset($allDetails[0]['cac_registration'])) echo $allDetails[0]['cac_registration'];?></td>
            </tr>
            <tr>
                <td  align="right">Contact Number</td>
                <td><?php if(isset($allDetails[0]['merchant_contact_number'])) echo $allDetails[0]['merchant_contact_number'];?></td>
            </tr>
            <tr>
                <td  align="right">Company URL</td>
                <td><?php if(isset($allDetails[0]['company_url'])) echo $allDetails[0]['company_url'];?></td>
            </tr>
          </table>
    </div>
    <?php } ?>

   <?php if(isset($tellerLogin) && $tellerLogin==true){?>
     <div class="TableForm">
                <h3>View Details</h3>
               <table>
            <tr>
                <td width="35%" align="right">Email</td>
                <td><?php if(isset($allDetails[0]['email'])) echo $allDetails[0]['email'];?></td>
            </tr>
            <tr>
                <td align="right">Address</td>
                <td><?php if(isset($allDetails[0]['address'])) echo $allDetails[0]['address'];?></td>
            </tr>
            <tr>
                <td align="right">Contact Number</td>
                <td><?php if(isset($allDetails[0]['phone_number'])) echo $allDetails[0]['phone_number'];?></td>
            </tr>
            <tr>
                <td align="right">Bank Name</td>
                <td><?php if(isset($allDetails[0]['bankName'])) echo $allDetails[0]['bankName'];?></td>
            </tr>
          </table>
    </div>
    <?php } ?>
    <?php if(isset($supportUserLogin) && $supportUserLogin==true){?>
     <div class="TableForm">
                <h3>View Details</h3>
               <table>
            <tr>
                <td width="35%" align="right">Email</td>
                <td><?php if(isset($allDetails[0]['email'])) echo $allDetails[0]['email'];?></td>
            </tr>
            <tr>
                <td align="right">Address</td>
                <td><?php if(isset($allDetails[0]['address'])) echo $allDetails[0]['address'];?></td>
            </tr>
            <tr>
                <td align="right">Contact Number</td>
                <td><?php if(isset($allDetails[0]['phone_number'])) echo $allDetails[0]['phone_number'];?></td>
            </tr>
          </table>
    </div>
    <?php } ?>

    <?php if($adminLogin || $subadminLogin){?>
     <div class="TableForm">
                <h3>View Details</h3>

                <table>
            <tr>
                <td  align="right">Name</td>
                <td>Admin</td>
            </tr>
            <tr>
                <td align="right">Email</td>
                <td><?php if(isset($allDetails[0]['email'])) echo $allDetails[0]['email'];?></td>
            </tr>

                </table>


    </div>
    <?php } ?>

    <?php if($customerLogin){?>

     <div class="TableForm">
                <h3>View Details</h3>
                
                <table width="100%">
                    <tr><td valign="top">
                <?php 
                    $i = 0;
                    foreach($allDetails as $k=>$v){if($i >= 0){
                ?>  
                    <div style="float:left; width:46%;border:0px solid red;padding:10px" >
                        <?php 
                            // Merchant Name
                            echo '<div><b><i>Merchant:</i></b>&nbsp;'.Doctrine::getTable('MerchantProfile')->find($v->getMerchantProfileId())->getMerchantName(); echo '</div>';
                        ?>
                        <div style="border:0px solid red;width:80%;margin:auto">
                                <div style="float:left;width:160px" ><b>Name:</b></div>
                            <div style="float:left"><?php echo $v->getName(); ?></div>
                            <div style="clear:both"></div>
                            <div style="float:left;width:160px"><b>Email:</b></div>
                            <div style="float:left"><?php echo $v->getEmailAddress(); ?></div>
                            <div style="clear:both"></div>
                            <div style="float:left;width:160px"><b>Sex:</b></div>
                            <div style="float:left"><?php echo $v->getSex(); ?></div>
                            <div style="clear:both"></div>
                            <div style="float:left;width:160px"><b>Contact Number:</b></div>
                            <div style="float:left"><?php echo $v->getPhoneNumber();?></div>
                            <div style="clear:both"></div>
                            <div style="float:left;width:160px"><b>Address:</b></div>
                            <div style="float:left"><?php echo $v->getAddress();?></div>
                            <div style="clear:both"></div>
                        </div>    
                    </div> 
                <?php $i++;
                   }
                } ?>
                        </td></tr></table>             
                
                
    </div>

    <?php } ?>
</div>
