   <div class="clear_new"></div>
   <div class="TableForm">
       <table>
           <tr>
               <td width="200"><?php echo $form['bank_name']->renderLabel() ?></td>
               <td>
                    <?php echo $form['bank_name']->render() ?>
                    <label id="bank_name_error" class="error">  <?php echo $form['bank_name']->renderError(); ?></label>
               </td>
           </tr>
           <tr>
               <td><?php echo $form['bank_branch']->renderLabel() ?></td>
               <td>
                    <?php echo $form['bank_branch']->render() ?>
                    <label id="bank_branch_error" class="error">   <?php echo $form['bank_branch']->renderError(); ?></label>
               </td>
           </tr>
           <tr>
               <td><?php echo $form['account_name']->renderLabel() ?></td>
               <td>
                    <?php echo $form['account_name']->render() ?>
                    <label id="account_name_error" class="error"> <?php echo $form['account_name']->renderError(); ?> </label>
               </td>
           </tr>
           <tr>
               <td><?php echo $form['account_number']->renderLabel() ?></td>
               <td>
                    <?php echo $form['account_number']->render() ?>
                    <label  id="account_number_error" class="error">   <?php echo $form['account_number']->renderError(); ?></label>
               </td>
           </tr>
           <tr>
               <td><?php echo $form['sort_code']->renderLabel() ?></td>
               <td>
                    <?php echo $form['sort_code']->render() ?>
                    <label id="sort_code_error" class="error">   <?php echo $form['sort_code']->renderError(); ?></label>
               </td>
           </tr>
           <tr><td></td>
           <td>
               <?php echo $form['_csrf_token']; ?>

                  <input class="blueBtn" type="submit" value="Submit" name="Submit">
                  <?php if(isset($adminLogin) && $adminLogin==true) { ?>
                  <input class="blueBtn" type="button" value="Back" name="Back" onclick="javascript:history.go(-1)" >
                  <?php } else { ?>
                  <input class="blueBtn" type="button" value="Back" name="Back" onclick="window.location='<?php echo url_for("userAdmin/newBank");?>';" >
                  <?php } ?>
           </td></tr>
       </table>
   </div>