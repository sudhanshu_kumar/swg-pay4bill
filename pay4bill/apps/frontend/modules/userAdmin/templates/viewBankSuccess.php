<?php use_helper('ePortal') ?>
<script>
    function validateSearch(){
        var err  = 0;
        var ban_name = jQuery.trim($('#ban_name').val());
        var com_name = jQuery.trim($('#com_name').val());

        if(ban_name =='' && $('#ban_name').is(":visible")){
            $('#ban_name_error').html("<font color='red'>Bank Name is Required.</font>");
            err = 1;
        }
        if(com_name =='' && $('#com_name').is(":visible"))  {
            $('#com_name_error').html("<font color='red'>Merchant Name is Required.</font>");
            $('#com_name').focus();
            err = 1;
        }
        if(err == 1) return false;
        
        return true;
    }

    function onChangeDropDown(){

        var ban_name = jQuery.trim($('#ban_name').val());
        var com_name = jQuery.trim($('#com_name').val());

        // Hide all once
        $('#bank_name').hide();
        $('#company_name').hide();

        if($('#search_par').val() == 'bank'){
            $('#bank_name').show(); // Show the div based on the value selected from the drop down
            $('#com_name').val(''); // Make rest of the field empty
        }
        if($('#search_par').val() == 'company'){
            $('#company_name').show();  // Show the div based on the value selected from the drop down
            $('#ban_name').val(''); // Make rest of the field empty
        }
        $('#com_name_error').html("");
        $('#ban_name_error').html("");
    }
    
<?php if (isset($successMsg)) { ?>
        $('#success_msg').html('<?php echo $successMsg; ?>');
        $('#success_msg').addClass('msg_serverside');
<?php } ?>
    $(document).ready(function(){
        onChangeDropDown();
        $('.resetCls').click(function(){
            $('input[type=text]').val('');
            $('#company_name').hide();
            $('#bank_name').hide();
            $('#search_par').val('');
        })
        $('#Settings').addClass('active');
    });
</script>

<div id="pgMenu">
    <div id="wrappernew">
        <div class="pgTitle">
            <h2>Settings</h2>
        </div>
        <div class="pgNav">
            <ul>
                <li class=""><a href="<?php echo url_for('userAdmin/changePassword') ?>">Change Password</a></li>
                <li class="current"><a href="<?php echo url_for('userAdmin/viewBank') ?>">View Bank</a></li>
                <?php if ($merchantLogin) {
                ?>
                    <li class=""><a href="<?php echo url_for('merchantRegistration/uploadMerchantLogo') ?>">Upload Merchant Logo</a></li>
                    <li><a href="<?php echo url_for('custMgt/addProfile') ?>">Add Customer Profile</a></li>
                <?php } ?>
                <li><a href="<?php echo url_for('userAdmin/viewDetail') ?>">View Details</a></li>
            </ul>
        </div>
    </div>
</div>
<?php if (!empty($msg)) {
 ?><div class="clear_new"></div> <?php } ?>
                <div id="mainnew">
    <?php
                $sf = sfContext::getInstance()->getUser();
                if ($sf->hasFlash('notice')) {
    ?><div id="flash_notice" class="msg_serverside" ><?php
                    echo nl2br($sf->getFlash('notice'));
    ?></div><?php } ?>
            <span id="success_msg"></span>
            <div class="clear_new"></div>
            <div id="wrappernew">
                <div id="content"><div id="Formwraper">
                <?php if (count($banks) == '0' && $merchantLogin) { ?>
                    <h1 align="right"> <a class="link" href="<?php echo url_for('userAdmin/newBank'); ?>">Add New Bank</a> </h1>
                <?php } ?>
                <!-- Search starts here -->
                <?php if ($adminLogin) {
 ?>
                    <h3>Search Banks</h3>
                    <div id="msg"></div>

                    <div class="TableForm" id="search">

                        <form action="<?php echo url_for('userAdmin/viewBank'); ?>" method="post" id="search_form" onsubmit="return validateSearch()">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>&nbsp;Merchant Name<font color="red"></font></td>
                                    <td>
                                        <input type="text" id="customer_id" name="merchant_name"  maxlength="50" value="<?php echo $merchant_name; ?>">
                                        <div class="red redlt" id="customer_id_error"></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;Bank Name<font color="red"></font></td>
                                    <td>
                                        <input type="text" id="customer_id" name="bank_name"  maxlength="50" value="<?php echo $bank_name; ?>">
                                        <div class="red redlt" id="customer_id_error"></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <div style="clear:left;" class="Errormrg">
                                            <input type="submit" value="Search" class="blueBtn">
                                            <input type="button" class="blueBtn resetCls" value="Reset" id="reset" onclick="window.location='<?php echo url_for('userAdmin/viewBank'); ?>'">
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </form>
                    </div>
<?php } ?>
                <div class="clear_new"></div>
                <!-- Search ends here -->
                <h3>Bank Details</h3>
                <div class="TableForm">
                    <table>
                        <thead>
                            <tr>
<?php if ($merchantLogin || $adminLogin) { ?>
                                <th>Merchant Name</th>
                                <th>Bank Name</th>
                                <th>Bank Branch</th>
                                <th>Account Name</th>
                                <th>Account Number</th>
                                <th>Sort Code</th>
<?php if ($adminLogin) { ?>
                                <th>Action</th>
                                <?php }
                        } ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if ($banks != '' && count($banks) > 0) {

                                $index_for_getting_invoice_amount = 0;
                            ?>
                            <?php $i = 0;
                                foreach ($banks as $bank): $i++ ?>  <!-- Varun :Bug Fix 35012 -->
                            <?php if ($i % 2 != 0) { ?>
                                        <tr class="odd">
                                <?php } else {
                                ?><tr class=""> <?php } ?>
                                    <?php if ($merchantLogin || $adminLogin) {
                                    ?>
                                    <td>
                                    <?php
                                        $merchantArr = Doctrine::getTable('MerchantProfile')->getMerchantInfoByMerchantId($bank->getMerchantId());
                                        echo $merchantArr[0]['merchant_name'];
                                    ?>
                                    </td>
                                    <td><?php echo $bank->getBankName() ?></td>
                                    <td><?php echo $bank->getBankBranch() ?></td>
                                    <td><?php echo $bank->getAccountName() ?></td>
                                    <td><?php echo $bank->getAccountNumber() ?></td>
                                    <td><?php echo $bank->getSortCode() ?></td>
                                <?php if ($adminLogin) {
                                ?>
                                            <!-- Varun : Encrypted id
                                            Bug Fix : 34958-->
                                            <td><?php echo link_to(image_tag('tdEdit.png'), 'userAdmin/editBank?id=' . cryptString::encrypt($bank->getId()), array('method' => 'get', 'class' => 'editInfo', 'title' => 'Edit')) ?>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                <?php }
                                    } ?>
                                </tr>
                            <?php endforeach; ?>
                            <?php
                                } else {
                                    echo "<tr><td colspan='7' align='center'>No Bank Found</td></tr>";
                                }
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="nofoList">
                    <?php
                                include_partial('global/records_number', array('amount_footer' => false, 'current_offset' => isset($current_offset) ? $current_offset : null, 'offset_next' => isset($offset_next) ? $offset_next : null, 'total_no_of_records' => isset($total_no_of_records) ? $total_no_of_records : count($banks), 'no_of_records_per_page' => isset($no_of_records_per_page) ? $no_of_records_per_page : null, 'from' => '', 'total_amount_of_all_transactions' => isset($total_amount_of_all_transactions) ? $total_amount_of_all_transactions : null));
                    ?></div>
                <?php if ($setPagination) {
 ?>
                                    <div align="right">
                    <?php
                                    include_partial('global/pagination', array('moduleName' => $module, 'amount_footer' => false, 'actionName' => $actionTobeCalled, 'current_offset' => $current_offset, 'offset_next' => $offset_next, 'offset_prev' => $offset_prev, 'offset_last' => $offset_last, 'search' => '', 'from' => '', 'to' => '', 'date_cat' => '', 'com_name' => isset($com_name) ? $com_name : '', 'ban_name' => isset($ban_name) ? $ban_name : ''));
                    ?></div><?php } ?>

            </div>
        </div></div></div>