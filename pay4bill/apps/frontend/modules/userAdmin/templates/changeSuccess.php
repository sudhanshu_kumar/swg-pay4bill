<?php use_helper('ePortal') ?>
<script language="javascript" >
    $(document).ready(function(){
        $('#Settings').addClass('active');
        $('form').attr('id','change_password_frm');
    });
</script>
<div class="form_section">
<?php    if ($customerLogin) {
include_partial('global/settingsSubHeaderForCustomerLogin', array('addClassTo' => $addClassTo));
} else { ?>
    <div id="pgMenu">
        <div id="wrappernew">
            <div class="pgTitle">
                <h2>Settings</h2>
            </div>
            <div class="pgNav">
                <ul>
                    <li class="current"><a href="<?php echo url_for('userAdmin/changePassword') ?>">Change Password</a></li>
                    <?php if ($merchantLogin || $adminLogin) {?>
                        <li class=""><a href="<?php echo url_for('userAdmin/viewBank') ?>">View Bank</a></li>
                    <?php } ?>
                    <?php if ($merchantLogin) { ?>
                        <li class=""><a href="<?php echo url_for('merchantRegistration/uploadMerchantLogo') ?>">Upload Merchant Logo</a></li>
                        <li class=""><a href="<?php echo url_for('custMgt/addProfile') ?>">Add Customer Profile</a></li>
                    <?php } ?>
                    <?php if ($customerLogin) { ?>
                        <li class=""><a href="<?php echo url_for('custMgt/viewProfiles') ?>">View Customer Profiles</a></li>
                    <?php } else { ?>
                        <li class=""><a href="<?php echo url_for('userAdmin/viewDetail') ?>">View Details</a></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
    <?php } ?>
    <?php echo ePortal_pagehead("", array('class' => 'msg_serverside')); ?>
<?php include_partial('formChange', array('form' => $form)) ?>
</div>