<script>
    //Varun : Bug fix FS#34859
    $(document).ready(function(){
        $('#Settings').addClass('active');
    });
    function validateForm(){
        var err  = 0;
        var bank_name = jQuery.trim($('#bank_name').val());
        var bank_branch = jQuery.trim($('#bank_branch').val());
        var account_name = jQuery.trim($('#account_name').val());
        var account_number = jQuery.trim($('#account_number').val());
        var sort_code = jQuery.trim($('#sort_code').val());
        if(bank_name == '') {
                $('#bank_name_error').html("Please enter Bank Name.");
                $('#bank_name').focus();
                err = err + 1;
         } else {
        if(bank_name != '')
        {
            if(validateName(bank_name) != 0){
            $('#bank_name_error').html("Please enter valid Bank Name.");
                err = err+1;
            }else if(bank_name.length > 40){
                $('#bank_name_error').html("Bank Name should be less than or equal to 40 characters.");
                $('#bank_name').focus();
                err = err + 1;
            } else if(bank_name.length < 5){
                $('#bank_name_error').html("Bank Name should be minimum 5 characters.");
                $('#bank_name').focus();
                err = err + 1;
            } else {
                $('#bank_name_error').html("");
            }

        }
        else
        {
            $('#bank_name_error').html("");
        }
      }
        if(bank_branch == '') {
                 $('#bank_branch_error').html("Please enter Bank Branch.");
                $('#bank_branch').focus();
                err = err + 1;
         } else {
        if(bank_branch != '')
        {
            if(validateName(bank_branch) != 0){
            $('#bank_branch_error').html("Please enter valid Bank Branch.");
                err = err+1;
            }else if(bank_branch.length > 50){
                $('#bank_branch_error').html("Bank Branch should be less than or equal to 50 characters.");
                $('#bank_branch').focus();
                err = err + 1;
            } else if(bank_branch.length < 5){
                $('#bank_branch_error').html("Bank Branch should be minimum 5 characters.");
                $('#bank_branch').focus();
                err = err + 1;
            }  else {
                $('#bank_branch_error').html("");
            }

        }
        else
        {
            $('#bank_branch_error').html("");
        }
      }
        if(account_name == '') {
                 $('#account_name_error').html("Please enter Account Name.");
                $('#account_name').focus();
                err = err + 1;
         } else {
        if(account_name != '')
        {
             if(validateName(account_name) != 0){
                $('#account_name_error').html("Please enter valid Account Name.");
                err = err+1;
             }else if(account_name.length > 100){
                $('#account_name_error').html("Account Name should be less than or equal to 100 characters.");
                $('#account_name').focus();
                err = err + 1;
            } else if(account_name.length < 5){
                $('#account_name_error').html("Account Name should be minimum 5 characters.");
                $('#account_name').focus();
                err = err + 1;
            }  else {
                $('#account_name_error').html("");
            }

        }
        else
        {
            $('#account_name_error').html("");
        }
      }
        if(account_number == '') {
                 $('#account_number_error').html("Please enter Account Number.");
                $('#account_number').focus();
                err = err + 1;
         } else {
        if(account_number != '')
        {
            //Varun : Bug fix FS#34861
             if(isNaN(account_number)|| account_number <= 0) {
                $('#account_number_error').html("Please enter Numeric Account Number.");
                err = err+1;
            }else if(account_number.length < 5){
                $('#account_number_error').html("Account Number should be minimum 5 characters.");
                $('#account_number').focus();
                err = err + 1;
            }else if(account_number.length > 20){
                $('#account_number_error').html("Account Number should be less than or equal to 20 characters.");
                $('#account_number').focus();
                err = err + 1;
            }
            else {
                $('#account_number_error').html("");
            }

        }
        else
        {
            $('#account_number_error').html("");
        }
      }
        if(sort_code == '') {
                 $('#sort_code_error').html("Please enter Sort Code.");
                $('#sort_code').focus();
                err = err + 1;
         } else {
        if(sort_code != '')
        {
             if(isNaN(sort_code) || sort_code <= 0) {
                $('#sort_code_error').html("Please enter Numeric Sort Code.");
                err = err+1;
            } else if(sort_code.length > 7){
                $('#sort_code_error').html("Sort Code should be minimum 1 characters.");
                $('#sort_code').focus();
                err = err + 1;
            } else if(sort_code.length < 1){
                $('#sort_code_error').html("Sort Code should be greater than or equal to 1 characters.");
                $('#sort_code').focus();
                err = err + 1;
            } else {
                $('#sort_code_error').html("");
            }

        }
        else
        {
            $('#sort_code_error').html("");
        }
      }
        if(err>0)
            {
                return false;
            }
        return true;

    }
    function validateName(str)
    {
      var reg = /^[A-Za-z \-\.]*$/; //allow alphabet and spaces only...
      if(reg.test(str) == false) {
        return 1;
      }

      return 0;
    }

</script>
<div class="clear_new"></div>
    <?php $sf = sfContext::getInstance()->getUser();
if($sf->hasFlash('notice')){
  ?><div id="flash_notice" class="error_serverside" ><?php
  echo nl2br($sf->getFlash('notice'));

  ?></div><?php }?>
<!-- Varun : Bug fix FS#34859 -->
<div class="form_section">
<div id="pgMenu">
<div id="wrappernew">
<div class="pgTitle">
<h2>Settings</h2>
</div>
     <div class="pgNav">
            <ul>
                <li class=""><a href="<?php echo url_for('userAdmin/changePassword') ?>">Change Password</a></li>
                <li class="current"><a href="<?php echo url_for('userAdmin/viewBank') ?>">View Bank</a></li>
                <li class=""><a href="<?php echo url_for('userAdmin/viewDetail') ?>">View Details</a></li>
        </ul>
        </div>

</div>
</div>
<form name="frm" action="<?php echo url_for('userAdmin/updateBank') ?>" method="post" onsubmit="return validateForm()">
<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>

  <?php if (!$form->getObject()->isNew()): ?>
  <input type="hidden" name="sf_method" value="put" />
  <?php endif; ?>
  <div class="sform">
    <h3>Update Bank Information</h3>
    <div class="clear_new"></div>
<?php include_partial('form', array('form' => $form,'adminLogin'=>$adminLogin)) ?>
     </div>
  <?php //echo $form ?>
    <input type="hidden" name="hdn_merchant_id" id="hdn_merchant_id" value="<?php echo $merchantId; ?>">
    <input type="hidden" name="hdn_id" id="hdn_id" value="<?php echo $id; ?>">
</form>
</div>
