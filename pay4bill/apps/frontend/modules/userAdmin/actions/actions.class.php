<?php

/**
 * userAdmin actions.
 *
 * @package    symTest
 * @subpackage userAdmin
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class userAdminActions extends sfActions {

    public function executeChangePassword(sfWebRequest $request) {
        /* Unset below session varibale which have not been set Customer -> Payment History
         * Function Name:executeAllPaymentsForCustomer && executePartlyPaidForCustomer on
         * date: 31/05/2012
         */
        $_SESSION['com_name_payment_history'] = NULL;
        $_SESSION['invoice_no_payment_history'] = NULL;
        $_SESSION['search_par_payment_history'] = NULL;
        /* End of unset below session varibale which have not been set Customer -> Payment History */

        $this->customerLogin = pay4billHelper::checkIfCustomerUser($this->getUser()->getGuardUser()->getGroups()->toArray());
        $this->adminLogin = pay4billHelper::checkIfAdminUser($this->getUser()->getGuardUser()->getGroups()->toArray());
        $this->merchantLogin = pay4billHelper::checkIfMerchantUser($this->getUser()->getGuardUser()->getGroups()->toArray());
        $this->addClassTo = $this->getActionName();
        $this->form = new ChangeUserForm($this->getUser()->getGuardUser());
        $this->setTemplate('change');
        $parameters = $request->getParameterHolder()->getAll();
        $this->merchant_key = array_key_exists('merchant_id', $parameters) ? $parameters['merchant_id'] : $this->getUser()->getGuardUser()->getMerchantUser()->getId();
    }

    /*
     * To show the detail of Admin/Customer/Merchant in non editable mode
     * Created by Saurabh
     */

    public function executeViewDetail(sfWebRequest $request) {
        /* Unset below session varibale which have not been set Customer -> Payment History
         * Function Name:executeAllPaymentsForCustomer && executePartlyPaidForCustomer on
         * date: 31/05/2012
         */
        $_SESSION['com_name_payment_history'] = NULL;
        $_SESSION['invoice_no_payment_history'] = NULL;
        $_SESSION['search_par_payment_history'] = NULL;
        /* End of unset below session varibale which have not been set Customer -> Payment History */

        $this->customerLogin = false;
        $this->merchantLogin = false;
        $this->adminLogin = false;
        $this->subadminLogin = false;
        $this->allDetails = array();

        $id = $this->getUser()->getGuardUser()->getId();
        $email = $this->getUser()->getGuardUser()->getEmailAddress();

        $this->setTemplate('viewDetail');



        if (isset($_SESSION['logedin_user_group']) && $_SESSION['logedin_user_group'] == 'customer') {

            $id = pay4billHelper::getCustomerProfilesForUserObj($this->getUser()->getGuardUser());
            $allCustomerId = $id;

            $this->customerLogin = true;
//          $this->allDetails = Doctrine::getTable('CustomerProfile')->getCustomerInfoByUserId($id);
            $arr = explode(",", $allCustomerId);
            $this->allDetails = Doctrine::getTable('CustomerProfile')->findByDql("id in ($allCustomerId)");
//              echo "<pre>";print_r( $this->allDetails->toArray());
            //$this->allDetails[0]['email'] = $email;
        } else if (isset($_SESSION['logedin_user_group']) && $_SESSION['logedin_user_group'] == 'merchant') {
            $this->merchantLogin = true;
            $this->allDetails = Doctrine::getTable('MerchantProfile')->getMerchantInfoByMerchantId($id);
            $this->allDetails[0]['email'] = $email;
        } else if (isset($_SESSION['logedin_user_group']) && $_SESSION['logedin_user_group'] == 'teller') {
            $this->tellerLogin = true;
            $this->allDetails = Doctrine::getTable('TellerProfile')->getTellerInfoById($id);
            $this->allDetails[0]['email'] = $email;
        } else if (isset($_SESSION['logedin_user_group']) && $_SESSION['logedin_user_group'] == 'support') {
            $this->supportUserLogin = true;
            $this->allDetails = Doctrine::getTable('SupportUserProfile')->getSupportUserInfoById($id);
            $this->allDetails[0]['email'] = $email;
        }else {
            $this->adminLogin = pay4billHelper::checkIfAdminUser($this->getUser()->getGuardUser()->getGroups()->toArray());
            $this->subadminLogin=(pay4billHelper::checkIfSubAdminMerchantReportUser($this->getUser()->getGuardUser()->getGroups()->toArray()) || pay4billHelper::checkIfSubAdminReportUser($this->getUser()->getGuardUser()->getGroups()->toArray()));
            $this->allDetails = Doctrine::getTable('sfGuardUser')->getSelectedInfoById($id);
            $this->allDetails[0]['email'] = $email;
        }
    }

    //save the new password
    public function executeSavechange(sfWebRequest $request) {
        /*
         * Manu : variable defined
         */
        $this->customerLogin = false;
        $this->merchantLogin = false;
        $this->adminLogin = false;

       if (isset($_SESSION['logedin_user_group']) && $_SESSION['logedin_user_group'] == 'customer') {
            $this->customerLogin = true;
        } else if (isset($_SESSION['logedin_user_group']) && $_SESSION['logedin_user_group'] == 'merchant') {
            $this->merchantLogin = true;
        }
        if (in_array('administrator', $this->getUser()->getGuardUser()->getGroupNames()) != false) {
            $this->adminLogin = true;
        } else {
            $this->adminLogin = false;
        }

        $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
        $this->form = new ChangeUserForm($this->getUser()->getGuardUser());
        #set last login time
        //$this->form->setDefault('last_login',date('Y-m-d h:i:s'));

        if ($this->processForm($request, $this->form, false)) {
            // print_r($this->getUser()->getGuardUser()->setLastLogin(date('Y-m-d h:i:s'))); die('Settings last login');
            $this->getUser()->setFlash('notice', "Password has been  changed successfully", false);

            $this->forward($this->getModuleName(), 'changePassword');
        }
        $this->addClassTo = 'changePassword';
        $this->setTemplate('change');
    }

    protected function processForm(sfWebRequest $request, sfForm $form, $redirect=true) {

        $form->bind($request->getParameter($form->getName()));
        
        //Check if the user is teller user
        $user_id = $this->getUser()->getGuardUser()->getId(); // Logged in user (sfGuardUser)
        $tellerProfile = Doctrine::getTable('TellerProfile')->findByUserId($user_id);
        $isTellerUser = pay4billHelper::checkIfTellerUser($this->getUser()->getGuardUser()->getGroups()->toArray());
        
        if ($form->isValid()) {
            $sf_guard_user = $form->save();
            
            //Saurabh @ 27 Dec 2012
            // Check if the user is teller user and he needs to change the password 
            // forcefully (as support user has been changed his password)
            if($isTellerUser == 1 && $tellerProfile->getFirst()->getChangePasswordRequired() == '1'){
                $tellerProfile->getFirst()->setChangePasswordRequired(false);
                $tellerProfile->getFirst()->save();

                // Using epActionAuditPlugin to keep track the changes of the password
                $userDetailsObj = $this->getUser()->getGuardUser();
                $applicationArr = array();//new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_TELLERINFO,$name,$userId));
                $eventHolder = new EpAuditEventHolder(
                    EpAuditEvent::$CATEGORY_SECURITY,
                    EpAuditEvent::$SUBCATEGORY_SECURITY_PASSWORD_CHANGE,
                    EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_SECURITY_RESET_PASSWORD_FORCEFULLY, array()),
                    $applicationArr);
                $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
                // End of epActionAuditPlugin
                
                //Logout the user so that he can logged in with thhe new changed password in
                // case when support user reset the password of teller only
                 $sessionTableObj = Doctrine::getTable('AppSessions')->findByUserId($user_id);
                 $sessionTableObj->delete();
            }
            
            if ($redirect) $this->forward('userAdmin', 'edit');
            return true;
        }
        //sfContext::getInstance()->getLogger()->info("Form invalid");
        return false;
    }

    public function executeResetPassword(sfWebRequest $request) {

        $this->form = new ResetUserForm();
        $this->form->bind($request->getParameter($this->form->getName()));
        if ($this->form->isValid()) {
            //$user->setPassword("password");
            //$user->save();
            $this->getUser()->setFlash('notice', "Password Has Been Reset Successfully", true);
            $this->redirect($this->getModuleName() . "/resetPassword");
        }
    }

    public function executeRemindPassword(sfWebRequest $request) {
        //$this->setTemplate('password');
    }

    /**
     * Varun : Bank functioanlity added to view bank list , add new bank and update existing bank details
     * @param sfWebRequest $request * */
    public function executeViewBank(sfWebRequest $request) {
        $parameters = $request->getParameterHolder()->getAll();
        $helperObj = new pay4billHelper();
        $parameters = $helperObj->trimArray($parameters);

        $offset = $request->getParameter('offset');

        $this->merchant_name = $request->hasParameter('merchant_name') ? $parameters['merchant_name'] : '';
        $this->bank_name = $request->hasParameter('bank_name') ? $parameters['bank_name'] : '';

        $this->merchantLogin = false;
        $this->customerLogin = false;

        if (pay4billHelper::checkIfCustomerUser($this->getUser()->getGuardUser()->getGroups()->toArray())) {
            $this->customerLogin = true;
        } else if (pay4billHelper::checkIfMerchantUser($this->getUser()->getGuardUser()->getGroups()->toArray())) {
            $this->banks = Doctrine::getTable('Bank')->findByMerchantId($this->getUser()->getGuardUser()->getId());
            $this->merchantLogin = true;
        }
        //Manu : Admin View and Edit Bank
        if (in_array('administrator', $this->getUser()->getGuardUser()->getGroupNames()) != false) {
//          $this->banks = Doctrine::getTable('Bank')->findAll();
//          $this->banks = Doctrine::getTable('Bank')->getAllBankInfo('','',$ban_name, $com_name);
            $this->banks = Doctrine::getTable('Bank')->findAllBank('', '', $this->bank_name, $this->merchant_name);
            $this->adminLogin = true;
        } else {
            $this->adminLogin = false;
        }

        $total_no_of_records = count($this->banks);

        if ($total_no_of_records > sfConfig::get('app_records_per_page')) {
            $this->actionTobeCalled = $this->getActionName();
            $this->total_no_of_records = $total_no_of_records;
            $this->no_of_records_per_page = sfConfig::get('app_records_per_page');
            $this->setPagination = true;
            if (!$request->hasParameter('offset')) {
                $offset = 0;
            } else {
                $this->getUser()->getAttributeHolder()->remove('msg_invoice');
                $offset = $request->getParameter('offset');
            }
            $this->current_offset = $offset;
            if ($this->merchantLogin) {
                $this->banks = Doctrine::getTable('Bank')->findMerchantId($this->getUser()->getGuardUser()->getId(), $offset, sfConfig::get('app_records_per_page'));
            } else {//Manu : In case of admin
                $this->banks = Doctrine::getTable('Bank')->findAllBank('', '', $this->bank_name, $this->merchant_name);
            }
            $this->actionTobeCalled = $this->getActionName();
            $this->module = $this->getModuleName();

            $this->offset_last = (($total_no_of_records % sfConfig::get('app_records_per_page') == 0 ? ($total_no_of_records - sfConfig::get('app_records_per_page') ) : $total_no_of_records - ($total_no_of_records % sfConfig::get('app_records_per_page'))));

            $this->offset_next = (($offset + sfConfig::get('app_records_per_page')) > $this->offset_last ? $this->offset_last : ($offset + sfConfig::get('app_records_per_page')));

            $this->offset_prev = (($offset - sfConfig::get('app_records_per_page')) < 0 ? 0 : $offset - sfConfig::get('app_records_per_page'));
        } else {
            $this->setPagination = false;
        }
        $this->addClassTo = $this->getActionName();

//    }
    }

    public function executeNewBank(sfWebRequest $request) {
        $this->getUser()->setAttribute('msg', '');
        $this->form = new BankForm();
        $this->addClassTo = $this->getActionName();
        $this->merchantId = $this->getUser()->getGuardUser()->getId();
    }

    public function executeSaveBank(sfWebRequest $request) {
        $this->form = new BankForm();
        if ($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT)) {
            $this->form->bind($request->getPostParameter($this->form->getName()));
            $bank_name = $request->getParameter('bank_name');
            $bank_branch = $request->getParameter('bank_branch');
            $account_name = $request->getParameter('account_name');
            $account_number = $request->getParameter('account_number');
            $sort_code = $request->getParameter('sort_code');
            $merchant_id = $this->getUser()->getGuardUser()->getId();

            $bankForMerchant = Doctrine::getTable('Bank')->findBankForMerchantId($this->getUser()->getGuardUser()->getId(), $bank_name, '');
            if ($bankForMerchant == 0) {
                $addBank = new Bank();
                $addBank->setBankName($bank_name);
                $addBank->setBankBranch($bank_branch);
                $addBank->setAccountName($account_name);
                $addBank->setAccountNumber($account_number);
                $addBank->setSortCode($sort_code);
                $addBank->setMerchantId($merchant_id);
                $addBank->save();
                $this->getUser()->setFlash('notice', 'Bank Successfully Added');
                $this->redirect('userAdmin/viewBank');
            } else {
                $bank_name = ucwords($bank_name);
                $this->getUser()->setFlash('notice', "Bank named <b>$bank_name</b> already exist. Please try another Bank. ");
                $this->redirect('userAdmin/newBank');
            }
        }
//        $this->processForm($request, $this->form);
    }

    public function executeEditBank(sfWebRequest $request) {
        //Varun : Decrypt id
        $this->id = cryptString::decrypt($request->getParameter('id'));
        $this->bank = Doctrine::getTable('Bank')->find($this->id);
        $this->form = new BankForm($this->bank);
        $this->addClassTo = $this->getActionName();
        $this->merchantId = $this->getUser()->getGuardUser()->getId();
        if (in_array('administrator', $this->getUser()->getGuardUser()->getGroupNames()) != false) {
            $this->adminLogin = true;
        } else {
            $this->adminLogin = false;
        }
    }

    public function executeUpdateBank(sfWebRequest $request) {
        $this->form = new BankForm();
        if ($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT)) {
            $this->form->bind($request->getPostParameter($this->form->getName()));
            $bank_name = $request->getParameter('bank_name');
            $bank_branch = $request->getParameter('bank_branch');
            $account_name = $request->getParameter('account_name');
            $account_number = $request->getParameter('account_number');
            $sort_code = $request->getParameter('sort_code');
            $id = $request->getParameter('hdn_id');
            $bankForMerchant = Doctrine::getTable('Bank')->findBankForMerchantId($this->getUser()->getGuardUser()->getId(), $bank_name, $id);
            if ($bankForMerchant == 0) {
                $update = Doctrine::getTable('Bank')->updateById($id, $bank_name, $bank_branch, $account_name, $account_number, $sort_code);
                $this->getUser()->setFlash('notice', 'Bank Details Successfully Updated');
                $this->redirect('userAdmin/viewBank');
            } else {
                $bank_name = ucwords($bank_name);
                $this->getUser()->setFlash('notice', "Bank named <b>$bank_name</b> already exist. Please try another Bank. ");
                $this->redirect('userAdmin/editBank?id=' . $id);
            }
        }
//        $this->processForm($request, $this->form);
    }

    /**
      Varun: End Bank functionality */
    /*
     * For customer to merchant and Vice versa
     * Created Date: 24 Apr 2012
     * Modified date; 25 apr 2012(add the new group permissions)
     * By : Santosh Kumar
     */
    public function executeSignInInfo($request) {
        if ($_SESSION['logedin_user_group'] == 'merchant') {
            $user = $this->getUser();
            $user->clearCredentials();
            $user->removeCredential(sfConfig::get('app_merchant_permission'));
            foreach (sfConfig::get('app_customer_permission') as $valCredentail) {
                $user->addCredentials($valCredentail);
            }
            unset($_SESSION['logedin_user_group']);
            $_SESSION['logedin_user_group'] = 'customer';
            $this->redirect('invoice/index?getExpired=false');
        } elseif ($_SESSION['logedin_user_group'] == 'customer') {
            $user = $this->getUser();
            $user->clearCredentials();
            $user->removeCredential(sfConfig::get('app_customer_permission'));
            foreach (sfConfig::get('app_merchant_permission') as $valCredentail) {
                $user->addCredentials($valCredentail);
            }
            unset($_SESSION['logedin_user_group']);
            $_SESSION['logedin_user_group'] = 'merchant';
        }
        $this->redirect('mypages/showPermissions');
    }

}
