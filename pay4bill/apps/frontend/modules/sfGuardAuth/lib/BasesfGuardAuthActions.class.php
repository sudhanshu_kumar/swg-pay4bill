<?php

/*
 * This file is part of the symfony package.
 * (c) 2004-2006 Fabien Potencier <fabien.potencier@symfony-project.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 *
 * @package    symfony
 * @subpackage plugin
 * @author     Fabien Potencier <fabien.potencier@symfony-project.com>
 * @version    SVN: $Id: BasesfGuardAuthActions.class.php 23800 2009-11-11 23:30:50Z Kris.Wallsmith $
 */
class BasesfGuardAuthActions extends sfActions {

    public function executeSignin($request) {

        $user = $this->getUser();

        $username = $request->getParameter('signin');
//        Varun : Check for disabled Merchant.
        if (isset($username['username'])) {
            $checkDisabled = Doctrine::getTable('sfGuardUser')->findByUsername($username['username']);
        }

        if (isset($checkDisabled[0]['id']) && $checkDisabled[0]['id'] != '') {
            $group = Doctrine::getTable('sfGuardUserGroup')->findByUserId($checkDisabled[0]['id']);
            $groupId = $group[0]['group_id'];
            /*
             *  by Santosh Kumar
             *  on 12 th Apr 2012
             * To put the user role in session variable
             */
            $this->getUser()->setAttribute('user_group', $group[0]['group_id']);

            $isActive = $checkDisabled[0]['is_active'];
            if (isset($isActive) && isset($groupId)) {
                if ($isActive != '1' && $groupId == '1') { 
                    $this->hideError = false;
                    $this->getUser()->setFlash('notice', "User is disabled. Kindly contact the administrator.", false);
                } else if ($isActive != '1' && $groupId == '6') { //Teller
                    $this->hideError = false;
                    $this->getUser()->setFlash('notice', "User is disabled. Kindly contact the administrator.", false);
                }else if ($isActive != '1' && ($groupId == '4' || $groupId == '5')) { //Subadmin
                    $this->hideError = false;
                    $this->getUser()->setFlash('notice', "User is disabled. Kindly contact the administrator.", false);
                }
            }
        }

        if ($user->isAuthenticated()) {


            foreach ($user->getGroups()->toArray() as $group_array) {
                switch ($group_array['id']) {
                    //Varun : Changes for Report and Merchant Admin

                    case 7:

                        return $this->redirect('@support_user_login');
                        break;
                      /*@Aditi- added for sign in as  new role of teller
                         *  20- jun 2012
                                  */
                    case 6:

                        return $this->redirect('@teller_login');
                        break;
                    /*****************/
                    case 5:
                        return $this->redirect('@merchant_admin');
                        break;
                    case 4:
                        return $this->redirect('@report_admin');
                        break;
                    case 3:
                        return $this->redirect('@admin_login');
                        break;
                    case 2:
                        return $this->redirect('@customer_login');
                        break;
                    case 1:
                        return $this->redirect('@merchant_login');
                        break;
                }
            }
        }

        $class = sfConfig::get('app_sf_guard_plugin_signin_form', 'sfGuardFormSignin');
        $this->form = new $class();

        if ($request->isMethod('post')) {
            $this->form->bind($request->getParameter('signin'));
            if ($this->form->isValid()) {
                $values = $this->form->getValues();
                $this->getUser()->signin($values['user'], array_key_exists('remember', $values) ? $values['remember'] : false);

                // always redirect to a URL set in app.yml
                // or to the referer
                // or to the homepage
                $signinUrl = sfConfig::get('app_sf_guard_plugin_success_signin_url', $user->getReferer($request->getReferer()));
                $EpDBSessionDetailObj = new EpDBSessionDetail();

                $EpDBSessionDetailObj->setUserSessionData();
                foreach ($this->getUser()->getGroups()->toArray() as $group_array) {
                    switch ($group_array['id']) {
                        /*@Saurabh - Added new role Support User*/
                        case 7:
                            return $this->redirect('@support_user_login');
                            break;
                        /*@Aditi- added for sign in as  new role of teller
                         *  20- jun 2012
                                  */
                        case 6:
                            return $this->redirect('@teller_login');
                            break;
                        /*-------------*/
                        case 5:
                            return $this->redirect('@merchant_admin');
                            break;
                        case 4:
                            return $this->redirect('@report_admin');
                            break;
                        case 3:
                            return $this->redirect('@admin_login');
                            break;
                        case 2:
                            return $this->redirect('@customer_login');
                            break;
                        case 1:
                            return $this->redirect('@merchant_login');
                            break;
                    }
                }
            }
        } else {
            if ($request->isXmlHttpRequest()) {
                $this->getResponse()->setHeaderOnly(true);
                $this->getResponse()->setStatusCode(401);

                return sfView::NONE;
            }

            // if we have been forwarded, then the referer is the current URL
            // if not, this is the referer of the current request
            $user->setReferer($this->getContext()->getActionStack()->getSize() > 1 ? $request->getUri() : $request->getReferer());

            $module = sfConfig::get('sf_login_module');
            if ($this->getModuleName() != $module) {
                return $this->redirect($module . '/' . sfConfig::get('sf_login_action'));
            }

            $this->getResponse()->setStatusCode(401);
        }
    }

    public function executeSignout($request) {
        $EpDBSessionDetailObj = new EpDBSessionDetail();

        $EpDBSessionDetailObj->doCleanUpSessionByUser();
        $this->getUser()->signOut();

        $signoutUrl = sfConfig::get('app_sf_guard_plugin_success_signout_url', $request->getReferer());
        //echo "<pre>";
        //print_r($signoutUrl);
        //exit;
        //    $this->redirect('' != $signoutUrl ? $signoutUrl : '@homepage');
        $this->redirect('@homepage');
    }

    public function executeSecure($request) {
        $this->getResponse()->setStatusCode(403);
    }

    public function executePassword($request) {
        throw new sfException('This method is not yet implemented.');
    }

}
