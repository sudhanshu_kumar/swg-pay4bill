<?php use_helper('I18N') ?>
    <?php $sf = sfContext::getInstance()->getUser();
    //Varun : Flash message displayed for Disabled Merchant
if($sf->hasFlash('notice')){
  ?><div id="flash_notice" class="error_serverside" ><?php
  echo nl2br($sf->getFlash('notice'));

  ?></div><?php }?>
<form action="<?php echo url_for('@sf_guard_signin') ?>" method="post">
  <div class="FormRepeatArea">
  <div class="FormLabel"> <?php echo $form['username']->renderLabel(); ?> </div>
  <div class="FromInputLabe5"> <?php echo $form['username']->render(); ?> <?php if(!$sf->hasFlash('notice')){ echo $form['username']->renderError(); } ?>  </div>
  <!--<div class="FormError"></div>-->
  <div class="clear_new"></div>
  </div>

  <div class="FormRepeatArea">
  <div class="FormLabel"> <?php echo $form['password']->renderLabel(); ?> </div>
  <div class="FromInputLabe5"> <?php echo $form['password']->render(); ?><?php echo $form['password']->renderError(); ?> </div>
  <!--<div class="FormError">  </div>-->
  <div class="clear_new"></div>
  </div>
   <div class="FormRepeatArea">
  <?php echo $form['_csrf_token']; ?>
  <div class="btn_section">
  <!--Manu : Hide of custemer and merchant options -->
    <!--<input type = 'Radio' Name ='customer' value= 'customer' checked >Customer
    <input type = 'Radio' Name ='customer' value= 'merchant' >Merchant-->
    <input  type="submit" value="<?php echo __('Log-In', null, 'sf_guard') ?>" class="submit" />
    <a href="<?php echo url_for('@sf_guard_forgot_password') ?>">Forgot your password?</a> </div>
    </div>
</form>
