<script>
function validateForm()
    {
      var err  = 0;
      var invoice_number = jQuery.trim($('#invoice_number').val());
      var validation_number = jQuery.trim($('#validation_number').val());
      var reg = /^[A-Za-z0-9]*$/; //allow alphabet and numbers only...
      if(invoice_number!='')
      {
      if(reg.test(invoice_number) == false) {
        $('#invoice_number_error').html("Please enter only Alphanumeric/Numerical characters.");
        $('#invoice_number').focus();
        $('#flash_notice').hide(); // Varun : Bug fix; FS#34882
        err = err + 1;
      } else {
        $('#invoice_number_error').html("&nbsp;");
    }
   } else {
        $('#invoice_number_error').html("Please enter Invoice Number.");
        $('#flash_notice').hide();// Varun : Bug fix; FS#34882
        err = err + 1;
   }
      if(validation_number!='')
      {
      if(reg.test(validation_number) == false) {
        $('#validation_number_error').html("Please enter only Alphanumeric/Numerical characters.");
        $('#validation_number').focus();
        $('#flash_notice').hide();// Varun : Bug fix; FS#34882
        err = err + 1;
      } else {
        $('#validation_number_error').html("&nbsp;");
    }
   } else {
       $('.thAlRigth .redLbl span').html('');
       $('#signin_username').val('');
       $('#signin_password').val('');

        $('#validation_number_error').html("Please enter Validation Number.");
        $('#flash_notice').hide();// Varun : Bug fix; FS#34882
        err = err + 1;
   }
    if(err>0)
            {
                return false;
            }
        return true;
   }
    $(document).ready(function() {
  $("table.thAlRigth td.redLbl span").removeClass('error_list');
    });
</script>
<?php use_helper('I18N') ?>
<?php $sf = sfContext::getInstance()->getUser();
if($sf->hasFlash('notice')){
  ?>
<div id="flash_notice" class="error_serverside" >
  <?php
  echo nl2br($sf->getFlash('notice'));

  ?>
</div>
<?php }?>
<div id="wrapperLeft">
  <div id="slides"> <?php echo image_tag('slide_01.jpg',array("height"=>"288","width"=>"610")); ?> <?php echo image_tag('slide_02.jpg',array("height"=>"288","width"=>"610" )); ?> <?php //echo image_tag('slide_03.jpg',array("height"=>"288","width"=>"610" )); ?> <?php echo image_tag('slide_04.jpg',array("height"=>"288","width"=>"610" )); ?> </div>
  <div id="slideControl"></div>
  <div class="clearfix"></div>
  <div class="largeShowcase whyUs">
    <div class="top"></div>
    <div class="middle">
      <div>
        <h3>Why us?</h3>
      </div>
      <p>pay4bill is a Billing Solution that enables companies and
        institutions present bills to their customers in simplified and
        transparent manner. <br>
        <br>
        The solution allows customers to make payments on the
        presented bills in a manner that assures that payment is validated and
        the company and/or institution receives its funds.<br>
      </p>
      <p class="more right pdR10"><a href="<?php //echo site_url() ; ?>mypages">read more </a> </p>
      <div class="clearfix"></div>
    </div>
    <div class="bottom"></div>
  </div>
  <div class="largeShowcase margR10 MrgT15">
    <div class="top"></div>
    <div class="middle">
      <div>
        <h3>Merchant Benefits</h3>
      </div>
      <div class="list">Multiple payment options</div>
      <div class="list">No more billing issues</div>
      <div class="list">A paperless solution that saves cost</div>
      <div class="list">Multiple channels for delivering  bills</div>
      <div class="list">Invoice/Bill Archiving
        <!--: No need to setup your own database-->
      </div>
      <div class="list">Multiple record trails for your bill
        <!-- presentment-->
      </div>
      <div class="list">Tie your bills/invoices to payments with no reconciliation issues </div>
     <div style="height:15px;">&nbsp;</div>
    </div>
    <div class="bottom"></div>
  </div>
  <div class="largeShowcase MrgT15">
    <div class="top"></div>
    <div class="middle">
      <div>
        <h3>Services We Provide</h3>
      </div>
      <div class="listNew">Recurring Bill Presentment</div>
      <div class="listNew">One-time Bill Presentment</div>
      <div class="listNew">Custom Bill/Invoicing Management Solutions</div>
      <div class="listNew">Payment Collection through our Payment</div>
      <div class="listNew">Aggregation Platform</div>
      <div class="listNew">Payment Tracing</div>
      <div class="listNew">Bill Archiving and Customizable organization</div>
      <div class="listNew">Wide-reaching payment network</div>
     <div style="height:27px;">&nbsp;</div>
    </div>
    <div class="bottom"></div>
  </div>
</div>
<div id="wrapperRight">
  <div class="blueShowcase">
    <div class="top"></div>
    <div class="middle">
      <div id="merchantLogin" class="mainLogin">
        <div>
          <h3 class="login_h3">Login to your account</h3>
        </div>
        Log in  to view your bills/invoices using your Email/Username.
        Get statistics on paid bills and more.
        <form action="<?php echo url_for('@sf_guard_signin') ?>" method="post">
          <table class="thAlRigth">
            <tbody>
             <tr><td colspan="2" height="10"></td></tr>
              <tr>
                <th><label for="signin_username"><?php echo $form['username']->renderLabel(); ?></label></th>
                <td><?php echo $form['username']->render(); ?> </td>
              </tr>
              <tr>
                 <td>&nbsp;</td>
                <td class="redLbl" ><?php if(!$sf->hasFlash('notice')){ echo $form['username']->renderError(); } ?></td>
              </tr>
              <tr>
                <th><label for="signin_password"><?php echo $form['password']->renderLabel(); ?></label></th>
                <td><?php echo $form['password']->render(); echo $form->renderHiddenFields(); ?></td>
              </tr>
              <tr>
                 <td>&nbsp;</td>
                  <td class="redLbl"><?php echo $form['password']->renderError(); ?></td>
              </tr>
             
            </tbody>
            <tfoot>
              <tr>

                <td colspan="2"><input  type="submit" value="<?php echo __('Log-In', null, 'sf_guard') ?>" class="submit2"/> 
                    <a href="<?php echo url_for('@sf_guard_forgot_password') ?>" class="password"> Forgot your password?</a>                    
                </td>
              </tr>
               <tr><td colspan="2" height="5"></td></tr>
            </tfoot>
          </table>
        </form>
      </div>
    </div>
    <div class="bottom"></div>
  </div>
  <div class="registerShowcase">
    <div class="top"></div>
    <div class="middle">
      <h3>Sign Up</h3>
      <div class="user_info">I am a Customer</div>
      <div class="tmzInfo"><span>1</span> Simply <a href="<?php echo url_for('@online_customer_registration_link') ?>">click here</a> to Register </div>
      <div class="tmzInfo">Associate customer profile(s) to your User ID</div>
       <div class="Sap"></div>
      <div class="user_info">I am a Merchant</div>
      <div class="tmzInfo"><span>1</span> Simply <a href="<?php echo url_for('merchantRegistration/new'); ; ?>">click here</a> to Register </div>
       <div class="tmzInfo"><span>2</span> <a href="<?php echo url_for('merchantRegistration/validateForLogin'); ; ?>">Activate your Account</a> to Access the &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Portal </div>
    </div>
    <div class="bottom"></div>
  </div>
  <br clear="all"/>

 <!-- <div id="merchantLogin"> </div>-->
  <div class="redShowcase">
    <div class="top"></div>
    <div class="middle" style="padding-left:4px;">
      <div>
        <h3>Print Paid Invoice</h3>
      </div>
      
      <!--
                 Varun : Fields added for Invoice and Validation number
                 -->
      <form action="<?php echo url_for('home/viewInvoice') ?>" method="post" onsubmit="return validateForm()">
        <table width="100%" class="formGrid form1 mg0">
        <tr>
        <td colspan="2"><?php $sf = sfContext::getInstance()->getUser();
                    if($sf->hasFlash('noticePaid')){
                      ?>
      <div id="flash_notice" class="" >
        <?php
                      echo nl2br($sf->getFlash('noticePaid'));

                      ?>
      </div>
      <?php }?></td>
        </tr>
         <tr><td colspan="2">&nbsp;</td></tr>
          <tr>
            <td align="right"><font color="white" style="font-size:10px">Invoice Number</font><font class="Yellow">*</font></td>
            <td><input type="text" id="invoice_number" name="invoice_number" style="width:142px;" class="inputTxt2" autocomplete="off" oncopy="return false" ondrag="return false"/></td>
          </tr>
          <tr>
        <td></td>
             <td  id="invoice_number_error" class="Yellow" valign="middle" nowrap style="font-size:10px">&nbsp;</td>
          </tr>
          <tr>
            <td align="right" nowrap="nowrap"><font color="white" style="font-size:10px">Validation Number</font><font  class="Yellow">*</font></td>
            <td><input type="text" id="validation_number" style="width:142px;" name="validation_number" class="inputTxt2" autocomplete="off" oncopy="return false" ondrag="return false"/></td>
          </tr>
          <tr>
            <td></td>
            <td  id="validation_number_error"  class="Yellow" valign="middle" nowrap style="font-size:10px">&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td ><input type="submit" value="<?php echo __('Print', null, 'sf_guard') ?>" class="submit23" /></td>
          </tr>
        </table>
      </form>
    </div>
    <div class="bottom"></div>
  </div>
</div>
<br clear="all" />
 