<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class merchantActions extends sfActions {
    /* Varun
     * Action : Tax
     * Function to show tax list to merchant
     */

    public function executeTax(sfWebRequest $request) {
        $this->getUser()->setAttribute('msg', '');
//        if (in_array('merchant', $this->getUser()->getGuardUser()->getGroupNames()) != false) {
        if (isset($_SESSION['logedin_user_group']) && $_SESSION['logedin_user_group'] == 'merchant') {
            $this->tax = Doctrine::getTable('tax')->findTaxList('', '', '1');

            $this->merchantId = $this->getUser()->getGuardUser()->getId();
            $this->merchantLogin = true;
        } else {
            $this->merchantLogin = false;
        }

        $total_no_of_records = count($this->tax);
        if ($total_no_of_records > sfConfig::get('app_records_per_page')) {
            $this->actionTobeCalled = $this->getActionName();
            $this->total_no_of_records = $total_no_of_records;
            $this->no_of_records_per_page = sfConfig::get('app_records_per_page');
            $this->setPagination = true;
            if (!$request->hasParameter('offset')) {
                $offset = 0;
            } else {

                $this->getUser()->getAttributeHolder()->remove('msg_invoice');
                $offset = $request->getParameter('offset');
            }
            $this->current_offset = $offset;
            if ($this->merchantLogin) {
                $this->tax = Doctrine::getTable('tax')->findTaxList($offset, sfConfig::get('app_records_per_page'), '1');
            } else {
                $this->tax = '';
            }
            $this->actionTobeCalled = $this->getActionName();
            $this->module = $this->getModuleName();

            $this->offset_last = (($total_no_of_records % sfConfig::get('app_records_per_page') == 0 ? ($total_no_of_records - sfConfig::get('app_records_per_page') ) : $total_no_of_records - ($total_no_of_records % sfConfig::get('app_records_per_page'))));

            $this->offset_next = (($offset + sfConfig::get('app_records_per_page')) > $this->offset_last ? $this->offset_last : ($offset + sfConfig::get('app_records_per_page')));

            $this->offset_prev = (($offset - sfConfig::get('app_records_per_page')) < 0 ? 0 : $offset - sfConfig::get('app_records_per_page'));
        } else {
            $this->setPagination = false;
        }
        $this->addClassTo = $this->getActionName();
        $this->taxes = $this->tax;
    }

    /* Varun
     * Action : UpdateTax
     * Function to update tax value for merchant
     */

    public function executeUpdateTax(sfWebRequest $request) {

        // Varun : Id decrypted
        $this->id = cryptString::decrypt($request->getParameter('taxId'));
        $this->merchantId = cryptString::decrypt($request->getParameter('merchantId'));
        $this->edit = $request->getParameter('edit');
        $taxId = Doctrine::getTable('merchanttax')->findEntryForMerchantTax($this->id, $this->merchantId);
        if (count($taxId->toArray()) > 0) {
            $this->tax = Doctrine::getTable('merchanttax')->find($taxId[0]['id']);
            if ($this->tax != '') {
                if (is_array($this->tax->toArray())) {
                    $this->exist = $this->tax->toArray();
                    $this->taxname = Doctrine::getTable('tax')->find($this->exist['tax_id']);
                    $this->taxname = $this->taxname->toArray();
                }
            } else {
                $this->exist = array();
            }
        } else {
            $this->tax = '';
            $this->exist = array();
        }
        $this->form = new MerchantTaxForm($this->tax);
        $this->addClassTo = $this->getActionName();
    }

    /* Varun
     * Action : SaveTax
     * Function to save tax value for merchant
     */

    public function executeSaveTax(sfWebRequest $request) {
        $this->form = new MerchantTaxForm();
        if ($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT)) {
            $this->form->bind($request->getPostParameter($this->form->getName()));

            $tax = $request->getParameter('merchant_tax');
            $tax_value = $tax['tax_value'];
            $tax_id = $request->getParameter('tax_id');
            $merchant_id = $request->getParameter('merchant_id');

            $taxExist = Doctrine::getTable('merchanttax')->findEntryForMerchantTax($tax_id, $merchant_id);
            $taxExist = count($taxExist);
            if ($taxExist == 0) {
                $addTax = new MerchantTax();
                $addTax->setTaxId($tax_id);
                $addTax->setMerchantId($merchant_id);
                $addTax->setTaxValue($tax_value);
                $addTax->save();
                $this->getUser()->setFlash('notice', 'Tax Details Successfully Updated');
                $this->redirect('merchant/tax');
            } else {
                $taxExist = Doctrine::getTable('merchanttax')->findEntryForMerchantTax($tax_id, $merchant_id);
                $addTax = Doctrine::getTable('merchanttax')->find($taxExist[0]['id']);
                $addTax->setTaxId($tax_id);
                $addTax->setMerchantId($merchant_id);
                $addTax->setTaxValue($tax_value);
                $addTax->save();
                $this->getUser()->setFlash('notice', 'Tax Details Successfully Updated');
                $this->redirect('merchant/tax');
            }
        }
//        $this->processForm($request, $this->form);
    }

}

?>
