<script>
<?php if(isset($successMsg)){?>
                $('#success_msg').html('<?php echo $successMsg; ?>');
                $('#success_msg').addClass('msg_serverside');


            <?php } ?>
$(document).ready(function(){
        $('#Tax').addClass('active');
         });
  function validateForm(){
        var err  = 0;
        var tax_value = jQuery.trim($('#merchant_tax_tax_value').val());
        if(tax_value == '') {
                $('#tax_value_error').html("Please enter Tax Value.");
                $('#tax_value').focus();
                err = err + 1;
         } else {
        if(tax_value != '')
        {
             if(isNaN(tax_value)){
                 $('#tax_value_error').html("Please enter numeric Tax Value.");
                 $('#tax_value').focus();
                 err = err + 1;
             }else if(tax_value >= 100){
                $('#tax_value_error').html("Please enter numeric value between 0 to 99.99"); //Varun : Bug fix : FS#35006
                $('#tax_value').focus();
                err = err + 1;
            } else if(tax_value < 0){
                $('#tax_value_error').html("Tax Value should be greater than 0.");
                $('#tax_value').focus();
                err = err + 1;
            } else {
                $('#tax_value_error').html("");
            }
        }
        else
        {
            $('#tax_value_error').html("");
        }
      }       
        if(err>0)
            {
                return false;
            }
        return true;

    }
</script>
<div id="pgMenu">
        <div id="wrappernew">
            <div class="pgTitle">
                <h2>Update Tax Details</h2>
            </div>
        </div>
    </div>
<div class="clear_new"></div>
    <?php $sf = sfContext::getInstance()->getUser();
if($sf->hasFlash('notice')){
  ?><div id="flash_notice" class="error_serverside" ><?php
  echo nl2br($sf->getFlash('notice'));

  ?></div><?php }?>
<form name="frmTax" id="frmTax" action="<?php echo url_for('merchant/saveTax') ?>" method="post" >
<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>

  <?php if (!$form->getObject()->isNew()): ?>
  <input type="hidden" name="sf_method" value="put" />
  <?php endif; ?>
  <div class="sform">

      <h3>Update Tax Information</h3>
    <div class="clear_new"></div>
<?php
if(isset($taxname) && $taxname!='') {
include_partial('formTax', array('form' => $form,'exist'=>$exist,'taxname'=>$taxname));
 } else {
include_partial('formTax', array('form' => $form,'exist'=>$exist));
 } ?>
      </div>
  <input type="hidden" name="tax_id" id="tax_id" value="<?php echo $id; ?>">
  <input type="hidden" name="merchant_id" id="merchant_id" value="<?php echo $merchantId; ?>">
  <?php //echo $form ?>
</form>
