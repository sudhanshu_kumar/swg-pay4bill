<?php use_helper('ePortal') ?>
<script>
<?php if (isset($successMsg)) { ?>
        $('#success_msg').html('<?php echo $successMsg; ?>');
        $('#success_msg').addClass('msg_serverside');


<?php } ?>
    $(document).ready(function(){
        $('#ApproveRegistration').addClass('active');
    });
</script>
<?php if (!empty($msg)) {
?><div class="clear_new"></div> <?php } ?>

<div id="pgMenu">
    <div id="wrappernew">
        <div class="pgTitle">
            <h2>Approve Merchant</h2>
        </div>
        <!--div class="pgNav">
               <ul>
           </ul>
           </div-->
    </div>
</div>
<div id="mainnew">
    <?php
    $sf = sfContext::getInstance()->getUser();
    if ($sf->hasFlash('notice')) {
    ?><div id="flash_notice" class="msg_serverside" ><?php
        echo nl2br($sf->getFlash('notice'));
    ?></div><?php } ?>
    <span id="success_msg"></span>
    <div class="clear_new"></div>
    <div id="wrappernew">
        <div id="content"><div id="Formwraper">
                <h3>Pending Approval Merchant List</h3>
                <div class="TableForm">
                    <table>
                        <thead>

                            <tr>
                                <?php if (isset($adminLogin)) {
                                ?>
                                    <th width="25%">Email Address</th>
                                    <th width="25%">Merchant Name</th>
                                    <th width="15%">CAC Registration</th>
                                    <th width="15%">Contact Number</th>
                                    <th width="15%">Action</th>
                                <?php } ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                if (isset($pendingMerchant) && count($pendingMerchant) > 0) {
                            ?>
                            <?php
                                    $i = 0;
                                    foreach ($pendingMerchant as $merchantVal): $i++;
                                        if ($i % 2 != 0) {
                            ?>
                                        <tr class="odd">
                                    <?php } else { ?><tr class=""> <?php } ?>
<?php if ($adminLogin) { ?>
                                        <td><?php echo strlen($merchantVal->getsfGuardUser()->getEmailAddress())>30?substr($merchantVal->getsfGuardUser()->getEmailAddress(),0,28).'...':$merchantVal->getsfGuardUser()->getEmailAddress(); ?></td>
                                        <td><?php echo $merchantVal->getMerchantName() ?></td>
                                        <td><?php echo $merchantVal->getCacRegistration() ?></td>
                                        <td><?php echo $merchantVal->getMerchantContactNumber() ?></td>
                                        <td>
                                            <!-- Varun : Encrypt id  -->
<?php echo link_to(image_tag('Approve1.png'), 'merchantRegistration/approveMerchantRegistration?id=' . cryptString::encrypt($merchantVal->getId()) . '&byadmin=1', array('method' => 'get', 'class' => 'delete', 'title' => 'Approve Merchant', 'confirm' => 'Are you sure, you want to approve this merchant?')) ?>&nbsp;&nbsp;&nbsp;&nbsp;
                                        </td>
<?php } ?>
                                    </tr>
                            <?php endforeach; ?>
                            <?php
                                    } else {
                                        echo "<tr><td colspan='5' align='center'>No pending registration for approval exist</td></tr>";
                                    }
                            ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="nofoList">
                    <?php
                                    if ($adminLogin) {

                                        include_partial('global/records_number', array('amount_footer' => false, 'current_offset' => isset($current_offset) ? $current_offset : null, 'offset_next' => isset($offset_next) ? $offset_next : null, 'total_no_of_records' => isset($total_no_of_records) ? $total_no_of_records : count($pendingMerchant), 'no_of_records_per_page' => isset($no_of_records_per_page) ? $no_of_records_per_page : null, 'from' => '', 'total_amount_of_all_transactions' => isset($total_amount_of_all_transactions) ? $total_amount_of_all_transactions : null));
                                    }
                    ?></div>
                <?php if ($setPagination) {
 ?>
                                        <div align="right">
                    <?php
                                        if ($adminLogin) {
                                            include_partial('global/pagination', array('moduleName' => $module, 'amount_footer' => false, 'actionName' => $actionTobeCalled, 'current_offset' => $current_offset, 'offset_next' => $offset_next, 'offset_prev' => $offset_prev, 'offset_last' => $offset_last));
                                        }
                    ?>
                                    </div>
                <?php } ?>
            </div>
        </div></div></div>
