<?php use_helper('ePortal') ?>
<script>
<?php if (isset($successMsg)) { ?>
        $('#success_msg').html('<?php echo $successMsg; ?>');
        $('#success_msg').addClass('msg_serverside');


<?php } ?>
                $(document).ready(function(){
                    ('#Taxes').addClass('active');
                });
</script>
<div id="pgMenu">
    <div id="wrappernew">
        <div class="pgTitle">
            <h2>Taxes</h2>
        </div>
        <div class="pgNav">
            <ul>
                <!-- Varun : Bug fix FS#34955 -->
                <!-- Aditi : Bug fix FS#35618 -->
                <li class="current"><a href="<?php echo url_for('adminManagement/tax') ?>">View Taxes</a></li>
                <li class=""><a href="<?php echo url_for('adminManagement/newTax') ?>">Create Tax</a></li>
            </ul>
        </div>
    </div>
</div>
<?php if (!empty($msg)) {
?><div class="clear_new"></div> <?php } ?>
<div id="mainnew">
    <?php
    $sf = sfContext::getInstance()->getUser();
    if ($sf->hasFlash('notice')) {
    ?><div id="flash_notice" class="msg_serverside" ><?php
        echo nl2br($sf->getFlash('notice'));
    ?></div><?php } ?>
    <span id="success_msg"></span>
    <div class="clear_new"></div>
    <div id="wrappernew">
        <div id="content"><div id="Formwraper">
                <h3>View Taxes</h3>
                <div class="TableForm">
                    <table>
                        <thead>

                            <tr>
                                <?php if ($adminLogin) {
                                ?>
                                    <th width="15%">Tax Name</th>
                                    <th width="60%">Description</th>
                                    <th width="15%">Status</th>
                                    <th width="10%">Action</th>
                                <?php } ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                if (count($taxes) > 0) {

                                    $index_for_getting_invoice_amount = 0;
                            ?>
                            <?php $i = 0;
                                    foreach ($taxes as $tax): $i++ ?>  <!-- Varun :Bug Fix 35012 -->
<?php if ($i % 2 != 0) { ?>
                                            <tr class="odd">
                                <?php } else {
 ?><tr class=""> <?php } ?>
<?php if ($adminLogin) { ?>
                                            <td><?php echo $tax->getTaxName() ?></td>
                                            <td><?php echo $tax->getDescription() ?></td>
                                            <td><?php if ($tax->getStatus() == '1') {
                                                echo "Active";
                                            } else {
                                                echo "Inactive";
                                            } ?></td>
                                            <td>
                                                <!-- Varun : Encrypt id  -->
                            <?php echo link_to(image_tag('tdEdit.png'), 'adminManagement/editTax?id=' . cryptString::encrypt($tax->getId()) . '&edit=1', array('method' => 'get', 'class' => 'editInfo', 'title' => 'Edit')) ?>&nbsp;&nbsp;&nbsp;&nbsp;
                                                </td>
                            <?php } ?>
                                        </tr>
                            <?php endforeach; ?>
<?php
                                    } else {
                                        echo "<tr><td colspan='4' align='center'>No Tax Found</td></tr>";
                                    }
?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="nofoList">
                    <?php
                                    if ($adminLogin) {

                                        include_partial('global/records_number', array('amount_footer' => false, 'current_offset' => isset($current_offset) ? $current_offset : null, 'offset_next' => isset($offset_next) ? $offset_next : null, 'total_no_of_records' => isset($total_no_of_records) ? $total_no_of_records : count($taxes), 'no_of_records_per_page' => isset($no_of_records_per_page) ? $no_of_records_per_page : null, 'from' => '', 'total_amount_of_all_transactions' => isset($total_amount_of_all_transactions) ? $total_amount_of_all_transactions : null));
                                    }
                    ?></div>
<?php
                                    if ($setPagination) {
?>
                                    <div align="right">
                    <?php
                                        if ($adminLogin) {
                                            include_partial('global/pagination', array('moduleName' => $module, 'amount_footer' => false, 'actionName' => $actionTobeCalled, 'current_offset' => $current_offset, 'offset_next' => $offset_next, 'offset_prev' => $offset_prev, 'offset_last' => $offset_last, 'search' => '', 'from' => '', 'to' => '', 'date_cat' => ''));
                                        }
                    ?> </div>
<?php
                                    }
?>

            </div>
        </div></div></div>
