<?php use_helper('ePortal') ?>
<script>
<?php if (isset($successMsg)) { ?>
        $('#success_msg').html('<?php echo $successMsg; ?>');
        $('#success_msg').addClass('msg_serverside');


<?php } ?>
    $(document).ready(function(){
        $('#SubAdmins').addClass('active');
    });
</script>
<div id="pgMenu">
    <div id="wrappernew">
        <div class="pgTitle">
            <h2>View SubAdmins</h2>
        </div>
        <div class="pgNav">
            <ul>
                <li class="current"><a href="<?php echo url_for('adminManagement/viewAdmin') ?>">View Sub Admins</a></li>
                <li class=""><a href="<?php echo url_for('adminManagement/newAdmin') ?>">Create Sub Admin</a></li>
            </ul>
        </div>
    </div>
</div>
<?php if (!empty($msg)) {
?><div class="clear_new"></div> <?php } ?>
<div id="mainnew">
    <?php
    $sf = sfContext::getInstance()->getUser();
    if ($sf->hasFlash('notice')) {
    ?><div id="flash_notice" class="msg_serverside" ><?php
        echo nl2br($sf->getFlash('notice'));
    ?></div><?php } ?>
    <span id="success_msg"></span>
    <div class="clear_new"></div>
    <div id="wrappernew">
        <div id="content"><div id="Formwraper">
                <h3>Sub Admins</h3>
                <div class="TableForm">
                    <table>
                        <thead>

                            <tr>
                                <?php if ($adminLogin) {
                                ?><th>Name</th>
                                    <th>Username</th>
                                    <th>Email Address</th>

                                    <th>Permissions</th>
                                    <th>Action</th>
                                <?php } ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                if (count($adminList) > 0) {

                                    $index_for_getting_invoice_amount = 0;
                            ?>
                            <?php $i = 0;
                                    foreach ($adminList as $admin): $i++ ?>  <!-- Varun :Bug Fix 35012 -->
                            <?php if ($i % 2 != 0) {
                            ?>
                                            <tr class="odd">
                                <?php } else {
                                ?><tr class=""> <?php } ?>
                                    <?php
                                        if ($adminLogin) {
                                            $test = $admin->toArray();
                                    ?><td><?php echo $admin->getFirstName() ?></td>
                                            <td><?php echo $admin->getUsername() ?></td>
                                            <td>
                                                <?php echo strlen($admin->getEmailAddress())>30?substr($admin->getEmailAddress(),0,28).'...':$admin->getEmailAddress(); ?>                                                    
                                                <?php //echo $admin->getEmailAddress() ?></td>

                                            <td><?php
                                            if (isset($test['sfGuardUserGroup'][0])) {
                                                if ($test['sfGuardUserGroup'][0]['group_id'] == '4') {
                                                    echo " Reports ";
                                                }
                                                if ($test['sfGuardUserGroup'][0]['group_id'] == '5') {
                                                    echo " Merchant";
                                                }
                                            }
                                            if (isset($test['sfGuardUserGroup'][1])) {
                                                if ($test['sfGuardUserGroup'][1]['group_id'] == '4') {
                                                    echo " Reports ";
                                                }
                                                if ($test['sfGuardUserGroup'][1]['group_id'] == '5') {
                                                    echo " Merchant ";
                                                }
                                            }
                                    ?></td>
                                        <td>
                                            <!-- Varun : Encrypt id  -->
                                    <?php echo link_to(image_tag('tdEdit.png'), 'adminManagement/editAdmin?id=' . cryptString::encrypt($admin->getId()) . '&edit=1', array('method' => 'get', 'class' => 'editInfo', 'title' => 'Edit')) ?>&nbsp;&nbsp;&nbsp;&nbsp;
                                    <?php
                                            if ($admin->getIsActive() == '0') {
                                                echo link_to(image_tag('disable.gif'), 'adminManagement/enableAdmin?id=' . cryptString::encrypt($admin->getId()), array('title' => 'Enable Admin', 'alt' => 'Enable Admin', 'class' => 'delete', 'confirm' => 'Are you sure you want to enable this admin?'));
                                            } else {
                                                echo link_to(image_tag('enable.gif'), 'adminManagement/disableAdmin?id=' . cryptString::encrypt($admin->getId()), array('title' => 'Disable Admin', 'alt' => 'Disable Admin', 'class' => 'delete', 'confirm' => 'Are you sure you want to disable this admin?'));
                                            }
                                    ?>
                                        </td>

                                <?php } ?>
                                    </tr>
                            <?php endforeach; ?>
                            <?php
                                    } else {
                                        echo "<tr><td colspan='5' align='center'>No Admin Found</td></tr>";
                                    }
                            ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="nofoList">
                    <?php
                                    if ($adminLogin) {
                                        include_partial('global/records_number', array('amount_footer' => false, 'current_offset' => isset($current_offset) ? $current_offset : null, 'offset_next' => isset($offset_next) ? $offset_next : null, 'total_no_of_records' => isset($total_no_of_records) ? $total_no_of_records : count($adminList), 'no_of_records_per_page' => isset($no_of_records_per_page) ? $no_of_records_per_page : null, 'from' => '', 'total_amount_of_all_transactions' => isset($total_amount_of_all_transactions) ? $total_amount_of_all_transactions : null));
                                    }
                    ?></div>
                <?php if ($setPagination) {
                ?>
                                        <div align="right">
                    <?php
                                        if ($adminLogin) {
                                            include_partial('global/pagination', array('moduleName' => $module, 'amount_footer' => false, 'actionName' => $actionTobeCalled, 'current_offset' => $current_offset, 'offset_next' => $offset_next, 'offset_prev' => $offset_prev, 'offset_last' => $offset_last, 'search' => '', 'from' => '', 'to' => '', 'date_cat' => ''));
                                        }
                    ?></div><?php } ?>

            </div>
        </div></div></div>
