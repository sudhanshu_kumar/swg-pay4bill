  <?php
               if(count($exist) > 0 ) {
               $exist = $sf_data->getRaw('exist');
               }
               ?>
<div class="clear_new"></div>

    <div class="TableForm">

      <table>
        <tr>
            <td width="35%" align="right">Tax Name<font color="red">*</font></td>
            <td><?php echo $form['tax_name']->render() ?><label id="tax_name_error" class="error"> <?php echo $form['tax_name']->renderError(); ?></label></td>
        </tr>
        <tr>
            <td align="right">Description<font color="red">*</font></td>
            <td><?php echo $form['description']->render() ?><label id="description_error" class="error">   <?php echo $form['description']->renderError(); ?></label></td>
        </tr>
       
        <?php if (is_array($exist)) { ?>
        <tr>
            <td align="right">Status<font color="red">*</font></td>
            <td>
                     <label for=""  class="check">
               <?php
               if (is_array($exist) && in_array('1', $exist) != false) { ?>
                   <input id="active" class="check" type="radio" name="status" value="1" checked>
                   <?php } else { ?><input id="active" class="check" type="radio" name="status" value="1">
                   <?php } ?>
                   Active</label>
               <label for=""  class="check"><?php if (is_array($exist) && in_array('0', $exist) != false) { ?>
                   <input  id="inactive" type="radio" name="status" value="0" checked>
                   <?php } else { ?><input  id="inactive" type="radio" name="status" value="0">
                   <?php } ?>Inactive</label>

                  <label id="status_error" class="error">   <?php echo $form['status']->renderError(); ?> </label>
                </td>
         </tr>

        <?php } else { ?>
         <tr>
            <td align="right"> Status<font color="red">*</font></td>
            <td>
                   <label for=""  class="check">
                     <input type="radio" name="status" id="active" value="1" checked/>&nbsp;Active&nbsp;&nbsp;
                     <input type="radio" name="status" id="inactive" value="0" />&nbsp;Inactive
                   </label>
            <label id="status_error" class="error">   <?php echo $form['status']->renderError(); ?> </label>
            </td>

         </tr>
        <?php } ?>
         <tr>
             <td></td>
             <td><?php echo $form['_csrf_token']; ?>
              <input class="blueBtn" type="submit" value="Submit" name="Submit" onclick="return validateForm()">
              <input class="blueBtn" type="button" value="Cancel" name="Cancel" onclick="window.location='<?php echo url_for("adminManagement/tax");?>';" >
             </td>
         </tr>
      </table>
    </div>
