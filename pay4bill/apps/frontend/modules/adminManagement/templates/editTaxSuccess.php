<script>
<?php if(isset($successMsg)){?>
                $('#success_msg').html('<?php echo $successMsg; ?>');
                $('#success_msg').addClass('msg_serverside');


            <?php } ?>
$(document).ready(function(){
        $('#Taxes').addClass('active');
         });
    function validateForm(){
        var err  = 0;
        var tax_name = jQuery.trim($('#tax_tax_name').val());

        if(tax_name == '') {
                $('#tax_name_error').html("Please enter Tax Name.");
                $('#tax_name').focus();
                err = err + 1;
         } else {
        if(tax_name != '')
        {
            if(validateName(tax_name) != 0){
            $('#tax_name_error').html("Please enter valid Tax Name.(Only 'alphabets , space, .' allowed.)");
                err = err+1;
            }else if(tax_name.length > 40){
                $('#tax_name_error').html("Tax Name should be less than or equal to 40 characters.");
                $('#tax_name').focus();
                err = err + 1;
            } else if(tax_name.length < 5){
                $('#tax_name_error').html("Tax Name should be minimum 5 characters.");
                $('#tax_name').focus();
                err = err + 1;
            } else {
                $('#tax_name_error').html("");
            }

        }
        else
        {
            $('#tax_name_error').html("");
        }
      }
            var id = '<?php echo $id; ?>';
              $.post('<?php echo url_for('adminManagement/checkTaxName'); ?>', {'tax_name': tax_name, 'id': id },
            function (data) {
            //$('#email_address_error').html(data);
            //return false;

            if(jQuery.trim(data) == 'success'){
            var description = jQuery.trim($('#tax_description').val());

            if(description == '') {
                $('#description_error').html("Please enter Description.");
                $('#description').focus();
                err = err + 1;
            } else {
                if(description != '')
                {
                    if(description.length > 100){ //Varun : Bug fix :FS#34956 
                        $('#description_error').html("Description should be less than or equal to 100 characters.");
                        $('#description').focus();
                        err = err + 1;
                    } else if(description.length < 5){
                        $('#description_error').html("Description should be minimum 5 characters.");
                        $('#description').focus();
                        err = err + 1;
                    }  else {
                        $('#description_error').html("");
                    }

                }
                else
                {
                    $('#description_error').html("");
                }
            }

                    if(err>0)
                        {
                            return false;
                        }
                    //return true;
                    $('#frmTax').submit();
            }else{
                if(data == 'tax'){
                     $('#tax_name_error').html("Tax Name already present.");
                     $('#tax_name').focus();
                }
                return false;
            }


        });


       return false;

    }
    function validateName(str)
    {
      var reg = /^[A-Za-z \.]*$/; //allow alphabet and spaces only...
      if(reg.test(str) == false) {
        return 1;
      }

      return 0;
    }


</script>
<!-- Varun : Bug fix : FS#34957-->
<div id="pgMenu">
        <div id="wrappernew">
            <div class="pgTitle">
                <h2>Tax Management</h2>
            </div>
     <div class="pgNav">
            <ul>
                <!-- Varun : Bug fix FS#34955 -->
                <li class="current"><a href="<?php echo url_for('adminManagement/tax') ?>">View Taxes</a></li>
                <li class=""><a href="<?php echo url_for('adminManagement/newTax') ?>">Create Tax</a></li>
                
        </ul>
        </div>
        </div>
    </div>
<div class="clear_new"></div>
    <?php $sf = sfContext::getInstance()->getUser();
if($sf->hasFlash('notice')){
  ?><div id="flash_notice" class="error_serverside" ><?php
  echo nl2br($sf->getFlash('notice'));

  ?></div><?php }?>
<form name="frmTax" id="frmTax" action="<?php echo url_for('adminManagement/updateTax') ?>" method="post" >
<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>

  <?php if (!$form->getObject()->isNew()): ?>
  <input type="hidden" name="sf_method" value="put" />
  <?php endif; ?>
  <div class="sform">

      <h3>Update Tax Information</h3>
    <div class="clear_new"></div>
<?php include_partial('formTax', array('form' => $form,'exist'=>$exist)) ?>
      </div>
  <input type="hidden" name="hdn_id" id="hdn_id" value="<?php echo $id; ?>">
  <?php //echo $form ?>
</form>
