<script>
    $(document).ready(function(){
        $('#SubAdmins').addClass('active');
         });
       function validateForm(){

        var err  = 0;
        var username = $('#sf_guard_user_username').val();
        var email_address = jQuery.trim($('#sf_guard_user_email_address').val());
        var reg = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i; //Email validation
        var edit = '<?php echo $edit; ?>';
        /*if(username == '') {
                $('#username_error').html("Please enter Username");
                $('#sf_guard_user_username').focus();
                err = err + 1;
         } else {
            if(username != '')
            {
            if(validateUserName(username) == 0) {
                $('#username_error').html("Please enter correct UserName.");
                $('#sf_guard_user_username').focus();
                err = err + 1;
            }else if(username.length > 70){
                $('#username_error').html("UserName should be less than or equal to 70 characters");
                $('#sf_guard_user_username').focus();
                err = err + 1;
            } else if(username.length < 1){
                $('#username_error').html("UserName should be minimum 1 characters");
                $('#sf_guard_user_username').focus();
                err = err + 1;
            } else {
                $('#username_error').html("");
            }
         }
        else
            {
            $('#username_error').html("");
            }
      }*/
        if(email_address == '') {
                $('#email_address_error').html("Please enter Email Address");
                $('#sf_guard_user_email_address').focus();
                err = err + 1;
         } else {
            if(email_address != '')
            {
            if(reg.test(email_address) == false) {
                $('#email_address_error').html("Please enter correct Email Address.");
                $('#sf_guard_user_email_address').focus();
                err = err + 1;
            }else if(email_address.length > 70){
                $('#email_address_error').html("Email Address should be less than or equal to 70 characters.");
                $('#sf_guard_user_email_address').focus();
                err = err + 1;
            } else {
                $('#email_address_error').html("");
            }
         }
        else
            {
            $('#email_address_error').html("");
            }
      }
      var id = '<?php echo $id; ?>';
        $.post('<?php echo url_for('adminManagement/checkUsernameEmail'); ?>', {'email_address': email_address , 'username': username, 'id': id},
            function (data) {
            //$('#email_address_error').html(data);
            //return false;

            if(jQuery.trim(data) == 'success'){
                 var password = jQuery.trim($('#sf_guard_user_password').val());
                    var password2 = jQuery.trim($('#sf_guard_user_password2').val());
                    var name = jQuery.trim($('#sf_guard_user_first_name').val());
                    var report = $('#report').is(':checked');
                    var merchant = $('#merchant').is(':checked');

                 if(edit!='1')
                   {
                    if(password == '') {
                             $('#password_error').html("Please enter Password");
                            $('#sf_guard_user_password').focus();
                            err = err + 1;
                     } else {
                    if(password != '')
                    {
                         if(password.length > 20){
                            $('#password_error').html("Password should be less than or equal to 20 characters.");
                            $('#sf_guard_user_password').focus();
                            err = err + 1;
                        } else if(password.length < 6){
                            $('#password_error').html("Password should be minimum 6 characters.");
                            $('#sf_guard_user_password').focus();
                            err = err + 1;
                        }  else {
                            $('#password_error').html("");
                        }

                    }
                    else
                    {
                        $('#password_error').html("");
                    }
                  }
                    if(password2 == '') {
                             $('#password2_error').html("Please enter Confirmation Password");
                            $('#sf_guard_user_password2').focus();
                            err = err + 1;
                     } else {
                    if(password2 != '')
                    {
                         if(password2.length > 20){
                            $('#password2_error').html("Confirm Password should be less than or equal to 20 characters.");
                            $('#sf_guard_user_password2').focus();
                            err = err + 1;
                        } else if(password2.length < 6){
                            $('#password2_error').html("Confirm Password should be minimum 6 characters.");
                            $('#sf_guard_user_password2').focus();
                            err = err + 1;
                        } else  if(password2 != password)
                        {
                            $('#password2_error').html("Confirm Password and Password don't match.");
                            $('#sf_guard_user_password2').focus();
                            err = err + 1;
                        } else {
                            $('#password2_error').html("");
                        }

                    }
                    else
                    {
                        $('#password_error2').html("");
                    }
                  }
               }
                   if(name == '') {
                             $('#first_name_error').html("Please enter Administrator Name.");
                            $('#sf_guard_user_first_name').focus();
                            err = err + 1;
                     } else {
                    if(name != '')
                    {
                         if(validateName(name) != 0){
                            $('#first_name_error').html("Please enter valid Administrator Name.(Only 'alphabets , spcace, -, .' allowed.)");
                            err = err+1;
                         }else if(name.length > 50){
                            $('#first_name_error').html("Administrator Name should be less than or equal to 50 characters.");
                            $('#sf_guard_user_first_name').focus();
                            err = err + 1;
                        } else if(name.length < 1){
                            $('#first_name_error').html("Administrator Name should be minimum 5 characters.");
                            $('#sf_guard_user_first_name').focus();
                            err = err + 1;
                        }  else {
                            $('#first_name_error').html("");
                        }

                    }
                    else
                    {
                        $('#first_name_error').html("");
                    }
                  }
                  if(report==false && merchant==false)
                      {
                          $('#permission_error').html("Please select atleast one Permission.");
                          err = err+1;
                      } else {
                          $('#permission_error').html("");
                      }
                    if(err>0)
                        {
                            return false;
                        }
                    //return true;
                    $('#frm').submit();
            }else{
                if(data == 'email'){
                     $('#email_address_error').html("Email Address already present.");
                     $('#sf_guard_user_email_address').focus();
                }/*else if(data == 'username'){
                  $('#username_error').html("Username already present.");
                   $('#sf_guard_user_username').focus();

                }*/else{
                    $('#email_address_error').html("Email Address already present.");
                     $('#sf_guard_user_email_address').focus();
                }
                return false;
            }


        });


       return false;

    }
    function validateName(str)
    {
      var reg = /^[A-Za-z \-\.]*$/; //allow alphabet and spaces only...
      if(reg.test(str) == false) {
        return 1;
      }

      return 0;
    }
    function validateUserName(str)
    {
      var reg = / /;
      if(reg.test(str) == true) {
        return 0;
      }
      return 1;
    }
</script>
<div id="pgMenu">
        <div id="wrappernew">
            <div class="pgTitle">
                <h2>Admin Management</h2>
            </div>
     <div class="pgNav">
            <ul>
                <!-- Varun : Bug fix :FS#34860 -->
                
                <li class="current"><a href="<?php echo url_for('adminManagement/viewAdmin') ?>">View Sub Admins</a></li>
                <li ><a href="<?php echo url_for('adminManagement/newAdmin') ?>">Create Sub Admin</a></li>
        </ul>
        </div>
        </div>
    </div>
<div class="clear_new"></div>
    <?php $sf = sfContext::getInstance()->getUser();
if($sf->hasFlash('notice')){
  ?><div id="flash_notice" class="error_serverside" ><?php
  echo nl2br($sf->getFlash('notice'));

  ?></div><?php }?>
<form name="frm" id="frm" action="<?php echo url_for('adminManagement/updateAdmin') ?>" method="post" >
<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>

  <?php if (!$form->getObject()->isNew()): ?>
  <input type="hidden" name="sf_method" value="put" />
  <?php endif; ?>
  <div class="sform">
    <h3>Update Sub Admin Information</h3>
    <div class="clear_new"></div>
<?php include_partial('form', array('form' => $form,'group_id'=>$group_id)) ?>
     </div>
  <?php //echo $form ?>
    <input type="hidden" name="hdn_id" id="hdn_id" value="<?php echo $id; ?>">
</form>
