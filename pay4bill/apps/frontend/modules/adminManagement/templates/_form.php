 <?php
               if(count($group_id) > 0 ) {
               $group_id = $sf_data->getRaw('group_id');
               }
               ?>
<div class="clear_new"></div>

                <div class="TableForm">
                      <table>
                        <tr>
                            <td width="35%" align="right"><?php echo $form['first_name']->renderLabel() ?></td>
                            <td><?php echo $form['first_name']->render() ?><div><label id="first_name_error" class="error"> <?php echo $form['first_name']->renderError(); ?></label></div></td>
                        </tr>
                        <tr>
                            <td  align="right"><?php echo $form['email_address']->renderLabel() ?></td>
                            <td><?php echo $form['email_address']->render() ?><div><label id="email_address_error" class="error">   <?php echo $form['email_address']->renderError(); ?></label></div></td>
                        </tr>
                       <tr>
                            <td  align="right"><?php echo $form['username']->renderLabel() ?></td>
                            <td>
                                <?php echo $form['username']->render() ?>
                                <div><label id="username_error" class="error"></label><?php echo $form['username']->renderError(); ?></div>
                            </td>
                        </tr>
                       <?php if (!is_array($group_id)) { ?>
                       <tr>
                            <td  align="right"><?php echo $form['password']->renderLabel() ?></td>
                            <td>
                                <?php echo $form['password']->render() ?>
                                <div><label  id="password_error" class="error"></label><?php echo $form['password']->renderError(); ?></div>
                            </td>
                        </tr>
                       <tr>
                            <td  align="right"><?php echo $form['password2']->renderLabel() ?></td>
                            <td>
                                <?php echo $form['password2']->render() ?>
                                 <div><label  id="password2_error" class="error">   <?php echo $form['password2']->renderError(); ?></label></div>
                            </td>
                        </tr>
                        <?php } ?>
                       <tr>
                            <td  align="right">Permissions <span><font color="red">*</font></span></td>
                            <td>
                                <label for=""  class="check">
                                <?php
                                if (is_array($group_id) && in_array('4', $group_id) != false) { ?>
                                <input id="report" class="check" type="checkbox" name="reports" value="4" checked>
                                <?php } else { ?><input id="report" class="check" type="checkbox" name="reports" value="4">
                                <?php } ?>
                                Reports</label>
                                <label for=""  class="check"><?php if (is_array($group_id) && in_array('5', $group_id) != false) { ?>
                                <input  id="merchant" type="checkbox" name="merchant" value="5" checked>
                                <?php } else { ?><input  id="merchant" type="checkbox" name="merchant" value="5">
                                <?php } ?>Merchant</label>
                                <label id="permission_error" class="error"></label>
                            </td>
                        </tr>
                        <tr>
                            <td align="right"><?php echo $form['_csrf_token']; ?></td>
                            <td>
                                <input class="blueBtn" type="button" value="Submit" name="Submit" onclick="return validateForm()">
                                <input class="blueBtn" type="button" value="Cancel" name="Cancel" onclick="window.location='<?php echo url_for("adminManagement/viewAdmin");?>';" >
                            </td>
                        </tr>                      
                      </table>
                </div>    