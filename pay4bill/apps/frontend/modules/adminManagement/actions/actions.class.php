<?php

/**
 * userAdmin actions.
 *
 * @package    symTest
 * @subpackage adminManagement
 * @author     Varun
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class adminManagementActions extends sfActions {

    // Varun : Function for New Admin Generation
    public function executeNewAdmin(sfWebRequest $request) {
        $this->getUser()->setAttribute('msg', '');
        $this->form = new AdminRegistrationForm();
        $this->addClassTo = $this->getActionName();
        $this->group_id = '';
    }

    // Varun : Function to save new admin details
    public function executeSaveAdmin(sfWebRequest $request) {
        $this->form = new AdminRegistrationForm();
        $this->group_id = '';
        if ($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT)) {

            $this->form->bind($request->getPostParameter($this->form->getName()));

            // Added to validate server side
            // By Saurabh on 12 July 2012

            if ($this->form->isValid()) {

                $sf_guard_user = $request->getParameter('sf_guard_user');
                $merchant = $request->getParameter('merchant');
                $report = $request->getParameter('reports');

                $name = $sf_guard_user['first_name'];
                $email_address = $sf_guard_user['email_address'];
                $username = $sf_guard_user['username'];
                $password = $sf_guard_user['password'];

                $emailExist = Doctrine::getTable('sfGuardUser')->findEntryForEmailAddress($email_address);
                //$usernameExist = Doctrine::getTable('sfGuardUser')->findEntryForUserName($username);
                //if($emailExist == 0 && $usernameExist == 0)
                if ($emailExist == 0) {
                    $addUser = new sfGuardUser();
                    $addUser->setEmailAddress($email_address);
                    $addUser->setUsername($username);
//                $addUser->setUsername($email_address);
                    $addUser->setFirstName($name);
                    $addUser->setPassword($password);
                    $addUser->setIsActive(1);
                    $addUser->save();
                    $user_id = $addUser->getId();
                    //Varun : Client feedback for email to newly created admin
                    $email_address = $email_address;
                  //  $username = $email_address;
                    $name = $name;
                    $password = $password;
                    $subject = "Admin Credentials";
                    $partial_name = 'newAdminEmail';
                    // Varun : Bug fix : FS#34856
                    $permission = '';
                    if (isset($merchant) && $merchant != '' && isset($report) && $report != '') {
                        $permission = 'Merchant and Reports';
                    } elseif (isset($report) && $report != '') {
                        $permission = 'Reports';
                    } elseif (isset($merchant) && $merchant != '') {
                        $permission = 'Merchant';
                    }
                    $host = $request->getUriPrefix();
                    $url_root = $request->getPathInfoPrefix();
                    $complete_url = $host . $url_root;
                        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
                    $taskId = EpjobsContext::getInstance()->addJob('SendMailNotification', $this->moduleName . "/newAdminEmail", array('email_address' => $email_address, 'username' => $username, 'name' => $name, 'password' => $password, 'subject' => $subject, 'partial_name' => $partial_name, 'complete_url' => $complete_url, 'permission' => $permission,'image_tag'=>public_path(sfConfig::get('app_logo_path'),true)), '-1', 'frontend');
                    $this->logMessage("scheduled mail job with id: $taskId", 'debug');
                    if (isset($merchant) && $merchant != '') {
                        $addUserGroup = new sfGuardUserGroup();
                        $addUserGroup->setUserId($user_id);
                        $addUserGroup->setGroupId($merchant);
                        $addUserGroup->save();
                    }
                    if (isset($report) && $report != '') {
                        $addUserGroup = new sfGuardUserGroup();
                        $addUserGroup->setUserId($user_id);
                        $addUserGroup->setGroupId($report);
                        $addUserGroup->save();
                    }
                    $this->getUser()->setFlash('notice', 'Admin Successfully Created');
                    $this->redirect('adminManagement/viewAdmin');
                } else {
                    $this->getUser()->setFlash('notice', "Email address and Username already exist. Please try another Email address and Username.");
                    $this->redirect('adminManagement/newAdmin');
                }
            } else {
                $this->setTemplate('newAdmin');
            }
        }
//        $this->processForm($request, $this->form);
    }

    /**
     * varun : Code for email to newly created admin
     * @param sfWebRequest $request
     * @return <type>
     */
    public function executeNewAdminEmail(sfWebRequest $request) {

        $email = $request->getParameter('email_address');
        $name = $request->getParameter('name');
        $username = $request->getParameter('username');
        $password = $request->getParameter('password');
        $subject = $request->getParameter('subject');
        $partialName = $request->getParameter('partial_name');
        $complete_url = $request->getParameter('complete_url');
        $permission = $request->getParameter('permission');

        /* Mail firing code starts */
        $mailTo = $email;
        $mailToGroup = array();

        $mailFrom = sfConfig::get('app_sender');
        $signature = sfConfig::get('app_sender');

        $partialVars = array('path'=>  $request->getParameter('image_tag'),'name' => ucwords($name), 'username' => $username, 'password' => $password, 'complete_url' => $complete_url, 'permission' => $permission);
        $mailingOptions = array('mailFrom' => $mailFrom, 'mailTo' => $mailTo, 'mailSubject' => $subject);

        $mailObj = new Mailing();
        $mailInfo = $mailObj->sendMail($partialName, $partialVars, $mailingOptions);
        if ($mailInfo == 1) {
            return $this->renderText('Mail has been sent successfully.');
        } else {
            return $this->renderText($mailInfo);
        }
    }

    // Varun : Function to list admin list
    public function executeViewAdmin(sfWebRequest $request) {
        $this->getUser()->setAttribute('msg', '');
        if (in_array('administrator', $this->getUser()->getGuardUser()->getGroupNames()) != false) {
            $this->adminList = Doctrine::getTable('sfGuardUser')->findAdminList();
            $this->adminLogin = true;
        } else {
            $this->adminLogin = false;
        }

        $total_no_of_records = count($this->adminList);
        if ($total_no_of_records > sfConfig::get('app_records_per_page')) {
            $this->actionTobeCalled = $this->getActionName();
            $this->total_no_of_records = $total_no_of_records;
            $this->no_of_records_per_page = sfConfig::get('app_records_per_page');
            $this->setPagination = true;
            if (!$request->hasParameter('offset')) {
                $offset = 0;
            } else {

                $this->getUser()->getAttributeHolder()->remove('msg_invoice');
                $offset = $request->getParameter('offset');
            }
            $this->current_offset = $offset;
            if ($this->adminLogin) {
                $this->adminList = Doctrine::getTable('sfGuardUser')->findAdminList($offset, sfConfig::get('app_records_per_page'));
            } else {
                $this->adminList = '';
            }
            $this->actionTobeCalled = $this->getActionName();
            $this->module = $this->getModuleName();

            $this->offset_last = (($total_no_of_records % sfConfig::get('app_records_per_page') == 0 ? ($total_no_of_records - sfConfig::get('app_records_per_page') ) : $total_no_of_records - ($total_no_of_records % sfConfig::get('app_records_per_page'))));

            $this->offset_next = (($offset + sfConfig::get('app_records_per_page')) > $this->offset_last ? $this->offset_last : ($offset + sfConfig::get('app_records_per_page')));

            $this->offset_prev = (($offset - sfConfig::get('app_records_per_page')) < 0 ? 0 : $offset - sfConfig::get('app_records_per_page'));
        } else {
            $this->setPagination = false;
        }
        $this->addClassTo = $this->getActionName();
    }

    //Varun : FUnction to enable merchant.
    public function executeEnableAdmin(sfWebRequest $request) {
        //Varun : Decrypt id
        $id = cryptString::decrypt($request->getParameter('id'));

        $adminObj = Doctrine::getTable('sfGuardUser')->find($id);

        $adminObj->setIsActive(1);
        $adminObj->save();
        $this->getUser()->setFlash('notice', 'Admin Successfully Enabled');
        $this->redirect('adminManagement/viewAdmin');
    }

    // Varun : Function to disable merchant
    public function executeDisableAdmin(sfWebRequest $request) {
        //Varun : Decrypt id
        $id = cryptString::decrypt($request->getParameter('id'));

        $adminObj = Doctrine::getTable('sfGuardUser')->find($id);

        $adminObj->setIsActive(0);
        $adminObj->save();
        /* 1-Nov-2012
         * If subadmin is disabled from Admin, and at the same time subadmin is already loggedin in another browser/machine,
         * then subadmin should be logged out as admin has disabled the subadmin. So subadmin is no longer to perform any further 
         * action.
         */
        $sessionTableObj = Doctrine::getTable('AppSessions')->findByUserId($id);
        $sessionTableObj->delete();
        /* **************** */
        $this->getUser()->setFlash('notice', 'Admin Successfully Disabled');
        $this->redirect('adminManagement/viewAdmin');
    }

    // Varun : Function To Edit admin Details
    public function executeEditAdmin(sfWebRequest $request) {
        $this->id = cryptString::decrypt($request->getParameter('id'));
        $this->edit = $request->getParameter('edit');
        $this->admin = Doctrine::getTable('sfGuardUser')->find($this->id);
        $this->sfGuardUserGroup = Doctrine::getTable('sfGuardUserGroup')->findByUserId($this->id);
        $count = count($this->sfGuardUserGroup->toArray());
        $sfGuardUser = $this->sfGuardUserGroup->toArray();
        $group_id = array();
        for ($i = 0; $i < $count; $i++) {
            $group_id[$i] = $sfGuardUser[$i]['group_id'];
        }
        $this->group_id = $group_id;
        $this->form = new AdminRegistrationForm($this->admin);
        $this->addClassTo = $this->getActionName();
    }

    //Varun : Function to save updated admin details
    public function executeUpdateAdmin(sfWebRequest $request) {
        $this->form = new AdminRegistrationForm();
        if ($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT)) {
            $this->form->bind($request->getPostParameter($this->form->getName()));

            $merchant = $request->getParameter('merchant');
            $report = $request->getParameter('reports');
            $sf_guard_user = $request->getParameter('sf_guard_user');
            $name = $sf_guard_user['first_name'];
            $email_address = $sf_guard_user['email_address'];
            //$username = $sf_guard_user['username'];
            //$password = $sf_guard_user['password'];
            $id = $request->getParameter('hdn_id');
            $emailExist = Doctrine::getTable('sfGuardUser')->findEntryForEmailAddress($email_address, $id);
            // $usernameExist = Doctrine::getTable('sfGuardUser')->findEntryForUserName($username,$id);
            // if($emailExist == 0 && $usernameExist == 0)
            if ($emailExist == 0) {
                $adminObj = Doctrine::getTable('sfGuardUser')->find($id);
                //$adminObj->setUsername($username);
                $adminObj->setEmailAddress($email_address);
//                $adminObj->setPassword($password);
                $adminObj->setFirstName($name);
                $adminObj->save();
                $deleteOldEntry = Doctrine::getTable('sfGuardUserGroup')->deletePrev($id);
                if (isset($report) && $report != '') {
                    $addUserGroup = new sfGuardUserGroup();
                    $addUserGroup->setUserId($id);
                    $addUserGroup->setGroupId($report);
                    $addUserGroup->save();
                }
                if (isset($merchant) && $merchant != '') {
                    $addUserGroup = new sfGuardUserGroup();
                    $addUserGroup->setUserId($id);
                    $addUserGroup->setGroupId($merchant);
                    $addUserGroup->save();
                }


                $this->getUser()->setFlash('notice', 'Admin Details Successfully Updated');
                $this->redirect('adminManagement/viewAdmin');
            } else {
                $this->getUser()->setFlash('notice', "Email address and Username already exist. Please try another Email address and Username.");
                $this->redirect('adminManagement/editAdmin?id=' . $id);
            }
        }
//        $this->processForm($request, $this->form);
    }

    public function executeCheckUsernameEmail(sfWebRequest $request) {

//        $this->setLayout(false);
//        $this->setTemplate(NULL) ;
//        $postArray = $request->getParameter('email_address');
//        $sf_guard_user = $request->getParameter('sf_guard_user');
        $email_address = $request->getPostParameter('email_address');
        $username = $request->getPostParameter('username');
        $id = $request->getPostParameter('id');

        $emailExist = Doctrine::getTable('sfGuardUser')->findEntryForEmailAddress($email_address, $id);
        $usernameExist = Doctrine::getTable('sfGuardUser')->findEntryForUserName($username, $id);

        if ($emailExist == 0 && $usernameExist == 0) {
            $msg = 'success';
        } else {
            if ($emailExist > 0 && $usernameExist == 0) {
                $msg = 'email';
            } else if ($emailExist == 0 && $usernameExist > 0) {
                $msg = 'username';
            } else {
                $msg = 'failed';
            }
        }
        return $this->renderText($msg);
    }

    /* Varun
     * Action : Tax
     * Function to show tax list to admnin
     */

    public function executeTax(sfWebRequest $request) {
        $this->getUser()->setAttribute('msg', '');
        if (in_array('administrator', $this->getUser()->getGuardUser()->getGroupNames()) != false) {
            $this->taxes = Doctrine::getTable('tax')->findTaxList();
            $this->adminLogin = true;
        } else {
            $this->adminLogin = false;
        }

        $total_no_of_records = count($this->taxes);

        if ($total_no_of_records > sfConfig::get('app_records_per_page')) {
            $this->actionTobeCalled = $this->getActionName();
            $this->total_no_of_records = $total_no_of_records;
            $this->no_of_records_per_page = sfConfig::get('app_records_per_page');
            $this->setPagination = true;
            if (!$request->hasParameter('offset')) {
                $offset = 0;
            } else {

                $this->getUser()->getAttributeHolder()->remove('msg_invoice');
                $offset = $request->getParameter('offset');
            }
            $this->current_offset = $offset;

            if ($this->adminLogin) {
                $this->taxes = Doctrine::getTable('tax')->findTaxList($offset, sfConfig::get('app_records_per_page'));
            } else {
                $this->taxes = '';
            }

            $this->module = $this->getModuleName();

            $this->offset_last = (($total_no_of_records % sfConfig::get('app_records_per_page') == 0 ? ($total_no_of_records - sfConfig::get('app_records_per_page') ) : $total_no_of_records - ($total_no_of_records % sfConfig::get('app_records_per_page'))));

            $this->offset_next = (($offset + sfConfig::get('app_records_per_page')) > $this->offset_last ? $this->offset_last : ($offset + sfConfig::get('app_records_per_page')));

            $this->offset_prev = (($offset - sfConfig::get('app_records_per_page')) < 0 ? 0 : $offset - sfConfig::get('app_records_per_page'));
        } else {
            $this->setPagination = false;
        }
        $this->addClassTo = $this->getActionName();
    }

    /* Varun
     * Action : NewTax
     * Function to create new tax for admnin
     */

    public function executeNewTax(sfWebRequest $request) {

        $this->getUser()->setAttribute('msg', '');
        $this->form = new TaxForm();
        $this->addClassTo = $this->getActionName();
        $this->exist = '';
    }

    // Varun : Function to save new tax details
    public function executeSaveTax(sfWebRequest $request) {

        $this->form = new TaxForm();
        if ($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT)) {

            $this->form->bind($request->getPostParameter($this->form->getName()));
            if ($this->form->isValid()) {

                $tax = $request->getParameter('tax');
                $tax_name = $tax['tax_name'];
                $description = $tax['description'];
                $status = $request->getParameter('status');

                $taxExist = Doctrine::getTable('tax')->findEntryForTax($tax_name);
                if ($taxExist == 0) {
                    $addTax = new Tax();
                    $addTax->setTaxName($tax_name);
                    $addTax->setDescription($description);
                    $addTax->setStatus($status);
                    $addTax->save();
                    $tax_id = $addTax->getId();
                    $this->getUser()->setFlash('notice', 'Tax Successfully Created');
                    $this->redirect('adminManagement/tax');
                } else {
                    $this->getUser()->setFlash('notice', "<b>$tax_name</b> Tax already exist. Please try another Tax Name.");
                    $this->redirect('adminManagement/newTax');
                }
            }
        }
//        $this->processForm($request, $this->form);
    }

    /* Varun
     * Action : CheckTaxName
     * Function to check tax name already present 
     */

    public function executeCheckTaxName(sfWebRequest $request) {

        $tax_name = $request->getPostParameter('tax_name');
        $id = $request->getPostParameter('id');

        $taxExist = Doctrine::getTable('tax')->findEntryForTax($tax_name, $id);

        if ($taxExist == 0) {
            $msg = 'success';
        } else {
            if ($taxExist > 0) {
                $msg = 'tax';
            }
        }
        return $this->renderText($msg);
    }

    /* Varun
     * Action : EditTax
     * Function To Edit tax Details 
     */

    public function executeEditTax(sfWebRequest $request) {
        //Varun : Decrypt id
        $this->id = cryptString::decrypt($request->getParameter('id'));
        $this->edit = $request->getParameter('edit');
        $this->tax = Doctrine::getTable('tax')->find($this->id);

        $this->exist = $this->tax->toArray();
        $this->form = new TaxForm($this->tax);
        $this->addClassTo = $this->getActionName();
    }

    /* Varun
     * Action : UpdateTax
     * Function to save updated tax details 
     */

    public function executeUpdateTax(sfWebRequest $request) {
        $this->form = new TaxForm();
        if ($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT)) {
            $this->form->bind($request->getPostParameter($this->form->getName()));

            $tax = $request->getParameter('tax');
            $tax_name = $tax['tax_name'];
            $description = $tax['description'];
            $status = $request->getParameter('status');
            $id = $request->getParameter('hdn_id');

            $taxExist = Doctrine::getTable('tax')->findEntryForTax($tax_name, $id);
            if ($taxExist == 0) {
                $taxObj = Doctrine::getTable('tax')->find($id);
                $taxObj->setTaxName($tax_name);
                $taxObj->setDescription($description);
                $taxObj->setStatus($status);
                $taxObj->save();

                $this->getUser()->setFlash('notice', 'Tax Details Successfully Updated');
                $this->redirect('adminManagement/tax');
            } else {
                $this->getUser()->setFlash('notice', "Tax Name already exist. Please try another Tax Name.");
                $this->redirect('adminManagement/editTax?id=' . $id);
            }
        }
//        $this->processForm($request, $this->form);
    }

    /*
     *
     *
     */

    public function executeMerchantRegList($request) {
        $this->getUser()->setAttribute('msg', '');
        if (in_array('administrator', $this->getUser()->getGuardUser()->getGroupNames()) != false) {
            $this->pendingMerchant = Doctrine::getTable('MerchantProfile')->getPendingApprovalMerchant();
            $this->adminLogin = true;
        }

        $total_no_of_records = count($this->pendingMerchant);

        if ($total_no_of_records > sfConfig::get('app_records_per_page')) {
            $this->actionTobeCalled = $this->getActionName();
            $this->total_no_of_records = $total_no_of_records;
            $this->no_of_records_per_page = sfConfig::get('app_records_per_page');
            $this->setPagination = true;
            if (!$request->hasParameter('offset')) {
                $offset = 0;
            } else {

                $this->getUser()->getAttributeHolder()->remove('msg_invoice');
                $offset = $request->getParameter('offset');
            }
            $this->current_offset = $offset;

            if ($this->adminLogin) {
                $this->pendingMerchant = Doctrine::getTable('MerchantProfile')->getPendingApprovalMerchant($offset, sfConfig::get('app_records_per_page'));
            } else {
                $this->pendingMerchant = '';
            }

            $this->module = $this->getModuleName();

            $this->offset_last = (($total_no_of_records % sfConfig::get('app_records_per_page') == 0 ? ($total_no_of_records - sfConfig::get('app_records_per_page') ) : $total_no_of_records - ($total_no_of_records % sfConfig::get('app_records_per_page'))));

            $this->offset_next = (($offset + sfConfig::get('app_records_per_page')) > $this->offset_last ? $this->offset_last : ($offset + sfConfig::get('app_records_per_page')));

            $this->offset_prev = (($offset - sfConfig::get('app_records_per_page')) < 0 ? 0 : $offset - sfConfig::get('app_records_per_page'));
        } else {
            $this->setPagination = false;
        }
        $this->addClassTo = $this->getActionName();
    }

}
