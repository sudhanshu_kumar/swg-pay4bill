<?php

/**
 * bankTeller actions.
 *
 * @package    nisp
 * @subpackage bankTeller
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class bankTellerActions extends sfActions {

    public function executeIndex(sfWebRequest $request) {
        $pay4billHelperObj = new pay4billHelper();
        $parameters = $pay4billHelperObj->trimArray($request->getParameterHolder()->getAll());
        $this->user_name = $request->hasParameter('user_name') ? $parameters['user_name'] : '';
        $this->emailaddress = $request->hasParameter('emailaddress') ? $parameters['emailaddress'] : '';
        $this->bank_name = $request->hasParameter('bank_name') ? $parameters['bank_name'] : '';


        $this->teller_profiles = Doctrine::getTable('TellerProfile')->getTellersList($this->user_name, $this->emailaddress, $this->bank_name);
        //echo "<pre>";print_r($this->teller_profiles->toArray());die;
        $total_no_of_records = count($this->teller_profiles);

        if ($total_no_of_records > sfConfig::get('app_records_per_page')) {
            $this->actionTobeCalled = $this->getActionName();
            $this->total_no_of_records = $total_no_of_records;
            $this->no_of_records_per_page = sfConfig::get('app_records_per_page');
            $this->setPagination = true;

            if (!$request->hasParameter('offset')) {
                $offset = 0;
            } else {

                $this->getUser()->getAttributeHolder()->remove('msg');
                $offset = $request->getParameter('offset');
            }
            $this->current_offset = $offset;
            $this->teller_profiles = Doctrine::getTable('TellerProfile')->getTellersList($this->user_name, $this->emailaddress, $this->bank_name, $offset, sfConfig::get('app_records_per_page'));


            $this->module = $this->getModuleName();
            $this->offset_last = (($total_no_of_records % sfConfig::get('app_records_per_page') == 0 ? ($total_no_of_records - sfConfig::get('app_records_per_page') ) : $total_no_of_records - ($total_no_of_records % sfConfig::get('app_records_per_page'))));

            $this->offset_next = (($offset + sfConfig::get('app_records_per_page')) > $this->offset_last ? $this->offset_last : ($offset + sfConfig::get('app_records_per_page')));

            $this->offset_prev = (($offset - sfConfig::get('app_records_per_page')) < 0 ? 0 : $offset - sfConfig::get('app_records_per_page'));
        } else {
            $this->setPagination = false;
        }
        $this->addClassTo = $this->getActionName();
    }

    public function executeShow(sfWebRequest $request) {
        $this->teller_profile = Doctrine::getTable('TellerProfile')->find(array($request->getParameter('id')));
        $this->forward404Unless($this->teller_profile);
    }

    public function executeNew(sfWebRequest $request) {
        $this->form = new TellerCreationForm();
        $this->addClassTo = $this->getActionName();
    }

    public function executeCreate(sfWebRequest $request) {
        $this->forward404Unless($request->isMethod(sfRequest::POST));

        $this->form = new TellerCreationForm();

        $this->processForm($request, $this->form);
        $this->addClassTo = 'new';

        $this->setTemplate('new');
    }

    public function executeSaveEditedTeller(sfWebRequest $request) {
        $this->addClassTo = $this->getActionName();
        $tellerUser = Doctrine::getTable('TellerProfile')->find(cryptString::decrypt($request->getParameter('id')));

        $inputValues = $this->createInputValuesArray($request);
        $this->form = $form = new TellerCreationForm(array(), $inputValues);
        $this->tellerProfileId = $request->getParameter('id');

        $tellerUser = Doctrine::getTable('TellerProfile')->find(cryptString::decrypt($request->getParameter('id')));
        $this->getUser()->setAttribute('existing_email_address', $tellerUser->getSfGuardUser()->getEmailAddress());

        $form->bind($inputValues);

        if ($form->isValid()) {
            $this->getUser()->getAttributeHolder()->remove('existing_email_address');

            try {
                $con = Doctrine::getTable('TellerProfile')->getConnection();
                $con->beginTransaction();
                $tellerUser->setAddress($request->getParameter('address'));
                $tellerUser->setBankName($request->getParameter('bankname'));
                $tellerUser->setSex($request->getParameter('sex'));
                $tellerUser->setName($request->getParameter('name'));
                $tellerUser->setPhoneNumber($request->getParameter('phone_number'));
                $tellerUser->getSfGuardUser()->setEmailAddress($request->getParameter('email_address'));
                // $tellerUser->getSfGuardUser()->setFirstName($request->getParameter('name'));
//                $tellerUser->getSfGuardUser()->setLastName($request->getParameter('last_name'));
//                foreach ($tellerUser->getTellerMerchantMapping() as $mappingObj) {
//
//                    $mappingObj->delete();
//                }
                $tellerUser->getTellerMerchantMapping()->delete();
                $tellerUser->save();
                foreach ($request->getParameter('merchants') as $id => $val) {
                    if ($val != "") {

                        $mappingObj = new TellerMerchantMapping();
                        $mappingObj->setMerchantProfileId($val);
                        $mappingObj->setTellerProfile($tellerUser);
//                        $tellerUser->setTellerMerchantMapping($mappingObj);
                        $mappingObj->save();
                    }
                }


                $con->commit();
            } catch (Exception $e) {
////            echo "<pre>";
//                print_r($e->getMessage());
//                exit;
                $con->rollback();

                $this->getUser()->setFlash('error', "Due to some problem, teller information couldn't be updated");
                $this->redirect('bankTeller/index');
            }
            $this->getUser()->setFlash('notice', 'Teller Edited Successfully');
            $this->redirect('bankTeller/index');
        } else {
            $this->setTemplate('edit');
            return;
//            $this->setTemplate('edit');
            return;
            foreach ($form->getFormFieldSchema() as $name => $formField) {

                echo $formField->getName() . "-" . $formField->getError() . "<br>"; // renders unformatted
            }
        }
    }

    public function executeEdit(sfWebRequest $request) {
        $this->forward404Unless($teller_profile = Doctrine::getTable('TellerProfile')->find(cryptString::decrypt($request->getParameter('id'))), sprintf('Object teller_profile does not exist (%s).', cryptString::decrypt($request->getParameter('id'))));
        $this->tellerProfileId = $request->getParameter('id');
        $exististingMerchantTellerMappingObj = Doctrine::getTable('TellerMerchantMapping')->findBy('teller_profile_id', $teller_profile->getId());
        $merchantsArray = array();
        foreach ($exististingMerchantTellerMappingObj as $obj) {
            array_push($merchantsArray, $obj->getMerchantProfileId());
        }
        $array_of_defaults = array('name' => $teller_profile->getName(), 'email_address' => $teller_profile->getSfGuardUser()->getEmailAddress()
            , 'sex' => $teller_profile->getSex(), 'address' => $teller_profile->getAddress(), 'bankname' => $teller_profile->getBankname(), 'phone_number' => $teller_profile->getPhoneNumber(), 'merchants' => $merchantsArray);


        $this->form = new TellerCreationForm(array(), $array_of_defaults);
        $this->addClassTo = $this->getActionName();
    }

    public function executeUpdate(sfWebRequest $request) {
        $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
        $this->forward404Unless($teller_profile = Doctrine::getTable('TellerProfile')->find(array($request->getParameter('id'))), sprintf('Object teller_profile does not exist (%s).', $request->getParameter('id')));
        $this->form = new TellerProfileForm($teller_profile);

        $this->processForm($request, $this->form);

        $this->setTemplate('edit');
    }

    public function executeDelete(sfWebRequest $request) {
        $request->checkCSRFProtection();

        $this->forward404Unless($teller_profile = Doctrine::getTable('TellerProfile')->find(array($request->getParameter('id'))), sprintf('Object teller_profile does not exist (%s).', $request->getParameter('id')));
        $teller_profile->delete();

        $this->redirect('bankTeller/index');
    }

    private function createInputValuesArray($request) {
        $selected_merchants = is_array($request->getParameter('merchants')) ? array_values($request->getParameter('merchants')) : array();
        return array('name' => $request->getParameter('name'),
            'email_address' => $request->getParameter('email_address')
            , 'sex' => $request->getParameter('sex'), 'address' => $request->getParameter('address'),
            'bankname' => $request->getParameter('bankname'), 'phone_number' =>
            $request->getParameter('phone_number'), 'merchants' => $selected_merchants
            , 'username' => $request->getParameter('username'),
            'password' => $request->getParameter('password'),
            'confirm_password' => $request->getParameter('confirm_password'));
    }

    protected function processForm(sfWebRequest $request, sfForm $form) {

        $form->bind($this->createInputValuesArray($request));

        if ($form->isValid()) {
            try {
                $con = Doctrine::getTable('sfGuardUser')->getConnection();
                $con->beginTransaction();
                $userObj = new sfGuardUser();
                $userObj->setUsername($request->getParameter('username'));
                $userObj->setPassword($request->getParameter('password'));
                $userObj->setIsActive(1);
                $userObj->link('Groups', pay4billHelper::getGroupIdsArrayFromGroupNames(array('teller')));
                $userObj->setEmailAddress($request->getParameter('email_address'));
                $tellerUser = new TellerProfile();
                $tellerUser->setAddress($request->getParameter('address'));
                $tellerUser->setBankName($request->getParameter('bankname'));
                $tellerUser->setSex($request->getParameter('sex'));
                $tellerUser->setName($request->getParameter('name'));
                $tellerUser->setPhoneNumber($request->getParameter('phone_number'));
                $tellerUser->setSfGuardUser($userObj);
                foreach ($request->getParameter('merchants') as $id => $val) {
                    if ($val != "") {
                        $mappingObj = new TellerMerchantMapping();
                        $mappingObj->setMerchantProfileId($val);
                        $mappingObj->setTellerProfile($tellerUser);
                        $mappingObj->save();
                    }
                }
                $userObj->save();
                $tellerUser->save();
                $con->commit();
                $this->sendEmailOnTellerCreation($tellerUser, $request->getParameter('password'));
            } catch (Exception $e) {

                $con->rollback();
            }
        } else {
            return;
            foreach ($form->getFormFieldSchema() as $name => $formField) {

                echo $formField->getName() . "-" . $formField->getError() . "<br>"; // renders unformatted
            }
        }
        $this->getUser()->setFlash('notice', 'Teller Added Successfully .');
        $this->redirect('bankTeller/index');
    }

    /**
     * Saurabh on 16 July 2012
     * @purpose:To upload teller from Admin
     */
    public function executeUpload(sfWebRequest $request) {
        ini_set('max_execution_time', '5000');
        ini_set('memory_limit', '512M');
        $this->valid = 0;
        $this->invalid = 0;
        $this->success = false;
        $this->titleXLS = false;
        $this->validFile = false;
        $this->invalidFile = false;
        $dateTime = date("dmY_H_i_s", time());
        $this->merchnatLogin = true;
        $this->getUser()->setAttribute('msg', '');
        $this->title = 'Bulk Teller Uploader';
        $this->action = $this->getModuleName() . "/upload";
        $this->uploadButtonText = 'Upload Tellers';
        $this->addClassTo = $this->getActionName();

        // @Saurabh To get the name of the downloaded file i.e., cus_sample_csv.csv
        // The file would be static as there are fixed set of teller's attribute
        // so, we do not need to make it dynamic
        $this->sample_link_file_name = 'teller_sample_csv.csv'; //pay4billHelper::executeDownloadTellerSample();

        if ($request->isMethod('POST')) {
            try {
                $uploadTemplate = new uploadTemplate();
                $upload_file = $request->getFiles('file');
                $uploadTemplate->upload($upload_file, 'Name,Sex,Address,PhoneNumber,EmailAddress,MerchantNameSeparatedByPipe,BankName,UserName,Password');

                $content_arr_list = $uploadTemplate->process();
                //////////////////////Validate CSV///////////////////////////////////////////////////////////////////////////////
                $host = $request->getUriPrefix();
                $url_root = $request->getPathInfoPrefix();
                $complete_url = $host . $url_root;
                $validate = $this->validate($content_arr_list, $complete_url);
                //////////////////////Creating Valid and Invlid files CSV////////////////////////////////////////////////////////
                if ($validate['validRows'] == '0' && $validate['invalidRows'] == '0') {
                    $this->msg = 'CSV is Empty.';
                    $this->error = true;
                } else {
                    $createdFiles = $uploadTemplate->writeFiles($validate['validArray'], $validate['invalidArray'], sfConfig::get('sf_upload_dir') . '/assets/');
                    $this->validFile = $createdFiles[0];
                    $this->invalidFile = $createdFiles[1];
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    $this->success = true;
                    if ($validate['validRows'] > 0 || $validate['invalidRows'] > 0) {
                        $this->valid = $validate['validRows'];
                        $this->invalid = $validate['invalidRows'];
                    }
                }
            } catch (Exception $e) {
                $this->getUser()->setFlash('notice', $e->getMessage(), false);
            }
        }
    }

    /**
     * @author:  Saurabh Kumar
     * @date: 16 July 2012
     * @purpose:    To check Email id valid or not
     */
    private function chkEmail($value) {
        preg_match("/^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i", $value, $matches);
        if (count($matches)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @author:  Saurabh Kumar
     * @date: 16 July 2012
     * @purpose:    To check Phone no valid or not
     */
    private function chkPhoneNumber($value) {
        preg_match("/^[0-9]*$/", $value, $matches);
        if (count($matches) && strlen($value) >= 6 && strlen($value) <= 20) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @author:     Saurabh Kumar
     * @date:       16 July 2012
     * @purpose:    To check valid merchant
     */
    private function chkValidMerchant($value) {

        $newval = explode("|", $value);
        foreach ($newval as $val) {
            $val = trim($val);
            $merchant = '';
            $isRecordExist = Doctrine::getTable('MerchantProfile')->getBillerMerchantWithServiceCodeKey($val)->count();
            if ($isRecordExist == 0) {
                $merchant = $val;
                break;
            }
        }
        return $merchant;
    }

    /**
     * @author:     Saurabh Kumar
     * @date:       16 July 2012
     * @purpose:    To check username exist ot not
     */
    private function isUsernameExist($value) {
        $val = trim($value);
        return $isRecordExist = Doctrine::getTable('sfGuardUser')->findBy('username', $val)->count();
    }

    /**
     * @author:     Saurabh Kumar
     * @date:       16 July 2012
     * @purpose:    To check email exist ot not
     */
    private function isEmailExist($value) {
        $val = trim($value);
        return $isRecordExist = Doctrine::getTable('sfGuardUser')->findBy('email_address', $val)->count();
    }

    /**
     * @author:     Saurabh Kumar
     * @date:       16 July 2012
     * @purpose:    To check name exist ot not
     */
    public function isValidName($name) {
        $msg = "";
        if (preg_match('/^[a-zA-Z\s\.]+$/i', $name)) {
            if (strlen($name) >= 3 && strlen($name) <= 50) {
                return $msg;
            } else {
                $msg = "Name should be between 3 to 50 characters";
                return $msg;
            }
        } else {
            $msg = "Please enter valid Name";
            return $msg;
        }
    }

    /**
     * @author:     Saurabh Kumar
     * @date:       16 July 2012
     * @purpose:    To check bank name exist ot not
     */
    public function isValidBankName($name) {
        $msg = "";
        if (preg_match('/^[a-zA-Z\s\.]+$/i', $name)) {
            if (strlen($name) >= 3 && strlen($name) <= 30) {
                return $msg;
            } else {
                $msg = "Bank Name should be between 3 to 30 characters";
                return $msg;
            }
        } else {
            $msg = "Please enter valid Bank Name. Should be between 3 to 30 characters";
            return $msg;
        }
    }

    /**
     * @author:     Saurabh Kumar
     * @date:       16 July 2012
     * @purpose:    To check email exist ot not
     */
    public function isValidSex($sex) {
        if (strtolower($sex) == 'male' || strtolower($sex) == 'female') {
            return true;
        } else {
            return false;
        }
    }

    public function isValidUserName($username) {
        if (preg_match('/^[a-zA-Z0-9]+$/i', $username)) {
            return true;
        } else {
            return false;
        }
    }

    public function isUserNameRestricted($username) {
        $array = sfConfig::get('app_ignore_users');
        $getResponse = pay4billHelper::isUserNameIgnore($username, $array);
        if ($getResponse == true

            )return false; else
            return true;
    }

    public function isPasswordInSecure($password) {
        $getResponse = pay4billHelper::isPasswordInSecure($password);
        if ($getResponse == true

            )return false; else
            return true;
    }

    /**
     * @author: Saurabh Kumar
     * @date: 16 July 2012
     * @purpose:To validate and save uploaded
     * tellers from Admin
     */
    public function validate($content_arr_list, $complete_url) {
        $upload_validator = new upload_validator();
        foreach ($content_arr_list as $key => $content_arr_new) {
            if ($key != 0) {
                $helperObj = new pay4billHelper();
                $content_arr_new = $helperObj->trimArray($content_arr_new);
                if (trim(strlen($content_arr_new[0]) != '')) {
                    // $merchant_id = $this->getUser()->getGuardUser()->MerchantUser->getId();
                    if (strlen($this->isValidName($content_arr_new[0])) > 0) {
                        $upload_validator->invalid_record($content_arr_new, $this->isValidName($content_arr_new[0]));
                    } else if ($content_arr_new[1] == '') {
                        $upload_validator->invalid_record($content_arr_new, 'Sex is Required');
                    } else if (!$this->isValidSex($content_arr_new[1])) {
                        $upload_validator->invalid_record($content_arr_new, 'Sex must be Male or Female');
                    } else if ($content_arr_new[2] == '') {
                        $upload_validator->invalid_record($content_arr_new, 'Address is Required');
                    } else if ($content_arr_new[3] == '') {
                        $upload_validator->invalid_record($content_arr_new, 'Phone Number is Required');
                    } else if (!$this->chkPhoneNumber($content_arr_new[3])) {
                        $upload_validator->invalid_record($content_arr_new, 'Phone Number is invalid. Should be between 6 to 20 digits');
                    } else if ($content_arr_new[4] == '') {
                        $upload_validator->invalid_record($content_arr_new, 'Email Address is Required');
                    } else if (!$this->chkEmail($content_arr_new[4])) {
                        $upload_validator->invalid_record($content_arr_new, 'Email Address is invalid');
                    } else if ($this->isEmailExist($content_arr_new[4]) > 0) {
                        $upload_validator->invalid_record($content_arr_new, 'Email Address is already exist');
                    } else if ($content_arr_new[5] == '') {
                        $upload_validator->invalid_record($content_arr_new, 'Merchant Name is Required');
                    } else if (strlen($this->chkValidMerchant($content_arr_new[5])) > 0) {
                        $upload_validator->invalid_record($content_arr_new, 'Merchant \'' . $this->chkValidMerchant($content_arr_new[5]) . '\' does not exist or is not approved');
                    } else if ($content_arr_new[6] == '') {
                        $upload_validator->invalid_record($content_arr_new, 'Bank Name is Required');
                    } else if (strlen($this->isValidBankName($content_arr_new[6])) > 0) {
                        $upload_validator->invalid_record($content_arr_new, $this->isValidBankName($content_arr_new[6]));
                    } else if ($content_arr_new[7] == '') {
                        $upload_validator->invalid_record($content_arr_new, 'Username is Required');
                    } else if ($this->isUsernameExist($content_arr_new[7]) > 0) {
                        $upload_validator->invalid_record($content_arr_new, 'Username is already exist');
                    } else if (strlen($content_arr_new[7]) < 6 || strlen($content_arr_new[7]) > 20) {
                        $upload_validator->invalid_record($content_arr_new, 'Username should be between 6 to 20 characters');
                    } else if (!$this->isUserNameRestricted($content_arr_new[7])) {
                        $upload_validator->invalid_record($content_arr_new, 'Restricted username. Please enter another username');
                    } else if (!$this->isValidUserName($content_arr_new[7])) {
                        $upload_validator->invalid_record($content_arr_new, 'Username should only be alphanumeric only');
                    } else if ($content_arr_new[8] == '') {
                        $upload_validator->invalid_record($content_arr_new, 'Password is Required');
                    } else if (!$this->isPasswordInSecure($content_arr_new[8])) {
                        $upload_validator->invalid_record($content_arr_new, 'Password must contain atleast 1 alphabet, 1 specialchar and 1 number');
                    } else {

                        $upload_validator->valid_record($content_arr_new);

                        try {
                            $helperObj = new pay4billHelper();
                            $content_arr_new = $helperObj->trimArray($content_arr_new);
                            $con = Doctrine::getTable('sfGuardUser')->getConnection();
                            $con->beginTransaction();
                            $userObj = new sfGuardUser();
                            $userObj->setUsername($content_arr_new[7]);
                            $userObj->setPassword($content_arr_new[8]);
                            $userObj->setIsActive(1);
                            $userObj->link('Groups', pay4billHelper::getGroupIdsArrayFromGroupNames(array('teller')));
                            $userObj->setEmailAddress($content_arr_new[4]);
                            $tellerUser = new TellerProfile();
                            $tellerUser->setAddress($content_arr_new[2]);
                            $tellerUser->setBankName($content_arr_new[6]);
                            $tellerUser->setSex($content_arr_new[1]);
                            $tellerUser->setName($content_arr_new[0]);
                            $tellerUser->setPhoneNumber($content_arr_new[3]);
                            $tellerUser->setSfGuardUser($userObj);

                            $merchantArr = explode("|", $content_arr_new[5]);
                            $helperObj = new pay4billHelper();
                            $merchantArr = $helperObj->trimArray($merchantArr);

                            $merArr = array();

                            foreach ($merchantArr as $value) {
                                $arr = Doctrine::getTable('MerchantProfile')->findOneBy('merchant_name', $value);
                                $merArr[$arr['id']] = $value;
                            }

                            foreach ($merArr as $id => $val) {
                                if ($val != "") {
                                    $mappingObj = new TellerMerchantMapping();
                                    $mappingObj->setMerchantProfileId($id);
                                    $mappingObj->setTellerProfile($tellerUser);
                                    $mappingObj->save();
                                }
                            }
                            $userObj->save();
                            $tellerUser->save();
                            $con->commit();
                            $this->sendEmailOnTellerCreation($tellerUser, $content_arr_new[8]);
                        } catch (Exception $e) {

                            $con->rollback();
                        }
                    }
                } else {
                    $upload_validator->invalid_record($content_arr_new, 'Name is Required');
                }
            }
        }//End Of Foreech
        return $upload_validator->getValues();
    }

    /*
     * Function to enable the teller
     * @ Saurabh on 19 July
     */

    public function executeEnableTeller(sfWebRequest $request) {

        $user_id = cryptString::decrypt($request->getParameter('user_id')); // Teller ID

        $ob = Doctrine::getTable('sfGuardUser')->find($user_id);
        $ob->setIsActive(1);
        $ob->save();
        $this->sendEmailOnTellerActivationStatusChange($ob->getTellerUser());

        $this->getUser()->setFlash('notice', 'Teller Enabled Successfully.');
        $this->redirect('bankTeller/index');
    }

    /*
     * Function to disable the teller
     * @ Saurabh on 19 July
     */

    public function executeDisableTeller(sfWebRequest $request) {
        $user_id = cryptString::decrypt($request->getParameter('user_id')); // Customer ID

        $ob = Doctrine::getTable('sfGuardUser')->find($user_id);
        $ob->setIsActive(0);
        $ob->save();
        $this->sendEmailOnTellerActivationStatusChange($ob->getTellerUser());

        /* 1-Nov-2012
         * If Teller is disabled from Admin, and at the same time Teller is already loggedin in another browser/machine,
         * then Teller should be logged out as admin has disabled the Teller. So Teller is no longer to perform any further
         * action.
         */
        $sessionTableObj = Doctrine::getTable('AppSessions')->findByUserId($user_id);
        $sessionTableObj->delete();
        /*         * *************** */

        $this->getUser()->setFlash('notice', 'Teller Disabled Successfully.');
        $this->redirect('bankTeller/index');
    }

    private function sendEmailOnTellerCreation($tellerProfileObj, $password) {
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
        $taskId = EpjobsContext::getInstance()->addJob('SendTellerUserCreationMail', $this->moduleName . "/sendEmail", array('email' => $tellerProfileObj->getSfGuardUser()->getEmailAddress(), 'name' => $tellerProfileObj->getName(), 'partial_name' => 'tellerCreationEmail', 'username' => $tellerProfileObj->getSfGuardUser()->getUsername(), 'password' => $password, 'image_tag' => public_path(sfConfig::get('app_logo_path'),true)), '-1', 'frontend');

        $this->logMessage("scheduled mail job with id: $taskId", 'debug');
    }

    private function sendEmailOnTellerActivationStatusChange($tellerProfileObj) {
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
        $taskId = EpjobsContext::getInstance()->addJob('SendTellerAccountStatusEmail', $this->moduleName . "/sendEmail", array('email' => $tellerProfileObj->getSfGuardUser()->getEmailAddress(), 'name' => $tellerProfileObj->getName(), 'status' => $tellerProfileObj->getSfGuardUser()->getIsActive(), 'partial_name' => 'tellerStatusChangeEmail', 'image_tag' => public_path(sfConfig::get('app_logo_path'),true)), '-1', 'frontend');

        $this->logMessage("scheduled mail job with id: $taskId", 'debug');
    }

    public function executeSendEmail(sfWebRequest $request) {

        $email = $request->getParameter('email');
        $teller_name = ucfirst($request->getParameter('name'));
        $subject = 'Welcome to pay4bill';
        $partialVars = array('teller_name' => $teller_name);
        if ($request->hasParameter('username') && $request->hasParameter('password')) {


            $partialVars['username'] = $request->getParameter('username');
            $partialVars['password'] = $request->getParameter('password');
        } else {
            $partialVars['status'] = $request->getParameter('status');
        }
        $partialVars['path'] = $request->getParameter('image_tag');
        $partialName = $request->getParameter('partial_name');

        /* Mail firing code starts */
        $mailTo = $email;
        $mailToGroup = array();

        $mailFrom = sfConfig::get('app_sender');
        $signature = sfConfig::get('app_sender');


        $mailingOptions = array('mailFrom' => $mailFrom, 'mailTo' => $mailTo, 'mailSubject' => $subject);

        $mailObj = new Mailing();
        $mailInfo = $mailObj->sendMail($partialName, $partialVars, $mailingOptions);

        return $this->renderText($mailInfo);
    }

}