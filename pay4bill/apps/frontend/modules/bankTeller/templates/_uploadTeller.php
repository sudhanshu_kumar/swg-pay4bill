<div id="mainnew" >
   <div id="wrappernew">
        <div id="content">
            <div class="sform">
                <div class="clear_new"></div>                    
                    <?php   
                        $sf = sfContext::getInstance()->getUser();
                        if($sf->hasFlash('notice')){
                            echo '<div id="flash_notice" class="error_serverside" >';
                            echo nl2br($sf->getFlash('notice'));
                            echo '</div>';
                        }
                    ?>
                
                <h3><?php echo $title;?></h3>
                <form name="upload_file" action="<?php echo url_for($action); ?>" method="post" enctype="multipart/form-data">   <div class="FormRepeatArea">

                        <div class="TableForm">
                            <table>
                                <tr><td><input type="file" value="Upload CSV" name="file" class="FormInput2" /></td></tr>
                            </table>
                        </div>
                        <div class="clear_new"></div>
                        <div class="form_table">
                            <?php 
                                if(($valid > 0  OR $invalid > 0 OR $success)){ 
                                  echo '<table style="width:50%;" align="center" border="0">';
                                }else{
                                  echo '<table style="width:100%;" border="0">'; 
                                } 
                            ?>
                            <?php if(($valid > 0 && $success) OR ($invalid > 0 && $success)){ ?>
                                <tr>
                                    <td class="thankyou">Thank You for Teller Uploading</td>
                                </tr>
                                <?php if(($valid > 0 && $invalid > 0) && $success){ ?>
                                <tr>
                                    <td class="statusBlock"><b >Status:</b> <span>Teller list Partially Uploaded</span></td>
                                </tr>
                                <tr>
                                    <td><div class="hyperlinksuccess"><?php echo link_to('Download Success List', 'invoice/downloads?file='.$validFile, array('popup'=>'false','class'=>'greenLbl')) ?></div></td>
                                </tr>
                                <tr>
                                    <td><div class="hyperlinkerror"><?php echo link_to('Download Failure List', 'invoice/downloads?file='.$invalidFile, 'popup=false') ?></div></td>
                                </tr>
                                <?php } else if($valid > 0 && $success){ ?>
                                <tr>
                                    <td class="statusBlock"><span class="green" >Status:</span> <span class="green">Teller list Successfully Uploaded</span></td>
                                </tr>
                                <tr>
                                    <td><div class="hyperlinksuccess"><?php echo link_to('Download Success List', 'invoice/downloads?file='.$validFile, 'popup=false') ?></div></td>
                                </tr>
                                <?php } else { ?>
                                <tr>
                                    <td class="statusBlock"><span class="green" >Status:</span> <span>Teller List Uploading Failed</span></td>
                                </tr>
                                <tr>
                                    <td><div class="hyperlinkerror"><?php echo link_to('Download Failure List', 'invoice/downloads?file='.$invalidFile, 'popup=false') ?></div></td>
                                </tr>
                                <?php }?>
                                <tr>
                                    <td><div class="hyperlink"></div></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>

                                    <?php } ?>

                            </table>
                        </div>
                        <input type="submit" name="submit" value="<?php echo $uploadButtonText;?>" class = "blueBtn" style="margin:0px!important;" ><!-- set the alignment only this page-->
                    </div>
                </form>
            </div>
            <div class="download_link"><a href="#" onclick="window.open('<?php echo _compute_public_path($sample_link_file_name, 'download', '', true); ?>');return false;"><?php echo $sample_link_text;?></a></div> <p>&nbsp;</p>
            <!--div class="download_link"><a href="#" onclick="window.open('<?php //echo _compute_public_path($sample_link_file_name, 'uploads/assets', '', true); ?>');return false;"><?php //echo $sample_link_text;?></a></div> <p>&nbsp;</p-->
        </div>
    </div>
</div>

