<?php
    include_partial('subHeader', array('addClassTo' => $addClassTo));
?>
<script>
    $('#Tellers').addClass('active');
</script>
<?php if (!empty($msg)) {
?><div class="clear_new"></div> <?php } ?>

<div id="mainnew">
    <!-- Varun : Flash messaged added -->
    <?php
    $sf = sfContext::getInstance()->getUser();
    if ($sf->hasFlash('notice')) {
    ?><div id="flash_notice" class="msg_serverside" ><?php
        echo nl2br($sf->getFlash('notice'));
    ?></div>
    <br/><?php } ?>
      <?php
            if($sf->hasFlash("successMsg")){
                echo '<div id="flash_notice" class="msg_serverside" >';
                echo $sf->getFlash("successMsg");
                echo "</div>";
            }
      ?>
    <div id="wrappernew">
        <div id="content">
            <div id="Formwraper">


                <!-- Search starts here -->

                <h3>Search Tellers</h3>
                <div id="msg"></div>
                <div class="TableForm" id="search">

                    <form action="<?php echo url_for('bankTeller/index'); ?>" method="post" id="search_form" onsubmit="return validateSearch()">

                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="17.5%" valign="top"><?php echo "Bank Name" ?> </td>
                                <td  width="35%" valign="top">
                                    <input class="FormInput9" type="textbox" name="bank_name" value="<?php echo $bank_name; ?>">
                                </td>

                            </tr>
                            <tr>
                                <td width="17.5%" valign="top"><?php echo "Username" ?> </td>
                                <td  width="35%" valign="top">
                                    <input class="FormInput9" type="textbox" name="user_name" value="<?php echo $user_name; ?>">
                                </td>

                            </tr>
                            <tr>
                                <td width="17.5%" valign="top"><?php echo "Email Address" ?> </td>
                                <td  width="35%" valign="top">
                                    <input class="FormInput9" type="textbox" name="emailaddress" value="<?php echo $emailaddress; ?>">
                                </td>

                            </tr>
                            <tr id="merchantSpecificAttributes">
                                <td colspan="3"  valign="top">
                                    <div class="Errormrg"><input type="submit" value="Search" class="blueBtn" >
                                        <input type="button" class="blueBtn" value="Reset" onClick="window.location='<?php echo url_for('bankTeller/index'); ?>'"/></div>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
                <div class="clear_new"></div>
                <!-- Search ends here -->
                <div style="clear:both"></div>
                <div class="TableForm">
                    <table cellspacing="0" cellpadding="0" border="0">
                        <thead>
                        <th>Name</th>
                        <th>Username</th>
                        <th>Email Address</th>
                        <th>Bank Name</th>
<!--                        <th>Address</th>-->
                        <th>Phone Number</th>
                        <th>Action</th>

                        </thead>
                        <tbody>
                            <?php
                            if (count($teller_profiles) > 0) {
                            ?>
                            <?php
                                $i = 0;
                                foreach ($teller_profiles as $tellerProfile): $arrUserEmail = '';
                                    $i++; ?>
                            <?php if ($i % 2 != 0) {
                            ?>
                                        <tr class="odd">
                            <?php } else {
 ?><tr class=""> <?php } ?>
                                        <td><?php echo strlen($tellerProfile->getName())>20?substr($tellerProfile->getName(),0,20).'...':$tellerProfile->getName();
                                                  /*echo $tellerProfile->getName();*/ ?></td>
                                        <td><?php echo $tellerProfile->getSfGuardUser()->getUsername() ?></td>
                                        <td>
                                            <?php echo strlen($tellerProfile->getSfGuardUser()->getEmailAddress())>30?substr($tellerProfile->getSfGuardUser()->getEmailAddress(),0,28).'...':$tellerProfile->getSfGuardUser()->getEmailAddress(); ?>
                                            <?php //echo $tellerProfile->getSfGuardUser()->getEmailAddress() ?></td>
                                        <td>
                                            <?php echo strlen($tellerProfile->getBankName())>15?substr(ucfirst($tellerProfile->getBankName()),0,20).'...':ucfirst($tellerProfile->getBankName()) ?>
                                            <?php //echo ucfirst($tellerProfile->getBankName()) ?>
                                        </td>
        <!--                                    <td><?php //echo wordwrap($tellerProfile->getAddress(),32,"\n",false);  ?></td>-->

                                        <td><?php echo $tellerProfile->getPhoneNumber() ?></td>
                                        <td align="center">

                                    <?php
                                        // @Saurabh - Check if the user is support user or not
                                        // Support user can be able to change the password of the teller
                                        if(pay4billHelper::checkIfSupportUser(sfContext::getInstance()->getUser()->getGroups())!=1){
                                            echo link_to(image_tag('tdEdit.png'), 'bankTeller/edit?id=' . cryptString::encrypt($tellerProfile->getId()), array('title' => 'Edit', 'alt' => 'Edit'));
                                            echo '&nbsp;&nbsp;';
                                            if ($tellerProfile->getSfGuardUser()->getIsActive() == '1') {
                                                echo link_to(image_tag('enable.gif'), 'bankTeller/disableTeller?user_id=' . cryptString::encrypt($tellerProfile->getUserId()), array('title' => 'Disable Teller', 'alt' => 'Disable Teller', 'class' => 'delete', 'confirm' => 'Are you sure you want to disable this teller?'));
                                            } else {
                                                echo link_to(image_tag('disable.gif'), 'bankTeller/enableTeller?user_id=' . cryptString::encrypt($tellerProfile->getUserId()), array('title' => 'Enable Teller', 'alt' => 'Enable Teller', 'class' => 'delete', 'confirm' => 'Are you sure you want to enable this teller?'));
                                            }
                                        } else {
                                            // Support User
                                            echo link_to(image_tag('change_password_icon.png',array('title'=>'Reset Password','Alt'=>'Reset Password')), 'supportUser/resetTellerPassword?id=' . cryptString::encrypt($tellerProfile->getId()), array('title' => 'Reset Password', 'alt' => 'Reset Password'));
                                        }
                                   ?>

                                   &nbsp;&nbsp;
                                </td>

                            </tr>
<?php endforeach; ?>

                            <?php
                                } else {
                                    echo "<tr><td colspan='8' align='center'>No Teller Found</td></tr>";
                                }
                            ?>
                            </tbody>
                        </table>

                    </div>
                    <div class="nofoList">
                    <?php
                                include_partial('global/records_number', array('amount_footer' => false, 'current_offset' => isset($current_offset) ? $current_offset : null, 'offset_next' => isset($offset_next) ? $offset_next : null, 'total_no_of_records' => isset($total_no_of_records) ? $total_no_of_records : count($teller_profiles), 'no_of_records_per_page' => isset($no_of_records_per_page) ? $no_of_records_per_page : null, 'from' => ''));


                                //include_partial('global/pagination',array('moduleName'=>$module,'current_offset'=>$current_offset,'offset_next'=>$offset_next,'offset_prev'=>$offset_prev,'offset_last'=>$offset_last));
                                //           include_partial('global/records_number', array('amount_footer' => false, 'current_offset' => $current_offset, 'offset_next' => $offset_next, 'total_no_of_records' => $total_no_of_records, 'no_of_records_per_page' => $no_of_records_per_page, 'from' => ''))
                    ?>

                            </div>
                            <div align="right">
                    <?php
                                if ($setPagination) {
                                    include_partial('global/pagination', array('moduleName' => $module, 'amount_footer' => false, 'actionName' => $actionTobeCalled, 'current_offset' => $current_offset, 'offset_next' => $offset_next, 'offset_prev' => $offset_prev, 'offset_last' => $offset_last, 'search' => '', 'from' => '', 'user_name' => isset($user_name) ? $user_name : '', 'emailaddress' => isset($emailaddress) ? $emailaddress : ''));
                                }
                    ?></div>

                        </div>
                    </div>
<!--                    <input type="button" value="Add New" class="blueBtn" onclick="window.location='<?php //echo url_for('bankTeller/new') ?>'">-->
    </div>
</div>