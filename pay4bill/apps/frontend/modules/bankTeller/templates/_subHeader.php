<div id="pgMenu">
    <div id="wrappernew">
        <div class="pgTitle">
            <h2>Tellers</h2>
        </div>
        <?php
            // Case of support user
            if(pay4billHelper::checkIfSupportUser(sfContext::getInstance()->getUser()->getGroups())!=1) {
        ?>
        <div class="pgNav">
            <ul>
                <li <?php if($addClassTo=='index'){ ?>class="current" <?php } ?>><a href="<?php echo url_for('bankTeller/index?offset=0') ?>">View Tellers</a></li>
                <li <?php if($addClassTo=='new'){ ?>class="current" <?php } ?>><a href="<?php echo url_for('bankTeller/new') ?>">Create Teller</a></li>
                <li <?php if($addClassTo=='upload'){ ?>class="current" <?php } ?>><a href="<?php echo url_for('bankTeller/upload') ?>">Upload CSV</a></li>
            </ul>
        </div>
        <?php } ?>
    </div>
</div>