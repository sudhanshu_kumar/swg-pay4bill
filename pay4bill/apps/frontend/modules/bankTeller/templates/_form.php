<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>
<script>
    $('#Tellers').addClass('active');
</script>
<div id="Formwraper">
    <div>
        <?php if ($new) {
 ?>
            <h3>Create Teller</h3>
        <?php
        } else {
        ?><h3>Edit Teller's Information</h3>
        <?php
        }
        ?>
    </div>
<!--      <form  action="<?php //echo url_for('bankTeller/' . ($form->getObject()->isNew() ? 'create' : 'update') . (!$form->getObject()->isNew() ? '?id=' . $form->getObject()->getId() : ''))    ?>" method="post" class="dlForm" <?php //$form->isMultipart() and print 'enctype="multipart/form-data" '    ?> >-->
    <form  action="<?php
        if ($new) {
            echo url_for('bankTeller/create');
        } else {
            echo url_for('bankTeller/saveEditedTeller?id=' . $tellerProfileId);
        } ?>" method="post" class="dlForm" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?> >

<?php //echo $form['_csrf_token'];   ?>
        <div class="TableForm">
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="35%" align="right"><?php echo $form['name']->renderLabel(); ?><span class="redLbl">*</span></td>
                    <td><?php echo $form['name']->render(); ?><?php echo $form['name']->renderError(); ?></td>
                </tr>
<!--                <tr>
                    <td width="35%" align="right"><?php //echo $form['last_name']->renderLabel();  ?><span class="redLbl">*</span></td>
                    <td><?php //echo $form['last_name']->render();  ?><?php //echo $form['last_name']->renderError();  ?></td>
                </tr>-->
                <tr>
                    <td width="35%" align="right"><?php echo $form['sex']->renderLabel(); ?><span class="redLbl">*</span></td>
                    <td><?php echo $form['sex']->render(); ?><?php echo $form['sex']->renderError(); ?></td>
                </tr>
                <tr>
                    <td width="35%" align="right"><?php echo $form['address']->renderLabel(); ?><span class="redLbl">*</span></td>
                    <td><?php echo $form['address']->render(); ?><?php echo $form['address']->renderError(); ?></td>
                </tr>
                <tr>
                    <td width="35%" align="right"><?php echo $form['phone_number']->renderLabel(); ?><span class="redLbl">*</span></td>
                    <td><?php echo $form['phone_number']->render(); ?><?php echo $form['phone_number']->renderError(); ?></td>
                </tr>
                <tr>
                    <td width="35%" align="right"><?php echo $form['email_address']->renderLabel(); ?><span class="redLbl">*</span></td>
                    <td><?php echo $form['email_address']->render(); ?><?php echo $form['email_address']->renderError(); ?></td>
                </tr>

                <tr>
                    <td width="35%" align="right"><?php echo $form['merchants']->renderLabel(); ?><span class="redLbl">*</span></td>
                    <td><?php echo $form['merchants']->render(); ?><?php echo $form['merchants']->renderError(); ?></td>
                </tr>
                <tr>
                    <td width="35%" align="right"><?php echo $form['bankname']->renderLabel(); ?><span class="redLbl">*</span></td>
                    <td><?php echo $form['bankname']->render(); ?><?php echo $form['bankname']->renderError(); ?></td>
                </tr>


<?php if ($new) { ?>
                    <tr>
                        <td width="35%" align="right"><?php echo $form['username']->renderLabel(); ?><span class="redLbl">*</span></td>
                    <td><?php echo $form['username']->render(); ?><?php echo $form['username']->renderError(); ?></td>
                </tr>
                <tr>
                    <td width="35%" align="right"><?php echo $form['password']->renderLabel(); ?><span class="redLbl">*</span></td>
                    <td><?php echo $form['password']->render(); ?><?php echo $form['password']->renderError(); ?></td>
                </tr>
                <tr>
                    <td width="35%" align="right"><?php echo $form['confirm_password']->renderLabel(); ?></td>
                    <td><?php echo $form['confirm_password']->render(); ?><?php echo $form['confirm_password']->renderError(); ?></td>
                </tr>
<?php } ?>



                <tr>
                    <td width="35%" align="right"></td>
                    <td><input type="submit" name="save" class="blueBtn" value="Save"/>
                        <input type="button" name="cancel" id="cancel_button"  class="blueBtn" value="Cancel" onclick="javascript: window.location='<?php echo url_for('bankTeller/index') ?>'"/></td>
                </tr>
            </table>
        </div>
    </form>
    <p>&nbsp;</p>
</div>
