<script>
    $(document).ready(function(){
        $('#Tellers').addClass('active');
            <?php if(isset($msg)){ ?>
                $('#if_error').html('<?php echo html_entity_decode($msg); ?>');
                
                <?php if($error){ ?>
                    $('#if_error').addClass('error_serverside'); 
                <?php } else { ?>
                    $('#if_error').addClass('msg_serverside');
                <?php } ?>
                    
            <?php } else { ?>
                    $('#if_error').html();
                    $('#if_error').removeClass('error_serverside');
                    $('#if_error').removeClass('msg_serverside');
           <?php } ?>
                    $('form').attr('id','division_frm');        
                    $("#division_frm").validate({
                        rules: {
                            'file': {
                                required: true
                            }
                        },
                        messages: {
                            'file':
                                {
                                required: "Please select CSV file to upload."
                                }
                        }
                    })
                });

</script><?php if (isset($addClassTo)) {
    include_partial('subHeader', array('addClassTo' => $addClassTo,'loginType'=>'', 'customerTellerId'=>''));
} else {
    include_partial('subHeader', array('addClassTo' => 'new'));
} ?>
<div id="if_error" ></div>
<?php //include_partial('global/uploadsInvoice', array('title' => $title, 'uploadButtonText' => $uploadButtonText,'action'=>$action,'sample_link_file_name'=>'invoice_sample_csv','sample_link_text'=>'Download Sample Invoice CSV File','valid'=>$valid,'invalid'=>$invalid,'success'=>$success,'validFile'=>$validFile,'invalidFile'=>$invalidFile)); ?>
<?php  include_partial('uploadTeller', array('title' => $title, 'uploadButtonText' => $uploadButtonText,'action'=>$action,'sample_link_file_name'=>$sample_link_file_name,'sample_link_text'=>'Download Sample Teller CSV File','valid'=>$valid,'invalid'=>$invalid,'success'=>$success,'validFile'=>$validFile,'invalidFile'=>$invalidFile));?>
