<table>
  <tbody>
    <tr>
      <th>Id:</th>
      <td><?php echo $teller_profile->getId() ?></td>
    </tr>
    <tr>
      <th>User:</th>
      <td><?php echo $teller_profile->getUserId() ?></td>
    </tr>
    <tr>
      <th>Sex:</th>
      <td><?php echo $teller_profile->getSex() ?></td>
    </tr>
    <tr>
      <th>Address:</th>
      <td><?php echo $teller_profile->getAddress() ?></td>
    </tr>
    <tr>
      <th>Bank name:</th>
      <td><?php echo $teller_profile->getBankName() ?></td>
    </tr>
    <tr>
      <th>Phone number:</th>
      <td><?php echo $teller_profile->getPhoneNumber() ?></td>
    </tr>
    <tr>
      <th>Created at:</th>
      <td><?php echo $teller_profile->getCreatedAt() ?></td>
    </tr>
    <tr>
      <th>Updated at:</th>
      <td><?php echo $teller_profile->getUpdatedAt() ?></td>
    </tr>
  </tbody>
</table>

<hr />

<a href="<?php echo url_for('bankTeller/edit?id='.$teller_profile->getId()) ?>">Edit</a>
&nbsp;
<a href="<?php echo url_for('bankTeller/index') ?>">List</a>
