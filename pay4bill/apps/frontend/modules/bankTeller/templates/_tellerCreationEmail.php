<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Untitled Document</title>
    </head>

    <body>
        <table align="center" cellpadding="0" cellspacing="0" style="width:650px;">

            <tr>
                <td style="padding-bottom:5px;">
                  <?php include_partial('global/pay4bill_logo', array('path' => $path));    ?>
                </td>


            </tr>


            <table cellspacing="0" cellpadding="10" border="0" align="center" bgcolor="#f0f0f0" style="width:650px; border-collapse:collapse; border:1px solid #dedcdc; color:#646464; font-family:Arial, Helvetica, sans-serif; font-size:14px;">
                <tr>
                    <td>Dear  <span><?php echo $teller_name; ?></span>,</td>
                </tr>

                <tr>
                    <td>You have been registered as a teller on pay4bill. Your details to access the portal are as follows:</td>
                </tr>

                <tr>
                    <td>Username:<strong style="color:#000"><?php echo $username; ?></strong></td>
                </tr>

                <tr>
                    <td>Password: <strong style="color:#000"><?php echo $password; ?></strong></td>
                </tr>
              <tr>
                            <td>On the portal you can create/edit the customers and can also create their invoices.</td></tr>


                    <tr>
                        <td>You can make the payment of customer's invoices through pay4me portal(<a href="www.pay4me.com" style="color:#0c5c89;"><i>www.pay4me.com</i></a>)</td>
                    </tr>                  

                <tr>
                    <td>
                        Regards,<br />
                        The Pay4bill Team
                    </td>
                </tr>

                <tr><td style="background-color:#18648e; color:#ffffff; padding:10px; font:14px Arial, Helvetica, sans-serif;">If you experience any problem, write to us at <a href="mailto:support@pay4bill.com" style="color:#fff">
                            support@pay4bill.com</a></td></tr>

            </table>

        </table>


    </body>
</html>
<?php

//$partialVars = array('customerName' => $customer_name,'username'=>$username,'complete_url'=>$complete_url,'password'=>$password,'merchantName'=>$merchantName);
$emailText = "Dear $teller_name,<br><br> You have been registered as a Teller on Pay4bill portal by the portal admin. Your login details are in two parts:
<br><br><b>Username:</b> $username<br><b>Password:</b> $password
<br><br>On the portal you can create/edit the customers and can also create their invoices.
<br> You can make the payment of customer's invoices through pay4me portal(<i>www.pay4me.com</i>)
<br><br>Regards,<br>The Pay4bill Team";
//echo $emailText;
