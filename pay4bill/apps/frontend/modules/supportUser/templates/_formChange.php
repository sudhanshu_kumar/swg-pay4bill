<?php include_stylesheets_for_form($form) ?>
<?php include_javascripts_for_form($form) ?>


<p><form action="<?php echo url_for('supportUser/savechange'); ?>" method="post" class="dlForm" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?> class="dlForm">

    <?php echo $form->renderHiddenFields(false) ?>
    <?php /* if (!$form->getObject()->isNew()): ?>
        <input type="hidden" name="sf_method" value="put" />
    <?php endif;*/ ?>

    <?php echo $form->renderGlobalErrors() ?>
        <div id="Formwraper">
                <h3>Change Password</h3>

        <div class="TableForm">
          <table>
            <tr>
                <td width="35%" align="right"><?php echo $form['old_password']->renderLabel() ?></td>
                <td><?php echo $form['old_password']->render() ?><?php echo $form['old_password']->renderError(); ?></td>
            </tr>
            <tr>
                <td align="right"><?php echo $form['password']->renderLabel() ?></td>
                <td><?php echo $form['password']->render() ?><?php echo $form['password']->renderError(); ?></td>
            </tr>
            <tr>
                <td  align="right"><?php echo $form['confirm_password']->renderLabel() ?></td>
                <td><?php echo $form['confirm_password']->render(); echo $form['confirm_password']->renderError(); ?></td>
            </tr>
            <?php echo $form['_csrf_token']->render(); ?>
          </table>
        </div>

            <div class="button-position">
                <input type="submit" name="change_pass" class="blueBtn" value="Change Password"/>
            </div>
        </div>

</form>











