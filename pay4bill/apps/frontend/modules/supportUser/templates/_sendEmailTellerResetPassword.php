<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Untitled Document</title>
    </head>
    <body>
        <table align="center" cellpadding="0" cellspacing="0" style="width:650px;">
            <tr>
                <td style="padding-bottom:5px;">
                    <?php include_partial('global/pay4bill_logo', array('path' => $path)); ?> 
                </td>
            </tr>
            <tr>
                <td>
                    <table cellspacing="0" cellpadding="10" border="0" align="center" bgcolor="#f0f0f0" style="width:650px; border-collapse:collapse; border:1px solid #dedcdc; color:#646464; font-family:Arial, Helvetica, sans-serif; font-size:14px;">
                        <tr>
                            <td>Dear  <span><?php echo $username; ?></span>,</td>
                        </tr>
                        <tr>
                            <td>This is an electronic registration system generated mail from pay4bill Portal.</td>
                        </tr>

                        <tr>
                            <td>Your password has been changed by the pay4bill support team. To access pay4bill portal, please use the below password:</td>
                        </tr>
                        <tr>
                            <td>Your new password is : <strong style="color:#000"><?php echo $password; ?></strong></td>
                        </tr>
                        <tr>
                            <td>Now you can login to pay4bill portal using this password. Your new password will need to be changed after your first login.</td>
                        </tr>
                        <tr>
                            <td>
                                Regards,<br />
                                The Pay4bill Team
                            </td>
                        </tr>
                        <tr><td style="background-color:#18648e; color:#ffffff; padding:10px; font:14px Arial, Helvetica, sans-serif;">If you experience any problem, write to us at <a href="mailto:support@pay4bill.com" style="color:#fff">
                                    support@pay4bill.com</a></td></tr>

                    </table>
                </td>
            </tr>
        </table>


    </body>
</html>

