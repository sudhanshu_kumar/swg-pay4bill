<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Untitled Document</title>
    </head>
    <body>
        <table align="center" cellpadding="0" cellspacing="0" style="width:650px;">
            <tr><td style="padding-bottom:5px;"><?php include_partial('global/pay4bill_logo', array('path' => $path));?></td></tr>
            <tr><td>
            <table cellspacing="0" cellpadding="10" border="0" align="center" bgcolor="#f0f0f0" style="width:650px; border-collapse:collapse; border:1px solid #dedcdc; color:#646464; font-family:Arial, Helvetica, sans-serif; font-size:14px;">
                <tr><td>Dear  <span><?php echo $support_user_name; ?></span>,</td></tr>
                <tr><td>
                        <?php 
                          switch ($status) { 
                            case 0: 
                                echo "Your account on pay4bill portal has been disabled by the the portal admin.
                                      To access the account kindly contact the site administrator."; break; 
                            case 1:                                 
                                echo "Your account has been enabled on pay4bill portal by the the portal admin.
                                      You can again reset the password of the teller(s) on the portal."; break; 
                           } 
                         ?>                      
                    </td>
                </tr>
                <tr><td>Regards,<br />The Pay4bill Team</td></tr>
                <tr><td style="background-color:#18648e; color:#ffffff; padding:10px; font:14px Arial, Helvetica, sans-serif;">If you experience any problem, write to us at <a href="mailto:support@pay4bill.com" style="color:#fff">
                     support@pay4bill.com</a></td></tr>
            </table>
            </td></tr>
        </table>
    </body>
</html>

