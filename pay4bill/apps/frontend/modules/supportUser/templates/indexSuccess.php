<?php
include_partial('subHeader', array('addClassTo' => $addClassTo));
?>
<script>
    $('#SupportUser').addClass('active');
</script>
<?php if (!empty($msg)) {
?><div class="clear_new"></div> <?php } ?>

<div id="mainnew">
    <?php
    $sf = sfContext::getInstance()->getUser();
    if ($sf->hasFlash('notice')) {
    ?><div id="flash_notice" class="msg_serverside" ><?php
        echo nl2br($sf->getFlash('notice'));
    ?></div><br/><?php } ?>
    <div id="wrappernew">
        <div id="content">
            <div id="Formwraper">


                <!-- Search starts here -->

                <h3>Search Users</h3>
                <div id="msg"></div>
                <div class="TableForm" id="search">

                    <form action="<?php echo url_for('supportUser/index'); ?>" method="post" id="search_form" onsubmit="return validateSearch()">

                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                             <tr>
                                <td width="17.5%" valign="top"><?php echo "Username" ?> </td>
                                <td  width="35%" valign="top">
                                    <input class="FormInput9" type="textbox" name="user_name" value="<?php echo $user_name; ?>">
                                </td>

                            </tr>
                            <tr>
                                <td width="17.5%" valign="top"><?php echo "Email Address" ?> </td>
                                <td  width="35%" valign="top">
                                    <input class="FormInput9" type="textbox" name="emailaddress" value="<?php echo $emailaddress; ?>">
                                </td>

                            </tr>
                            <tr id="merchantSpecificAttributes">
                                <td colspan="3"  valign="top">
                                    <div class="Errormrg"><input type="submit" value="Search" class="blueBtn" >
                                        <input type="button" class="blueBtn" value="Reset" onClick="window.location='<?php echo url_for('supportUser/index'); ?>'"/></div>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
                <div class="clear_new"></div>
                <!-- Search ends here -->
                <div style="clear:both"></div>
                <div class="TableForm">
                    <table cellspacing="0" cellpadding="0" border="0">
                        <thead>
                        <th>Name</th>
                        <th>Username</th>
                        <th>Email Address</th>                        
                        <th>Phone Number</th>
                        <th>Action</th>

                        </thead>
                        <tbody>
                            <?php
                            if (count($support_user_profiles) > 0) {
                            ?>
                            <?php
                                $i = 0;
                                foreach ($support_user_profiles as $supportUserProfiles): $arrUserEmail = '';
                                    $i++; ?>
                            <?php if ($i % 2 != 0) {
                            ?>
                                        <tr class="odd">
                            <?php } else {
 ?><tr class=""> <?php } ?>
                                        <td><?php echo strlen($supportUserProfiles->getName())>20?substr($supportUserProfiles->getName(),0,20).'...':$supportUserProfiles->getName(); ?></td>
                                        <td><?php echo $supportUserProfiles->getSfGuardUser()->getUsername() ?></td>
                                        <td>
                                            <?php echo strlen($supportUserProfiles->getSfGuardUser()->getEmailAddress())>30?substr($supportUserProfiles->getSfGuardUser()->getEmailAddress(),0,28).'...':$supportUserProfiles->getSfGuardUser()->getEmailAddress(); ?>
                                        <td><?php echo $supportUserProfiles->getPhoneNumber() ?></td>
                                        <td>
                                    <?php
                                    echo link_to(image_tag('tdEdit.png'), 'supportUser/edit?id=' . cryptString::encrypt($supportUserProfiles->getId()), array('title' => 'Edit', 'alt' => 'Edit')); ?>&nbsp;&nbsp;
                                    <?php
                                    if ($supportUserProfiles->getSfGuardUser()->getIsActive() == '1') {
                                        echo link_to(image_tag('enable.gif'), 'supportUser/disableUser?user_id=' . cryptString::encrypt($supportUserProfiles->getUserId()), array('title' => 'Disable User', 'alt' => 'Disable User', 'class' => 'delete', 'confirm' => 'Are you sure you want to disable this user?'));
                                    } else {
                                        echo link_to(image_tag('disable.gif'), 'supportUser/enableUser?user_id=' . cryptString::encrypt($supportUserProfiles->getUserId()), array('title' => 'Enable User', 'alt' => 'Enable User', 'class' => 'delete', 'confirm' => 'Are you sure you want to enable this user?'));
                                    }
                                    ?>
                                    &nbsp;&nbsp;
                                </td>
                            </tr>
<?php endforeach; ?>
                            <?php
                                } else {
                                    echo "<tr><td colspan='8' align='center'>No User Found</td></tr>";
                                }
                            ?>
                            </tbody>
                        </table>

                    </div>
                    <div class="nofoList">
                    <?php
                                include_partial('global/records_number', array('amount_footer' => false, 'current_offset' => isset($current_offset) ? $current_offset : null, 'offset_next' => isset($offset_next) ? $offset_next : null, 'total_no_of_records' => isset($total_no_of_records) ? $total_no_of_records : count($support_user_profiles), 'no_of_records_per_page' => isset($no_of_records_per_page) ? $no_of_records_per_page : null, 'from' => ''));
                    ?>
                            </div>
                            <div align="right">
                    <?php
                                if ($setPagination) {
                                    include_partial('global/pagination', array('moduleName' => $module, 'amount_footer' => false, 'actionName' => $actionTobeCalled, 'current_offset' => $current_offset, 'offset_next' => $offset_next, 'offset_prev' => $offset_prev, 'offset_last' => $offset_last, 'search' => '', 'from' => '', 'user_name' => isset($user_name) ? $user_name : '', 'emailaddress' => isset($emailaddress) ? $emailaddress : ''));
                                }
                    ?></div>

                        </div>
                    </div>
    </div>
</div>
