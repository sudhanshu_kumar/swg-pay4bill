<div id="pgMenu">
    <div id="wrappernew">
        <div class="pgTitle">
            <h2>Support Users</h2>
        </div>
        <div class="pgNav">
            <ul>
                <li <?php if($addClassTo=='index'){ ?>class="current" <?php } ?>><a href="<?php echo url_for('supportUser/index?offset=0') ?>">View Users</a></li>
                <li <?php if($addClassTo=='new'){ ?>class="current" <?php } ?>><a href="<?php echo url_for('supportUser/new') ?>">Create User</a></li>
<!--                <li <?php //if($addClassTo=='upload'){ ?>class="current" <?php //} ?>><a href="<?php //echo url_for('bankTeller/upload') ?>">Upload CSV</a></li>-->
            </ul>
        </div>
    </div>
</div>
