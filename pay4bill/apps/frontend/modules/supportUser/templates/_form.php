<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>
<script>
    $('#SupportUsers').addClass('active');
</script>
<div id="Formwraper">
    <div>
        <?php 
            if ($new) echo '<h3>Create User</h3>'; else echo '<h3>Edit User\'s Information</h3>';
        ?>
    </div>
    <form  action="<?php
        if ($new) {
            echo url_for('supportUser/create');
        } else {
            echo url_for('supportUser/saveEditedSupportUser?id=' . $supportUserProfileId);
        } ?>" method="post" class="dlForm" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?> >
        <div class="TableForm">
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="35%" align="right"><?php echo $form['name']->renderLabel(); ?><span class="redLbl">*</span></td>
                    <td><?php echo $form['name']->render(); ?><?php echo $form['name']->renderError(); ?></td>
                </tr>
                <tr>
                    <td width="35%" align="right"><?php echo $form['sex']->renderLabel(); ?><span class="redLbl">*</span></td>
                    <td><?php echo $form['sex']->render(); ?><?php echo $form['sex']->renderError(); ?></td>
                </tr>
                <tr>
                    <td width="35%" align="right"><?php echo $form['address']->renderLabel(); ?><span class="redLbl">*</span></td>
                    <td><?php echo $form['address']->render(); ?><?php echo $form['address']->renderError(); ?></td>
                </tr>
                <tr>
                    <td width="35%" align="right"><?php echo $form['phone_number']->renderLabel(); ?><span class="redLbl">*</span></td>
                    <td><?php echo $form['phone_number']->render(); ?><?php echo $form['phone_number']->renderError(); ?></td>
                </tr>
                <tr>
                    <td width="35%" align="right"><?php echo $form['email_address']->renderLabel(); ?><span class="redLbl">*</span></td>
                    <td><?php echo $form['email_address']->render(); ?><?php echo $form['email_address']->renderError(); ?></td>
                </tr>
                <?php if ($new) { ?>
                    <tr>
                        <td width="35%" align="right"><?php echo $form['username']->renderLabel(); ?><span class="redLbl">*</span></td>
                    <td><?php echo $form['username']->render(); ?><?php echo $form['username']->renderError(); ?></td>
                </tr>
                <tr>
                    <td width="35%" align="right"><?php echo $form['password']->renderLabel(); ?><span class="redLbl">*</span></td>
                    <td><?php echo $form['password']->render(); ?><?php echo $form['password']->renderError(); ?></td>
                </tr>
                <tr>
                    <td width="35%" align="right"><?php echo $form['confirm_password']->renderLabel(); ?></td>
                    <td><?php echo $form['confirm_password']->render(); ?><?php echo $form['confirm_password']->renderError(); ?></td>
                </tr>
                <?php } ?>
                <tr>
                    <td width="35%" align="right"></td>
                    <td><input type="submit" name="save" class="blueBtn" value="Save"/>
                        <input type="button" name="cancel" id="cancel_button"  class="blueBtn" value="Cancel" onclick="javascript: window.location='<?php echo url_for('supportUser/index') ?>'"/></td>
                </tr>
            </table>
        </div>
    </form>
    <p>&nbsp;</p>
</div>

