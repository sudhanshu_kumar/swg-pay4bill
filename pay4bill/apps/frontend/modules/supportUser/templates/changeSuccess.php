<?php use_helper('ePortal') ?>
<script language="javascript" >
    $(document).ready(function(){
        $('#Tellers').addClass('active');
        $('form').attr('id','change_password_frm');
    });
</script>
<div id="pgMenu">
    <div id="wrappernew">
        <div class="pgTitle">
            <h2>Tellers</h2>
        </div> 
    </div>
</div>
<div class="form_section">
<?php echo ePortal_pagehead("", array('class' => 'msg_serverside')); ?><br/>
<?php include_partial('formChange', array('form' => $form)) ?>
</div>