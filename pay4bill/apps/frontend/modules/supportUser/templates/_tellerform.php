<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>
<script>$('#Tellers').addClass('active');</script>
<div id="Formwraper">
    <div><h3>Teller's Information</h3></div>

    <form  action="<?php echo url_for('supportUser/sendNewPassword?id=' . $tellerProfileId); ?>" method="post" class="dlForm" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?> >
        <div class="TableForm">
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="35%" align="right"><?php echo $form['name']->renderLabel(); ?></td>
                    <td><?php echo $defaultvalues['name']; ?></td>
                </tr>
                <tr>
                    <td width="35%" align="right"><?php echo $form['sex']->renderLabel(); ?></td>
                    <td><?php echo $defaultvalues['sex']; ?></td>
                </tr>
                <tr>
                    <td width="35%" align="right"><?php echo $form['address']->renderLabel(); ?></td>
                    <td><?php echo $defaultvalues['address']; ?></td>
                </tr>
                <tr>
                    <td width="35%" align="right"><?php echo $form['phone_number']->renderLabel(); ?></td>
                    <td><?php echo $defaultvalues['phone_number']; ?></td>
                </tr>
                <tr>
                    <td width="35%" align="right"><?php echo $form['email_address']->renderLabel(); ?></td>
                    <td><?php echo $defaultvalues['email_address']; ?></td>
                </tr>

                <tr>
                    <td width="35%" align="right"><?php echo $form['merchants']->renderLabel(); ?></td>
                    <td>
                    <?php   $flg = 0;
                            foreach($defaultvalues['merchants'] as $key=>$val){ 
                                $merchant = Doctrine::getTable('MerchantProfile')->getMerchantInfoById($val);                    
                                echo $merchant[0]['merchant_name']; 
                                if(count($defaultvalues['merchants']) > 1){ 
                                    $flg++;
                                    if($flg < count($defaultvalues['merchants'])) echo ', '; 
                                }
                          } 
                   ?></td>
                </tr>
                <tr>
                    <td width="35%" align="right"><?php echo $form['bankname']->renderLabel(); ?></td>
                    <td><?php echo $defaultvalues['bankname']; ?></td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-left:178px;"><input type="submit" name="save" class="blueBtn" onclick="return confirm('Are you sure you want to reset the password for this teller ?');" value="Reset Password"/>
                        <input type="button" name="cancel" id="cancel_button"  class="blueBtn" value="Cancel" onclick="javascript: window.location='<?php echo url_for('bankTeller/index') ?>'"/></td>
                </tr>
            </table>
        </div>
    </form>
    <p>&nbsp;</p>
</div>

