<?php
/**
 * supportUser actions.
 *
 * @package    nisp
 * @subpackage supportUser
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class supportUserActions extends sfActions {

    public function executeIndex(sfWebRequest $request) {
        $pay4billHelperObj = new pay4billHelper();
        $parameters = $pay4billHelperObj->trimArray($request->getParameterHolder()->getAll());
        $this->user_name = $request->hasParameter('user_name') ? $parameters['user_name'] : '';
        $this->emailaddress = $request->hasParameter('emailaddress') ? $parameters['emailaddress'] : '';

        $this->support_user_profiles = Doctrine::getTable('SupportUserProfile')->getSupportUserList($this->user_name, $this->emailaddress);
        $total_no_of_records = count($this->support_user_profiles);

        if ($total_no_of_records > sfConfig::get('app_records_per_page')) {
            $this->actionTobeCalled = $this->getActionName();
            $this->total_no_of_records = $total_no_of_records;
            $this->no_of_records_per_page = sfConfig::get('app_records_per_page');
            $this->setPagination = true;

            if (!$request->hasParameter('offset')) {
                $offset = 0;
            } else {
                $this->getUser()->getAttributeHolder()->remove('msg');
                $offset = $request->getParameter('offset');
            }
            $this->current_offset = $offset;
            $this->support_user_profiles = Doctrine::getTable('SupportUserProfile')->getSupportUserList($this->user_name, $this->emailaddress, $offset, sfConfig::get('app_records_per_page'));
            $this->module = $this->getModuleName();
            $this->offset_last = (($total_no_of_records % sfConfig::get('app_records_per_page') == 0 ? ($total_no_of_records - sfConfig::get('app_records_per_page') ) : $total_no_of_records - ($total_no_of_records % sfConfig::get('app_records_per_page'))));
            $this->offset_next = (($offset + sfConfig::get('app_records_per_page')) > $this->offset_last ? $this->offset_last : ($offset + sfConfig::get('app_records_per_page')));
            $this->offset_prev = (($offset - sfConfig::get('app_records_per_page')) < 0 ? 0 : $offset - sfConfig::get('app_records_per_page'));
        } else {
            $this->setPagination = false;
        }
        $this->addClassTo = $this->getActionName();
    }

    public function executeNew(sfWebRequest $request) {
        $this->form = new SupportUserCreationForm();
        $this->addClassTo = $this->getActionName();
    }

    public function executeCreate(sfWebRequest $request) {
        $this->forward404Unless($request->isMethod(sfRequest::POST));
        $this->form = new SupportUserCreationForm();
        $this->processForm($request, $this->form);
        $this->addClassTo = 'new';
        $this->setTemplate('new');
    }

    public function executeSaveEditedSupportUser(sfWebRequest $request) {
        $this->addClassTo = $this->getActionName();
        $supportUser = Doctrine::getTable('SupportUserProfile')->find(cryptString::decrypt($request->getParameter('id')));

        $inputValues = $this->createInputValuesArray($request);
        $this->form = $form = new SupportUserCreationForm(array(), $inputValues);
        $this->supportUserProfileId = $request->getParameter('id');

        $supportUser = Doctrine::getTable('SupportUserProfile')->find(cryptString::decrypt($request->getParameter('id')));
        $this->getUser()->setAttribute('existing_email_address', $supportUser->getSfGuardUser()->getEmailAddress());

        $form->bind($inputValues);

        if ($form->isValid()) {
            $this->getUser()->getAttributeHolder()->remove('existing_email_address');
            try {
                $con = Doctrine::getTable('SupportUserProfile')->getConnection();
                $con->beginTransaction();
                $supportUser->setAddress($request->getParameter('address'));
                
                $supportUser->setSex($request->getParameter('sex'));
                $supportUser->setName($request->getParameter('name'));
                $supportUser->setPhoneNumber($request->getParameter('phone_number'));
                $supportUser->getSfGuardUser()->setEmailAddress($request->getParameter('email_address'));
                $supportUser->save();
                $con->commit();
            } catch (Exception $e) {
                $con->rollback();
                $this->getUser()->setFlash('error', "Due to some problem, user information couldn't be updated");
                $this->redirect('supportUser/index');
            }
            $this->getUser()->setFlash('notice', 'User Edited Successfully.');
            $this->redirect('supportUser/index');
        } else {
            $this->setTemplate('edit');
            return;
            foreach ($form->getFormFieldSchema() as $name => $formField) {
                echo $formField->getName() . "-" . $formField->getError() . "<br>"; // renders unformatted
            }
        }
    }
    
    public function executeChangePassword(sfWebRequest $request) {
        $teller_profile_id = cryptString::decrypt($request->getParameter('id'));
        $this->addClassTo = $this->getActionName();
        $this->form = new SupportUserChangePasswordForm(array(),array('teller_profile_id'=>$teller_profile_id));
        $this->setTemplate('change');
        $parameters = $request->getParameterHolder()->getAll();
    }

    public function executeEdit(sfWebRequest $request) {
        $this->forward404Unless($support_user_profile = Doctrine::getTable('SupportUserProfile')->find(cryptString::decrypt($request->getParameter('id'))), sprintf('Object support_user_profile does not exist (%s).', cryptString::decrypt($request->getParameter('id'))));
        $this->supportUserProfileId = $request->getParameter('id');
        $array_of_defaults = array('name' => $support_user_profile->getName(), 'email_address' => $support_user_profile->getSfGuardUser()->getEmailAddress()
            , 'sex' => $support_user_profile->getSex(), 'address' => $support_user_profile->getAddress(), 'phone_number' => $support_user_profile->getPhoneNumber());
        $this->form = new SupportUserCreationForm(array(), $array_of_defaults);
        $this->addClassTo = $this->getActionName();
    }
    
    public function executeResetTellerPassword(sfWebRequest $request) {
        $this->forward404Unless($teller_profile = Doctrine::getTable('TellerProfile')->find(cryptString::decrypt($request->getParameter('id'))), sprintf('Object teller_profile does not exist (%s).', cryptString::decrypt($request->getParameter('id'))));
        $this->tellerProfileId = $request->getParameter('id');
        $exististingMerchantTellerMappingObj = Doctrine::getTable('TellerMerchantMapping')->findBy('teller_profile_id', $teller_profile->getId());
        $merchantsArray = array();
        foreach ($exististingMerchantTellerMappingObj as $obj) {
            array_push($merchantsArray, $obj->getMerchantProfileId());
        }
        $array_of_defaults = array('name' => $teller_profile->getName(), 'email_address' => $teller_profile->getSfGuardUser()->getEmailAddress()
            , 'sex' => $teller_profile->getSex(), 'address' => $teller_profile->getAddress(), 'bankname' => $teller_profile->getBankname(), 'phone_number' => $teller_profile->getPhoneNumber(), 'merchants' => $merchantsArray);
        $this->allDefaultValuesForTemplate = $array_of_defaults;
        $this->form = new TellerCreationForm(array(), $array_of_defaults);
        $this->addClassTo = $this->getActionName();
    }

    public function executeUpdate(sfWebRequest $request) {
        $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
        $this->forward404Unless($support_user_profile = Doctrine::getTable('SupportUserProfile')->find(array($request->getParameter('id'))), sprintf('Object support_user_profile does not exist (%s).', $request->getParameter('id')));
        $this->form = new SupportUserProfileForm($support_user_profile);

        $this->processForm($request, $this->form);

        $this->setTemplate('edit');
    }

    private function createInputValuesArray($request) {
        $selected_merchants = is_array($request->getParameter('merchants')) ? array_values($request->getParameter('merchants')) : array();
        return array('name' => $request->getParameter('name'),
            'email_address' => $request->getParameter('email_address')
            , 'sex' => $request->getParameter('sex'), 'address' => $request->getParameter('address'),
            'phone_number' =>
            $request->getParameter('phone_number'), 'merchants' => $selected_merchants
            , 'username' => $request->getParameter('username'),
            'password' => $request->getParameter('password'),
            'confirm_password' => $request->getParameter('confirm_password'));
    }

    protected function processForm(sfWebRequest $request, sfForm $form) {

        $form->bind($this->createInputValuesArray($request));

        if ($form->isValid()) {
            try {
                $con = Doctrine::getTable('sfGuardUser')->getConnection();
                $con->beginTransaction();
                $userObj = new sfGuardUser();
                $userObj->setUsername($request->getParameter('username'));
                $userObj->setPassword($request->getParameter('password'));
                $userObj->setIsActive(1);
                $userObj->link('Groups', pay4billHelper::getGroupIdsArrayFromGroupNames(array('support')));
                $userObj->setEmailAddress($request->getParameter('email_address'));
                
                $supportUser = new SupportUserProfile();
                $supportUser->setAddress($request->getParameter('address'));
                $supportUser->setSex($request->getParameter('sex'));
                $supportUser->setName($request->getParameter('name'));
                $supportUser->setPhoneNumber($request->getParameter('phone_number'));
                $supportUser->setSfGuardUser($userObj);
                $userObj->save();
                $supportUser->save();
                $con->commit();
                $this->sendEmailOnSupportUserCreation($supportUser, $request->getParameter('password'));
            } catch (Exception $e) {
                $con->rollback();
            }
        } else {
            return;
            foreach ($form->getFormFieldSchema() as $name => $formField) {
                echo $formField->getName() . "-" . $formField->getError() . "<br>"; // renders unformatted
            }
        }
        $this->getUser()->setFlash('notice', 'User Added Successfully.');
        $this->redirect('supportUser/index');
    }

    /*
     * Function to enable the user
     * @ Saurabh on 21 Dec
     */

    public function executeEnableUser(sfWebRequest $request) {

        $user_id = cryptString::decrypt($request->getParameter('user_id')); // Support User ID

        $ob = Doctrine::getTable('sfGuardUser')->find($user_id);
        $ob->setIsActive(1);
        $ob->save();
        $this->sendEmailOnSupportUserActivationStatusChange($ob->getSupportUser(),1);

        $this->getUser()->setFlash('notice', 'User Enabled Successfully.');
        $this->redirect('supportUser/index');
    }

    /*
     * Function to disable the user
     * @ Saurabh on 19 July
     */

    public function executeDisableUser(sfWebRequest $request) {
        $user_id = cryptString::decrypt($request->getParameter('user_id')); // Customer ID

        $ob = Doctrine::getTable('sfGuardUser')->find($user_id);
        $ob->setIsActive(0);
        $ob->save();
        $this->sendEmailOnSupportUserActivationStatusChange($ob->getSupportUser(),0);

        /* 1-Nov-2012
         * If User is disabled from Admin, and at the same time User is already loggedin in another browser/machine,
         * then User should be logged out as admin has disabled the User. So User is no longer to perform any further 
         * action.
         */
        $sessionTableObj = Doctrine::getTable('AppSessions')->findByUserId($user_id);
        $sessionTableObj->delete();
        /*         * *************** */

        $this->getUser()->setFlash('notice', 'User Disabled Successfully.');
        $this->redirect('supportUser/index');
    }

    private function sendEmailOnSupportUserCreation($userProfileObj, $password) {
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
        $taskId = EpjobsContext::getInstance()->addJob('SendSupportUserCreationMail', $this->moduleName . "/sendEmail", array('email' => $userProfileObj->getSfGuardUser()->getEmailAddress(), 'name' => $userProfileObj->getName(), 'partial_name' => 'supportUserCreationEmail', 'username' => $userProfileObj->getSfGuardUser()->getUsername(), 'password' => $password, 'image_tag' => public_path(sfConfig::get('app_logo_path'),true)), '-1', 'frontend');

        $this->logMessage("scheduled mail job with id: $taskId", 'debug');
    }

    private function sendEmailOnSupportUserActivationStatusChange($userProfileObj, $isActive) {
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
        switch($isActive){
            case 0: $subject = 'Account Disable Notification'; break;
            case 1: $subject = 'Account Enable Notification'; break;
            default:$subject = ''; break;
        }
        $taskId = EpjobsContext::getInstance()->addJob('SendSupportUserAccountStatusEmail', $this->moduleName . "/sendEmail", array('subject'=>$subject, 'email' => $userProfileObj->getSfGuardUser()->getEmailAddress(), 'name' => $userProfileObj->getName(), 'status' => $userProfileObj->getSfGuardUser()->getIsActive(), 'partial_name' => 'supportUserStatusChangeEmail', 'image_tag' => public_path(sfConfig::get('app_logo_path'),true)), '-1', 'frontend');

        $this->logMessage("scheduled mail job with id: $taskId", 'debug');
    }

    public function executeSendEmail(sfWebRequest $request) {

        $email = $request->getParameter('email');
        $user_name = ucfirst($request->getParameter('name'));
        $subject = strlen($request->getParameter('subject'))>0?$request->getParameter('subject'):'Welcome to pay4bill';
        $partialVars = array('support_user_name' => $user_name);
        if ($request->hasParameter('username') && $request->hasParameter('password')) {


            $partialVars['username'] = $request->getParameter('username');
            $partialVars['password'] = $request->getParameter('password');
        } else {
            $partialVars['status'] = $request->getParameter('status');
        }
        $partialVars['path'] = $request->getParameter('image_tag');
        $partialName = $request->getParameter('partial_name');

        /* Mail firing code starts */
        $mailTo = $email; 
        $mailToGroup = array();

        $mailFrom = sfConfig::get('app_sender');
        $signature = sfConfig::get('app_sender');


        $mailingOptions = array('mailFrom' => $mailFrom, 'mailTo' => $mailTo, 'mailSubject' => $subject);

        $mailObj = new Mailing();
        $mailInfo = $mailObj->sendMail($partialName, $partialVars, $mailingOptions);
        return $this->renderText($mailInfo);
    }    
    
        public function executeSendEmailPasswordReset(sfWebRequest $request) {
        $subject = " Your new password for pay4bill portal";
        $cmo_reg_id = $request->getParameter('cmo_reg_id');
        $img = $request->getParameter('img');
        $name = $request->getParameter('name');
        $email = $request->getParameter("email");
        $pwd = $request->getParameter("pwd");
        $partialName = 'sendEmailTellerResetPassword';
        /*
         * Mail firing code starts */
        $mailTo = $email;
        $mailToGroup = array();

        $mailFrom = sfConfig::get('app_sender');
        $signature = sfConfig::get('app_sender');

        $partialVars = array('path'=>  $request->getParameter('image_tag'),'username' => $name, 'password' => $pwd, 'img' => $img);

        $mailingOptions = array('mailFrom' => $mailFrom, 'mailTo' => $mailTo, 'mailSubject' => $subject);

        $mailObj = new Mailing();
        $mailInfo = $mailObj->sendMail($partialName, $partialVars, $mailingOptions);

        return $this->renderText($mailInfo);
    }
    
    //save the new password
    public function executeSavechange(sfWebRequest $request) {
        $this->forward404Unless($request->isMethod('post') || $request->isMethod('put'));
        $this->form = new SupportUserChangePasswordForm();
      
        if ($this->savePasswordForm($request, $this->form, false)) {
            $this->getUser()->setFlash('notice', "Password has been  changed successfully", false);
            $this->forward($this->getModuleName(), 'changePassword');
        }
        $this->addClassTo = 'changePassword';
        $this->setTemplate('change');
    }

    protected function savePasswordForm(sfWebRequest $request, sfForm $form, $redirect=true) {
        
        $form->bind($request->getParameter($form->getName()));
        if ($form->isValid()) {

            $sf_guard_user = $form->save($form);
//            if ($redirect) $this->forward('userAdmin', 'edit');
            return true;
        }
        return false;
    }
    public function executeSendNewPassword(sfWebRequest $request){
        
        $this->forward404Unless($request->getParameter('id'));
        
        $teller_id = cryptString::decrypt($request->getParameter('id'));
        $tellerProfile = Doctrine::getTable('TellerProfile')->find($teller_id);
        $userId = $tellerProfile->getUserId();        

        $result = Doctrine_Query::create()
                        ->select('*')
                        ->from('SfGuardUser')
                        ->andWhere('id=?', $userId)
                        ->andWhere('is_active=?', 1)
                        ->execute();
        $resultData = $result->toArray();
        if ($resultData) {
             $password = substr(md5(rand(100000, 999999)), 0, 8);
            
            $email = $resultData ['0']['email_address'];
            $name = $resultData ['0']['username'];
//            $userData = array('name' => $name, 'email' => $email, 'pwd' => $password);
//
            $this->getContext()->getConfiguration()->loadHelpers('crumb');
            sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
//            $this->redirect('supportUser/sendEmail');
            
            $taskId = EpjobsContext::getInstance()->addJob('sendMailTellerResetPassword', $this->moduleName . "/SendEmailPasswordReset", array('name' => $name, 'email' => $email, 'pwd' => $password, 'img' => returnLogoPath(),'image_tag'=>public_path(sfConfig::get('app_logo_path'),true)), '-1', 'frontend');
            $this->logMessage("sceduled mail job with id: $taskId", 'debug');
            $user = Doctrine::getTable('sfGuardUser')->find($userId);
            $user->setPassword($password);
            $user->setLastLogin(null);
            $user->save();
            $tellerProfile->setChangePasswordRequired(true);
            $tellerProfile->save();

            // Using epActionAuditPlugin to keep track which user changes the password
            $userDetailsObj = $this->getUser()->getGuardUser();
            $applicationArr = array(new EpAuditEventAttributeHolder(EpAuditEvent::$ATTR_TELLERINFO,$name,$userId));
            $eventHolder = new EpAuditEventHolder(
                EpAuditEvent::$CATEGORY_SECURITY,
                EpAuditEvent::$SUBCATEGORY_SECURITY_RESET_PASSWORD,
                EpAuditEvent::getFomattedMessage(EpAuditEvent::$MSG_SUBCATEGORY_TELLER_PASSWORD_RESET, array('username'=>$name)),
                $applicationArr);
            $this->dispatcher->notify(new sfEvent($eventHolder, 'epAuditEvent'));
            // End of epActionAuditPlugin
            
            $this->successMsg = "You have reset the password of <b>".$name."</b>. The new password is: <b>".$password."</b>";
            $this->getUser()->setFlash("successMsg", $this->successMsg);
            $this->redirect("bankTeller/index");
        } else {
            $this->errorMsg = "There is some error. Please contact to Administrator.";
        }
    }
   

}
