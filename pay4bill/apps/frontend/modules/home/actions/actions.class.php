<?php

/**
 * home actions.
 * @package    sec
 * @subpackage home
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class homeActions extends sfActions {

    /**
     * Executes index action
     *
     * @param sfRequest $request A request object
     */
    public function executeIndex(sfWebRequest $request) {

        $this->getRequest()->setAttribute('addClassTo', 'home');
        $class = sfConfig::get('app_sf_guard_plugin_signin_form', 'sfGuardFormSignin');
        $this->form = new $class();
//if user's is forcely coming at home page after log-in
        if ($this->getUser()->getGuardUser()) {
            $this->redirect('@sf_guard_signout');
        }
        if (count($request->getParameterHolder()->getAll()) == 3) {
            $this->customerLogin = true;
        } else {
            $this->customerLogin = false;
        }
    }

    /**
     * Varun : Functionality added to view paid invoice details from home page
     */
    public function executeViewInvoice(sfWebRequest $request) {
        $invoice_number = $request->getParameter('invoice_number');
        $validation_number = $request->getParameter('validation_number');
        //Varun : Validation number defined as empty , if in case data is not rpesne tin database .
        $validationNumber = array(); // Varun : Bug fix : FS#34885  and FS#34881
        $invoice = Doctrine::getTable('invoice')->chk_Invoice_number($invoice_number);

        if (!$invoice) {
            $invoice = Doctrine::getTable('invoice')->findPaidInvoiceNo($invoice_number);

            if (count($invoice) > 0) {
                $invoice_id = $invoice[0]['id'];


                $invoiceItem = Doctrine::getTable('BillPaymentRequest')->findByInvoiceId($invoice_id);
                for ($i = 0; $i < count($invoiceItem); $i++) {
                    $item_number = $invoiceItem[$i]['id'];
                    $epP4mRequest = Doctrine::getTable('EpP4mRequest')->findByItemNumber($item_number);
                    if ($epP4mRequest[0]['id'] != '' && isset($epP4mRequest[0]['id'])) {
                        $epP4mRequestId = $epP4mRequest[0]['id'];
                        $epP4mResponse = Doctrine::getTable('EpP4mResponse')->findByRequestId($epP4mRequestId);
                        $validationNumber[] = $epP4mResponse[0]['validation_number'];
                    }
                }
                if (in_array($validation_number, $validationNumber) != false) {
                    if ($invoice[0]['status'] == 2 || $invoice[0]['status'] == 3) {
                        $id = $invoice[0]['id'];
                        $this->redirect('invoice/showPaid?id=' . $id);
                    } else {
                        $this->getUser()->setFlash('noticePaid', "<font color='yellow'>Invoice Number entered is not Paid.</font> ", true);
                        $this->redirect('home/index');
                    }
                } else {
                    $this->getUser()->setFlash('noticePaid', "<font color='yellow'>Please enter correct combination of Invoice Number and Validation Number.</font>", true);
                    $this->redirect('home/index');
                }
            } else {
                $this->getUser()->setFlash('noticePaid', "<font color='yellow'>Invoice Number entered is not Paid.</font> ");
                $this->redirect('home/index');
            }
        } else {
            $this->getUser()->setFlash('noticePaid', "<font color='yellow'>Please enter valid Invoice Number.</font>");
            $this->redirect('home/index');
        }
    }

    public function executeError404(sfWebRequest $request) {
        if ($request->hasParameter('message')) {
            $this->respmessage = $request->getParameter('message');
        } else {
            $this->respmessage = '';
        }
        $this->setLayout('permissionsLayout');
    }

}
