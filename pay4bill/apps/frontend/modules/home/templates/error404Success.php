<?php
    $message = "";
    if (isset($respmessage)&& $respmessage=="waitingResponse") {
        $message = "Payment already done for this application."; //Varun : FS#34743
    } else if (isset($respmessage)&& $respmessage=="paymentError") {
        $message = "Payment request is not successfully completed on pay4me.";
    }else {
        $message = "<b>Oops! Page Not Found</b> <br/> The server returned a 404 response.";
    }


?>

<div class="error">
    <div id="flash_error" class="error_list" style="width:auto">
        <span>
           <?php echo $message;?>
        </span>
    </div>
</div>
<div style="float:center; margin-top:20px; width:960px;" class="">
<input name="back" type="button" class="blueBtn" value="Back" style="float:center" onclick="javascript:history.go(-1)"/>
</div>
 