<?php

/**
 * payment actions.
 *
 * @package    mysfp
 * @subpackage payment
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12479 2008-10-31 10:54:40Z fabien $
 */
class paymentActions extends sfActions {

    public function executeFail(sfWebRequest $request) {
        $this->error = $this->getUser()->getFlash('error');
    }

    // added a new paramer to upadte the same amount in request as in response table Santosh Kumar
    private function constructParamsArray($obj, $merchantServiceId, $item_no=0, $transaction_no=0, $paidAmount=0) {
        //To create Array for pay4me request object to create xml for biller-   @author-Aditi

        $return_array = array("merchant_service_id" => $merchantServiceId, "item_details" => array(
                "item_number" => $obj->getBillPaymentRequest()->getFirst()->getId(), "transaction_number" => rand(1000000, 100000000900), "name" => $obj->getCustomer()->getName(),
                "description" => "Payment from pay4me Biller",
                "price" => $obj->getAmountDue(),
                "currency" => "naira",
                "payment_type" => "online",
                "buyer_ip_address" => $_SERVER['REMOTE_ADDR'],
                "parameters" => array("cust_name" => $obj->getCustomer()->getName(), "cust_username" => $obj->getCustomer()->getCustomerId(), "bill_date" => date("M,Y", strtotime($obj->getDeliveryDate())), "merchant_name" => $obj->getMerchant()->getMerchantName())));

        if ($transaction_no != 0) {
            $return_array['item_details']['price'] = $paidAmount; //$obj->getAmountDue();
        }



        return $return_array;
    }

    private function checkIfInvoiceExists($tablename, $customerObj, $itemNo, $merchantProfileObj) {

        $latestBillPaymentReqObj = Doctrine::getTable('BillPaymentRequest')->findLatestInvoiceOfCustomer($customerObj->getId(), $merchantProfileObj->getId());

        if ($latestBillPaymentReqObj->count() > 0) {
            $itemNo = $latestBillPaymentReqObj->getFirst()->getId();
            $tablename = "Invoice";
            $customerObj = $latestBillPaymentReqObj->getFirst()->getInvoice();
        }
    }

    private function validateAndCreateArrayFromBillerInput($merchantServiceId, $value) {

        /* @author-Aditi- To validate search Input and return array to create XML         */
        $itemNo = 0;

        $obj = Doctrine::getTable('Invoice')->isValidInvoiceNumberForThsMerchant($value, $merchantServiceId);
//echo "<pre>";
//print_r($obj->toArray());
//exit;
        if ($obj) {

            return $return_array = $this->constructParamsArray($obj, $merchantServiceId, $itemNo);
        } else {
            return $return_array = array();
        }
    }

    private function createXML($return_array) {
        /**     @author-Aditi ---Create XML from array using pay4meV1Biller */
        if (count($return_array) > 0) {
            $mp_ob = Doctrine::getTable('MerchantProfile')->getMerchantCodeKeyfromServiceId($return_array['merchant_service_id']);

            $pay4meObj = Pay4meGatewayManagerFactory::getIntegrator('V1Biller', $mp_ob->getMerchantCode(), $mp_ob->getMerchantKey());

            return $pay4meObj->PayRequest($return_array);
        } else {

            return "<error>Record not Found</error>";
        }
    }

    private function createItemsArrayFromItemNumber($itemNo, $merchantServiceId, $transactionNumber, $paidAmount) {

        /**     @author-Aditi ---Create array of request parameters sent to pay4me in case of
          successful payment to be saved in ep_p4m_request Table---- */
        $itemOb = Doctrine::getTable('BillPaymentRequest')->find($itemNo);
        return $this->constructParamsArray(Doctrine::getTable('Invoice')->find($itemOb->getInvoiceId()), $merchantServiceId, $itemNo, $transactionNumber, $paidAmount);
    }

    /**
     * Executes penalty action
     *
     * @param sfRequest $request A requeAnuj Raj Shekhar st object
     */
    public function executePayment(sfWebRequest $request) {

        /* Purpose:  to update the part payment to full payment
         * update a variable in invoice table
         * Author : Santosh Kumar
         * date 04 Apr 2012
         */
        if ($request->getParameter('part_amount') == 'Y') {
            Doctrine::getTable('Invoice')->updateFullAmount($request->getParameter('invoice_id'), 'Y');
        } else {
            Doctrine::getTable('Invoice')->updateFullAmount($request->getParameter('invoice_id'), 'N');
        }

        // end of part payment to full payment code
        $amount = $request->getParameter('amount');
        $transactionNo = rand(1000000, 100000000900);
        $itemNo = $request->getParameter('item_number');
        $name = $request->getParameter('customer_name');
        $params = array('appno' => rand(10, 1000));
        $this->service_id = $request->getParameter('merchant_id');
        $receipt_number = $request->getParameter('receipt_number');
        $merchant_name = $request->getParameter('merchant_name');
        $description = 'Payment of pay4bill';
//        $merchant_service_id = "3124";
        $mp_ob = Doctrine::getTable('MerchantProfile')->find($request->getParameter('merchant_id'));
        $invoiceOb = Doctrine::getTable('BillPaymentRequest')->find($itemNo)->getInvoice();

        $sampleitemV1 =
                array(
                    'merchant_service_id' => $mp_ob->getMerchantServiceId(),
                    'item_details' =>
                    array(
                        'item_number' => $itemNo,
                        'transaction_number' => $transactionNo,
                        'name' => $name,
                        'description' => $description
                        , 'price' => (float) $amount,
                        'currency' => 'naira',
                        'payment_type' => 'online',
                        'buyer_ip_address' => $_SERVER['REMOTE_ADDR'],
                        'parameters' => array(
                            'cust_name' => $name,
                            'merchant_name' => $merchant_name,
                            "cust_username" => $invoiceOb->getCustomer()->getCustomerId(),
                            "bill_date" => date("M,Y", strtotime($invoiceOb->getDeliveryDate()))
                        )
                    )
        );

        $pay4meObj = Pay4meGatewayManagerFactory::getIntegrator('V1', $mp_ob->getMerchantCode(), $mp_ob->getMerchantKey());

        $return_url = $pay4meObj->PayRequest($sampleitemV1);

        $this->redirect($return_url);
    }

    /*
      executeBillerPayment:-
      Notification URL in Case of biller payments
     * Author- Aditi Malhotra


     */

    public function executeBillerPayment(sfWebRequest $request) {
        $result_array = $this->parseXMLToArray(file_get_contents('php://input'));

        if ($result_array[0] == 'biller-search') {
            /* --Search XML from Pay4me--- */
            $return_array = $this->validateAndCreateArrayFromBillerInput($result_array[1]['biller-search']['merchant-service']['id'], $result_array[1]['biller-search']['identification-num']['VALUE']);

            $return_xml = $this->createXML($return_array);

            $this->setLayout(false);


            echo $return_xml;
            die;
        } else {// for handling of payment response
            /* ----------------------Successfull Payment Notification from Pay4me------ */
            $mp_ob = Doctrine::getTable('MerchantProfile')->getMerchantCodeKeyfromServiceId($result_array[1]['payment-notification']['merchant-service']['id']);
            $item_array = $this->createItemsArrayFromItemNumber($result_array[1]['payment-notification']['merchant-service']['item']['number'], $result_array[1]['payment-notification']['merchant-service']['id'], $result_array[1]['payment-notification']['merchant-service']['item']['transaction-number']['VALUE'], $result_array[1]['payment-notification']['merchant-service']['item']['payment-information']['amount']['VALUE']);
            $pay4meObj = Pay4meGatewayManagerFactory::getIntegrator('V1Biller', $mp_ob->getMerchantCode(), $mp_ob->getMerchantKey());
            $arrResponse = $pay4meObj->PayResponse($item_array);
            if (count($arrResponse) > 0) {
                $this->updateBillStatus($arrResponse);
            } else {
                $error = true;
            }
            exit;
        }
    }

    /*     * ****** Function for returns management ************ */

    private function updateBillStatus($arrResponse) {

        if (isset($arrResponse['payment-notification']['merchant-service'])) {
            $arrDetails = $arrResponse['payment-notification']['merchant-service'];
            $payment_date = date("Y-m-d", strtotime($arrDetails['item']['payment-information']['payment-date']['VALUE']));
            $ob = Doctrine::getTable('BillPaymentRequest')->find($arrDetails['item']['number']);

            $amount_paid = $arrDetails['item']['payment-information']['amount']['VALUE'];
            if (!is_null($ob->getInvoiceId())) {
                $invoice_ob_array = Doctrine::getTable('Invoice')->getInvoiceDetailsForPaymentStatusUpdates($ob->getInvoiceId(), $ob->getMerchantId());

                $invoice_ob_to_be_updated = Doctrine::getTable('Invoice')->find($ob->getInvoiceId());

                if ($invoice_ob_array[0]['part_payment'] == 0 && count($invoice_ob_array[0]['RecurrentBilling']) == 0) {

                    $invoice_ob_to_be_updated->setPaymentDate($payment_date);
                    $invoice_ob_to_be_updated->setAmountPaid($amount_paid);
                    $invoice_ob_to_be_updated->setStatus(2);
                    $invoice_ob_to_be_updated->setAmountDue($invoice_ob_to_be_updated->getAmountDue() - $amount_paid);
                } else if ($invoice_ob_array[0]['part_payment'] == 0 && count($invoice_ob_array[0]['RecurrentBilling']) != 0) {
                    /*                     * ---------------------Manu : Invoice number generate Start---------------------------------- */
                    $invoiceNo = pay4billHelper::getInvoiceNo($invoice_ob_to_be_updated->getMerchantId());
                    /*                     * ---------------------Invoice number generate End---------------------------------- */
                    $invoice_ob_to_be_updated->setStatus(2);
                    $invoice_ob_to_be_updated->setPaymentDate($payment_date);
                    $invoice_ob_to_be_updated->setAmountPaid($amount_paid);
                    $invoice_ob_to_be_updated->setAmountDue($invoice_ob_to_be_updated->getAmountDue() - $invoice_ob_to_be_updated->getAmountPaid());
                    $new_recurring_invoice = new Invoice();

                    if ($invoice_ob_to_be_updated->getParentIdIfRecurring() != null) {
                        $new_recurring_invoice->setParentIdIfRecurring($invoice_ob_to_be_updated->getParentIdIfRecurring());
                    } else {
                        $new_recurring_invoice->setParentIdIfRecurring($invoice_ob_to_be_updated->getId());
                    }
                    $new_recurring_invoice->setMerchantId($invoice_ob_to_be_updated->getMerchantId());
                    $new_recurring_invoice->setCustomerId($invoice_ob_to_be_updated->getCustomerId());
                    // Varun : new Field added
                    $new_recurring_invoice->setTaxId($invoice_ob_to_be_updated->getTaxId());

                    $recurring_obj = Doctrine::getTable('RecurrentBilling')->findBy('invoice_id', $invoice_ob_to_be_updated->id)->getFirst();

                    switch ($recurring_obj->getType()) {

                        case 'weekly':
                            $nextInvoice = date("Y-m-d", strtotime($invoice_ob_to_be_updated->getDueDate() . "+1 week"));
                            break;
                        case 'monthly':
                            $nextInvoice = date("Y-m-d", strtotime($invoice_ob_to_be_updated->getDueDate() . "+1 month"));
                            break;
                        case 'yearly':
                            $nextInvoice = date("Y-m-d", strtotime($invoice_ob_to_be_updated->getDueDate() . "+1 year"));
                            break;
                        case 'quarterly':
                            $nextInvoice = date("Y-m-d", strtotime($invoice_ob_to_be_updated->getDueDate() . "+3 months"));
                            break;
                        case 'custom':
                            $nextInvoice = date("Y-m-d", strtotime($invoice_ob_to_be_updated->getDueDate() . "+" . $recurring_obj->getValue() . " days"));
                            break;
                    }
                    //Varun : delivery date added with recurring period
                    switch ($recurring_obj->getType()) {

                        case 'weekly':
                            $nextDeliveryDate = date("Y-m-d", strtotime($invoice_ob_to_be_updated->getDeliveryDate() . "+1 week"));
                            break;
                        case 'monthly':
                            $nextDeliveryDate = date("Y-m-d", strtotime($invoice_ob_to_be_updated->getDeliveryDate() . "+1 month"));
                            break;
                        case 'yearly':
                            $nextDeliveryDate = date("Y-m-d", strtotime($invoice_ob_to_be_updated->getDeliveryDate() . "+1 year"));
                            break;
                        case 'quarterly':
                            $nextDeliveryDate = date("Y-m-d", strtotime($invoice_ob_to_be_updated->getDeliveryDate() . "+3 months"));
                            break;
                        case 'custom':
                            $nextDeliveryDate = date("Y-m-d", strtotime($invoice_ob_to_be_updated->getDeliveryDate() . "+" . $recurring_obj->getValue() . " days"));
                            break;
                    }

                    $new_recurring_invoice->setInvoiceNo($invoiceNo);
                    $new_recurring_invoice->setDueDate($nextInvoice);
                    $new_recurring_invoice->setIssueDate(date("Y-m-d"));
                    $new_recurring_invoice->setDeliveryDate($nextDeliveryDate);
                    $new_recurring_invoice->setCurrencyType($invoice_ob_to_be_updated->getCurrencyType());
                    $new_recurring_invoice->setAdditionalInformation($invoice_ob_to_be_updated->getAdditionalInformation());
                    $new_recurring_invoice->setAmountPaid(0);
//                    $new_recurring_invoice->setAmountDue($invoice_ob_to_be_updated->getAmountPaid());
                    $new_recurring_invoice->setStatus(0);
                    $new_recurring_invoice->save();
                    // Check status for tax , item tax value saved is status is active else set to '0'

                    $statusCheck = Doctrine::getTable('tax')->findTax($invoice_ob_to_be_updated->getTaxId());
                    $merchant = $invoice_ob_to_be_updated->getMerchantId();
                    $merchant_detail = Doctrine::getTable('MerchantProfile')->getMerchantInfoById($merchant);
                    $merchant_id = $merchant_detail[0]['merchant_id'];
                    ;
                    if ($statusCheck[0]['status'] == '1') { //For Active tax
                        $taxExist = Doctrine::getTable('MerchantTax')->findEntryForMerchantTax($invoice_ob_to_be_updated->getTaxId(), $merchant_id);
                        $item_tax = $taxExist[0]['tax_value'];
                    } else {
                        $item_tax = '0.00';
                    }
                    /*                     * ---------------------Manu : Item Details inserted new invoice row---------------------------------- */
                    $item_details_obj = Doctrine::getTable('InvoiceItemDetails')->findBy('invoice_id', $invoice_ob_to_be_updated->id);
                    $amountDue = 0;
                    foreach ($item_details_obj as $item_details_obj) {
                        $new_item_details_obj = new InvoiceItemDetails();
                        $new_item_details_obj->setInvoiceId($new_recurring_invoice->id);
                        $new_item_details_obj->setItemDescription($item_details_obj->getItemDescription());
                        $new_item_details_obj->setNoOfItems($item_details_obj->getNoOfItems());
                        $new_item_details_obj->setItemPrice($item_details_obj->getItemPrice());
                        $new_item_details_obj->setItemTax($item_tax);
                        $new_item_details_obj->setSubTotal($item_details_obj->getSubTotal());
                        $tax_total = round(($item_details_obj->getSubTotal() * $item_tax / 100), 2);
                        $new_item_details_obj->setTaxTotal($tax_total);
                        $amountDue +=$new_item_details_obj->getSubTotal() + $new_item_details_obj->getTaxTotal();
//                        $new_item_details_obj->setTaxTotal($item_details_obj->getTaxTotal());

                        $new_item_details_obj->save();
                    }

                    $new_recurring_invoice->setTotalAmount($amountDue);
                    $new_recurring_invoice->save();
                    // Varun : Amount due calculated for new tax
                    if ($new_recurring_invoice->getPartPayment() != '') {
                        $amtDue = $new_recurring_invoice->getPartPayment() * $amountDue / 100;
                    } else {
                        $amtDue = $amountDue;
                    }

                    $updateAmountDue = Doctrine::getTable('Invoice')->updateAmountDue($new_recurring_invoice->getId(), $amtDue);


//                    Doctrine::getTable('InvoiceItemDetails')->updateOnRecurringBillGeneration($invoice_ob_to_be_updated->id, $new_recurring_invoice->id);
                    /*                     * ---------------------End  ----------------------------------------------------------------------- */
                    Doctrine::getTable('RecurrentBilling')->updateOnRecurringBillGeneration($invoice_ob_to_be_updated->id, $new_recurring_invoice->id);
                } else if ($invoice_ob_array[0]['part_payment'] != 0 && count($invoice_ob_array[0]['RecurrentBilling']) != 0) {
                    /*                     * ---------------------Manu : Invoice number generate Start---------------------------------- */
                    $invoiceNo = pay4billHelper::getInvoiceNo($invoice_ob_to_be_updated->getMerchantId());
                    /*                     * ---------------------Invoice number generate End---------------------------------- */
                    /* Santosh Kumar
                     * Date 04 Apr 2012
                     *  if customer has paid full amount while the invoice was fro part payment
                     */


//                    echo "<pre>";
//                    print_r('dfhfghfhf');
//                    exit;
                    if ($invoice_ob_array[0]['pay_full_amount'] == 'Y' && $invoice_ob_to_be_updated->getAmountPaid() == 0) {
                        //First Tranaction
//                          echo "First Transaction";
                        $fltPartPaymentPer = 0;
                        if ($invoice_ob_to_be_updated->getPartPayment() != 0) {
                            $fltPartPaymentPer = $invoice_ob_to_be_updated->getPartPayment();
                        }
                        $invoice_ob_to_be_updated->setPaymentDate($payment_date);
                        $invoice_ob_to_be_updated->setStatus(2);
                        $receivedAmount = $invoice_ob_to_be_updated->getAmountDue() + ((100 - $invoice_ob_array[0]['part_payment']) * ($invoice_ob_array[0]['subtotal'] + $invoice_ob_array[0]['tax']) / 100);
                        $invoice_ob_to_be_updated->setAmountPaid($receivedAmount);
                        $invoice_ob_to_be_updated->setPartPayment(0);
                        $invoice_ob_to_be_updated->setAmountDue($invoice_ob_to_be_updated->getAmountDue() - $receivedAmount);
                        /*
                         * below code added for correctingh the Part payment = recuring as full payment scneraio
                         * on 30 Apr
                         * Santosh Kumar
                         */
                        //Second Transaction
                        $new_recurring_invoice = new Invoice();
                        $new_recurring_invoice->setParentIdIfRecurring($invoice_ob_to_be_updated->getId());
                        $new_recurring_invoice->setInvoiceNo($invoiceNo);

                        $new_recurring_invoice->setMerchantId($invoice_ob_to_be_updated->getMerchantId());
                        $new_recurring_invoice->setCustomerId($invoice_ob_to_be_updated->getCustomerId());
                        $new_recurring_invoice->setTaxId($invoice_ob_to_be_updated->getTaxId());

                        $new_recurring_invoice->setIssueDate(date("Y-m-d"));

                        $recurring_obj = Doctrine::getTable('RecurrentBilling')->findBy('invoice_id', $invoice_ob_to_be_updated->id)->getFirst();

                        switch ($recurring_obj->getType()) {

                            case 'weekly':
                                $nextInvoice = date("Y-m-d", strtotime($invoice_ob_to_be_updated->getDueDate() . "+1 week"));
                                break;
                            case 'monthly':
                                $nextInvoice = date("Y-m-d", strtotime($invoice_ob_to_be_updated->getDueDate() . "+1 month"));
                                break;
                            case 'yearly':
                                $nextInvoice = date("Y-m-d", strtotime($invoice_ob_to_be_updated->getDueDate() . "+1 year"));
                                break;
                            case 'quarterly':
                                $nextInvoice = date("Y-m-d", strtotime($invoice_ob_to_be_updated->getDueDate() . "+3 months"));
                                break;
                            case 'custom':
                                $nextInvoice = date("Y-m-d", strtotime($invoice_ob_to_be_updated->getDueDate() . "+" . $recurring_obj->getValue() . " days"));
                                break;
                        }
                        //Varun : delivery date added with recurring period
                        switch ($recurring_obj->getType()) {

                            case 'weekly':
                                $nextDeliveryDate = date("Y-m-d", strtotime($invoice_ob_to_be_updated->getDeliveryDate() . "+1 week"));
                                break;
                            case 'monthly':
                                $nextDeliveryDate = date("Y-m-d", strtotime($invoice_ob_to_be_updated->getDeliveryDate() . "+1 month"));
                                break;
                            case 'yearly':
                                $nextDeliveryDate = date("Y-m-d", strtotime($invoice_ob_to_be_updated->getDeliveryDate() . "+1 year"));
                                break;
                            case 'quarterly':
                                $nextDeliveryDate = date("Y-m-d", strtotime($invoice_ob_to_be_updated->getDeliveryDate() . "+3 months"));
                                break;
                            case 'custom':
                                $nextDeliveryDate = date("Y-m-d", strtotime($invoice_ob_to_be_updated->getDeliveryDate() . "+" . $recurring_obj->getValue() . " days"));
                                break;
                        }
                        $new_recurring_invoice->setDueDate($nextInvoice);
                        $new_recurring_invoice->setDeliveryDate($nextDeliveryDate);
                        $new_recurring_invoice->setCurrencyType($invoice_ob_to_be_updated->getCurrencyType());
                        $new_recurring_invoice->setAdditionalInformation($invoice_ob_to_be_updated->getAdditionalInformation());
                        $new_recurring_invoice->setPartPayment($fltPartPaymentPer);
                        $new_recurring_invoice->setAmountPaid(0);
//                        $new_recurring_invoice->setAmountDue($fltPartPaymentPer * $invoice_ob_to_be_updated->getAmountPaid() / 100);
                        $new_recurring_invoice->setStatus(0);
                        $new_recurring_invoice->save();


                        $statusCheck = Doctrine::getTable('tax')->findTax($invoice_ob_to_be_updated->getTaxId());
                        $merchant = $invoice_ob_to_be_updated->getMerchantId();
                        $merchant_detail = Doctrine::getTable('MerchantProfile')->getMerchantInfoById($merchant);
                        $merchant_id = $merchant_detail[0]['merchant_id'];
                        ;
                        if ($statusCheck[0]['status'] == '1') { //For Active tax
                            $taxExist = Doctrine::getTable('MerchantTax')->findEntryForMerchantTax($invoice_ob_to_be_updated->getTaxId(), $merchant_id);
                            $item_tax = $taxExist[0]['tax_value'];
                        } else {
                            $item_tax = '0.00';
                        }
                        /*                         * ---------------------Manu : Item Details inserted new invoice row---------------------------------- */
                        $item_details_obj = Doctrine::getTable('InvoiceItemDetails')->findBy('invoice_id', $invoice_ob_to_be_updated->id);
                        $amountDue = 0;
                        foreach ($item_details_obj as $item_details_obj) {
                            $new_item_details_obj = new InvoiceItemDetails();
                            $new_item_details_obj->setInvoiceId($new_recurring_invoice->id);
                            $new_item_details_obj->setItemDescription($item_details_obj->getItemDescription());
                            $new_item_details_obj->setNoOfItems($item_details_obj->getNoOfItems());
                            $new_item_details_obj->setItemPrice($item_details_obj->getItemPrice());
                            $new_item_details_obj->setItemTax($item_tax);
                            $new_item_details_obj->setSubTotal($item_details_obj->getSubTotal());
                            $tax_total = round(($item_details_obj->getSubTotal() * $item_tax / 100), 2);
                            $new_item_details_obj->setTaxTotal($tax_total);
//                          $new_item_details_obj->setTaxTotal($item_details_obj->getTaxTotal());
                            $amountDue +=$new_item_details_obj->getSubTotal() + $new_item_details_obj->getTaxTotal();
                            $new_item_details_obj->save();
                        }
                        $new_recurring_invoice->setTotalAmount($amountDue);
                        $new_recurring_invoice->save();
                        if ($new_recurring_invoice->getPartPayment() != '') {
                            $amtDue = $new_recurring_invoice->getPartPayment() * $amountDue / 100;
                        } else {
                            $amtDue = $amountDue;
                        }

                        $updateAmountDue = Doctrine::getTable('Invoice')->updateAmountDue($new_recurring_invoice->getId(), $amtDue);



                        //                    Doctrine::getTable('InvoiceItemDetails')->updateOnRecurringBillGeneration($invoice_ob_to_be_updated->id, $new_recurring_invoice->id);
                        /*                         * ---------------------End  ----------------------------------------------------------------------- */

                        Doctrine::getTable('RecurrentBilling')->updateOnRecurringBillGeneration($invoice_ob_to_be_updated->id, $new_recurring_invoice->id);
                    } // end of 04 Apr 2012 code
                    elseif ($invoice_ob_to_be_updated->getAmountPaid() == 0) {//First Tranaction
//                          echo "First Transaction";
                        $invoice_ob_to_be_updated->setStatus(3);
                        $invoice_ob_to_be_updated->setPaymentDate($payment_date);
                        $invoice_ob_to_be_updated->setAmountPaid($invoice_ob_to_be_updated->getAmountDue());
                        $invoice_ob_to_be_updated->setAmountDue((100 - $invoice_ob_array[0]['part_payment']) * ($invoice_ob_array[0]['subtotal'] + $invoice_ob_array[0]['tax']) / 100);
                        $new_ob = new BillPaymentRequest();
                        $new_ob->setInvoiceId($invoice_ob_to_be_updated->getId());
                        $new_ob->setMerchantId($invoice_ob_to_be_updated->getMerchantId());
                        $new_ob->setCustomerId($invoice_ob_to_be_updated->getCustomerId());
                        $new_ob->save();
                    } else { //Second Transaction
                        $invoice_ob_to_be_updated->setAmountPaid($invoice_ob_array[0]['subtotal'] + $invoice_ob_array[0]['tax']);
                        $invoice_ob_to_be_updated->setAmountDue(0);
                        $invoice_ob_to_be_updated->setPaymentDate($payment_date);
                        $invoice_ob_to_be_updated->setStatus(2);
                        $new_recurring_invoice = new Invoice();
                        $new_recurring_invoice->setParentIdIfRecurring($invoice_ob_to_be_updated->getId());
                        $new_recurring_invoice->setInvoiceNo($invoiceNo);

                        $new_recurring_invoice->setMerchantId($invoice_ob_to_be_updated->getMerchantId());
                        $new_recurring_invoice->setCustomerId($invoice_ob_to_be_updated->getCustomerId());
                        $new_recurring_invoice->setTaxId($invoice_ob_to_be_updated->getTaxId());

                        $new_recurring_invoice->setIssueDate(date("Y-m-d"));

                        $recurring_obj = Doctrine::getTable('RecurrentBilling')->findBy('invoice_id', $invoice_ob_to_be_updated->id)->getFirst();

                        switch ($recurring_obj->getType()) {

                            case 'weekly':
                                $nextInvoice = date("Y-m-d", strtotime($invoice_ob_to_be_updated->getDueDate() . "+1 week"));
                                break;
                            case 'monthly':
                                $nextInvoice = date("Y-m-d", strtotime($invoice_ob_to_be_updated->getDueDate() . "+1 month"));
                                break;
                            case 'yearly':
                                $nextInvoice = date("Y-m-d", strtotime($invoice_ob_to_be_updated->getDueDate() . "+1 year"));
                                break;
                            case 'quarterly':
                                $nextInvoice = date("Y-m-d", strtotime($invoice_ob_to_be_updated->getDueDate() . "+3 months"));
                                break;
                            case 'custom':
                                $nextInvoice = date("Y-m-d", strtotime($invoice_ob_to_be_updated->getDueDate() . "+" . $recurring_obj->getValue() . " days"));
                                break;
                        }
                        //Varun : delivery date added with recurring period
                        switch ($recurring_obj->getType()) {

                            case 'weekly':
                                $nextDeliveryDate = date("Y-m-d", strtotime($invoice_ob_to_be_updated->getDeliveryDate() . "+1 week"));
                                break;
                            case 'monthly':
                                $nextDeliveryDate = date("Y-m-d", strtotime($invoice_ob_to_be_updated->getDeliveryDate() . "+1 month"));
                                break;
                            case 'yearly':
                                $nextDeliveryDate = date("Y-m-d", strtotime($invoice_ob_to_be_updated->getDeliveryDate() . "+1 year"));
                                break;
                            case 'quarterly':
                                $nextDeliveryDate = date("Y-m-d", strtotime($invoice_ob_to_be_updated->getDeliveryDate() . "+3 months"));
                                break;
                            case 'custom':
                                $nextDeliveryDate = date("Y-m-d", strtotime($invoice_ob_to_be_updated->getDeliveryDate() . "+" . $recurring_obj->getValue() . " days"));
                                break;
                        }
                        $new_recurring_invoice->setDueDate($nextInvoice);
                        $new_recurring_invoice->setDeliveryDate($nextDeliveryDate);
                        $new_recurring_invoice->setCurrencyType($invoice_ob_to_be_updated->getCurrencyType());
                        $new_recurring_invoice->setAdditionalInformation($invoice_ob_to_be_updated->getAdditionalInformation());
                        if ($invoice_ob_to_be_updated->getPartPayment() != 0) {
                            $new_recurring_invoice->setPartPayment($invoice_ob_to_be_updated->getPartPayment());
                        }
                        $new_recurring_invoice->setAmountPaid(0);
//                        $new_recurring_invoice->setAmountDue($new_recurring_invoice->getPartPayment() * $invoice_ob_to_be_updated->getAmountPaid() / 100);
                        $new_recurring_invoice->setStatus(0);
                        $new_recurring_invoice->save();

                        $statusCheck = Doctrine::getTable('tax')->findTax($invoice_ob_to_be_updated->getTaxId());
                        $merchant = $invoice_ob_to_be_updated->getMerchantId();
                        $merchant_detail = Doctrine::getTable('MerchantProfile')->getMerchantInfoById($merchant);
                        $merchant_id = $merchant_detail[0]['merchant_id'];
                        ;
                        if ($statusCheck[0]['status'] == '1') { //For Active tax
                            $taxExist = Doctrine::getTable('MerchantTax')->findEntryForMerchantTax($invoice_ob_to_be_updated->getTaxId(), $merchant_id);
                            $item_tax = $taxExist[0]['tax_value'];
                        } else {
                            $item_tax = '0.00';
                        }
                        /*                         * ---------------------Manu : Item Details inserted new invoice row---------------------------------- */
                        $item_details_obj = Doctrine::getTable('InvoiceItemDetails')->findBy('invoice_id', $invoice_ob_to_be_updated->id);
                        $amountDue = 0;
                        foreach ($item_details_obj as $item_details_obj) {
                            $new_item_details_obj = new InvoiceItemDetails();
                            $new_item_details_obj->setInvoiceId($new_recurring_invoice->id);
                            $new_item_details_obj->setItemDescription($item_details_obj->getItemDescription());
                            $new_item_details_obj->setNoOfItems($item_details_obj->getNoOfItems());
                            $new_item_details_obj->setItemPrice($item_details_obj->getItemPrice());
                            $new_item_details_obj->setItemTax($item_tax);
                            $new_item_details_obj->setSubTotal($item_details_obj->getSubTotal());
                            $tax_total = round(($item_details_obj->getSubTotal() * $item_tax / 100), 2);
                            $new_item_details_obj->setTaxTotal($tax_total);
//                          $new_item_details_obj->setTaxTotal($item_details_obj->getTaxTotal());
                            $amountDue +=$new_item_details_obj->getSubTotal() + $new_item_details_obj->getTaxTotal();
                            $new_item_details_obj->save();
                        }
                        $new_recurring_invoice->setTotalAmount($amountDue);
                        $new_recurring_invoice->save();
                        if ($new_recurring_invoice->getPartPayment() != '') {
                            $amtDue = $new_recurring_invoice->getPartPayment() * $amountDue / 100;
                        } else {
                            $amtDue = $amountDue;
                        }

                        $updateAmountDue = Doctrine::getTable('Invoice')->updateAmountDue($new_recurring_invoice->getId(), $amtDue);


                        //                    Doctrine::getTable('InvoiceItemDetails')->updateOnRecurringBillGeneration($invoice_ob_to_be_updated->id, $new_recurring_invoice->id);
                        /*                         * ---------------------End  ----------------------------------------------------------------------- */

                        Doctrine::getTable('RecurrentBilling')->updateOnRecurringBillGeneration($invoice_ob_to_be_updated->id, $new_recurring_invoice->id);
                    }
//                    }
                } else if ($invoice_ob_array[0]['part_payment'] != 0 && count($invoice_ob_array[0]['RecurrentBilling']) == 0) {
                    /*
                     * Date: 05 Apr 2012
                     * Autor: Santosh Kumar
                     * Purpose: for update full payemnt while selecting teh aprt payment option
                     *
                     */
                    if ($invoice_ob_array[0]['pay_full_amount'] == 'Y' && $invoice_ob_to_be_updated->getAmountPaid() == 0) {//First Tranaction
                        $invoice_ob_to_be_updated->setPaymentDate($payment_date);
                        $invoice_ob_to_be_updated->setStatus(2);
                        $receivedAmount = $invoice_ob_to_be_updated->getAmountDue() + ((100 - $invoice_ob_array[0]['part_payment']) * ($invoice_ob_array[0]['subtotal'] + $invoice_ob_array[0]['tax']) / 100);
                        $invoice_ob_to_be_updated->setAmountPaid($receivedAmount);
                        $invoice_ob_to_be_updated->setPartPayment(0);
                        $invoice_ob_to_be_updated->setAmountDue(0);
                    } elseif ($invoice_ob_to_be_updated->getAmountPaid() == 0) {//First Tranaction
                        $invoice_ob_to_be_updated->setPaymentDate($payment_date);
                        $invoice_ob_to_be_updated->setStatus(3);
                        $invoice_ob_to_be_updated->setAmountPaid($amount_paid);
                        $invoice_ob_to_be_updated->setAmountDue((100 - $invoice_ob_array[0]['part_payment']) * ($invoice_ob_array[0]['subtotal'] + $invoice_ob_array[0]['tax']) / 100);
                        $new_ob = new BillPaymentRequest();
                        $new_ob->setInvoiceId($invoice_ob_to_be_updated->getId());
                        $new_ob->setMerchantId($invoice_ob_to_be_updated->getMerchantId());
                        $new_ob->setCustomerId($invoice_ob_to_be_updated->getCustomerId());
                        $new_ob->save();
                    } else {
                        $invoice_ob_to_be_updated->setAmountPaid($invoice_ob_array[0]['subtotal'] + $invoice_ob_array[0]['tax']);
                        $invoice_ob_to_be_updated->setAmountDue(0);
                        $invoice_ob_to_be_updated->setPaymentDate($payment_date);
                        $invoice_ob_to_be_updated->setStatus(2);
                    }
                }
                $invoice_ob_to_be_updated->setPaymentMode($arrDetails['item']['payment-information']['mode']['VALUE']);
                // Varun : Code added to send email to customer and merchant afetr payment is done.
                $invoice_ob_to_be_updated->save();
                $partial_name = 'paymentContentCustomer';
                $partial_name1 = 'paymentContentMerchant';
                $subject = "Payment Notification";
                $invoice = Doctrine::getTable('Invoice')->find($ob->getInvoiceId());
                $merchantDetail = Doctrine::getTable('MerchantProfile')->getMerchantInfoById($invoice['merchant_id']);
                $merchantMailAddress = '';
                $merchantName = $merchantDetail[0]['merchant_name'];
                if (isset($merchantDetail[0]['merchant_id']) && $merchantDetail[0]['merchant_id'] != '') {
                    $merchantMail = Doctrine::getTable('sfGuardUser')->findById($merchantDetail[0]['merchant_id']);
                    $merchantMailAddress = $merchantMail[0]['email_address'];
                }

                $customerDetail = Doctrine::getTable('CustomerProfile')->find($ob->getCustomerId())->toArray();

                $customerMailAddress = '';
//                echo "<pre>";
//                print_r($customerDetail);
//                exit;
                $customerName = $customerDetail['name'];
                if (isset($customerDetail['email_address']) && $customerDetail['email_address'] != '') {
                    $customerMailAddress = $customerDetail['email_address'];
                }

                $invoiceNo = '';
                $issueDate = '';
                $due_date = '';
                $additional_information = '';
                $amount_due = '';
                $amount_paid = '';
                $status = '';
                $deducted = '';
                if ($invoice != '') {

                    $status = $invoice['status'];
                    if ($status == '2') {
                        $part_payment = $invoice['part_payment'];
                        $part_payment = $part_payment / 100;
                        $amount_paid = $invoice['amount_paid'];
                        $deducted = round($amount_paid - ($amount_paid * $part_payment), 2);
                    } else {
                        $amount_paid = $invoice['amount_paid'];
                    }
                    $invoiceNo = $invoice['invoice_no'];
                    $issueDate = $invoice['issue_date'];
                    $due_date = $invoice['due_date'];
                    $additional_information = $invoice['additional_information'];
                    $amount_due = $invoice['amount_due'];
                }


                exit;
            } else {


                //update customer request
            }
            $taskId = EpjobsContext::getInstance()->addJob('SendMailNotification', $this->moduleName . "/sendCustomerEmail", array('email' => $customerMailAddress, 'customer_name' => $customerName, 'merchant_name' => $merchantName, 'subject' => $subject, 'invoiceNo' => $invoiceNo, 'issueDate' => $issueDate, 'due_date' => $due_date, 'additional_information' => $additional_information, 'partial_name' => $partial_name, 'amount_paid' => $amount_paid, 'amount_due' => $amount_due, 'deducted' => $deducted), '-1', 'frontend');
            $this->logMessage("scheduled mail job with id: $taskId", 'debug');
            $taskId = EpjobsContext::getInstance()->addJob('SendMailNotification', $this->moduleName . "/sendMerchantEmail", array('email' => $merchantMailAddress, 'customer_name' => $customerName, 'merchant_name' => $merchantName, 'subject' => $subject, 'invoiceNo' => $invoiceNo, 'issueDate' => $issueDate, 'due_date' => $due_date, 'additional_information' => $additional_information, 'partial_name' => $partial_name1, 'amount_paid' => $amount_paid, 'amount_due' => $amount_due, 'deducted' => $deducted), '-1', 'frontend');
            $this->logMessage("scheduled mail job with id: $taskId", 'debug');
        }
    }

    private function parseXMLToArray($xml) {
        $result_array = array();

        if (!is_null($xml)) {

            $xml_parser = new gc_XmlParser($xml);
            $root = $xml_parser->GetRoot();
            $data = $xml_parser->GetData();
            $result_array = array($root, $data);
        }
        return $result_array;
    }

    public function executePaymentResponse(sfWebRequest $request) {

        $arr = $this->parseXMLToArray(file_get_contents('php://input'));

        $mp_ob = Doctrine::getTable('MerchantProfile')->getMerchantCodeKeyfromServiceId($arr[1]['payment-notification']['merchant-service']['id']);

        $pay4meClient = Pay4meGatewayManagerFactory::getIntegrator('V1', $mp_ob->getMerchantCode(), $mp_ob->getMerchantKey());

        $arrResponse = $pay4meClient->PayResponse();
//         $arrResponse=array('payment-notification'=>array('merchant-service'=>array('item'=>array('number'=>1,'payment-information' =>array ('payment-date' => array(
//                                         'VALUE' => '2011-11-11T21:40:37'))))));



        $error = false;
        if (count($arrResponse) > 0) {
            $this->updateBillStatus($arrResponse);
        } else {
            $error = true;
        }
    }

    //Varun : Functions added fto send email to customer and merchant after payment is done
    public function executeSendCustomerEmail(sfWebRequest $request) {

        $email = $request->getParameter('email');
        $customerName = $request->getParameter('customer_name');
        $merchantName = $request->getParameter('merchant_name');
        $subject = $request->getParameter('subject');
        $invoiceNo = $request->getParameter('invoiceNo');
        $issueDate = $request->getParameter('issueDate');
        $dueDate = $request->getParameter('due_date');
        $additionalInfo = $request->getParameter('additional_information');
        $amountPaid = $request->getParameter('amount_paid');
        $amountDue = $request->getParameter('amount_due');
        $deducted = $request->getParameter('deducted');
        if ($deducted != '') {
            $amountPaid = $deducted;
        }

        $partialName = $request->getParameter('partial_name');

        /* Mail firing code starts */
        $mailTo = $email;
        $mailToGroup = array();

        $mailFrom = sfConfig::get('app_sender');
        $signature = sfConfig::get('app_sender');

        $partialVars = array('dueDate' => $dueDate, 'merchantName' => $merchantName, 'customerName' => $customerName, 'invoiceNo' => $invoiceNo, 'issueDate' => $issueDate, 'additionalInfo' => $additionalInfo, 'amountPaid' => $amountPaid, 'amountDue' => $amountDue, 'deducted' => $deducted);
        $mailingOptions = array('mailFrom' => $mailFrom, 'mailTo' => $mailTo, 'mailSubject' => $subject);

        $mailObj = new Mailing();
        $mailInfo = $mailObj->sendMail($partialName, $partialVars, $mailingOptions);

        return $this->renderText($mailInfo);
    }

    public function executeSendMerchantEmail(sfWebRequest $request) {

        $email = $request->getParameter('email');
        $customerName = $request->getParameter('customer_name');
        $merchantName = $request->getParameter('merchant_name');
        $subject = $request->getParameter('subject');
        $invoiceNo = $request->getParameter('invoiceNo');
        $issueDate = $request->getParameter('issueDate');
        $dueDate = $request->getParameter('due_date');
        $additionalInfo = $request->getParameter('additional_information');
        $amountPaid = $request->getParameter('amount_paid');
        $amountDue = $request->getParameter('amount_due');
        $deducted = $request->getParameter('deducted');
        if ($deducted != '') {
            $amountPaid = $deducted;
        }

        $partialName = $request->getParameter('partial_name');

        /* Mail firing code starts */
        $mailTo = $email;
        $mailToGroup = array();

        $mailFrom = sfConfig::get('app_sender');
        $signature = sfConfig::get('app_sender');

        $partialVars = array('dueDate' => $dueDate, 'merchantName' => $merchantName, 'customerName' => $customerName, 'invoiceNo' => $invoiceNo, 'issueDate' => $issueDate, 'additionalInfo' => $additionalInfo, 'amountPaid' => $amountPaid, 'amountDue' => $amountDue, 'deducted' => $deducted);
        $mailingOptions = array('mailFrom' => $mailFrom, 'mailTo' => $mailTo, 'mailSubject' => $subject);

        $mailObj = new Mailing();
        $mailInfo = $mailObj->sendMail($partialName, $partialVars, $mailingOptions);

        return $this->renderText($mailInfo);
    }

}
