<?php

       $emailText="<b>".$companyName."</b><br>".$completeUrl."<br><b>Invoice Number:</b>".$invoiceNo."<br><br><b>Due Date:</b>".date("d-M-Y",strtotime($dueDate))."<br><b>Issue Date: </b>".date("d-M-Y",strtotime($issueDate))."<br><b>Next Invoice Date: </b>".$next_invoice;
$emailText.="<br><br><b>Invoice Items</b><br>";

$emailText.="<table border=1>";
$emailText.="<tr><td><b>Quantity</b></td><td><b>Description</b></td><td><b>Cost</b></td></tr>";

        $item_info_obs = Doctrine::getTable('InvoiceItemDetails')->findBy('invoice_id', $invoice_id);
        $recur_ob = Doctrine::getTable('RecurrentBilling')->findBy('invoice_id', $invoice_id)->getFirst();
//
////$emailText.=var_dump($item_info_obs);
////        $r=new InvoiceItemDetails();
////        $r->
        $subttl=0;
        $ttl_tax=0;
foreach($item_info_obs as $item_ob)
{
    $emailText.="<tr>";
    $emailText.="<td>".$item_ob->getNoOfItems()."</td>";
    $emailText.="<td>".$item_ob->getItemDescription()."</td>";
    $emailText.="<td>".$item_ob->getItemPrice()."</td>";
    $emailText.="</tr>";
    $subttl+=$item_ob->getSubTotal();
    $ttl_tax+=$item_ob->getTaxTotal();
//
}
$emailText.="</table>";
$emailText.="<table><tr><td>Sub-Total:</td><td>$subttl NGN</td></tr><tr><td>Total-Tax:</td><td>$ttl_tax NGN</td></tr>
<tr><td>Total-NGN:</td><td>".($subttl+$ttl_tax)."NGN</td></tr>

</table>";
$emailText.=$customerName.",<br><br><br>Your ".$recur_ob->getType()." payment for ".$companyName." is almost due.In
    order to meet up with your payment , please ensure to pay your bill by ".date("d-M-Y",strtotime($dueDate)).". If you fail to do so you will have to contact ".$companyName." directly to pay and to issue a new recurring bill for future payments.
        <br><br>";
$emailText.="<br>If you have any questions regarding your account, please feel free to contact us at:<br><br>info@pay4bill.com<br>
    <br>Thank You!<br><br>The Pay4bill Team";
echo $emailText;
