<div>
    Dear User<br /><br />
    Thank you for the payment for registration on Securities and Exchange Commission, Nigeria.<br /><br />
    Please note following:<br /><br />
    <ul>
        <li>Complete the application process. </li><br />
        <li>Make additional changes (if required) before payment.</li>
    </ul>
    NOTE: <i>Notification email will be sent to you on your specified email address once we receive the payment.</i>
    <div style="margin-top: 10px;"><?php echo button_to("Home", "home/index", array("class"=>"formSubmit")); ?></div>
</div>