<?php use_helper('Form'); ?>
<script>
    $(document).ready(function(){
        
        $('form').attr('id','division_frm');
        $('#division_frm').bind('submit' ,  function ()
        {
            var jerr = 0;
            $('#divPassword1').html('');
            $('#divPassword2').html('');

            if($('#password1').val() == ''){
                 $('#divPassword1').html('Password is Required');
                 jerr = 1;
            } else if($('#password1').val().length < 6){
                 $('#divPassword1').html('Minimum 6 characters');
                  jerr = 1;
            } else if($('#password1').val().length > 20){
                 $('#divPassword1').html('Maximum 20 characters');
                  jerr = 1;
            }

            if($('#password2').val() == ''){
                 $('#divPassword2').html('Confirm Password is Required');
                 jerr = 1;
            } else if($('#password2').val().length < 6){
                 $('#divPassword2').html('Minimum 6 characters');
                  jerr = 1;
            } else if($('#password2').val().length > 20){
                 $('#divPassword2').html('Maximum 20 characters');
                  jerr = 1;
            }

            if(($('#password1').val() != $('#password2').val()) && ($('#password1').val() != '' && $('#password2').val() != '' && $('#password2').val().length <= 20)){
                $('#divPassword2').html('Confirm password do not match password');
                 jerr = 1;
            }
            
            if($('#accept_chkbox').attr('checked')){
                $("#agree_to_terms_condition").html();
                $('#agree_to_terms_condition').css('display', 'none');
            }
            else
            {
                $("#agree_to_terms_condition").html('You must agree to terms and conditions of use');
                $("#agree_to_terms_condition").addClass('error');
                $('#agree_to_terms_condition').css('display', 'inline');
                jerr = 1;
            }

              if(($('#password1').val() == $('#password2').val()) && ($('#password1').val() != '' && $('#password2').val() != '')){
               
                if($('#agree_to_terms_condition').html() == 'You must agree to terms and conditions of use'){
                    $('#password1').val('');
                    $('#password2').val('');
                    jerr = 1;
                 }
                 
            }
            if( jerr == 0) return true; else return false;

        });

        $.validator.addMethod("pwd_minLength", function(value, element)
        {
            return value.length>=6;
        }, "Minimum 6 characters");


        //Varun : Changes for maxlength validation
        $.validator.addMethod("pwd_maxLength", function(value, element)
        {
            return value.length<=20;
        }, "Maximum 20 characters");


        $.validator.addMethod("chk_passwords", function(value, element)
        {
            //            alert($('[name="password1"]').val());
            return $('[name="password1"]').val()==$('[name="password2"]').val();
        }, "Passwords Don't Match");


        jQuery.validator.addMethod('webUrlValidation', function(value, element) {

            return this.optional(element) || value.match(/http(s)*:\/\/[A-Za-z0-9\.-]{3,}\.[A-Za-z]{3}/);
        },'Invalid URL');

        
        $("#division_frm").validate({
            rules: {
                'address': {
                    required: true,
                    maxlength: 100
                }, 'Company URL': {
                    required: true,
                    webUrlValidation: true,
                    maxlength: 100

                }/*, 'password1': {
                    required: true,
                    pwd_minLength: true,
                    pwd_maxLength: true

                }, 'password2': {
                    required: true,
                    pwd_minLength: true,
                    pwd_maxLength: true,
                    chk_passwords: true

                }*/
                
            },

            messages: {
                'address':
                    {
                    required: "Address is Required",
                    maxlength: "Maximum 100 characters"

                },

                'Company URL':
                    {
                    required: "Company URL is Required",
                    maxlength: "Maximum 100 characters"

                }/*,'password1':
                    {
                    required: "Password is Required"

                },'password2':
                    {
                    required: "Password Confirmation is Required"

                }*/

            }


        })

    });

</script>
<div id="main" >
    <div id="wrappernew">
        <div id="content">
            <p>
            <h3>Details</h3>
            <table cellpadding="0" cellspacing="0" border="0" class="data_grid">

                <tr><td width="199" class="txtRightAlign"><strong>Email:</strong></td>
                    <td width="368"><?php
echo $merchantObj->sfGuardUser->getEmailAddress();
?></td>
                </tr><tr><td class="txtRightAlign"><strong>Company name:</strong></td><td><?php
                        echo $merchantObj->getMerchantName();
?></td></tr><tr><td class="txtRightAlign">
                        <strong>CAC Registration Number:</strong></td><td><?php
                        echo $merchantObj->getCacRegistration();
?></td></tr><tr><td class="txtRightAlign"><strong>Contact Number:</strong></td><td><?php
                            echo $merchantObj->getMerchantContactNumber();
?></td></tr>

            </table>





            <div>&nbsp;</div>


            <form action="<?php echo url_for('merchantRegistration/submitLoginDetails') ?>" method="post">
                <div class="sform">
                    <h3>Login Information</h3>

                    <div class="FormRepeatArea">
                        <div class="FormLabel txtRightAlign"> Address<span class="redLbl">*</span> </div>
                        <div class="FromInputLabe2"> <input type="text" name="address" value=""></div>
                        <!--                <div class="FormError">
                                            <div id="error-msg"></div>
                                        </div>-->
                        <div class="clear_new"></div>
                    </div>



                    <div class="FormRepeatArea">
                        <div class="FormLabel txtRightAlign"> WEB URL<span class="redLbl">*</span> </div>
                        <div class="FromInputLabe2">  <input type="text" name="Company URL" value=""></div>
                        <!--                <div class="FormError">
                                            <div id="error-msg"></div>
                                        </div>-->
                        <div class="clear_new"></div>
                    </div>



                    <div class="FormRepeatArea">
                        <div class="FormLabel txtRightAlign"> Password <span class="redLbl">*</span></div>
                        <div class="FromInputLabe2"> <?php echo input_password_tag('password1'); ?>
                        <div id="divPassword1" style="float:left;padding:0 0 0 4px;font-size:11px" class="error"></div>
                        </div>
                        <div class="clear_new"></div>
                    </div>

                    <div class="FormRepeatArea">
                        <div class="FormLabel txtRightAlign"> Confirm Password<span class="redLbl">*</span></div>
                        <div class="FromInputLabe2"> <?php echo input_password_tag('password2'); ?>
                        <div id="divPassword2" style="float:left;padding:0 0 0 4px;font-size:11px" class="error"></div>
                        </div>
                        <div class="clear_new"></div>
                    </div>





                    <?php echo input_hidden_tag('username', $merchantObj->sfGuardUser->getEmailAddress()) ?>




                    <h3>Terms and Conditions</h3>


                    <textarea readonly="readonly" rows="15" cols="200" style="width:597px" name="terms_condition">
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas eu dolor eu elit blandit sodales non in est. Suspendisse blandit lacus sed velit sollicitudin blandit. Pellentesque eu nisl eget sapien ultricies tincidunt hendrerit sit amet magna. Etiam consequat neque id nisi iaculis vitae ornare sem congue. Phasellus non ultricies tellus. Curabitur interdum felis at tellus placerat nec convallis arcu tincidunt. Nulla pulvinar accumsan est eget pellentesque. Praesent enim metus, posuere ac dapibus a, imperdiet et eros. Proin urna magna, mattis et tincidunt at, faucibus egestas augue. Donec tempor, lorem a pellentesque consectetur, arcu ipsum suscipit quam, sollicitudin ultricies enim sem ut ipsum. Cras massa ipsum, fermentum vitae laoreet vel, luctus ac elit. Vestibulum venenatis mollis ligula, id semper metus accumsan at. Quisque consectetur nisl ut turpis viverra laoreet. Nullam tincidunt dui a quam hendrerit nec accumsan diam ultricies. Integer tortor lacus, pulvinar et sagittis sed, sagittis at libero. Suspendisse tristique justo in lorem congue vel tempus ligula condimentum. Praesent suscipit imperdiet mi, eget tempus mauris imperdiet vitae. Etiam ornare dapibus justo, sit amet aliquam nisi facilisis sed. Pellentesque ut dolor enim. Sed ullamcorper porttitor pulvinar. Mauris a arcu nibh. Nulla non tortor a nibh tempus sodales. Nam tristique arcu eu sem sagittis cursus. Donec sed risus odio. Quisque tempus mollis risus ac vestibulum. Fusce urna purus, blandit sit amet dapibus in, ultrices quis nunc. Sed eget eros ac erat convallis scelerisque. Duis consectetur erat in felis malesuada pharetra. Donec ac lorem dolor, a cursus ipsum. Duis congue pretium odio, nec varius nibh elementum vel. Duis magna nisl, aliquam quis ultrices id, aliquam non lacus. Donec convallis, lacus accumsan sodales varius, urna massa convallis urna, a dignissim lectus urna a magna. Integer adipiscing mattis metus, sed auctor turpis ornare id. Mauris adipiscing purus ut lacus luctus condimentum. Aenean eros justo, sagittis sit amet mattis eget, facilisis at tellus. Maecenas et risus sem, et pretium odio. Ut quis eros dolor. Aenean id sem luctus nibh tempor ultrices sit amet a nisi. Morbi ultricies pellentesque sem at pretium. Duis a justo enim, at vulputate magna. Suspendisse arcu felis, consectetur sit amet lacinia sed, venenatis sed enim. Suspendisse potenti. Donec sodales sagittis vestibulum. In hac habitasse platea dictumst. Quisque et odio vel quam euismod ullamcorper. Quisque accumsan enim non lorem porta hendrerit. Donec pellentesque, arcu nec aliquet egestas, sapien justo lacinia ligula, ac ornare justo ipsum quis erat. Maecenas in nisl ipsum. Pellentesque vitae pharetra erat. Nunc condimentum hendrerit sapien, sit amet ultricies nisl tincidunt nec. Aenean dignissim justo eget massa venenatis ultrices. Nunc ac suscipit eros. Morbi ac metus et nisl rutrum malesuada. Vestibulum eu tempor nulla. Sed euismod justo tortor, non pharetra purus. Vivamus convallis sem libero, a imperdiet mauris. Fusce sed adipiscing purus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc malesuada facilisis ipsum, sed sagittis dui tempus nec. Quisque at ipsum ac libero venenatis semper ac a erat. Cras nec erat vitae lectus tincidunt ultrices. Mauris mollis placerat ante vel ultricies. Etiam eget urna a leo convallis convallis. Integer vel felis metus. Suspendisse diam risus, luctus ut volutpat et, porta varius lectus. Ut aliquam libero quis odio venenatis lobortis.</textarea>
                    <div class="spacer big-text">
                        <label><input type="checkbox" name="accept" value="accept" id="accept_chkbox"/>
                            <span class="redLbl">*</span></label>
                        <span onclick="if(!$('#accept_chkbox').attr('checked')){$('#accept_chkbox').attr('checked','checked');}else{$('#accept_chkbox').attr('checked',0); }">I have read and agreed to the terms and conditions</span>
                        <div id="agree_to_terms_condition" style="font-size:11px"></div></div>


                    <br clear="all"/>


                    <div class="btn_section"> <input type="submit" value="Proceed to Login" class="submit"></div>






                </div>
            </form>
            <p>&nbsp;</p>
        </div>
    </div>

</div>
