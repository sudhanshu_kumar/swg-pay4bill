<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>


            <p>
            <form action="<?php echo url_for('merchantRegistration/saveMerchant?id=' . $form->getObject()->getId()) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
                <?php if (!$form->getObject()->isNew()): ?>
                    <input type="hidden" name="sf_method" value="put" />
                <?php endif; ?>
                    <div>&nbsp;</div>


                    <div class="TableForm">
                        <h3>Merchant's Information</h3>
                      <table>
                        <tr>
                            <td width="20%">Company Name</td>
                            <td><?php echo $form['merchant_name']->render(array('disabled' => 'disabled')); ?> <?php echo $form['merchant_name']->renderError(); ?></td>
                        </tr>
                        <tr>
                            <td width="20%">Email Address</td>
                            <td><input type="text" name="" value="<?php echo $email_address;?>" disabled></td>
                        </tr>
                        <tr>
                            <td width="20%">CAC Registration</td>
                            <td><?php echo $form['cac_registration']->render(array('disabled' => 'disabled')); ?> <?php echo $form['cac_registration']->renderError(); ?></td>
                        </tr>
                        <tr>
                            <td width="20%"><?php echo $form['merchant_contact_number']->renderLabel(); ?></td>
                            <td><?php echo $form['merchant_contact_number']->render(); ?> <?php echo $form['merchant_contact_number']->renderError(); ?></td>
                        </tr>
                        <tr>
                            <td width="20%">Company URL <span class="redLbl">*</span></td>
                            <td><input type="text" name="merchant_profile[company_url]" value="<?php echo $url;?>"></td>
                        </tr>
                        <tr>
                            <td width="20%">Office Address <span class="redLbl">*</span></td>
                            <td><textarea class="FormInput2" style="height:100px;margin-bottom:10px" name="merchant_profile[merchant_contact_address]"><?php echo $address; ?></textarea>
                            <?php echo $form['_csrf_token']; ?>
                            </td>
                        </tr>
                        <tr>
                            <td></td><td>
                                <input type="submit" name="Save" class="blueBtn" value="Save"/>
                            <input type="button" name="cancel" id="cancel_button"  class="blueBtn" value="Cancel" onclick="javascript:history.go(-1)"/> 
                            </td>
                        </tr>
                      </table>
                    </div>

                    <div class="sform">



                    
                        <div style="clear:both;"></div>

                    <div class="button-position" style="padding-top:64px">
                   </div>
                </div>
            </form>            <p>&nbsp;</p>
       

