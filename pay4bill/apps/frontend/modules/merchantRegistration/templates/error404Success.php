<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="ErrorContainer">
    <div><h1>We apologise for the inconvenience. The Pay4Bill page you've requested is not available at this time. </h1>
        <div>
            There are several possible reasons you are unable to reach the page:
            <ul>
                <li>If you tried to load a bookmarked page or followed a link from another Web site. It's possible that the URL you've requested has been moved</li>
                <li>The web address you entered may be incorrect</li>
                <li>Technical difficulties are preventing us from displaying the requested page</li>
            </ul>
            We want to help you to access what you are looking for
            <ul>
                <li><a href="../">Visit our Homepage</a></li>
                <li>Try returning to the page later</li>
            </ul>

        </div></div>
    <div></div>
</div>
