<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Untitled Document</title>
    </head>

    <body>
        <table align="center" cellpadding="0" cellspacing="0" style="width:650px;">

            <tr>
                <td style="padding-bottom:5px;">
                    <?php include_partial('global/pay4bill_logo', array('path' => $path)); ?></td>


            </tr>


            <table cellspacing="0" cellpadding="10" border="0" align="center" bgcolor="#f0f0f0" style="width:650px; border-collapse:collapse; border:1px solid #dedcdc; color:#646464; font-family:Arial, Helvetica, sans-serif; font-size:14px;">
                <tr>
                    <td>Dear <span><?php echo $companyName ?></span>,</td>
                </tr>

                <tr>
                    <td align="justify">
                        Welcome to pay4bill, your fully featured online billing service. You have just registered successfully
                        and will receive access to the site once your CAC number has been verified.
                        Soon you will be able to access your account with Username(email-address) and password you choose, and start managing your invoices and sending them out to your customers</td>
                </tr>

                <tr>
                    <td>Your Login has been activated with the following details : </td>
                </tr>


                <tr>
                    <td>Username: <strong style="color:#000"><?php echo $username; ?></strong></td>
                </tr>

                <tr>
                    <td>Password: <strong style="color:#000"><?php echo $password; ?></strong></td>
                </tr>
                <tr>
                    <td>Please click on the link below to login on the portal:</td>
                </tr>


                <tr>
                    <td><a href="<?php echo $complete_url; ?>" style="color:#0c5c89;"><?php echo $complete_url; ?></a></td>
                </tr>


                <tr>
                    <td>
                        Regards,<br />
                        The Pay4bill Team
                    </td>
                </tr>

                <tr><td style="background-color:#18648e; color:#ffffff; padding:10px; font:14px Arial, Helvetica, sans-serif;">If you experience any problem, write to us at <a href="mailto:support@pay4bill.com" style="color:#fff">
                            support@pay4bill.com</a></td></tr>

            </table>

        </table>


    </body>
</html>