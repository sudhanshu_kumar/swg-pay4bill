<script>
    $(document).ready(function(){
        $('#Settings').addClass('active'); //Varun : bug fix FS#34995
        
        $('form').attr('id','division_frm');
        $.validator.addMethod("checkIfSexSelected", function(value, element)
        {
            if(value==0)
                {
                    return false;
                }
                else
                    {
                        return true;
                    }

        }, "Please select sex");

        $("#division_frm").validate({
            rules: {
                 'sf_guard_user[Details][sex]': {
                    checkIfSexSelected: true

                }
            },

            messages: {
                'sf_guard_user[Details][sex]':
                    {
                    checkIfSexSelected: "Sex is Required"
                }

            }


        })

    });

    function validate()
    {
        var err  = 0;
        var sex = jQuery.trim($('#sf_guard_user_Details_sex').val());
         if(sex=='')
          {
            $('#sex_error').html("<font color='red'>Please select sex.</font>");
            $('#sf_guard_user_Details_sex').focus();
            err = err + 1;
          } else {
            $('#sex_error').html("");
           }
            if(err>0)
            {
                return false;
            }
            return true;
    }

</script>

<?php if(isset($addClassTo)) {   include_partial('subHeader',array('addClassTo'=>$addClassTo,'adminLogin'=>''));  } else { include_partial('subHeader',array('addClassTo'=>'new','adminLogin'=>'')); } ?>
<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>
<form action="<?php echo url_for('merchantRegistration/merchantAsCustomer') ?>" method="post" class="dlForm" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?> onsubmit="return validate()">
        <?php if (!$form->getObject()->isNew()): ?>
        <input type="hidden" name="sf_method" value="put" />
        <?php endif; ?>
        <div class="sform"><div id="Formwraper">
          <h3>Customer's Information</h3>

         <div class="FormRepeatArea">
            <div class="FormLabel"> <?php echo $form['Details']['sex']->renderLabel(); ?></div>
            <div class="FromInputLabe4"><?php echo $form['Details']['sex']->render(); ?> <div class="red redlt" id="sex_error">  </div> </div>
            <!--<div class="FormError">
              <div id="error-msg"> </div>
            </div>-->
          </div>
            <?php echo $form['_csrf_token']; ?>

            <div class="button-position">
              <input type="submit" name="save" class="blueBtn" value="Save"/>
              <input type="button" name="cancel" id="cancel_button"  class="blueBtn" value="Cancel" onclick="javascript:history.go(-1)"/>

            </div>
          <input type="hidden" name="var" id="var" value="<?php echo "1"; ?>">
          </div>
        </div>
      </form>
      <p>&nbsp;</p>
<!-- <input type="hidden" name="login_for" value="<?php echo $hidden_value;?>"> -->

