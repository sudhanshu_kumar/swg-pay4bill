<script>
    $(document).ready(function(){
        $('#Merchants').addClass('active');
        $('form').attr('id','division_frm');
        
        
        jQuery.validator.addMethod('isValidPhone', function(value, element) {

            return this.optional(element) || value.match(/^[0-9+-]*$/);
        },'Please enter valid contact number.');


        jQuery.validator.addMethod('webUrlValidation', function(value, element) {
            return this.optional(element) || value.match(/http(s)*:\/\/[A-Za-z0-9\.-]{3,}\.[A-Za-z]{3}/);
        },'Please enter valid Company URL');
        
        $("#division_frm").validate({
            rules: {
                'merchant_profile[merchant_contact_number]': {
                    required: true,
                    isValidPhone: true
                }, 'merchant_profile[company_url]': {
                    required: true,
                    webUrlValidation: true,
                    maxlength: 100

                }, 'merchant_profile[merchant_contact_address]': {
                    required: true,
                    maxlength: 100
                }
            },

            messages: {
                'merchant_profile[merchant_contact_number]':
                    {
                    required: "Please enter Contact Number",
                    isValidPhone: "Please enter the valid contact number"

                },
                'merchant_profile[company_url]':
                    {
                    required: "Please enter Company URL",
                    maxlength: "Maximum 100 characters"
                    
                },'merchant_profile[merchant_contact_address]':
                    {
                    required: "Please enter Office Address",
                    maxlength: "Maximum 100 characters"

                }
            }
        })

    });

</script><?php if(isset($addClassTo)) {   include_partial('subHeader',array('addClassTo'=>$addClassTo,'adminLogin'=>''));  } else { include_partial('subHeader',array('addClassTo'=>'new')); } ?>

<?php  include_partial('editForm', array('form' => $form,'address'=>$address,'url'=>$url,'email_address'=>$email_address)) ?>
