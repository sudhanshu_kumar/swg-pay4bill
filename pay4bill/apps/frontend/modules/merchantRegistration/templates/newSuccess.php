<script>
    $(document).ready(function(){
        
//        $('.error_list').fadeOut(6000);
//        $('#logo_div').fadeOut(6000);
        $('input[type=submit]').click(
            function(){
                 $('#captchaErr').html('');
                 $('#EmailAddErr').html('');
            }
        )
//
        $('#sf_guard_user_email_address').keypress(function(){
            $('#EmailAddErr').html('');
        })

        

        $('form').attr('id','division_frm');
        $.validator.addMethod("email", function(value, element)
        {
            return this.optional(element) || /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i.test(value);
        }, "Please enter a valid email address.");
        // Varun : bug fix : FS#34472 
        $.validator.addMethod("isValidMerchantName", function(value, element)
        {
            length = value.split(' ');
            if(length[0].length>=3)
               {
                  return true;
               }
        }, "Please enter a valid Merchant Name<br/>(Minimum 3 characters Eg:ABC XYZXZ).");
        jQuery.validator.addMethod('isValidPhone', function(value, element) {

            return this.optional(element) || value.match(/^[+]{0,1}[0-9-]*$/);
        },'Invalid contact number.');
        jQuery.validator.addMethod('isValidName', function(value, element) {

            return this.optional(element) || value.match(/^[a-zA-Z0-9 .&]*$/);
        },'Invalid Name');
        jQuery.validator.addMethod('isValidCAC', function(value, element) {

            return this.optional(element) || value.match(/^[a-zA-Z0-9]*$/);
        },'Invalid CAC');
        $("#division_frm").validate({
            rules: {
                'sf_guard_user[email_address]': {
                    required: true,
                    email: true

                }, 'sf_guard_user[Profile][merchant_name]': {
                    required: true,
                    isValidMerchantName: true,
                    maxlength: 40,
                    minlength: 3,
                    isValidName: true

                }, 'sf_guard_user[Profile][merchant_contact_number]': {
                    required: true,
                    isValidPhone: true,
                    maxlength: 20,
                    minlength: 6

                }, 'sf_guard_user[Profile][cac_registration]': {
                    required: true,
                    isValidCAC: true,
                    maxlength: 20

                },
                'sf_guard_user[captcha]': {
                    required: true

                }

            },

            messages: {
                'sf_guard_user[email_address]':
                    {
                    required: "Email Address is Required"

                },
                'sf_guard_user[Profile][merchant_name]':
                    {
                    required: "Company Name is Required",
                    maxlength: "Maximum 40 characters",
                    minlength: "Minimum 3 characters"

                },'sf_guard_user[Profile][merchant_contact_number]':
                    {
                    required: "Contact Number is Required",
                    maxlength: "Maximum 20 digits",
                    minlength: "Minimum 6 digits"

                },
                'sf_guard_user[Profile][cac_registration]':
                    {
                    required: "CAC Registration is Required",
                    maxlength: "Maximum 20 digits"

                },
                'sf_guard_user[captcha]':
                    {
                    required: "Verification Code is Required"

                }

            }


        })

    });

</script>
<?php include_partial('form', array('form' => $form)) ?>
