<?php use_helper('Form'); ?>
<script>
    $(document).ready(function(){
        $('form').attr('id','division_frm');
       

 <?php if(isset($error_text)){?>
                
                $('#if_error').html('<?php echo html_entity_decode($error_text); ?>');
                <?php


                if($error){   ?>$('#if_error').addClass('error_serverside'); <?php } else { ?>$('#if_error').addClass('msg_serverside');<?php } ?>

            <?php }
                else
                    {
                        ?>$('#if_error').html();
//                                $('#if_error').re
                                $('#if_error').removeClass('error_serverside');
                                $('#if_error').removeClass('msg_serverside');
                        <?php
                    }
                    ?>

        $.validator.addMethod("email", function(value, element)
        {
            return this.optional(element) || /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i.test(value);
        }, "Please enter a valid email address.");

        $("#division_frm").validate({
            rules: {
                'email': {
                    required: true,
                    email: true

                }, 'cac': {
                    required: true

                }
            },

            messages: {
                'email':
                    {
                    required: "Email Address is Required"

                },

                'cac':
                    {
                    required: "CAC Registration is Required"

                }

            }


        })

        $('input[type=submit]').click(
            function(){
                      $('#if_error').fadeOut(500);
            }
        )
      

    }
    );

</script>


<form action="<?php echo url_for('merchantRegistration/loginDetailsForm') ?>" method="post">
    <div id="Formwraper">
        <h3>Activate your Account</h3>
        <?php if (isset($_POST['submit'])) { ?><div id="if_error" ></div> <?php } ?>

        <div class="FormRepeatArea">
            <div class="FormLabel txtRightAlign"> Email Address<span class="redLbl">*</span></div>
            <div class="FromInputLabe2"> <?php echo input_tag('email', '', ''); ?></div>

            <div class="clear_new"></div>
        </div>


        <div class="FormRepeatArea">
            <div class="FormLabel txtRightAlign">CAC Registration<span class="redLbl">*</span></div>
            <div class="FromInputLabe2"> <?php echo input_tag('cac', '', ''); ?></div>

            <div class="clear_new"></div>
        </div>




        <div class="button-position">
            <input type="submit" value="Submit" class="submit" name="submit">
        </div>




    </div>
</form>

