<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>
<?php use_helper('sfCryptoCaptcha'); ?>


<p>
<form action="<?php echo url_for('merchantRegistration/' . ($form->getObject()->isNew() ? 'create' : 'update') . (!$form->getObject()->isNew() ? '?id=' . $form->getObject()->getId() : '')) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
    <?php if (!$form->getObject()->isNew()): ?>
        <input type="hidden" name="sf_method" value="put" />
    <?php endif; ?>
        <div class="sform">
            <h3>New Merchant Registration</h3>
            <div class="FormRepeatArea">
                <div class="FormLabel"> <?php echo $form['email_address']->renderLabel(); ?> </div>
                <div class="FromInputLabe2"> <?php echo $form['email_address']->render(); ?>
                    <div id="EmailAddErr"><?php echo $form['email_address']->renderError(); ?></div>
                </div>
                <div class="clear_new"></div>
            </div>
            <div class="FormRepeatArea">
                <div class="FormLabel"> <?php echo $form['Profile']['merchant_name']->renderLabel(); ?> </div>
                <div class="FromInputLabe2"> <?php echo $form['Profile']['merchant_name']->render(); ?><?php echo $form['Profile']['merchant_name']->renderError(); ?></div>
                <div class="clear_new"></div>
            </div>
            <div class="FormRepeatArea">
                <div class="FormLabel"> <?php echo $form['Profile']['cac_registration']->renderLabel(); ?> </div>
                <div class="FromInputLabe2"> <?php echo $form['Profile']['cac_registration']->render(); ?> <?php echo $form['Profile']['cac_registration']->renderError(); ?></div>
                <div class="clear_new"></div>
            </div>
            <div class="FormRepeatArea">
                <div class="FormLabel"> <?php echo $form['Profile']['merchant_contact_number']->renderLabel(); ?> </div>
                <div class="FromInputLabe2"> <?php echo $form['Profile']['merchant_contact_number']->render(); ?><i><span class="">(e.g: +1234567891)</span></i> <?php echo $form['Profile']['merchant_contact_number']->renderError(); ?></div>
                <div class="clear_new"></div>
            </div>

            <!--  New div added for File upload -->
            <div class="FormRepeatArea">
                <div class="FormLabel"><?php echo $form['Profile']['company_logo']->renderLabel(); ?></div>
                <div class="FromInputLabe2"><?php echo $form['Profile']['company_logo']->render(); ?> <br>
                    -Please upload JPG/GIF/PNG images only.
                    -File size upto 400 KB.<br>
                    <?php echo $form['Profile']['company_logo']->renderError(); ?>
                </div>
           </div>
            <?php if($form['Profile']['company_logo']->renderError()) { ?>
                <div style="clear:both" id="logo_div">&nbsp;</div>
            <?php } ?>

        <?php
        captcha_image();
        captcha_reload_button();
        ?>
        <?php echo $form['_csrf_token']; ?>
        <div class="FormRepeatArea">
            <div class="FormLabel"> <?php echo $form['captcha']->renderLabel(); ?> </div>
            <div class="FromInputLabe2"> <?php echo $form['captcha']->render(); ?><div id="captchaErr"><?php echo $form['captcha']->renderError(); ?></div></div>
            <div class="clear_new"></div>
        </div>
                <div class="captcha_img">
                    <?php echo captcha_image();
                    echo captcha_reload_button(); ?>
                </div>

        <div class="button-position">
            <input type="submit" class="submit" value="<?php if ($form->getObject()->getMerchantUser()->getApproved() == 1) { ?>SAVE<?php } else { ?>Send an e-mail<?php } ?>" />
        </div>
    </div>
</form>

<p>&nbsp;</p>

