<?php use_helper('ePortal') ?>
<script language="javascript" >

    function onChangeDropDown(){

        var cac_reg = jQuery.trim($('#cac_reg').val());
        var email_address = jQuery.trim($('#email_address').val());
        var mer_name = jQuery.trim($('#mer_name').val());

        // Hide all once
        $('#email_address1').hide();
        $('#merchant_name').hide();
        $('#cac_registration').hide();

        

        if($('#search_par').val() == 'email'){
            $('#email_address1').show();
            $('#cac_reg').val('');
            $('#mer_name').val('');
        }
        if($('#search_par').val() == 'merchant'){
            $('#merchant_name').show();
            $('#cac_reg').val('');
            $('#email_address').val('');
        }
        if($('#search_par').val() == 'cac'){
            $('#cac_registration').show();
            $('#mer_name').val('');
            $('#email_address').val('');
        }
        $('#email_address_error').html("");
        $('#mer_name_error').html("");
        $('#cac_reg_error').html("");
    }

    function validateSearch()
    {
        var err  = 0;
        var cac_reg = jQuery.trim($('#cac_reg').val());
        var email_address = jQuery.trim($('#email_address').val());
        var mer_name = jQuery.trim($('#mer_name').val());
               
     
        var reg = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i; //Email validation
        // var regcheck = /^[a-zA-Z0-9 .&]*$/; //Invoice Number  and Customer Name validation
   
        if(email_address !='' && $('#email_address').is(":visible"))
        {
            if(reg.test(email_address) == false) {
                $('#email_address_error').html("<font color='red'>Please enter correct Email Address.</font>");
                $('#email_address').focus();
                err = 1;
            } else {
                $('#email_address_error').html("");
                $('#email_address').focus();
                err = 0
            }
        }

        if(email_address =='' && $('#email_address').is(":visible")){
            $('#email_address_error').html("<font color='red'>Please select Email Address.</font>");
            err = 1;
        }

        if(mer_name =='' && $('#mer_name').is(":visible"))  {
            $('#mer_name_error').html("<font color='red'>Please select Merchant Name.</font>");
            $('#mer_name').focus();
            err = 1;
        }

        if(cac_reg =='' && $('#cac_reg').is(":visible")){
            $('#cac_reg_error').html("<font color='red'>Please select CAC Registration.</font>");
            $('#cac_reg').focus();
            err = 1;
        }

        if(err == 1)
        {
            return false;
        }
        return true;
    }
               
    $(document).ready(function(){
        // onChangeDropDown();
        //        $('.resetCls').click(function(){
        //            $('input[type=text]').val('');
        //            $('#email_address1').hide();
        //            $('#merchant_name').hide();
        //            $('#cac_registration').hide();
        //            $('#search_par').val('');
        //        })
        
        $('#Merchants').addClass('active');
<?php if (isset($msg)) { ?>

            $('#if_error').html('<?php echo html_entity_decode($msg); ?>');
            $('#if_error').addClass('msg_serverside');
<?php
} else {
?>$('#if_error').html();
            $('#if_error').removeClass('msg_serverside');
<?php
}
?>
    }
);

</script>

<?php //include_partial('subHeader', array('addClassTo' => $addClassTo)); ?>

<?php if (!empty($msg)) {
?><div class="clear_new"></div> <?php } ?>


<div id="mainnew">

    <div id="wrappernew">
        <div id="content">
            <div id="Formwraper">
                <div id="pgMenu">
                    <div id="wrappernew">
                        <div class="pgTitle">
                            <h2>  Merchants</h2>
                        </div>
                    </div>
                </div>


                <!-- Search starts here -->

                <h3>Search Merchants</h3>
                <div id="msg"></div>
                <div class="TableForm" id="search">

                    <form action="<?php echo url_for('merchantRegistration/index'); ?>" method="post" id="search_form" onsubmit="return validateSearch()">

                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="17.5%" valign="top"><?php echo "Merchant Name" ?> </td>
                                <td  width="35%" valign="top">
                                    <input class="FormInput9" type="textbox" name="merchant_name" value="<?php
echo $merchant_name;
?>">
                                </td>
                                </td>

                            </tr>
                            <tr>
                                <td width="17.5%" valign="top"><?php echo "CAC Registration" ?> </td>
                                <td  width="35%" valign="top">
                                    <input class="FormInput9" type="textbox" name="cac_registration" value="<?php
                                           echo $cac_registration;
?>">
                                </td>

                            </tr>
                            <tr>
                                <td width="17.5%" valign="top"><?php echo "Email Address" ?> </td>
                                <td  width="35%" valign="top">
                                    <input class="FormInput9" type="textbox" name="email_address" value="<?php
                                           echo $email_address;
?>">
                                </td>

                            </tr>
                            <tr id="merchantSpecificAttributes">
                                <td colspan="3"  valign="top">
                                    <div class="Errormrg"><input type="submit" value="Search" class="blueBtn" >
                                        <input type="button" class="blueBtn" value="Reset" onClick="window.location='<?php echo url_for('merchantRegistration/index'); ?>'"/></div>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
                <div class="clear_new"></div>
                <!-- Search ends here -->



                <?php
                                           $sf = sfContext::getInstance()->getUser();
                                           if ($sf->hasFlash('notice')) {
                ?><div id="flash_notice" class="msg_serverside" ><?php
                                               echo nl2br($sf->getFlash('notice'));
                ?>
                                               </div><div style="clear:both"></div><?php } ?>

                                           <!--h3>Merchants</h-->
                                           <div id="if_error" class="msg_serverside" style="display:none"></div>
                                           <div class="TableForm">
                                               <table>
                                                   <thead>
                                                   <th width="30%">Merchant Name</th>
                                                   <th width="20%">Email Address</th>
                                                   <th width="10%" nowrap>CAC Registration</th>

                                                   <th width="20%">Contact Number</th>
                                                   <!--th width="20%">Office Address</th-->
                                                   <th width="10%">Actions</th>
                                                   </thead>
                                                   <tbody>
                            <?php
                                           if (count($merchant_profiles) > 0) {
                            ?>
                            <?php $i = 0;
                                               foreach ($merchant_profiles as $merchant_profile): $i++ ?>  <!-- Varun :Bug Fix 35012 -->
                            <?php if ($i % 2 != 0) {
                            ?>
                                                       <tr class="odd">
                                <?php } else {
                                ?><tr class=""> <?php } ?>
                                                   <td><?php echo $merchant_profile->getMerchantName() ?></td>
                                                   <td><?php echo strlen($merchant_profile->sfGuardUser->getEmailAddress())>30?substr($merchant_profile->sfGuardUser->getEmailAddress(),0,28).'...':$merchant_profile->sfGuardUser->getEmailAddress(); ?>                                                           
                                                       <?php //echo $merchant_profile->sfGuardUser->getEmailAddress() ?>
                                                   </td>
                                                   <td><?php echo $merchant_profile->getCacRegistration() ?></td>
                                                   <td><?php echo $merchant_profile->getMerchantContactNumber() ?></td>
                                                   <!--td><?php //echo $merchant_profile->getMerchantContactAddress()     ?></td-->
                                                   <td>
                                                       <!-- Varun: Id to be sent encrypted-->
                                    <?php echo link_to(image_tag('showpage.jpeg'), 'custMgt/index?merchant_id=' . cryptString::encrypt($merchant_profile->getId()), array('title' => 'View Customers', 'alt' => 'View Customers')); ?>&nbsp;&nbsp;&nbsp;&nbsp;
                                    <?php echo link_to(image_tag('tdEdit.png'), 'merchantRegistration/editMerchant?id=' . cryptString::encrypt($merchant_profile->getId()), array('title' => 'Edit Merchant', 'alt' => 'Edit Merchant')); ?>&nbsp;&nbsp;&nbsp;&nbsp;
                                    <?php
                                                   if ($merchant_profile->getDisabled() == '1') {
                                                       echo link_to(image_tag('disable.gif'), 'merchantRegistration/enableMerchant?id=' . cryptString::encrypt($merchant_profile->getId()), array('title' => 'Enable Merchant', 'alt' => 'Enable Merchant', 'class' => 'delete', 'confirm' => 'Are you sure you want to enable this merchant?'));
                                                   } else {
                                                       echo link_to(image_tag('enable.gif'), 'merchantRegistration/disableMerchant?id=' . cryptString::encrypt($merchant_profile->getId()), array('title' => 'Disable Merchant', 'alt' => 'Disable Merchant', 'class' => 'delete', 'confirm' => 'Are you sure you want to disable this merchant?'));
                                                   }
                                    ?>


                                           </tr>
                            <?php endforeach; ?>

                            <?php
                                               } else {
                                                   echo "<tr><td colspan='5' align='center'>No Merchant Found</td></tr>";
                                               }
                            ?>
                                           </tbody>
                                       </table>

                                   </div>
                                   <div class="nofoList">
                    <?php
                                               include_partial('global/records_number', array('amount_footer' => false, 'current_offset' => isset($current_offset) ? $current_offset : null, 'offset_next' => isset($offset_next) ? $offset_next : null, 'total_no_of_records' => isset($total_no_of_records) ? $total_no_of_records : count($merchant_profiles), 'no_of_records_per_page' => isset($no_of_records_per_page) ? $no_of_records_per_page : null, 'from' => '', 'total_amount_of_all_transactions' => isset($total_amount_of_all_transactions) ? $total_amount_of_all_transactions : null));
                    ?>
                                           </div>
                                           <div align="right">
                    <?php
                                               if ($setPagination) {
                                                   include_partial('global/pagination', array('moduleName' => $module, 'amount_footer' => false, 'actionName' => $actionTobeCalled, 'current_offset' => $current_offset, 'offset_next' => $offset_next, 'offset_prev' => $offset_prev, 'offset_last' => $offset_last, 'search' => '', 'from' => '', 'email_address' => $email_address, 'mer_name' => $merchant_name, 'cac_reg' => $cac_registration));
                                               }
                    ?></div>
            </div>
        </div>
    </div>
</div>
