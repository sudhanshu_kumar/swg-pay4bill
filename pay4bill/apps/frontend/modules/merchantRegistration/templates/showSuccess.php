<div id="Formwraper">
    <h3>Details</h3>

    <?php if ($msgforadmin != '') {
 ?><div class="success-msg"><?php echo $msgforadmin; ?></div>

<?php } elseif ($msgforadmin == '' && !$merchant_profile->sfGuardUser->getIsActive() == 1 && $merchant_profile->getApproved()) { ?>

        <div class="error_serverside_beforelogin">You have approved Registration for <b><?php echo ucfirst($merchant_profile->getMerchantName()) ?> </b>  on pay4bill Portal. An email has been sent to merchant to activate Login.</div>
    <?php } elseif ($merchant_profile->sfGuardUser->getIsActive() == 0 && !$merchant_profile->getApproved()) { ?>
        <div class="error_serverside_beforelogin"><b>Thank you for registering on pay4bill. Your account  can  be activated after Admin approves your request</b></div>
        <?php } 
     elseif ($merchant_profile->sfGuardUser->getIsActive() == 1) {
    ?><div class="error_serverside_beforelogin">You can now login using your Login details sent to your Email Address. &nbsp;
            <a class="link" href="<?php echo url_for('@homepage');
        ; ?>">Click here</a>  &nbsp;to Login</div><?php } ?><table cellpadding="0" cellspacing="0" border="0" width="100%" class="data_grid">
        <tbody>
            <tr>
                <td class="label_name" width="25%">Name:</td>
                <td><?php echo $merchant_profile->getMerchantName() ?></td>
            </tr>
            <tr>
                <td class="label_name">CAC Registration:</td>
                <td><?php echo $merchant_profile->getCacRegistration() ?></td>
            </tr>
            <tr>
                <td class="label_name">Email Address:</td>
                <td><?php echo $merchant_profile->sfGuardUser->getUsername() ?></td>
            </tr>
            <tr>
                <td class="label_name">Contact number:</td>
                <td><?php echo $merchant_profile->getMerchantContactNumber() ?></td>
            </tr>

        </tbody>
    </table>

</div>
