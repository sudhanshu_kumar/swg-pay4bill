<?php use_helper('ePortal') ?>
<script>
    $(document).ready(function(){
        $('#Settings').addClass('active');
<?php if(isset($msg)){?>
        $('#if_error').html('<?php echo html_entity_decode($msg); ?>');
    <?php
    if($error){   ?>$('#if_error').addClass('error_serverside'); <?php } else { ?>$('#if_error').addClass('msg_serverside');<?php } ?>
    <?php }
else
{
    ?>$('#if_error').html();
            $('#if_error').removeClass('error_serverside');
            $('#if_error').removeClass('msg_serverside');
    <?php } ?>
        $('form').attr('id','division_frm');
        $("#division_frm").validate({
            rules: {
                'file': {
                    required: false
                }
            },
            messages: {
                'file':
                    {
                    required: "File is Required"
                }
            }
        })
    });
</script>
<div id="pgMenu">
    <div id="wrappernew">
        <div class="pgTitle">
            <h2>Settings</h2>
        </div>
        <div class="pgNav">
            <ul>
                <li class=""><a href="<?php echo url_for('userAdmin/changePassword') ?>">Change Password</a></li>
                <li class=""><a href="<?php echo url_for('userAdmin/viewBank') ?>">View Bank</a></li>
                <li class="current"><a href="<?php echo url_for('merchantRegistration/uploadMerchantLogo') ?>">Upload Merchant Logo</a></li>
                <li ><a href="<?php echo url_for('custMgt/addProfile') ?>">Add Customer Profile</a></li>
                <li><a href="<?php echo url_for('userAdmin/viewDetail') ?>">View Details</a></li>
            </ul>
        </div>
    </div>
</div>
<?php $sf = sfContext::getInstance()->getUser();
if($sf->hasFlash('notice')){
    ?><div id="flash_notice" class="msg_serverside" ><?php
    echo nl2br($sf->getFlash('notice'));
    ?>
</div><?php }?>
<div id="if_error" ></div>

<div style="float:right;border:0px solid red">
    <?php
    $detail = Doctrine::getTable('MerchantProfile')->findByMerchantId($merchantId);
    $logo = $detail[0]['company_logo'];
    
    if (file_exists(sfConfig::get('sf_upload_dir') . '/merchant_logo/' . $logo)){
        $wdth = '80%';
    } else {
        $wdth = '';
    }
       
    if (file_exists(sfConfig::get('sf_upload_dir') . '/merchant_logo/' . $logo) && !empty($logo)) { 
    ?>
        <h1 style="margin-bottom: 10px;">Current Logo</h1>
        <div style=" margin-bottom: 10px;"> 
            <img src="<?php echo _compute_public_path($logo, 'uploads/merchant_logo', '', true); ?>" style="height: 100px; width: 150px;margin-left:4px" alt="Test" class="" /> <br>  <a style="margin-left: 10px;" class="link" href="<?php echo url_for('merchantRegistration/removeLogo?merchantId='.$merchantId);?>">Remove Logo</a> 
        </div>
    <?php } ?>
</div>

 <h3><?php echo $title;?></h3> 
 
 <div>

 
<div style="float:left1; overflow:hidden;border:0px solid red"><?php include_partial('global/upload', array('title' => $title, 'uploadButtonText' => $uploadButtonText,'action'=>$action,'sample_link_file_name'=>'invoice_sample_csv','sample_link_text'=>'')); ?>
    <br>-Please upload JPG/GIF/PNG images only.  <br>-File size upto 400 KB.<br></div>
     
 </div>