<?php

/**
 * merchantRegistration actions.
 *
 * @package    nisp
 * @subpackage merchantRegistration
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class merchantRegistrationActions extends sfActions {

    public function executeIndex(sfWebRequest $request) {

        $pay4billHelperObj = new pay4billHelper();
        $parameters = $pay4billHelperObj->trimArray($request->getParameterHolder()->getAll());
        $this->merchant_name = $request->hasParameter('merchant_name') ? $parameters['merchant_name'] : '';
        $this->cac_registration = $request->hasParameter('cac_registration') ? $parameters['cac_registration'] : '';
        $this->email_address = $request->hasParameter('email_address') ? $parameters['email_address'] : '';


        //Pass search parameters: Saurabh
        $this->merchant_profiles = Doctrine::getTable('MerchantProfile')->detailedMerchantProfile('', '', $this->email_address, $this->cac_registration, $this->merchant_name);

        $total_no_of_records = count($this->merchant_profiles);
        /*         * *********************----------To be removed After Paging is enabled----------- */
        if ($request->hasParameter('offset')) {

            $this->getUser()->getAttributeHolder()->remove('msg');
        }
        /*         * *********************----------To be removed After Paging is enabled----------- */

        if ($total_no_of_records > sfConfig::get('app_records_per_page')) {
            $this->actionTobeCalled = $this->getActionName();
            $this->total_no_of_records = $total_no_of_records;
            $this->no_of_records_per_page = sfConfig::get('app_records_per_page');
            $this->setPagination = true;

            if (!$request->hasParameter('offset')) {
                $offset = 0;
            } else {

                $this->getUser()->getAttributeHolder()->remove('msg');
                $offset = $request->getParameter('offset');
            }
            $this->current_offset = $offset;

            // Pass search parameters: Saurabh

            $this->merchant_profiles = Doctrine::getTable('MerchantProfile')
                            ->detailedMerchantProfile($offset, sfConfig::get('app_records_per_page'), $this->email_address, $this->cac_registration, $this->merchant_name);


            $this->module = $this->getModuleName();
            $this->offset_last = (($total_no_of_records % sfConfig::get('app_records_per_page') == 0 ? ($total_no_of_records - sfConfig::get('app_records_per_page') ) : $total_no_of_records - ($total_no_of_records % sfConfig::get('app_records_per_page'))));

            $this->offset_next = (($offset + sfConfig::get('app_records_per_page')) > $this->offset_last ? $this->offset_last : ($offset + sfConfig::get('app_records_per_page')));

            $this->offset_prev = (($offset - sfConfig::get('app_records_per_page')) < 0 ? 0 : $offset - sfConfig::get('app_records_per_page'));
        } else {
//                            echo "<pre>";
//               print_r($request->getParameterHolder()->getAll());
//                exit;
            $this->setPagination = false;
        }

        if ($this->getUser()->hasAttribute('msg')) {
            $this->msg = $this->getUser()->getAttribute('msg');
        }
        $this->addClassTo = $this->getActionName();
    }

    public function executeError404(sfWebRequest $request) {

    }

    // Varun : Function to dusable merchant
    public function executeDisableMerchant(sfWebRequest $request) {

        //Varun : Decrypt id
        $id = cryptString::decrypt($request->getParameter('id'));
        // below validation query added by santosh to check the merchant custoemrs future invoices
        $invCount = Doctrine::getTable('Invoice')->getInvoiceByMerchant($id);
        if ($invCount == 0) {
            $details = Doctrine::getTable('MerchantProfile')->getMerchantInfoById($id);

            $merchant_id = $details[0]['merchant_id'];
            $sf_guard_details = Doctrine::getTable('sfGuardUser')->getInfoById($merchant_id);

            $email_address = $sf_guard_details[0]['email_address'];
            $merchantName = $details[0]['merchant_name'];
            $subject = "Account Disable Notification";
            //Varun : Decrypt id
            $merchantObj = Doctrine::getTable('MerchantProfile')->find(cryptString::decrypt($request->getParameter('id')));

            $merchantObj->setDisabled(1);
            $merchantObj->sfGuardUser->setIsActive(0);
            $merchantObj->save();
            $partial_name = 'disableMerchant';
            sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
            $taskId = EpjobsContext::getInstance()->addJob('SendMailNotification', $this->moduleName . "/sendMerchantDisableEmail", array('email' => $email_address, 'merchant_name' => $merchantName, 'subject' => $subject, 'partial_name' => $partial_name, 'image_tag' => public_path(sfConfig::get('app_logo_path'), true)), '-1', 'frontend');
            $this->logMessage("scheduled mail job with id: $taskId", 'debug');
            /* 1-Nov-2012
             * If Merchant is disabled from Admin, and at the same time merchant is already loggedin in another browser/machine,
             * then merchant should be logged out as admin has disabled the merchant. So merchant is no longer to perform any further 
             * action.
             */
            $arr = Doctrine::getTable('MerchantProfile')->find($id);
            $sf_user_id = $arr->getMerchantId();
            $sessionTableObj = Doctrine::getTable('AppSessions')->findByUserId($sf_user_id);
            $sessionTableObj->delete();
            /*             * *************** */
            $this->getUser()->setFlash('notice', 'Merchant Disabled Successfully .');
        } else {
            $this->getUser()->setFlash('notice', 'Merchant can not be disabled as its customers invoices are due.');
        }
        $this->redirect('merchantRegistration/index');
    }

    // Saurabh: Function to edit merchant details
    public function executeEditMerchant(sfWebRequest $request) {
        $this->addClassTo = $this->getActionName();
        $this->getUser()->setAttribute('msg', '');
        $id = cryptString::decrypt($request->getParameter('id'));

        $arr = Doctrine::getTable('MerchantProfile')->findById($id)->toArray();
        $userOb = Doctrine::getTable('sfGuardUser')->findById($arr[0]['merchant_id'])->toArray();

        $this->address = $arr[0]['merchant_contact_address'];
        $this->url = $arr[0]['company_url'];
        $this->email_address = $userOb[0]['email_address'];
        $this->forward404Unless($merchant_profile = Doctrine::getTable('MerchantProfile')->find(array(cryptString::decrypt($request->getParameter('id')))), sprintf('Object merchant_profile does not exist (%s).', cryptString::decrypt($request->getParameter('merchant_id'))));
        $this->form = new MerchantProfileForm($merchant_profile, array('edit' => true));
    }

    // Saurabh: Function to save merchant details
    public function executeSaveMerchant(sfWebRequest $request) {

        $fullArr = $request->getParameterHolder()->getAll();

        $merchant_id = $request->getParameter('id');

        $address = $fullArr['merchant_profile']['merchant_contact_address'];
        $comurl = $fullArr['merchant_profile']['company_url'];
        $number = $fullArr['merchant_profile']['merchant_contact_number'];

//        $merchant_ob = Doctrine::getTable('MerchantProfile')->findById($merchant_id);
        $merchant_ob = Doctrine::getTable('MerchantProfile')->find($merchant_id);

        $merchant_ob->setMerchantContactAddress($address);
        $merchant_ob->setCompanyUrl($comurl);
        $merchant_ob->setMerchantContactNumber($number);

        $merchant_ob->save();
        $this->getUser()->setFlash('notice', 'Merchant updated Successfully.');
        $this->redirect('merchantRegistration/index');
    }

    public function executeSendMerchantDisableEmail(sfWebRequest $request) {

        $email = $request->getParameter('email');
        $merchantName = $request->getParameter('merchant_name');
        $subject = $request->getParameter('subject');
        $partialName = $request->getParameter('partial_name');

        /* Mail firing code starts */
        $mailTo = $email;
        $mailToGroup = array();

        $mailFrom = sfConfig::get('app_sender');
        $signature = sfConfig::get('app_sender');

        $partialVars = array('merchantName' => $merchantName, 'path' => $request->getParameter('image_tag'));
        $mailingOptions = array('mailFrom' => $mailFrom, 'mailTo' => $mailTo, 'mailSubject' => $subject);

        $mailObj = new Mailing();
        $mailInfo = $mailObj->sendMail($partialName, $partialVars, $mailingOptions);

        return $this->renderText($mailInfo);
    }

    //Varun : FUnction to enable merchant.
    public function executeEnableMerchant(sfWebRequest $request) {
        //Varun : Decrypt id
        $id = cryptString::decrypt($request->getParameter('id'));
        $details = Doctrine::getTable('MerchantProfile')->getMerchantInfoById($id);

        $merchant_id = $details[0]['merchant_id'];
        $sf_guard_details = Doctrine::getTable('sfGuardUser')->getInfoById($merchant_id);

        $email_address = $sf_guard_details[0]['email_address'];
        $merchantName = $details[0]['merchant_name'];
        $subject = "Account Enable Notification";
        //Varun : Decrypt id 
        $merchantObj = Doctrine::getTable('MerchantProfile')->find(cryptString::decrypt($request->getParameter('id')));

        $merchantObj->setDisabled(0);
        $merchantObj->sfGuardUser->setIsActive(1);
        $merchantObj->save();
        $partial_name = 'enableMerchant';
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
        $taskId = EpjobsContext::getInstance()->addJob('SendMailNotification', $this->moduleName . "/sendMerchantEnableEmail", array('email' => $email_address, 'merchant_name' => $merchantName, 'subject' => $subject, 'partial_name' => $partial_name, 'image_tag' => public_path(sfConfig::get('app_logo_path'), true)), '-1', 'frontend');
        $this->logMessage("scheduled mail job with id: $taskId", 'debug');
        $this->getUser()->setFlash('notice', 'Merchant Enabled Successfully .');
        $this->redirect('merchantRegistration/index');
    }

    public function executeSendMerchantEnableEmail(sfWebRequest $request) {

        $email = $request->getParameter('email');
        $merchantName = $request->getParameter('merchant_name');
        $subject = $request->getParameter('subject');
        $partialName = $request->getParameter('partial_name');

        /* Mail firing code starts */
        $mailTo = $email;
        $mailToGroup = array();

        $mailFrom = sfConfig::get('app_sender');
        $signature = sfConfig::get('app_sender');

        $partialVars = array('merchantName' => $merchantName, 'path' => $request->getParameter('image_tag'));
        $mailingOptions = array('mailFrom' => $mailFrom, 'mailTo' => $mailTo, 'mailSubject' => $subject);

        $mailObj = new Mailing();
        $mailInfo = $mailObj->sendMail($partialName, $partialVars, $mailingOptions);

        return $this->renderText($mailInfo);
    }

    public function executeShow(sfWebRequest $request) {
        if ($this->getUser()->hasAttribute('msg_for_admin')) {
            $this->msgforadmin = $this->getUser()->getAttribute('msg_for_admin');
        } else {
            $this->getUser()->getAttributeHolder()->remove('msg_for_admin');
            $this->msgforadmin = '';
        }
        $this->merchant_profile = Doctrine::getTable('MerchantProfile')->find(array(cryptString::decrypt($request->getParameter('id'))));
        $this->forward404Unless($this->merchant_profile);
    }

    public function executeNew(sfWebRequest $request) {
//    $this->form = new MerchantProfileForm();
        $this->form = new MerchantRegistrationForm(null, array('valid_cac' => 0));
    }

    public function executeCreate(sfWebRequest $request) {
        $this->forward404Unless($request->isMethod(sfRequest::POST));

        $this->form = new MerchantRegistrationForm();

        $this->processForm($request, $this->form);

        $this->setTemplate('new');
    }

    public function executeEdit(sfWebRequest $request) {
        $this->forward404Unless($merchant_profile = Doctrine::getTable('MerchantProfile')->find(array($request->getParameter('id'))), sprintf('Object merchant_profile does not exist (%s).', $request->getParameter('id')));
        $this->form = new MerchantProfileForm($merchant_profile);
    }

    public function executeUpdate(sfWebRequest $request) {

        $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
        $this->forward404Unless($merchant_profile = Doctrine::getTable('MerchantProfile')->find(array($request->getParameter('id'))), sprintf('Object merchant_profile does not exist (%s).', $request->getParameter('id')));
        $this->form = new MerchantProfileForm($merchant_profile);

        $this->processForm($request, $this->form);

        $this->setTemplate('edit');
    }

    public function executeDelete(sfWebRequest $request) {
        $request->checkCSRFProtection();

        $this->forward404Unless($merchant_profile = Doctrine::getTable('MerchantProfile')->find(array($request->getParameter('id'))), sprintf('Object merchant_profile does not exist (%s).', $request->getParameter('id')));
        $merchant_profile->delete();

        $this->redirect('merchantRegistration/index');
    }

    public function executeApproveMerchantRegistration(sfWebRequest $request) {
        $id = cryptString::decrypt($request->getParameter('id'));
//  $id='1';

        $merchant_obj = Doctrine::getTable('MerchantProfile')->find($id);
        if ($merchant_obj->getApproved() == 0) {

            /* @Saurabh
             * Check if the merchant is yet to approve and his merchant service id (from pay4me)
             * is not set (0 or empty), then he can not be approved.
             */
            if ($merchant_obj->getMerchantServiceId() == '' || $merchant_obj->getMerchantServiceId() == 0) {

                if ($this->getUser()->isAuthenticated()) {
                    $this->getUser()->setFlash('notice', 'Merchant can not be approved because his Pay4Me setup is not configured');
                    $this->redirect('adminManagement/merchantRegList');
                } else {
                    $this->getUser()->setAttribute('msg_for_admin', 'Merchant can not be approved because his Pay4Me setup is not configured');
                    $this->redirect('merchantRegistration/show?id=' . cryptString::encrypt($merchant_obj->getId()));
                }
            } else {
                $merchant_obj->setApproved(1);

                $this->getUser()->setAttribute('msg_for_admin', '');
                $merchant_obj->save();
                $email = $merchant_obj->sfGuardUser->getUsername();

                $companyName = $merchant_obj->getMerchantName();

                $host = $request->getUriPrefix();
                $url_root = $request->getPathInfoPrefix();
                $complete_url = $host . $url_root;
                sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
                $taskId = EpjobsContext::getInstance()->addJob('SendMailNotification', $this->moduleName . "/sendEmail", array('email' => $email, 'cac_registration' => $merchant_obj->getCacRegistration(), 'companyName' => $companyName, 'complete_url' => $complete_url . "/merchantRegistration/validateForLogin", 'partial_name' => 'approvalMailToMerchant', 'image_tag' => public_path(sfConfig::get('app_logo_path'), true)), '-1', 'frontend');

                if ($request->getParameter('byadmin') == 1) {
                    $this->getUser()->setFlash('notice', 'Merchant registration approved successfully');
                    $this->redirect('adminManagement/merchantRegList');
                } else {
                    $this->redirect('merchantRegistration/show?id=' . cryptString::encrypt($merchant_obj->getId()));
                }
            }
        } else {
            if ($this->getUser()->isAuthenticated()) {
                $this->getUser()->setFlash('notice', 'Merchant registration already approved');
                $this->redirect('adminManagement/merchantRegList');
            } else {
                $this->getUser()->setAttribute('msg_for_admin', 'Already Approved');
                $this->redirect('merchantRegistration/show?id=' . cryptString::encrypt($merchant_obj->getId()));
            }
        }
    }

    public function executeValidateForLogin(sfWebRequest $request) {
        $captcha = new sfCryptoCaptchaForm();
        $this->captcha = $captcha;
    }

    public function executeSubmitLoginDetails(sfWebRequest $request) {
//        echo "<pre>";
//        print_r($request->getParameterHolder()->getAll());
//        exit;
        if ($request->getParameter('address') != '' && $request->getParameter('Company_URL') != '') {
            if ($request->getParameter('password1') === $request->getParameter('password2')) {
                $userOb = Doctrine::getTable('sfGuardUser')->findBy('username', $request->getParameter('username'))->getFirst();
                $userOb->setPassword($request->getParameter('password1'));
                $userOb->setIsActive(1);

                $userOb->link('Groups', pay4billHelper::getGroupIdsArrayFromGroupNames(array('merchant')));

//                $ob->Groups->
                $userOb->MerchantUser->setMerchantContactAddress($request->getParameter('address'));
                $userOb->MerchantUser->setCompanyUrl($request->getParameter('Company_URL'));
                $userOb->save();

                $email = $userOb->getUsername();

                $companyName = $userOb->getMerchantUser()->getMerchantName();

                $host = $request->getUriPrefix();
                $url_root = $request->getPathInfoPrefix();
                $complete_url = $host . $url_root;
sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
                $taskId = EpjobsContext::getInstance()->addJob('SendMailNotification', $this->moduleName . "/sendEmail", array('email' => $email, 'companyName' => $companyName, 'username' => $userOb->getUsername(), 'password' => $request->getParameter('password1'), 'complete_url' => $complete_url, 'partial_name' => 'sendEmail','image_tag'=>public_path(sfConfig::get('app_logo_path'),true)), '-1', 'frontend');

                $this->redirect('merchantRegistration/show?id=' . cryptString::encrypt($userOb->MerchantUser->getId()));
            } else {
//                echo "<pre>";
                print_r('Passwords Don"t Match');
//                exit;
            }
        } else {
//            echo "<pre>";
            print_r('Enter all fields Data');
//            exit;
        }
    }

    public function executeLoginDetailsForm(sfWebRequest $request) {
//        echo "<pre>";
//        print_r($request->getParameterHolder()->getAll());
//        exit;

        $userObj = Doctrine::getTable('sfGuardUser')->findBy('username', $request->getParameter('email'))->getFirst();
        if ($userObj && $userObj->MerchantUser->getDisabled() == false) {
            $this->error = false;
            if ($userObj->getMerchantUser()->getCacRegistration() == $request->getParameter('cac')) {
                if ($userObj->getMerchantUser()->getApproved() == 1) {
//                $userObj = Doctrine::getTable('sfGuardUser')->findBy('username', 'adi.m84@gmail.com')->getFirst();
                    if ($userObj->getIsActive() == 1) {
                        $this->error_text = 'Merchant already activated';
                        $this->error = true;
                        $this->setTemplate('validateForLogin');
                    } else {
                        $this->merchantObj = $userObj->getMerchantUser();
                    }
                } else {
                    $this->error_text = 'Admin has not yet Approved your registration';
                    $this->error = true;
                    $this->setTemplate('validateForLogin');
//                    echo "<pre>";
//                    print_r('First click on the link sent to you in the email');
//                    exit;
                }
            } else {
//                echo "<pre>";
//                print_r('Invalid CAC');
//                exit;
                $this->error_text = 'Invalid CAC Registration';
                $this->error = true;
                $this->setTemplate('validateForLogin');
            }
        } else {
//            echo "<pre>";
//            print_r('You are not Registered');
//            exit;
            $this->error_text = 'You are not Registered';
            $this->error = true;
            $this->setTemplate('validateForLogin');
        }
    }

    protected function processForm(sfWebRequest $request, sfForm $form) {

        $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
        if ($form->isValid()) {
            /**
             * Varun : Logo upload functionality
             */
            $sfGuardUser = $form->save();
            // New created merchant profile id
            $merchantId = $sfGuardUser->getId();
            $companyLogo = $sfGuardUser->getMerchantUser()->getCompanyLogo();
            if ($companyLogo != '' && isset($companyLogo)) {
                $detail = Doctrine::getTable('MerchantProfile')->findByMerchantId($merchantId);
                // update company logo field for merchant profile table
                $logo = $detail[0]['company_logo'];
                $logo = explode(".", $logo);
                $i = count($logo) - 1;
                $setCompanyLogo = $merchantId . '.' . $logo[$i];
                $setLogo = Doctrine::getTable('MerchantProfile')->findLogo($merchantId, $setCompanyLogo);
                // Rename file in Merchant Logo folder
                $path = sfConfig::get('sf_upload_dir') . '/merchant_logo';
                $file_name = $path . "/" . $companyLogo;
                $new_file_name = $path . "/" . $setCompanyLogo;
                if (file_exists($file_name)) {
                    exec("mv $file_name  $new_file_name");
                }
            }
//            $email=$sfGuardUser->getEmailAddress();
            $email = sfConfig::get('app_to');

            $companyName = $sfGuardUser->getMerchantUser()->getMerchantName();

            $host = $request->getUriPrefix();
            $url_root = $request->getPathInfoPrefix();
            $complete_url = $host . $url_root;
sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
            $taskId = EpjobsContext::getInstance()->addJob('SendMailNotification', $this->moduleName . "/sendEmail", array('email' => $email, 'companyName' => $companyName, 'content' => $this->getModuleName() . '/approveMerchantRegistration?id=' . cryptString::encrypt($sfGuardUser->getMerchantUser()->getId()), 'complete_url' => $complete_url, 'partial_name' => 'mailToAdmin','image_tag'=>public_path(sfConfig::get('app_logo_path'),true)), '-1', 'frontend');

            $this->logMessage("scheduled mail job with id: $taskId", 'debug');
            if ($this->getUser()->hasAttribute('msg_for_admin')) {
                $this->getUser()->getAttributeHolder()->remove('msg_for_admin');
            }
            $this->redirect('merchantRegistration/show?id=' . cryptString::encrypt($sfGuardUser->getMerchantUser()->getId()));
        }
    }

    public function executeSendEmail(sfWebRequest $request) {

        $email = $request->getParameter('email');
        $companyName = $request->getParameter('companyName');
        $content = $request->getParameter('content');
        $complete_url = $request->getParameter('complete_url');
        $subject = 'Welcome to pay4bill';
        $partialName = $request->getParameter('partial_name');
        if ($partialName == 'sendEmail') {
            $username = $request->getParameter('username');
            $password = $request->getParameter('password');

            $partialVars = array('companyName' => $companyName, 'username' => $username, 'password' => $password, 'complete_url' => $complete_url);
        } else if ($partialName == 'approvalMailToMerchant') {
            $partialVars = array('companyName' => $companyName, 'email' => $email, 'cac_registration' => $request->getParameter('cac_registration'), 'complete_url' => $complete_url);
        } else {
            $partialVars = array('companyName' => $companyName, 'content' => $content, 'complete_url' => $complete_url);
        }
        $partialVars['path'] = $request->getParameter('image_tag');
        /* Mail firing code starts */
        $mailTo = $email;
        $mailToGroup = array();

        $mailFrom = sfConfig::get('app_sender');
        $signature = sfConfig::get('app_sender');

//        $partialVars = array('companyName' => $companyName, 'content' => $content, 'complete_url' => $complete_url);
        $mailingOptions = array('mailFrom' => $mailFrom, 'mailTo' => $mailTo, 'mailSubject' => $subject);

        $mailObj = new Mailing();
        $mailInfo = $mailObj->sendMail($partialName, $partialVars, $mailingOptions);

        return $this->renderText($mailInfo);
    }

    /**
      Varun : Functionality added to upload merchnant logo and remove merchant logo
     * */
    public function executeUploadMerchantLogo(sfWebRequest $request) {
        $this->addClassTo = $this->getActionName();
        $this->title = 'Upload Merchant Logo';
        $this->action = $this->getModuleName() . "/uploadMerchantLogo";
        $this->uploadButtonText = 'Upload Merchant Logo';
        $merchant_id = $this->getUser()->getGuardUser()->getId();
        $this->merchantId = $merchant_id;
        if ($request->getMethod() == 'POST') {
            if ($_FILES["file"]["name"] != '' && isset($_FILES["file"]["name"])) {
                if ($_FILES["file"]["error"] == 0) {
                    $helperOb = new csvHelper();
                    $file_type = $helperOb->findexts($_FILES["file"]['name']);
                    $file_type = explode('.', $file_type);
                    $i = count($file_type) - 1;
                    if ($file_type[$i] == 'jpg' || $file_type[$i] == 'png' || $file_type[$i] == 'gif') {
                        if ($_FILES["file"]['size'] <= '410623') {

                            $targetPath = sfConfig::get('sf_upload_dir') . '/merchant_logo/' . $merchant_id . '.' . $file_type[$i];
                            $file_uploaded = $helperOb->saveFileInTargetFolder($_FILES['file']['tmp_name'], $targetPath);
                            $setLogo = Doctrine::getTable('MerchantProfile')->findLogo($merchant_id, $merchant_id . '.' . $file_type[$i]);
                            if ($file_uploaded) {
                                $this->getUser()->setFlash('notice', 'Merchant Logo Successfully Added');
                                $merchant_id = $this->getUser()->getGuardUser()->getId();
                                $this->redirect('merchantRegistration/uploadMerchantLogo');
                            } else {
                                $this->msg = 'Error Uploading File';
                                $this->error = true;
                            }
                        } else {
                            $this->msg = 'Merchant Logo can not be more then 400 KB.';
                            $this->error = true;
                        }
                    } else {
                        $this->msg = 'Upload only .png .jpeg or .gif files';
                        $this->error = true;
                    }
                } else {
                    $this->msg = 'Error Uploading File';
                    $this->error = true;
                }
            } else {
                $this->msg = 'Please select company logo to be uploaded.';
                $this->error = true;
            }
        }
    }

    public function executeRemoveLogo(sfWebRequest $request) {
        $merchantId = $request->getParameter('merchantId');
        $removeLogo = Doctrine::getTable('MerchantProfile')->removeLogo($merchantId);
        $this->getUser()->setFlash('notice', "Logo Removed Successfully.", true);
        $this->redirect($this->getModuleName() . '/uploadMerchantLogo');
    }

    /**
     * Varun : End Merchant logo
     */

    /**
     * @author:  Manu Datta
     * @param <type> $request
     * @purpose:    To Merchant As Customer
     */
    public function executeMerchantAsCustomer(sfWebRequest $request) {
        $param = cryptString::decrypt($request->getParameter('var'));
        $details = $request->getPostParameters();
        $_SESSION['is_logout'] = 1;
        $user_id = $this->getUser()->getGuardUser()->getId();
        $group_name = $this->getUser()->getGuardUser()->getGroupNames();
        if (in_array('customer', $group_name)) {
            $this->allreadyCustomer = false;
        } else {
            $this->allreadyCustomer = true;
        }
        $merchantInfo = Doctrine::getTable('MerchantProfile')->findOneByMerchantId($user_id, Doctrine::HYDRATE_ARRAY);
        if (isset($details['sf_guard_user']['Details']['sex'])) {
            $userGroupObj = new sfGuardUserGroup();
            $userGroupObj->setUserId($user_id);
            $userGroupObj->setGroupId(2);
            $userGroupObj->save();
            //added to fix 35174 bug on 23 May 2012
            $customerInfo = Doctrine::getTable('CustomerProfile')->findOneByUserId($user_id);
            if (empty($customerInfo['id'])) {
                $customerObj = new CustomerProfile();
                $customerObj->setName($merchantInfo['merchant_name']);
                $customerObj->setUserId($user_id);
                $customerObj->setSex($details['sf_guard_user']['Details']['sex']);
                $customerObj->setAddress($merchantInfo['merchant_contact_address']);
                $customerObj->setPhoneNumber($merchantInfo['merchant_contact_number']);
                $customerObj->save();
                $customerId = $customerObj->getId();
            } else {
                $customerId = $customerInfo['id'];
            }
            $customerMerchantObj = new MerchantCustomer();
            $customerMerchantObj->setCustomerId($customerId);
            $customerMerchantObj->setMerchantId($merchantInfo['id']);
            $customerMerchantObj->save();
            $this->getUser()->setFlash('notice', 'Merchant as a Customer set successfully'); //Varun : Bug fix FS#34997
            $this->redirect('merchantRegistration/merchantAsCustomer');
        } else if ($param == 2) {
            $customerInfo = Doctrine::getTable('CustomerProfile')->findOneByUserId($user_id);
            $GuardUserGroupObj = Doctrine::getTable('sfGuardUserGroup')->findByUserIdAndGroupId($user_id, 2);
            $invoiceObj = Doctrine::getTable('Invoice')->findByCustomerId($customerInfo->getId(), Doctrine::HYDRATE_ARRAY);

            if (count($invoiceObj) == 0) {
                if (count($GuardUserGroupObj->toArray()) != 0) {
                    foreach ($GuardUserGroupObj as $GuardUserGroupObj) {
                        $GuardUserGroupObj->delete();
                    }
                }
                Doctrine::getTable('MerchantCustomer')->unsetMerchantAsCustomer($merchantInfo['id'], $customerInfo->getId());

                $arrCustomerRelation = Doctrine::getTable('MerchantCustomer')->findByCustomerId($customerInfo->getId());
                // if no relation between customer and mercahnt then delete customer permanently
                $chkRelation = $arrCustomerRelation->toArray();
                if (empty($chkRelation)) {
                    Doctrine::getTable('CustomerProfile')->unsetCustomer($customerInfo->getId());
                }
                $this->getUser()->setFlash('notice', 'Merchant as a Customer removed successfully');
                $this->redirect('merchantRegistration/merchantAsCustomer');
            } else {
                //$this->getUser()->setFlash('error', ' Customer role can not be revoked as Invoice are generated for you');
                $this->getUser()->setFlash('error', 'Merchant cannot be unset as customer as invoice is created for customer role.');
                $this->redirect('merchantRegistration/merchantAsCustomer');
            }
        }
    }

    public function executeCustomerDetails(sfWebRequest $request) {
        $this->form = new CustomerRegistrationForm();
    }

}
