<?php

/**
 * custMgt actions.
 *
 * @package    nisp
 * @subpackage custMgt
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class custMgtActions extends sfActions {

    var $newDynamicArray = array('');

    public function executeGetMerchantSpecificAttributesForSearch(sfWebRequest $request) {
        $merchantId = cryptString::decrypt($request->getParameter('merchant_id'));
        $customerObjType = 1;
        $str = "";
        if ($merchantId != "") {
            $arr = Doctrine::getTable('AttributeMaster')->getAttributeNameByObjectId($merchantId, $customerObjType);
            $str = "";
            $i = 2;
            foreach ($arr as $name => $label) {
                if ($i % 2 == 0) {
                    $varStyle = "class='AlterBG'";
                }
                else
                    $varStyle='';
                $str.="<tr " . $varStyle . "><td  width='17.5%' valign='top'>" . $label . "</td><td  width='35%' valign='top'><input class='FormInput9' type='text' name='dynamicAttr[" . $name . "]' id='" . $name . "_id' ></td></tr>";
                $i++;
            }
        }

        return $this->renderText($str);
    }

    public function executeListingForTeller(sfWebRequest $request) {
        $this->searchCriterias = array('merchant_id' => '', 'cust_id' => '', 'name' => '', 'email_address' => '', 'address' => '', 'phone_number' => '');
        $teller_profileId = $this->getUser()->getGuardUser()->getTellerUser()->getId();

        $parameters = $request->getParameterHolder()->getAll();
        //Varun : Merchant key , to display section to merchant . Variables maintained for search result.
        $this->merchant_key = array_key_exists('merchant_id', $parameters) ? cryptString::decrypt($parameters['merchant_id']) : $this->getUser()->getGuardUser()->getMerchantUser()->getId();
        $email_address = '';
        $cust_name = '';

        $tellerMerchantMappingObjs = Doctrine::getTable('TellerMerchantMapping')->getEnabledMerchantsMappedWithTeller($teller_profileId);
        $this->tellerMerchantMappingObjs = $tellerMerchantMappingObjs;

        $this->customer_profiles = array();
        $this->setPagination = false;
        if ($tellerMerchantMappingObjs->count()) {
            $merchantArray = $this->createArrayOfMerchantIds($tellerMerchantMappingObjs);
            $this->customer_profiles = Doctrine::getTable('CustomerProfile')->getCustomersOfMerchants($merchantArray);

            $total_no_of_records = count($this->customer_profiles);

            if ($total_no_of_records > sfConfig::get('app_records_per_page')) {
                $this->actionTobeCalled = $this->getActionName();
                $this->total_no_of_records = $total_no_of_records;
                $this->no_of_records_per_page = sfConfig::get('app_records_per_page');
                $this->setPagination = true;

                if (!$request->hasParameter('offset')) {
                    $offset = 0;
                } else {

                    $this->getUser()->getAttributeHolder()->remove('msg');
                    $offset = $request->getParameter('offset');
                }
                $this->current_offset = $offset;
                $this->customer_profiles = Doctrine::getTable('CustomerProfile')->getCustomersOfMerchants($merchantArray, $offset, sfConfig::get('app_records_per_page'));


                $this->module = $this->getModuleName();
                $this->offset_last = (($total_no_of_records % sfConfig::get('app_records_per_page') == 0 ? ($total_no_of_records - sfConfig::get('app_records_per_page') ) : $total_no_of_records - ($total_no_of_records % sfConfig::get('app_records_per_page'))));

                $this->offset_next = (($offset + sfConfig::get('app_records_per_page')) > $this->offset_last ? $this->offset_last : ($offset + sfConfig::get('app_records_per_page')));

                $this->offset_prev = (($offset - sfConfig::get('app_records_per_page')) < 0 ? 0 : $offset - sfConfig::get('app_records_per_page'));
            } else {
                $this->setPagination = false;
            }

            if ($this->getUser()->hasAttribute('msg')) {
                $this->msg = $this->getUser()->getAttribute('msg');
            }
        }
        $this->addClassTo = 'index';
    }

    public function executeSearchForTeller(sfWebRequest $request) {

        $searchParams = $request->getParameterHolder()->getAll();
        $teller_profileId = $this->getUser()->getGuardUser()->getTellerUser()->getId();

        if (isset($searchParams['merchant_id'])) {
            $mid = cryptString::decrypt($searchParams['merchant_id']);
            $this->attrNameLabelCombination = Doctrine::getTable('AttributeMaster')->getAttributeNameByObjectId($mid, 1); //@aditi- Added to get Label against each dynamic attribute for the template
        } else {
            $mid = '';
        }
//        $searchParams['merchant_id']=$mid;
        $merchant_id = $mid;
        $searchParams['name'] = $name = array_key_exists('name', $searchParams) ? $searchParams['name'] : '';
        $searchParams['email_address'] = $email_address = array_key_exists('email_address', $searchParams) ? $searchParams['email_address'] : '';
        $address = ''; //$searchParams['address'];
        if (isset($searchParams['customer_id']))
            $searchParams['cust_id'] = $searchParams['customer_id'];
        $searchParams['cust_id'] = $customer_id = array_key_exists('cust_id', $searchParams) ? $searchParams['cust_id'] : '';
        $searchParams['phone_number'] = $phoneNumber = array_key_exists('phone_number', $searchParams) ? $searchParams['phone_number'] : '';
        $this->searchCriterias = $searchParams;

        $tellerMerchantMappingObjs = Doctrine::getTable('TellerMerchantMapping')->getEnabledMerchantsMappedWithTeller($teller_profileId); //aditt- to get only enabled Merchants
        $this->tellerMerchantMappingObjs = $tellerMerchantMappingObjs;

        $merchantsArray = $this->createArrayOfMerchantIds($tellerMerchantMappingObjs);
        if ($merchant_id != '') {
            $merchantsArray = array($merchant_id);
        }
        $dynamic_attributes_array = array_key_exists('dynamicAttr', $searchParams) ? $searchParams['dynamicAttr'] : array();

        $customerProfileObjects = Doctrine::getTable('CustomerProfile')->getCustomersListWithAttributeSearch($merchantsArray, $name, $customer_id, $email_address, $address, $phoneNumber, $dynamic_attributes_array);
        $this->customer_profiles = array();
        $this->setPagination = false;
        if ($tellerMerchantMappingObjs->count()) {
            $this->customer_profiles = $customerProfileObjects;
            $total_no_of_records = count($this->customer_profiles);

            if ($total_no_of_records > sfConfig::get('app_records_per_page') && !$this->admin_login) {
                $this->actionTobeCalled = $this->getActionName();
                $this->total_no_of_records = $total_no_of_records;
                $this->no_of_records_per_page = sfConfig::get('app_records_per_page');
                $this->setPagination = true;

                if (!$request->hasParameter('offset')) {
                    $offset = 0;
                } else {

                    $this->getUser()->getAttributeHolder()->remove('msg');
                    $offset = $request->getParameter('offset');
                }
                $this->current_offset = $offset;
                $this->customer_profiles = Doctrine::getTable('CustomerProfile')->getCustomersListWithAttributeSearch($merchantsArray, $name, $customer_id, $email_address, $address, $phoneNumber, $dynamic_attributes_array, $offset, sfConfig::get('app_records_per_page'));


                $this->module = $this->getModuleName();
                $this->offset_last = (($total_no_of_records % sfConfig::get('app_records_per_page') == 0 ? ($total_no_of_records - sfConfig::get('app_records_per_page') ) : $total_no_of_records - ($total_no_of_records % sfConfig::get('app_records_per_page'))));

                $this->offset_next = (($offset + sfConfig::get('app_records_per_page')) > $this->offset_last ? $this->offset_last : ($offset + sfConfig::get('app_records_per_page')));

                $this->offset_prev = (($offset - sfConfig::get('app_records_per_page')) < 0 ? 0 : $offset - sfConfig::get('app_records_per_page'));
            } else {
                $this->setPagination = false;
            }

            if ($this->getUser()->hasAttribute('msg')) {
                $this->msg = $this->getUser()->getAttribute('msg');
            }
            $this->dynamicAttr_value = $this->returnDynamicAttributesValuesArray($customerProfileObjects, $dynamic_attributes_array);
        }
        $this->addClassTo = 'index';
        $this->setTemplate('listingForTeller');
    }

    public function executeIndex(sfWebRequest $request) {

        ini_set('max_execution_time', '5000');
        ini_set('memory_limit', '512M');

        $this->admin_login = false;
        $this->merchant_login = false;
        $helperOb = new pay4billHelper();
        $parameters = $helperOb->trimArray($request->getParameterHolder()->getAll());
        //Varun : Merchant key , to display section to merchant . Variables maintained for search result.
        $this->merchant_key = array_key_exists('merchant_id', $parameters) ? cryptString::decrypt($parameters['merchant_id']) : $this->getUser()->getGuardUser()->getMerchantUser()->getId();


        $email_address = '';
        $cust_name = '';
        //Varun : Offset value to unset session
        $offset = $request->getParameter('offset');

        $this->merchant_id = array_key_exists('merchant_id', $parameters) ? cryptString::decrypt($parameters['merchant_id']) : $this->getUser()->getGuardUser()->getMerchantUser()->getId();

        $this->createInputHiddenOfMerchantIdForAdminLogin = array_key_exists('merchant_id', $parameters);

        $this->attributes = $attributes = Doctrine::getTable('AttributeMaster')->getAttributeNameByObjectId($this->merchant_id, 1);
        $this->admin_login = pay4billHelper::checkIfAdminUser($this->getUser()->getGuardUser()->getGroups()->toArray());
        $this->merchant_login = pay4billHelper::checkIfMerchantUser($this->getUser()->getGuardUser()->getGroups()->toArray());
        $searchParams = $helperOb->trimArray($request->getParameterHolDer()->getAll());
//@Aditi For dynamic attributes serach for Admin / Merchant Login
        $this->name = $request->hasParameter('name') ? $parameters['name'] : '';
        $this->email_address = $request->hasParameter('email_address') ? $parameters['email_address'] : '';

        $this->phone_number = $request->hasParameter('phone_number') ? $parameters['phone_number'] : '';
        $this->customer_id = $request->hasParameter('customer_id') ? $parameters['customer_id'] : '';
        $this->address = $request->hasParameter('address') ? $parameters['address'] : '';
        $this->dynamic_attributes_array = array_key_exists('dynamicAttr', $searchParams) ? $searchParams['dynamicAttr'] : array();
        foreach ($this->dynamic_attributes_array as $parName => $searchVal) {

            $this->$parName = $searchVal;
        }
//echo "<pre>";
//print_r($this->dynamic_attributes_array);
//exit;
        if ($this->admin_login) {

            $merchantOb = Doctrine::getTable('MerchantProfile')->find($this->merchant_id);
            $this->merchantName = $merchantOb->getMerchantName();
        }
        $searchCriterias['merchant_id'] = $this->merchant_id;
        $customerProfileObjects =
                $this->customer_profiles = Doctrine::getTable('CustomerProfile')->getCustomersListWithAttributeSearch(array($this->merchant_id), $this->name, $this->customer_id, $this->email_address, $this->address, $this->phone_number, $this->dynamic_attributes_array);
        $total_no_of_records = count($this->customer_profiles);


        if ($total_no_of_records > sfConfig::get('app_records_per_page')) {
            $this->actionTobeCalled = $this->getActionName();
            $this->total_no_of_records = $total_no_of_records;
            $this->no_of_records_per_page = sfConfig::get('app_records_per_page');
            $this->setPagination = true;

            if (!$request->hasParameter('offset')) {
                $offset = 0;
            } else {

                $this->getUser()->getAttributeHolder()->remove('msg');
                $offset = $request->getParameter('offset');
            }
            $this->current_offset = $offset;

            $this->customer_profiles = Doctrine::getTable('CustomerProfile')->getCustomersListWithAttributeSearch(array($this->merchant_id), $this->name, $this->customer_id, $this->email_address, $this->address, $this->phone_number, $this->dynamic_attributes_array, $offset, sfConfig::get('app_records_per_page'));


            $this->module = $this->getModuleName();
            $this->offset_last = (($total_no_of_records % sfConfig::get('app_records_per_page') == 0 ? ($total_no_of_records - sfConfig::get('app_records_per_page') ) : $total_no_of_records - ($total_no_of_records % sfConfig::get('app_records_per_page'))));

            $this->offset_next = (($offset + sfConfig::get('app_records_per_page')) > $this->offset_last ? $this->offset_last : ($offset + sfConfig::get('app_records_per_page')));

            $this->offset_prev = (($offset - sfConfig::get('app_records_per_page')) < 0 ? 0 : $offset - sfConfig::get('app_records_per_page'));
        } else {
            $this->setPagination = false;
        }

        if ($this->getUser()->hasAttribute('msg')) {
            $this->msg = $this->getUser()->getAttribute('msg');
        }
        $this->dynamicAttr_value = $this->returnDynamicAttributesValuesArray($this->customer_profiles, $this->dynamic_attributes_array);

        $this->addClassTo = $this->getActionName();
    }

    /*
     * Function to enable the customer
     * @ Saurabh
     */

    public function executeEnableCustomer(sfWebRequest $request) {

        $id = cryptString::decrypt($request->getParameter('id')); // Customer ID
        $mid = $request->getParameter('mid'); // Merchant ID

        $obj = Doctrine::getTable('CustomerProfile')->find($id);
        $obj->setIsActive(1);
        $obj->save();
        $this->getUser()->setFlash('notice', 'Customer Enabled Successfully');
        if (isset($mid) && !isset($_SESSION['logedin_user_group']) && !empty($mid)) {
            $this->redirect('custMgt/index?merchant_id=' . $mid);
        } else {
            $this->redirect('custMgt/index');
        }
    }

    /*
     * Function to disable the customer
     * @ Saurabh
     */

    public function executeDisableCustomer(sfWebRequest $request) {

        $id = cryptString::decrypt($request->getParameter('id')); // Customer ID
        $mid = cryptString::decrypt($request->getParameter('mid')); // Merchant ID
        // Check for the condition from invoice table
        // Customer would not be disabled if due date is greater than equal to todays date
        // And status is 2

        $cnt = Doctrine::getTable('CustomerProfile')->canDisable($id, $mid);

        if(strlen($cnt) > 0){    // If length is 0, merchant is not valid    
            if ($cnt >= 1) {
                $this->getUser()->setFlash('notice', 'Customer cannot be disabled as it is having pending invoices');
            } else {
                //$updateTable = Doctrine::getTable('MerchantCustomer')->setDisableCustomer($id, $mid);
                $obj = Doctrine::getTable('CustomerProfile')->find($id);
                $obj->setIsActive(0);
                $obj->save();
                $this->getUser()->setFlash('notice', 'Customer Disabled Successfully');
            }        
            if (isset($mid) && !isset($_SESSION['logedin_user_group']) && !empty($mid)) {   //If admin Login
                $this->redirect('custMgt/index?merchant_id=' . $request->getParameter('mid'));
            } else {
                $this->redirect('custMgt/index');
            }        
        } else {
             $this->getUser()->setFlash('notice', 'Merchant is not valid');
             $this->redirect('custMgt/index');            
        }
    }

    public function executeShow(sfWebRequest $request) {
        $this->customer_profile = Doctrine::getTable('CustomerProfile')->find(array($request->getParameter('id')));
        $this->addClassTo = 'index';
        $this->forward404Unless($this->customer_profile);
    }

    public function executeNew(sfWebRequest $request) {
        $dynamicAttFieldName = array();
        $uniqueFieldId = '';
        $uniqueFieldName = '';

        if ($request->getParameter('merchant_id') <> '') {
            $merchant_id = $request->getParameter('merchant_id');
            $this->merchantLogin = false;
        } else {
            $merchant_id = $this->getUser()->getGuardUser()->MerchantUser->getId();
            $this->merchantLogin = true;
        }

        $info = Doctrine::getTable('MerchantProfile')->find($merchant_id)->toArray();
        $email_address_mandatary = $info['payment_type'] == 'o' ? true : false;
        /*
         * @Saurabh - 13June2012
         * Get all the dynamic attribute from Attribute Master table against the
         * merchant which are active and are customer type.
         *
         */
        $this->dynamicAttFieldName = Doctrine::getTable('AttributeMaster')->getAllAttributeDetails($merchant_id, 1); // 1: For customer

        $this->getUser()->setAttribute('msg', '');
        $this->dynamicAttFieldName = $dynAttrOption = pay4billHelper::createArrayForDynamicAttrForm($this->dynamicAttFieldName); //@Aditi- Create Array for Dyamic attribute form
        $this->getUser()->setAttribute('msg', '');
        $this->form = new CustomerProfileForm(new CustomerProfile(), array('edit' => false, 'optionForDynAttr' => $dynAttrOption, 'email_address_mandatary' => $email_address_mandatary, 'merchant_id' => $merchant_id));
        $this->addClassTo = $this->getActionName();
    }

    public function executeCustomerNew(sfWebRequest $request) {
        $this->addClassTo = 'new';
        $teller_id = $this->getUser()->getGuardUser()->getTellerUser()->getId();
        $this->merchantObj = Doctrine::getTable('MerchantCustomer')->getAllMerchantFromTellerMerchant($teller_id);
//      echo "<pre>";print_r($merchantObj->toArray());die;
        $this->setTemplate('selectMerchant');
        $this->addClassTo = 'new';
    }

    /**
     * @author:  Manu Datta
     * @param <type> $request
     * @purpose:    To Serch Customer
     */
    public function executeSearchCustomer(sfWebRequest $request) {
        $this->customerDetail = null;
        if ($request->getMethod() == 'POST') {
            $this->actionTobeCalled = $this->getActionName();
            $name = $email = $gender = $phone_number = '';
            $name = $request->getParameter('name');
            $email = $request->getParameter('email');
            $gender = $request->getParameter('gender');
            $phone_number = $request->getParameter('phone_number');
            //getting Merchant ID
            $merchantId = sfContext::getInstance()->getUser()->getGuardUser()->MerchantUser->getId();
            //getting customer details
            $this->customerDetail = Doctrine::getTable('CustomerProfile')->getCustomerInfo($name, $email, $gender, $phone_number, $merchantId);
            $total_no_of_records = count($this->customerDetail);
        }
        if ($this->getUser()->hasAttribute('msg')) {
            $this->msg = $this->getUser()->getAttribute('msg');
        }
    }

    /**
     * @author:  Manu Datta
     * @param <type> $request
     * @purpose:    To add existing Customer
     */
    public function executeAddCustomer(sfWebRequest $request) {
        $allParam = $request->getParameterHolder()->getAll();
        //getting Merchant ID
        $merchantId = sfContext::getInstance()->getUser()->getGuardUser()->MerchantUser->getId();
        foreach ($allParam['customer_ids'] as $cust_id) {
            $merchantCustomerObj = new MerchantCustomer (); //MerchantCustomer object
            $merchantCustomerObj->setCustomerId($cust_id);
            $merchantCustomerObj->setMerchantId($merchantId);
            $merchantCustomerObj->save(); //Inserted the customer and merchant mapping
            //Send mail to Customer
            $customerDetail = Doctrine::getTable('CustomerProfile')->getCustomerInfoById($cust_id);
            $merchantDetail = Doctrine::getTable('MerchantProfile')->getMerchantInfoById($merchantId);
            $email = $customerDetail[0]['sfGuardUser']['email_address'];
            $host = $request->getUriPrefix();
            $url_root = $request->getPathInfoPrefix();
            $complete_url = $host . $url_root;
            $taskId = EpjobsContext::getInstance()->addJob('SendMailNotification', $this->moduleName . "/sendEmailExistCust", array('email' => $email, 'customer_name' => $customerDetail[0]['name'], 'merchant_name' => $merchantDetail[0]['merchant_name'], 'complete_url' => $complete_url . "/" . $this->moduleName . "/login/" . $merchantDetail[0]['tiny_url']), '-1', 'frontend');

            $this->logMessage("scheduled mail job with id: $taskId", 'debug');
        }
        /**
         * Varun : Flash message added
         */
        $this->getUser()->setFlash('notice', 'Customer Successfully Added');
        $this->redirect('custMgt/searchCustomer');
    }

    public function executeCreate(sfWebRequest $request) {

        $this->forward404Unless($request->isMethod(sfRequest::POST));
        /*
         * @Saurabh - 13June2012
         * Get all the dynamic attribute from Attribute Master table against the
         * merchant which are active and are customer type.
         *
         */
        $dynamicAttFieldName = array();
        $uniqueFieldId = '';
        $uniqueFieldName = '';

        $allArr = $request->getParameterHolder()->getAll();

        if ($allArr['customer_profile']['merchant_id'] != '') {
            $merchant_id = $allArr['customer_profile']['merchant_id'];
        } else {
            $merchant_id = $this->getUser()->getGuardUser()->MerchantUser->getId();
        }

        //Fetching the unique attribute
        $arr_unique = Doctrine::getTable('AttributeMaster')->getUniqueAttribute($merchant_id, 1);
        if (count($arr_unique) > 0) {
            $uniqueFieldId = str_replace(' ', '', $arr_unique[0]['id']);
            $uniqueFieldName = str_replace(' ', '', $arr_unique[0]['att_name']);  //DrivingLicence, PanCard etc
        }
        $this->dynamicAttFieldName = Doctrine::getTable('AttributeMaster')->getAllAttributeDetails($merchant_id, 1); // 1: For customer
        //$this->dynamicAttFieldName['is_customer_login'] = array('login' => $is_customer_login);
        // Check if the merchant has customers to login - @Saurabh
        $info = Doctrine::getTable('MerchantProfile')->find($merchant_id)->toArray();
        $email_address_mandatary = $info['payment_type'] == 'o' ? true : false;
        $this->dynamicAttFieldName['email_address_mandatary'] = $email_address_mandatary;

        $this->dynamicAttFieldName['misc'] = array('unique_field_id' => $uniqueFieldId, 'unique_field_name' => $uniqueFieldName);
        $this->dynamicAttFieldName['merchant_id'] = $merchant_id;

//        $this->form = new CustomerRegistrationForm(array(), $this->dynamicAttFieldName);
        $this->form = new CustomerProfileForm(array(), $this->dynamicAttFieldName);

        $this->processForm($request, $this->form);

        $this->setTemplate('new');
    }

    public function executeEdit(sfWebRequest $request) {
        $this->admin_login = false;
        $this->merchant_login = false;
        $this->admin_login = pay4billHelper::checkIfAdminUser($this->getUser()->getGuardUser()->getGroups()->toArray());
        $this->merchant_login = pay4billHelper::checkIfMerchantUser($this->getUser()->getGuardUser()->getGroups()->toArray());

        $dynamicAttFieldName = array();
        $customerProfileObj = Doctrine::getTable('CustomerProfile')->find(cryptString::decrypt($request->getParameter('id')));
        $merchantProfileId = $customerProfileObj->getMerchantProfileId();
        /*
         * @Aditi - 21Jun2012
         * Get all the dynamic attribute from Attribute Master table against the
         * merchant which are active and are customer type.
         *
         */
        $info = $customerProfileObj->getMerchantProfile()->toArray();
        $email_address_mandatary = $info['payment_type'] == 'o' ? true : false;
        $dynamicAttFieldName = Doctrine::getTable('AttributeMaster')->getAllAttributeDetails($merchantProfileId, 1); // 1: For customer



        $this->dynamicAttFieldName = $dynAttrOption = pay4billHelper::createArrayForDynamicAttrForm($dynamicAttFieldName, $customerProfileObj->getObjectId());

        $this->getUser()->setAttribute('msg', '');
// Varun : Id decrypted.
        $this->forward404Unless($customer_profile = Doctrine::getTable('CustomerProfile')->find(array(cryptString::decrypt($request->getParameter('id')))), sprintf('Object customer_profile does not exist (%s).', cryptString::decrypt($request->getParameter('id'))));
        $this->form = new CustomerProfileForm($customer_profile, array('edit' => true, 'optionForDynAttr' => $dynAttrOption, 'email_address_mandatary' => $email_address_mandatary));
        if (!array_key_exists('logedin_user_group', $_SESSION)) {
            $this->admin_login = true;
        }
        $this->addClassTo = '';
        if (array_key_exists('logedin_user_group', $_SESSION) && $_SESSION['logedin_user_group'] == 'customer') {
            $this->admin_login = false;
            $this->addClassTo = 'viewProfiles';
        }
    }

    /*
     * @Saurabh 21June2012
     * Function to show customer details
     */

    public function executeShowcustomer(sfWebRequest $request) {
        $this->customer_profile = Doctrine::getTable('CustomerProfile')->getAllCustomerDetails(cryptString::decrypt($request->getParameter('id')));
        $this->form = $this->customer_profile;
        $this->addClassTo = '';
    }

    /*
     * @Saurabh 21June2012
     * Function to show invoice details against customer
     */

    public function executeShowinvoice(sfWebRequest $request) {
        $id = cryptString::decrypt($request->getParameter('id'));
        $invoiceArr = Doctrine::getTable('Invoice')->getAllInvoicesForCustomer($id);
        $this->invoices = $invoiceArr;
//        $this->form = $this->customer_profile;
        $this->addClassTo = '';
    }

    public function executeLogin(sfWebRequest $request) {

        $getMerchant = explode('/login/', $request->getUri());
        $merchantObj = Doctrine::getTable('MerchantProfile')->findBy('tiny_url', $getMerchant[1]);
        if ($merchantObj) {
            $this->forward('home', 'index');
        } else {
            echo "<pre>";
            print_r('Invalid Request');
            exit;
        }
    }

    /**
     * @author:  Manu Datta
     * @param <type> $request
     * @purpose:    To upload customers
     */
    public function executeUpload(sfWebRequest $request) {
        $this->valid = 0;
        $this->invalid = 0;
        $this->success = false;
        $this->titleXLS = false;
        $this->validFile = false;
        $this->invalidFile = false;
        $dateTime = date("dmY_H_i_s", time());
        $this->merchant_login = pay4billHelper::checkIfMerchantUser($this->getUser()->getGuardUser()->getGroups()->toArray());

        $this->getUser()->setAttribute('msg', '');
        $this->title = 'Bulk Customer Uploader';
        //        $this->action = $this->getModuleName() . "/upload";
        $this->action = "custMgt/upload";
        $this->addClassTo = $this->getActionName();
        $this->uploadButtonText = 'Upload Customers';

        // @Saurabh on 11 June 2012: To get the name of the downloaded file i.e., cus_sample_csv.csv
        // self::executeDownloadSample($request); -- Function moved to pay4billhelper on 13 July 2012
        $this->sample_link_file_name = pay4billHelper::executeDownloadCustomerSample($this->getUser()->getGuardUser()->MerchantUser->getId(), &$this->newDynamicArray);

        //$this->sample_link_file_name = $this->fileName;

        $dynamicFields = implode(",", $this->newDynamicArray);
        // End of function

        if ($request->isMethod('POST')) {
            try {
                $uploadTemplate = new uploadTemplate();
                $upload_file = $request->getFiles('file');
                /* Taking dynamic field as the user get those fields from the sample file */
                //$uploadTemplate->upload($upload_file, 'EmailAddress,Password,Name,Sex,Address,PhoneNumber,use_existing_login');
                $uploadTemplate->upload($upload_file, $dynamicFields);
                $content_arr_list = $uploadTemplate->process();
                //////////////////////Validate CSV///////////////////////////////////////////////////////////////////////////////
                $host = $request->getUriPrefix();
                $url_root = $request->getPathInfoPrefix();
                $complete_url = $host . $url_root;
                $validate = $this->validate($request, $content_arr_list, $complete_url, $dynamicFields);
                //Varun : Error message shown for empty csv FS#34948
                if ($validate['validRows'] == '0' && $validate['invalidRows'] == '0') {
                    $this->msg = 'CSV is Empty.';
                    $this->error = true;
                } else {
                    //////////////////////Creating Valid and Invlid files CSV////////////////////////////////////////////////////////
                    $createdFiles = $uploadTemplate->writeFiles($validate['validArray'], $validate['invalidArray'], sfConfig::get('sf_upload_dir') . '/assets/');
                    $this->validFile = $createdFiles[0];
                    $this->invalidFile = $createdFiles[1];
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    $this->success = true;
                    if ($validate['validRows'] > 0 || $validate['invalidRows'] > 0) {
                        $this->valid = $validate['validRows'];
                        $this->invalid = $validate['invalidRows'];
                    }
                }
            } catch (Exception $e) {
                $this->getUser()->setFlash('notice', $e->getMessage(), false);
            }
        }
    }

    /*
     * @Saurabh on 11 June 2012
     * Function to add fields for download
     */

    public function executeDownloadSample(sfWebRequest $request) {

        $merchant_id = $this->getUser()->getGuardUser()->MerchantUser->getId();
        $arrayHeadings1 = Doctrine::getTable('AttributeMaster')->getAttributeNameByObjectId($merchant_id, 1); // 1 : Customer
        //echo "<pre>";print_r($arrayHeadings1);die;
        $fileName = '';

        $this->fileName = 'cus_sample_csv_' . cryptString::encrypt($merchant_id) . '.csv';

        $arrayHeadings2 = array('EmailAddress', 'Password', 'Name', 'Sex', 'Address', 'PhoneNumber', 'use_existing_login');
        $validate['arrayHeading'][0] = array_merge($arrayHeadings2, $arrayHeadings1);
        $this->newDynamicArray = $validate['arrayHeading'][0];

        pay4billHelper::createCsv(sfConfig::get('sf_upload_dir') . '/assets/' . $this->fileName, $validate['arrayHeading']);
        return $this->fileName;
    }

    /**
     * @author:  Manu Datta
     * @param <type> $content_arr_list
     * @param <type> $complete_url
     * @purpose:    To validate and save uploaded customers
     */
    public function validate($request, $content_arr_list, $complete_url, $dynamicFields = "") {

        $merchant_id = $this->getUser()->getGuardUser()->MerchantUser->getId();
        $uniqueFieldId = '';
        //Fetching the unique attribute
        $arr_unique = Doctrine::getTable('AttributeMaster')->getUniqueAttribute($merchant_id, 1);
        if (count($arr_unique) > 0) {
            $uniqueFieldId = str_replace(' ', '', $arr_unique[0]['id']);  //DrivingLicence, PanCard etc
        }
        /*
         * @Saurabh on 12 June 2012
         * Check whether the customer is allowed to login for a merchant
         */

        $info = Doctrine::getTable('MerchantProfile')->find($merchant_id)->toArray();

        $is_customer_login = $info['payment_type'] == 'o' ? true : false;
        $fieldName = explode(',', $dynamicFields);
        $isError = 0;

        $upload_validator = new upload_validator();
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
        foreach ($content_arr_list as $key => $content_arr_new) {
            // Trimmed the array values - Saurabh on 30 july 2012
            $helperObj = new pay4billHelper();
            $content_arr_new = $helperObj->trimArray($content_arr_new);
            if ($key != 0) {
                if (count($fieldName) == count($content_arr_new)) {
                    if ($content_arr_new[0] == '') {
                        $isError = 1;
                        $upload_validator->invalid_record($content_arr_new, 'Name is Required');
                    } else if ($content_arr_new[0] != '' && !$this->chkName($content_arr_new[0])) {
                        $isError = 1;
                        $upload_validator->invalid_record($content_arr_new, 'Only aplhabets, space and periods are allowed in Name');
                    } else if (strlen($content_arr_new[0]) > 50) {
                        $isError = 1;
                        $upload_validator->invalid_record($content_arr_new, 'Name is more than 50 characters');
                    } else if (strlen($content_arr_new[0]) < 3) {
                        $isError = 1;
                        $upload_validator->invalid_record($content_arr_new, 'Name is less than 3 characters');
                    } else if ($content_arr_new[1] == '') {
                        $isError = 1;
                        $upload_validator->invalid_record($content_arr_new, 'Sex is Required');
                    } else if (strtolower($content_arr_new[1]) != 'male' && strtolower($content_arr_new[1]) != 'female') {
                        $isError = 1;
                        $upload_validator->invalid_record($content_arr_new, 'Invalid Sex');
                    } else if ($content_arr_new[2] == '') {
                        $isError = 1;
                        $upload_validator->invalid_record($content_arr_new, 'Address is Required');
                    } else if (strlen($content_arr_new[2]) > 256) {
                        $isError = 1;
                        $upload_validator->invalid_record($content_arr_new, 'Address is more than 256 characters');
                    } else if ($content_arr_new[3] == '') {
                        $isError = 1;
                        $upload_validator->invalid_record($content_arr_new, 'Phone Number is Required');
                    } else if (!$this->chkPhoneNumber($content_arr_new[3])) {
                        $isError = 1;
                        $upload_validator->invalid_record($content_arr_new, 'Phone Number should be between 6 to 20 digits');
                    } else if ($is_customer_login && $content_arr_new[4] == '') {

                        $isError = 1;
                        $upload_validator->invalid_record($content_arr_new, 'Email Address is Required');
                    } else if ($content_arr_new[4] != '' && !$this->chkEmail($content_arr_new[4])) { // Added by @Aditi
                        $isError = 1;
                        $upload_validator->invalid_record($content_arr_new, 'Invalid Email Address');
                    } else if ($content_arr_new[4] != '' && $this->chkEmailLength($content_arr_new[4])) { // Added by @Aditi
                        $isError = 1;
                        $upload_validator->invalid_record($content_arr_new, 'Email address can not be more than 40 characters');
                    } else if (isset($content_arr_new[5]) && strlen($content_arr_new[5]) == 0) { // Added by @Saurabh
                        $dataType = Doctrine::getTable('AttributeMaster')->getAttributeType($merchant_id, $fieldName[5], 1);
                        $mandatory = $dataType[0]['is_mandatory'];
                        if ($mandatory == 1) {
                            $isError = 1;
                            $upload_validator->invalid_record($content_arr_new, $fieldName[5] . ' is Required');
                        }
                    } else if (isset($content_arr_new[5]) && strlen($content_arr_new[5]) != 0) { // Added by @Saurabh
                        if (strlen($content_arr_new[5]) > 50) {
                            $isError = 1;
                            $upload_validator->invalid_record($content_arr_new, $fieldName[5] . ' can\'t be more than 50 characters');
                        } else {
                            $dataType = Doctrine::getTable('AttributeMaster')->getAttributeType($merchant_id, $fieldName[5], 1);
                            $type = $dataType[0]['att_type'];
                            // Check for uniqueness
                            $getResponse = Doctrine::getTable('AttributeMaster')->isUniqueDataAlreadyExist($uniqueFieldId, $content_arr_new[5], 1);

                            if ($getResponse > 0) { // Already exists
                                $isError = 1;
                                $upload_validator->invalid_record($content_arr_new, $fieldName[5] . ' already exists. It should be unique');
                            } else if (isset($type) && strtolower($type) == 'string') {

                                preg_match("/^[a-zA-Z ]*$/", $content_arr_new[5], $matches);
                                if (!count($matches)) {
                                    $isError = 1;
                                    $upload_validator->invalid_record($content_arr_new, $fieldName[5] . ' should be String');
                                }
                            } else if (isset($type) && strtolower($type) == 'integer') {
                                preg_match("/^[0-9]*$/", $content_arr_new[5], $matches);
                                if (!count($matches)) {
                                    $isError = 1;
                                    $upload_validator->invalid_record($content_arr_new, $fieldName[5] . ' should be Integer');
                                }
                            } else if (isset($type) && strtolower($type) == 'alphanumeric') {
                                preg_match("/^[a-zA-Z0-9]*$/", $content_arr_new[5], $matches);
                                if (!count($matches)) {
                                    $isError = 1;
                                    $upload_validator->invalid_record($content_arr_new, $fieldName[5] . ' should be Alphanumeric');
                                }
                            }
                        }
                    }
                    /*
                     * @Saurabh on 12 June 2012
                     * In any case, the control will go in $content_arr_new[7] to check if it is mandatory or not and
                     * will never come in here. So, we have explicitly check if there is no error, validate further dynamic fields
                     */
                    if ($isError == 0) {
                        if (isset($content_arr_new[6]) && strlen($content_arr_new[6]) == 0) { // Added by @Saurabh
                            $dataType = Doctrine::getTable('AttributeMaster')->getAttributeType($merchant_id, $fieldName[6], 1);
                            $mandatory = $dataType[0]['is_mandatory'];
                            if ($mandatory == 1) {
                                $isError = 1;
                                $upload_validator->invalid_record($content_arr_new, $fieldName[6] . ' is Required');
                            }
                        } else if (isset($content_arr_new[6]) && strlen($content_arr_new[6]) != 0) { // Added by @Saurabh
                            if (strlen($content_arr_new[6]) > 50) {
                                $isError = 1;
                                $upload_validator->invalid_record($content_arr_new, $fieldName[6] . ' can\'t be more than 50 characters');
                            } else {
                                $dataType = Doctrine::getTable('AttributeMaster')->getAttributeType($merchant_id, $fieldName[6], 1);
                                $type = $dataType[0]['att_type'];

                                // Check for uniqueness
                                $getResponse = Doctrine::getTable('AttributeMaster')->isUniqueDataAlreadyExist($uniqueFieldId, $content_arr_new[6], 1);

                                if ($getResponse > 0) { // Already exists
                                    $isError = 1;
                                    $upload_validator->invalid_record($content_arr_new, $fieldName[6] . ' already exists. It should be unique');
                                } else if (isset($type) && strtolower($type) == 'string') {
                                    preg_match("/^[a-zA-Z ]*$/", $content_arr_new[6], $matches);
                                    if (!count($matches)) {
                                        $isError = 1;
                                        $upload_validator->invalid_record($content_arr_new, $fieldName[6] . ' should be String');
                                    }
                                } else if (isset($type) && strtolower($type) == 'integer') {
                                    preg_match("/^[0-9]*$/", $content_arr_new[6], $matches);
                                    if (!count($matches)) {
                                        $isError = 1;
                                        $upload_validator->invalid_record($content_arr_new, $fieldName[6] . ' should be Integer');
                                    }
                                } else if (isset($type) && strtolower($type) == 'alphanumeric') {
                                    preg_match("/^[a-zA-Z0-9]*$/", $content_arr_new[6], $matches);
                                    if (!count($matches)) {
                                        $isError = 1;
                                        $upload_validator->invalid_record($content_arr_new, $fieldName[6] . ' should be Alphanumeric');
                                    }
                                }
                            }
                        }
                    }

                    if ($isError == 0) {
                        if (isset($content_arr_new[7]) && strlen($content_arr_new[7]) == 0) { // Added by @Saurabh
                            $dataType = Doctrine::getTable('AttributeMaster')->getAttributeType($merchant_id, $fieldName[7], 1);
                            $mandatory = $dataType[0]['is_mandatory'];
                            if ($mandatory == 1) {
                                $isError = 1;
                                $upload_validator->invalid_record($content_arr_new, $fieldName[7] . ' is Required');
                            }
                        } else if (isset($content_arr_new[7]) && strlen($content_arr_new[7]) != 0) { // Added by @Saurabh
                            if (strlen($content_arr_new[7]) > 50) {
                                $isError = 1;
                                $upload_validator->invalid_record($content_arr_new, $fieldName[7] . ' can\'t be more than 50 characters');
                            } else {
                                $dataType = Doctrine::getTable('AttributeMaster')->getAttributeType($merchant_id, $fieldName[7], 1);
                                $type = $dataType[0]['att_type'];
                                // Check for uniqueness
                                $getResponse = Doctrine::getTable('AttributeMaster')->isUniqueDataAlreadyExist($uniqueFieldId, $content_arr_new[9], 1);

                                if ($getResponse > 0) { // Already exists
                                    $isError = 1;
                                    $upload_validator->invalid_record($content_arr_new, $fieldName[7] . ' already exists. It should be unique');
                                } else if (isset($type) && strtolower($type) == 'string') {

                                    preg_match("/^[a-zA-Z ]*$/", $content_arr_new[7], $matches);
                                    if (!count($matches)) {
                                        $isError = 1;
                                        $upload_validator->invalid_record($content_arr_new, $fieldName[7] . ' should be String');
                                    }
                                } else if (isset($type) && strtolower($type) == 'integer') {
                                    preg_match("/^[0-9]*$/", $content_arr_new[7], $matches);
                                    if (!count($matches)) {
                                        $isError = 1;
                                        $upload_validator->invalid_record($content_arr_new, $fieldName[7] . ' should be Integer');
                                    }
                                } else if (isset($type) && strtolower($type) == 'alphanumeric') {
                                    preg_match("/^[a-zA-Z0-9]*$/", $content_arr_new[7], $matches);
                                    if (!count($matches)) {
                                        $isError = 1;
                                        $upload_validator->invalid_record($content_arr_new, $fieldName[7] . ' should be Alphanumeric');
                                    }
                                }
                            }
                        }
                    }

                    if ($isError == 0) {
                        if (isset($content_arr_new[8]) && strlen($content_arr_new[8]) == 0) { // Added by @Saurabh
                            $dataType = Doctrine::getTable('AttributeMaster')->getAttributeType($merchant_id, $fieldName[8], 1);
                            $mandatory = $dataType[0]['is_mandatory'];
                            if ($mandatory == 1) {
                                $isError = 1;
                                $upload_validator->invalid_record($content_arr_new, $fieldName[8] . ' is Required');
                            }
                        } else if (isset($content_arr_new[8]) && strlen($content_arr_new[8]) != 0) { // Added by @Saurabh
                            if (strlen($content_arr_new[8]) > 50) {
                                $isError = 1;
                                $upload_validator->invalid_record($content_arr_new, $fieldName[8] . ' can\'t be more than 50 characters');
                            } else {

                                $dataType = Doctrine::getTable('AttributeMaster')->getAttributeType($merchant_id, $fieldName[8], 1);
                                $type = $dataType[0]['att_type'];
                                // Check for uniqueness
                                $getResponse = Doctrine::getTable('AttributeMaster')->isUniqueDataAlreadyExist($uniqueFieldId, $content_arr_new[10], 1);

                                if ($getResponse > 0) { // Already exists
                                    $isError = 1;
                                    $upload_validator->invalid_record($content_arr_new, $fieldName[8] . ' already exists. It should be unique');
                                } else if (isset($type) && strtolower($type) == 'string') {

                                    preg_match("/^[a-zA-Z ]*$/", $content_arr_new[8], $matches);
                                    if (!count($matches)) {
                                        $isError = 1;
                                        $upload_validator->invalid_record($content_arr_new, $fieldName[8] . ' should be String');
                                    }
                                } else if (isset($type) && strtolower($type) == 'integer') {
                                    preg_match("/^[0-9]*$/", $content_arr_new[8], $matches);
                                    if (!count($matches)) {
                                        $isError = 1;
                                        $upload_validator->invalid_record($content_arr_new, $fieldName[8] . ' should be Integer');
                                    }
                                } else if (isset($type) && strtolower($type) == 'alphanumeric') {
                                    preg_match("/^[a-zA-Z0-9]*$/", $content_arr_new[8], $matches);
                                    if (!count($matches)) {
                                        $isError = 1;
                                        $upload_validator->invalid_record($content_arr_new, $fieldName[8] . ' should be Alphanumeric');
                                    }
                                }
                            }
                        }
                    }

                    if ($isError == 0) {
                        if (isset($content_arr_new[9]) && strlen($content_arr_new[9]) == 0) { // Added by @Saurabh
                            $dataType = Doctrine::getTable('AttributeMaster')->getAttributeType($merchant_id, $fieldName[9], 1);
                            $mandatory = $dataType[0]['is_mandatory'];
                            if ($mandatory == 1) {
                                $isError = 1;
                                $upload_validator->invalid_record($content_arr_new, $fieldName[9] . ' is Required');
                            }
                        } else if (isset($content_arr_new[9]) && strlen($content_arr_new[9]) != 0) { // Added by @Saurabh
                            if (strlen($content_arr_new[9]) > 50) {
                                $isError = 1;
                                $upload_validator->invalid_record($content_arr_new, $fieldName[9] . ' can\'t be more than 50 characters');
                            } else {

                                $dataType = Doctrine::getTable('AttributeMaster')->getAttributeType($merchant_id, $fieldName[9], 1);
                                $type = $dataType[0]['att_type'];
                                // Check for uniqueness
                                $getResponse = Doctrine::getTable('AttributeMaster')->isUniqueDataAlreadyExist($uniqueFieldId, $content_arr_new[11], 1);

                                if ($getResponse > 0) { // Already exists
                                    $isError = 1;
                                    $upload_validator->invalid_record($content_arr_new, $fieldName[9] . ' already exists. It should be unique');
                                } else if (isset($type) && strtolower($type) == 'string') {

                                    preg_match("/^[a-zA-Z ]*$/", $content_arr_new[9], $matches);
                                    if (!count($matches)) {
                                        $isError = 1;
                                        $upload_validator->invalid_record($content_arr_new, $fieldName[9] . ' should be String');
                                    }
                                } else if (isset($type) && strtolower($type) == 'integer') {
                                    preg_match("/^[0-9]*$/", $content_arr_new[9], $matches);
                                    if (!count($matches)) {
                                        $isError = 1;
                                        $upload_validator->invalid_record($content_arr_new, $fieldName[9] . ' should be Integer');
                                    }
                                } else if (isset($type) && strtolower($type) == 'alphanumeric') {
                                    preg_match("/^[a-zA-Z0-9]*$/", $content_arr_new[9], $matches);
                                    if (!count($matches)) {
                                        $isError = 1;
                                        $upload_validator->invalid_record($content_arr_new, $fieldName[9] . ' should be Alphanumeric');
                                    }
                                }
                            }
                        }
                    }
                    // If no error is found then proceed
                    if ($isError == 0) {



                        /*
                         * @Saurabh on 12 June 2012
                         * Add entry in tbl_object table
                         */
                        $objectOB = new Object();
                        $objectOB->setMerchantId($merchant_id);
                        $objectOB->setObjectTypeId('1'); // 1: In case of customer
                        $objectOB->save();

                        $objectID = $objectOB->getId();

                        $new_customer_obj = new CustomerProfile();
                        $new_customer_obj->setName($content_arr_new[0]);
                        $new_customer_obj->setMerchantProfileId($merchant_id);
                        $new_customer_obj->setObjectId($objectID); // Add object id in customr profile
                        $new_customer_obj->setSex(ucfirst(strtolower($content_arr_new[1])));
                        if (trim($content_arr_new[4]) != '') {
                            $new_customer_obj->setEmailAddress(trim($content_arr_new[4]));
                        }
                        $new_customer_obj->setAddress($content_arr_new[2]);
                        $new_customer_obj->setPhoneNumber($content_arr_new[3]);
                        $new_customer_obj->setCustomerId(pay4billHelper::generateCustomerId($merchant_id, $content_arr_new[0]));
                        if ($is_customer_login) {
                            $new_customer_obj->setPin(pay4billHelper::generatePin());
                        }
                        $new_customer_obj->save();
                        $upload_validator->valid_record($content_arr_new);

                        /*
                         * @Saurabh on 12 June 2012
                         * Add entry in tbl_attribute_value table
                         */
                        if (isset($fieldName[5]) && !empty($fieldName[5])) {

                            $dataType = Doctrine::getTable('AttributeMaster')->getAttributeType($merchant_id, $fieldName[5], 1);
                            $attID = $dataType[0]['id'];
                            $attVal = $content_arr_new[5];

                            if (!empty($attVal)) {
                                $objectOB = new AttributeValue();
                                $objectOB->setAttValue($attVal);
                                $objectOB->setAttMasterId($attID);
                                $objectOB->setObjectId($objectID);
                                $objectOB->save();
                            }
                        }
                        if (isset($fieldName[6]) && !empty($fieldName[6])) {
                            $dataType = Doctrine::getTable('AttributeMaster')->getAttributeType($merchant_id, $fieldName[6], 1);
                            $attID = $dataType[0]['id'];
                            $attVal = $content_arr_new[6];
                            if (!empty($attVal)) {
                                $objectOB = new AttributeValue();
                                $objectOB->setAttValue($attVal);
                                $objectOB->setAttMasterId($attID);
                                $objectOB->setObjectId($objectID);
                                $objectOB->save();
                            }
                        }
                        if (isset($fieldName[7]) && !empty($fieldName[7])) {
                            $dataType = Doctrine::getTable('AttributeMaster')->getAttributeType($merchant_id, $fieldName[7], 1);
                            $attID = $dataType[0]['id'];
                            $attVal = $content_arr_new[7];
                            if (!empty($attVal)) {
                                $objectOB = new AttributeValue();
                                $objectOB->setAttValue($attVal);
                                $objectOB->setAttMasterId($attID);
                                $objectOB->setObjectId($objectID);
                                $objectOB->save();
                            }
                        }
                        if (isset($fieldName[8]) && !empty($fieldName[8])) {
                            $dataType = Doctrine::getTable('AttributeMaster')->getAttributeType($merchant_id, $fieldName[8], 1);
                            $attID = $dataType[0]['id'];
                            $attVal = $content_arr_new[8];

                            if (!empty($attVal)) {
                                $objectOB = new AttributeValue();
                                $objectOB->setAttValue($attVal);
                                $objectOB->setAttMasterId($attID);
                                $objectOB->setObjectId($objectID);
                                $objectOB->save();
                            }
                        }
                        if (isset($fieldName[9]) && !empty($fieldName[9])) {
                            $dataType = Doctrine::getTable('AttributeMaster')->getAttributeType($merchant_id, $fieldName[9], 1);
                            $attID = $dataType[0]['id'];
                            $attVal = $content_arr_new[9];
                            if (!empty($attVal)) {
                                $objectOB = new AttributeValue();
                                $objectOB->setAttValue($attVal);
                                $objectOB->setAttMasterId($attID);
                                $objectOB->setObjectId($objectID);
                                $objectOB->save();
                            }
                        }
//                        if (!is_null($new_customer_obj->getEmailAddress())) {
//                            $subject = "Welcome to Pay4bill";
//                            $partial_name = 'sendCustEmail';
//                            $taskId = EpjobsContext::getInstance()->addJob('SendMailNotification', $this->moduleName . "/SendEmail", array('subject' => $subject, 'email_address' => $new_customer_obj->getEmailAddress(), 'partial_name' => $partial_name, 'customer_name' => $new_customer_obj->getName()), '-1', 'frontend');
//                            $this->logMessage("scheduled mail job with id: $taskId", 'debug');
//                        }
                        if (!is_null($new_customer_obj->getEmailAddress())) {
                            $host = $request->getUriPrefix();
                            sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));

                            $subject = 'Welcome to Pay4bill';
                            $partial_name = 'sendCustEmail';

                            $parametersArray = array('subject' => $subject, 'email_address' => $new_customer_obj->getEmailAddress(), 'partial_name' => $partial_name, 'customer_name' => $new_customer_obj->getName(), 'merchant_name' => $new_customer_obj->getMerchantProfile()->getMerchantName(), 'customer_id' => $new_customer_obj->getCustomerId(), 'image_tag' => public_path(sfConfig::get('app_logo_path'), true));
//                            $parametersArray = array('subject' => $subject, 'email_address' => $new_customer_obj->getEmailAddress(), 'partial_name' => $partial_name, 'customer_name' => $new_customer_obj->getName(), 'merchant_name' => $new_customer_obj->getMerchantProfile()->getMerchantName(), 'customer_id' => $new_customer_obj->getCustomerId());
                            if (!is_null($new_customer_obj->getPin())) {
                                $url = $host . "/" . url_for('@online_customer_registration_link');
                                $parametersArray['pin'] = $new_customer_obj->getPin();
                            } else {
                                $url = $host . "/" . url_for('@biller_customer_registration_link');
                            }
                            $parametersArray['url'] = $url;
                            $taskId = EpjobsContext::getInstance()->addJob('SendMailNotification', $this->moduleName . "/SendEmail", $parametersArray, '-1', 'frontend');
                        }
                    }
                } else {
                    $isError = 1;
                    $upload_validator->invalid_record($content_arr_new, 'Incomplete Data in Row');
                }
            }
            $isError = 0; //added to reset the condition var value
        }//End Of Foreach
        return $upload_validator->getValues();
    }

    /**
     * @author:  Manu Datta
     * @param <type> $value
     * @purpose:    To check Email no valid or not
     */
    private function chkEmail($value) {
        preg_match("/^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/", $value, $matches);

        if (count($matches)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @author:  Saurabh
     * @param <type> $value
     * @purpose:    To check Email count
     */
    private function chkEmailLength($value) {
        if (strlen($value) > 40) {
            return true;
        } else {
            return false;
        }
    }

    private function chkName($value) {
        preg_match('/^[a-zA-Z\s\.]+$/', $value, $matches);

        if (count($matches)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @author:  Manu Datta
     * @param <type> $value
     * @purpose:    To check Phone no valid or not
     */
    private function chkPhoneNumber($value) {
        preg_match("/^[0-9]*$/", $value, $matches);
        if (count($matches) && (strlen($value) >= 6 && strlen($value) <= 20)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @author:  Manu Datta
     * @param <type> $request
     * @purpose:    To downloads
     */
    public function executeDownloads(sfWebRequest $request) {
        $title = cryptString::decrypt($request->getParameter('file'));
        if (pay4billHelper::downloads($title)) {
            $this->setLayout(false);
            return sfView::NONE;
        } else {
            $this->getUser()->setFlash('notice', "Download Template Does Not Exist In Directory.", false);
            $this->forward('custMgt', 'upload');
        }
    }

    public function executeUpdate(sfWebRequest $request) {
        $this->getUser()->setAttribute('msg', '');
        $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
        $this->forward404Unless($customer_profile = Doctrine::getTable('CustomerProfile')->find(array($request->getParameter('id'))), sprintf('Object customer_profile does not exist (%s).', $request->getParameter('id')));
        $this->form = new CustomerProfileForm($customer_profile);

        $this->processForm($request, $this->form);

        $this->setTemplate('edit');
    }

    /**
     * @author:  Manu Datta
     * @param <type> $request
     * @purpose:    To delete customer
     */
    public function executeDelete(sfWebRequest $request) {
        //getting Merchant ID
        $merchantId = sfContext::getInstance()->getUser()->getGuardUser()->MerchantUser->getId();

        $this->forward404Unless($customer_profile = Doctrine::getTable('CustomerProfile')->find(array($request->getParameter('id'))), sprintf('Object customer_profile does not exist (%s).', $request->getParameter('id')));

        $invoices_of_customer_merchant = Doctrine::getTable('Invoice')->findByCustomerIdAndMerchantId($customer_profile->getId(), $merchantId);
        if (count($invoices_of_customer_merchant->toArray()) == 0) {

            try {
                $con = Doctrine::getTable('CustomerProfile')->getConnection();
                $con->beginTransaction();

                $resultObjs = Doctrine::getTable('UserCustomerMapping')->findBy('customer_profile_id', $customer_profile->getId());
                foreach ($resultObjs as $ob) {
                    $ob->delete();
                }
                $resultObjs = Doctrine::getTable('AttributeValue')->findBy('object_id', $customer_profile->getObjectId());
                foreach ($resultObjs as $ob) {
                    $ob->delete();
                }
                $customer_profile->delete();
                $customer_profile->getObject()->delete();

                $con->commit();
            } catch (Exception $e) {


                print_r($e->getMessage());

                $con->rollback();
            }
            $this->getUser()->setFlash('notice', "Customer Deleted Successfully.");
            $this->redirect('custMgt/index');
        } else {
            $this->getUser()->setFlash('notice', "Customer cannot be deleted as invoice is generated");
            $this->redirect('custMgt/index');
        }
    }

    protected function processForm(sfWebRequest $request, sfForm $form) {

        $arrAll = $request->getParameterHolder()->getAll();

        if ($arrAll['customer_profile']['merchant_id'] != '') {
            $merchant_id = $arrAll['customer_profile']['merchant_id'];
            $tellerLogin = true;
        } else {
            $merchant_id = $this->getUser()->getGuardUser()->MerchantUser->getId();
            $tellerLogin = false;
        }
        $form_array = $request->getParameter($form->getName());
//        $form_array['username'] = pay4BillHelper::generateCustomerId($merchant_id, $form_array['Details']['name']);
//        $form_array['username'] = pay4BillHelper::generateCustomerId($merchant_id, $form_array['name']);
        /*
         * @Saurabh 14-jun-2012
         *  Checking if the merchant allow their customer to login. If No,
         *  No entry will be forced to add in sfGuardUser Table
         *  unset the email :
         */
        $info = Doctrine::getTable('MerchantProfile')->find($merchant_id)->toArray();
//        $is_customer_login = $info['is_customer_login'];
//        if($is_customer_login == 0) {
//           unset($form_array['email_address']);
//        }

        $form->bind($form_array, $request->getFiles($form->getName()));

        if ($form->isValid()) {


            $isNew = $form->getObject()->isNew();
            $all_values = $form->getTaintedValues();

//            if ($is_customer_login == 0) {unset($all_values['email_address']);}

            $name = '';

//            if (array_key_exists("Details", $all_values)) {
//                if ($all_values['Details']['name'] != '' && isset($all_values['Details']['name'])) {
//                    $name = $all_values['Details']['name']; //Varun : name value to display name in flash message .
//                }
//            }
            if (array_key_exists("name", $all_values)) {
                if ($all_values['name'] != '' && isset($all_values['name'])) {
                    $name = $all_values['name']; //Varun : name value to display name in flash message .
                }
            }

            /*
             * @Saurabh 14June12
             * Create array for dynamic Attributes of customer
             * from the array
             */
            $attArr = array();
            foreach ($all_values as $k => $v) {
                if (is_array($v))
                    continue;
//                if ($k == 'merchant_id' || $k == 'username' || $k == 'email_address' || $k == 'password' || $k == 'password2' || $k == '_csrf_token' || $k == 'attribute_master_id' || $k == 'attribute_master_name'
//                    )continue;
                if ($k == 'merchant_id' || $k == 'name' || $k == 'sex' || $k == 'address' || $k == 'phone_number' || $k == 'email_address' || $k == '_csrf_token' || $k == 'attribute_master_id' || $k == 'attribute_master_name'

                    )continue;
                $attArr[$k] = $v;
            }

            /*
             * @Saurabh on 13 June 2012
             * Add entry in tbl_object table
             */
            $objectMyOB = new Object();
            $objectMyOB->setMerchantId($merchant_id);
            $objectMyOB->setObjectTypeId('1'); // 1: In case of customer
            $objectMyOB->save();
            $objectID = $objectMyOB->getId();
            /* End of insertion into tbl_object */

            /*
             * @Saurabh - 14June12
             * $form->save :  Saves data at 2 places - Customer_Profile & SFGuardUser Table
             * If customer login is allowed then use $form->Save method
             * otherwise saving the data in just Customer_Profile table
             */
//            if ($is_customer_login == 1) {
//                $sfGuardUser = $form->save();
//                $id = $sfGuardUser->getCustomerUser()->getId();
//            } else {
            $customerOb = new CustomerProfile();
            $customerOb->setName($all_values['name']);
            $customerOb->setAddress($all_values['address']);
            $customerOb->setSex($all_values['sex']);
            $customerOb->setMerchantProfileId($all_values['merchant_id']);
            $customerOb->setPhoneNumber($all_values['phone_number']);
            $customerOb->save();
            $id = $customerOb->getId();
            // }


            /*
             * @Saurabh 14June12
             * Update Object ID of customer_profile table
             */
            $CPObject = Doctrine::getTable('CustomerProfile')->find($id);
            $CPObject->setObjectId($objectID);
            $CPObject->save();
            /* End of updation of customer_profile */


            /* @Saurabh 14June12
             * Save the value in Attribute_Value table
             */

            foreach ($attArr as $k => $v) {
                if (strlen($v) > 0) {
                    $dataType = Doctrine::getTable('AttributeMaster')->getAttributeType($merchant_id, $k, 1);
                    $attID = $dataType[0]['id'];
                    $attVal = $v;

                    $objectOB = new AttributeValue();
                    $objectOB->setAttValue($attVal);
                    $objectOB->setAttMasterId($attID);
                    $objectOB->setObjectId($objectID);
                    $objectOB->save();
                }
            }

            /* End of insertion in attribute_value table */

            //Geting Merchant id and save MerchantCustomer Table
            // $merchantId = sfContext::getInstance()->getUser()->getGuardUser()->MerchantUser->getId();
            if ($isNew) {
                $customerId = $id; //$sfGuardUser->getCustomerUser()->getId();
            } else {
                $customerId = $form->getObject()->getId();
            }

//            $merchantCustomerObj = new MerchantCustomer();
//            $merchantCustomerObj->setCustomerId($customerId);
//            $merchantCustomerObj->setMerchantId($merchant_id);
//            try {
//                $merchantCustomerObj->save(); //Inserted the customer and merchant mapping
//            } catch (Exception $ex) {
//                echo $ex->getMessage() . '<br>';
//                echo $ex->getTraceAsString() . '<br>';
//            }
            $merchantData = Doctrine::getTable('MerchantProfile')->findOneBy('id', $merchant_id); //get Merchant Data By Merchant Id
            if (array_key_exists('email_address', $all_values)) {
                //$sfGuardUser->link('Groups', array(2));
                //$sfGuardUser->save();
                //$email = $sfGuardUser->getEmailAddress();

                $host = $request->getUriPrefix();
                $url_root = $request->getPathInfoPrefix();
                $complete_url = $host . $url_root;
//                $taskId = EpjobsContext::getInstance()->addJob('SendMailNotification', $this->moduleName . "/sendEmail", array('email' => $email, 'customer_name' => $sfGuardUser->getCustomerUser()->getName(), 'merchant_name' => $merchantData->getMerchantName(), 'username' => $all_values['email_address'], 'password' => $all_values['password'], 'complete_url' => $complete_url . "/" . $this->moduleName . "/login/" . $merchantData->getTinyUrl()), '-1', 'frontend');
                $taskId = EpjobsContext::getInstance()->addJob('SendMailNotification', $this->moduleName . "/sendEmail", array('email' => '', 'customer_name' => $all_values['name'], 'merchant_name' => $merchantData->getMerchantName(), 'username' => $all_values['email_address'], 'password' => '', 'complete_url' => $complete_url . "/" . $this->moduleName . "/login/" . $merchantData->getTinyUrl()), '-1', 'frontend');

                $this->logMessage("scheduled mail job with id: $taskId", 'debug');
                $this->getUser()->setFlash('notice', 'Customer Successfully Added');

                if ($_SESSION['logedin_user_group'] == 'teller') {// COming from teller
                    $this->redirect('custMgt/listingForTeller');
                } elseif ($_SESSION['logedin_user_group'] == 'merchant') {
                    $this->redirect('custMgt/index');
                }
            } else {
//              $this->getUser()->setFlash('notice', "Details for <b>" . ucwords($name) . "</b> Successfully Updated");
                $this->getUser()->setFlash('notice', "Customer Successfully Added");

                if ($arrAll['customer_profile']['merchant_id'] != '') {// COming from teller
                    $this->redirect('custMgt/listingForTeller');
                } else {
                    $this->redirect('custMgt/index');
                }
            }
        }
    }

    public function executeSaveNewCustomer(sfWebRequest $request) {
        $this->admin_login = false;
        $this->addClassTo = 'new';

        $params = $request->getParameterHolder()->getAll();
        if ($params['customer_profile']['merchant_id'] <> '') {
            $merchant_id = $params['customer_profile']['merchant_id'];
            $this->merchantLogin = true;
        } else {
            $merchant_id = $this->getUser()->getGuardUser()->MerchantUser->getId();
            $this->merchantLogin = false;
        }

        $dynamicAttFieldName = Doctrine::getTable('AttributeMaster')->getAllAttributeDetails($merchant_id, 1); // 1: For customer


        $info = Doctrine::getTable('MerchantProfile')->find($merchant_id)->toArray();
        $email_address_mandatary = $info['payment_type'] == 'o' ? true : false;
        $this->dynamicAttFieldName = $dynAttrOption = pay4billHelper::createArrayForDynamicAttrForm($dynamicAttFieldName);

        $this->form = new CustomerProfileForm('', array('edit' => false, 'optionForDynAttr' => $dynAttrOption, 'email_address_mandatary' => $email_address_mandatary));


        $this->form->bind($request->getPostParameter($this->form->getName()));
        if ($this->form->isValid()) {
            try {

                $info = Doctrine::getTable('MerchantProfile')->find($merchant_id)->toArray();
                $is_customer_login = $info['payment_type'] == 'o' ? true : false;

                $con = Doctrine::getTable('CustomerProfile')->getConnection();
                $con->beginTransaction();

                $tblObject = new Object();
                $tblObject->setMerchantId($merchant_id);
                $tblObject->setObjectTypeId(1);

                $CustomerPrObj = new CustomerProfile();
                $CustomerPrObj->setObject($tblObject);
                $CustomerPrObj->setName($params['customer_profile']['name']);
                $CustomerPrObj->setAddress($params['customer_profile']['address']);
                $CustomerPrObj->setSex($params['customer_profile']['sex']);
                if (trim($params['customer_profile']['email_address']) != '') {
                    $CustomerPrObj->setEmailAddress(trim($params['customer_profile']['email_address']));
                }
                $CustomerPrObj->setPhoneNumber($params['customer_profile']['phone_number']);
                $CustomerPrObj->setMerchantProfileId($merchant_id);

                $CustomerPrObj->setCustomerId(pay4billHelper::generateCustomerId($merchant_id, $params['customer_profile']['name']));
                if ($is_customer_login) {
                    $CustomerPrObj->setPin(pay4billHelper::generatePin());
                }
                $tblObject->save();

                $CustomerPrObj->save();

                if (isset($params['customer_profile']['attrForm'])) {  //If Merchant of the Customer is having any dynamic attribute
                    foreach ($params['customer_profile']['attrForm'] as $atName => $attVal) {
                        if ($attVal != "") {
                            foreach ($dynamicAttFieldName as $attMasterRecordNo => $attDetails) {
                                if ($attDetails['att_name'] == $atName) {
                                    $attValoBj = new AttributeValue();
                                    $attValoBj->setAttMasterId($attDetails['id']);
                                    $attValoBj->setAttValue($attVal);
                                    $attValoBj->setObject($tblObject);
                                    $attValoBj->save();
                                }
                            }
                        }
                    }
                }
                $con->commit();
                $this->getUser()->setFlash('notice', "Customer Created Successfully");
                if (!is_null($CustomerPrObj->getEmailAddress())) {
                    $host = $request->getUriPrefix();
                    sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));

                    $subject = 'Welcome to Pay4bill';
                    $partial_name = 'sendCustEmail';
                    sfContext::getInstance()->getConfiguration()->loadHelpers(array('Asset'));
                    $parametersArray = array('subject' => $subject, 'email_address' => $CustomerPrObj->getEmailAddress(), 'partial_name' => $partial_name, 'customer_name' => $CustomerPrObj->getName(), 'merchant_name' => $CustomerPrObj->getMerchantProfile()->getMerchantName(), 'customer_id' => $CustomerPrObj->getCustomerId(), 'image_tag' => public_path('images/logo-pay4bill.gif', true));
                    if ($CustomerPrObj->getMerchantProfile()->getPaymentType() == 'o') {
                        $url = $host . url_for('@online_customer_registration_link');
                        $parametersArray['pin'] = $CustomerPrObj->getPin();
                    } else {
                        $url = $host . url_for('@biller_customer_registration_link');
                    }
                    $parametersArray['url'] = $url;
                    sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));

//print_r($request->getUriPrefix().public_path(''));
//exit;
                    $taskId = EpjobsContext::getInstance()->addJob('SendMailNotification', $this->moduleName . "/SendEmail", $parametersArray, '-1', 'frontend');
                }
            } catch (Exception $e) {
                $con->rollback();
            }
        } else {
            //foreach ($this->form->getFormFieldSchema() as $name => $formField) {
            //  echo $formField->getName() . "-" . $formField->getError() . "<br>"; // renders unformatted
            // }
            //exit;
            if (array_key_exists('logedin_user_group', $_SESSION) && $_SESSION['logedin_user_group'] == 'merchant') {
                $this->admin_login = false;
            } elseif (!array_key_exists('logedin_user_group', $_SESSION)) {
                $this->admin_login = true;
            }
            $this->setTemplate('new');
            return;
        }

        if (!array_key_exists('logedin_user_group', $_SESSION)) {
            $this->admin_login = true;

            $this->redirect('custMgt/index?merchant_id=' . cryptString::encrypt($merchantCustObj->getMerchantId()));
        } else {

            if ($_SESSION['logedin_user_group'] == 'teller') {// COming from teller
                $this->redirect('@teller_login');
            } elseif ($_SESSION['logedin_user_group'] == 'merchant') {

                $this->addClassTo = '';
                $this->redirect('custMgt/index');
            }
        }
    }

    public function executeSaveEditedCustomer(sfWebRequest $request) {
        $this->admin_login = false;
        $this->merchant_login = false;
        $this->admin_login = pay4billHelper::checkIfAdminUser($this->getUser()->getGuardUser()->getGroups()->toArray());
        $this->merchant_login = pay4billHelper::checkIfMerchantUser($this->getUser()->getGuardUser()->getGroups()->toArray());

        $params = $request->getParameterHolder()->getAll();
        $customer_profile_id = cryptString::decrypt($request->getParameter('id'));
        $customer_profile = Doctrine::getTable('CustomerProfile')->find(array($customer_profile_id));
        $info = $customer_profile->getMerchantProfile()->toArray();
        $email_address_mandatary = $info['payment_type'] == 'o' ? true : false;
        $dynamicAttFieldName = Doctrine::getTable('AttributeMaster')->getAllAttributeDetails($customer_profile->getMerchantProfileId(), 1); // 1: For customer
        $this->dynamicAttFieldName = $dynAttrOption = pay4billHelper::createArrayForDynamicAttrForm($dynamicAttFieldName, $customer_profile->getObjectId());

        $this->form = new CustomerProfileForm($customer_profile, array('edit' => true, 'optionForDynAttr' => $dynAttrOption, 'object_id' => $customer_profile->getObjectId(), 'email_address_mandatary' => $email_address_mandatary));


        $this->form->bind($request->getPostParameter($this->form->getName()));
        if ($this->form->isValid()) {

            try {
                $con = Doctrine::getTable('CustomerProfile')->getConnection();
                $con->beginTransaction();
                $CustomerPrObj = $customer_profile;
                $CustomerPrObj->setName($params['customer_profile']['name']);
                $CustomerPrObj->setAddress($params['customer_profile']['address']);
                if (trim($params['customer_profile']['email_address']) != '') {
                    $CustomerPrObj->setEmailAddress(trim($params['customer_profile']['email_address']));
                } else {
                    $CustomerPrObj->setEmailAddress(NULL);
                }
                $CustomerPrObj->setSex($params['customer_profile']['sex']);
                $CustomerPrObj->setPhoneNumber($params['customer_profile']['phone_number']);
                $aValueCollection = $CustomerPrObj->getObject()->getAttributeValue();
                $errorOnDynAttributes = array();
//                if (!empty($aValueCollection)) {
                if (isset($params['customer_profile']['attrForm'])) {
                    foreach ($params['customer_profile']['attrForm'] as $atName => $attVal) {

                        $attVal = trim($attVal);

                        $found = false;
                        //Aditi- Edit /delete attribute value
                        foreach ($aValueCollection as $aValObj) {
                            try {
                                if ($aValObj->getAttributeMaster()->getAttName() == $atName) {
                                    $found = true;

                                    if ($attVal == '') {
                                        $aValObj->delete(); //Delete that attVal object if input is blank
                                    } else {
                                        $aValObj->setAttValue($attVal);
                                        $aValObj->save();
                                    }
                                }
                            } catch (Exception $e) {

                                echo "<pre>";
                                print_r($e->getMessage());
                            }
                        }
                        //If attribute not found in Attribute Value Table , insert a new object in Attribute Value Table
                        if (!$found && $attVal != '') {
                            foreach ($dynamicAttFieldName as $attMasterRecordNo => $attDetails) {
                                if ($attDetails['att_name'] == $atName) {
                                    $attValoBj = new AttributeValue();
                                    $attValoBj->setAttMasterId($attDetails['id']);
                                    $attValoBj->setAttValue($attVal);
                                    $attValoBj->setObject($CustomerPrObj->getObject());


                                    $attValoBj->save();
                                }
                            }
                        }
                    }
                }
                $CustomerPrObj->save();
                $con->commit();
                $this->getUser()->setFlash('notice', "Customer Edited Successfully");
            } catch (Exception $e) {
                $con->rollback();
            }
        } else {
//foreach ($this->form->getFormFieldSchema() as $name => $formField) {
//
//echo $formField->getName() . "-" . $formField->getError() . "<br>"; // renders unformatted
//}
//exit;


            if (array_key_exists('logedin_user_group', $_SESSION) && $_SESSION['logedin_user_group'] == 'merchant') {
                $this->admin_login = false;
                $this->addClassTo = '';
            } elseif (array_key_exists('logedin_user_group', $_SESSION) && $_SESSION['logedin_user_group'] == 'customer') {
                $this->admin_login = false;
                $this->addClassTo = 'viewProfiles';
            } elseif (!array_key_exists('logedin_user_group', $_SESSION)) {
                $this->admin_login = true;
            }
            $this->setTemplate('edit');
            return;
        }

        if (!array_key_exists('logedin_user_group', $_SESSION)) {
            $this->admin_login = true;

            $this->redirect('custMgt/index?merchant_id=' . cryptString::encrypt($customer_profile->getMerchantProfileId()));
        } else {

            if ($_SESSION['logedin_user_group'] == 'teller') {// COming from teller
                $this->redirect('@teller_login');
            } elseif ($_SESSION['logedin_user_group'] == 'merchant') {

                $this->addClassTo = '';
                $this->redirect('custMgt/index');
            }
        }
        if (pay4billHelper::checkIfCustomerUser($this->getUser()->getGuardUser()->getGroups()->toArray())) {

            $this->redirect('custMgt/viewProfiles');
        }
    }

    public function executeSendEmail(sfWebRequest $request) {
        //array(rl)
        $pin = NULL;
        $email = $request->getParameter('email_address');
        $customerName = $request->getParameter('customer_name');
        $merchantName = $request->getParameter('merchant_name');
        $customerId = $request->getParameter('customer_id');
        $image_tag = $request->getParameter('image_tag');
        if ($request->hasParameter('pin')) {
            $pin = $request->getParameter('pin');
        }


        $url = $request->getParameter('url');
        $subject = 'Welcome to pay4bill';
        /* Mail firing code starts */
        $mailTo = $email;
        $mailToGroup = array();

        $mailFrom = sfConfig::get('app_sender');
        $signature = sfConfig::get('app_sender');
        $partialName = $request->getParameter('partial_name');

        $partialVars = array('path' => $image_tag, 'pin' => $pin, 'customer_id' => $customerId, 'customerName' => $customerName, 'url' => $url, 'merchantName' => $merchantName);
        $mailingOptions = array('mailFrom' => $mailFrom, 'mailTo' => $mailTo, 'mailSubject' => $subject);

        $mailObj = new Mailing();
        $mailInfo = $mailObj->sendMail($partialName, $partialVars, $mailingOptions);

        return $this->renderText($mailInfo);
    }

    /**
     * @author:  Manu Datta
     * @param <type> $request
     * @purpose:    To Send the email
     */
    public function executeSendEmailExistCust(sfWebRequest $request) {
        //array(rl)
        $email = $request->getParameter('email');
        $customerName = $request->getParameter('customer_name');
        $merchantName = $request->getParameter('merchant_name');
        $complete_url = $request->getParameter('complete_url');
        $subject = 'Welcome to pay4bill';

        $partialName = 'sendExistCustEmail';

        /* Mail firing code starts */
        $mailTo = $email;
        $mailToGroup = array();

        $mailFrom = sfConfig::get('app_sender');
        $signature = sfConfig::get('app_sender');

        $partialVars = array('customerName' => $customerName, 'complete_url' => $complete_url, 'merchantName' => $merchantName);
        $mailingOptions = array('mailFrom' => $mailFrom, 'mailTo' => $mailTo, 'mailSubject' => $subject);

        $mailObj = new Mailing();
        $mailInfo = $mailObj->sendMail($partialName, $partialVars, $mailingOptions);

        return $this->renderText($mailInfo);
    }

    /**
     * @author:  Manu Datta
     * @param <type> $request
     * @purpose:    To Customer As Merchant
     */
    public function executeCustomerAsMerchant(sfWebRequest $request) {

        /* Unset below session varibale which have not been set Customer -> Payment History
         * Function Name:executeAllPaymentsForCustomer && executePartlyPaidForCustomer on
         * date: 31/05/2012
         */
        $_SESSION['com_name_payment_history'] = NULL;
        $_SESSION['invoice_no_payment_history'] = NULL;
        $_SESSION['search_par_payment_history'] = NULL;
        /* End of unset below session varibale which have not been set Customer -> Payment History */


        $param = cryptString::decrypt($request->getParameter('var'));
        $details = $request->getPostParameters();
        $_SESSION['is_logout'] = 1;
        $user_id = $this->getUser()->getGuardUser()->getId();
        $group_name = $this->getUser()->getGuardUser()->getGroupNames();
        if (in_array('merchant', $group_name)) {
            $this->allreadyMerchant = false;
        } else {
            $this->allreadyMerchant = true;
        };
        $customerInfo = Doctrine::getTable('CustomerProfile')->findOneByUserId($user_id, Doctrine::HYDRATE_ARRAY);

        if (isset($details['sf_guard_user']['Profile']['cac_registration'])) {
            $userGroupObj = new sfGuardUserGroup();
            $userGroupObj->setUserId($user_id);
            $userGroupObj->setGroupId(1);
            $userGroupObj->save();
            $merchantObj = new MerchantProfile();
            $merchantObj->setMerchantName($customerInfo['name']);
            $merchantObj->setMerchantId($user_id);
            $merchantObj->setCacRegistration($details['sf_guard_user']['Profile']['cac_registration']);
            $merchantObj->setMerchantContactAddress($customerInfo['address']);
            $merchantObj->setMerchantContactNumber($customerInfo['phone_number']);
            $merchantObj->setCompanyUrl($details['Company_URL']);
            $merchantObj->setTinyUrl(str_replace(" ", "-", strtolower($customerInfo['name']))); //
            $merchantObj->setApproved(1);
            $merchantObj->save();

            /*
             * to save the customer n merchant relation
             * 21 May 2012
             * @ Santosh Kumar
             */
            $merchantInfo = Doctrine::getTable('MerchantProfile')->findOneByMerchantId($user_id);
            $merchantInfo = Doctrine::getTable('MerchantProfile')->findOneByMerchantId($user_id);
            $userGroupObj = new MerchantCustomer();
            $userGroupObj->setMerchantId($merchantInfo->getId());
            $userGroupObj->setCustomerId($customerInfo['id']);
            $userGroupObj->save();

            $this->getUser()->setFlash('notice', 'Customer as a Merchant set successfully'); // Varun : Bug fix FS#34999
            $this->redirect('custMgt/customerAsMerchant');
        } else if ($param == 2) {
            $merchantInfo = Doctrine::getTable('MerchantProfile')->findOneByMerchantId($user_id);
            $GuardUserGroupObj = Doctrine::getTable('sfGuardUserGroup')->findByUserIdAndGroupId($user_id, 1);
            $customerObj = Doctrine::getTable('MerchantCustomer')->findByMerchantId($merchantInfo->getId(), Doctrine::HYDRATE_ARRAY);

            if (count($customerObj) == 0) {
                if (count($GuardUserGroupObj->toArray()) != 0) {
                    foreach ($GuardUserGroupObj as $GuardUserGroupObj) {
                        $GuardUserGroupObj->delete();
                    }
                }
                if (count($merchantInfo) != 0) {
                    $merchantInfo->delete();
                }
                $this->getUser()->setFlash('notice', 'Customer as a Merchant removed successfully');
                $this->redirect('custMgt/customerAsMerchant');
            } else {
                //$this->getUser()->setFlash('error', ' Merchant role can not be revoked as Customer are generated for you');
                $this->getUser()->setFlash('error', ' Customer cannot be unset as merchant because customer(s) are affiliated by merchant role.');
                $this->redirect('custMgt/customerAsMerchant');
            }
        }
    }

    //Varun : new function created fot merchant details
    public function executeMerchantDetails(sfWebRequest $request) {
        $this->form = new MerchantDetailsForm(null, array('valid_cac' => 0));
    }

    /* Aditi
     * To create array of merchant profile id  teller is mapped to
     */

    private function createArrayOfMerchantIds($tellerMerchantMappingObjs) {
        $mid_array = array();
        foreach ($tellerMerchantMappingObjs as $obj) {
            array_push($mid_array, $obj->getMerchantProfileId());
        }
        return $mid_array;
    }

    /* Aditi
     * To create array of Dynamic attributes values search
     * to display on the template "-" for a value which doesn't match
     */

    private function returnDynamicAttributesValuesArray($customerProfileObjs, $dynamic_attributes_array) {
        $searchValueForAttributeExist = false;
        if (count($dynamic_attributes_array) > 0) {

            foreach ($dynamic_attributes_array as $attSearchVal) {
                if (trim($attSearchVal) != '') {
                    $searchValueForAttributeExist = true;
                    break;
                }
            }
        }
        $array_to_return = array();
        if ($searchValueForAttributeExist == true) {
            foreach ($customerProfileObjs as $custPro) {
                foreach ($custPro->getObject()->getAttributeValue() as $aValObj) {

                    foreach ($dynamic_attributes_array as $attName => $attSearchVal) {
                        /** Bug ##35652- Aditi--Following 2 lines added to prevent search if only space enterd in search text box */
                        if (trim($attSearchVal) == '')
                            continue;
                        if ($attName == $aValObj->getAttributeMaster()->getAttName()) {
                            $array_to_return[$custPro->getId()][$attName] = $aValObj->getAttValue();
                        }
                    }
                }
            }

            foreach ($array_to_return as $custId => $dbValues) {
                foreach ($dynamic_attributes_array as $attName => $searchVal) {
                    if (!array_key_exists($attName, $dbValues) && trim($searchVal) != '') {

                        $array_to_return[$custId][$attName] = "-";
                    }
                }
            }
        }

        return $array_to_return;
    }

    /* Aditi
     * Customer Registration Process-first Step- Customer Id Authentication- 16 Aug 2012
     */

    public function executeAddProfile(sfWebRequest $request) {
        //echo cryptString::decrypt('MVExWTFtMVJjMVQxUDFZ');
        $this->getLogginUser = $this->getUser()->getGuardUser();
        if (isset($this->getLogginUser) && !empty($this->getLogginUser)) {
            $this->setLayout('permissionsLayout');
            $this->addClassTo = $this->getActionName();
        }


        $this->form = $form = new CustomerIdAuthenticationForm();

        if ($request->isMethod('post')) {
            $form_array = $request->getParameter($form->getName());

            $form->bind($form_array);
            if ($form->isValid()) {
                $this->getUser()->setAttribute('customerId', $form->getFormFieldSchema()->offsetGet('customerId')->getValue());
                if (isset($this->getLogginUser) && !empty($this->getLogginUser)) {
                    if (pay4billHelper::checkIfMerchantUser($this->getUser()->getGuardUser()->getGroups()->toArray())) {
                        $this->redirect('custMgt/addProfileForMerchant');
                    } elseif (pay4billHelper::checkIfCustomerUser($this->getUser()->getGuardUser()->getGroups()->toArray())) {
                        $ob = new UserCustomerMapping();
                        $ob->setSfGuardUser($this->getUser()->getGuardUser());
                        $ob->setCustomerProfile(Doctrine::getTable("CustomerProfile")->findByCustomerId($form_array['customerId'])->getFirst());
                        $ob->save();
                        $this->getUser()->setFlash('notice', "Profile Added successfully", true);
                        $this->redirect('custMgt/viewProfiles');
                    }
                }
                $this->redirect('custMgt/setUsernamePassword');
            } else {

                if ($form->getFormFieldSchema()->offsetGet('activeMerchants')->getValue() != '') {
                    $this->merchantType = Doctrine::getTable('MerchantProfile')->find($form->getFormFieldSchema()->offsetGet('activeMerchants')->getValue())->getPaymentType();
                }
                $err = 0;
                foreach ($form->getFormFieldSchema() as $name => $formField) {

                    if ($formField->hasError()) {
                        $err++;
                    }
                }

                if ($err == 0) {

                    $this->getUser()->setFlash('error', $form->getErrorSchema()->offsetGet($form->getName())->getMessage());
                }
            }
        }
    }

    /* Aditi
     * Sending Mail at  User Creation - 22 Aug 2012
     */

    public function executeSendUserCreationEmail(sfWebRequest $request) {


        $userObj = Doctrine::getTable('sfGuardUser')->findByUsername($request->getParameter('username'))->getFirst();
        $ucObj = Doctrine::getTable('UserCustomerMapping')->findByUserId($userObj->getId());
        $customerIdArray = array();
        foreach ($ucObj as $ob) {
            array_push($customerIdArray, $ob->getCustomerProfile()->getCustomerId());
        }
        $image_tag = $request->getParameter('image_tag');
        $url = $request->getParameter('url');
        $subject = 'Welcome to pay4bill';
        /* Mail firing code starts */
        $mailTo = $userObj->getEmailAddress();
        $mailToGroup = array();

        $mailFrom = sfConfig::get('app_sender');
        $signature = sfConfig::get('app_sender');
        $partialName = 'userCreationEmail';
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));

        $partialVars = array('path' => $image_tag, 'username' => $request->getParameter('username'), 'password' => $request->getParameter('password'), 'customer_id' => $customerIdArray, 'url' => $url);
        $mailingOptions = array('mailFrom' => $mailFrom, 'mailTo' => $mailTo, 'mailSubject' => $subject);

        $mailObj = new Mailing();
        $mailInfo = $mailObj->sendMail($partialName, $partialVars, $mailingOptions);

        return $this->renderText($mailInfo);
    }

    /* Saurabh
     * Add more profile(Merchant Login) when logged in with merchant - 21 Aug 2012
     */

    public function executeAddProfileForMerchant(sfWebRequest $request) {

        //Check if this user id is already mapped as a customer id
        $user_id = $sfGuardUserId = $this->getUser()->getGuardUser()->getId();
        $arr = Doctrine::getTable('sfGuardUserGroup')->findBy('user_id', $user_id)->toArray();
        $customerGroup = 0;
        foreach ($arr as $k => $v) {
            if ($v['group_id'] == '2') {
                $customerGroup = 1;
            }
        }
        if ($customerGroup == 0) {
            $obj = new sfGuardUserGroup();
            $obj->user_id = $user_id;
            $obj->group_id = 2;
            $obj->save();
        }

        if ($this->getUser()->hasAttribute('customerId')) {
            $customer_id = $this->getUser()->getAttribute('customerId');
            $this->getUser()->getAttributeHolder()->remove('customerId');
            $cob = Doctrine::getTable('CustomerProfile')->findByCustomerId($customer_id)->getFirst();
            $ob = new UserCustomerMapping();
            $ob->setUserId($user_id);
            $ob->setCustomerProfile($cob);
            $ob->save();
            $this->getUser()->setFlash('notice', 'You have successfully added profile into your account.');
            $this->redirect('custMgt/addProfile');
        }
    }

    /* Aditi
     * To display Validation Number/Pin text Box- 16 Aug 2012
     */

    public function executeGetTextBoxOnMerchantSelection(sfWebRequest $request) {
        return $this->renderText(Doctrine::getTable('MerchantProfile')->find($request->getParameter('merchant_profile_id'))->getPaymentType());
    }

    /* Aditi
     * To set username and Password after user has entered valid Customer Id, and
     *  Pin/Validation Number combination in Customer Registration   - 17 Aug 2012
     */

    public function executeSetUsernamePassword(sfWebRequest $request) {
        $this->userCreated = false;
        $customerId = $this->getUser()->hasAttribute('customerId') ? $this->getUser()->getAttribute('customerId') : $request->getParameter('SetUserNamePassword[customerId]');
        $this->form = $form = new SetLoginCredentialsForm(array(), array('customerId' => $customerId));
        if ($request->isMethod('post')) {
            $this->getUser()->getAttributeHolder()->remove('customerId');
            $form_array = $request->getParameter($form->getName());
            $form->bind($form_array);
            if ($form->isValid()) {
                $form->updateUserDetails($form_array);
                $this->userCreated = true;
                sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
                $this->getUser()->setFlash('notice', "Your Login credential has been created successfully. Click <a href=" . url_for('@homepage') . ">here</a> to login", false);

                $host = $request->getUriPrefix();
                $url_root = $request->getPathInfoPrefix();
                $complete_url = $host . $url_root;
                $taskId = EpjobsContext::getInstance()->addJob('User Creation Notification', $this->moduleName . "/sendUserCreationEmail", array('username' => $form_array['username'], 'password' => $form_array['password'], 'url' => $complete_url, 'image_tag' => public_path(sfConfig::get('app_logo_path'), true)), '-1', 'frontend');

                $this->logMessage("scheduled mail job with id: $taskId", 'debug');
            } else {


                $err = 0;
                foreach ($form->getFormFieldSchema() as $name => $formField) {

                    if ($formField->hasError()) {
                        $err++;
                    }
                }

                if ($err == 0) {
                    $this->getUser()->setFlash('error', $form->getErrorSchema()->offsetGet($form->getName())->getMessage());

                    return;
                }
            }
        } else {
            $this->getUser()->setFlash('notice', "Please enter Username and Password", false);
        }
    }

    /* Aditi
     * To display all Customer Profiles linked to a user- 21 Aug 2012
     */

    public function executeViewProfiles(sfWebRequest $request) {

        $helperObj = new pay4billHelper();
        $parameters = $helperObj->trimArray($request->getParameterHolder()->getAll());
        $this->customer_id = $customerId = array_key_exists('customer_id', $parameters) ? $parameters['customer_id'] : '';
        $this->merchant_name = $merchantName = array_key_exists('merchant_name', $parameters) ? $parameters['merchant_name'] : '';
//        $loggedInUserObj=$this->getUser()->getGuardUser();
        if (pay4billHelper::checkIfCustomerUser($this->getUser()->getGuardUser()->getGroups()->toArray())) {

            $this->customer_profiles = Doctrine::getTable('CustomerProfile')->getCustomerProfilesOfLoggedInUser($this->getUser()->getGuardUser()->getId(), $customerId, $merchantName);

            $total_no_of_records = count($this->customer_profiles);

            if ($total_no_of_records > sfConfig::get('app_records_per_page')) {
                $this->actionTobeCalled = $this->getActionName();
                $this->total_no_of_records = $total_no_of_records;
                $this->no_of_records_per_page = sfConfig::get('app_records_per_page');
                $this->setPagination = true;

                if (!$request->hasParameter('offset')) {
                    $offset = 0;
                } else {

                    $this->getUser()->getAttributeHolder()->remove('msg');
                    $offset = $request->getParameter('offset');
                }
                $this->current_offset = $offset;

                $this->customer_profiles = Doctrine::getTable('CustomerProfile')->getCustomerProfilesOfLoggedInUser($this->getUser()->getGuardUser()->getId(), $customerId, $merchantName, $offset, sfConfig::get('app_records_per_page'));


                $this->module = $this->getModuleName();
                $this->offset_last = (($total_no_of_records % sfConfig::get('app_records_per_page') == 0 ? ($total_no_of_records - sfConfig::get('app_records_per_page') ) : $total_no_of_records - ($total_no_of_records % sfConfig::get('app_records_per_page'))));

                $this->offset_next = (($offset + sfConfig::get('app_records_per_page')) > $this->offset_last ? $this->offset_last : ($offset + sfConfig::get('app_records_per_page')));

                $this->offset_prev = (($offset - sfConfig::get('app_records_per_page')) < 0 ? 0 : $offset - sfConfig::get('app_records_per_page'));
            } else {
                $this->setPagination = false;
            }
        }
        $this->addClassTo = $this->getActionName();
    }

}
