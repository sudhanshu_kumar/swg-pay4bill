<script language="javascript">
    $(document).ready(function(){
<?php if ($to_be_printed) { ?>
            window.print();

<?php } ?>




    }
);
    function isatLeastOneInvoiceSelected()
    {
        var return_var=false;
        $('.invoice_chkBox').each(function(index,element) {
            //alert($);
            if($(this).attr('checked'))
            {
                return_var=true;
            }

        });
        if(!return_var){
            alert('Please select at least one Invoice to Print');
//            $('#error_form').html('<i>Please select at least one Invoice to Print</i>');
//            $('#error_form').css('display','inline');
            return false;
        }
        else{
            $('#error_form').html('');
            $('#error_form').css('display','none');
        }
        //              alert(return_var);
        return return_var;
        //        return false;
    }
</script>
<?php $to_be_printed = true;?>
<div id="pgMenu">
    <div id="wrappernew">
<?php
             if(!empty($logo)){
            if (file_exists(sfConfig::get('sf_upload_dir') . '/merchant_logo/' . $logo)) { ?>
            <div class="noprint" style="margin-left: 600px; margin-bottom: 25px;"> <img src="<?php echo _compute_public_path($logo, 'uploads/merchant_logo', '', true); ?>" style="height: 75px; width: 75px;" alt="Merchant Logo" class="" /> </div>
                    <?php } }?>
        <div class="pgTitle">

            <h2>Invoices</h2></div>
    </div>

</div>



<div id="wrappernew">
    <div id="content">
        <div id="Formwraper">


            <div style="float:right"><input type="button" class="blueBtn" value="Back" name="btnBack" onclick="history.go(-1)"></div>
<?php if (!$to_be_printed) {
?>

            <form action="<?php echo url_for('report/printSelectedInvoices') ?>" id ="print_form" onsubmit="return isatLeastOneInvoiceSelected()">
                <input type="hidden" name="pgTitle" value="<?php echo $pgTitle; ?>">
<?php } ?>

                <div class="TableForm">

                    <table border="0" cellspacing="0" cellpadding="0">
                        <thead>
<?php if (!$to_be_printed) {
?> <th></th><?php } ?>
                        <th>Invoice Number</th>
                        <th>Customer Name</th>
                        <th>Amount(N)</th>
                        <th>Issue Date</th>
                        <th>Delivery Date</th>
                        <th>Due Date</th>
                        <th>Status</th>
                        </thead>

                        <tbody>
<?php
                        $total_amount_to_be_displayed = 0;
                        if(count($invoices) > 0 ){
                        foreach ($invoices as $invoice) {
?>
                            <tr>
<?php if (!$to_be_printed) {
?>  <td></td><?php } ?>
                                <td><?php echo $invoice->getInvoiceNo(); ?></td>
                                <td><?php echo $invoice->Customer->getName(); ?></td>
                                <td align=""><?php
                                switch ($invoice->getStatus()) {
                                        case 0: $amt = $invoice->getAmountDue(); break;
                                        case 2: $amt = $invoice->getAmountPaid(); break;
                                }
                                
                                echo $amt;
                                $total_amount_to_be_displayed = $total_amount_to_be_displayed + $amt;
//                                foreach ($item_details as $ob) {
//                                    if ($ob->getInvoiceId() == $id_Items_invoiceId[$invoice->getId()]) {
//                                        echo number_format(($ob->getPartialSum() + $ob->getTax()), 2, ".", "");
//                                        $total_amount_to_be_displayed+=$ob->getPartialSum() + $ob->getTax();
//                                    }
//                                }
?></td>
                                <td><?php echo $invoice->getIssueDate(); ?></td>
                                <td><?php echo $invoice->getDeliveryDate(); ?></td>
                                <td><?php echo $invoice->getDueDate(); ?></td>
                                <td><?php
                                    switch ($invoice->getStatus()) {
                                        case 0:
//                                            if (strtotime($invoice->getDueDate()) >= strtotime(date("Y-m-d"))) {
                                                echo image_tag('pending.png', array('alt' => "Pending", 'title' => "Pending"));
//                                            } else {
//                                                echo image_tag('expired.png', array('alt' => "Expired", 'title' => "Expired"));
//                                            }
                                            break;
//                                        case 1:
//                                            echo image_tag('cancel.png', array('alt' => "Cancelled", 'title' => "Cancelled"));
//                                            break;
                                        case 2;
                                            echo image_tag('paid.png', array('alt' => "Paid", 'title' => "Paid"));

                                            break;
//                                        case 3:
//
//                                            if (strtotime($invoice->getDueDate()) > time()) {
//                                                echo image_tag('partially-paid.png', array('alt' => "Partially Paid", 'title' => "Partially Paid"));
//                                            } else {
//                                                echo image_tag('expired.png', array('alt' => "Expired", 'title' => "Expired"));
//                                            }
//
//                                            break;
                                    }
?></td>
                            </tr>



<?php } ?>
                            <tr class="border-bottom"><?php if (!$to_be_printed) { ?><td></td><?php } ?>
                                <td></td>
                                <td align="right"><b>Amount:</b></td>
                                <td align="" ><b><?php echo number_format($total_amount_to_be_displayed, 2, '.', '');
                                ; ?></b><td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <?php } else { ?>
                            <tr>
                                <td colspan="8" align="center">No Invoice Found.</td>
                            </tr>
                            <?php } ?>
                            </tbody>
                        </table>



<?php if (!$to_be_printed) { ?> </div>
<div class="clearfix btn-row" align="center"><?php if (!$to_be_printed) { ?>
                            <input type="submit" value="Print" align="center">
                            <input type="button" value="Close" onClick="window.close()" align="center">
                            </div><div id="error_form" class="error" style="display:none"></div>
                            <?php } ?>

            </form>
                        <?php } ?>
        </div>
    </div></div>
