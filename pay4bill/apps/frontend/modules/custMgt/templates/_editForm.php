<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>

<p>
<form action="<?php echo url_for('custMgt/saveEditedCustomer?id=' . cryptString::encrypt($form->getObject()->getId())) ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
    <?php if (!$form->getObject()->isNew()): ?>
        <input type="hidden" name="sf_method" value="put" />
    <?php endif; ?>

        <div id="Formwraper">
            <div>
                <h3>Edit Customer's Information</h3>
            </div>
            <div class="TableForm">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="35%" align="right"><?php echo $form['name']->renderLabel(); ?></td>
                        <td><?php echo $form['name']->render(); ?> <?php echo $form['name']->renderError(); ?></td>
                    </tr>
                    <tr>
                        <td width="35%" align="right"><?php echo $form['sex']->renderLabel(); ?></td>
                        <td><?php echo $form['sex']->render(); ?> <?php echo $form['sex']->renderError(); ?></td>
                    </tr>
                    <tr>
                        <td width="35%" align="right"><?php echo $form['address']->renderLabel(); ?></td>
                        <td><?php echo $form['address']->render(); ?> <?php echo $form['address']->renderError(); ?></td>
                    </tr>
                    <tr>
                        <td width="35%" align="right"><?php echo $form['phone_number']->renderLabel(); ?></td>
                        <td><?php echo $form['phone_number']->render(); ?> <?php echo $form['phone_number']->renderError(); ?></td>
                    </tr>
                    <tr>
                        <td width="35%" align="right"><?php echo $form['email_address']->renderLabel(); ?></td>
                        <td><?php echo $form['email_address']->render(); ?><?php echo $form['email_address']->renderError(); ?></td>
                    </tr>
                <?php
                if (count($attr) > 0) {
                    foreach ($attr as $name => $value) {
                ?> <tr>
                        <tr>
                            <td width="35%" align="right"><?php echo $form['attrForm'][$name]->renderLabel(); ?>

                            <td><?php echo $form['attrForm'][$name]->render(); ?>
<?php echo $form['attrForm'][$name]->renderError(); ?>

                            </td>
                        </tr>   </tr>
<?php
                    }
                }
?>
                <tr>
                    <td width="35%" align="right"></td>
                    <td><input type="submit" name="Save" class="blueBtn" value="Save"/>
                        <input type="button" name="cancel" id="cancel_button"  class="blueBtn" value="Cancel" onclick="javascript:history.go(-1)"/></td>
                </tr>

            </table>
        </div>

        <?php echo $form['_csrf_token']; ?>
    </div>
</form>
<p>&nbsp;</p>



