<script type="text/javascript">

    function validateSearch()
    {
       
        return true;
    }

    function populateDynamicAttributes(val)
    {

        var url="<?php echo url_for('custMgt/getMerchantSpecificAttributesForSearch'); ?>";
        if(val!=""){
            $.post(url, {merchant_id:val}, function(data){

                $('#merchantSpecificAttributes').html(data);
<?php
if ($searchCriterias->offsetExists('dynamicAttr')) {
    foreach ($searchCriterias->getRaw('dynamicAttr') as $attName => $attVal) {
?>

                        $('#<?php echo $attName; ?>_id').val('<?php echo $attVal; ?>');
<?php }
} ?>

            });
        }
        else
        {
            $('#merchantSpecificAttributes').html("");
        }
    }

    $(document).ready(function(){
<?php
if (count($searchCriterias) > 0) {
    if ($searchCriterias['merchant_id'] != "") {
?>populateDynamicAttributes('<?php echo $searchCriterias['merchant_id']; ?>');
<?php }
} ?>
        //                 function resetForm(){
        //                     $('#email_address').val('');
        //                     $('#cust_name').val('');
        //                     $('#email_address1').hide();
        //                     $('#customer_name').hide();
        //                     $('#search_par').val('');
        //                 }
        function validateSearch1()
        {
            //        var err  = 0;
            //        var email_address = jQuery.trim($('#email_address').val());
            //        var cust_name = jQuery.trim($('#cust_name').val());
            //
            //
            //        var reg = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i; //Email validation
            //        var regcheck = /^[a-zA-Z0-9 .&]*$/; //Invoice Number  and Customer Name validation
            //
            //        $('#email_address_error').html("");
            //        $('#cust_name_error').html("");
            //
            //        if(email_address!='')
            //        {
            //            if(reg.test(email_address) == false) {
            //                $('#email_address_error').html("<font color='red'>Please enter correct Email Address.</font>");
            //                $('#email_address').focus();
            //                err = err + 1;
            //            }
            //        } else {
            //            if($('#email_address').is(':visible')){
            //                $('#email_address_error').html("<font color='red'>Please enter Email Address.</font>");
            //                $('#email_address').focus();
            //                err = err + 1;
            //            }
            //        }
            //        if(cust_name!='')
            //        {
            //            if(cust_name.length >50 ) {
            //                $('#cust_name_error').html("<font color='red'>Customer Name cannot be more than 50 characters.</font>");
            //                $('#cust_name').focus();
            //                err = err + 1;
            //            } else {
            //                $('#cust_name_error').html("");
            //            }
            //            if(regcheck.test(cust_name) == false ) {
            //                $('#cust_name_error').html("<font color='red'>Please enter valid Customer Name.</font>");
            //                $('#cust_name').focus();
            //                err = err + 1;
            //            } else {
            //                $('#cust_name_error').html("");
            //            }
            //        }
            //        else {
            //            if($('#cust_name').is(':visible')){
            //                $('#cust_name_error').html("<font color='red'>Please enter Customer Name.</font>");
            //                $('#cust_name').focus();
            //                err = err + 1;
            //            }
            //        }
            //        if(err>0)
            //        {
            //            return false;
            //        }
            return true;



        }


        $('#Customers').addClass('active');


    }
);
</script>


<?php
include_partial('subHeaderForTeller', array('addClassTo' => 'index', 'adminLogin' => false));
?>
<?php if (!empty($msg)) {
?><div class="clear_new"></div> <?php } ?>

<div id="mainnew">
    <!-- Varun : Flash messaged added -->
    <?php
    $sf = sfContext::getInstance()->getUser();
    if ($sf->hasFlash('notice')) {
    ?><div id="flash_notice" class="msg_serverside" ><?php
        echo nl2br($sf->getFlash('notice'));
    ?></div><?php } ?>
    <div id="wrappernew">
        <div id="content">
            <div id="Formwraper">

                <h3>Search Customer</h3>
                <div class="TableForm" id="search">
                    <form action="<?php echo url_for('custMgt/searchForTeller'); ?>" method="post" id="userForm" onsubmit="return validateSearch()" >
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="17.5%" valign="top"><?php echo "Merchant Selection"; ?> </td>
                                <td  width="35%" valign="top">
                                    <select name="merchant_id" id="merchant_id" onchange="return populateDynamicAttributes(this.value);">

                                        <?php
                                        if ($tellerMerchantMappingObjs->count() > 0) {
                                            echo "<option value=''>All Merchants</option>";
                                        } else {
                                            echo "<option value=''>No Merchant Associated</option>";
                                        }
                                        ?>

                                        <?php
                                        foreach ($tellerMerchantMappingObjs as $tellerMerchantMappingObj) {
                                        ?> <option value="<?php echo cryptString::encrypt($tellerMerchantMappingObj->getMerchantProfileId()); ?>" <?php
                                            if (isset($searchCriterias['merchant_id']) && cryptString::decrypt($searchCriterias['merchant_id']) == $tellerMerchantMappingObj->getMerchantProfileId()) {
                                                echo 'selected';
                                            }
                                        ?>><?php echo $tellerMerchantMappingObj->getMerchantProfile()->getMerchantName(); ?></option>

                                        <?php } ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="17.5%" valign="top"><?php echo "Name" ?> </td>
                                    <td  width="35%" valign="top">
                                        <input class="FormInput9" type="textbox" name="name" value="<?php
                                            echo $searchCriterias['name'];
                                        ?>">
                                 </td>
                             </tr>
                             <tr>
                                 <td width="17.5%" valign="top"><?php echo "Customer Id" ?> </td>
                                 <td  width="35%" valign="top">
                                     <input class="FormInput9" type="textbox" name="cust_id" value="<?php
                                            echo $searchCriterias['cust_id'];
                                        ?>">
                                 </td>
                             </tr>

    <!--                             <tr>
                                     <td width="17.5%" valign="top"><?php //echo "Email Address"     ?> </td>
                                     <td  width="35%" valign="top">
                                         <input class="FormInput9" type="textbox" id="email_address" name="email_address" value="<?php
//                                            if (isset($searchCriterias)) {
//                                                echo $searchCriterias['email_address'];
//                                            }
                                        ?>">
                                                                                                     <div id="email_address_error"></div>
                                                                                                 </td>
                                                                                             </tr>-->
                                            <tr>
                                                <td width="17.5%" valign="top"><?php echo "Phone Number" ?> </td>
                                                <td  width="35%" valign="top">
                                                    <input class="FormInput9" type="textbox" name="phone_number" value="<?php
                                            echo $searchCriterias['phone_number'];
                                        ?>">
                                 </td>
                             </tr>
                             <tr class="">
                                 <td colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="0" id="merchantSpecificAttributes">
                                         <tbody></tbody>
                                     </table>
                                 </td>
                             </tr>
                             <tr id="merchantSpecificAttributes">
                                 <td colspan="3"  valign="top">
                                     <div class="Errormrg"><input type="submit" value="Search" class="blueBtn" >
                                         <input type="button" class="blueBtn" value="Reset" onClick="window.location='<?php echo url_for('custMgt/listingForTeller'); ?>'"/>
                                     </div>
                                 </td>
                             </tr>
                         </table>

                     </form>
                 </div>

                 <h3> Customers</h3>
                 <div id="if_error" class="msg_serverside"></div>
                 <div style="clear:both"></div>
                 <div class="TableForm">
                     <table cellspacing="0" cellpadding="0" border="0">
                         <thead>
                         <th>Name</th>
                         <th>Customer Id</th>
                         <th>Merchant Name </th>
 <!--                         <th>Email Address</th>-->

                         <th>Phone Number</th>
                        <?php
                                            if (count($customer_profiles) > 0 && $searchCriterias->count() > 0 && $searchCriterias->offsetExists('dynamicAttr')) {
                                                foreach ($searchCriterias['dynamicAttr'] as $colName => $colValue) {
                                                    if (trim($colValue) != "") {
                        ?><th><?php echo $attrNameLabelCombination[$colName]; ?></th>
                        <?php
                                                    }
                                                }
                                            }
                        ?>
                                            <th>Action</th>
                                            </thead>
                                            <tbody>
                            <?php
                                            if (count($customer_profiles) > 0) {
                            ?>
                            <?php
                                                $i = 0;
                                                foreach ($customer_profiles as $customer_profile): $arrUserEmail = '';
                                                    $i++;
                            ?>
                            <?php if ($i % 2 != 0) {
                            ?>
                                                        <tr class="odd">
                                <?php } else {
                                ?><tr class=""> <?php } ?>
                                                    <td><?php echo $customer_profile->getName() ?></td>
                                                    <td><?php echo $customer_profile->getCustomerId(); ?></td>
                                                    <td><?php echo $customer_profile->get('mName'); ?></td>
                    <!--                                                    <td><?php //echo wordwrap($customer_profile->getEmailAddress(), 24, "<br />\n", TRUE);     ?></td>-->

                                                    <td><?php echo wordwrap($customer_profile->getPhoneNumber(), 11, "<br />\n", TRUE); ?></td>
                                <?php
                                                    if ($searchCriterias->count() > 0 && $searchCriterias->offsetExists('dynamicAttr')) {

                                                        foreach ($searchCriterias['dynamicAttr'] as $colname => $colValue) {

                                                            if (trim($colValue) != "") {
                                ?><td><?php echo $dynamicAttr_value[$customer_profile->getId()][$colname]; ?></td>
                                <?php
                                                            }
                                                        }
                                                    }
                                ?>
                                                    <td>
                                    <?php //echo link_to(image_tag('invoice.png'), 'custMgt/showinvoice?id=' . cryptString::encrypt($customer_profile->getId()), array('title' => 'View Invoices', 'alt' => 'View Invoices'));  ?>&nbsp;&nbsp;&nbsp;&nbsp;
                                    <?php echo link_to(image_tag('invoice.png'), 'invoice/index?id=' . cryptString::encrypt($customer_profile->getId()), array('title' => 'View Invoices', 'alt' => 'View Invoices')); ?>&nbsp;&nbsp;&nbsp;&nbsp;

                                    <?php
                                                    echo link_to(image_tag('detail.png'), 'custMgt/showcustomer?id=' . cryptString::encrypt($customer_profile->getId()), array('title' => 'View Detail', 'alt' => 'View Detail',
                                                        'popup' => array('Windowtitle', 'resizeable=yes,scrollbars=yes,location=0,menubar,top=140,left=200,width=920,height=700')
                                                    ));
                                    ?>


                                    <?php
                                                    //echo "<pre>"; print_r($customer_profile->getMerchantCustomer()->getFirst()->getIsActive());
                                                    //Varun : Id to be sent encrypted.
                                                    echo link_to(image_tag('tdEdit.png'), 'custMgt/edit?id=' . cryptString::encrypt($customer_profile->getId()), array('title' => 'Edit Customer', 'alt' => 'Edit Customer'));
                                    ?>&nbsp;&nbsp;&nbsp;&nbsp;

                                                </td>

                                            </tr>
                            <?php endforeach; ?>

                            <?php
                                                } else {
                                                    echo "<tr><td colspan='7' align='center'>No Customers Found</td></tr>";
                                                }
                            ?>
                                            </tbody>
                                        </table>

                                    </div>
                                    <div class="nofoList">
                    <?php
                                                include_partial('global/records_number', array('amount_footer' => false, 'current_offset' => isset($current_offset) ? $current_offset : null, 'offset_next' => isset($offset_next) ? $offset_next : null, 'total_no_of_records' => isset($total_no_of_records) ? $total_no_of_records : count($customer_profiles), 'no_of_records_per_page' => isset($no_of_records_per_page) ? $no_of_records_per_page : null, 'from' => '', 'total_amount_of_all_transactions' => isset($total_amount_of_all_transactions) ? $total_amount_of_all_transactions : null));
                    ?></div>
                                            <div align="right">
                    <?php
                                                if ($setPagination) {

//                                                    echo "<pre>";print_r($searchCriterias);die;

                                                    $array_to_be_included_in_pagination = array('moduleName' => $module, 'amount_footer' => false, 'actionName' => $actionTobeCalled, 'phone_number' => $searchCriterias['phone_number'], 'merchant_id' => isset($searchCriterias['merchant_id']) ? $searchCriterias['merchant_id'] : '', 'name' => $searchCriterias['name'], 'email_address' => $searchCriterias['email_address'], 'current_offset' => $current_offset, 'offset_next' => $offset_next, 'offset_prev' => $offset_prev, 'offset_last' => $offset_last, 'search' => '', 'from' => '', 'customer_id' => $searchCriterias['cust_id']);
                                                    if (count($customer_profiles) > 0 && $searchCriterias->count() > 0 && $searchCriterias->offsetExists('dynamicAttr')) {
                                                        foreach ($searchCriterias['dynamicAttr'] as $colName => $colValue) {
                                                            if (trim($colValue) != "") {

                                                                $array_to_be_included_in_pagination['dynamicAttr'][$colName] = trim($colValue);
                                                            }
                                                        }
                                                    }
                                                    $helperObj = new pay4billHelper();
                                                    $array_to_be_included_in_pagination = $helperObj->trimArray($array_to_be_included_in_pagination);

//                                                    echo "<pre>";print_r($array_to_be_included_in_pagination);die;
                                                    include_partial('global/pagination', $array_to_be_included_in_pagination);
                                                }
                    ?></div>

            </div>
        </div>
    </div>
</div>
