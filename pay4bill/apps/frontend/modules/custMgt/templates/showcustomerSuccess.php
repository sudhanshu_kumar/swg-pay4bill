<script>
    function callPrint(){
        $(':button').hide();
        window.print();
        window.close();
    }
</script>
<div id="pgMenu" style="margin-top:50px">
    <div id="wrappernew">
        <?php
        if (!empty($logo)) {
            if (file_exists(sfConfig::get('sf_upload_dir') . '/merchant_logo/' . $logo)) {
 ?>
                <div class="noprint" style="margin-left: 600px; margin-bottom: 25px;"> <img src="<?php echo _compute_public_path($logo, 'uploads/merchant_logo', '', true); ?>" style="height: 75px; width: 75px;" alt="Merchant Logo" class="" /> </div>
<?php }
        } ?>
        <div class="pgTitle">

            <h2>Customer Details <?php echo '- ' . $form[0]['name']; ?></h2><br></div>

    </div>
</div>
<div id="Formwraper">
    <div class="TableForm">
        <div>&nbsp;</div>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td align="left" width="50%"><b>Customer Id</b></td>
                <td>&nbsp;<?php echo $form[0]['customer_id'] ?></td>
            </tr>
            <tr>
                <td colspan="2" width="100%"><br><i> Your profile is registered for merchant - '<b><?php echo $form[0]['merchant_name'] ?></b></i>'<br><br> The profile details are as follows:<br><br></td>
            </tr>
        </table>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">

            <tr>
                <td width="50%" align="left"><b>Name</b></td>
                <td>&nbsp;<?php echo $form[0]['name'] ?></td>
            </tr>
            <tr>
                <td align="left"><b>Sex</b></td>
                <td>&nbsp;<?php echo $form[0]['sex'] ?></td>
            </tr>
            <tr>
                <td align="left"><b>Address</b></td>
                <td>&nbsp;<?php echo $form[0]['address'] ?></td>
            </tr>
            <tr>
                <td align="left"><b>Email Address</b></td>
                <td>&nbsp;<?php echo $form[0]['email_address'] ?></td>
            </tr>
            <tr>
                <td align="left"><b>Phone Number</b></td>
                <td>&nbsp;<?php echo $form[0]['phone_number'] ?></td>
            </tr>
<?php foreach ($form[0]['Object']['AttributeValue'] as $key => $value) { ?>
            <tr>
                <td align="left"><b><?php echo $value['AttributeMaster']['att_label'] ?></b></td>
                <td>&nbsp;<?php if ($value['att_value'] != '')
                echo $value['att_value']; else
                echo 'N/A'; ?></td>
            </tr>
<?php } ?>

        </table>
        <div style="height:30px">&nbsp;</div>
        <div>
<!--                <div style="float:left;border:0px solid red;padding-top:10px"><b><i>Please contact the bank teller for any change in your profile.</i></b></div>-->
            <div style="float:right;border:0px solid red;width:57%">
                <input type="button" value="Print" align="center"  class="blueBtn" onclick="callPrint();">
                <input type="button" value="Close" onClick="window.close()" align="center" class="blueBtn">
            </div>
        </div>
        <div style="height:30px">&nbsp;</div>

    </div>
</div>

