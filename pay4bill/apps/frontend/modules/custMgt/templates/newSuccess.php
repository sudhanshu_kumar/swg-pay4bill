<script>
    $(document).ready(function(){
        $('#Customers').addClass('active');
        return false; // Added so that client side validation stops @saurabh 13june 2012

        $('#Customers').addClass('active');
        
        $('form').attr('id','division_frm');

        $.validator.addMethod("valid_name", function(value, element)
        {
            return this.optional(element) || /^[a-zA-Z ]*$/.test(value);
        }, "Please enter valid name");
        $.validator.addMethod("pwd_minLength", function(value, element)
        {
            $('.error_list').html('');
            return value.length>=6;
        }, "Minimum 6 characters");
        $.validator.addMethod("chk_passwords", function(value, element)
        {
            //            alert($('[name="password1"]').val());
            $('.error_list').html('');
            return $('[name="sf_guard_user[password]"]').val()==$('[name="sf_guard_user[password2]"]').val();
        }, "Confirm Password do not match Password");
        $.validator.addMethod("email", function(value, element)
        {
            return this.optional(element) || /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i.test(value);
        }, "Please enter correct Email Address");
        $.validator.addMethod("checkIfSexSelected", function(value, element)
        {
            if(value==0)
            {
                return false;
            }
            else
            {
                return true;
            }
            
        }, "Please select sex");
        jQuery.validator.addMethod('isValidPhone', function(value, element) {

            return this.optional(element) || value.match(/^[+]{0,1}[0-9-]*$/);
        },'Please enter valid Phone Number');
        $("#division_frm").validate({
            rules: {
                'sf_guard_user[email_address]': {
                    required: true,
                    email: true

                }, 'sf_guard_user[password]': {
                    required: true,
                    pwd_minLength: true

                }, 'sf_guard_user[password2]': {
                    required: true,
                    pwd_minLength: true,
                    chk_passwords: true
                    

                }, 'sf_guard_user[Details][name]': {
                    required: true,
                    valid_name: true,
                    maxlength: 50

                }, 'sf_guard_user[Details][sex]': {
                    checkIfSexSelected: true

                }, 'sf_guard_user[Details][address]': {
                    required: true,
                    maxlength: 100
                    

                }, 'sf_guard_user[Details][phone_number]': {
                    required: true,
                    isValidPhone: true,
                    maxlength: 20,
                    minlength: 6

                }
            },

            messages: {
               
                'sf_guard_user[email_address]':
                    {
                    required: "Please enter Email Address"

                },
                'sf_guard_user[password]':
                    {
                    required: "Please enter Password"

                },'sf_guard_user[password2]':
                    {
                    required: "Please enter Confirmation Password"

                },
                'sf_guard_user[Details][name]':
                    {
                    required: "Please enter Name",
                    maxlength: "Maximum 50 characters"

                },
                'sf_guard_user[Details][sex]':
                    {
                    checkIfSexSelected: "Please enter Sex"
                },
                'sf_guard_user[Details][address]':
                    {
                    required: "Please enter Address",
                    maxlength: "Maximum 100 characters"

                },
                'sf_guard_user[Details][phone_number]':
                    {
                    required: "Please enter Phone Number",
                    maxlength: "Maximum 20 digits",
                    minlength: "Minimum 6 digits"

                }
            }


        })

    });

   

</script>
<?php
//if(!$merchantLogin){ // means coming from teller page
if (isset($_SESSION['logedin_user_group']) && $_SESSION['logedin_user_group'] == 'teller') {
    include_partial('subHeaderForTeller', array('addClassTo' => 'new', 'adminLogin' => ''));
} else {
    if (isset($addClassTo)) {
        include_partial('subHeader', array('addClassTo' => $addClassTo, 'merchantLogin' => $merchantLogin));
    } else {
        include_partial('subHeader', array('addClassTo' => 'new', 'merchantLogin' => $merchantLogin));
    }
}
?>

<?php include_partial('form', array('form' => $form, 'attr' => $dynamicAttFieldName)) ?>
<input type="hidden" name="login_for" value="<?php if (isset($hidden_value)) {
    echo $hidden_value;
} ?>">

