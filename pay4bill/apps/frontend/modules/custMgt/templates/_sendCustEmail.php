<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Untitled Document</title>
    </head>

    <body>
        <table align="center" cellpadding="0" cellspacing="0" style="width:650px;">

            <tr><?php



            ?>
                <td style="padding-bottom:5px;">
                    <?php include_partial('global/pay4bill_logo', array('path' => $path));    ?>
                    
                </td>


            </tr>


            <table cellspacing="0" cellpadding="10" border="0" align="center" bgcolor="#f0f0f0" style="width:650px; border-collapse:collapse; border:1px solid #dedcdc; color:#646464; font-family:Arial, Helvetica, sans-serif; font-size:14px;">
                <tr>
                    <td>Dear <span><?php echo $customerName; ?></span>,</td>
                </tr>

                <tr>
                    <td>Your have been registered on the pay4bill portal by the merchant <strong style="color:#000"><?php echo ucwords($merchantName); ?></strong>.</td>
                </tr>

                <tr>
                    <td>Your details to access the portal are as follows:</td>
                </tr>

                <tr>
                    <td>Customer Id: <strong style="color:#000"><?php echo $customer_id; ?></strong></td>
                </tr>
                <?php
                    if (!is_null($pin)) {
                ?>
                        <tr>
                            <td>PIN: <strong style="color:#000"><?php echo cryptString::decrypt($pin); ?></strong></td>
                        </tr>
                <?php
                    } else {
                ?><tr>
                            <td>You need to pay at least one invoice to get online access on the pay4bill portal.<br />
                                Validation Number on the Transaction receipt will be required for registration process.</td>
                        </tr>
<?php
                    }
?>


                    <tr>
                        <td>Please click on the below link to register on the portal.</td>
                    </tr>


                    <tr>
                        <td><a href="<?php echo $url; ?>" style="color:#0c5c89;"><?php echo $url; ?></a></td>
                </tr>

                <tr>
                    <td>
                        Regards,<br />
                        The Pay4bill Team
                    </td>
                </tr>

                <tr><td style="background-color:#18648e; color:#ffffff; padding:10px; font:14px Arial, Helvetica, sans-serif;">If you experience any problem, write to us at <a href="mailto:support@pay4bill.com" style="color:#fff">
                            support@pay4bill.com</a></td></tr>

            </table>

        </table>


    </body>
</html>