<script>
    function validateSearch(){
        return true;
    }
    function populateSearch(){       
    }

    $(document).ready(function(){
        populateSearch();
        
<?php if ($admin_login) { ?>
            $('#Merchants').addClass('active');
<?php } else { ?>
            $('#Customers').addClass('active');
<?php } ?>
         
<?php if (isset($msg) && $msg != '') { ?>
            $('#if_error').html('<?php echo html_entity_decode($msg); ?>');
            $('#if_error').addClass('msg_serverside');
<?php } else { ?>
            $('#if_error').html();
            $('#if_error').removeClass('msg_serverside');
<?php } ?>
        
    }
);
</script>


<?php include_partial('subHeader', array('addClassTo' => $addClassTo, 'merchantLogin' => $merchant_login)); ?>

<?php if (!empty($msg)) {
?>
    <div class="clear_new"></div>
<?php } ?>

<div id="mainnew">
    <?php
    $sf = sfContext::getInstance()->getUser();
    if ($sf->hasFlash('notice')) {
    ?>
        <div id="flash_notice" class="msg_serverside" >
        <?php echo nl2br($sf->getFlash('notice')); ?>
    </div>
    <?php } ?>

    <div id="wrappernew">
        <div id="content">
            <div id="Formwraper">
                <?php if (isset($merchant_key) && $merchant_key != '') {
                ?>

                    <h3>Search Customer</h3>
                    <div class="TableForm" id="search">
                        <form action="<?php echo url_for('custMgt/index'); ?>" method="post" id="userForm" onsubmit="return validateSearch()" >
                        <?php if ($createInputHiddenOfMerchantIdForAdminLogin) {
                        ?>
                            <input type="hidden" name="merchant_id" value="<?php echo cryptString::encrypt($merchant_id); ?>">
                        <?php } ?>
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="17.5%" valign="top"><?php echo "Customer Name" ?> </td>
                                <td  width="35%" valign="top">
                                    <input class="FormInput9" type="textbox" name="name" value="<?php echo $name; ?>">
                                </td>
                            </tr>
                            <tr>
                                <td width="17.5%" valign="top"><?php echo "Customer Id" ?> </td>
                                <td  width="35%" valign="top">
                                    <input class="FormInput9" type="textbox" name="customer_id" value="<?php echo $customer_id; ?>">
                                </td>
                            </tr>
                            <tr>
                                <td width="17.5%" valign="top"><?php echo "Phone Number" ?> </td>
                                <td  width="35%" valign="top">
                                    <input class="FormInput9" type="textbox" name="phone_number" value="<?php echo $phone_number; ?>">
                                </td>
                            </tr>
                            <tr class="">
                                <td colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="0" id="merchantSpecificAttributes">
                                        <tbody>
                                            <?php foreach ($attributes as $attName => $attLabel) {
 ?>
                                                <tr>
                                                    <td width="17.5%" valign="top"><?php echo $attLabel; ?> </td>
                                                    <td  width="35%" valign="top">
                                                        <input class="FormInput9" type="textbox" name="dynamicAttr[<?php echo $attName; ?>]" value="<?php
                                                    if (isset($$attName)) {
                                                        echo $$attName;
                                                    }
                                            ?>">
                                                </td>
                                            </tr>
                                            <?php
                                                }
                                            ?></tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr >
                                    <td colspan="3"  valign="top">
                                        <div class="Errormrg"><input type="submit" value="Search" class="blueBtn" >
                                            <input type="button" class="blueBtn" value="Reset" onClick="window.location='<?php echo url_for('custMgt/index?merchant_id=' . cryptString::encrypt($merchant_id)); ?>'"/>
                                        </div>
                                    </td>
                                </tr>
                            </table>

                        </form>
                    </div>

<?php } ?>
                                            <h3> <?php if (!$admin_login) {
?>Customers<?php
                                            } else {
                                                echo "Merchant : " . ucfirst($merchantName);
                                            }
?></h3>
                                        <div id="if_error" class="msg_serverside" <?php if ($admin_login) { ?>style="display: none" <?php } ?>></div>
                                        <div class="clear_new"></div>
                                        <div class="TableForm">

                                            <table cellspacing="0" cellpadding="0" border="0">
                                                <thead>
                                                <th>Name</th>
                                                <th>Customer Id</th>
                        <!--                        <th>Email Address</th>-->
                                                <th>Sex</th>
                        <?php if ($admin_login) { ?>  <th>Address</th><?php } ?>
                                            <th>Phone number</th>
                        <?php
                                            if ($customer_profiles->count() > 0 && $dynamic_attributes_array->count() > 0) {

                                                foreach ($dynamic_attributes_array as $colName => $colValue) {
                                                    if (trim($colValue) != "") {
                        ?><th><?php echo $attributes[$colName]; ?></th>
                        <?php
                                                    }
                                                }
                                            }
                        ?>
                        <?php if ($merchant_login) {
                        ?>  <th>Action</th><?php } ?>
                                            </thead>
                                            <tbody>
                            <?php
                                            if ($customer_profiles->count()> 0) {
                            ?>
                            <?php
                                                $i = 0;
                                                foreach ($customer_profiles as $customer_profile): $arrUserEmail = '';
                                                    $i++;
                            ?>
                            <?php if ($i % 2 != 0) {
 ?>
                                                        <tr class="odd">
<?php } else { ?><tr class=""> <?php } ?>
                                                        <td><?php echo $customer_profile->getName() ?></td>
                                                        <td><?php echo $customer_profile->getCustomerId(); ?></td>
                                                    <!--    <td><?php
                                                    //    echo $customer_profile->getEmailAddress();
                                                    /* if(!empty($customer_profile['sfGuardUser'])) {echo $customer_profile->sfGuardUser->getUserName(); } else echo "N/A"; */
?></td> -->
                                                    <td><?php echo ucfirst($customer_profile->getSex()) ?></td>
                                <?php if ($admin_login) {
                                ?><td><?php echo $customer_profile->getAddress() ?></td>
<?php } ?>
                                                    <td><?php echo $customer_profile->getPhoneNumber() ?></td>
                                <?php
                                                    if ($dynamic_attributes_array->count() > 0) {

                                                        foreach ($dynamic_attributes_array as $colname => $colValue) {

                                                            if (trim($colValue) != "") {
                                ?><td><?php echo $dynamicAttr_value[$customer_profile->getId()][$colname]; ?></td>
                                <?php
                                                            }
                                                        }
                                                    }
                                ?>
<?php if ($merchant_login) { ?>
                                                        <td>
                                    <?php
                                                        echo link_to(image_tag('tdEdit.png'), 'custMgt/edit?id=' . cryptString::encrypt($customer_profile->getId()), array('title' => 'Edit', 'alt' => 'Edit'));
                                    ?>&nbsp;&nbsp;&nbsp;&nbsp;
                                    <?php echo link_to(image_tag('tdDelete.png'), 'custMgt/delete?id=' . $customer_profile->getId(), array('title' => 'Delete', 'alt' => 'Delete', 'class' => 'delete', 'confirm' => 'Are you sure you want to delete the customer?')); ?>&nbsp;&nbsp;&nbsp;&nbsp;
                                    <?php
                                                        if ($customer_profile->getIsActive() == '1') {
                                                            echo link_to(image_tag('enable.gif'), 'custMgt/disableCustomer?mid=' . cryptString::encrypt($customer_profile->getMerchantProfileId()) . '&id=' . cryptString::encrypt($customer_profile->getId()), array('title' => 'Disable Customer', 'alt' => 'Disable Customer', 'class' => 'delete', 'confirm' => 'Are you sure you want to disable this customer?'));
                                                        } else {
                                                            echo link_to(image_tag('disable.gif'), 'custMgt/enableCustomer?mid=' . cryptString::encrypt($customer_profile->getMerchantProfileId()) . '&id=' . cryptString::encrypt($customer_profile->getId()), array('title' => 'Enable Customer', 'alt' => 'Enable Customer', 'class' => 'delete', 'confirm' => 'Are you sure you want to enable this customer?'));
                                                        }
                                    ?>
                                                    </td>
<?php } ?>
                                                </tr>
<?php endforeach; ?>

                            <?php
                                                } else {
                                                    echo "<tr><td colspan='6' align='center'>No Customer Found</td></tr>";
                                                }
                            ?>
                                            </tbody>
                                        </table>

                                    </div>
                                    <div class="nofoList">
                    <?php
                                                include_partial('global/records_number', array('amount_footer' => false, 'current_offset' => isset($current_offset) ? $current_offset : null, 'offset_next' => isset($offset_next) ? $offset_next : null, 'total_no_of_records' => isset($total_no_of_records) ? $total_no_of_records : count($customer_profiles), 'no_of_records_per_page' => isset($no_of_records_per_page) ? $no_of_records_per_page : null, 'from' => '', 'total_amount_of_all_transactions' => isset($total_amount_of_all_transactions) ? $total_amount_of_all_transactions : null));
                    ?>

                                            </div>
                                            <div align="right">
                    <?php
                                                if ($setPagination) {
                                                    $array_for_pagination = array('moduleName' => $module, 'amount_footer' => false, 'actionName' => $actionTobeCalled, 'current_offset' => $current_offset, 'offset_next' => $offset_next, 'offset_prev' => $offset_prev, 'offset_last' => $offset_last, 'search' => '', 'from' => '', 'cust_name' => $name, 'customer_id' => $customer_id);
                                                    if (isset($phone_number) && $phone_number != '') {
                                                        $array_for_pagination['phone_number'] = $phone_number;
                                                    }
                                                    if ($dynamic_attributes_array->count() > 0) {

                                                        foreach ($dynamic_attributes_array as $colname => $colValue) {

                                                            if (trim($colValue) != "") {
                                                                $array_for_pagination['dynamicAttr'][$colname] = trim($$colname);
                                                            }
                                                        }
                                                    }
                                                    if ($createInputHiddenOfMerchantIdForAdminLogin) {
                                                        $array_for_pagination['merchant_id'] = cryptString::encrypt($merchant_id);
                                                    }
                                                    $helperObj = new pay4billHelper();
                                                    $array_for_pagination = $helperObj->trimArray($array_for_pagination);
                                                    include_partial('global/pagination', $array_for_pagination);
                                                }
                    ?></div>
                <?php if ($admin_login) { ?>
                                                                <!--                                   <input name="back" type="button" class="blueBtn" value="Back" style="float:left" onclick="window.location='<?php //echo url_for('custMgt/index?merchant_id=' . cryptString::encrypt($merchant_id));     ?>'"/>-->
                                                    <input name="back" type="button" class="blueBtn" value="Back" style="float:left" onclick="window.location='<?php echo url_for('merchantRegistration/index'); ?>'"/>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
