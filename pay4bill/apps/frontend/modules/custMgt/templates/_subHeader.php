<?php


?><div id="pgMenu">
    <div id="wrappernew">
        <div class="pgTitle">
               <h2>Customers</h2>
        </div><?php if ($merchantLogin) {?>
        <div class="pgNav">
            <ul>
                <li <?php if($addClassTo=='index'){ ?>class="current" <?php } ?>><a href="<?php echo url_for('custMgt/index?offset=0') ?>">View Customers</a></li>
                <li <?php if($addClassTo=='new'){ ?>class="current" <?php } ?>><a href="<?php echo url_for('custMgt/new') ?>">Create Customer</a></li>
                <li <?php if($addClassTo=='upload'){ ?>class="current" <?php } ?>><a href="<?php echo url_for('custMgt/upload') ?>">Upload CSV</a></li>

        </ul>
        </div><?php } ?>
    </div>
</div>
