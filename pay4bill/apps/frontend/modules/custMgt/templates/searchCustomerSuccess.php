<script>
    $(document).ready(function(){
        $('#Customers').addClass('active');
<?php if (isset($msg)) { ?>
            $('#if_error').html('<?php echo html_entity_decode($msg); ?>');
            $('#if_error').addClass('msg_serverside');
<?php
} else {
?>$('#if_error').html();
                $('#if_error').removeClass('msg_serverside');
<?php
}
?>
    }
);
    //Validation Section for serch customer page
    function validateForm()
    {

        var err  = 0;
        var name = jQuery.trim($('#name').val());
        var email = jQuery.trim($('#email').val());
        var phone_number = jQuery.trim($('#phone_number').val());
        var reg = /^[A-Za-z0-9 ]*$/; //allow alphabet and numbers only...
        var emailreg = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i; //valid email check
        var phonereg = /^[0-9]*$/; //valid phone number check
        if(name =='' && email == ''){
            $('#name_error').html("<font color='red'>Please enter Name or Email</font>");
            $('#email_error').html("");
            err = err + 1;

        }else {
            if(name!='')
            {
                if(reg.test(name) == false) {
                    $('#name_error').html("<font color='red'>Please enter valid Name</font>");
                    $('#name').focus();
                    err = err + 1;
                } else {
                    $('#name_error').html("");
                }
            }
            if(email!='')
            {
                if(emailreg.test(email) == false) {
                    $('#email_error').html("<font color='red'>Please enter correct Email</font>");
                    $('#email_name').focus();
                    $('#name_error').html("");
                    err = err + 1;
                } else {
                    $('#email_error').html("");
                }
            }
        }
        if(phone_number!='')
        {
            if(phonereg.test(phone_number) == false) {
                $('#phone_number_error').html("<font color='red'>Please enter valid Phone Number</font>");
                $('#phone_number').focus();
                err = err + 1;
            } else {
                $('#phone_number_error').html("");
            }
        }

        if(err>0)
        {
            return false;
        }
        return true;
    }
    //Validation Section for add customer page
    function isatLeastOneCustomerSelected()
    {
        var return_var=false;
        $('.customer_chkBox').each(function(index,element) {
            if($(this).attr('checked'))
            {
                return_var=true;
            }

        });
        if(!return_var){
            $('#error_form').html('<i>Please select at least one Customer</i>');
            $('#error_form').css('display','inline');

        }
        else{
            $('#error_form').html('');
            $('#error_form').css('display','none');
        }
        return return_var;
    }
</script>

<?php include_partial('subHeader', array('addClassTo' => 'searchCustomer', 'adminLogin' => '')); ?>
<!-- Varun : Flash messaged added -->
<?php
$sf = sfContext::getInstance()->getUser();
if ($sf->hasFlash('notice')) {
?><div id="flash_notice" class="msg_serverside" ><?php
    echo nl2br($sf->getFlash('notice'));
?></div><?php } ?>
<div class="clear_new"></div>
<div id="merchantLogin">
    <div>
        <h3>Search Customer</h3>
    </div>
    <form action="<?php echo url_for('custMgt/searchCustomer') ?>" method="post" onsubmit="return validateForm()">
        <div class="TableForm">
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="35%" align="right">Name</td>
                    <td><input type="text" id="name" name="name" /><span id="dispErr_date" style="display: none;"></span>
                        <label for="name" generated="false" class="error" id="name_error"></label></td>
                </tr>
                <tr>
                    <td width="35%" align="right">Email</td>
                    <td>
                        <input type="text" id="email" name="email" />
                        <label for="email" generated="false" class="error" id="email_error"></label></td>
                </tr>
                <tr>
                    <td width="35%" align="right">Phone Number&nbsp;</td>
                    <td><input type="text" id="phone_number" name="phone_number" />
                        <label for="phone_number" generated="false" class="error" id="phone_number_error"></label></td>
                </tr>
                <tr>
                    <td width="35%" align="right">Gender</td>
                    <td><select id="gender" name="gender">
                            <option value="">Please Select</option>
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                        </select>
                        <label for="gender" generated="false" class="error" id="gender_error"></label></td>
                </tr>
                <tr>
                    <td width="35%" align="right"></td>
                    <td><input class="blueBtn" type="submit" value="Search" name="Submit">
                        <input class="blueBtn" type="reset" value="Reset" name="Reset" style="font-weight:bold"></td>
                </tr>
            </table>
        </div>
    </form>
</div>
<?php if ($customerDetail) {
 ?>
        <form action="<?php echo url_for('custMgt/addCustomer') ?>" id ="print_form" onsubmit="return isatLeastOneCustomerSelected()">
            <div class="TableForm" style="margin-top:0px;">
                <table border="0" cellspacing="0" cellpadding="0">
                    <thead>
                    <th></th>
                    <th>Customer Name</th>
                    <th>Email</th>
                    <th>Phone Number</th>
                    <th>Gender</th>
                    </thead>

                    <tbody>
                <?php
                if (count($customerDetail) > 0) {
                    $i = 0;
                    foreach ($customerDetail as $customerDetails) {
                        $i++;
                ?>
                <?php if ($i % 2 != 0) {
 ?>
                            <tr class="odd">
<?php } else { ?>
                        <tr class=""> <?php } ?>
                        <td><input type="checkbox" class="customer_chkBox" name="customer_ids[]" value="<?php echo $customerDetails->getId() ?>"></td>
                        <td><?php echo $customerDetails->getName(); ?></td>
                        <td><?php echo $customerDetails->getSfGuardUser()->getEmailAddress(); ?></td>
                        <td><?php echo $customerDetails->getPhoneNumber(); ?></td>
                        <td><?php echo $customerDetails->getSex(); ?></td>
                    </tr>
                <?php } ?>
                <?php
                } else {
                ?><tr><td colspan='5' align='center'>No Customer Found</td></tr>
<?php } ?>
            </tbody>
        </table>
<?php if (count($customerDetail) > 0) { ?>
                    <div class="right"><input class="blueBtn" type="submit" value="Add Customers" name="Submit"></div>
<?php } ?>
                <div id="error_form" class="error" style="display:none"></div>
            </div>
        </form>
<?php } ?>