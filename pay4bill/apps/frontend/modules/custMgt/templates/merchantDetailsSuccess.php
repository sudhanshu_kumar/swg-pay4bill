<script>
    $(document).ready(function(){
        $('#Settings').addClass('active'); //Varun : bug fix FS#34998 
        <?php if(isset($msg)){?>
        $('#if_error').html('<?php echo html_entity_decode($msg); ?>');
    <?php
    if($error){   ?>$('#if_error').addClass('error_serverside'); <?php } else { ?>$('#if_error').addClass('msg_serverside');<?php } ?>
    <?php }else
{
    ?>$('#if_error').html();
            $('#if_error').removeClass('error_serverside');
            $('#if_error').removeClass('msg_serverside');
    <?php } ?>
        $('form').attr('id','division_frm');
        // Varun : bug fix : FS#34472

        jQuery.validator.addMethod('isValidCAC', function(value, element) {

            return this.optional(element) || value.match(/^[a-zA-Z0-9]*$/);
        },'Invalid CAC');
        jQuery.validator.addMethod('webUrlValidation', function(value, element) {

            return this.optional(element) || value.match(/http(s)*:\/\/[A-Za-z0-9\.-]{3,}\.[A-Za-z]{3}/);
        },'Invalid URL');
        $("#division_frm").validate({
            rules: {
                  'sf_guard_user[Profile][cac_registration]': {
                    required: true,
                    isValidCAC: true,
                    maxlength: 20

                }, 'Company URL': {
                    required: true,
                    webUrlValidation: true,
                    maxlength: 100

                }

            },

            messages: {

                'sf_guard_user[Profile][cac_registration]':
                    {
                    required: "CAC Registration is Required",
                    maxlength: "Maximum 20 digits"

                },

                'Company URL':
                    {
                    required: "Company URL is Required",
                    maxlength: "Maximum 100 characters"

                }

            }


        })

    });

</script>
<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>
<!-- Varun : Bug fix :FS#34998 -->
<div id="pgMenu">
    <div id="wrappernew">
        <div class="pgTitle">
            <h2>Merchant Details</h2>
        </div>
    </div>
</div>

<p>
<form action="<?php echo url_for('custMgt/customerAsMerchant') ?>" method="post" <?php $form->isMultipart() and print 'enctype="multipart/form-data" ' ?>>
    <?php if (!$form->getObject()->isNew()): ?>
        <input type="hidden" name="sf_method" value="put" />
    <?php endif; ?>
         <div class="clear_new"></div>
        <div class="sform">
            <h3>Merchant Information</h3>

            <div class="FormRepeatArea">
                <div class="FormLabel"> <?php echo $form['Profile']['cac_registration']->renderLabel(); ?> </div>
                <div class="FromInputLabe2"> <?php echo $form['Profile']['cac_registration']->render(); ?> <?php echo $form['Profile']['cac_registration']->renderError(); ?></div>
                <!--                <div class="FormError">
                                    <div id="error-msg"></div>
                                </div>-->
                <div class="clear_new"></div>
            </div>
            <!--  New div added for File upload -->
          <!--  <div class="FormRepeatArea">
                            <div class="FormLabel"><?php //echo $form['Profile']['company_logo']->renderLabel(); ?></div>
                            <div class="FromInputLabe2"><?php //echo $form['Profile']['company_logo']->render(); ?> <br>
                                -Please upload JPG/GIF/PNG images only.
                                -File size upto 400 KB.<br>
                                <?php //echo $form['Profile']['company_logo']->renderError(); ?> </div>
           </div> -->
            <div class="FormRepeatArea">
                        <div class="FormLabel txtRightAlign"> WEB URL<span class="redLbl">*</span> </div>
                        <div class="FromInputLabe2">  <input type="text" name="Company URL" value=""></div>
                        <!--                <div class="FormError">
                                            <div id="error-msg"></div>
                                        </div>-->
                        <div class="clear_new"></div>
                    </div>

        <?php echo $form['_csrf_token']; ?>
        <input type="hidden" name="var" id="var" value="<?php echo "1"; ?>">

        <div class="button-position">
            <input type="submit" value="Save" class="blueBtn" />
            <input type="button" name="cancel" id="cancel_button"  class="blueBtn" value="Cancel" onclick="javascript:history.go(-1)"/>
        </div>
    </div>
</form>

<p>&nbsp;</p>


