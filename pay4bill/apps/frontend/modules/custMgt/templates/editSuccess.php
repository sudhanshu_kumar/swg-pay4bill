<?php
use_helper('pay4bill');
?><script>
    
    $(document).ready(function(){
        var firstClickOnSubmit=true;
        $('#Customers').addClass('active');
        $('form').attr('id','customerEdit');
         
        
        $.validator.addMethod("valid_name", function(value, element)
        {
            return this.optional(element) || /^[a-zA-Z\.\s]*$/.test(value);
        }, "Please enter valid name");
        
        jQuery.validator.addMethod('isValidPhone', function(value, element) {

            return this.optional(element) || value.match(/^[0-9+-]*$/);
        },'Please enter valid Phone Number');
        
        $("#customerEdit").validate({
            rules: {
                'customer_profile[address]': {
                    required: true                    
                }, 'customer_profile[name]': {
                    required: true,
                    valid_name: true,
                    maxlength: 50
                }, 'customer_profile[phone_number]': {
                    required: true,
                    isValidPhone: true,
                    maxlength: 20,
                    minlength: 6,
                }, 'customer_profile[sex]': { //Varun : Bug fix : FS#35025 
                    required: true
                }
                <?php if($form->getOption('email_address_mandatary')==true){
?>

                ,'customer_profile[email_address]': {
                    required: true,
                    email: true
}
                <?php
                    }  ?>
            },

            messages: {
                'customer_profile[address]':
                    {
                    required: "Please enter Address"

                },
                'customer_profile[name]':
                    {
                    required: "Please enter Name",
                    maxlength: "Maximum 50 characters"

                },'customer_profile[phone_number]':
                    {
                    required: "Please enter Phone Number",
                    maxlength: "Phone Number can not be more than 20 digits",
                    minlength: "Phone Number can not be less than 6 digits"


                },
                'customer_profile[sex]':
                    {
                    required: "Please select Sex"

                }
                <?php if($form->getOption('email_address_mandatary')==true){
?>

                ,'customer_profile[email_address]': {
                    required: "Please enter email address",
                    email: "Pleas enter valid email address"
}
                <?php
                    }  ?>

            }


        })
       

<?php
if (isset($dynamicAttFieldName)) {
    foreach ($dynamicAttFieldName as $attName => $attDetails) {

        if ($attDetails['validator']['is_mandatory'] == 1) {
?>
                    $.validator.addMethod("<?php echo $attName ?>_required", function(value, element)
                    {
                        if((value==""))
                        {
                            $('span[class=error_list]').hide();
                        }
                        return !(value=="");
                    }, "Please enter <?php echo $attDetails['label'] ?>");
                    $('#<?php echo $attName ?>_id').rules("add","<?php echo $attName ?>_required");

<?php
        }
?>

                $.validator.addMethod("<?php echo $attName ?>_regex_rule", function(value, element)
                {
                    if(value!=""){

                        if(!value.match(<?php echo pay4billHelper::regexPatternforAttType($attDetails['validator']["att_type"]); ?>))
                        {
                            $('span[class=error_list]').hide();
                        }
                        return (value.match(<?php echo pay4billHelper::regexPatternforAttType($attDetails['validator']["att_type"]); ?>));
                    }
                    else
                    {
                        return true;
                    }
                }, '<?php echo pay4billHelper::validationMsgforAttType($attDetails['validator']["att_type"]); ?>');
                $('#<?php echo $attName ?>_id').rules("add","<?php echo $attName ?>_regex_rule");



<?php
    }
}
?>

              
    });

</script><?php
if (isset($addClassTo)) {

    if (isset($_SESSION['logedin_user_group']) && $_SESSION['logedin_user_group'] == 'teller') {
        include_partial('subHeaderForTeller', array('addClassTo' => $addClassTo, 'adminLogin' => $admin_login));
    } else if (isset($_SESSION['logedin_user_group']) && $_SESSION['logedin_user_group'] == 'customer') {
        include_partial('global/settingsSubHeaderForCustomerLogin', array('addClassTo' => $addClassTo));
    } else {
        include_partial('subHeader', array('addClassTo' => $addClassTo, 'adminLogin' => $admin_login, 'merchantLogin' => $merchant_login));
    }
} else {

    if (isset($_SESSION['logedin_user_group']) && $_SESSION['logedin_user_group'] == 'teller') {
        include_partial('subHeaderForTeller', array('addClassTo' => 'new'));
    } else {
        include_partial('subHeader', array('addClassTo' => 'new'));
    }
} ?>


<?php include_partial('editForm', array('form' => $form, 'attr' => $dynamicAttFieldName)) ?>
