<script type="text/javascript">


    $(document).ready(function(){


        //        var emptyMerchant=false;

        $("#<?php echo $form->getName(); ?>").validate({
            rules: {
                '<?php echo $form->getName(); ?>[activeMerchants]': {
                    required: true

                }
                , '<?php echo $form->getName(); ?>[customerId]': {
                    required: true

                }
            },

            messages: {
                '<?php echo $form->getName(); ?>[activeMerchants]':
                    {
                    required: "Please select Merchant"
                },
                '<?php echo $form->getName(); ?>[customerId]':
                    {
                    required: "Please enter  Customer Id"
                }

            }


        })


        var nameFormat='<?php echo $form->getName(); ?>';
        if(nameFormat!='')
        {
            nameFormat+='_';
        }
        function hidePinValidationNumber()
        {


        
            if($('#'+nameFormat+'activeMerchants').val()==''){


                $('#'+nameFormat+'pin').hide();
                $('[for='+nameFormat+'pin]').hide();
                $('#'+nameFormat+'validation_number').hide();
                $('[for='+nameFormat+'validation_number]').hide();
//                $('#idDynamicRow').hide();
            }
<?php
if (isset($merchantType) && $merchantType == 'b') {
?>
                $('#'+nameFormat+'pin').hide();
                $('[for='+nameFormat+'pin]').hide();
                //            }

<?php
}
?>
<?php
if (isset($merchantType) && $merchantType == 'o') {
?>

                $('#'+nameFormat+'validation_number').hide();
                $('[for='+nameFormat+'validation_number]').hide();

<?php
}
?>
            removeRule($('#'+nameFormat+'pin'),'required');
            removeRule($('#'+nameFormat+'validation_number'),'required');

        }
        function showPin()
        {
            
            $('#'+nameFormat+'pin').show();
            $('[for='+nameFormat+'pin]').show();
            $('#'+nameFormat+'validation_number').hide();
            $('[for='+nameFormat+'validation_number]').hide();
            $('#'+nameFormat+'validation_number').val('');
            addRequiredRule( $('#'+nameFormat+'pin'),'Please enter PIN')
            removeRule( $('#'+nameFormat+'validation_number'),'required')

        }
        function showValidationNumber()
        {
            $('#'+nameFormat+'pin').hide();
            $('[for='+nameFormat+'pin]').hide();
            $('#'+nameFormat+'pin').val('');
            $('#'+nameFormat+'validation_number').show();
            $('[for='+nameFormat+'validation_number]').show();
            addRequiredRule( $('#'+nameFormat+'validation_number'),'Please enter Validation Number')
            removeRule( $('#'+nameFormat+'pin'),'required')

        }
        function addRequiredRule(element,msg)
        {
            
            element.rules('add',{required:true,messages: {required:msg}});         


        }
        function removeRule(element,rule)
        {
            element.rules('remove',rule);
        }
        

        $('#'+nameFormat+'activeMerchants').bind('change', function(){
            $('span[class="error_list"]').html('');
            $('label[class="error"]').html('');
 
            if($(this).val()!=""){

                var url="<?php echo url_for('custMgt/getTextBoxOnMerchantSelection'); ?>";
                
                $.post(url, {merchant_profile_id:$(this).val()}, function(data){
                    switch(data)
                    {
                        case "o":
                            showPin();
                            break;
                        case "b":
                            showValidationNumber();
                            break;

                    }

                });
            }
            else{
                $('#'+nameFormat+'customerId').val('');
                hidePinValidationNumber();
            }
        });

        hidePinValidationNumber();
    }
);
</script>


<div id="form_section">
    <?php
    if (isset($getLogginUser) && !empty($getLogginUser) ) {
        if (pay4billHelper::checkIfCustomerUser(sfContext::getInstance()->getUser()->getGuardUser()->getGroups()->toArray())) {
            include_partial('global/settingsSubHeaderForCustomerLogin', array('addClassTo' => $addClassTo));
            echo '<h3>Add Customer Profile</h3>';
        }
        
        if (pay4billHelper::checkIfMerchantUser(sfContext::getInstance()->getUser()->getGuardUser()->getGroups()->toArray())) {
            include_partial('global/settingsSubHeaderForMerchant', array('addClassTo' => $addClassTo));
            echo '<h3>Add Customer Profile</h3>';

        }
        
    } else {
    ?>
        <div id="pgMenu">
            <div id="wrappernew">
                <div class="pgTitle"><h3>Customer Registration</h3></div>
            </div>
        </div>
<?php } ?>
    
    <div style="clear:both"></div>


<?php
    $sf = sfContext::getInstance()->getUser();
    if ($sf->hasFlash('error')) {
?>
    <div id="flash_notice" class="error_serverside" >
        <?php echo nl2br($sf->getFlash('error'));?>
    </div>
    <div style="clear:both"></div>
<?php
    }
    if ($sf->hasFlash('notice')) {
?>
    <div id="flash_notice" class="msg_serverside" >
        <?php echo nl2br($sf->getFlash('notice')); ?>
    </div>
    <div style="clear:both"></div>
<?php } ?>
    
    <form  id="<?php echo $form->getName(); ?>" action="<?php echo url_for('custMgt/addProfile'); ?>" method="post" class="dlForm"  >
        <div class="TableForm">
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="35%" align="right"><?php echo $form['activeMerchants']->renderLabel(); ?></td>
                    <td><?php echo $form['activeMerchants']->render(); ?><?php echo $form['activeMerchants']->renderError(); ?></td>
                </tr>
                <tr>
                    <td width="35%" align="right"><?php echo $form['customerId']->renderLabel(); ?></td>
                    <td><?php echo $form['customerId']->render(); ?><?php echo $form['customerId']->renderError(); ?></td>
                </tr>
                <tr id="idDynamicRow">
                    <td width="35%" align="right"><?php echo $form['pin']->renderLabel(); ?><?php echo $form['validation_number']->renderLabel(); ?></td>
                    <td>
                        <?php echo $form['pin']->render(); ?><?php echo $form['pin']->renderError(); ?>
                        <?php echo $form['validation_number']->render(); ?><?php echo $form['validation_number']->renderError(); ?>
                    </td>
                </tr>
                <?php echo $form['_csrf_token']; 
                
                if(isset($getLogginUser) && !empty($getLogginUser))
                    $redirect = 'userAdmin/changePassword';
                else
                    $redirect = '@homepage';
                
                ?>
                <tr>
                    <td width="35%" align="right"></td>
                    <td>
                        <input type="submit" class="blueBtn" value="<?php if(isset($getLogginUser) && !empty($getLogginUser)) echo 'Add'; else echo 'Continue';?>" onclick="$('span[class=error_list]').html('')"/>
                        <input type="button" name="cancel" id="cancel_button"  class="blueBtn" value="Cancel" onclick="<?php if(isset($getLogginUser)){    ?> history.go(-1) <?php } else { ?>javascript: window.location='<?php echo url_for($redirect); ?>'<?php } ?>"/>
                   </td>
                </tr>
            </table>
        </div>
    </form>
    <p>&nbsp;</p>
</div>
