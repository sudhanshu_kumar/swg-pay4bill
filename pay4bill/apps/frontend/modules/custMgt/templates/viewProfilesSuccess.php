<script type="text/javascript">



    $(document).ready(function(){


        $('#Customers').addClass('active');
        $('#Settings').addClass('active');


    }
);
</script>


<?php
include_partial('global/settingsSubHeaderForCustomerLogin', array('addClassTo' => $addClassTo));
?>
<?php if (!empty($msg)) {
?><div class="clear_new"></div> <?php } ?>

<div id="mainnew">

    <?php
    $sf = sfContext::getInstance()->getUser();
    if ($sf->hasFlash('notice')) {
    ?><div id="flash_notice" class="msg_serverside" ><?php
        echo nl2br($sf->getFlash('notice'));
    ?></div><?php } ?>
    <div id="wrappernew">
        <div id="content">
            <div id="Formwraper">

                <h3>Search Profile</h3>
                <div class="TableForm" id="search">
                    <form action="<?php echo url_for('custMgt/viewProfiles'); ?>" method="post" id="userForm"  >
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="17.5%" valign="top"><?php echo "Customer Id"; ?> </td>
                                <td  width="35%" valign="top">
                                    <input class="FormInput9" type="textbox" name="customer_id" value="<?php
        echo $customer_id;
    ?>">
                                </td>
                            </tr>
                            <tr>
                                <td width="17.5%" valign="top"><?php echo "Merchant Name" ?> </td>
                                <td  width="35%" valign="top">
                                    <input class="FormInput9" type="textbox" name="merchant_name" value="<?php
                                           echo $merchant_name;
    ?>">
                                </td>
                            </tr>


<!--                             <tr class="">
                              <td colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="0" id="merchantSpecificAttributes">
                                      <tbody></tbody>
                                  </table>
                              </td>
                          </tr>-->
                            <tr id="merchantSpecificAttributes">
                                <td colspan="3"  valign="top">
                                    <div class="Errormrg"><input type="submit" value="Search" class="blueBtn" >
                                        <input type="button" class="blueBtn" value="Reset" onClick="window.location='<?php echo url_for('custMgt/viewProfiles'); ?>'"/>
                                    </div>
                                </td>
                            </tr>
                        </table>

                    </form>
                </div>

                <h3>Profiles</h3>
                <div id="if_error" class="msg_serverside"></div>
                <div style="clear:both"></div>
                <div class="TableForm">
                    <table cellspacing="0" cellpadding="0" border="0">
                        <thead>
                        <th>Customer Id</th>
                        <th>Merchant Name </th>
                        <th>Name </th>
                        <th>Address</th>
                        <th>Phone Number</th>

                        <th>Action</th>
                        </thead>
                        <tbody>
                            <?php
                                if (count($customer_profiles) > 0) {
                            ?>
                            <?php
                                               $i = 0;
                                               foreach ($customer_profiles as $customer_profile):
                                            if($customer_profile->getIsActive() == 0 ) $bgcl = " style='color:#ff0000 !important;'; title=\"This profile has been disabled by merchant.\""; else $bgcl = "";           
                                                   $i++;
                            ?>
                            <?php if ($i % 2 != 0) {
                            ?>
                                                       <tr class="odd">
                                <?php } else {
                                ?><tr class=""  > <?php } ?>
                                                   <td <?php echo $bgcl?>><?php echo $customer_profile->getCustomerId() ?></td>
                                                   <td <?php echo $bgcl?>><?php echo $customer_profile->getMerchantProfile()->getMerchantName(); ?></td>
                                                   <td <?php echo $bgcl?>><?php echo $customer_profile->getName(); ?></td>
                                                   <td <?php echo $bgcl?>><?php echo wordwrap($customer_profile->getAddress(), 24, "<br />\n", TRUE); ?></td>

                                                   <td <?php echo $bgcl?>><?php echo wordwrap($customer_profile->getPhoneNumber(), 11, "<br />\n", TRUE); ?></td>

                                                   <td <?php echo $bgcl?>>

                                    <?php 
                                            if($customer_profile->getIsActive() != 0){
                                                   echo link_to(image_tag('detail.png'), 'custMgt/showcustomer?id=' . cryptString::encrypt($customer_profile->getId()), array('title' => 'View Detail', 'alt' => 'View Detail',
                                                       'popup' => array('Windowtitle', 'resizeable=yes,scrollbars=yes,location=0,menubar,top=140,left=200,width=920,height=700')
                                                   ));
                                                   echo link_to(image_tag('tdEdit.png'), 'custMgt/edit?id=' . cryptString::encrypt($customer_profile->getId()), array('title' => 'Edit Customer', 'alt' => 'Edit Customer'));
                                            } else {
                                                   echo image_tag('detail.png', array("title"=>"Disable"));
                                                   echo image_tag('tdEdit.png', array("title"=>"Disable"));
                                            }      
                                    ?>&nbsp;&nbsp;&nbsp;&nbsp;

                                               </td>

                                           </tr>
                            <?php endforeach; ?>

                            <?php
                                               } else {
                                                   echo "<tr><td colspan='7' align='center'>No Customer Found</td></tr>";
                                               }
                            ?>
                                           </tbody>
                                       </table>

                                   </div>
                                   <div class="nofoList">
                    <?php
                                               include_partial('global/records_number', array('amount_footer' => false, 'current_offset' => isset($current_offset) ? $current_offset : null, 'offset_next' => isset($offset_next) ? $offset_next : null, 'total_no_of_records' => isset($total_no_of_records) ? $total_no_of_records : count($customer_profiles), 'no_of_records_per_page' => isset($no_of_records_per_page) ? $no_of_records_per_page : null, 'from' => '', 'total_amount_of_all_transactions' => isset($total_amount_of_all_transactions) ? $total_amount_of_all_transactions : null));
                    ?>

                                           </div>
                                           <div align="right">
                    <?php
                                               if ($setPagination) {


                                                   $array_to_be_included_in_pagination = array('moduleName' => $module, 'amount_footer' => false, 'actionName' => $actionTobeCalled, 'mer_name' => $merchant_name, 'customer_id' => $customer_id, 'current_offset' => $current_offset, 'offset_next' => $offset_next, 'offset_prev' => $offset_prev, 'offset_last' => $offset_last);
                                                   include_partial('global/pagination', $array_to_be_included_in_pagination);
                                               }
                    ?></div>

            </div>
        </div>
    </div>
</div>
