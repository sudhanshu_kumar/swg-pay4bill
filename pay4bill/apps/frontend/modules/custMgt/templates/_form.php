<?php use_stylesheets_for_form($form) ?>
<?php use_javascripts_for_form($form) ?>

<script>
    function checkServerSideMessage(){
        if($('#division_frm').valid() == false){
            $('#msgPwd').html('');
            $('#msgPwd2').html('');
            $('#msgEmail').html('');
            return false;
        } else {
            return true;
        }
    }

    function removeEmailServerSideMessage(){
        $('#msgEmail').text('');
    }
   
</script>
<div id="Formwraper">
    <div>
        <h3>Create Customer</h3>
    </div>
<!--    <form  action="<?php //echo url_for('custMgt/saveNewCustomer') . (!$form->getObject()->isNew() ? '?id=' . $form->getObject()->getId() : '')) ?>" method="post" class="dlForm" <?php //$form->isMultipart() and print 'enctype="multipart/form-data" ' ?> onsubmit="return checkServerSideMessage()">-->
<form  action="<?php echo url_for('custMgt/saveNewCustomer');?>" method="post" class="dlForm"  onsubmit="return checkServerSideMessage()">

        <?php if (!$form->getObject()->isNew()): ?>
            <input type="hidden" name="sf_method" value="put" />
        <?php endif; ?>
            <div class="TableForm">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="35%" align="right"><?php echo $form['name']->renderLabel(); ?></td>
                        <td><?php echo $form['name']->render(); ?><?php echo $form['name']->renderError(); ?></td>
                    </tr>
                    <tr>
                        <td width="35%" align="right"><?php echo $form['sex']->renderLabel(); ?></td>
                        <td><?php echo $form['sex']->render(); ?><?php echo $form['sex']->renderError(); ?></td>
                    </tr>
                    <tr>
                        <td width="35%" align="right"><?php echo $form['address']->renderLabel(); ?></td>
                        <td><?php echo $form['address']->render(); ?><?php echo $form['address']->renderError(); ?></td>
                    </tr>
                    <tr>
                        <td width="35%" align="right"><?php echo $form['phone_number']->renderLabel(); ?></td>
                        <td><?php echo $form['phone_number']->render(); ?><?php echo $form['phone_number']->renderError(); ?></td>
                    </tr>
                    <tr>
                        <td width="35%" align="right"><?php echo $form['email_address']->renderLabel(); ?></td>
                        <td><?php echo $form['email_address']->render(); ?><?php echo $form['email_address']->renderError(); ?></td>
                    </tr>
                    
                    
                    <?php
                    
                    
                if (count($attr) > 0) {
                    foreach ($attr as $name => $value) {
                ?> <tr>
                        <tr>
                            <td width="35%" align="right"><?php echo $form['attrForm'][$name]->renderLabel(); ?>

                            <td><?php echo $form['attrForm'][$name]->render(); ?>
<?php echo $form['attrForm'][$name]->renderError(); ?>

                            </td>
                        </tr>   </tr>
<?php
                    }
                }
?>
      <?php
                        /* Hidden Fields to Post to Validator */
                        //echo $form['attribute_master_id'];
                        //echo $form['attribute_master_name'];
                        echo $form['merchant_id'];
                        ?>
                       


                <?php
                        /*
                         * @Saurabh 13-jun-2012
                         * Displaying dynamic fields
                         */
//                        $arr = array();
//                        echo "<pre>";
//                        print_r($dynamicAttFieldName);
//                        
//                         foreach ($dynamicAttFieldName as $k => $v) { echo gettype($v);echo "<br>";}die;
//                        foreach ($dynamicAttFieldName as $k => $v) {
//                            if (!is_string($v) && !is_bool($v)) {
//                          
//                                foreach ($v as $key => $value) {
//                                    if ($key == 'att_name')
//                                        $arr[$k]['att_name'] = $value;
//                                    if ($key == 'att_label')
//                                        $arr[$k]['att_label'] = $value;
//                                    if ($key == 'att_type')
//                                        $arr[$k]['att_type'] = $value;
//                                    if ($key == 'att_is_mandatory')
//                                        $arr[$k]['att_is_mandatory'] = $value;
//                                    if ($key == 'is_active')
//                                        $arr[$k]['is_active'] = $value;
//                                    if ($key == 'att_name')
//                                        $arr[$k]['att_name'] = $value;
//                                }
//                            }
//                        }
//                        foreach ($arr as $va) {
//                            $va = array_reverse($va);   
//                            foreach ($va as $k => $v) { 
//                                if ($k == 'att_label') { $label = $v;}
//                                $v = str_replace(" ", "", $v);
//                                if ($k == 'att_name') {
//                ?>
<!--                                    <tr>
                                        <td width="35%" align="right">//<?php //echo $form[$v]->renderLabel(); ?></td>
                                        <td><?php //echo $form[$v]->render(); ?><?php //echo $form[$v]->renderError(); ?></td>
                                    </tr>-->
                <?php
//                                }
//                            }
//                        }
                ?>

<?php echo $form['_csrf_token']; ?>
                <tr>
                    <td width="35%" align="right"></td>
                    <td><input type="submit" name="save" class="blueBtn" value="Save"/>
                        <?php if (isset($_SESSION['logedin_user_group']) && $_SESSION['logedin_user_group'] == 'teller') { // added by saurabh on 13 July ?>                        
                            <input type="button" name="cancel" id="cancel_button"  class="blueBtn" value="Cancel" onclick="javascript: window.location='<?php echo url_for('custMgt/listingForTeller')?>'"/>
                        <?php }else { ?>                        
                            <input type="button" name="cancel" id="cancel_button"  class="blueBtn" value="Cancel" onclick="javascript: window.location='<?php echo url_for('custMgt/index')?>'"/>
                        <?php } ?>                        
                    </td>
                    
                </tr>
            </table>
        </div>
    </form>
    <p>&nbsp;</p>
</div>