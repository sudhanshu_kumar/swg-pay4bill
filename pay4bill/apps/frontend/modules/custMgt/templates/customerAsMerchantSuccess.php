<?php use_helper('ePortal') ?>
<script>
    $(document).ready(function(){
        $('#Settings').addClass('active');
    });
</script>
<div id="pgMenu">
    <div id="wrappernew">
        <div class="pgTitle">
            <h2>Settings</h2>
        </div>
        <div class="pgNav">
            <ul>
                <li class=""><a href="<?php echo url_for('userAdmin/changePassword') ?>">Change Password</a></li>
                <li class="current"><a href="<?php echo url_for('custMgt/customerAsMerchant') ?>">Customer as a Merchant</a></li>
                <li><a href="<?php echo url_for('userAdmin/viewDetail') ?>">View Details</a></li>
            </ul>
        </div>
    </div>
</div>
<?php if (!empty($msg)) {
    ?><div class="clear_new"></div> <?php } ?>
<div id="mainnew">
    <?php $sf = sfContext::getInstance()->getUser();
    if($sf->hasFlash('notice')){
        ?><div id="flash_notice" class="msg_serverside" ><?php
        echo nl2br($sf->getFlash('notice'));

        ?></div><?php } elseif($sf->hasFlash('error')) {?>
    <div id="flash_notice" class="error_serverside" ><?php
        echo nl2br($sf->getFlash('error'));

        ?></div>
    <?php } ?>
    <span id="success_msg"></span>
    <div class="clear_new"></div>
    <div id="wrappernew">
        <div id="content"><div id="Formwraper">
                <h3>Customer As Merchant</h3>
                <form name="upload_file" action="<?php echo url_for('custMgt/customerAsMerchant'); ?>" method="post" enctype="multipart/form-data">   <div class="FormRepeatArea">
                        <?php if($allreadyMerchant){ ?>
                        <!-- Varun : new function added for merchant details-->
                        <div class="hyperlink"><?php echo link_to('Set Customer as a Merchant', 'custMgt/merchantDetails?var='.cryptString::encrypt(1), 'popup=false') ?></div>
                        <?php } else { ?>
                        <div class="hyperlink"><?php echo link_to('Unset Customer as a Merchant', 'custMgt/customerAsMerchant?var='.cryptString::encrypt(2), 'popup=false') ?></div>
                        <?php } ?>
                        <div class="clear_new"></div>
                    </div>
                </form>
            </div>
</div></div></div>