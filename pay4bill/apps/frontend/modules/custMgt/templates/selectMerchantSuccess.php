<script>
    $(document).ready(function(){
        $('#Customers').addClass('active');
        $(':submit').click(
        function(){
            if($('#merchant_id').val() == ''){
                $('#merchantErr').html('&nbsp;Please select the merchant');
                return false;
            }            
        }
    )
    });



</script>
<?php
include_partial('subHeaderForTeller', array('addClassTo' => 'new', 'adminLogin' => ''));
// if(isset($addClassTo)) {   include_partial('subHeader',array('addClassTo'=>$addClassTo,'adminLogin'=>''));  } else { include_partial('subHeader',array('addClassTo'=>'new','adminLogin'=>'')); } ?>
<div id="Formwraper">
    <div>
        <h3>Customer's Information</h3>
    </div>
    <form  action="<?php echo url_for('custMgt/new') ?>" method="post" class="dlForm">

        <div class="TableForm">
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="35%" align="right"><?php if (count($merchantObj) > 0) { ?>Select Merchant<?php } ?></td>
                    <td>
                        <?php if (count($merchantObj) > 0) {
 ?>
                            <select name="merchant_id" id="merchant_id">
                                <option value="">--Please select--</option>
<?php foreach ($merchantObj as $val) { ?>
                                <option value="<?php echo $val->getMerchantProfileId() ?>"><?php echo $val->getMerchantName() ?></option>
<?php } ?>
                        </select>
                        <div style="float:left" class="error_list" id="merchantErr"></div>
<?php } else
                            echo "<b>No Merchant is mapped for this teller.</b>"; ?>
                    </td>
                </tr>
<?php if (count($merchantObj) > 0) { ?><tr>
                                <td colspan="2" style="padding-left:350px"><input type="submit" class="blueBtn" value="Continue"></td>
                            </tr>
<?php } ?>
            </table>
        </div>
    </form>
    <p>&nbsp;</p>
</div>
