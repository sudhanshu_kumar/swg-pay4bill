<script type="text/javascript">
    $(document).ready(function(){
        $("#<?php echo $form->getName(); ?>").validate({
            rules: {
                '<?php echo $form->getName(); ?>[username]': {
                    required: true

                }
                , '<?php echo $form->getName(); ?>[password]': {
                    required: true

                }
                , '<?php echo $form->getName(); ?>[email_address]': {
                    email: true,
                    required: true

                }
                , '<?php echo $form->getName(); ?>[confirm_password]': {
                    required: true

                }
            },

            messages: {
                '<?php echo $form->getName(); ?>[username]':
                    {
                    required: "Please enter Username"
                },
                '<?php echo $form->getName(); ?>[password]':
                    {
                    required: "Please enter  Password"
                }
                ,
                '<?php echo $form->getName(); ?>[email_address]':
                    {
                    required: "Please enter  Email Address",
                    email: "Please enter valid Email Address"
                }
                ,
                '<?php echo $form->getName(); ?>[confirm_password]':
                    {
                    required: "Please confirm Password"
                }

            }


        })
    }
);
</script>
<style>.error_list{width:auto;}</style>
<div id="Formwraper1">
    <!--div><h3>Login Credentials</h3></div-->
    <div id="pgMenu">
        <div id="wrappernew">
            <div class="pgTitle"><h3>Login Credentials</h3></div>

        </div>
    </div>
    <div style="clear:both"></div>
    <?php
    $sf = sfContext::getInstance()->getUser();
    if ($sf->hasFlash('error')) {
    ?><div id="flash_notice" class="error_serverside" ><?php
        echo nl2br($sf->getFlash('error'));
    ?>
        </div>
        <div style="clear:both"></div>
    <?php
    }
    if ($sf->hasFlash('notice')) {
    ?><div id="flash_notice" class="msg_serverside" ><?php
        echo nl2br($sf->getFlash('notice'));
    ?>
        </div>
        <div style="clear:both"></div>
    <?php
    }
    if (!$userCreated) {
    ?>
        <form  id="<?php echo $form->getName(); ?>" action="<?php echo url_for('custMgt/setUsernamePassword'); ?>" method="post" class="dlForm"  >


            <div class="TableForm">
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="35%" align="right"><?php echo $form['username']->renderLabel(); ?></td>
                        <td><?php echo $form['username']->render(); ?><span style="font-size:9px;float:left">(small letters only)</span><?php echo $form['username']->renderError(); ?></td>
                    </tr>
                    <tr>
                        <td width="35%" align="right"><?php echo $form['email_address']->renderLabel(); ?></td>
                        <td><?php echo $form['email_address']->render(); ?><?php echo $form['email_address']->renderError(); ?></td>
                    </tr>
                    <tr>
                        <td width="35%" align="right"><?php echo $form['password']->renderLabel(); ?></td>
                        <td><?php echo $form['password']->render(); ?><?php echo $form['password']->renderError(); ?></td>
                    </tr>
                    <tr>
                        <td width="35%" align="right"><?php echo $form['confirm_password']->renderLabel(); ?></td>
                        <td>  <?php echo $form['confirm_password']->render(); ?><?php echo $form['confirm_password']->renderError(); ?></td>


                    </tr>

                <?php echo $form['_csrf_token']; ?>
                <?php echo $form['customerId']->render(); ?>
                <tr>
                    <td width="35%" align="right"></td>
                    <td><input type="submit" class="blueBtn" value="Submit" onclick="$('#flash_notice').hide();$('span[class=error_list]').html('')"/>

                        <input type="button" name="cancel" id="cancel_button"  class="blueBtn" value="Cancel" onclick="javascript: window.location='<?php echo url_for('@online_customer_registration_link') ?>'"/>

                    </td>

                </tr>
            </table>
        </div>
    </form>
    <p>&nbsp;</p>
    <?php
            } else {
    ?>

                <div class="TableForm">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="35%" align="right"><b>Customer Id</b></td>
                            <td><?php echo $form['customerId']->getValue(); ?></td>
                        </tr>
                        <tr>
                            <td width="35%" align="right"><b>Username</b></td>
                            <td><?php echo $form['username']->getValue(); ?></td>
                        </tr>
                        <tr>
                            <td width="35%" align="right"><b>Email Address</b></td>
                            <td><?php echo $form['email_address']->getValue(); ?></td>
                        </tr>

            <?php echo $form['customerId']->render(); ?>
    <!--                <tr>
                        <td width="35%" align="right"></td>
                        <td>

                            <input type="button" name="cancel" id="cancel_button"  class="blueBtn" value="Go to Login Page" onclick="javascript: window.location='<?php //echo url_for('@homepage')  ?>'"/>

                        </td>

                    </tr>-->
            </table>
        </div>

    <?php
            }
    ?>

</div>