<div id="pgMenu">
    <div id="wrappernew" >
        <div class="pgTitle"><?php if($addClassTo==''){ ?><h2>Edit Customers</h2><?php } ?>
            <?php if($addClassTo=='index'){ ?><h2>View Customers</h2><?php } ?>
            <?php if($addClassTo=='new'){ ?><h2>Add Customer</h2><?php } ?>
            </div>
            <div class="pgNav">
                <ul>
                    <li <?php if ($addClassTo == 'index') { ?>class="current" <?php  } ?>><a href="<?php echo url_for('custMgt/listingForTeller') ?>">View Customers</a></li>
                    <li <?php if ($addClassTo == 'new') { ?>class="current" <?php } ?>><a href="<?php echo url_for('custMgt/customerNew') ?>">Create Customer</a></li>
                </ul>
            </div>
        </div></div>
