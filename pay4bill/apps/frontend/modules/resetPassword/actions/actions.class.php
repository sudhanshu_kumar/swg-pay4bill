<?php

/**
 * resetPassword actions.
 *
 * @package    sec
 * @subpackage resetPassword
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class resetPasswordActions extends sfActions {

    public function executeIndex(sfWebRequest $request) {
        $this->form = new ResetUserForm();
        if ($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT)) {
            $this->form->bind($request->getParameter($this->form->getName()));

            if ($this->form->isValid()) {
                $userData = $request->getParameter('sf_guard_user');
                $result = Doctrine_Query::create()
                                ->select('*')
                                ->from('SfGuardUser')
                                ->andWhere('email_address=?', $userData['username'])
                                ->andWhere('is_active=?', 1)
                                ->execute();
                $resultData = $result->toArray();

                if ($resultData) {
                    $password = substr(md5(rand(100000, 999999)), 0, 8);

                    $userId = $resultData ['0']['id'];
                    $email = $resultData ['0']['email_address'];
                    $name = $resultData ['0']['username'];
                    $userData = array('name' => $name, 'email' => $email, 'pwd' => $password);

                    $this->getContext()->getConfiguration()->loadHelpers('crumb');
                    sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
                    $taskId = EpjobsContext::getInstance()->addJob('sendMail', $this->moduleName . "/sendEmail", array('name' => $name, 'email' => $email, 'pwd' => $password, 'img' => returnLogoPath(),'image_tag'=>public_path(sfConfig::get('app_logo_path'),true)), '-1', 'frontend');
                    $this->logMessage("sceduled mail job with id: $taskId", 'debug');
                    $user = Doctrine::getTable('sfGuardUser')->find($userId);
                    $user->setPassword($password);
                    $user->setLastLogin(null);
                    $user->save();
                    //$this->successMsg = "Password has been reset. Please check your email for new password";
                    $this->successMsg = "Password has been reset. Please check the email sent to your registered email.";
                    $this->getUser()->setFlash("successMsg", $this->successMsg);
                    $this->redirect("resetPassword/index");
//                    $this->getUser()->setFlash('notice', "Password has been reset. Please check your email for new password.", false);
                } else {
                    //$this->errorMsg = "Invalid Email Address, doesn't match with our records.";
                    $this->errorMsg = "Please enter correct email address.";
//                    $this->getUser()->setFlash('notice', "Incorrect details entered, please try again.", true);
                }
            }
        }
    }

    public function executeSendEmail(sfWebRequest $request) {

        $subject = " Your new password for pay4bill portal";
        $cmo_reg_id = $request->getParameter('cmo_reg_id');
        $img = $request->getParameter('img');
        $name = $request->getParameter('name');
        $email = $request->getParameter("email");
        $pwd = $request->getParameter("pwd");
        $partialName = 'sendEmail';
        /*
         * Mail firing code starts */
        $mailTo = $email;
        $mailToGroup = array();

        $mailFrom = sfConfig::get('app_sender');
        $signature = sfConfig::get('app_sender');

        $partialVars = array('path'=>  $request->getParameter('image_tag'),'username' => $name, 'password' => $pwd, 'img' => $img);

        $mailingOptions = array('mailFrom' => $mailFrom, 'mailTo' => $mailTo, 'mailSubject' => $subject);

        $mailObj = new Mailing();
        $mailInfo = $mailObj->sendMail($partialName, $partialVars, $mailingOptions);

        return $this->renderText($mailInfo);
    }

}
