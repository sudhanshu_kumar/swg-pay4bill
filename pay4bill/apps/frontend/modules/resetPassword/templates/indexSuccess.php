<?php include_stylesheets_for_form($form) ?>
<?php //include_javascripts_for_form($form) ?>
<?php  use_helper('Form'); ?>
<script>
    $(document).ready(function(){
       
                $('form').attr('id','division_frm');
        
                $('#sf_guard_user_username').keypress(
                    function(){ $('.msg_serverside').fadeOut(1000);}
                )

        <?php /*if(isset($successMsg)){?>
                $('#success_msg').html('<?php echo $successMsg; ?>');
                 $('#success_msg').addClass('msg_serverside');
                $('#sf_guard_user_username').keypress(
                    function(){ $('#success_msg').fadeOut(1000);}
                )
           <?php } else */
            
            if(isset($errorMsg)){
            ?>
            $('#msg').html('<?php echo $errorMsg; ?>');
            $('#msg').addClass('error');
           <?php
            }/*else{
           ?>
                    $('#success_msg').html();
                    $('#success_msg').removeClass('msg_serverside');
           <?php
                   }*/
           ?>

        $.validator.addMethod("email", function(value, element){
            $('#msg').html('');
            return this.optional(element) || /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i.test(value);
        }, "Please enter a valid email address");

        $("#division_frm").validate({
            rules: {
                'sf_guard_user[username]': {
                    required: true,
                    email: true
                }
            },
            messages: {
                'sf_guard_user[username]':
                    {
                    required: "Email Address is Required"
                }
            }
        })
    });

</script>

<div id="Formwraper">
  <form action="<?php echo url_for('@sf_guard_forgot_password') ?>" method="post">
    <?php echo $form->renderHiddenFields(false) ?>
      <h3>Forgot your password</h3>

    <?php echo $form->renderGlobalErrors() ?> 
     
    <?php 
            if(sfContext::getInstance()->getUser()->hasFlash('successMsg')){
                    echo '<span class="msg_serverside">'.sfContext::getInstance()->getUser()->getFlash('successMsg').'</span>';
            }
    ?>  
      
    <div class="clear_new"></div>
	<span id="success_msg"></span>
     <div class="FormRepeatArea">	
    <div class="FormLabel"><?php echo $form['username']->renderLabel() ?></div>
    <div class="FromInputLabe2"><?php echo $form['username'] ?><span id="msg"></span><?php echo $form['username']->renderError() ?></div>
    
    <div class="clear_new"></div>
    </div>
    
    <div class="FormRepeatArea">
    <div class="btn_section">
      <input name="submit" type="submit" class="submit"  value="Reset Password"  />
    </div>
    </div>
  </form>
</div>
<b>NOTE:-</b><span class="spacer big-text">Enter email Address registered on pay4bill Portal
    </span>