<?php

/**
 * mypages actions.
 *
 * @package    Mypages
 * @subpackage mypages
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class mypagesActions extends sfActions {

    /**
     * Executes index action
     *
     * @param sfRequest $request A request object
     */
    public function executeIndex(sfWebRequest $request) {
//	$this->addClassTo="learn_more";
        $this->getRequest()->setAttribute('addClassTo', 'learn_more');
    }

    public function executeShowPermissions(sfWebRequest $request) {

//$q = Doctrine::getTable('CustomerProfile')
//  ->createQuery('p')
//  ->select('p.customer_id,p.pin');
//
//$view = new Doctrine_View($q, 'test_vkkiew');
//try {
//    $view->create();
//} catch (Exception $e) {
//
////    echo "<pre>";
////    print_r($e->getMessage());
//    print_r('asdf');
////    exit;
//
//}
//echo "<pre>";
//print_r($q->execute()->toArray());
//exit;
////$view->create();
//echo "<pre>";
//print_r('jgffgh');
//exit;

        $data = Doctrine::getTable('Invoice')->getInvoiceForDashBoard($this->getUser()->getGuardUser()->getMerchantUser()->getId());
        $this->data = $data->toArray();


        // @Saurabh on 13 July 2012: To get the name of the downloaded file i.e., cus_sample_csv.csv
        $this->sample_link_file_name_customer = pay4billHelper::executeDownloadCustomerSample($this->getUser()->getGuardUser()->MerchantUser->getId(), array());

        $string_for_dashboard = '[';
        $labels = '';
        foreach ($data as $key => $value) {
            if ($string_for_dashboard === '[') {

                $string_for_dashboard.="[" . ($key + 1) . "," . $value['sum'] . "]";
                $labels = ($key + 1) . "," . date("d-M-y", strtotime($value['payment_date']));
            } else {
                $string_for_dashboard.=",[" . ($key + 1) . "," . $value['sum'] . "]";

                $labels.=";" . ($key + 1) . "," . date("d-M-y", strtotime($value['payment_date']));
            }
        }

        $string_for_dashboard = $string_for_dashboard . "]";
        $this->labels = $labels;
        $this->string_for_dashboard = $string_for_dashboard;

        $data = Doctrine::getTable('MerchantProfile')->find($this->getUser()->getGuardUser()->MerchantUser->getId());
        if ($data->getPaymentType() == 'o') {
            $this->sample_link_file_name = 'invoice_sample_online.csv';
            $this->sample_link_file_name_xls = 'invoice_sample_online.xls';
        } else if ($data->getPaymentType() == 'b') {
            $this->sample_link_file_name = 'invoice_sample_biller.csv';
            $this->sample_link_file_name_xls = 'invoice_sample_biller.xls';
        }
        $this->setLayout('permissionsLayout');
    }

    public function executeFeatures(sfWebRequest $request) {
        $this->getRequest()->setAttribute('addClassTo', 'features');
    }

    public function executeFeedback(sfWebRequest $request) {
        $this->getRequest()->setAttribute('addClassTo', 'feedback');
    }

    public function executeSendFeedbackMail(sfWebRequest $request) {
        $this->getRequest()->setAttribute('addClassTo', 'feedback');
        if ($request->isMethod('post')) {
            sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
            $name = $request->getParameter('name');
            $email = $request->getParameter('email');
            $contact_number = $request->getParameter('contact_number');
            $subject = $request->getParameter('subject');
            $comments = $request->getParameter('comments');
            $taskId = EpjobsContext::getInstance()->addJob('SendMailNotification', $this->moduleName . "/sendFeedback", array('name' => $name, 'email' => $email, 'contact_number' => $contact_number, 'subject' => $subject, 'comments' => $comments, 'partial_name' => 'sendFeedback', 'image_tag' => public_path(sfConfig::get('app_logo_path'), true)), '-1', 'frontend');
            $this->logMessage("scheduled mail job with id: $taskId", 'debug');
            $this->getUser()->setFlash('notice', 'Thanks for your feedback.', true);
            $this->redirect('mypages/feedback');
        }
    }

    public function executeSendFeedback(sfWebRequest $request) {

        $contactNumber = $request->getParameter('contact_number');
        if ($contactNumber == '')
            $contactNumber = 'N/A';
        $email = $request->getParameter('email');
        $name = $request->getParameter('name');
        $comments = $request->getParameter('comments');
        $subject = $request->getParameter('subject');
        $image_tag = $request->getParameter('image_tag');
        $partialName = $request->getParameter('partial_name');
        $partialVars = array('path' => $image_tag, 'name' => $name, 'email' => $email, 'contact_number' => $contactNumber, 'subject' => $subject, 'comments' => $comments);

        /* Mail firing code starts */
        $mailFrom = sfConfig::get('app_sender');
        $signature = sfConfig::get('app_sender');
        $mailTo = sfConfig::get('app_to');

        $mailingOptions = array('mailFrom' => $mailFrom, 'mailTo' => $mailTo, 'mailSubject' => 'Pay4bill Feedback');

        $mailObj = new Mailing();
        $mailInfo = $mailObj->sendMail($partialName, $partialVars, $mailingOptions);
        return $this->renderText($mailInfo);
    }

    public function executeServices(sfWebRequest $request) {
        $this->getRequest()->setAttribute('addClassTo', 'services');
    }

    public function executeFaqs(sfWebRequest $request) {
        $this->getRequest()->setAttribute('addClassTo', 'faqs');
    }

    public function executeAboutus(sfWebRequest $request) {

        $this->getRequest()->setAttribute('addClassTo', 'aboutus');
        $this->setLayout('permissionsLayout');
    }

    public function executeFeaturesAfterLogin(sfWebRequest $request) {
        $this->getRequest()->setAttribute('addClassTo', 'featuresAfterLogin');
        $this->setLayout('permissionsLayout');
    }

    public function executeFaqsAfterLogin(sfWebRequest $request) {
        $this->getRequest()->setAttribute('addClassTo', 'faqsAfterLogin');
        $this->setLayout('permissionsLayout');
    }

    public function executeSupport(sfWebRequest $request) {
        $this->getRequest()->setAttribute('addClassTo', 'support');

        $this->setLayout('permissionsLayout');
    }

}
