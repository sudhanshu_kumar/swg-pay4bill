<div id="wrapperInnerLeft">

    <div class="subnavShowcase">
        <div class="top"></div>
        <div class="middle">
            <div>
                <h3>Services</h3>
            </div>
        </div>
        <div class="bottom"></div>

    </div>
</div>
<div id="wrapperInnerRight">
    <div id="innerImg">
    <?php echo image_tag('img_services.jpg',array('alt'=>"Features","border"=>"0" ,"height"=>"242","width"=>"600" )); ?>
    </div>
    <div>
        <div id="content">
        <p>&nbsp;</p>
            <p><h3>Our Services</h3>
                <ul class="bullet_list">

                    <li>Recurring Bill Presentment</li>

                    <li>One-time Bill Presentment</li>

                    <li>Custom Bill/Invoicing Management Solutions</li>

                    <li>Payment Collection through our Payment Aggregation Platform</li>

                    <li>Payment Tracing</li>
                    
                    <li>Bill Archiving and Customizable organization</li>


                    <li>Wide-reaching payment network</li>
            </ul></p>
            <p>&nbsp;</p>
    </div>      </div>
    <div id="slideControl"></div>
    <div class="clearfix"></div>
</div>
<div id="wrapMid">

    <div class="clearfix"></div>
</div>
<!--<div id="curvebottom"></div>-->

