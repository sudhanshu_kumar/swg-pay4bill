<div id="wrapperInnerLeft">
    <div class="subnavShowcase">
        <div class="top"></div>
        <div class="middle">
            <div>
                <h3>FAQ</h3>
            </div>
        </div>
        <div class="bottom"></div>

    </div>
</div>
<div id="wrapperInnerRight">
    <div id="innerImg">
    <?php echo image_tag('img_faqs.jpg',array('alt'=>"Features","border"=>"0" ,"height"=>"242","width"=>"600" )); ?>
    </div>
    <div>
        <div id="content">
        <p>&nbsp;</p>
            <p><h3>FAQ</h3>
                <p>

                    <b>Can I use any kind of debit or credit card?</b><br>
                    Currently you may only use Visa, VPay, Mastercard, E-Transact, and interswitch. Pay4Me  will update users of its service when other cards become available.
                </p>
<br />


                <p>
                    <b>I can't login</b><br>
                    If you cant login it usually due to an incorrect username name, password, or downtime with your internet service provider. Please check the spelling of your username and password and ensure you are properly connected to the internet. If you can't remember your username or password click one of the following two links below to begin recovery.
                </p>
<br />
                <p>
                    <b>I don't remember the email address I registered </b><br>

                    This option will require you to put in (one) two alternate email addresses you registered with pay4bill while you were signing up. Your new temporary login details will be sent to (this) these email address(es). Please change your password to something more memorable once you login.
                </p>
<br />
                <p>
                    <b>I don't remember my password.</b><br>
                    If you don't remember your password you will be asked to input the email address you used to sign up.  Your new password will be sent to this email address. Please change your password to something more memorable once you login.
                </p>
<br />
                <p>
                    <b>My account has been compromised - what should I do?</b><br>
                    Report this to Pay4Bill at support@pay4bill.com - It is important to keep your details safe so no one can enter your account. Pay4bill are not responsible for any bills paid on your behalf my anonymous strangers.
                </p>

<br />
                <p>
                    <b>Am I protected from scams and identity frauds?</b><br />

                    Sure, pay4bill protects you from scams and all identity frauds so far as we have built-in alerts that inform you when there is a identity fraud theft threat You will forward any email that may be fraudulent to us at <br/>support@pay4bill.com We have put in place fraud prevention tools that track all transactions related to your <br/> e-Wallet/ bank transactions making it unlikely  for your account to be accessed unless you share your login details.
            </p></p>

    </div>      </div>
    <div id="slideControl"></div>
    <div class="clearfix"></div>
</div>



