<?php
$sf = sfContext::getInstance()->getUser();
if ($sf->hasFlash('notice')) {
?><div id="flash_notice" class="error_serverside" ><?php
    echo nl2br($sf->getFlash('notice'));
?></div><?php } ?>
<div id="pgMenu">
    <div id="wrappernew">
        <div class="pgTitle">
            <h2>Dashboard</h2>
            
        </div>
        <!--<div class="pgNav"> </div>-->
    </div>
</div>

<div id="mainnew">
    <div id="wrappernew">
        <div id="content"><div id="Formwraper">
                <h3>Report Analysis</h3>
                <div class="reportGraphHolder">
                    <div id="placeholder"  class="reportGraph"></div></div>
                <script type="text/javascript" src="<?php echo _compute_public_path("excanvas" . '.js', 'js', '', true); ?>"></script>

                <script id="source" language="javascript" type="text/javascript">
                    $(function () {
                        var monthName_labels=new Array();
<?php
    $eachArray = explode(";", $labels);
    if (!empty($eachArray)) {
        foreach ($eachArray as $no_with_label) {
            $tempArray = explode(',', $no_with_label);
            if ($tempArray[0] != '') {
?>
                    var key_with_label=new Array(<?php echo $tempArray[0]; ?>,"<?php echo $tempArray[1]; ?>");
                    monthName_labels.push(key_with_label);
<?php
            }
        }
    }
?>
        var d1=<?php echo $string_for_dashboard; ?>;
              

        $.plot($("#placeholder"), [d1],monthName_labels);
    });
                </script>
                <div class="dashCol_new">
                    <h4><span class="imgHolder"><?php echo image_tag('invoices.png', array('')); ?></span>Invoices</h4>

                    <ul>
                        <li><a href="<?php echo url_for('invoice/index?offset=0'); ?>">View Invoices</a></li>
                        <li><a href="<?php echo url_for('invoice/new'); ?>">Create Invoice</a></li>
                        <li><a href="<?php echo url_for('invoice/upload'); ?>">Upload Invoices</a></li>

                    </ul>
                </div>
                <div class="dashCol_new">
                    <h4><span class="imgHolder"><?php echo image_tag('custmers.png', array('')); ?></span>Customers</h4>

                    <ul>
                        <li><a href="<?php echo url_for('custMgt/index?offset=0'); ?>">View Customers</a></li>
                        <li><a href="<?php echo url_for('custMgt/new'); ?>">Create Customer</a></li>
                        <li><a href="<?php echo url_for('custMgt/upload'); ?>">Upload Customers</a></li>
                    </ul>
                </div>
                <div class="dashCol_new">

                    <h4><span class="imgHolder"><?php echo image_tag('settings.png', array('')); ?></span>Settings</h4>
                    <ul>
                        <li><a href="<?php echo url_for('userAdmin/changePassword'); ?>">Change Password</a></li>
                        <li><a href="<?php echo url_for('merchantRegistration/uploadMerchantLogo'); ?>">Upload Merchant Logo</a></li>
                        <li><a href="<?php echo url_for('userAdmin/viewBank'); ?>">View Bank</a></li>
                        <li><a href="<?php echo url_for('@online_customer_registration_link'); ?>">Add Customer Profile</a></li>
                        <li><a href="<?php echo url_for('userAdmin/viewDetail'); ?>">View Details</a></li>

<!--                        <li class=""><a href="<?php //echo url_for('merchantRegistration/merchantAsCustomer')   ?>">Merchant as a Customer</a></li>-->
                    </ul>
                </div>
                <div class="dashCol_new mrgR0">
                    <h4><span class="imgHolder"><?php echo image_tag('faQ.png', array('')); ?></span>Downloads</h4>

                    <ul>
                        <li><a href="#" onclick="window.open('<?php echo _compute_public_path("gettingstartedguide" . '.zip', 'download', '', true); ?>');return false;">User Guides</a></li>
                        <li><a href="#" onclick="window.open('<?php echo _compute_public_path($sample_link_file_name_customer, 'uploads/assets', '', true); ?>');return false;">Sample Customer CSV file</a></li>
                        <li><a href="#" onclick="window.open('<?php echo _compute_public_path($sample_link_file_name, 'download', '', true); ?>');return false;">Sample Invoice CSV File</a></li>
                        <li><a href="#" onclick="window.open('<?php echo _compute_public_path($sample_link_file_name_xls, 'download', '', true); ?>');return false;">Sample Invoice XLS File</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>