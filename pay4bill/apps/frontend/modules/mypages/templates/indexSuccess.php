<div id="wrapperInnerLeft">
    <div class="subnavShowcase">
        <div class="top"></div>
        <div class="middle">
            <div>
                <h3>About pay4bill</h3>
            </div>
        </div>
        <div class="bottom"></div>

    </div>
</div>
<div id="wrapperInnerRight">
    <div id="innerImg">
        <?php echo image_tag('img_about_pay4bill.jpg',array('alt'=>"Privacy Policy","border"=>"0" ,"height"=>"242","width"=>"600" )); ?>
    </div>
    <div>
        <div id="content">
        <p>&nbsp;</p>
            <p><h3>About the company</h3>

                <p>pay4bill is a Billing Solution that enables companies and institutions to present bills to their customers in a simplified and transparent manner. The solution allows customers to make payments on the presented bills through methods that assure that payment is validated and the company and/or institution receives its funds.</p>

                <br>
            <p>The pay4bill service provides customers with more payment options and allows companies and institutions to focus on core business rather than worry about billing and payment collection.</p></p>
            <p>&nbsp;</p>
    </div>      </div>

</div>












