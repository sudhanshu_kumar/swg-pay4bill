<div id="wrapperInnerLeft">

    <div class="subnavShowcase">
        <div class="top"></div>
        <div class="middle">
            <div>
                <h3>Features</h3>
            </div>
        </div>
        <div class="bottom"></div>

    </div>
</div>
<div id="wrapperInnerRight">
 <div id="innerImg">
<?php echo image_tag('img_features.jpg',array('alt'=>"Features","border"=>"0" ,"height"=>"242","width"=>"600" )); ?>
</div>
    <div>
        <div id="content">
            <p><h3>Features of our services</h3>
                <p>

                    <b>Pay4Me @Bank services</b>
                    <br>
                    A convenient multi-bank/multi-branch payment network that opens up over 2500 payment points to you; easing your long journeys to a particular bank for a particular merchant. Pay4Me sure is a bank branch near you.<br><br>
                    Here are the list of participating banks<br>
                    <ul class="bullet_list">
                        <li>First Bank</li>
                        <li>Union Bank</li>

                        <li>Sterling Bank</li>
                        <li>Intercontinental Bank</li>
                        <li>UBA</li>
                        <li>Skye Bank</li>
                        <li>Afribank</li>
                        <li>Bank PHB</li>

                        <li>Zenith</li>
                        <li>Oceanic Bank</li>
                        <li>Fidelity Bank</li>
                    </ul>
                </p>
                <br>
                <p>

                    <b>Pay4Me e-Wallet</b>
                    <br>
                    Take a trip to the next frontier in online shopping and bill payment experience by running your very own e-Wallet - an Online purse accepted by online Merchants. Register and load your e-Wallet at any of our 2,500 payment locations and start your new shopping experience.
                </p>
                <br>
                <p>
                    <b>Debit cards</b>
                    <br>

                    We also accept Nigerian debit cards; Interswitch and e-Tranzact cards if you got their cards. pay4bill will accept all payment types through our payment aggregation service provider, Pay4Me.
            </p></p>
            <p>&nbsp;</p>
    </div>      </div>
    <div id="slideControl"></div>
    <div class="clearfix"></div>
</div>
