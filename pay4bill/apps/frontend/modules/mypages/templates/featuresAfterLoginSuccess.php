<div id="pgMenu">
<div id="wrappernew">
<div class="pgTitle">
<h2>Features</h2>
</div>
<!--<div class="pgNav"> </div>-->
</div>
</div>

<div id="mainnew" >
    <div id="wrappernew"><br>
        <div class="tableHolder">
            <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
                <tbody>

                <tr><td>
                        <p>
                            <b>Pay4Me @Bank services</b>
                            <br>
                            A convenient multi-bank/multi-branch payment network that opens up over 2500 payment points to you; easing your long journeys to a particular bank for a particular merchant. Pay4Me sure is a bank branch near you.
                        </p>
                        <br>
                        <p>
                            <b>Pay4Me e-Wallet</b>
                            <br>
                            Take a trip to the next frontier in online shopping and bill payment experience by running your very own e-Wallet - an Online purse accepted by online Merchants. Register and load your e-Wallet at any of our 2,500 payment locations and start your new shopping experience.
                        </p>
                        <br>
                        <p>
                            <b>Debit cards</b>
                            <br>
                            We also accept Nigerian debit cards; Interswitch and e-Tranzact cards if you got their cards. pay4bill will accept all payment types through our payment aggregation service provider, Pay4Me.
                        </p>
                </td></tr>
                <tbody>
            </table>

        </div>
    </div>
</div>

