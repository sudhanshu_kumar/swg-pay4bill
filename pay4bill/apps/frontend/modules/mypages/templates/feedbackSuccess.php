<script>
    $(document).ready(function(){

        $('input[type=submit]').bind('click' ,  function ()
        {
            $('#flash_notice').fadeOut();
        });
        
        jQuery.validator.addMethod('isValidName', function(value, element) {
            return this.optional(element) || value.match(/^[a-zA-Z ]*$/);
        },'Invalid Name');


        jQuery.validator.addMethod('isValidPhone', function(value, element) {
            return this.optional(element) || value.match(/^[+]{0,1}[0-9-]*$/);
        },'Invalid contact number.');

        
        $("#feedback_frm").validate({
            rules: {
                name: {
                    required: true,
                    maxlength:50,
                    isValidName: true
                },
                email: {
                    required: true,
                    email: true
                },
                contact_number:{
                    required: false,
                    minlength:6,
                    maxlength:14,
                    isValidPhone: true
                },
                subject:{
                    required: true,
                    maxlength:100
                },
                comments:{
                    required: true,
                    maxlength:250
                }
            },
            messages: {
                name: {
                    required: "Name is Required",
                    maxlength: "<br>Maximum limit should be 50 character( Only alphabet, space are allowed).",
                    isValidName: "Only alphabet, space are allowed"
                },
                email: {
                    required: "Email is Required",
                    email:"Valid Email is Required"
                },
                contact_number:{
                    minlength:'Minimum 6 digits',
                    maxlength:'Maximum 20 digits'

                },
                subject: {
                    required :"Subject is Required",
                    maxlength:'Maximum 100 characters'
                },
                comments: {
                    required: "Comments are Required",
                    maxlength:"Maximum 250 characters"
                },
            
                // the errorPlacement has to take the table layout into account
                errorPlacement: function(error, element) {
                    if ( element.is(":radio") )
                        error.appendTo( element.parent().next().next() );
                    else if ( element.is(":checkbox") )
                        error.appendTo ( element.next() );
                    else
                        error.appendTo( element.parent().next() );
                }
        
            } });
    });

</script>

<div id="wrapperInnerLeft">

    <div class="subnavShowcase">
        <div class="top"></div>
        <div class="middle">
            <div>
                <h3>Feedback</h3>
            </div>
        </div>
        <div class="bottom"></div>

    </div>
</div>
<div id="wrapperInnerRight">
    <div>
        <div id="content">
            <form enctype="multipart/form-data" method="post" name="feedback_frm" action="<?php echo url_for('mypages/sendFeedbackMail') ?>" id="feedback_frm">
                <div class="sform">
                    <?php
                    $sf = sfContext::getInstance()->getUser();
                    if ($sf->hasFlash('notice')) {
                    ?><div id="flash_notice" class="msg_serverside" ><?php
                        echo nl2br($sf->getFlash('notice'));
                    ?>
                        </div>
                        <div style="clear:both"></div>
                    <?php } ?>

                    <!-- Added  by SK on 09-07-2012 -->

                    <div class="TableForm">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="30%" align="right"><label>Name <span class="redLbl">*</span> </label></td>
                                <td><input type="text" id="name" name="name" class="FormInput9"></td>
                            </tr>
                            <tr>
                                <td align="right"><label>Email <span class="redLbl">*</span> </label></td>
                                <td><input type="text" id="email" name="email" class="FormInput9"></td>
                            </tr>
                            <tr>
                                <td align="right"><label>Contact Number</label></td>
                                <td><input type="text" id="contact_number" name="contact_number" class="FormInput9">
                                    <div style="bottom:5px; right: 130px;">(e.g: +1234567891)</div></td>
                            </tr>
                            <tr>
                                <td  align="right"><label>Subject <span class="redLbl">*</span> </label></td>
                                <td><input type="text" id="subject" name="subject" class="FormInput9"></td>
                            </tr>
                            <tr>
                                <td align="right"><label>Comments <span class="redLbl">*</span> </label></td>
                                <td><textarea id="comments"  style="height:80px;margin-bottom:10px;width:192px" name="comments" class="FormInput9"></textarea></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td colspan="1"><input type="submit" value="Send" class="submit"></td>
                            </tr>
                        </table>
                    </div>

                    <!--div class="FormRepeatArea">
                        <div class="FormLabel7"> <label>Name <span class="redLbl">*</span> </label> </div>
                        <div class="FromInputLabe7" style="float:none;"> <input type="text" id="name" name="name" class="FormInput7">
                        </div>
                        <div style="float:right;width:65%"></div>
                        <div class="clear_new"></div>
                    </div>
                    <div class="FormRepeatArea">
                        <div class="FormLabel7"> <label>Email <span class="redLbl">*</span> </label> </div>
                        <div class="FromInputLabe7" style="float:none"> <input type="text" id="email" name="email" class="FormInput7"></div>
                        <div class="error" style="float:right;width:65%"></div>
                        <div class="clear_new"></div>
                    </div>
                    <div class="FormRepeatArea">
                        <div class="FormLabel7"> <label>Contact Number</label> </div>
                        <div class="FromInputLabe7" style="float:none"> <input type="text" id="contact_number" name="contact_number" class="FormInput7">
                        </div><div style="position:relative;top: -5px; " >  <div style="position:absolute; bottom:5px; right: 130px; *right: 108px;">(e.g: +1234567891)</div></div>
                        <div class="error" style="float:right;width:65%"></div>
                        <div class="clear_new"></div>
                    </div>
                    <div class="FormRepeatArea">
                        <div class="FormLabel7"> <label>Subject <span class="redLbl">*</span> </label> </div>
                        <div class="FromInputLabe7" style="float:none"> <input type="text" id="subject" name="subject" class="FormInput7"></div>
                        <div class="error" style="float:right;width:65%"></div>
                        <div class="clear_new"></div>
                    </div>
                    <div class="FormRepeatArea">
                        <div class="FormLabel7"> <label>Comments <span class="redLbl">*</span> </label> </div>
                        <div class="FromInputLabe7" style="float:none"> <textarea id="comments"  style="height:30px;margin-bottom:10px;width:250px" name="comments" class="FormInput2"></textarea></div>
                        <div class="error" style="float:right;width:65%"></div>
                        <div class="clear_new"></div>
                    </div>
                    <div class="button-position">
                        <input type="submit" value="Send" class="submit">
                    </div-->
                </div>
            </form>
        </div>
    </div>
    <div id="slideControl"></div>
    <div class="clearfix"></div>
</div>
<div id="wrapMid">

    <div class="clearfix"></div>
</div>
<!--<div id="curvebottom"></div>-->

