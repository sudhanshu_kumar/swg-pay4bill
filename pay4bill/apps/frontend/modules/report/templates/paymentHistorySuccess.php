<script language="javascript" >
    $(document).ready(function(){
        populateSearch();
        $('#PaymentHistory').addClass('active');
        $('#<?php echo $class_on ?>').addClass('on');
    });


    function resetForm(){
        document.location.href = '<?php echo url_for('report/resetForSearchForm'); ?>';
        //     $('#invoice_no').val('');
        //     $('#invoice_number').hide();
        //     $('#search_par').val('');
        //     $('#com_name').val('');
        //     $('#company_name').hide();

    }

    function validateSearch()
    {
        var err  = 0;
        var invoice_number = jQuery.trim($('#invoice_no').val());
        var com_name = jQuery.trim($('#com_name').val());
        var regcheck = /^[a-zA-Z0-9 .&]*$/; //Invoice Number  and Customer Name validation

        if(com_name!=''){
            if(com_name.length >50 ) {
                $('#com_name_error').html("<font color='red'>Merchant Name cannot be more than 50 characters.</font>");
                $('#com_name').focus();
                err = err + 1;
            } else {
                $('#com_name_error').html("");
            }
            if(regcheck.test(com_name) == false ) {
                $('#com_name_error').html("<font color='red'>Please enter valid Merchant Name.</font>");
                $('#com_name').focus();
                err = err + 1;
            } else {
                $('#com_name_error').html("");
            }
        } else {
            //            if($('#com_name').is(':visible')){
            //                $('#com_name_error').html("<font color='red'>Please enter Merchant Name.</font>");
            //                $('#com_name').focus();
            //                err = err + 1;
            //            }
        }

        if(invoice_number!=''){
            if(invoice_number.length >10 ) {
                $('#invoice_no_error').html("<font color='red'>Invoice Number cannot be more than 10 characters.</font>");
                $('#invoice_no').focus();
                err = err + 1;
            } else {
                $('#invoice_no_error').html("");
            }
            if(regcheck.test(invoice_number) == false ) {
                $('#invoice_no_error').html("<font color='red'>Please enter valid Invoice Number.</font>");
                $('#invoice_no').focus();
                err = err + 1;
            } else {
                $('#invoice_no_error').html("");
            }
        }else {
            //             if($('#invoice_no').is(':visible')){
            //                $('#invoice_no_error').html("<font color='red'>Please enter Invoice Number.</font>");
            //                $('#invoice_no').focus();
            //                err = err + 1;
            //             }
        }
        if(err>0)return false;
        return true;
    }

    function populateSearch()
    {
        var settings = $('#userForm').validate().settings;
        var company_name = $('#company_name');
        var invoice_number = $('#invoice_number');

        var id = $('#search_par').val();

        //                $('#company_name').hide();
        //                $('#invoice_number').hide();

        if(id == 'invoice'){
            $('#com_name').val('');
            $('#com_name_error').html('');
            //company_name.css("display", "none");
            //invoice_number.css("display", "block");
        } else  if(id == 'company'){
            $('#invoice_no').val('');
            $('#invoice_no_error').html("");
            // company_name.css("display", "block");
            // invoice_number.css("display", "none");
        }else{
            //invoice_number.css("display", "none");
            // company_name.css("display", "none");
        }
    }
</script>



<div id="pgMenu">

    <div id="wrappernew">
        <div id="content">
            <div class="pgTitle">

                <h2>Payment History</h2>
            </div>
            <!--<div class="pgNav">
            </div>-->
        </div></div></div>

<div id="mainnew">

    <div id="wrappernew">
        <div id="content">
            <div id="Formwraper">

                <!--p><h3 style="float:left;">Search Invoice</h3-->
                <h3>Search Invoice</h3>
                <?php //echo $search_par;?>
                <!-- Search starts here -->
                <div class="TableForm" id="search">
                    <form action="<?php echo url_for('report/allPaymentsForCustomer') ?>" method="post" id="userForm" onsubmit="return validateSearch()" >

                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td nowrap>
                                    Customer Id
                                </td>
                                <td width="1">
                                    <input type="text" id="customer_id" name="customer_id"  maxlength="50" value="<?php echo $customer_id ?>">
                                </td>
                                <td width="60%">
                                    <div class="red redlt" id="customer_id_error"></div>
                                </td>
                            </tr>
                            <tr>
                                <td nowrap>
                                    Merchant Name
                                </td>
                                <td width="1">
                                    <input type="text" id="com_name" name="com_name"  maxlength="50" value="<?php echo $com_name ?>">
                                </td>
                                <td width="60%">
                                    <div class="red redlt" id="com_name_error"></div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Invoice Number
                                </td>
                                <td>
                                    <input type="text" id="invoice_no" name="invoice_no" maxlength="10" value="<?php echo $invoice_no ?>">
                                    <div class="clear_new"></div>
                                    <!--select name="search_par" id="search_par" onchange="return populateSearch()">
                                          <option value="">Please Select</option>
                                    <?php /* if(isset($merchnatLogin) && $merchnatLogin){ ?>
                                      <option value="customer" <?php if(isset($_POST['search_par']) && $_POST['search_par']=='customer') { echo "selected"; } else if($search_par=='customer'){ echo "selected"; } else { echo ""; }   ?> >Customer Name</option>
                                      <?php } else { ?>
                                      <option value="company" <?php if(isset($_POST['search_par']) && $_POST['search_par']=='company') { echo "selected"; } else if($search_par=='company'){ echo "selected"; } else { echo ""; }   ?> >Company Name</option>
                                      <?php } */ ?>

                                          <option value="invoice" <?php // if(isset($_POST['search_par']) && $_POST['search_par']=='invoice') { echo "selected"; } else if($search_par=='invoice'){ echo "selected"; } else { echo ""; }      ?> >Invoice Number</option>
                                      </select-->
                                </td>
                                <td>
                                    <div class="red redlt" id="invoice_no_error"></div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" class="Errormrg">
                                    <div style="clear:left;" class="Errormrg">
                                        <input type="submit" value="Search" class="blueBtn" >
                                        <input type="button" class="blueBtn" value="Reset" onClick="resetForm()"/>
                                    </div>
                                </td>
                            </tr>

                        </table>

                    </form>
                </div>

                <!-- Search ends  here -->
                <div style="line-height:8px;clear:both">&nbsp;</div>
                <?php
                                    $queryString = "customer_id=" . $customer_id . "&invoice_no=" . $invoice_no . "&com_name=" . $com_name;
                ?>
                                    <div class="TableForm">
                                        <table>
                                            <tr>
                                                <td>Search By  :&nbsp;
                                                    <a href="<?php echo url_for('report/allPaymentsForCustomer') . "?" . $queryString; ?>" <?php
                                    if ($class_on == 'allPaymentsForCustomer') {
                                        echo 'style="font-weight:bold";';
                                    }
                ?>>All</a>&nbsp;
                                 | <a href="<?php echo url_for('report/partlyPaidForCustomer') . "?" . $queryString; ?>"<?php
                                    if ($class_on == 'partlyPaidForCustomer') {
                                        echo 'style="font-weight:bold";';
                                    }
                ?>>Partially Paid</a>
                                &nbsp;
                                | <a href="<?php echo url_for('report/fullyPaidInvoicesForCustomer') . "?" . $queryString; ?>"<?php
                                     if ($class_on == 'fullyPaidInvoicesForCustomer') {
                                         echo 'style="font-weight:bold";';
                                     }
                ?>>Paid</a></td>
                        </tr>
                    </table>
                </div>

                <!--div class="searchOption  clearfixnew">
                    <ul>
                        <li>Search By  :</li>
                        <li id="allPaymentsForCustomer"></li>
                        <li id="partlyPaidForCustomer"></li>
                    </ul>
                </div-->

                <div class="TableForm">
                    <table border="0" cellspacing="0" cellpadding="0">
                        <thead>

                        <th>Invoice Number</th>
                        <th>Customer Id</th>
                        <th>Merchant Name</th>
                        <th>Due Date</th>
                        <th>Currency</th>
                        <th>Amount Paid</th>
                        <th>Payment Date</th>
                        <th>Payment Mode</th>

                        <th>Status</th>
<!--                        <th>Action</th>-->
                        </thead>

                        <tbody>
                            <?php
                                     //Varun : Helper added for proper amount format
                                     $helperOb = new csvHelper();
                                     if (count($invoices) > 0) {

                                         $total_amount_to_be_displayed = 0;
                                         $i = 0;
                                         foreach ($invoices as $invoice) {
                                             $i++;
                            ?>
                            <?php if ($i % 2 != 0) {
                            ?>
                                                 <tr class="odd">
                            <?php } else {
 ?><tr class=""> <?php } ?>
                                                 <td><a href="<?php echo url_for('invoice/show?id=' . cryptString::encrypt($invoice->getId()) . '&report=1&hidePaybutton=1'); ?>"><?php echo $invoice->getInvoiceNo(); ?></a></td>
                                                 <td><?php echo $invoice->getCustomer()->getCustomerId(); ?></td>
                                                 <td><?php echo $invoice->Merchant->getMerchantName(); ?></td>
                                                 <td><?php echo $invoice->getDueDate(); ?></td>
                                                 <td><?php echo ucfirst($invoice->getCurrencyType()); ?></td>
                                                 <td align="right"><?php
                                             // correct the amount paid; Santosh Kumar
                                             echo $helperOb->price_format($invoice->getAmountPaid());

                                             $total_amount_to_be_displayed+=$invoice->getAmountPaid();


//
                            ?></td>
                                         <td ><?php
                                             if ($invoice->getPaymentDate() != '') {
                                                 echo $invoice->getPaymentDate();
                                             } else {
                                                 echo 'N/A';
                                             }
                            ?></td>
                                         <td ><?php
                                             if (!is_null($invoice->getPaymentMode())) {
                                                 echo ucfirst($invoice->getPaymentMode());
                                             } else {
                                                 echo "N/A";
                                             }
                            ?></td>

                                         <td><?php
                                             switch ($invoice->getStatus()) {
                                                 case 0:
                                                     if (strtotime($invoice->getDueDate()) > time()) {
                                                         echo image_tag('pending.png', array('alt' => "Pending", 'title' => "Pending"));
                                                     } else {
                                                         echo image_tag('expired.png', array('alt' => "Expired", 'title' => "Expired"));
                                                     }
                                                     break;
                                                 case 1:
                                                     echo image_tag('cancel.png', array('alt' => "Cancelled", 'title' => "Cancelled"));
                                                     break;
                                                 case 2;

                                                     echo image_tag('paid.png', array('alt' => "Paid", 'title' => "Paid"));

                                                     break;
                                                 case 3:

                                                     if (strtotime($invoice->getDueDate()) >= strtotime(date("Y-m-d"))) {
                                                         echo image_tag('partially-paid.png', array('alt' => "Partially Paid", 'title' => "Partially Paid"));
                                                     } else {
                                                         echo image_tag('partiallypaid_expired.png', array('alt' => "PartiallyPaid/Expired", 'title' => "PartiallyPaid/Expired"));
//                                                echo image_tag('expired.png', array('alt' => "Expired", 'title' => "Expired"));
                                                     }

                                                     break;
                                             }
                                             //Varun : Encrypted id
                            ?></td>
         <!--                                    <td><a href="<?php //echo url_for('invoice/show?id=' . cryptString::encrypt($invoice->getId()) . '&report=1');    ?>">View Invoice</a></td>-->
                                     </tr>



                            <?php } ?>
                                         <tr class="border-bottom"><td></td><td></td><td></td><td></td><td align="right"><b>Amount<?php if ($setPagination) {
 ?><span class="sign">+</span><?php } ?></b></td><td align="right" ><b><?php echo $helperOb->price_format($total_amount_to_be_displayed); ?></b><td></td></td><td></td><td></td></tr>
                            <?php
                                     } else {
                            ?><tr><td colspan='9' align='center'>No Invoice Found</td></tr>
<?php } ?>
                                 </tbody>
                             </table>

                         </div>

                         <div class="nofoList">
                    <?php
                                     if (count($invoices) > 0) {
                                         include_partial('global/records_number', array('amount_footer' => true, 'current_offset' => isset($current_offset) ? $current_offset : null, 'offset_next' => isset($offset_next) ? $offset_next : null, 'total_no_of_records' => isset($total_no_of_records) ? $total_no_of_records : count($invoices), 'no_of_records_per_page' => isset($no_of_records_per_page) ? $no_of_records_per_page : null, 'from' => '', 'total_amount_of_all_transactions' => isset($total_amount_of_all_transactions) ? $total_amount_of_all_transactions : $total_amount_to_be_displayed));
                                     } ?></div>
                                 <div align="right">
                    <?php
                                     if ($setPagination) {
                                         include_partial('global/pagination', array('moduleName' => $module, 'actionName' => $actionTobeCalled, 'amount_footer' => true, 'current_offset' => $current_offset, 'offset_next' => $offset_next, 'offset_prev' => $offset_prev, 'offset_last' => $offset_last, 'search' => '', 'com_name' => $com_name, 'invoice_no' => $invoice_no));
                                     }
                    ?></div>

            </div></div>
    </div></div>