<script> 
    function validateSearch(status){
        $("#search_form").validate();
        if(status=='expired'){
            $('<input>').attr({
                type: 'hidden',
                id: 'expired',
                name: 'expired',
                value: '1'
            }).appendTo('#search_form');
            status=0;            
        }
        $('#status').val(status);
        //alert($('#status').val(status));
        displayDateSearch1();
        var err  = 0;
        var invoice_number = jQuery.trim($('#invoice_no').val());
        // var email_address = jQuery.trim($('#email_address').val());
        var cust_name = jQuery.trim($('#cust_name').val());
        if(err>0){
            return false;
        }
        $('#search_form').submit();
    }    
    /**
     * Manu : Search date display in case of page refresh
     */
    function displayDateSearch1(){
        var checked = $('input[type=checkbox]').is(':checked');
        if(checked){
            $('#date_selection').css('display', 'inline');
            $('#invoice_filters_to_day').rules("add", "diff_between_dates");
        }
    }
    jQuery.validator.addMethod('diff_between_dates', function(value, element) {
        if(value!=''){
            if($('#invoice_filters_from_date').val()=='') {
                /**
                 * Manu : Search date display in case of page refresh
                 */
                $("#dispErr_date2").html('Please enter From Date');
                $("#dispErr_date2").addClass('error');
                $('#dispErr_date2').css('display', 'inline');
                $("#dispErr_date3").html('');
                $("#dispErr_date3").addClass('error');
                $('#dispErr_date3').css('display', 'inline');
                return false;
            } else{
                var month=$("#invoice_filters_from_month").val();
                if(parseInt($("#invoice_filters_from_month").val())<10){
                    var month="0"+$("#invoice_filters_from_month").val();
                }
                var day=parseInt($("#invoice_filters_from_day").val());
                if(day<10) {                    
                    var day="0"+day;
                }
                var d = new Date($("#invoice_filters_from_year").val()+"/"+month+"/"+day);
                d.setHours(0, 0, 0, 0);
                var delivery_month=$("#invoice_filters_to_month").val();
                if(parseInt($("#invoice_filters_to_month").val())<10)
                {
                    var delivery_month="0"+$("#invoice_filters_to_month").val();
                }

                var delivery_day=parseInt($("#invoice_filters_to_day").val());
                if(delivery_day<10)
                {
                    var delivery_day="0"+delivery_day;
                }
                //alert(delivery_day);
                var delivery_date = new Date($("#invoice_filters_to_year").val()+"/"+delivery_month+"/"+delivery_day);
                delivery_date.setHours(0,0,0,0);
                //                    alert(d.getTime().valueOf());alert(delivery_date.getTime());
                //                    return false;
                
                //                alert("To Date:"+delivery_date);
                // alert(d.getTime().valueOf() > delivery_date.getTime().valueOf());
                if(d.getTime().valueOf() > delivery_date.getTime().valueOf())
                {
                    //                        alert("From Cannot be Greater than To Date");

                    /**
                     * Manu : Search date display in case of page refresh
                     */
                    $("#dispErr_date3").html('"To" Date Cannot be Less than "From" Date');
                    $("#dispErr_date3").addClass('error');
                    $('#dispErr_date3').css('display', 'inline');
                    $("#dispErr_date2").html('');
                    $("#dispErr_date2").addClass('error');
                    $('#dispErr_date2').css('display', 'inline');
                    return false;
                }
                else
                {
                    $('#dispErr_date2').css('display', 'none');
                    $('#dispErr_date3').css('display', 'none');
                    return true;
                }

            }

        }
        else
        {

            /**
             * Manu : Search date display in case of page refresh
             */
            if($('#invoice_filters_to_date').val()==''&& $('#invoice_filters_from_date').val()=='')
            {
                $("#dispErr_date3").html('Please enter To Date');
                $("#dispErr_date3").addClass('error');
                $('#dispErr_date3').css('display', 'inline');
                $("#dispErr_date2").html('Please enter From Date');
                $("#dispErr_date2").addClass('error');
                $('#dispErr_date2').css('display', 'inline');
                return false;
            }else if($('#invoice_filters_to_date').val()==''&& $('#invoice_filters_from_date').val()!='')
            {
                /**
                 * Manu : Search date display in case of page refresh
                 */
                $("#dispErr_date3").html('Please enter To Date');
                $("#dispErr_date3").addClass('error');
                $('#dispErr_date3').css('display', 'inline');
                $("#dispErr_date2").html('');
                $("#dispErr_date2").addClass('error');
                $('#dispErr_date2').css('display', 'inline');
                return false;
            }else if($('#invoice_filters_to_date').val()!='' && $('#invoice_filters_from_date').val()=='')
            {
                /**
                 * Manu : Search date display in case of page refresh
                 */
                $("#dispErr_date2").html('Please enter From Date');
                $("#dispErr_date2").addClass('error');
                $('#dispErr_date2').css('display', 'inline');
                $("#dispErr_date3").html('');
                $("#dispErr_date3").addClass('error');
                $('#dispErr_date3').css('display', 'inline');
                return false;
            }
            else
            {
                return true;
            }

        }
    },'');

    $(document).ready(function(){
        
        
         $('iframe').load(
            function() {
                this.style.height = 0;
                this.style.width= 0;
                this.style.height = (this.contentWindow.document.body.scrollHeight-25) + 'px';
                this.style.width = '102%'; // (this.contentWindow.document.body.scrollWidth + 2000) + 'px';
            });
            
            /* The input parameters for jasper report */
        $('input[type=submit]').click(
            function(){
                $('#PARAM_customer_id').val($('#customer_id').val()); 
                $('#PARAM_invoice_number').val($('#invoice_no').val());

                if(document.getElementById('date_type'))
                    $('#PARAM_date_type').val($('#date_category:checked').val());
                
                if(document.getElementById('com_name') && $('#com_name').val().length > 0)
                    $('#PARAM_merchant_name').val($('#com_name').val());
              
              
                if(document.getElementById('cust_name') && $('#cust_name').val().length > 0)
                    $('#PARAM_customer_name').val($('#cust_name').val());

                if($('#invoice_filters_to_date').val().length > 0)
                    $('#PARAM_to_date').val($('#invoice_filters_to_date').val());
                if($('#invoice_filters_from_date').val().length > 0)
                    $('#PARAM_from_date').val($('#invoice_filters_from_date').val());
            }
        )
        
        // populateSearch();
        //        $('#email_address').keypress(
        //            function(){
        //                if($('#email_address').val().length == 1){
        //                 $('#email_address_error').html('');
        //                }
        //            }
        //        )

      
        $('#Reports').addClass('active');
        
<?php if ($expired == true) { 
        if($status == 0) echo "$('#status_expired').addClass('on');";
        if($status == 3) echo "$('#status_partialexpired').addClass('on');";
 } else { ?>
$('#status_<?php echo $status; ?>').addClass('on');
<?php } if ($partlypaid == 1) {?>
$('#status_3').addClass('on');
<?php } /*if ($both == 1) {?>
$('#status_expired').addClass('on');
<?php }*/ ?>
       



<?php if (!is_null($from_date)) { ?>$('#invoice_filters_from_date').val('<?php echo $from_date; ?>');
            $('#date_selection').css('display', 'inline');
            $("#invoice_filters_from_year").val('<?php echo date('Y', strtotime($from_date)) ?>');
            $("#invoice_filters_from_month").val('<?php echo date('n', strtotime($from_date)) ?>');
            $("#invoice_filters_from_day").val('<?php echo date('j', strtotime($from_date)) ?>');



<?php
} else {
?>$('#invoice_filters_from_date').val('');
<?php
}
?>
<?php if (!is_null($to_date)) { ?>$('#invoice_filters_to_date').val('<?php echo $to_date; ?>');
            $('#date_selection').css('display', 'inline');
            $("#invoice_filters_to_year").val('<?php echo date('Y', strtotime($to_date)) ?>');
            $("#invoice_filters_to_month").val('<?php echo date('n', strtotime($to_date)) ?>');
            $("#invoice_filters_to_day").val('<?php echo date('j', strtotime($to_date)) ?>');

<?php
} else {
?>$('#invoice_filters_to_date').val('');

<?php
}
?>
<?php if (!is_null($date_cat)) { ?>$('#<?php echo $date_cat; ?>').attr('checked','checked');
            $('#date_search').attr('checked','checked');
            $('#date_type').css('display', 'inline');
            $('#date_type').addClass('date_type');
            //          displayDateSearch();
<?php } ?>


        $('#reset').bind('click' ,  function ()
        {
            $('#search_form').find('input:text, input:password, input:file, select, textarea').val('');// Aditi:- To clear all text input search  on reset
        
            $('#issue_date').attr('checked',true); //Varun : Bug fix : FS#34873, on reset Issue date will be set as checked as by default.
            $('input[type=text]').val('');
            $('input [name=date_category]').val('');
            $('#invoice_filters_from_date').val('');
            $('#invoice_filters_to_date').val('');$('label[for=date_category]').hide();
            $("#invoice_filters_to_year").val('');
            $("#invoice_filters_to_month").val('');
            $("#invoice_filters_to_day").val('');
            $("#invoice_filters_from_year").val('');
            $("#invoice_filters_from_month").val('');
            $("#invoice_filters_from_day").val('');
            // Varun : Form Submit removed from reset functionality , as it validates date search on reset .
            //            $('#search_form').submit();
            //    populateSearch();
        });
        $('#date_search').bind('click' ,  function ()
        {
            if($(this).attr('checked') )
            {


                displayDateSearch();
            }
            else
            {
                hideDateSearch();

            }

        });
        // populateSearch();
        function displayDateSearch()
        {
            $('#date_type').css('display', 'inline');
            $('#date_type').addClass('date_type');
            //$('#date_type').css('clear', 'left');
            //                alert($('input [name="date_category"]').val());

            $("#search_form").validate({
                rules: {
                    'date_category':
                        {
                        required: true
                    }

                },
                messages:{
                    'date_category':
                        {
                        required: "Please enter Date Type"
                    }

                }
            });
            //            $('$('input [name="date_category"]').rules("add", "required");
            $('#date_selection').css('display', 'inline');
            $('#invoice_filters_to_day').rules("add", "diff_between_dates");

        }
        function hideDateSearch()
        {
            $('#dispErr_date2').html('');
            $('#dispErr_date3').html('');
            $('#date_type').css('display', 'none');
            $('#date_type').addClass('date_type');
            $('#date_selection').css('display', 'none');
            $('input[name="date_category"]').rules('remove');
            $('#invoice_filters_to_day').rules('remove');
            $('input [name=date_category]').val('');
            $('#invoice_filters_from_date').val('');
            $('#invoice_filters_to_date').val('');
            $('label[for=date_category]').hide();
        }





    });
</script>



<?php
include_partial('global/reportSubHeader', array('addClassTo' => $addClassTo));
?>

<div id="mainnew">

    <div id="wrappernew">
        <div id="content">
            <div id="Formwraper">



                <h3>Search Invoice</h3>
                <div id="msg"></div>



                <div class="TableForm" id="search">
                    <!--form method="post" id="search_form" action="<?php echo url_for('report/allInvoicesForMerchant'); ?>"-->
                    <form  action ="<?php echo url_for('report/jasperReport')?>" method="get" id="search_form"  target="iframe_id">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>&nbsp;Customer Id<font color="red"></font></td>
                                <td>
                                    <input type="text" id="customer_id" name="customer_id"   value="<?php echo $customer_id; ?>">
                                    <div class="red redlt" id="customer_id_error"></div>
                                    
                                    <input type="hidden" name="PARAM_status" id="PARAM_status" value="<?php echo isset($jasperStatus)?$jasperStatus:''?>"/>
                                    <input type="hidden" name="PARAM_customer_id" id="PARAM_customer_id" value="<?php echo $customer_id;?>"/>
                                    <input type="hidden" name="PARAM_to_date" id="PARAM_to_date" value="<?php echo $to_date?>"/>
                                    <input type="hidden" name="PARAM_date_type" id="PARAM_date_type" value="issue_date"/>
                                    <input type="hidden" name="PARAM_from_date" id="PARAM_from_date" value="<?php echo $from_date?>"/>
                                    <input type="hidden" name="PARAM_invoice_number" id="PARAM_invoice_number" value="<?php echo $invoice_no?>"/>
                                       <?php if ($adminLogin || $subadminLogin4Report) {?> 
                                                <input type="hidden" name="PARAM_merchant_name" id="PARAM_merchant_name" value="<?php echo $com_name;?>"/>
                                                <input type="hidden" name="ReportName" value="Admin_Invoice"/>
                                        <?php } else {?>  
                                                <input type="hidden" name="PARAM_customer_name" id="PARAM_customer_name" value="<?php echo $cust_name;?>"/>
                                                <input type="hidden" name="ReportName" value="Merchant_Invoice"/>
                                                <input type="hidden" value="<?php echo sfContext::getInstance()->getUser()->getGuardUser()->getMerchantUser()->getId()?>" name="PARAM_merchant_id" id="PARAM_merchant_id" />
                                        <?php } ?> 
                                            
                                    
                                </td>
                            </tr>
                            <?php if ($merchnatLogin) {
                            ?>
                                <tr>
                                    <td><?php echo $search_form['cust_name']->renderLabel() ?><font color="red"></font></td>
                                    <td>
                                        <input type="text" id="cust_name" name="cust_name"  value="<?php echo $cust_name; ?>">
                                        <div class="red redlt" id="cust_name_error"> <?php echo $search_form['cust_name']->renderError(); ?></div>
                                    </td>
                                </tr>
                            <?php } if ($admin_login) {
 ?>
                                <tr>
                                    <td>&nbsp;Merchant Name <font color="red"></font></td>
                                    <td>
                                        <input type="text" id="com_name" name="com_name"   value="<?php echo $com_name; ?>">
                                        <div class="red redlt" id="com_name_error"></div>
                                    </td>
                                </tr>
<?php } ?>

                            <tr>
                                <td><?php echo $search_form['invoice_no']->renderLabel() ?><font color="red"></font></td>
                                <td>
                                    <input type="text" id="invoice_no" name="invoice_no"  value="<?php echo $invoice_no; ?>">
                                    <div class="red redlt" id="invoice_no_error"> <?php echo $search_form['invoice_no']->renderError(); ?></div>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="3" >
                                    <div style="padding:5px 0 0 5px; float:left; font-weight:bold;"><input type="checkbox" id="date_search" class="date_f">Search Date</div>
                                    <div style="clear:both; display:inline; margin:15px 0 5px 0;">
                                        <div id="date_type" style="display:none">
<!--                                            <input type="radio" id="issue_date" name="date_category" checked value="issue_date">Issue Date
                                            <input type="radio" name="date_category" id="delivery_date" value="delivery_date">Delivery Date
                                            <input type="radio" name="date_category" id="due_date" value="due_date">Due Date
                                            <input type="radio" name="date_category" id="payment_date" value="payment_date">Payment Date-->
                                           <input type="radio" id="date_category" name="date_category" checked value="issue_date">Issue Date
                                            <input type="radio" name="date_category" id="date_category" value="delivery_date">Delivery Date
                                            <input type="radio" name="date_category" id="date_category" value="due_date">Due Date
                                            <input type="radio" name="date_category" id="date_category" value="payment_date">Payment Date
                                            
                                            <!-- Varun : Bug fix : FS#34688 -->
                                            (<font color="red">*</font>Please select one option)
                                            <label for="date_category" generated="false" class="error"></label>
                                        </div>  
                                    </div>
                                    <div id="date_selection" style="display:none">
                                        <div class="FormRepeatArea form_no_height" style="clear:left;">
                                            <div style="margin-bottom:5px; display:inline-block;">
                                                <div style="float:left;width:80px;"><?php echo $form['from']->renderLabel() ?></div>
                                                <div class="FromInputLabe2 select_size"> <?php echo $form['from']->render() ?></div>
                                                <span id="dispErr_date2" style="display: none;"></span>
                                            </div>
                                            <div class="FormRepeatArea form_no_height">
                                                <div style="float:left;width:80px;"> <?php echo $form['to']->renderLabel() ?></div>
                                                <div class="FromInputLabe2 select_size"> <?php echo $form['to']->render() ?></div><label for="invoice_filters_to_day" generated="false" class="error" id="label_error"></label>
                                                <span id="dispErr_date3" style="display: none;"></span>
                                                <div class="clear_new"></div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                            <input type="hidden" name="status" id="status" value="<?php echo $status; ?>">
                            <td colspan="3" >
                                <div class="Errormrg"><input type="submit" value="Search" class="blueBtn"  onclick="return validateSearch()">
                                    <input type="button" class="blueBtn" id="reset" value="Reset"  onClick="window.location='<?php echo url_for('report/allInvoicesForMerchant'); ?>'"/>
                                </div>
                            </td>
                            </tr>
                        </table>
                    </form>
                </div>
                <div class="clear_new"></div>
                <div class="clear_new"></div>
                <div class="icon_cmd" style="display:none">
                    <?php
                            //Varun : Helper added for proper amount format
                            $helperOb = new csvHelper();
//                          $query_string = 'search=' . $searchKey . '&filter=' . $class_on . '&print=1&from=' . $from_date . '&to=' . $to_date . '&date_cat=' . $date_cat.'&email_address=' . $email_address .'&cust_name=' . $cust_name.'&search_par=' . $search_par;
                            $queryStringForStatusSearch_print = $queryStringForStatusSearch . 'filter=' . $class_on . '&print=1&status=' . $status . '&expired=' . $expired;
                            echo link_to(image_tag('print_icon.png', array('alt' => "Print invoice", 'title' => "Print invoice", "border" => "0", "width" => "16")), 'report/print', array(
                                'popup' => array('Windowtitle', 'resizeable=yes,scrollbars=yes,location=0,menubar'),
                                'query_string' => $queryStringForStatusSearch_print));
                            $queryStringForStatusSearch_export = $queryStringForStatusSearch . 'filter=' . $class_on . '&export=1&status=' . $status . '&expired=' . $expired;
                            echo link_to(image_tag('export_icon.png', array('alt' => "Export report to PC", 'title' => "Export report to PC", "border" => "0", "width" => "16")), 'report/export', array(
                                'popup' => array('Windowtitle', 'width=640,height=300,left=320,top=200,resizeable=yes,scroll=yes'),
                                'query_string' => $queryStringForStatusSearch_export));
                            $queryStringForStatusSearch_pdf = $queryStringForStatusSearch . 'filter=' . $class_on . '&pdf=1&status=' . $status . '&expired=' . $expired;
                            echo link_to(image_tag('pdf_icon.png', array('alt' => "Compile as PDF", 'title' => "Compile as PDF", "border" => "0", "width" => "16")), 'report/pdf', array(
                                'popup' => array('Windowtitle', 'width=640,height=300,left=320,top=200,resizeable=yes,scroll=yes'),
                                'query_string' => $queryStringForStatusSearch_print));
                    ?>
                        </div>
                       <div class="searchOption1  clearfixnew">
                            <ul><li>Search By  :</li>
                            <li id="status_"><a href="<?php echo url_for('report/allInvoicesForMerchant') . "?" . $queryStringForStatusSearch; ?>" >All</a></li>
                            <li id="status_0"><a  href="<?php echo url_for('report/allInvoicesForMerchant') . '?status=0' . $queryStringForStatusSearch ?>" >Pending</a></li>
                            <li id="status_2"><a href="<?php echo url_for('report/allInvoicesForMerchant') . '?status=2' . $queryStringForStatusSearch; ?>" >Paid</a></li>
                            <?php if ($showPartiallyPaidInvoicesSearch) { ?>   
                                <li id="status_3"><a href="<?php echo url_for('report/allInvoicesForMerchant') . '?status=3' . $queryStringForStatusSearch; ?>" >Partially Paid</a></li>
                            <?php } ?>
                            <li id="status_expired"><a href="<?php echo url_for('report/allInvoicesForMerchant') . '?status=0&expired=1' . $queryStringForStatusSearch; ?>" >Expired</a></li>
                            <li id="status_partialexpired"><a href="<?php echo url_for('report/allInvoicesForMerchant') . '?status=3&expired=1' . $queryStringForStatusSearch; ?>" >Partial/Expired</a></li>
                            <?php if (!$admin_login) { ?>    
                                <li id="status_1"><a href="<?php echo url_for('report/allInvoicesForMerchant') . '?status=1' . $queryStringForStatusSearch ?>" >Cancelled</a></li>
                            <?php } ?>
                        </ul></div>
                    <div style="clear:both;line-height:4px;">&nbsp;</div>
                    <div style="text-align:center;margin-top:100px" id='waitingDiv'><?php echo image_tag('loadingBar.gif', array('border'=>0,'alt'=>'Loading','title'=>'Loading'));?></div>
                    <iframe id="iframe_id" width="100%" scrolling="no" cellpadding="4" style="position:relative;right:16px;" name="iframe_id" src="<?php echo url_for('report/jasperReport')?>" frameborder="0"></iframe>
                    <div style="display:none"> 
                    <div class="TableForm" style="margin-top:0px;">
                        <table border="0" cellspacing="0" cellpadding="0">
                            <thead>
                        <th>Invoice Number</th>
                        <?php if ($admin_login) {?>
                            <th>Merchant Name</th>
                        <?php } else { ?>
                                <th>Customer Name</th>
                        <?php } ?>
                            <th>Customer Id</th>
                            <th>Amount(<?php echo image_tag('naira.gif', array('align' => 'absmiddle')); ?>)</th>
                            <th>Issue Date</th>
                            <th>Delivery Date</th>
                            <th>Due Date</th>
                            <th>Payment Date</th>
                            <th>Status</th>
                            </thead>
                            <tbody>
                            <?php
                            if (count($invoices) > 0) {
                                $total_amount_to_be_displayed = 0;
                                $i = 0;
                                foreach ($invoices as $invoice) {
                                    $i++;
                                     if ($i % 2 != 0) {
                            ?>
                                <tr class="odd">
                                <?php } else {
                                ?><tr class=""> <?php } ?>
                                    <td><a href="<?php echo url_for('invoice/show?id=' . cryptString::encrypt($invoice->getId())); ?>"><?php echo $invoice->getInvoiceNo(); ?></a></td>
                                <?php if ($admin_login) {
                                ?><td><?php echo $invoice->getMerchant()->getMerchantName(); ?></td>
                                <?php
                                    }
                                    if (!$admin_login) {
                                ?>
                                    <td><?php echo $invoice->Customer->getName(); ?></td><?php } ?>
                                    <td><?php echo $invoice->getCustomer()->getCustomerId(); ?></td>
                                    <td align="right"><?php
                                    foreach ($item_details as $ob) {
                                        if ($ob->getInvoiceId() == $id_Items_invoiceId[$invoice->getId()]) {
                                            $amount = $helperOb->price_format($ob->getPartialSum() + $ob->getTax());
                                            echo $amount;
                                            $total_amount_to_be_displayed+=$ob->getPartialSum() + $ob->getTax();
                                        }
                                    }
                                ?></td>
                                <td><?php echo $invoice->getIssueDate(); ?></td>
                                <td><?php echo $invoice->getDeliveryDate(); ?></td>
                                <td><?php echo $invoice->getDueDate(); ?></td>
                                <?php if ($invoice->getPaymentDate()) {
 ?>
                                        <td><?php echo $invoice->getPaymentDate(); ?></td>
                                <?php } else { ?>
                                        <td> NA </td>
                                <?php } ?>
                                    <td><?php
                                    switch ($invoice->getStatus()) {
                                        case 0:
                                            if (strtotime($invoice->getDueDate()) >= strtotime(date("Y-m-d"))) {
                                                echo image_tag('pending.png', array('alt' => "Pending", 'title' => "Pending"));
                                            } else {
                                                echo image_tag('expired.png', array('alt' => "Expired", 'title' => "Expired"));
                                            }
                                            break;
                                        case 1:
                                            echo image_tag('cancel.png', array('alt' => "Cancelled", 'title' => "Cancelled"));
                                            break;
                                        case 2;
                                            echo image_tag('paid.png', array('alt' => "Paid", 'title' => "Paid"));
                                            break;
                                        case 3:
                                            if (strtotime($invoice->getDueDate()) >= strtotime(date("Y-m-d"))) {
                                                echo image_tag('partially-paid.png', array('alt' => "Partially Paid", 'title' => "Partially Paid"));
                                            } else {
                                                echo image_tag('partiallypaid_expired.png', array('alt' => "PartiallyPaid/Expired", 'title' => "PartiallyPaid/Expired"));
//                                                echo image_tag('expired.png', array('alt' => "Expired", 'title' => "Expired"));
                                            }
                                            break;
                                    }
                                ?></td>
                            </tr>
                            <?php } ?>
                                <tr class="border-bottom">
                                    <td></td>
                                    <td></td>
                                    <td align="left"><b>Amount<?php if ($setPagination) {
                            ?><span class="sign">+</span><?php } ?></b></td>
                                <td align="right" ><b>
                                        <?php echo $helperOb->price_format($total_amount_to_be_displayed);
                                        ?></b>
                                </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <?php
                                    } else {
                            ?><tr><td colspan='9' align='center'>No Invoices Found</td></tr>
                            <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="nofoList"><?php include_partial('global/records_number', array('amount_footer' => true, 'current_offset' => isset($current_offset) ? $current_offset : null, 'offset_next' => isset($offset_next) ? $offset_next : null, 'total_no_of_records' => isset($total_no_of_records) ? $total_no_of_records : count($invoices), 'no_of_records_per_page' => isset($no_of_records_per_page) ? $no_of_records_per_page : null, 'from' => '', 'total_amount_of_all_transactions' => isset($total_amount_of_all_transactions) ? $total_amount_of_all_transactions : $total_amount_to_be_displayed)); ?></div>
                                <div align="right">
                    <?php
                                    if ($setPagination) {
                                        $array_for_pagination = array('moduleName' => $module, 'actionName' => $actionTobeCalled, 'amount_footer' => true, 'current_offset' => $current_offset, 'offset_next' => $offset_next, 'offset_prev' => $offset_prev, 'offset_last' => $offset_last, 'from' => $from_date, 'to' => $to_date, 'date_cat' => $date_cat, 'invoice_status' => $status, 'invoice_no' => $invoice_no, 'customer_id' => $customer_id, 'cust_name' => $cust_name, 'com_name' => $com_name, 'both' => $both, 'partlyPaid' => $partlypaid);
                                        if ($expired == true) {
                                            $array_for_pagination['expired'] = 1;
                                        }
                                        include_partial('global/pagination', $array_for_pagination);
                                    }
                    ?></div>
                    </div>
                  </div>
                </div>
              </div> 
            </div>
<script>
$(document).ready(
function(){
    $('form').submit();
}
);    
</script>   