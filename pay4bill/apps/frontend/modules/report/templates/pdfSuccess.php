<script language="javascript">
    function closeWindow()
    {

        if($("#pdf_form").valid() == true){
            var is_chrome = /chrome/.test( navigator.userAgent.toLowerCase() );
            if(!is_chrome){
                window.close();
            }
        }
    }

    //    function closePopUp() {
    //        if($("#pdf_form").valid() == true){
    //         window.setTimeout('closeWindow()',1000);
    //         return true;
    //        }
    //        return false;
    //    }
    $(document).ready(function(){

<?php if ($file_to_be_downloaded == false) { ?>

            $("#pdf_form").validate({
                rules: {
                    'filename': {
                        required: true,
                        maxlength: 30
                    }
                },
                messages: {
                    'filename':
                        {
                        required: "Please enter Filename",
                        maxlength: "Maximum 30 Characters"
                    }
                }
            })

<?php } else { ?>
            //Varun : Bug fix FS#34705, folder shifted inside uploads as permission is not caused for uploads folder
            d=window.open("<?php echo _compute_public_path($file_to_be_downloaded, 'uploads/exported_files', '', true) ?>");
            window.close();
            //d.close();
            //    window.location=<?php //echo $file_to_be_downloaded;  ?>;
<?php } ?>
    });
</script>
<div style="clear:left; padding-top:20px;">
    <h3>Save As</h3>



    <form name="pdf_form" method="post" action="<?php echo url_for('report/pdf'); ?>" id="pdf_form" target="_blank" onSubmit="return closeWindow()"><!--Manu :  Close popup after Submission of Form -->

        <div class="TableForm">
            <table>
                <tr>
                    <td width="20%">File Name</td>
                    <td>
                        <input type="text" class="FormInput9" value="" id="file_nameText" name="filename">
                        <input type="hidden" name="status" value="<?php echo $status; ?>">
                        <input type="hidden" name="invoice_no" value="<?php echo $invoice_no; ?>">
                        <input type="hidden" name="cust_name" value="<?php echo $cust_name; ?>">
                        <input type="hidden" name="from" value="<?php echo $from; ?>">
                        <input type="hidden" name="to" value="<?php echo $to; ?>">
                        <input type="hidden" name="date_cat" value="<?php echo $date_cat; ?>">
                        <input type="hidden" name="filter" value="<?php echo $filter; ?>">
                        <input type="hidden" name="expired" value="<?php echo $expired; ?>">
                        <input type="hidden" name="customer_id" value="<?php echo $customer_id; ?>"
                    </td>
                </tr>
            </table>
        </div>

        <div class="button-position"> <input type="submit" class="blueBtn" value="Save"></div>
        <div class="button-position"> <input type="button" class="blueBtn" value="Close" onClick="window.close()"></div>
    </form>

</div>


