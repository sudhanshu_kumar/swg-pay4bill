<script language="javascript">
    $(document).ready(function(){
<?php if ($to_be_printed) { ?>
            window.print();

<?php } ?>




    }
);
    function isatLeastOneInvoiceSelected()
    {
        var return_var=false;
        $('.invoice_chkBox').each(function(index,element) {
            //alert($);
            if($(this).attr('checked'))
            {
                return_var=true;
            }

        });
        if(!return_var){
            alert('Please select at least one Invoice to Print');
            //            $('#error_form').html('<i>Please select at least one Invoice to Print</i>');
            //            $('#error_form').css('display','inline');
            return false;
        }
        else{
            $('#error_form').html('');
            $('#error_form').css('display','none');
        }
        //              alert(return_var);
        return return_var;
        //        return false;
    }
</script>
<?php /* ?><?php echo html_entity_decode($pgTitle);?><?php */ ?>
<div id="pgMenu">
    <div id="wrappernew">        <?php
if (!empty($logo)) {
    if (file_exists(sfConfig::get('sf_upload_dir') . '/merchant_logo/' . $logo)) {
 ?>
        <div class="noprint" style="margin-left: 600px; margin-bottom: 25px;"> <img src="<?php echo _compute_public_path($logo, 'uploads/merchant_logo', '', true); ?>" style="height: 75px; width: 75px;" alt="Merchant Logo" class="" /> </div>
<?php }
} ?>
        <div class="pgTitle">

            <h2><?php echo html_entity_decode($pgTitle); ?></h2></div>
    </div>

</div>



<div id="wrappernew">
    <div id="content">
        <div id="Formwraper">

<?php if (!$to_be_printed) { ?>

            <form action="<?php echo url_for('report/printSelectedInvoices') ?>" id ="print_form" onsubmit="return isatLeastOneInvoiceSelected()">
                <input type="hidden" name="pgTitle" value="<?php echo $pgTitle; ?>">
<?php } ?>

                <div class="TableForm">

                    <table border="0" cellspacing="0" cellpadding="0">
                        <thead>
                            <?php if (!$to_be_printed) { ?> <th></th><?php } ?>
                        <th>Invoice Number</th>
                        <?php if ($admin_login) {
                        ?><th>Merchant Name</th>
                        <?php } else {
                        ?>
                                <th>Customer Name</th>
                        <?php } ?>
                            <th>Customer Id</th>
                            <th>Amount(N)</th>
                            <th>Issue Date</th>
                            <th>Delivery Date</th>
                            <th>Due Date</th>
                            <th>Payment Date</th>
                            <th>Status</th>
                            </thead>

                            <tbody>
    <!--                        <thead>

                        <th>Invoice Number</th>
                        <th>Customer Name</th>
                        <th>Amount(N)</th>
                        <th>Issue Date</th>
                        <th>Delivery Date</th>
                        <th>Due Date</th>
                        <th>Status</th>
                        </thead>-->

                            <tbody>
                            <?php
                            $total_amount_to_be_displayed = 0;
                            foreach ($invoices as $invoice) {
                            ?>
                                <tr>
                                <?php if (!$to_be_printed) {
                                ?>  <td><input type="checkbox" class="invoice_chkBox" name="invoice_ids[]" value="<?php echo $invoice->getId() ?>"></td><?php } ?>
                                <td><?php echo $invoice->getInvoiceNo(); ?></td>
                                <td><?php if ($admin_login) {
                                    echo $invoice->Merchant->getMerchantName();
                                } else {
                                    echo $invoice->Customer->getName();
                                } ?></td>
                                <td><?php echo $invoice->Customer->getCustomerId(); ?></td>
                                <td align=""><?php
                                    foreach ($item_details as $ob) {
                                        if ($ob->getInvoiceId() == $id_Items_invoiceId[$invoice->getId()]) {
                                            echo number_format(($ob->getPartialSum() + $ob->getTax()), 2, ".", "");
                                            $total_amount_to_be_displayed+=$ob->getPartialSum() + $ob->getTax();
                                        }
                                    }
                                ?></td>
                                <td><?php echo $invoice->getIssueDate(); ?></td>
                                <td><?php echo $invoice->getDeliveryDate(); ?></td>
                                <td><?php echo $invoice->getDueDate(); ?></td>
                                <td><?php echo $a = ($invoice->getPaymentDate() == '') ? "NA" : $invoice->getPaymentDate(); ?></td>
                                <td><?php
                                    switch ($invoice->getStatus()) {
                                        case 0:
                                            if (strtotime($invoice->getDueDate()) >= strtotime(date("Y-m-d"))) {
                                                echo image_tag('pending.png', array('alt' => "Pending", 'title' => "Pending"));
                                            } else {
                                                echo image_tag('expired.png', array('alt' => "Expired", 'title' => "Expired"));
                                            }
                                            break;
                                        case 1:
                                            echo image_tag('cancel.png', array('alt' => "Cancelled", 'title' => "Cancelled"));
                                            break;
                                        case 2;
                                            echo image_tag('paid.png', array('alt' => "Paid", 'title' => "Paid"));

                                            break;
                                        case 3:

                                            if (strtotime($invoice->getDueDate()) > time()) {
                                                echo image_tag('partially-paid.png', array('alt' => "Partially Paid", 'title' => "Partially Paid"));
                                            } else {
                                                echo image_tag('partiallypaid_expired.png', array('alt' => "Partial/Expired", 'title' => "Partial Expired"));
                                            }

                                            break;
                                    }
                                ?></td>
                            </tr>



<?php } ?>
                            <tr class="border-bottom"><?php if (!$to_be_printed) { ?><td></td><?php } ?><td></td><td></td><td align="right"><b>Amount:</b></td><td align="" ><b><?php echo number_format($total_amount_to_be_displayed, 2, '.', ''); ?></b><td></td><td></td><td></td><td></td><td></td></tr>
                        </tbody>
                    </table>



<?php if (!$to_be_printed) { ?> </div>
                                <div class="clearfix btn-row" align="center"><?php if (!$to_be_printed) { ?>
                                        <input type="submit" value="Print" align="center">
                                        <input type="button" value="Close" onClick="window.close()" align="center">
                                    </div><div id="error_form" class="error" style="display:none"></div>
<?php } ?>

                                </form>
<?php } ?>
        </div>
    </div></div>
