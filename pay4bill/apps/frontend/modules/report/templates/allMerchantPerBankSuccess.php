<script type="text/javascript">
    $(document).ready(function(){
    
    $('iframe').load(
            function() {
                this.style.height = 0;
                this.style.width= 0;
                this.style.height = (this.contentWindow.document.body.scrollHeight-25) + 'px';
                this.style.width = '102%'; // (this.contentWindow.document.body.scrollWidth + 2000) + 'px';
            });
            
            /* The input parameters for jasper report */
        $('input[type=submit]').click(
            function(){
                $('#PARAM_merchant_name').val($('#merchant_name').val());    
                if($('#to_date').val().length > 0)
                    $('#PARAM_to_date').val($('#to_date').val());
                if($('#from_date').val().length > 0)
                    $('#PARAM_from_date').val($('#from_date').val());
            }
        )

        
        

$('#Reports').addClass('active');
        $('#btnReset').click(
        function(){
            // Redirect to the same window once reset
            window.location.href = window.location.href;
        }
    );
    }
);
</script>

<?php
include_partial('global/reportSubHeader', array('addClassTo' => $addClassTo));
?>

<?php if (!empty($msg)) {
?><div class="clear_new"></div> <?php } ?>

<div id="mainnew">

    <?php
    $sf = sfContext::getInstance()->getUser();
    if ($sf->hasFlash('notice')) {
    ?><div id="flash_notice" class="msg_serverside" ><?php
        echo nl2br($sf->getFlash('notice'));
    ?></div><?php } ?>
    <div id="wrappernew">
        <div id="content">
            <div id="Formwraper">

                <h3>Merchant Listing <span style="font-size:13px">( <b>Bank:</b> <?php echo $bank ?> )</span></h3>
                <div style="clear:both"></div>

                <div class="TableForm" id="search">
                     <form  action ="<?php echo url_for('report/jasperReport')?>" method="get" id="search_form"  target="iframe_id">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <?php
                            if ($form->offsetExists('merchant_name')) {
                            ?> <tr>
                                    <td align="left"><?php echo $form['merchant_name']->renderLabel(); ?></td>
                                    <td><?php echo $form['merchant_name']->render(); ?><?php echo $form['merchant_name']->renderError(); ?></td>
                                </tr>
                            <?php
                            }
                            ?>
                            <tr id="merchantSpecificAttributes">
                                <td colspan="3"  valign="top">
                                    <div class="Errormrg">
                                    <input type="hidden" name="PARAM_merchant_name" id="PARAM_merchant_name"/>
                                    <input type="hidden" name="PARAM_to_date" id="PARAM_to_date" value="<?php echo $to_date?>"/>
                                    <input type="hidden" name="PARAM_from_date" id="PARAM_from_date" value="<?php echo $from_date?>"/>
                                    <input type="hidden" name="PARAM_bank_name" id="PARAM_bank_name" value="<?php echo $bank?>"/>
                                       <?php  if ($adminLogin || $subadminLogin4Report) { ?>
                                            <input type="hidden" name="ReportName" value="Admin_AllMerchantPerBank"/>
                                        <?php } else {?>  
                                            <input type="hidden" name="ReportName" value=""/>
                                            <input type="hidden" value="<?php echo sfContext::getInstance()->getUser()->getGuardUser()->getMerchantUser()->getId()?>" name="PARAM_merchant_id" id="PARAM_merchant_id" />
                                        <?php } ?>    

                                        
                                        <input type="submit" value="Search" class="blueBtn" >
                                        <input type="button" class="blueBtn" value="Reset" id="btnReset"/>
                                    </div>
                                </td>
                            </tr>
                        </table>

                    </form>
                </div>
                <div style="clear:both"></div>
                <div style="text-align:center;margin-top:100px" id='waitingDiv'><?php echo image_tag('loadingBar.gif', array('border'=>0,'alt'=>'Loading','title'=>'Loading'));?></div>
                <iframe id="iframe_id" width="100%" scrolling="no" cellpadding="4" style="position:relative;right:16px;" name="iframe_id" src="<?php echo url_for('report/jasperReport')?>" frameborder="0"></iframe>

                <div style="display:none">

                <div class="TableForm">
                    <table cellspacing="0" cellpadding="0" border="0">
                        <th>Merchant Name</th>
                        <th>Number of Payments</th>
                        <th>Total Amount Paid(<?php echo image_tag('naira.gif', array('align' => 'absmiddle')); ?>)</th>
                        <th>Last Payment Date</th>

                        <?php
                            $total_amount_to_be_displayed = '';
                            $total_no_of_payments = 0;

                            if (count($total) > 0) {
                                $i = 0;
                                foreach ($total as $stotal): $i++;
                                    $total_amount_to_be_displayed += $stotal['totalamount'];
                        ?>
                                    <tr class="<?php
                                    if ($i % 2 != 0)
                                        echo 'odd'; else
                                        echo '';
                        ?>">
                                    <td><?php echo $stotal['merchant_name'] ?></td>
                                    <td align ="right"><?php
                                    $total_no_of_payments+=$stotal['no_of_payments'];
                                    echo $stotal['no_of_payments']; ?>
                                </td>
                                <td align="right"><?php echo csvHelper::price_format($stotal['totalamount'], 2, '.', ','); ?></td>
                                <td><?php echo $stotal['last_payment_date']; ?></td>  </tr>
                        <?php
                                    endforeach;
                                    echo '<tr class=\'border-bottom\'><td></td><td align="right"><b>' . $total_no_of_payments . '</b></td><td align="right"><b>' . csvHelper::price_format($total_amount_to_be_displayed, 2, '.', ',') . '</b></td></td><td></tr>';
                                } else {
                                    echo "<tr><td colspan='4' align='center'>No Merchant Found</td></tr>";
                                }
                        ?>
                            </table>
                        </div>

                        <div class="nofoList">
                    <?php
                                include_partial('global/records_number', array('amount_footer' => true, 'current_offset' => isset($current_offset) ? $current_offset : null, 'offset_next' => isset($offset_next) ? $offset_next : null, 'total_no_of_records' => isset($total_no_of_records) ? $total_no_of_records : count($total), 'no_of_records_per_page' => isset($no_of_records_per_page) ? $no_of_records_per_page : null, 'from' => '', 'total_amount_of_all_transactions' => isset($total_amount_of_all_transactions) ? $total_amount_of_all_transactions : $total_amount_to_be_displayed))
                   ?>
                            </div>
                            <div align="right">
                    <?php
                                if ($setPagination) {
                                    $array_to_be_included_in_pagination = array('moduleName' => $module, 'amount_footer' => true, 'actionName' => $actionTobeCalled, 'current_offset' => $current_offset, 'offset_next' => $offset_next, 'offset_prev' => $offset_prev, 'offset_last' => $offset_last, 'bank' => $bank, 'from' => $from_date, 'to' => $to_date);
                                    include_partial('global/pagination', $array_to_be_included_in_pagination);
                                }
                    ?>
                </div>
 
                </div>
                
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(
function(){
     $('form').submit();}
);
</script>