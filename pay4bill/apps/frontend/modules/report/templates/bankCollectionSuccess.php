<script type="text/javascript">
    $(document).ready(function(){
        
        
         $('iframe').load(
            function() {
                this.style.height = 0;
                this.style.width= 0;
                this.style.height = (this.contentWindow.document.body.scrollHeight-25) + 'px';
                this.style.width = '102%'; //(this.contentWindow.document.body.scrollWidth + 2000) + 'px';
            });
            
            /* The input parameters for jasper report */
        $('input[type=submit]').click(
            function(){
                $('#PARAM_bank_name').val($('#bank_name').val());
                if($('#to_date').val().length > 0)
                    $('#PARAM_to_date').val($('#to_date').val());
                if($('#from_date').val().length > 0)
                    $('#PARAM_from_date').val($('#from_date').val());
            }
        )

        
        
        
        $('#Reports').addClass('active');
        $('#date_search').bind('click', function(){
            if($(this).attr('checked')){
                displayDateSearch();
            }else{
                hideDateSearch();
            }
        });
        
<?php if (($from_date) != "") { ?>
            displayDateSearch();
            $('#date_search').attr('checked','checked');
            $('#from_date').val('<?php echo $from_date; ?>');
            $('#date_selection').css('display', 'inline');
            $("#from_year").val('<?php echo date('Y', strtotime($from_date)) ?>');
            $("#from_month").val('<?php echo date('n', strtotime($from_date)) ?>');
            $("#from_day").val('<?php echo date('j', strtotime($from_date)) ?>');
<?php } else { ?>
            $('#from_date').val('');
<?php } ?>
        
        
<?php if ($to_date != '') { ?>$('#to_date').val('<?php echo $to_date; ?>');
            $('#date_selection').css('display', 'inline');
            $("#to_year").val('<?php echo date('Y', strtotime($to_date)) ?>');
            $("#to_month").val('<?php echo date('n', strtotime($to_date)) ?>');
            $("#to_day").val('<?php echo date('j', strtotime($to_date)) ?>');
<?php } else { ?>
            $('#to_date').val('');
<?php } ?>
        
        
        jQuery.validator.addMethod('diff_between_dates', function(value, element) {
            if(value!=''){
                if($('#from_date').val()=='')
                {
                    $("#dispErr_date2").html('Please enter From Date');
                    $("#dispErr_date2").addClass('error');
                    $('#dispErr_date2').css('display', 'inline');
                    $("#dispErr_date3").html('');
                    $("#dispErr_date3").addClass('error');
                    $('#dispErr_date3').css('display', 'inline');
                    return false;

                } else{

                    var month=$("#from_month").val();
                    if(parseInt($("#from_month").val())<10){
                        var month="0"+$("#from_month").val();
                    }

                    var day=parseInt($("#from_day").val());
                    if(day<10){
                        var day="0"+day;
                    }
                    
                    var d = new Date($("#from_year").val()+"/"+month+"/"+day);
                    d.setHours(0, 0, 0, 0);

                    var delivery_month=$("#to_month").val();
                    if(parseInt($("#to_month").val())<10){
                        var delivery_month="0"+$("#to_month").val();
                    }

                    var delivery_day=parseInt($("#to_day").val());
                    if(delivery_day<10){
                        var delivery_day="0"+delivery_day;
                    }

                    var delivery_date = new Date($("#to_year").val()+"/"+delivery_month+"/"+delivery_day);
                    delivery_date.setHours(0,0,0,0);

                    if(d.getTime().valueOf() > delivery_date.getTime().valueOf()){
                        $("#dispErr_date3").html('"To" Date Cannot be Less than "From" Date');
                        $("#dispErr_date3").addClass('error');
                        $('#dispErr_date3').css('display', 'inline');
                        $("#dispErr_date2").html('');
                        $("#dispErr_date2").addClass('error');
                        $('#dispErr_date2').css('display', 'inline');
                        return false;
                    } else {
                        $('#dispErr_date2').css('display', 'none');
                        $('#dispErr_date3').css('display', 'none');
                        return true;
                    }
                }
            } else {
                if($('#to_date').val()==''&& $('#from_date').val()==''){
                    $("#dispErr_date3").html('Please enter To Date');
                    $("#dispErr_date3").addClass('error');
                    $('#dispErr_date3').css('display', 'inline');
                    $("#dispErr_date2").html('Please enter From Date');
                    $("#dispErr_date2").addClass('error');
                    $('#dispErr_date2').css('display', 'inline');
                    return false;
                } else if($('#to_date').val()==''&& $('#from_date').val()!=''){
                    $("#dispErr_date3").html('Please enter To Date');
                    $("#dispErr_date3").addClass('error');
                    $('#dispErr_date3').css('display', 'inline');
                    $("#dispErr_date2").html('');
                    $("#dispErr_date2").addClass('error');
                    $('#dispErr_date2').css('display', 'inline');
                    return false;
                } else if($('#to_date').val()!='' && $('#from_date').val()==''){
                    $("#dispErr_date2").html('Please enter From Date');
                    $("#dispErr_date2").addClass('error');
                    $('#dispErr_date2').css('display', 'inline');
                    $("#dispErr_date3").html('');
                    $("#dispErr_date3").addClass('error');
                    $('#dispErr_date3').css('display', 'inline');
                    return false;
                } else {
                    return true;
                }
            }
        },'');
        
        function displayDateSearch(){
            $("#search_form").validate({
                rules: {
                    'date_category':
                        {
                        required: true
                    }

                },
                messages:{
                    'date_category':
                        {
                        required: "Please enter Date Type"
                    }

                }
            });
            $('#date_selection').css('display', 'inline');
            $('#to_day').rules("add", "diff_between_dates");
        }

        function hideDateSearch(){
            $('#date_selection').css('display', 'none');
            $('#to_day').rules('remove');
            $('#from_date').val('');
            $('#to_date').val('');
            $('label[for=date_category]').hide();
        }


    }
);
</script>

<?php
include_partial('global/reportSubHeader', array('addClassTo' => $addClassTo));
?>

<?php if (!empty($msg)) {
?><div class="clear_new"></div> <?php } ?>

<div id="mainnew">

    <?php
    $sf = sfContext::getInstance()->getUser();
    if ($sf->hasFlash('notice')) {
    ?><div id="flash_notice" class="msg_serverside" ><?php
        echo nl2br($sf->getFlash('notice'));
    ?></div><?php } ?>
    <div id="wrappernew">
        <div id="content">
            <div id="Formwraper">
                <h3>Bank Collection</h3>
                <div class="TableForm" id="search">
                    <!--form action="<?php //echo url_for('report/bankCollection'); ?>" method="post" id="search_form"  -->
                    <form  action ="<?php echo url_for('report/jasperReport')?>" method="get" id="search_form"  target="iframe_id">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="left"><?php echo $form['bank_name']->renderLabel(); ?></td>
                                <td width="68%"><?php echo $form['bank_name']->render(); ?><?php echo $form['bank_name']->renderError(); ?>
                                
                                    <input type="hidden" name="PARAM_bank_name" id="PARAM_bank_name"/>
                                    <input type="hidden" name="PARAM_to_date" id="PARAM_to_date"/>
                                    <input type="hidden" name="PARAM_from_date" id="PARAM_from_date"/>
                                    <?php  if ($adminLogin || $subadminLogin4Report) { ?>
                                        <input type="hidden" name="ReportName" value="Admin_BankCollection"/>
                                    <?php } else {?>  
                                        <input type="hidden" name="ReportName" value="Merchant_BankCollection"/>
                                        <input type="hidden" value="<?php echo sfContext::getInstance()->getUser()->getGuardUser()->getMerchantUser()->getId()?>" name="PARAM_merchant_id" id="PARAM_merchant_id" />
                                    <?php } ?>    
                                
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" >
                                    <div style="padding:5px 0 0 5px; float:left; font-weight:bold;">
                                        <input type="checkbox" id="date_search" class="date_f">Search Payment Date </div>
                                    <div style="clear:both; display:inline; margin:15px 0 5px 0;">

                                    </div>
                                    <div id="date_selection" style="display:none">
                                        <div class="FormRepeatArea form_no_height" style="clear:left;">
                                            <div style="margin-bottom:5px; display:inline-block;">
                                                <div style="float:left;width:80px;"><?php echo $form['from']->renderLabel() ?></div>
                                                <div class="FromInputLabe2 select_size"> <?php echo $form['from']->render() ?></div>
                                                <span id="dispErr_date2" style="display: none;"></span>
                                            </div>
                                            <div class="FormRepeatArea form_no_height">
                                                <div style="float:left;width:80px;"> <?php echo $form['to']->renderLabel() ?></div>
                                                <div class="FromInputLabe2 select_size"> <?php echo $form['to']->render() ?></div><label for="to_day" generated="false" class="error" id="label_error"></label>
                                                <span id="dispErr_date3" style="display: none;"></span>
                                                <div class="clear_new"></div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3"  valign="top">
                                    <div class="Errormrg"><input type="submit" value="Search" class="blueBtn" >
                                        <input type="button" class="blueBtn" value="Reset" onClick="window.location='<?php echo url_for('report/bankCollection'); ?>'"/>
                                    </div>
                                </td>
                            </tr>
                        </table>

                    </form>
                </div>

                <!--h3>Banks</h3>
                <div id="if_error" class="msg_serverside"></div-->
                <div style="clear:both"></div>
                <div style="text-align:center;margin-top:100px" id='waitingDiv'><?php echo image_tag('loadingBar.gif', array('border'=>0,'alt'=>'Loading','title'=>'Loading'));?></div>
                <iframe id="iframe_id" width="100%" scrolling="no" cellpadding="4" style="position:relative;right:16px;" name="iframe_id" src="<?php echo url_for('report/jasperReport')?>" frameborder="0"></iframe>
                
                <div style="display:none">
                
                <div class="TableForm">
                    <table cellspacing="0" cellpadding="0" border="0">
                        <th>Bank Name</th>
                        <th>Number of Payments</th>
                        <th>Total Amount Paid(<?php echo image_tag('naira.gif', array('align' => 'absmiddle')); ?>)</th>
                        <th>Last Payment Date</th>
                        <?php if ($adminLogin) {
                        ?><th>Action</th><?php } ?>
                        <?php
                        $total_amount_to_be_displayed = '';
                        $total_no_of_payments = 0;
                        if (count($total) > 0) {
                            $i = 0;
                            foreach ($total as $stotal): $i++;
                                $total_amount_to_be_displayed += $stotal['totalamount'];
                        ?>
                                <tr class="<?php
                                if ($i % 2 != 0)
                                    echo 'odd'; else
                                    echo '';
                        ?>">
                                <td><?php $bn = $stotal['bank_name'];
                                echo (!empty($bn) ? $bn : '-'); ?></td>
                            <td align ="right"><?php
                                $total_no_of_payments+=$stotal['no_of_payments'];

                                echo $stotal['no_of_payments']; ?></td>
                            <td align="right"><?php echo csvHelper::price_format($stotal['totalamount'], 2, '.', ','); ?></td>
                            <td><?php echo $stotal['last_payment_date']; ?></td>
                            <?php if ($adminLogin) {
 ?>
                                    <td>
                                        <a href="<?php echo url_for('report/allMerchantPerBank') . '?bank=' . $bn . '&from=' . $from_date . '&to=' . $to_date; ?>" ><?php echo image_tag('invoice.png', array('alt' => 'View Merchants', 'title' => 'View Merchants')) ?></a>

<?php } ?>
                        </tr>
                        <?php
                                endforeach;
                                echo '<tr class=\'border-bottom\'><td colspan="1"></td><td align=right><b>' . $total_no_of_payments . '</b></td><td align="right"><b>' . csvHelper::price_format($total_amount_to_be_displayed, 2, '.', ',') . '</b></td><td></td>';
                                if ($adminLogin) {
                                    echo '<td></td>';
                                }
                                echo '</tr>';
                            } else {
                                echo "<tr><td colspan='5' align='center'>No Bank Found</td></tr>";
                            }
                        ?>
                        </table>
                    </div>

                    <div class="nofoList">
                    <?php
                            include_partial('global/records_number', array('amount_footer' => true, 'current_offset' => isset($current_offset) ? $current_offset : null, 'offset_next' => isset($offset_next) ? $offset_next : null, 'total_no_of_records' => isset($total_no_of_records) ? $total_no_of_records : count($total), 'no_of_records_per_page' => isset($no_of_records_per_page) ? $no_of_records_per_page : null, 'from' => '', 'total_amount_of_all_transactions' => isset($total_amount_of_all_transactions) ? $total_amount_of_all_transactions : $total_amount_to_be_displayed))
                    ?>
                        </div>
                        <div align="right">
<?php
                            if ($setPagination) {
                                $array_to_be_included_in_pagination = array('moduleName' => $module, 'amount_footer' => true, 'actionName' => $actionTobeCalled, 'current_offset' => $current_offset, 'offset_next' => $offset_next, 'offset_prev' => $offset_prev, 'offset_last' => $offset_last);
                                include_partial('global/pagination', $array_to_be_included_in_pagination);
                            }
?>
                </div> 
                </div>    
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(
function(){
     $('form').submit();}
);
</script>