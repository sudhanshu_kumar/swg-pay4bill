<script type="text/javascript">
    $(document).ready(function(){

    
    $('iframe').load(
            function() {
                this.style.height = 0;
                this.style.width= 0;
                this.style.height = (this.contentWindow.document.body.scrollHeight-25) + 'px';
                this.style.width = '102%'; //(this.contentWindow.document.body.scrollWidth + 2000) + 'px';
            });
            
            /* The input parameters for jasper report */
        $('input[type=submit]').click(
            function(){
                $('#PARAM_invoice_number').val($('#invoice_no').val());    
                if($('#to_date').val().length > 0)
                    $('#PARAM_to_date').val($('#to_date').val());
                if($('#from_date').val().length > 0)
                    $('#PARAM_from_date').val($('#from_date').val());
            }
        )


        $('#Reports').addClass('active');
    });
</script>

<?php
include_partial('global/reportSubHeader', array('addClassTo' => $addClassTo));
?>
<?php
//}
?>
<!-- Varun : Flash messaged added -->
<?php
$sf = sfContext::getInstance()->getUser();
if ($sf->hasFlash('notice')) {
?>
    <div id="flash_notice" class="msg_serverside" >
    <?php
    echo nl2br($sf->getFlash('notice'));
    ?>
</div>
<?php } ?>
<div id="mainnew">
    <div id="wrappernew">
        <div id="content">
            <div id="Formwraper">
                <?php //if(isset($merchant_key) && $merchant_key!='') {   ?>
                <!-- Varun : Search section added to display search div-->
                <h3><b><?php echo $heading; ?></b></h3>
                <div style="clear:both"></div>
                <div class="TableForm" id="search">
<!--                    <form action="<?php //echo url_for($formResetAction); ?>" method="post" id="search_form"  > onsubmit="return validateSearch()"-->
                    <form  action ="<?php echo url_for('report/jasperReport')?>" method="get" id="search_form"  target="iframe_id">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">

                            <tr>
                                <td>Invoice Number<font color="red"></font></td>
                                <td>
                                    <?php 
                                    /* Check if the referr is from Payment By Customer by checking the dat_cat parameter */
                                    $date_cat = sfContext::getInstance()->getRequest()->getParameter('date_cat');
                                    ?>
                                    <input type="text" id="invoice_no" name="invoice_no"  value="<?php echo $invoice_no ?>">
                                                                       
                                    <input type="hidden" name="PARAM_customer_id" id="PARAM_customer_id" value="<?php echo $customer_id;?>"/>
                                    <input type="hidden" name="PARAM_to_date" id="PARAM_to_date" value="<?php echo $to_date?>"/>
                                    <input type="hidden" name="PARAM_from_date" id="PARAM_from_date" value="<?php echo $from_date?>"/>
                                    <input type="hidden" name="PARAM_invoice_number" id="PARAM_invoice_number" value="<?php echo $invoice_no?>"/>
                                       <?php  if ($adminLogin || $subadminLogin4Report) {?>                               
                                            
                                           <?php if($date_cat != 'payment_date'){ /*coming from Payment By Customer*/?>
                                                <input type="hidden" name="ReportName" value="Admin_ViewInvoice"/>
                                            <?php } else {?>    
                                                <input type="hidden" name="ReportName" value="Admin_ViewInvoice_PaymentByCustomer"/>
                                            <?php  } ?>    
                                           <input type="hidden" name="PARAM_merchant_name" id="PARAM_merchant_name" value="<?php echo $merchantName;?>"/>     
                                        <?php } else {?>  
                                            <?php if($date_cat != 'payment_date'){ /*coming from Payment By Customer*/?>
                                                <input type="hidden" name="ReportName" value="Merchant_ViewInvoice"/>
                                            <?php } else {?>    
                                                <input type="hidden" name="ReportName" value="Merchant_ViewInvoice_PaymentByCustomer"/>
                                            <?php  } ?>
                                            
                                            <input type="hidden" value="<?php echo sfContext::getInstance()->getUser()->getGuardUser()->getMerchantUser()->getId()?>" name="PARAM_merchant_id" id="PARAM_merchant_id" />
                                        <?php } ?> 
                                    
                                    
                                    <div class="red redlt" id="invoice_no_error"> </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" >
                                    <div class="Errormrg"><input type="submit" value="Search" class="blueBtn"  onclick="return validateSearch()" >
                                        <input type="button" class="blueBtn" value="Reset" onClick="window.location='<?php
                echo $formResetAction;
                ?>'"/>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
                
                <!--div style="float:left"><h3><b>Customer Id: </b><?php //echo $customer_id; ?></h3></div-->
                <?php /*if ($adminLogin) { ?> 
                    <div style="float:right"><h3><b>Merchant : </b><?php echo $merchantName; ?></h3></div>
                    <div style="clear:both"></div>  
                <?php }*/ ?>
                    
                <!--div style="float:right"><input type="button" name="Back" id="cancel_button"  class="blueBtn" value="Back" onclick="javascript:history.go(-1)"/></div-->

                

                                               <div id="if_error" ></div>
           
                            <div style="text-align:center;margin-top:100px" id='waitingDiv'><?php echo image_tag('loadingBar.gif', array('border'=>0,'alt'=>'Loading','title'=>'Loading'));?></div>
                            <iframe id="iframe_id" width="100%" scrolling="no" cellpadding="4" style="position:relative;right:16px;" name="iframe_id" src="<?php echo url_for('report/jasperReport')?>" frameborder="0"></iframe>                                               
                                  
                                
                            <div style="display:none">
                                               <div style="clear:both"></div>                                               
                                               <div class="TableForm">
                                                   <table>
                                                       <thead>
                                                           <tr>
                                                <?php
                                               $helperOb = new csvHelper();
                                                ?>
                                               <th>Invoice Number</th>
                                               <th><?php echo $amountColumnheading; ?>(<?php echo image_tag('naira.gif', array('align' => 'absmiddle')) ?>)</th>
                                               <th>Issue Date</th>
                                               <th>Delivery Date</th>
                                               <th>Due Date</th>
                                               <th>Payment Date</th>
                                               <th>Status</th>
                                           </tr>
                                       </thead>
                                       <tbody>
                                            <?php
                                               $total_amount_to_be_displayed = 0;
                                               if (count($invoices) > 0) {
                                                   $helperOb = new csvHelper();
                                                   $index_for_getting_invoice_amount = 0;
                                                   $i = 0;
                                                   foreach ($invoices as $invoice):
                                                      $i++;
                                            ?>
                                            <?php if ($i % 2 != 0) { ?>
                                                     <tr class="odd">
                                            <?php } else { ?>
                                                       <tr class="">
                                            <?php } ?>
                                                       <!-- View the invoice details 23-Feb-2012 -->
                                                       <!-- Varun : Encrypted id  -->
                                                       <td><a href="<?php echo url_for('invoice/show?id=' . cryptString::encrypt($invoice->getId())); ?>"><?php echo $invoice->getInvoiceNo(); ?></a></td>
                                                       <td align="right"><?php                                                       
                                                       if ($invoice->getStatus() == 0) {
                                                           echo $helperOb->price_format($invoice->getTotalAmount());
                                                           $total_amount_to_be_displayed+=$invoice->getTotalAmount();
                                                       } else {         //If Invoice is Paid/Partially Paid
                                                           $total_amount_to_be_displayed+=$invoice->$functionToGetAmount();
                                                           echo $helperOb->price_format($invoice->$functionToGetAmount());
                                                       }
                                                ?></td>
                                                   <td><?php echo $invoice->getIssueDate() ?></td>
                                                   <td><?php echo $invoice->getDeliveryDate() ?></td>
                                                   <td><?php echo $invoice->getDueDate() ?></td>
                                                   <td><?php
                                                       if ($invoice->getPaymentDate() == '') {
                                                           echo "N/A";
                                                       } else {
                                                           echo $invoice->getPaymentDate();
                                                       }
                                ?></td>
                                                   <td ><?php
                                                       if ($invoice->getStatus() == 3 && date('Y-m-d') > $invoice->getDueDate()) {
                                                           echo image_tag('partiallypaid_expired.png', array('alt' => "PartiallyPaid/Expired", 'title' => "PartiallyPaid/Expired"));
                                                       } else if (date('Y-m-d') > $invoice->getDueDate() && ($invoice->getStatus() == 0)) {
                                                           echo image_tag('expired.png', array('alt' => "Expired", 'title' => "Expired"));
                                                       } else if ($invoice->getStatus() == 3) {
                                                           echo image_tag('partially-paid.png', array('alt' => "Partially Paid", 'title' => "Partially Paid"));
                                                       } else if ($invoice->getStatus() == 0) {
                                                           echo image_tag('pending.png', array('alt' => "pending", 'title' => "pending"));
                                                       } else if ($invoice->getStatus() == 1) {
                                                           echo image_tag('cancel.png', array('alt' => "Cancel", 'title' => "Cancel"));
                                                       } else if ($invoice->getStatus() == 2) {
                                                           echo image_tag('paid.png', array('alt' => "Paid", 'title' => "Paid"));
                                                       }
                                ?></td>
                                               </tr>
                            <?php endforeach; ?>
                                                       <tr class="border-bottom">
                                                         <td align="left"><b>Amount<?php if ($setPagination) { ?><span class="sign">+</span><?php } ?></b></td>
                                                           <td align="right" ><b>
                                        <?php
                                                       echo $helperOb->price_format($total_amount_to_be_displayed);
                                        ?></b>
                                               </td>
                                               <td></td>
                                               <td></td><td></td>
                                               <td></td>
                                               <td></td>
                                           </tr>
                            <?php
                                                   } else {
                                                       echo "<tr><td colspan='9' align='center'>No Invoice Found</td></tr>";
                                                   }
                            ?>
                                               </tbody>
                                           </table>

                                       </div>
                                       <div class="nofoList">
                    <?php
                                                   include_partial('global/records_number', array('amount_footer' => true, 'current_offset' => isset($current_offset) ? $current_offset : null, 'offset_next' => isset($offset_next) ? $offset_next : null, 'total_no_of_records' => isset($total_no_of_records) ? $total_no_of_records : count($invoices), 'no_of_records_per_page' => isset($no_of_records_per_page) ? $no_of_records_per_page : null, 'from' => '', 'total_amount_of_all_transactions' => isset($total_amount_of_all_transactions) ? $total_amount_of_all_transactions : $total_amount_to_be_displayed));
                    ?>
                                               </div>
                <?php if ($setPagination) {
 ?>
                                                       <div align="right">
                    <?php
                                                       include_partial('global/pagination', array('moduleName' => $module, 'amount_footer' => true, 'actionName' => $actionTobeCalled, 'current_offset' => $current_offset, 'offset_next' => $offset_next, 'offset_prev' => $offset_prev, 'offset_last' => $offset_last, 'from' => $from_date, 'to' => $to_date, 'customer_id' => $customer_id, 'date_cat' => $date_cat, 'invoice_no' => $invoice_no, 'invoice_status' => $status, 'partlyPaid' => $partlypaid, 'both' => $both));
                    ?></div><?php
                                                   }
                    ?>
                                  
                            </div>
                                               </div>
                                           </div>
                                       </div>
                                   </div> 
<script>
$(document).ready(
function(){
     $('form').submit();}
);
</script>