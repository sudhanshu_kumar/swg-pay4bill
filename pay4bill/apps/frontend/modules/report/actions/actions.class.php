<?php
/**
 * report actions.
 *
 * @package    nisp
 * @subpackage report
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class reportActions extends sfActions {

    /**
     * Executes index action
     *
     * @param sfRequest $request A request object
     */
    public function executeIndex(sfWebRequest $request) {
        //Php Ini setting Varun : Bug fix :FS#34920
        ini_set('max_execution_time', '2000');
        ini_set('memory_limit', '256M');
        $this->setLayout('permissionsLayout');
    }

    public function executePaidInvoicesForMerchant(sfWebRequest $request) {

        $allParams = $request->getParameterHolder()->getAll();
        //Varun : Offset value to unset session
        $offset = $request->getParameter('offset');
//        print_r($allParams);
        if (array_key_exists('search', $allParams) && $allParams['search'] != '') {
            $searchKey = $allParams['search'];
        } else {
            $searchKey = null;
        }
        //Varun : Condition added for email address and cust name
        if (!isset($_SESSION['email_address'])) {
            $_SESSION['email_address'] = '';
        }
        if (!isset($_SESSION['cust_name'])) {
            $_SESSION['cust_name'] = '';
        }
        if (!isset($_SESSION['invoice_no'])) {
            $_SESSION['invoice_no'] = '';
        }
        if (!isset($_SESSION['search_par'])) {
            $_SESSION['search_par'] = '';
        }
        if (array_key_exists('email_address', $allParams) && $allParams['email_address'] != '') {
            if ($request->getParameter('offset') > 0) {
                $email_address = cryptString::decrypt($request->getParameter('email_address'));
            } else {
                $email_address = $allParams['email_address'];
            }
        } else {
            $email_address = '';
        }
        if (array_key_exists('cust_name', $allParams) && $allParams['cust_name'] != '') {
            if ($request->getParameter('offset') > 0) {
                $cust_name = cryptString::decrypt($request->getParameter('cust_name'));
            } else {
                $cust_name = $allParams['cust_name'];
            }
        } else {
            $cust_name = '';
        }
        if (array_key_exists('search_par', $allParams) && $allParams['search_par'] != '') {
            if ($request->getParameter('offset') > 0) {
                $search_par = cryptString::decrypt($request->getParameter('search_par'));
            } else {
                $search_par = $allParams['search_par'];
            }
        } else {
            $search_par = '';
        }
        if ($request->isMethod('post')) {

            $_SESSION['email_address'] = $email_address;
            $_SESSION['cust_name'] = $cust_name;
            $_SESSION['search_par'] = $search_par;
            $_SESSION['search_text'] = $searchKey;
        } else {

            //Varun :Session will be unset for the first time page opens
            if (!isset($offset)) {
                if (isset($email_address) && $email_address != '') {
                    $_SESSION['email_address'] = $email_address;
                } else {
                    $_SESSION['email_address'] = NULL;
                }
                if (isset($cust_name) && $cust_name != '') {
                    $_SESSION['cust_name'] = $cust_name;
                } else {
                    $_SESSION['cust_name'] = NULL;
                }
                if (isset($searchKey) && $searchKey != '') {
                    $_SESSION['search_text'] = $searchKey;
                } else {
                    $_SESSION['search_text'] = NULL;
                }
                if (isset($search_par) && $search_par != '') {
                    $_SESSION['search_par'] = $search_par;
                } else {
                    $_SESSION['search_par'] = NULL;
                }
            } else {
                $email_address = (isset($_SESSION['email_address'])) ? $_SESSION['email_address'] : '';
                $cust_name = (isset($_SESSION['cust_name'])) ? $_SESSION['cust_name'] : '';
                $search_par = (isset($_SESSION['search_par'])) ? $_SESSION['search_par'] : '';
                $searchKey = (isset($_SESSION['search_text'])) ? $_SESSION['search_text'] : '';
            }
        }
        $this->form = new InvoiceFormFilter();
        if (array_key_exists('invoice_filters', $allParams) && ($allParams['invoice_filters']['from']['day'] != '' || $allParams['invoice_filters']['from']['day'] != '')) {
            /**
             * Manu : Search date display in case of page refresh
             */
            if ($allParams['invoice_filters']['to']['day'] != '') {
                $to_date = date("Y-m-d", strtotime($allParams['invoice_filters']['to']['year'] . "-" . $allParams['invoice_filters']['to']['month'] . "-" . $allParams['invoice_filters']['to']['day']));
            }
            if ($allParams['invoice_filters']['from']['day'] != '') {
                $fromdate = date("Y-m-d", strtotime($allParams['invoice_filters']['from']['year'] . "-" . $allParams['invoice_filters']['from']['month'] . "-" . $allParams['invoice_filters']['from']['day']));
            }
            $dateCat = $allParams['date_category'];
        } elseif (array_key_exists('to', $allParams) && $allParams['to'] != '') {
            $this->setTemplate('print');
            $to_date = date("Y-m-d", strtotime($allParams['to']));

            $dateCat = $allParams['date_cat'];
            $fromdate = date("Y-m-d", strtotime($allParams['from']));
        }
        $this->searchKey = (isset($searchKey)) ? $searchKey : null;
        $this->email_address = (isset($email_address)) ? $email_address : '';
        $this->cust_name = (isset($cust_name)) ? $cust_name : '';
        $this->search_par = (isset($search_par)) ? $search_par : '';

        $this->to_date = (isset($to_date)) ? $to_date : null;
        $this->from_date = (isset($fromdate)) ? $fromdate : null;
        $this->date_cat = (isset($dateCat)) ? $dateCat : null;
        $this->class_on = $this->getActionName();
//Varun : Check for authentication and redirect to report if session doesn't exist
        if ($this->getUser()->isAuthenticated()) {
            $invoices = Doctrine::getTable('Invoice')->getPaidInvoicesForMerchant($this->getUser()->getGuardUser()->getMerchantUser()->getId(), $this->searchKey, $this->from_date, $this->to_date, $this->date_cat, '', '', $this->email_address, $this->cust_name);
            $total_no_of_records = count($invoices);
            /**
             * Manu : Replace of check filename place of filetype(filename is common parameter pdf,csv and xls)
             */
            if ($total_no_of_records > sfConfig::get('app_records_per_page') && !array_key_exists('filename', $allParams) && !array_key_exists('print', $allParams) && !array_key_exists('pdf', $allParams)) {

                $this->actionTobeCalled = $this->getActionName();
                $this->total_no_of_records = $total_no_of_records;
                $this->no_of_records_per_page = sfConfig::get('app_records_per_page');
                $this->setPagination = true;
                $this->total_amount_of_all_transactions = $this->getTotalofAllInvoices($invoices);

                if (!$request->hasParameter('offset')) {
                    $offset = 0;
                } else {
                    $offset = $request->getParameter('offset');
                }
                $this->current_offset = $offset;
                $invoices = Doctrine::getTable('Invoice')->getPaidInvoicesForMerchant($this->getUser()->getGuardUser()->getMerchantUser()->getId(), $this->searchKey, $this->from_date, $this->to_date, $this->date_cat, $offset, sfConfig::get('app_records_per_page'), $this->email_address, $this->cust_name);

                $this->actionTobeCalled = $this->getActionName();
                $this->module = $this->getModuleName();
                $this->offset_last = (($total_no_of_records % sfConfig::get('app_records_per_page') == 0 ? ($total_no_of_records - sfConfig::get('app_records_per_page') ) : $total_no_of_records - ($total_no_of_records % sfConfig::get('app_records_per_page'))));

                $this->offset_next = (($offset + sfConfig::get('app_records_per_page')) > $this->offset_last ? $this->offset_last : ($offset + sfConfig::get('app_records_per_page')));

                $this->offset_prev = (($offset - sfConfig::get('app_records_per_page')) < 0 ? 0 : $offset - sfConfig::get('app_records_per_page'));
            } else {
                $this->setPagination = false;
            }
            $id_Items_invoiceId = $this->construct_invoice_itemId_array($invoices);



            $item_details = !is_null($id_Items_invoiceId) ? Doctrine::getTable('InvoiceItemDetails')->getItemDetailsForInvoices(array_unique(array_values($id_Items_invoiceId))) : null;
            $this->id_Items_invoiceId = $id_Items_invoiceId;
            $this->invoices = $invoices;
            $this->item_details = $item_details;
            if (array_key_exists('print', $allParams)) {

                $this->setTemplate('print');
                $this->pgTitle = "Paid " . $allParams['pgTitle'];
                $this->to_be_printed = $allParams['to_be_printed'];

                $this->setLayout('layout_for_print');
            } else {

                /**
                 * Manu : Case added for xls and csv download
                 */
                if ($allParams['action'] === 'export' || $allParams['action'] === 'pdf') {//Manu : In case of PDF
                    if ($allParams['action'] === 'pdf') {
                        $allParams['filename'] = $allParams['filename'] . '_' . time();
                        $allParams['fileType'] = 'pdf';
                    }
                    if ($allParams['fileType'] === 'xls') {
                        $allParams['filename'] = $allParams['filename'] . '_' . time();
                        $allParams['fileType'] = 'xls';
                    }
                    if ($allParams['fileType'] === 'csv') {
                        $allParams['filename'] = $allParams['filename'] . '_' . time();
                        $allParams['fileType'] = 'csv';
                    }
                    $this->createFile_to_be_exported($invoices, $item_details, $id_Items_invoiceId, $allParams['filename'], $allParams['fileType']);
                    /**
                     * Manu : Close functionality , Move on Templates
                     */
                    exit;
                }
                $this->setTemplate('index');
            }
        } else {
            $this->redirect('report/');
        }
    }

    public function executeViewInvoices(sfWebRequest $request) {
        $this->adminLogin = false;
        $this->class_on = $this->getActionName();
        $this->search_form = new InvoiceSearchForm();
        $allParams = $request->getParameterHolder()->getAll();

        $offset = $request->getParameter('offset');

        $helperObj = new pay4billHelper();
        $parameters = $helperObj->trimArray($allParams);

        $this->adminLogin = (pay4billHelper::checkIfAdminUser($this->getUser()->getGuardUser()->getGroups()->toArray()) || pay4billHelper::checkIfSubAdminReportUser($this->getUser()->getGuardUser()->getGroups()->toArray()));
        $this->subadminLogin4Report = pay4billHelper::checkIfSubAdminReportUser($this->getUser()->getGuardUser()->getGroups()->toArray());
        //  $this->form = new InvoiceFormFilter();


        $this->invoice_no = $request->hasParameter('invoice_no') ? $parameters['invoice_no'] : '';

        $this->customer_id = $request->hasParameter('customer_id') ? $parameters['customer_id'] : '';
        $this->merchantName = Doctrine::getTable('CustomerProfile')->findByCustomerId($this->customer_id)->getFirst()->getMerchantProfile()->getMerchantName();
        $this->status = $parameters['status'];
        $this->expired = ($request->hasParameter('expired') && $parameters['expired'] == 1) ? true : false;

        $this->customer_id = $parameters['customer_id'];
        $this->both = 0;
        $this->partlypaid = 0;
        $this->heading = '';
        $this->amountColumnheading = '';
        $this->functionToGetAmount = '';
        if ($request->hasParameter('both') && $parameters['both'] == 1) {
            $this->heading = "Unpaid Invoices";
            $this->footerNote = "This list includes Pending,Expired and Partially Paid Invoices";
            $this->getUser()->setAttribute('allUnpaid', 1);
            $this->functionToGetAmount = $functionToGetAmount = "getAmountDue";
            $this->both = 1;
            $this->status = 0;
            $this->date_cat = 'delivery_date';
            $this->amountColumnheading = "Amount Due";
            $this->addClassTo = 'customersWithPendingInvoices';
        } else {


            $this->getUser()->getAttributeHolder()->remove('allUnpaid');
        }
        if ($request->hasParameter('partlypaid') && $parameters['partlypaid'] == 1) {
            $this->heading = "Paid Invoices";
            $this->footerNote = "This list includes Paid and Partially Paid Invoices";
            $this->getUser()->setAttribute('allPaid', 1);
            $this->functionToGetAmount = $functionToGetAmount = "getAmountPaid";
            $this->partlypaid = 1;
            $this->status = 2;
            $this->date_cat = 'payment_date';
            $this->amountColumnheading = "Amount Paid";
            $this->addClassTo = 'amountPaidByCustomers';
        } else {


            $this->getUser()->getAttributeHolder()->remove('allPaid');
        }
        sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url'));
        $this->from_date = (array_key_exists('from', $parameters) && $parameters['from'] != '') ? $parameters['from'] : null;
        $this->to_date = (array_key_exists('to', $parameters) && $parameters['to'] != '') ? $parameters['to'] : null;
        $this->formResetAction = url_for($this->getModuleName() . "/" . $this->getActionName()) . "?customer_id=" . $this->customer_id . "&status=" . $this->status . "&partlypaid=" . $this->partlypaid . "&both=" . $this->both . "&from=" . $this->from_date . "&to=" . $this->to_date . "&date_cat=payment_date";

        $invoices = Doctrine::getTable('Invoice')->getAllInvoiceForMerchantReport($this->getUser()->getGuardUser()->getMerchantUser()->getId(), $this->invoice_no, $this->from_date, $this->to_date, $this->date_cat, $this->cust_name, $this->customer_id, $this->status, $this->expired, $this->com_name);


        $total_no_of_records = count($invoices);

        if ($total_no_of_records > sfConfig::get('app_records_per_page') && !array_key_exists('filename', $allParams) && !array_key_exists('print', $allParams) && !array_key_exists('pdf', $allParams)) {
            foreach ($invoices as $obj) {
                if ($obj->getStatus() == 0 && $functionToGetAmount == "getAmountDue") {
                    $this->total_amount_of_all_transactions+=$obj->getTotalAmount();
                } else {

                    $this->total_amount_of_all_transactions+=$obj->$functionToGetAmount();
                }
            }

            $this->actionTobeCalled = $this->getActionName();
            $this->total_no_of_records = $total_no_of_records;
            $this->no_of_records_per_page = sfConfig::get('app_records_per_page');
            $this->setPagination = true;


            if (!$request->hasParameter('offset')) {
                $offset = 0;
            } else {
                $offset = $request->getParameter('offset');
            }
            $this->current_offset = $offset;

            $invoices = Doctrine::getTable('Invoice')->getAllInvoiceForMerchantReport($this->getUser()->getGuardUser()->getMerchantUser()->getId(), $this->invoice_no, $this->from_date, $this->to_date, $this->date_cat, $this->cust_name, $this->customer_id, $this->status, $this->expired, $this->com_name, $offset, sfConfig::get('app_records_per_page'));
            $this->actionTobeCalled = $this->getActionName();
            $this->module = $this->getModuleName();
            $this->offset_last = (($total_no_of_records % sfConfig::get('app_records_per_page') == 0 ? ($total_no_of_records - sfConfig::get('app_records_per_page') ) : $total_no_of_records - ($total_no_of_records % sfConfig::get('app_records_per_page'))));

            $this->offset_next = (($offset + sfConfig::get('app_records_per_page')) > $this->offset_last ? $this->offset_last : ($offset + sfConfig::get('app_records_per_page')));

            $this->offset_prev = (($offset - sfConfig::get('app_records_per_page')) < 0 ? 0 : $offset - sfConfig::get('app_records_per_page'));
        } else {
            $this->setPagination = false;
        }
        if ($total_no_of_records > 0) {
            $id_Items_invoiceId = $this->construct_invoice_itemId_array($invoices);

            $item_details = !is_null($id_Items_invoiceId) ? Doctrine::getTable('InvoiceItemDetails')->getItemDetailsForInvoices(array_unique(array_values($id_Items_invoiceId))) : null;
        }
        $this->id_Items_invoiceId = isset($id_Items_invoiceId) ? $id_Items_invoiceId : null;
        $this->invoices = $invoices;

        $this->item_details = isset($item_details) ? $item_details : null;
        $merchnatLogin = false;
        $admin_login = false;
        foreach (sfContext::getInstance()->getUser()->getGuardUser()->getGroups()->toArray() as $group_array) {


            if ($group_array['id'] == 2) {//Customer
                $admin_login = false;
                $merchnatLogin = false;
            } else if ($group_array['id'] == 1) {//Merchant
                $admin_login = false;
                $merchnatLogin = true;
            } else if ($group_array['id'] == 3) { //SuperAdmin
                $admin_login = true;
            } else if ($group_array['id'] == 4) { //Varun : Conditiob for report admin
                $admin_login = true;
            }
        }
        $this->merchnatLogin = $merchnatLogin;
        $this->admin_login = $admin_login;

        if (array_key_exists('print', $allParams)) {

            $this->setTemplate('print');
            $this->to_be_printed = $allParams['to_be_printed'];

            $this->setLayout('layout_for_print');
            $this->pgTitle = "All " . $allParams['pgTitle'];
        } else {
            /**
             * Varun : Case added for xls and csv download
             */
            if ($allParams['action'] === 'export' || $allParams['action'] === 'pdf') {//Manu : In case of PDF
                if ($allParams['action'] === 'pdf') {
                    $allParams['filename'] = $allParams['filename'] . '_' . time();
                    $allParams['fileType'] = 'pdf';
                }
                if ($allParams['fileType'] === 'xls') {
                    $allParams['filename'] = $allParams['filename'] . '_' . time();
                    $allParams['fileType'] = 'xls';
                }
                if ($allParams['fileType'] === 'csv') {
                    $allParams['filename'] = $allParams['filename'] . '_' . time();
                    $allParams['fileType'] = 'csv';
                }

                $this->createFile_to_be_exported($invoices, $item_details, $id_Items_invoiceId, $allParams['filename'], $allParams['fileType']);
                /**
                 * Manu : Close functionality , Move on Templates
                 */
                exit;
            }
        }
        $this->showPartiallyPaidInvoicesSearch = true;
        $helper = new pay4billHelper();
        $result = $helper->checkMerchantPaymentType($this->getUser()->getGuardUser()->getMerchantUser()->getId());

        switch ($result) {
            case 'o':
                $this->showPartiallyPaidInvoicesSearch = true;
                break;
            case 'b':
                $this->showPartiallyPaidInvoicesSearch = false;
                break;
        }
    }

    public function executeAllInvoicesForMerchant(sfWebRequest $request) {
        set_time_limit(0);
        ini_set('max_execution_time', '2000');
        ini_set('memory_limit', '256M');

        $this->class_on = $this->getActionName();
        $this->search_form = new InvoiceSearchForm();
        $allParams = $request->getParameterHolder()->getAll();

        $offset = $request->getParameter('offset');

        $helperObj = new pay4billHelper();
        $parameters = $helperObj->trimArray($allParams);

        $this->cust_name = '';
        if (array_key_exists('cust_name', $parameters) && $parameters['cust_name'] != '') {

            $this->cust_name = $parameters['cust_name'];
        }

        $this->adminLogin           = pay4billHelper::checkIfAdminUser($this->getUser()->getGuardUser()->getGroups()->toArray());
        $this->subadminLogin4Report = pay4billHelper::checkIfSubAdminReportUser($this->getUser()->getGuardUser()->getGroups()->toArray());

        $this->form = new InvoiceFormFilter();

        if (array_key_exists('invoice_filters', $parameters) && ($parameters['invoice_filters']['from']['day'] != '' || $parameters['invoice_filters']['from']['day'] != '')) {
            /**
             * Manu : Search date display in case of page refresh
             */
            if ($parameters['invoice_filters']['to']['day'] != '') {
                $to_date = date("Y-m-d", strtotime($parameters['invoice_filters']['to']['year'] . "-" . $parameters['invoice_filters']['to']['month'] . "-" . $parameters['invoice_filters']['to']['day']));
            }
            if ($parameters['invoice_filters']['from']['day'] != '') {
                $fromdate = date("Y-m-d", strtotime($parameters['invoice_filters']['from']['year'] . "-" . $parameters['invoice_filters']['from']['month'] . "-" . $parameters['invoice_filters']['from']['day']));
            }
            $dateCat = $parameters['date_category'];
        } elseif (array_key_exists('to', $parameters) && $parameters['to'] != '') {
            $to_date = date("Y-m-d", strtotime($parameters['to']));

            $dateCat = $parameters['date_cat'];
            $fromdate = date("Y-m-d", strtotime($parameters['from']));
        }
        $this->queryStringForStatusSearch = '&';

        if (!empty($this->cust_name)) {
            $this->queryStringForStatusSearch.='cust_name=' . $this->cust_name . '&';
        }

//        $this->search_par = (isset($search_par)) ? $search_par : null;
        $this->to_date = (isset($to_date)) ? $to_date : null;
        if (!is_null($this->to_date)) {
            $this->queryStringForStatusSearch.='to=' . $this->to_date . '&';
        }
        if (!is_null($this->from_date)) {
            $this->queryStringForStatusSearch.='from=' . $this->from_date . '&';
        }
        $this->from_date = (isset($fromdate)) ? $fromdate : null;
        if (!is_null($this->from_date)) {
            $this->queryStringForStatusSearch.='from=' . $this->from_date . '&';
        }
        $this->date_cat = (isset($dateCat)) ? $dateCat : null;

        if (!is_null($this->date_cat)) {
            $this->queryStringForStatusSearch.='date_cat=' . $this->date_cat . '&';
        }
        $this->invoice_no = $request->hasParameter('invoice_no') ? $parameters['invoice_no'] : '';
        if (!empty($this->invoice_no)) {
            $this->queryStringForStatusSearch.='invoice_no=' . $this->invoice_no . '&';
        }

        $this->customer_id = $request->hasParameter('customer_id') ? $parameters['customer_id'] : '';
        if ($this->customer_id != '')
            $this->queryStringForStatusSearch.='customer_id=' . $this->customer_id . '&';

        $this->status = $request->getParameter('status');
        
        $this->expired = $request->getParameter('expired') == 1 ? true : false;
        
        if($this->status == 0 && $this->expired == true){
            $this->jasperStatus = 'expired';
        } else if(strlen($this->status)>0){ 
            $this->jasperStatus = 'pending';
        }
        
        if($this->status == 3 && $this->expired == true){
            $this->jasperStatus = 'partialexpired';
        } else if($this->status == 3) {
            if($this->status == 3) $this->jasperStatus = 'partialpaid';
        }
        
        if($this->status == 1) $this->jasperStatus = 'cancelled';
        if($this->status == 2) $this->jasperStatus = 'paid';
        //if($this->status == 3) $this->jasperStatus = 'partialpaid';
        
       

        $this->com_name = $request->hasParameter('com_name') ? $parameters['com_name'] : '';
        $this->both = 0;
        $this->partlypaid = 0;
        if (!empty($this->com_name)) {
            $this->queryStringForStatusSearch.='com_name=' . $this->com_name . '&';
        }
        if ($request->hasParameter('both') && $request->getParameter('both') == 1) {
            $this->getUser()->setAttribute('allUnpaid', 1);
            $this->both = 1;
        } else {


            $this->getUser()->getAttributeHolder()->remove('allUnpaid');
        }
        if ($request->hasParameter('partlypaid') && $request->getParameter('partlypaid') == 1) {
            $this->getUser()->setAttribute('allPaid', 1);
            $this->partlypaid = 1;
        } else {


            $this->getUser()->getAttributeHolder()->remove('allPaid');
        }

        $invoices = Doctrine::getTable('Invoice')->getAllInvoiceForMerchantReport($this->getUser()->getGuardUser()->getMerchantUser()->getId(), $this->invoice_no, $this->from_date, $this->to_date, $this->date_cat, $this->cust_name, $this->customer_id, $this->status, $this->expired, $this->com_name);

//        echo "<pre>";
//        print_r($invoices->toArray());
//        exit;
        $total_no_of_records = count($invoices);
        /**
         * Manu : Replace of check filename place of filetype(filename is common parameter pdf,csv and xls)
         */
        if ($total_no_of_records > sfConfig::get('app_records_per_page') && !array_key_exists('filename', $allParams) && !array_key_exists('print', $allParams) && !array_key_exists('pdf', $allParams)) {
            $this->actionTobeCalled = $this->getActionName();
            $this->total_no_of_records = $total_no_of_records;
            $this->no_of_records_per_page = sfConfig::get('app_records_per_page');
            $this->setPagination = true;
            $this->total_amount_of_all_transactions = $this->getTotalofAllInvoices($invoices);

            if (!$request->hasParameter('offset')) {
                $offset = 0;
            } else {
                $offset = $request->getParameter('offset');
            }
            $this->current_offset = $offset;

            $invoices = Doctrine::getTable('Invoice')->getAllInvoiceForMerchantReport($this->getUser()->getGuardUser()->getMerchantUser()->getId(), $this->invoice_no, $this->from_date, $this->to_date, $this->date_cat, $this->cust_name, $this->customer_id, $this->status, $this->expired, $this->com_name, $offset, sfConfig::get('app_records_per_page'));
            $this->actionTobeCalled = $this->getActionName();
            $this->module = $this->getModuleName();
            $this->offset_last = (($total_no_of_records % sfConfig::get('app_records_per_page') == 0 ? ($total_no_of_records - sfConfig::get('app_records_per_page') ) : $total_no_of_records - ($total_no_of_records % sfConfig::get('app_records_per_page'))));

            $this->offset_next = (($offset + sfConfig::get('app_records_per_page')) > $this->offset_last ? $this->offset_last : ($offset + sfConfig::get('app_records_per_page')));

            $this->offset_prev = (($offset - sfConfig::get('app_records_per_page')) < 0 ? 0 : $offset - sfConfig::get('app_records_per_page'));
        } else {
            $this->setPagination = false;
        }
        if ($total_no_of_records > 0) {
            $id_Items_invoiceId = $this->construct_invoice_itemId_array($invoices);

            $item_details = !is_null($id_Items_invoiceId) ? Doctrine::getTable('InvoiceItemDetails')->getItemDetailsForInvoices(array_unique(array_values($id_Items_invoiceId))) : null;
        }
        $this->id_Items_invoiceId = isset($id_Items_invoiceId) ? $id_Items_invoiceId : null;
        $this->invoices = $invoices;

        $this->item_details = isset($item_details) ? $item_details : null;
        $merchnatLogin = false;
        $admin_login = false;
        foreach (sfContext::getInstance()->getUser()->getGuardUser()->getGroups()->toArray() as $group_array) {


            if ($group_array['id'] == 2) {//Customer
                $admin_login = false;
//                $merchnatLogin = false;
            } else if ($group_array['id'] == 1) {//Merchant
                $admin_login = false;
//                $merchnatLogin = true;
            } else if ($group_array['id'] == 3) { //SuperAdmin
                $admin_login = true;
            } else if ($group_array['id'] == 4) { //Varun : Conditiob for report admin
                $admin_login = true;
            }
        }
        $merchnatLogin = pay4billHelper::checkIfMerchantUser(sfContext::getInstance()->getUser()->getGuardUser()->getGroups()->toArray());
        $this->merchnatLogin = $merchnatLogin;
        $this->admin_login = $admin_login;

        if (array_key_exists('print', $allParams)) {

            $this->setTemplate('print');
            $this->to_be_printed = $allParams['to_be_printed'];

            $this->setLayout('layout_for_print');
            $this->pgTitle = "All " . $allParams['pgTitle'];
        } else {
            /**
             * Varun : Case added for xls,pdf and csv download
             */
            if ($allParams['action'] === 'export' || $allParams['action'] === 'pdf') {//Manu : In case of PDF
                if ($allParams['action'] === 'pdf') {
                    $allParams['filename'] = $allParams['filename'] . '_' . time();
                    $allParams['fileType'] = 'pdf';
                }
                if ($allParams['fileType'] === 'xls') {
                    $allParams['filename'] = $allParams['filename'] . '_' . time();
                    $allParams['fileType'] = 'xls';
                }
                if ($allParams['fileType'] === 'csv') {
                    $allParams['filename'] = $allParams['filename'] . '_' . time();
                    $allParams['fileType'] = 'csv';
                }

                $this->createFile_to_be_exported($invoices, $item_details, $id_Items_invoiceId, $allParams['filename'], $allParams['fileType']);
                /**
                 * Manu : Close functionality , Move on Templates
                 */
                exit;
            }

            $this->setTemplate('index');
        }
        $this->showPartiallyPaidInvoicesSearch = true;
        $helper = new pay4billHelper();
        $result = $helper->checkMerchantPaymentType($this->getUser()->getGuardUser()->getMerchantUser()->getId());

        switch ($result) {
            case 'o':
                $this->showPartiallyPaidInvoicesSearch = true;
                break;
            case 'b':
                $this->showPartiallyPaidInvoicesSearch = false;
                break;
        }
        $this->addClassTo = 'index';
    }

    public function executePendingInvoicesForMerchant(sfWebRequest $request) {
        $this->merchnatLogin = true;
        $this->class_on = $this->getActionName();
        $allParams = $request->getParameterHolder()->getAll();
        $this->search_form = new InvoiceSearchForm();
        //Varun : Offset value to unset session
        $offset = $request->getParameter('offset');


        //Varun : Condition added for email address and cust name

        if (!isset($_SESSION['cust_name'])) {
            $_SESSION['cust_name'] = '';
        }

        if (!isset($_SESSION['invoice_no'])) {
            $_SESSION['invoice_no'] = '';
        }

        if (array_key_exists('cust_name', $allParams) && $allParams['cust_name'] != '') {
            if ($request->getParameter('offset') > 0) {
                $cust_name = cryptString::decrypt($request->getParameter('cust_name'));
            } else {
                $cust_name = $allParams['cust_name'];
            }
        } else {
            $cust_name = '';
        }
        if (array_key_exists('search_par', $allParams) && $allParams['search_par'] != '') {
            if ($request->getParameter('offset') > 0) {
                $search_par = cryptString::decrypt($request->getParameter('search_par'));
            } else {
                $search_par = $allParams['search_par'];
            }
        } else {
            $search_par = '';
        }
        if ($request->isMethod('post')) {

            $_SESSION['email_address'] = $email_address;
            $_SESSION['cust_name'] = $cust_name;
            $_SESSION['search_par'] = $search_par;
            $_SESSION['search_text'] = $searchKey;
        } else {

            //Varun :Session will be unset for the first time page opens
            if (!isset($offset)) {
                if (isset($email_address) && $email_address != '') {
                    $_SESSION['email_address'] = $email_address;
                } else {
                    $_SESSION['email_address'] = NULL;
                }
                if (isset($cust_name) && $cust_name != '') {
                    $_SESSION['cust_name'] = $cust_name;
                } else {
                    $_SESSION['cust_name'] = NULL;
                }
                if (isset($search_par) && $search_par != '') {
                    $_SESSION['search_par'] = $search_par;
                } else {
                    $_SESSION['search_par'] = NULL;
                }
                if (isset($searchKey) && $searchKey != '') {
                    $_SESSION['search_text'] = $searchKey;
                } else {
                    $_SESSION['search_text'] = NULL;
                }
            } else {
                $email_address = (isset($_SESSION['email_address'])) ? $_SESSION['email_address'] : '';
                $cust_name = (isset($_SESSION['cust_name'])) ? $_SESSION['cust_name'] : '';
                $search_par = (isset($_SESSION['search_par'])) ? $_SESSION['search_par'] : '';
                $searchKey = (isset($_SESSION['search_text'])) ? $_SESSION['search_text'] : '';
            }
        }
        $this->form = new InvoiceFormFilter();
        if (array_key_exists('invoice_filters', $allParams) && ($allParams['invoice_filters']['from']['day'] != '' || $allParams['invoice_filters']['from']['day'] != '')) {
            /**
             * Manu : Search date display in case of page refresh
             */
            if ($allParams['invoice_filters']['to']['day'] != '') {
                $to_date = date("Y-m-d", strtotime($allParams['invoice_filters']['to']['year'] . "-" . $allParams['invoice_filters']['to']['month'] . "-" . $allParams['invoice_filters']['to']['day']));
            }
            if ($allParams['invoice_filters']['from']['day'] != '') {
                $fromdate = date("Y-m-d", strtotime($allParams['invoice_filters']['from']['year'] . "-" . $allParams['invoice_filters']['from']['month'] . "-" . $allParams['invoice_filters']['from']['day']));
            }
            $dateCat = $allParams['date_category'];
        } elseif (array_key_exists('to', $allParams) && $allParams['to'] != '') {
            $this->setTemplate('print');
            $to_date = date("Y-m-d", strtotime($allParams['to']));

            $dateCat = $allParams['date_cat'];
            $fromdate = date("Y-m-d", strtotime($allParams['from']));
        }
        $this->searchKey = (isset($searchKey)) ? $searchKey : null;
        $this->email_address = (isset($email_address)) ? $email_address : '';
        $this->cust_name = (isset($cust_name)) ? $cust_name : '';
        $this->search_par = (isset($search_par)) ? $search_par : '';

        $this->to_date = (isset($to_date)) ? $to_date : null;
        $this->from_date = (isset($fromdate)) ? $fromdate : null;
        $this->date_cat = (isset($dateCat)) ? $dateCat : null;
//Varun : Check for authentication and redirect to report if session doesn't exist
        if ($this->getUser()->isAuthenticated()) {
            $invoices = Doctrine::getTable('Invoice')->getPendingInvoicesForMerchant($this->getUser()->getGuardUser()->getMerchantUser()->getId(), $this->searchKey, $this->from_date, $this->to_date, $this->date_cat, '', '', $this->email_address, $this->cust_name);
            $total_no_of_records = count($invoices);
            /**
             * Manu : Replace of check filename place of filetype(filename is common parameter pdf,csv and xls)
             */
            if ($total_no_of_records > sfConfig::get('app_records_per_page') && !array_key_exists('filename', $allParams) && !array_key_exists('print', $allParams) && !array_key_exists('pdf', $allParams)) {

                $this->actionTobeCalled = $this->getActionName();
                $this->total_no_of_records = $total_no_of_records;
                $this->no_of_records_per_page = sfConfig::get('app_records_per_page');
                $this->setPagination = true;
                $this->total_amount_of_all_transactions = $this->getTotalofAllInvoices($invoices);

                if (!$request->hasParameter('offset')) {
                    $offset = 0;
                } else {

                    $offset = $request->getParameter('offset');
                }
                $this->current_offset = $offset;
                $invoices = Doctrine::getTable('Invoice')->getPendingInvoicesForMerchant($this->getUser()->getGuardUser()->getMerchantUser()->getId(), $this->searchKey, $this->from_date, $this->to_date, $this->date_cat, $offset, sfConfig::get('app_records_per_page'), $this->email_address, $this->cust_name);

                $this->actionTobeCalled = $this->getActionName();
                $this->module = $this->getModuleName();
                $this->offset_last = (($total_no_of_records % sfConfig::get('app_records_per_page') == 0 ? ($total_no_of_records - sfConfig::get('app_records_per_page') ) : $total_no_of_records - ($total_no_of_records % sfConfig::get('app_records_per_page'))));

                $this->offset_next = (($offset + sfConfig::get('app_records_per_page')) > $this->offset_last ? $this->offset_last : ($offset + sfConfig::get('app_records_per_page')));

                $this->offset_prev = (($offset - sfConfig::get('app_records_per_page')) < 0 ? 0 : $offset - sfConfig::get('app_records_per_page'));
            } else {
                $this->setPagination = false;
            }
            $id_Items_invoiceId = $this->construct_invoice_itemId_array($invoices);


            $item_details = !is_null($id_Items_invoiceId) ? Doctrine::getTable('InvoiceItemDetails')->getItemDetailsForInvoices(array_unique(array_values($id_Items_invoiceId))) : null;
            $this->id_Items_invoiceId = $id_Items_invoiceId;
            $this->invoices = $invoices;
            $this->item_details = $item_details;

            if (array_key_exists('print', $allParams)) {
                $this->pgTitle = "Pending " . $allParams['pgTitle'];
                $this->to_be_printed = $allParams['to_be_printed'];

                $this->setTemplate('print');
                $this->setLayout('layout_for_print');
            } else {
                /**
                 * Varun : Case added for xls and csv download
                 */
                if ($allParams['action'] === 'export' || $allParams['action'] === 'pdf') {//Manu : In case of PDF
                    if ($allParams['action'] === 'pdf') {
                        $allParams['filename'] = $allParams['filename'] . '_' . time();
                        $allParams['fileType'] = 'pdf';
                    }
                    if ($allParams['fileType'] === 'xls') {
                        $allParams['filename'] = $allParams['filename'] . '_' . time();
                        $allParams['fileType'] = 'xls';
                    }
                    if ($allParams['fileType'] === 'csv') {
                        $allParams['filename'] = $allParams['filename'] . '_' . time();
                        $allParams['fileType'] = 'csv';
                    }
                    $this->createFile_to_be_exported($invoices, $item_details, $id_Items_invoiceId, $allParams['filename'], $allParams['fileType']);
                    /**
                     * Manu : Close functionality , Move on Templates
                     */
                    exit;
                }
                $this->setTemplate('index');
            }
        } else {
            $this->redirect('report/');
        }
    }

    public function executeExpiredInvoicesForMerchant(sfWebRequest $request) {
        $this->class_on = $this->getActionName();
        $allParams = $request->getParameterHolder()->getAll();
        //Varun : Offset value to unset session
        $offset = $request->getParameter('offset');
        if (array_key_exists('search', $allParams) && $allParams['search'] != '') {
            $searchKey = $allParams['search'];
        } else {
            $searchKey = null;
        }
        //Varun: Variables added for search
        if (!isset($_SESSION['email_address'])) {
            $_SESSION['email_address'] = '';
        }
        if (!isset($_SESSION['cust_name'])) {
            $_SESSION['cust_name'] = '';
        }
        if (!isset($_SESSION['search_par'])) {
            $_SESSION['search_par'] = '';
        }
        if (!isset($_SESSION['invoice_no'])) {
            $_SESSION['invoice_no'] = '';
        }
        if (array_key_exists('email_address', $allParams) && $allParams['email_address'] != '') {
            if ($request->getParameter('offset') > 0) {
                $email_address = cryptString::decrypt($request->getParameter('email_address'));
            } else {
                $email_address = $allParams['email_address'];
            }
        } else {
            $email_address = '';
        }
        if (array_key_exists('cust_name', $allParams) && $allParams['cust_name'] != '') {
            if ($request->getParameter('offset') > 0) {
                $cust_name = cryptString::decrypt($request->getParameter('cust_name'));
            } else {
                $cust_name = $allParams['cust_name'];
            }
        } else {
            $cust_name = '';
        }
        if (array_key_exists('search_par', $allParams) && $allParams['search_par'] != '') {
            if ($request->getParameter('offset') > 0) {
                $search_par = cryptString::decrypt($request->getParameter('search_par'));
            } else {
                $search_par = $allParams['search_par'];
            }
        } else {
            $search_par = '';
        }
        if ($request->isMethod('post')) {

            $_SESSION['email_address'] = $email_address;
            $_SESSION['cust_name'] = $cust_name;
            $_SESSION['search_par'] = $search_par;
            $_SESSION['search_text'] = $searchKey;
        } else {
//Varun :Session will be unset for the first time page opens
            if (!isset($offset)) {
                if (isset($email_address) && $email_address != '') {
                    $_SESSION['email_address'] = $email_address;
                } else {
                    $_SESSION['email_address'] = NULL;
                }
                if (isset($cust_name) && $cust_name != '') {
                    $_SESSION['cust_name'] = $cust_name;
                } else {
                    $_SESSION['cust_name'] = NULL;
                }
                if (isset($search_par) && $search_par != '') {
                    $_SESSION['search_par'] = $search_par;
                } else {
                    $_SESSION['search_par'] = NULL;
                }
                if (isset($searchKey) && $searchKey != '') {
                    $_SESSION['search_text'] = $searchKey;
                } else {
                    $_SESSION['search_text'] = NULL;
                }
            } else {
                $email_address = (isset($_SESSION['email_address'])) ? $_SESSION['email_address'] : '';
                $cust_name = (isset($_SESSION['cust_name'])) ? $_SESSION['cust_name'] : '';
                $search_par = (isset($_SESSION['search_par'])) ? $_SESSION['search_par'] : '';
                $searchKey = (isset($_SESSION['search_text'])) ? $_SESSION['search_text'] : '';
            }
        }
        $this->form = new InvoiceFormFilter();
        if (array_key_exists('invoice_filters', $allParams) && ($allParams['invoice_filters']['from']['day'] != '' || $allParams['invoice_filters']['from']['day'] != '')) {
            /**
             * Manu : Search date display in case of page refresh
             */
            if ($allParams['invoice_filters']['to']['day'] != '') {
                $to_date = date("Y-m-d", strtotime($allParams['invoice_filters']['to']['year'] . "-" . $allParams['invoice_filters']['to']['month'] . "-" . $allParams['invoice_filters']['to']['day']));
            }
            if ($allParams['invoice_filters']['from']['day'] != '') {
                $fromdate = date("Y-m-d", strtotime($allParams['invoice_filters']['from']['year'] . "-" . $allParams['invoice_filters']['from']['month'] . "-" . $allParams['invoice_filters']['from']['day']));
            }
            $dateCat = $allParams['date_category'];
        } elseif (array_key_exists('to', $allParams) && $allParams['to'] != '') {
            $this->setTemplate('print');
            $to_date = date("Y-m-d", strtotime($allParams['to']));

            $dateCat = $allParams['date_cat'];
            $fromdate = date("Y-m-d", strtotime($allParams['from']));
        }
        $this->searchKey = (isset($searchKey)) ? $searchKey : null;
        $this->email_address = (isset($email_address)) ? $email_address : '';
        $this->cust_name = (isset($cust_name)) ? $cust_name : '';
        $this->search_par = (isset($search_par)) ? $search_par : '';

        $this->to_date = (isset($to_date)) ? $to_date : null;
        $this->from_date = (isset($fromdate)) ? $fromdate : null;
        $this->date_cat = (isset($dateCat)) ? $dateCat : null;
//Varun : Check for authentication and redirect to report if session doesn't exist
        if ($this->getUser()->isAuthenticated()) {
            $invoices = Doctrine::getTable('Invoice')->getExpiredInvoicesForMerchant($this->getUser()->getGuardUser()->getMerchantUser()->getId(), $this->searchKey, $this->from_date, $this->to_date, $this->date_cat, '', '', $this->email_address, $this->cust_name);
            $total_no_of_records = count($invoices);
            /**
             * Manu : Replace of check filename place of filetype(filename is common parameter pdf,csv and xls)
             */
            if ($total_no_of_records > sfConfig::get('app_records_per_page') && !array_key_exists('filename', $allParams) && !array_key_exists('print', $allParams) && !array_key_exists('pdf', $allParams)) {




                $this->actionTobeCalled = $this->getActionName();
                $this->total_no_of_records = $total_no_of_records;
                $this->no_of_records_per_page = sfConfig::get('app_records_per_page');
                $this->setPagination = true;
                $this->total_amount_of_all_transactions = $this->getTotalofAllInvoices($invoices);

                if (!$request->hasParameter('offset')) {
                    $offset = 0;
                } else {
                    $offset = $request->getParameter('offset');
                }
                $this->current_offset = $offset;
                $invoices = Doctrine::getTable('Invoice')->getExpiredInvoicesForMerchant($this->getUser()->getGuardUser()->getMerchantUser()->getId(), $this->searchKey, $this->from_date, $this->to_date, $this->date_cat, $offset, sfConfig::get('app_records_per_page'), $this->email_address, $this->cust_name);

                $this->actionTobeCalled = $this->getActionName();
                $this->module = $this->getModuleName();
                $this->offset_last = (($total_no_of_records % sfConfig::get('app_records_per_page') == 0 ? ($total_no_of_records - sfConfig::get('app_records_per_page') ) : $total_no_of_records - ($total_no_of_records % sfConfig::get('app_records_per_page'))));

                $this->offset_next = (($offset + sfConfig::get('app_records_per_page')) > $this->offset_last ? $this->offset_last : ($offset + sfConfig::get('app_records_per_page')));

                $this->offset_prev = (($offset - sfConfig::get('app_records_per_page')) < 0 ? 0 : $offset - sfConfig::get('app_records_per_page'));
            } else {
                $this->setPagination = false;
            }
            $id_Items_invoiceId = $this->construct_invoice_itemId_array($invoices);


            $item_details = !is_null($id_Items_invoiceId) ? Doctrine::getTable('InvoiceItemDetails')->getItemDetailsForInvoices(array_unique(array_values($id_Items_invoiceId))) : null;
            $this->id_Items_invoiceId = $id_Items_invoiceId;
            $this->invoices = $invoices;
            $this->item_details = $item_details;
            if (array_key_exists('print', $allParams)) {

                $this->setTemplate('print');
                $this->pgTitle = "All " . $allParams['pgTitle'];
                $this->to_be_printed = $allParams['to_be_printed'];


                $this->setLayout('layout_for_print');
            } else {
                /**
                 * Varun : Case added for xls and csv download
                 */
                if ($allParams['action'] === 'export' || $allParams['action'] === 'pdf') {//Manu : In case of PDF
                    if ($allParams['action'] === 'pdf') {
                        $allParams['filename'] = $allParams['filename'] . '_' . time();
                        $allParams['fileType'] = 'pdf';
                    }
                    if ($allParams['fileType'] === 'xls') {
                        $allParams['filename'] = $allParams['filename'] . '_' . time();
                        $allParams['fileType'] = 'xls';
                    }
                    if ($allParams['fileType'] === 'csv') {
                        $allParams['filename'] = $allParams['filename'] . '_' . time();
                        $allParams['fileType'] = 'csv';
                    }
                    $this->createFile_to_be_exported($invoices, $item_details, $id_Items_invoiceId, $allParams['filename'], $allParams['fileType']);
                    /**
                     * Manu : Close functionality , Move on Templates
                     */
                    exit;
                }
                $this->setTemplate('index');
            }
        } else {
            $this->redirect('report/');
        }
    }

    public function executeCancelledInvoicesForMerchant(sfWebRequest $request) {
        $this->class_on = $this->getActionName();
        $allParams = $request->getParameterHolder()->getAll();
        //Varun : Offset value to unset session
        $offset = $request->getParameter('offset');
        if (array_key_exists('search', $allParams) && $allParams['search'] != '') {
            $searchKey = $allParams['search'];
        } else {
            $searchKey = null;
        }
        if (!isset($_SESSION['email_address'])) {
            $_SESSION['email_address'] = '';
        }
        if (!isset($_SESSION['cust_name'])) {
            $_SESSION['cust_name'] = '';
        }
        if (!isset($_SESSION['search_par'])) {
            $_SESSION['search_par'] = '';
        }
        if (!isset($_SESSION['invoice_no'])) {
            $_SESSION['invoice_no'] = '';
        }
        if (array_key_exists('email_address', $allParams) && $allParams['email_address'] != '') {
            if ($request->getParameter('offset') > 0) {
                $email_address = cryptString::decrypt($request->getParameter('email_address'));
            } else {
                $email_address = $allParams['email_address'];
            }
        } else {
            $email_address = '';
        }
        if (array_key_exists('cust_name', $allParams) && $allParams['cust_name'] != '') {
            if ($request->getParameter('offset') > 0) {
                $cust_name = cryptString::decrypt($request->getParameter('cust_name'));
            } else {
                $cust_name = $allParams['cust_name'];
            }
        } else {
            $cust_name = '';
        }
        if (array_key_exists('search_par', $allParams) && $allParams['search_par'] != '') {
            if ($request->getParameter('offset') > 0) {
                $search_par = cryptString::decrypt($request->getParameter('search_par'));
            } else {
                $search_par = $allParams['search_par'];
            }
        } else {
            $search_par = '';
        }
        if ($request->isMethod('post')) {

            $_SESSION['email_address'] = $email_address;
            $_SESSION['cust_name'] = $cust_name;
            $_SESSION['search_par'] = $search_par;
            $_SESSION['search_text'] = $searchKey;
        } else {
//Varun :Session will be unset for the first time page opens
            if (!isset($offset)) {
                if (isset($email_address) && $email_address != '') {
                    $_SESSION['email_address'] = $email_address;
                } else {
                    $_SESSION['email_address'] = NULL;
                }
                if (isset($cust_name) && $cust_name != '') {
                    $_SESSION['cust_name'] = $cust_name;
                } else {
                    $_SESSION['cust_name'] = NULL;
                }
                if (isset($search_par) && $search_par != '') {
                    $_SESSION['search_par'] = $search_par;
                } else {
                    $_SESSION['search_par'] = NULL;
                }
                if (isset($searchKey) && $searchKey != '') {
                    $_SESSION['search_text'] = $searchKey;
                } else {
                    $_SESSION['search_text'] = NULL;
                }
            } else {
                $email_address = (isset($_SESSION['email_address'])) ? $_SESSION['email_address'] : '';
                $cust_name = (isset($_SESSION['cust_name'])) ? $_SESSION['cust_name'] : '';
                $search_par = (isset($_SESSION['search_par'])) ? $_SESSION['search_par'] : '';
                $searchKey = (isset($_SESSION['search_text'])) ? $_SESSION['search_text'] : '';
            }
        }
        $this->form = new InvoiceFormFilter();
        if (array_key_exists('invoice_filters', $allParams) && ($allParams['invoice_filters']['from']['day'] != '' || $allParams['invoice_filters']['from']['day'] != '')) {
            /**
             * Manu : Search date display in case of page refresh
             */
            if ($allParams['invoice_filters']['to']['day'] != '') {
                $to_date = date("Y-m-d", strtotime($allParams['invoice_filters']['to']['year'] . "-" . $allParams['invoice_filters']['to']['month'] . "-" . $allParams['invoice_filters']['to']['day']));
            }
            if ($allParams['invoice_filters']['from']['day'] != '') {
                $fromdate = date("Y-m-d", strtotime($allParams['invoice_filters']['from']['year'] . "-" . $allParams['invoice_filters']['from']['month'] . "-" . $allParams['invoice_filters']['from']['day']));
            }
            $dateCat = $allParams['date_category'];
        } elseif (array_key_exists('to', $allParams) && $allParams['to'] != '') {
            $this->setTemplate('print');
            $to_date = date("Y-m-d", strtotime($allParams['to']));

            $dateCat = $allParams['date_cat'];
            $fromdate = date("Y-m-d", strtotime($allParams['from']));
        }
        $this->searchKey = (isset($searchKey)) ? $searchKey : null;
        $this->email_address = (isset($email_address)) ? $email_address : '';
        $this->cust_name = (isset($cust_name)) ? $cust_name : '';
        $this->search_par = (isset($search_par)) ? $search_par : '';

        $this->to_date = (isset($to_date)) ? $to_date : null;
        $this->from_date = (isset($fromdate)) ? $fromdate : null;
        $this->date_cat = (isset($dateCat)) ? $dateCat : null;
//Varun : Check for authentication and redirect to report if session doesn't exist
        if ($this->getUser()->isAuthenticated()) {
            $invoices = Doctrine::getTable('Invoice')->getCancelledInvoicesForMerchant($this->getUser()->getGuardUser()->getMerchantUser()->getId(), $this->searchKey, $this->from_date, $this->to_date, $this->date_cat, '', '', $this->email_address, $this->cust_name);
            $total_no_of_records = count($invoices);
            /**
             * Manu : Replace of check filename place of filetype(filename is common parameter pdf,csv and xls)
             */
            if ($total_no_of_records > sfConfig::get('app_records_per_page') && !array_key_exists('filename', $allParams) && !array_key_exists('print', $allParams) && !array_key_exists('pdf', $allParams)) {

                $this->actionTobeCalled = $this->getActionName();
                $this->total_no_of_records = $total_no_of_records;
                $this->no_of_records_per_page = sfConfig::get('app_records_per_page');
                $this->setPagination = true;
                $this->total_amount_of_all_transactions = $this->getTotalofAllInvoices($invoexportices);

                if (!$request->hasParameter('offset')) {
                    $offset = 0;
                } else {
                    $offset = $request->getParameter('offset');
                }
                $this->current_offset = $offset;
                $invoices = Doctrine::getTable('Invoice')->getCancelledInvoicesForMerchant($this->getUser()->getGuardUser()->getMerchantUser()->getId(), $this->searchKey, $this->from_date, $this->to_date, $this->date_cat, $offset, sfConfig::get('app_records_per_page'), $this->email_address, $this->cust_name);

                $this->actionTobeCalled = $this->getActionName();
                $this->module = $this->getModuleName();
                $this->offset_last = (($total_no_of_records % sfConfig::get('app_records_per_page') == 0 ? ($total_no_of_records - sfConfig::get('app_records_per_page') ) : $total_no_of_records - ($total_no_of_records % sfConfig::get('app_records_per_page'))));

                $this->offset_next = (($offset + sfConfig::get('app_records_per_page')) > $this->offset_last ? $this->offset_last : ($offset + sfConfig::get('app_records_per_page')));

                $this->offset_prev = (($offset - sfConfig::get('app_records_per_page')) < 0 ? 0 : $offset - sfConfig::get('app_records_per_page'));
            } else {
                $this->setPagination = false;
            }
            $id_Items_invoiceId = $this->construct_invoice_itemId_array($invoices);


            $item_details = !is_null($id_Items_invoiceId) ? Doctrine::getTable('InvoiceItemDetails')->getItemDetailsForInvoices(array_unique(array_values($id_Items_invoiceId))) : null;
            $this->id_Items_invoiceId = $id_Items_invoiceId;
            $this->invoices = $invoices;
            $this->item_details = $item_details;
            if (array_key_exists('print', $allParams)) {
                $this->to_be_printed = $allParams['to_be_printed'];

                $this->setTemplate('print');
                $this->pgTitle = "Cancelled " . $allParams['pgTitle'];

                $this->setLayout('layout_for_print');
            } else {
                /**
                 * Varun : Case added for xls and csv download
                 */
                if ($allParams['action'] === 'export' || $allParams['action'] === 'pdf') {//Manu : In case of PDF
                    if ($allParams['action'] === 'pdf') {
                        $allParams['filename'] = $allParams['filename'] . '_' . time();
                        $allParams['fileType'] = 'pdf';
                    }
                    if ($allParams['fileType'] === 'xls') {
                        $allParams['filename'] = $allParams['filename'] . '_' . time();
                        $allParams['fileType'] = 'xls';
                    }
                    if ($allParams['fileType'] === 'csv') {
                        $allParams['filename'] = $allParams['filename'] . '_' . time();
                        $allParams['fileType'] = 'csv';
                    }
                    $this->createFile_to_be_exported($invoices, $item_details, $id_Items_invoiceId, $allParams['filename'], $allParams['fileType']);
                    /**
                     * Manu : Close functionality , Move on Templates
                     */
                    exit;
                }
                $this->setTemplate('index');
            }
        } else {
            $this->redirect('report/');
        }
    }

    public function executeResetForSearchForm(sfWebRequest $request) {
//        $this->getUser()->getAttributeHolder()->remove('com_name_payment_history');
        $_SESSION['com_name_payment_history'] = "";
        $_SESSION['invoice_no_payment_history'] = "";
        $this->redirect('report/allPaymentsForCustomer');
    }

    public function executeAllPaymentsForCustomer(sfWebRequest $request) {
        ini_set('max_execution_time', '2000');
        ini_set('memory_limit', '256M');
        $this->class_on = $this->getActionName();
        $this->formAction = "report/allPaymentsForCustomer";

        ################ Search Criteria
        $com_name = '';
        $invoice_no = '';
        $search_par = '';

        $offset = $request->getParameter('offset');

        if (!isset($_SESSION['com_name_payment_history'])) {
            $_SESSION['com_name_payment_history'] = '';
        }
        if (!isset($_SESSION['invoice_no_payment_history'])) {
            $_SESSION['invoice_no_payment_history'] = '';
        }
        if ($request->getParameter('com_name') != '') {
            if ($request->getParameter('offset') > 0) {
                $com_name = cryptString::decrypt($request->getParameter('com_name'));
                $com_name = trim($com_name);
            } else {
                $com_name = $request->getParameter('com_name');
                $com_name = trim($com_name);
            }
        }

        if ($request->getParameter('invoice_no') != '') {
            if ($request->getParameter('offset') > 0) {
                $invoice_no = cryptString::decrypt($request->getParameter('invoice_no'));
                $invoice_no = trim($invoice_no);
            } else {
                $invoice_no = $request->getParameter('invoice_no');
                $invoice_no = trim($invoice_no);
            }
        }

        if ($request->getParameter('search_par') != '') {
            if ($request->getParameter('offset') > 0) {
                $search_par = cryptString::decrypt($request->getParameter('search_par'));
            } else {
                $search_par = $request->getParameter('search_par');
            }
        }

        if ($request->isMethod('post')) {
            $_SESSION['com_name_payment_history'] = $com_name;
            $_SESSION['invoice_no_payment_history'] = $invoice_no;
            $_SESSION['search_par_payment_history'] = $search_par;
        } else {
            if (!isset($offset)) {

                // date: 31/05/2012
                // Making comment for the below 3 Session variable so that
                // tabs on customer -> invoice i.e., 'All', 'Partly Paid'
                // work on the same search for both of these tabs. Means that
                // User do not need to search again (type again) for search in different tabs
//                $_SESSION['com_name_payment_history']   = NULL;
//                $_SESSION['invoice_no_payment_history'] = NULL;
//                $_SESSION['search_par_payment_history'] = NULL;

                $com_name = (isset($_SESSION['com_name_payment_history'])) ? $_SESSION['com_name_payment_history'] : '';
                $invoice_no = (isset($_SESSION['invoice_no_payment_history'])) ? $_SESSION['invoice_no_payment_history'] : '';
                $search_par = (isset($_SESSION['search_par_payment_history'])) ? $_SESSION['search_par_payment_history'] : '';
            } else {
                $com_name = (isset($_SESSION['com_name_payment_history'])) ? $_SESSION['com_name_payment_history'] : '';
                $invoice_no = (isset($_SESSION['invoice_no_payment_history'])) ? $_SESSION['invoice_no_payment_history'] : '';
                $search_par = (isset($_SESSION['search_par_payment_history'])) ? $_SESSION['search_par_payment_history'] : '';
            }
        }
        $this->com_name = (isset($com_name)) ? $com_name : null;
        $this->invoice_no = (isset($invoice_no)) ? $invoice_no : null;
        $this->search_par = (isset($search_par)) ? $search_par : null;

        #################### End of Search Criteria: Saurabh

        $this->customer_id = trim($request->getParameter('customer_id'));
        $allParams = $request->getParameterHolder()->getAll();

        // Customer User only
        $id = '';
        $varObjUserCustomer = $this->getUser()->getGuardUser()->getUserCustomerMapping();
        $totalRecord = count($varObjUserCustomer);
        foreach ($varObjUserCustomer as $k => $ob) {
            $id .= $ob['customer_profile_id'] . '';
            if ($k < ($totalRecord - 1)) {
                $id .=',';
            }
        }
        $allCustomerId = $id;


//        $invoices = Doctrine::getTable('Invoice')->paymentHistoryForCustomer($this->getUser()->getGuardUser()->getCustomerUser()->getId(), '', '', $com_name, $invoice_no);
        $invoices = Doctrine::getTable('Invoice')->paymentHistoryForCustomer($allCustomerId, '', '', $com_name, $invoice_no, $this->customer_id);

        $total_no_of_records = count($invoices);

        if ($total_no_of_records > sfConfig::get('app_records_per_page')) {

            $this->actionTobeCalled = $this->getActionName();
            $this->total_no_of_records = $total_no_of_records;
            $this->no_of_records_per_page = sfConfig::get('app_records_per_page');
            $this->setPagination = true;
            $this->total_amount_of_all_transactions = $this->getTotalofAllInvoices($invoices);

            if (!$request->hasParameter('offset')) {
                $offset = 0;
            } else {
                $offset = $request->getParameter('offset');
            }
            $this->current_offset = $offset;
//            $invoices = Doctrine::getTable('Invoice')->paymentHistoryForCustomer($this->getUser()->getGuardUser()->getCustomerUser()->getId(), $offset, sfConfig::get('app_records_per_page'), $com_name, $invoice_no);
            $invoices = Doctrine::getTable('Invoice')->paymentHistoryForCustomer($allCustomerId, $offset, sfConfig::get('app_records_per_page'), $com_name, $invoice_no, $this->customer_id);

            $this->actionTobeCalled = $this->getActionName();
            $this->module = $this->getModuleName();
            $this->offset_last = (($total_no_of_records % sfConfig::get('app_records_per_page') == 0 ? ($total_no_of_records - sfConfig::get('app_records_per_page') ) : $total_no_of_records - ($total_no_of_records % sfConfig::get('app_records_per_page'))));

            $this->offset_next = (($offset + sfConfig::get('app_records_per_page')) > $this->offset_last ? $this->offset_last : ($offset + sfConfig::get('app_records_per_page')));

            $this->offset_prev = (($offset - sfConfig::get('app_records_per_page')) < 0 ? 0 : $offset - sfConfig::get('app_records_per_page'));
        } else {
            $this->setPagination = false;
        }

        $id_Items_invoiceId = $this->construct_invoice_itemId_array($invoices);


        $item_details = !is_null($id_Items_invoiceId) ? Doctrine::getTable('InvoiceItemDetails')->getItemDetailsForInvoices(array_unique(array_values($id_Items_invoiceId))) : null;
        $this->id_Items_invoiceId = $id_Items_invoiceId;
        $this->invoices = $invoices;
        $this->item_details = $item_details;
        $this->setTemplate('paymentHistory');
    }

    public function executePartlyPaidForCustomer(sfWebRequest $request) {
        $this->class_on = $this->getActionName();
        $this->formAction = "report/partlyPaidForCustomer";
        $allParams = $request->getParameterHolder()->getAll();

        ################ Search Criteria: Saurabh
        $com_name = '';
        $invoice_no = '';
        $offset = $request->getParameter('offset');
        if (!isset($_SESSION['com_name_payment_history'])) {
            $_SESSION['com_name_payment_history'] = '';
        }
        if (!isset($_SESSION['invoice_no_payment_history'])) {
            $_SESSION['invoice_no_payment_history'] = '';
        }
        if ($request->getParameter('com_name') != '') {
            if ($request->getParameter('offset') > 0) {
                $com_name = cryptString::decrypt($request->getParameter('com_name'));
            } else {
                $com_name = $request->getParameter('com_name');
            }
        }

        if ($request->getParameter('invoice_no') != '') {
            if ($request->getParameter('offset') > 0) {
                $invoice_no = cryptString::decrypt($request->getParameter('invoice_no'));
            } else {
                $invoice_no = $request->getParameter('invoice_no');
            }
        }


        if ($request->getParameter('search_par') != '') {
            if ($request->getParameter('offset') > 0) {
                $search_par = cryptString::decrypt($request->getParameter('search_par'));
            } else {
                $search_par = $request->getParameter('search_par');
            }
        }

        if ($request->isMethod('post')) {
            $_SESSION['com_name_payment_history'] = $com_name;
            $_SESSION['invoice_no_payment_history'] = $invoice_no;
//            $_SESSION['search_par_payment_history'] = $search_par;
        } else {
            if (!isset($offset)) {
                // date: 31/05/2012
                // Making comment for the below 3 Session variable so that
                // tabs on customer -> invoice i.e., 'All', 'Partly Paid'
                // work on the same search for both of these tabs. Means that
                // User do not need to search again (type again) for search in different tabs
//                $_SESSION['com_name_payment_history']   = NULL;
//                $_SESSION['invoice_no_payment_history'] = NULL;
//                $_SESSION['search_par_payment_history'] = NULL;
                $com_name = (isset($_SESSION['com_name_payment_history'])) ? $_SESSION['com_name_payment_history'] : '';
                $invoice_no = (isset($_SESSION['invoice_no_payment_history'])) ? $_SESSION['invoice_no_payment_history'] : '';
                $search_par = (isset($_SESSION['search_par_payment_history'])) ? $_SESSION['search_par_payment_history'] : '';
            } else {
                $com_name = (isset($_SESSION['com_name_payment_history'])) ? $_SESSION['com_name_payment_history'] : '';
                $invoice_no = (isset($_SESSION['invoice_no_payment_history'])) ? $_SESSION['invoice_no_payment_history'] : '';
                $search_par = (isset($_SESSION['search_par_payment_history'])) ? $_SESSION['search_par_payment_history'] : '';
            }
        }
        $this->com_name = (isset($com_name)) ? $com_name : null;
        $this->invoice_no = (isset($invoice_no)) ? $invoice_no : null;
        $this->search_par = (isset($search_par)) ? $search_par : null;
        $this->customer_id = trim($request->getParameter('customer_id'));

        #################### End of Search Criteria: Saurabh
//        $customerId = $this->getUser()->getGuardUser()->getUserCustomerMapping()->getFirst()->getCustomerProfileId();
//        $invoices = Doctrine::getTable('Invoice')->partlyPaidForCustomer($this->getUser()->getGuardUser()->getCustomerUser()->getId(), '', '', $com_name, $invoice_no);
        // All customers belong to one User only
        $id = '';
        $varObjUserCustomer = $this->getUser()->getGuardUser()->getUserCustomerMapping();
        $totalRecord = count($varObjUserCustomer);
        foreach ($varObjUserCustomer as $k => $ob) {
            $id .= $ob['customer_profile_id'] . '';
            if ($k < ($totalRecord - 1)) {
                $id .=',';
            }
        }
        $allCustomerId = $id;


        $invoices = Doctrine::getTable('Invoice')->partlyPaidForCustomer($allCustomerId, '', '', $com_name, $invoice_no, $this->customer_id);
        $total_no_of_records = count($invoices);

        if ($total_no_of_records > sfConfig::get('app_records_per_page')) {

            $this->actionTobeCalled = $this->getActionName();
            $this->total_no_of_records = $total_no_of_records;
            $this->no_of_records_per_page = sfConfig::get('app_records_per_page');
            $this->setPagination = true;


            if (!$request->hasParameter('offset')) {
                $offset = 0;
            } else {
                $offset = $request->getParameter('offset');
            }
            $this->current_offset = $offset;
            $invoices = Doctrine::getTable('Invoice')->partlyPaidForCustomer($allCustomerId, $offset, sfConfig::get('app_records_per_page'), $com_name, $invoice_no, $this->customer_id);

            $this->actionTobeCalled = $this->getActionName();
            $this->module = $this->getModuleName();
            $this->offset_last = (($total_no_of_records % sfConfig::get('app_records_per_page') == 0 ? ($total_no_of_records - sfConfig::get('app_records_per_page') ) : $total_no_of_records - ($total_no_of_records % sfConfig::get('app_records_per_page'))));

            $this->offset_next = (($offset + sfConfig::get('app_records_per_page')) > $this->offset_last ? $this->offset_last : ($offset + sfConfig::get('app_records_per_page')));

            $this->offset_prev = (($offset - sfConfig::get('app_records_per_page')) < 0 ? 0 : $offset - sfConfig::get('app_records_per_page'));
        } else {
            $this->setPagination = false;
        }
        $id_Items_invoiceId = $this->construct_invoice_itemId_array($invoices);

        $item_details = !is_null($id_Items_invoiceId) ? Doctrine::getTable('InvoiceItemDetails')->getItemDetailsForInvoices(array_unique(array_values($id_Items_invoiceId))) : null;

        $this->id_Items_invoiceId = $id_Items_invoiceId;
        $this->invoices = $invoices;
        $this->item_details = $item_details;
        $this->setTemplate('paymentHistory');
    }

    private function getTotalofAllInvoices($invoices) {
        $invoice_ids = $this->getIds($invoices);
//        echo "<pre>";print_r($invoice_ids);die;
        $item_details = Doctrine::getTable('InvoiceItemDetails')->getItemDetailsForInvoices((array_values($invoice_ids)))->toArray();
        $total = 0;
        foreach ($item_details as $record => $fields) {
            foreach ($fields as $colName => $value) {
                if ($colName == 'partial_sum' or $colName == 'tax') {
                    $total += ( floatval($value));
                }
            }
        }

        return $total;
    }

    private function getIds($invoices) {
        $ids = array();

        foreach ($invoices as $invoice_record) {
            
            /*if (is_null($invoice_record->getParentIdIfRecurring())) {

                $childInvOb = Doctrine::getTable('Invoice')->findBy('parent_id_if_recurring', $invoice_record->getId())->getLast();
                if ($childInvOb) {
                    $ids[] = ($childInvOb->getId());
                } else {
                    $ids[] = ($invoice_record->getId());
                }
            } else {

                $ids[] = ($invoice_record->getId());
                $ids[] = ($invoice_record->getId());
            }*/
        }
        return $ids;
    }

    private function construct_invoice_itemId_array($invoices) {
        $id_Items_invoiceId = null;
        foreach ($invoices as $invoice_record) {

            if (is_null($invoice_record->getParentIdIfRecurring())) {

                $childInvOb = Doctrine::getTable('Invoice')->findBy('parent_id_if_recurring', $invoice_record->getId())->getLast();
                if ($childInvOb) {

                    $id_Items_invoiceId[$invoice_record->getId()] = ($childInvOb->getId());
                } else {
                    $id_Items_invoiceId[$invoice_record->getId()] = ($invoice_record->getId());
                }
            } else {

                $id_Items_invoiceId[$invoice_record->getParentIdIfRecurring()] = ($invoice_record->getId());
                $id_Items_invoiceId[$invoice_record->getId()] = ($invoice_record->getId());
            }
        }
        return $id_Items_invoiceId;
    }

    public function executePrintSelectedInvoices(sfWebRequest $request) {
        $allParam = $request->getParameterHolder()->getAll();

        $array_of_id = array();
        foreach ($allParam['invoice_ids'] as $id) {

            array_push($array_of_id, $id);
        }
        $invoices = Doctrine::getTable('Invoice')->returnInvoiceObjectsFromIds($array_of_id);
        $id_Items_invoiceId = $this->construct_invoice_itemId_array($invoices);


        $item_details = !is_null($id_Items_invoiceId) ? Doctrine::getTable('InvoiceItemDetails')->getItemDetailsForInvoices(array_unique(array_values($id_Items_invoiceId))) : null;
        $merchant_id = $this->getUser()->getGuardUser()->getId();
        $merchantProfile = Doctrine::getTable('MerchantProfile')->findByMerchantId($merchant_id);
        $this->logo = $merchantProfile[0]['company_logo'];
        $this->id_Items_invoiceId = $id_Items_invoiceId;
        $this->invoices = $invoices;
        $this->item_details = $item_details;
        $this->to_be_printed = true;
        $this->pgTitle = $allParam['pgTitle'];
        $merchnatLogin = false;
        $admin_login = false;
        foreach (sfContext::getInstance()->getUser()->getGuardUser()->getGroups()->toArray() as $group_array) {


            if ($group_array['id'] == 2) {//Customer
                $admin_login = false;
                $merchnatLogin = false;
            } else if ($group_array['id'] == 1) {//Merchant
                $admin_login = false;
                $merchnatLogin = true;
            } else if ($group_array['id'] == 3) { //SuperAdmin
                $admin_login = true;
            } else if ($group_array['id'] == 4) { //Varun : Conditiob for report admin
                $admin_login = true;
            }
        }
        $this->merchnatLogin = $merchnatLogin;
        $this->admin_login = $admin_login;
//        $this->setLayout('layout_for_print');
        $this->setTemplate('print');
    }

    public function executeExport(sfWebRequest $request) {
        //Php Ini setting Varun : Bug fix :FS#34920
        ini_set('max_execution_time', '2000');
        ini_set('memory_limit', '256M');
        if ($request->getPostParameter('fileType')) {
            $allParams = $request->getParameterHolder()->getAll();

            $this->forward('report', $allParams['filter']);
        } else {
            $parameters = $request->getParameterHolder()->getAll();
            unset($parameters['module']);
            unset($parameters['action']);

            $this->invoice_no = $request->hasParameter('invoice_no') ? $parameters['invoice_no'] : '';
            $this->file_to_be_downloaded = false;
//            $this->search = $params['search'];
//            $this->email_address = $params['email_address'];
            $this->cust_name = $request->hasParameter('cust_name') ? $parameters['cust_name'] : '';
            $this->from = $request->hasParameter('from') ? $parameters['from'] : '';
            $this->to = $request->hasParameter('to') ? $parameters['to'] : '';
            $this->filter = $request->hasParameter('filter') ? $parameters['filter'] : '';
            $this->date_cat = $request->hasParameter('date_cat') ? $parameters['date_cat'] : '';
            $this->expired = $request->hasParameter('expired') ? $parameters['expired'] : 0;
            $this->status = $request->hasParameter('status') ? $parameters['status'] : '';
            $this->customer_id = $request->hasParameter('customer_id') ? $parameters['customer_id'] : '';
        }
    }

    private function createFile_to_be_exported($invoices, $item_details, $id_Items_invoiceId, $filename, $fileType) {
        $csvHelperOb = new csvHelper();
        $csvHelperOb->exportData($invoices, $item_details, $id_Items_invoiceId, $filename, $fileType);
    }

    /**
     * @author:  Manu Datta
     * @param <type> $request
     * @purpose:    Export Invoice in PDF
     */
    public function executePdf(sfWebRequest $request) {
        //Php Ini setting Varun : Bug fix :FS#34920
        ini_set('max_execution_time', '2000');
        ini_set('memory_limit', '512M');
        if ($request->getPostParameter('filename')) {
            $allParams = $request->getParameterHolder()->getAll();
            $this->forward('report', $allParams['filter']);
        } else {
            $parameters = $request->getParameterHolder()->getAll();
            unset($parameters['module']);
            unset($parameters['action']);

            $this->invoice_no = $request->hasParameter('invoice_no') ? $parameters['invoice_no'] : '';
            $this->file_to_be_downloaded = false;
            $this->cust_name = $request->hasParameter('cust_name') ? $parameters['cust_name'] : '';
            $this->from = $request->hasParameter('from') ? $parameters['from'] : '';
            $this->to = $request->hasParameter('to') ? $parameters['to'] : '';
            $this->filter = $request->hasParameter('filter') ? $parameters['filter'] : '';
            $this->date_cat = $request->hasParameter('date_cat') ? $parameters['date_cat'] : '';
            $this->expired = $request->hasParameter('expired') ? $parameters['expired'] : 0;
            $this->status = $request->hasParameter('status') ? $parameters['status'] : '';
            $this->customer_id = $request->hasParameter('customer_id') ? $parameters['customer_id'] : '';
        }
    }

    public function executeXls(sfWebRequest $request) {
        $allParams = $request->getParameterHolder()->getAll();

        $this->forward('report', $allParams['filter']);
    }

    public function executePrint(sfWebRequest $request) {
        $allParams = $request->getParameterHolder()->getAll();
        $request->addRequestParameters(array('pgTitle' => $this->setTitle_for_page($allParams), 'to_be_printed' => false));
//        $request->addRequestParameters($parameters)

        $this->forward('report', $allParams['filter']);
    }

    private function setTitle_for_page($params) {

        $title = 'Invoices:<ul>';

        if (array_key_exists('search', $params) && $params['search'] != '') {



            $title.='<li>Invoice Number like "' . $params['search'] . '"</li>';
        }
        if (array_key_exists('from', $params) && $params['from'] != '') {

            switch ($params['date_cat']) {
                case "issue_date":
                    $date_cat = "Issue Date";
                    break;

                case "due_date":
                    $date_cat = "Due Date";
                    break;

                case "delivery_date":
                    $date_cat = "Delivery Date";
                    break;

                case "payment_date":
                    $date_cat = "Payment Date";
                    break;
            }
            $title.="<li>" . $date_cat . ' between ' . $params['from'] . ' and ' . $params['to'] . "</li>";
        }
        return $title . "</ul>";
    }

    public function executeJasperReport(sfWebRequest $request) {
        
        $report_name = $request->getParameter('ReportName');
        
        $this->setLayout(false);

        $this->callBack = ''; //array($this, "replaceVal");  // This will replace "_show_actions_" parameter with provided option.
        $this->formats = array('pdf',"xls");           // In this array you can give download options.        
        
        /* If session expires, take the user to main page from iFrame */
        if(!$this->getUser()->isAuthenticated()){
            echo "<script>top.location.reload();</script>";die;
        }

        $this->adminLogin           = pay4billHelper::checkIfAdminUser($this->getUser()->getGuardUser()->getGroups()->toArray());
        $this->subadminLogin4Report = pay4billHelper::checkIfSubAdminReportUser($this->getUser()->getGuardUser()->getGroups()->toArray());
        
        if($this->adminLogin || $this->subadminLogin4Report){
            switch ($report_name) {
                case 'Admin_BankCollection':
                    $this->reportPath = sfConfig::get('app_jasper_report_path').'/Admin_BankCollection';
                    break;
                case 'Admin_PaymentByCustomer':
                    $this->reportPath = sfConfig::get('app_jasper_report_path').'/Admin_PaymentByCustomer';
                    break;
                case 'Admin_AllMerchantPerBank':                            /* Does not working in Merchant case*/
                    $this->reportPath = sfConfig::get('app_jasper_report_path').'/Admin_AllMerchantPerBank';
                    break;
                case 'Admin_CustomerWithPendingInvoice':
                    $this->reportPath = sfConfig::get('app_jasper_report_path').'/Admin_CustomerWithPendingInvoice';
                    break;  
                case 'Admin_ViewInvoice':
                    $this->reportPath = sfConfig::get('app_jasper_report_path').'/Admin_ViewInvoice';
                    break;
                case 'Admin_ViewInvoice_PaymentByCustomer':
                    $this->reportPath = sfConfig::get('app_jasper_report_path').'/Admin_ViewInvoice_PaymentByCustomer';
                    break;
                case 'Admin_Invoice':
                    $this->reportPath = sfConfig::get('app_jasper_report_path').'/Admin_Invoice';
                    break;                
                    }
            
        }else {
              $this->isPost = $request->isMethod('post');
              switch ($report_name) {
                case 'Merchant_BankCollection':
                    $this->reportPath = sfConfig::get('app_jasper_report_path').'/Merchant_BankCollection';
                    break;
                case 'Merchant_PaymentByCustomer':
                    $this->reportPath = sfConfig::get('app_jasper_report_path').'/Merchant_PaymentByCustomer';
                    break;
                case 'Merchant_CustomerWithPendingInvoice':
                    $this->reportPath = sfConfig::get('app_jasper_report_path').'/Merchant_CustomerWithPendingInvoice';
                    break; 
                case 'Merchant_ViewInvoice':
                    $this->reportPath = sfConfig::get('app_jasper_report_path').'/Merchant_ViewInvoice';
                    break;
               case 'Merchant_ViewInvoice_PaymentByCustomer':
                    $this->reportPath = sfConfig::get('app_jasper_report_path').'/Merchant_ViewInvoice_PaymentByCustomer';
                    break;          
                case 'Merchant_Invoice':
                    $this->reportPath = sfConfig::get('app_jasper_report_path').'/Merchant_Invoice';
                    break;                 
               }
              
        } 
    }
    
    public function executeAmountPaidByCustomers(sfWebRequest $request) {
        $this->addClassTo = $this->getActionName();
        $helperObj = new pay4billHelper();
        $parameters = $helperObj->trimArray($request->getParameterHolder()->getAll());
        $this->adminLogin = pay4billHelper::checkIfAdminUser($this->getUser()->getGuardUser()->getGroups()->toArray());
        $this->subadminLogin4Report = pay4billHelper::checkIfSubAdminReportUser($this->getUser()->getGuardUser()->getGroups()->toArray());
        $this->cust_name = $customerName = array_key_exists('cust_name', $parameters) ? $parameters['cust_name'] : "";
        $this->customer_id = $customer_id = array_key_exists('customer_id', $parameters) ? $parameters['customer_id'] : "";
        $this->merchant_name = $merchant_name = array_key_exists('merchant_name', $parameters) ? $parameters['merchant_name'] : "";
        if (array_key_exists('from', $parameters)) {
            if (strlen($parameters['from']['month']) == 1) {
                $parameters['from']['month'] = "0" . $parameters['from']['month'];
            }
            if (strlen($parameters['to']['month']) == 1) {
                $parameters['to']['month'] = "0" . $parameters['to']['month'];
            }
            if (strlen($parameters['from']['day']) == 1) {
                $parameters['from']['day'] = "0" . $parameters['from']['day'];
            }
            if (strlen($parameters['to']['day']) == 1) {
                $parameters['to']['day'] = "0" . $parameters['to']['day'];
            }
        }
        $this->from_date = $from_date = (array_key_exists('from', $parameters) && $parameters['from']['year'] != '') ? $parameters['from']['year'] . "-" . $parameters['from']['month'] . "-" . $parameters['from']['day'] : "";

        $this->to_date = $to_date = (array_key_exists('to', $parameters) && $parameters['to']['year'] != '') ? $parameters['to']['year'] . "-" . $parameters['to']['month'] . "-" . $parameters['to']['day'] : "";


        $this->customers = $customers = Doctrine::getTable('CustomerProfile')->getTotalAmountPaidByCustomer($customerName, $customer_id, $from_date, $to_date, $merchant_name);


        $total_no_of_records = $customers->count();
        $this->form = $form = new ReportSearchForm();
        $form->bind($parameters);
        if ($total_no_of_records > sfConfig::get('app_records_per_page')) {
            foreach ($this->customers as $obj) {
                $this->total_amount_of_all_transactions+=$obj->getAmountPaid();
            }
            $this->actionTobeCalled = $this->getActionName();
            $this->total_no_of_records = $total_no_of_records;
            $this->no_of_records_per_page = sfConfig::get('app_records_per_page');
            $this->setPagination = true;
            if (!$request->hasParameter('offset')) {
                $offset = 0;
            } else {
                $offset = $request->getParameter('offset');
            }
            $this->current_offset = $offset;
            $this->customers = $customers = Doctrine::getTable('CustomerProfile')->getTotalAmountPaidByCustomer($customerName, $customer_id, $from_date, $to_date, $merchant_name, $offset, $this->no_of_records_per_page);

            $this->actionTobeCalled = $this->getActionName();
            $this->module = $this->getModuleName();
            $this->offset_last = (($total_no_of_records % sfConfig::get('app_records_per_page') == 0 ? ($total_no_of_records - sfConfig::get('app_records_per_page') ) : $total_no_of_records - ($total_no_of_records % sfConfig::get('app_records_per_page'))));

            $this->offset_next = (($offset + sfConfig::get('app_records_per_page')) > $this->offset_last ? $this->offset_last : ($offset + sfConfig::get('app_records_per_page')));

            $this->offset_prev = (($offset - sfConfig::get('app_records_per_page')) < 0 ? 0 : $offset - sfConfig::get('app_records_per_page'));
        } else {
            $this->setPagination = false;
        }
    }

    public function executeCustomersWithPendingInvoices(sfWebRequest $request) {
        $this->addClassTo = $this->getActionName();

        $helperObj = new pay4billHelper();
        $parameters = $helperObj->trimArray($request->getParameterHolder()->getAll());
        $this->adminLogin = pay4billHelper::checkIfAdminUser($this->getUser()->getGuardUser()->getGroups()->toArray());
        $this->subadminLogin4Report = pay4billHelper::checkIfSubAdminReportUser($this->getUser()->getGuardUser()->getGroups()->toArray());
        $this->cust_name = $customerName = array_key_exists('cust_name', $parameters) ? $parameters['cust_name'] : "";
        $this->customer_id = $customer_id = array_key_exists('customer_id', $parameters) ? $parameters['customer_id'] : "";
        $this->merchant_name = $merchant_name = array_key_exists('merchant_name', $parameters) ? $parameters['merchant_name'] : "";
        if (array_key_exists('from', $parameters)) {
            if (strlen($parameters['from']['month']) == 1) {
                $parameters['from']['month'] = "0" . $parameters['from']['month'];
            }
            if (strlen($parameters['to']['month']) == 1) {
                $parameters['to']['month'] = "0" . $parameters['to']['month'];
            }
            if (strlen($parameters['from']['day']) == 1) {
                $parameters['from']['day'] = "0" . $parameters['from']['day'];
            }
            if (strlen($parameters['to']['day']) == 1) {
                $parameters['to']['day'] = "0" . $parameters['to']['day'];
            }
        }
        $this->from_date = $from_date = (array_key_exists('from', $parameters) && $parameters['from']['year'] != '') ? $parameters['from']['year'] . "-" . $parameters['from']['month'] . "-" . $parameters['from']['day'] : "";

        $this->to_date = $to_date = (array_key_exists('to', $parameters) && $parameters['to']['year'] != '') ? $parameters['to']['year'] . "-" . $parameters['to']['month'] . "-" . $parameters['to']['day'] : "";


        $this->customers = $customers = Doctrine::getTable('CustomerProfile')->getCustomersWithPendingInvoices($customerName, $customer_id, $from_date, $to_date, $merchant_name);


        $total_no_of_records = count($customers);
        $this->form = $form = new ReportSearchForm();
        $form->bind($parameters);
        if ($total_no_of_records > sfConfig::get('app_records_per_page')) {
            foreach ($this->customers as $obj) {
                $this->total_amount_of_all_transactions+=$obj['total_amount_due'];
            }
            $this->actionTobeCalled = $this->getActionName();
            $this->total_no_of_records = $total_no_of_records;
            $this->no_of_records_per_page = sfConfig::get('app_records_per_page');
            $this->setPagination = true;
            if (!$request->hasParameter('offset')) {
                $offset = 0;
            } else {
                $offset = $request->getParameter('offset');
            }
            $this->current_offset = $offset;
            $this->customers = $customers = Doctrine::getTable('CustomerProfile')->getCustomersWithPendingInvoices($customerName, $customer_id, $from_date, $to_date, $merchant_name, $offset, $this->no_of_records_per_page);

            $this->actionTobeCalled = $this->getActionName();
            $this->module = $this->getModuleName();
            $this->offset_last = (($total_no_of_records % sfConfig::get('app_records_per_page') == 0 ? ($total_no_of_records - sfConfig::get('app_records_per_page') ) : $total_no_of_records - ($total_no_of_records % sfConfig::get('app_records_per_page'))));

            $this->offset_next = (($offset + sfConfig::get('app_records_per_page')) > $this->offset_last ? $this->offset_last : ($offset + sfConfig::get('app_records_per_page')));

            $this->offset_prev = (($offset - sfConfig::get('app_records_per_page')) < 0 ? 0 : $offset - sfConfig::get('app_records_per_page'));
        } else {
            $this->setPagination = false;
        }
    }

    public function executeBankCollection(sfWebRequest $request) {
        $this->addClassTo = $this->getActionName();
        $helperObj = new pay4billHelper();
        $parameters = $helperObj->trimArray($request->getParameterHolder()->getAll());
        $this->adminLogin = (pay4billHelper::checkIfAdminUser($this->getUser()->getGuardUser()->getGroups()->toArray()) || pay4billHelper::checkIfSubAdminReportUser($this->getUser()->getGuardUser()->getGroups()->toArray()));
        $this->subadminLogin4Report = pay4billHelper::checkIfSubAdminReportUser($this->getUser()->getGuardUser()->getGroups()->toArray());
        $this->bank_name = $bank_name = array_key_exists('bank_name', $parameters) ? $parameters['bank_name'] : "";

        if (array_key_exists('from', $parameters)) {
            if (strlen($parameters['from']['month']) == 1) {
                $parameters['from']['month'] = "0" . $parameters['from']['month'];
            }
            if (strlen($parameters['to']['month']) == 1) {
                $parameters['to']['month'] = "0" . $parameters['to']['month'];
            }
            if (strlen($parameters['from']['day']) == 1) {
                $parameters['from']['day'] = "0" . $parameters['from']['day'];
            }
            if (strlen($parameters['to']['day']) == 1) {
                $parameters['to']['day'] = "0" . $parameters['to']['day'];
            }
        }

        $this->from_date = $from_date = (array_key_exists('from', $parameters) && $parameters['from']['year'] != '') ? $parameters['from']['year'] . "-" . $parameters['from']['month'] . "-" . $parameters['from']['day'] : "";
        $this->to_date = $to_date = (array_key_exists('to', $parameters) && $parameters['to']['year'] != '') ? $parameters['to']['year'] . "-" . $parameters['to']['month'] . "-" . $parameters['to']['day'] : "";
        $this->total = Doctrine::getTable('BillPaymentRequest')->totalAmountCollectedPerBank($from_date, $to_date, $this->bank_name)->fetchAll();

        $total_no_of_records = count($this->total);
        $this->form = $form = new ReportSearchForm(array(), array('report' => 'bankcollection'));
        $form->bind($parameters);
        if ($total_no_of_records > sfConfig::get('app_records_per_page')) {
            foreach ($this->total as $recordArray) {
                $this->total_amount_of_all_transactions+=$recordArray['totalamount'];
            }

            $this->actionTobeCalled = $this->getActionName();
            $this->total_no_of_records = $total_no_of_records;
            $this->no_of_records_per_page = sfConfig::get('app_records_per_page');
            $this->setPagination = true;
            if (!$request->hasParameter('offset')) {
                $offset = 0;
            } else {
                $offset = $request->getParameter('offset');
            }
            $this->current_offset = $offset;
            $this->total = Doctrine::getTable('BillPaymentRequest')->totalAmountCollectedPerBank($from_date, $to_date, $this->bank_name, $offset, $this->no_of_records_per_page, $this->current_offset, sfConfig::get('app_records_per_page'))->fetchAll();

            $this->actionTobeCalled = $this->getActionName();
            $this->module = $this->getModuleName();
            $this->offset_last = (($total_no_of_records % sfConfig::get('app_records_per_page') == 0 ? ($total_no_of_records - sfConfig::get('app_records_per_page') ) : $total_no_of_records - ($total_no_of_records % sfConfig::get('app_records_per_page'))));

            $this->offset_next = (($offset + sfConfig::get('app_records_per_page')) > $this->offset_last ? $this->offset_last : ($offset + sfConfig::get('app_records_per_page')));

            $this->offset_prev = (($offset - sfConfig::get('app_records_per_page')) < 0 ? 0 : $offset - sfConfig::get('app_records_per_page'));
        } else {
            $this->setPagination = false;
        }
    }

    public function executeAllMerchantPerBank(sfWebRequest $request) {

        $this->addClassTo = $this->getActionName();
        $helperObj = new pay4billHelper();
        $parameters = $helperObj->trimArray($request->getParameterHolder()->getAll());
        $this->adminLogin = (pay4billHelper::checkIfAdminUser($this->getUser()->getGuardUser()->getGroups()->toArray()) || pay4billHelper::checkIfSubAdminReportUser($this->getUser()->getGuardUser()->getGroups()->toArray()));
        $this->subadminLogin4Report = pay4billHelper::checkIfSubAdminReportUser($this->getUser()->getGuardUser()->getGroups()->toArray());
        $this->bank = $bank = array_key_exists('bank', $parameters) ? $parameters['bank'] : "";
        $this->merchant_name = $bank = array_key_exists('merchant_name', $parameters) ? $parameters['merchant_name'] : "";


        $this->from_date = $from_date = (array_key_exists('from', $parameters)) ? $parameters['from'] : "";
        $this->to_date = $to_date = array_key_exists('to', $parameters) ? $parameters['to'] : "";

        $this->total = Doctrine::getTable('BillPaymentRequest')->allMerchantPerBank($from_date, $to_date, $this->bank, $this->merchant_name)->fetchAll();
        ;

        $total_no_of_records = count($this->total);
        $this->form = $form = new ReportSearchForm(array(), array('report' => 'bankcollection'));
        $form->bind($parameters);
        if ($total_no_of_records > sfConfig::get('app_records_per_page')) {
            foreach ($this->total as $obj) {
                $this->total_amount_of_all_transactions+=$obj['totalamount'];
            }

            $this->actionTobeCalled = $this->getActionName();
            $this->total_no_of_records = $total_no_of_records;
            $this->no_of_records_per_page = sfConfig::get('app_records_per_page');
            $this->setPagination = true;
            if (!$request->hasParameter('offset')) {
                $offset = 0;
            } else {
                $offset = $request->getParameter('offset');
            }
            $this->current_offset = $offset;
            $this->total = Doctrine::getTable('BillPaymentRequest')->allMerchantPerBank($from_date, $to_date, $this->bank, $this->merchant_name, $offset, $this->no_of_records_per_page)->fetchAll();
            ;

            $this->actionTobeCalled = $this->getActionName();
            $this->module = $this->getModuleName();
            $this->offset_last = (($total_no_of_records % sfConfig::get('app_records_per_page') == 0 ? ($total_no_of_records - sfConfig::get('app_records_per_page') ) : $total_no_of_records - ($total_no_of_records % sfConfig::get('app_records_per_page'))));

            $this->offset_next = (($offset + sfConfig::get('app_records_per_page')) > $this->offset_last ? $this->offset_last : ($offset + sfConfig::get('app_records_per_page')));

            $this->offset_prev = (($offset - sfConfig::get('app_records_per_page')) < 0 ? 0 : $offset - sfConfig::get('app_records_per_page'));
        } else {
            $this->setPagination = false;
        }
    }

    public function executeFullyPaidInvoicesForCustomer(sfWebRequest $request) {
        $this->class_on = $this->getActionName();
        $this->formAction = "report/fullyPaidInvoicesForCustomer";
        $helperObj = new pay4billHelper();
        $allParams = $helperObj->trimArray($request->getParameterHolder()->getAll());
//echo "<pre>";
//print_r($allParams);
//exit;
        ################ Search Criteria: Saurabh

        $this->com_name = $allParams['com_name'];
        $this->invoice_no = $allParams['invoice_no'];

        $this->customer_id = $allParams['customer_id'];

        $id = '';
        $varObjUserCustomer = $this->getUser()->getGuardUser()->getUserCustomerMapping();
        $totalRecord = count($varObjUserCustomer);
        foreach ($varObjUserCustomer as $k => $ob) {
            $id .= $ob['customer_profile_id'] . '';
            if ($k < ($totalRecord - 1)) {
                $id .=',';
            }
        }
        $allCustomerId = $id;


        $invoices = Doctrine::getTable('Invoice')->fullyPaidForCustomer($allCustomerId, '', '', $this->com_name, $this->invoice_no, $this->customer_id);
        $total_no_of_records = count($invoices);

        if ($total_no_of_records > sfConfig::get('app_records_per_page')) {

            $this->actionTobeCalled = $this->getActionName();
            $this->total_no_of_records = $total_no_of_records;
            $this->no_of_records_per_page = sfConfig::get('app_records_per_page');
            $this->setPagination = true;


            if (!$request->hasParameter('offset')) {
                $offset = 0;
            } else {
                $offset = $request->getParameter('offset');
            }
            $this->current_offset = $offset;

            $invoices = Doctrine::getTable('Invoice')->fullyPaidForCustomer($allCustomerId, $offset, sfConfig::get('app_records_per_page'), $this->com_name, $this->invoice_no, $this->customer_id);

            $this->actionTobeCalled = $this->getActionName();
            $this->module = $this->getModuleName();
            $this->offset_last = (($total_no_of_records % sfConfig::get('app_records_per_page') == 0 ? ($total_no_of_records - sfConfig::get('app_records_per_page') ) : $total_no_of_records - ($total_no_of_records % sfConfig::get('app_records_per_page'))));

            $this->offset_next = (($offset + sfConfig::get('app_records_per_page')) > $this->offset_last ? $this->offset_last : ($offset + sfConfig::get('app_records_per_page')));

            $this->offset_prev = (($offset - sfConfig::get('app_records_per_page')) < 0 ? 0 : $offset - sfConfig::get('app_records_per_page'));
        } else {
            $this->setPagination = false;
        }
        $id_Items_invoiceId = $this->construct_invoice_itemId_array($invoices);

        $item_details = !is_null($id_Items_invoiceId) ? Doctrine::getTable('InvoiceItemDetails')->getItemDetailsForInvoices(array_unique(array_values($id_Items_invoiceId))) : null;

        $this->id_Items_invoiceId = $id_Items_invoiceId;
        $this->invoices = $invoices;
        $this->item_details = $item_details;
        $this->setTemplate('paymentHistory');
    }

}
