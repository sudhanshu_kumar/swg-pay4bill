<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of bankFilter
 *
 * @author santoshk
 */
class bankFilter {
    /*
     *  by Santosh Kumar
     *  on 13 th Apr 2012
     * For forcing the merchant to enter the bank details on initial login
     */

    public function execute($filterChain) {
//        // execute this filter only once
//        echo strlen('MWMyWjFkMWcxcjJKMkg%3D').$_SERVER['REQUEST_URI'].':::'.cryptString::decrypt(substr($_SERVER['REQUEST_URI'], -22));die;
        $isCustomer=0;
        if(cryptString::decrypt(substr($_SERVER['REQUEST_URI'], -22))=='escbankl')
        {
            $isCustomer =1;
        }
//        echo "<pre>";print_r($_SESSION);die();
        if ((sfContext::getInstance()->getUser()->isAuthenticated()) && (sfContext::getInstance()->getUser()->getAttribute('user_group') == 1 && $_SESSION['logedin_user_group']!='customer' && $isCustomer!=1)) {
            $varLogOut = substr(sfContext::getInstance()->getRequest()->getURI(), -6);
            $varSlash = substr(sfContext::getInstance()->getRequest()->getURI(), -1);
            if ($varLogOut != 'logout' && $varSlash != '/') {
                $this->bank = Doctrine::getTable('Bank')->findMerchantId(sfContext::getInstance()->getUser()->getGuardUser()->getId(), 0, 1)->toArray();
                if (empty($this->bank)) {
                    $newBank = substr(sfContext::getInstance()->getRequest()->getURI(), -7);
                    $saveBank = substr(sfContext::getInstance()->getRequest()->getURI(), -8);
                    if ($saveBank != 'saveBank' && $newBank != 'newBank') {
                        sfContext::getInstance()->getController()->redirect('user_admin');
                    }
                }
            }/*  added below condition for bug fix of switch to merchant view bank mandate */
        } elseif ((sfContext::getInstance()->getUser()->isAuthenticated()) && (sfContext::getInstance()->getUser()->getAttribute('user_group') == 2 && $_SESSION['logedin_user_group'] == 'merchant')) {
                if (cryptString::decrypt(substr($_SERVER['REQUEST_URI'], -22)) != 'escbankl') {
                $varLogOut = substr(sfContext::getInstance()->getRequest()->getURI(), -6);
                $varSlash = substr(sfContext::getInstance()->getRequest()->getURI(), -1);
                if ($varLogOut != 'logout' && $varSlash != '/') {
                    $this->bank = Doctrine::getTable('Bank')->findMerchantId(sfContext::getInstance()->getUser()->getGuardUser()->getId(), 0, 1)->toArray();
                    if (empty($this->bank)) {
                        $newBank = substr(sfContext::getInstance()->getRequest()->getURI(), -7);
                        $saveBank = substr(sfContext::getInstance()->getRequest()->getURI(), -8);
                        if ($saveBank != 'saveBank' && $newBank != 'newBank') {
                            sfContext::getInstance()->getController()->redirect('user_admin');
                        }
                    }
                }
            }
        }
//        // execute next filter
        $filterChain->execute();
    }

}

?>
