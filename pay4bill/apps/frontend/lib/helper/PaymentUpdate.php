<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PaymentUpdate
 *
 * @author amalhotra
 */
class PaymentUpdate {

    public $itemNo;

    public function setItemNo($val) {
        $this->itemNo = $val;
    }

    public static function update($itemNo) {

//        echo "<pre>";
//        print_r($this->itemNo);
//        exit;
//        $itemNo=19;
        $ob = Doctrine::getTable('BillPaymentRequest')->find($itemNo);
//          $r=new Invoice();
//          $r->RecurrentBilling->
        $merchant_id ='';
        if($merchant_id!='' && isset($merchant_id)){
        $merchant_id = $this->getUser()->getGuardUser()->MerchantUser->getId();
        }
        $invoice_ob_array = Doctrine::getTable('Invoice')->getInvoiceDetailsForPaymentStatusUpdates($ob->getInvoiceId(),$merchant_id);
//          echo "<pre>";
//          print_r($invoice_ob_array);
//          exit;
        $invoice_ob_to_be_updated = Doctrine::getTable('Invoice')->find($ob->getInvoiceId());
        if ($invoice_ob_array[0]['part_payment'] == 0 && count($invoice_ob_array[0]['RecurrentBilling']) == 0) {
            $invoice_ob_to_be_updated->setStatus(2);
            $invoice_ob_to_be_updated->setAmountPaid($invoice_ob_to_be_updated->getAmountDue());
            $invoice_ob_to_be_updated->setAmountDue(0);
//                      echo "<pre>";
            print_r('Full Payement');
//                      exit;
        } else if ($invoice_ob_array[0]['part_payment'] == 0 && count($invoice_ob_array[0]['RecurrentBilling']) != 0) {
            $invoice_ob_to_be_updated->setStatus(2);
            $invoice_ob_to_be_updated->setAmountPaid($invoice_ob_to_be_updated->getAmountDue());
            $invoice_ob_to_be_updated->setAmountDue(0);
//                      $recussringObject=Doctrine::getTable('RecurrentBilling')->findBy('invoice_id',$invoice_ob_to_be_updated->getId())->getFirst();
            $new_recurring_invoice = new Invoice();
            if ($invoice_ob_to_be_updated->getParentIdIfRecurring() != null) {
                $new_recurring_invoice->setParentIdIfRecurring($invoice_ob_to_be_updated->getParentIdIfRecurring());
            } else {
                $new_recurring_invoice->setParentIdIfRecurring($invoice_ob_to_be_updated->getId());
            }
            $new_recurring_invoice->setMerchantId($invoice_ob_to_be_updated->getMerchantId());
            $new_recurring_invoice->setCustomerId($invoice_ob_to_be_updated->getCustomerId());

            $recurring_obj = Doctrine::getTable('RecurrentBilling')->findBy('invoice_id', $invoice_ob_to_be_updated->id)->getFirst();

            switch ($recurring_obj->getType()) {

                case 'weekly':
                    $nextInvoice = date("Y-m-d", strtotime($invoice_ob_to_be_updated->getIssueDate() . "+1 week"));
                    break;
                case 'monthly':
                    $nextInvoice = date("Y-m-d", strtotime($invoice_ob_to_be_updated->getIssueDate() . "+1 month"));
                    break;
                case 'yearly':
                    $nextInvoice = date("Y-m-d", strtotime($invoice_ob_to_be_updated->getIssueDate() . "+1 year"));
                    break;
                case 'quarterly':
                    $nextInvoice = date("Y-m-d", strtotime($invoice_ob_to_be_updated->getIssueDate() . "+3 months"));
                    break;
                case 'custom':
                    $nextInvoice = date("Y-m-d", strtotime($invoice_ob_to_be_updated->getIssueDate() . "+" . $recurring_obj->getValue() . " days"));
                    break;
            }
//                    echo "<pre>";
//                    print_r($nextInvoice);
//                    exit;
            $new_recurring_invoice->setInvoiceNo(Doctrine::getTable('Invoice')->returnDistinctInvoiceNumer());
            $new_recurring_invoice->setDueDate($nextInvoice);
            $new_recurring_invoice->setIssueDate(date("Y-m-d"));
            $new_recurring_invoice->setDeliveryDate(date("Y-m-d"));
            $new_recurring_invoice->setCurrencyType($invoice_ob_to_be_updated->getCurrencyType());
            $new_recurring_invoice->setAdditionalInformation($invoice_ob_to_be_updated->getAdditionalInformation());
            $new_recurring_invoice->setAmountPaid(0);
            $new_recurring_invoice->setAmountDue($invoice_ob_to_be_updated->getAmountPaid());
            $new_recurring_invoice->setStatus(0);
            $new_recurring_invoice->save();
            Doctrine::getTable('InvoiceItemDetails')->updateOnRecurringBillGeneration($invoice_ob_to_be_updated->id, $new_recurring_invoice->id);
            Doctrine::getTable('RecurrentBilling')->updateOnRecurringBillGeneration($invoice_ob_to_be_updated->id, $new_recurring_invoice->id);

//                      $a=new Recurr
//                      echo "<pre>";,$inv
//                      print_r('Only Recurring');
//                      exit;
        } else if ($invoice_ob_array[0]['part_payment'] != 0 && count($invoice_ob_array[0]['RecurrentBilling']) != 0) {


            if ($invoice_ob_to_be_updated->getAmountPaid() == 0) {//First Tranaction
//                          echo "First Transaction";
                $invoice_ob_to_be_updated->setStatus(3);
                $invoice_ob_to_be_updated->setAmountPaid($invoice_ob_to_be_updated->getAmountDue());
                $invoice_ob_to_be_updated->setAmountDue((100 - $invoice_ob_array[0]['part_payment']) * ($invoice_ob_array[0]['subtotal'] + $invoice_ob_array[0]['tax']) / 100);
                $new_ob = new BillPaymentRequest();
                $new_ob->setInvoiceId($invoice_ob_to_be_updated->getId());
                $new_ob->setMerchantId($invoice_ob_to_be_updated->getMerchantId());
                $new_ob->setCustomerId($invoice_ob_to_be_updated->getCustomerId());
                $new_ob->save();
            } else { //Second Transaction
//                          echo "second Transaction";
                $invoice_ob_to_be_updated->setAmountPaid($invoice_ob_array[0]['subtotal'] + $invoice_ob_array[0]['tax']);
                $invoice_ob_to_be_updated->setAmountDue(0);
                $invoice_ob_to_be_updated->setStatus(2);
                $new_recurring_invoice = new Invoice();
                $new_recurring_invoice->setParentIdIfRecurring($invoice_ob_to_be_updated->getId());
                $new_recurring_invoice->setInvoiceNo(Doctrine::getTable('Invoice')->returnDistinctInvoiceNumer());

                $new_recurring_invoice->setMerchantId($invoice_ob_to_be_updated->getMerchantId());
                $new_recurring_invoice->setCustomerId($invoice_ob_to_be_updated->getCustomerId());
                $new_recurring_invoice->setIssueDate(date("Y-M-d"));
                $recurring_obj = Doctrine::getTable('RecurrentBilling')->findBy('invoice_id', $invoice_ob_to_be_updated->id)->getFirst();

                switch ($recurring_obj->getType()) {

                    case 'weekly':
                        $nextInvoice = date("Y-m-d", strtotime($invoice_ob_to_be_updated->getIssueDate() . "+1 week"));
                        break;
                    case 'monthly':
                        $nextInvoice = date("Y-m-d", strtotime($invoice_ob_to_be_updated->getIssueDate() . "+1 month"));
                        break;
                    case 'yearly':
                        $nextInvoice = date("Y-m-d", strtotime($invoice_ob_to_be_updated->getIssueDate() . "+1 year"));
                        break;
                    case 'quarterly':
                        $nextInvoice = date("Y-m-d", strtotime($invoice_ob_to_be_updated->getIssueDate() . "+3 months"));
                        break;
                    case 'custom':
                        $nextInvoice = date("Y-m-d", strtotime($invoice_ob_to_be_updated->getIssueDate() . "+" . $recurring_obj->getValue() . " days"));
                        break;
                }
                $new_recurring_invoice->setDueDate($nextInvoice);
                $new_recurring_invoice->setDeliveryDate(date("Y-m-d"));
                $new_recurring_invoice->setCurrencyType($invoice_ob_to_be_updated->getCurrencyType());
                $new_recurring_invoice->setAdditionalInformation($invoice_ob_to_be_updated->getAdditionalInformation());
                $new_recurring_invoice->setAmountPaid(0);
                $new_recurring_invoice->setAmountDue($invoice_ob_to_be_updated->getAmountPaid());
                $new_recurring_invoice->setStatus(0);
                $new_recurring_invoice->save();
                Doctrine::getTable('InvoiceItemDetails')->updateOnRecurringBillGeneration($invoice_ob_to_be_updated->id, $new_recurring_invoice->id);
//
                Doctrine::getTable('RecurrentBilling')->updateOnRecurringBillGeneration($invoice_ob_to_be_updated->id, $new_recurring_invoice->id);
            }
//                   print_r('Both recurring and partial Payments');
//                      exit;
        } else if ($invoice_ob_array[0]['part_payment'] != 0 && count($invoice_ob_array[0]['RecurrentBilling']) == 0) {
            if ($invoice_ob_to_be_updated->getAmountPaid() == 0) {//First Tranaction
//                           echo "First Transaction";
                $invoice_ob_to_be_updated->setStatus(3);
                $invoice_ob_to_be_updated->setAmountPaid($invoice_ob_to_be_updated->getAmountDue());
                $invoice_ob_to_be_updated->setAmountDue((100 - $invoice_ob_array[0]['part_payment']) * ($invoice_ob_array[0]['subtotal'] + $invoice_ob_array[0]['tax']) / 100);
                $new_ob = new BillPaymentRequest();
                $new_ob->setInvoiceId($invoice_ob_to_be_updated->getId());
                $new_ob->setMerchantId($invoice_ob_to_be_updated->getMerchantId());
                $new_ob->setCustomerId($invoice_ob_to_be_updated->getCustomerId());
                $new_ob->save();
            } else//Second Transaction {
//                          echo "second Transaction";
                $invoice_ob_to_be_updated->setAmountPaid($invoice_ob_array[0]['subtotal'] + $invoice_ob_array[0]['tax']);
                $invoice_ob_to_be_updated->setAmountDue(0);
                $invoice_ob_to_be_updated->setStatus(2);
            }

//                      print_r('Only partial Payments');
//                      exit;
//        }
        $invoice_ob_to_be_updated->save();
//                  echo "<pre>";
//                  print_r('die');
//                  exit;
    }

    //put your code here
}

?>
