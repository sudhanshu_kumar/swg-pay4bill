<?php

/**
 * Project Ordered List of menu.
 *
 * @package    Menu Render Formating
 * @path       /lib/Widget/MenuHelpers.php
 */
function renderMenuYAML($elm) {
//  $permissions = $_SESSION['symfony/user/sfUser/credentials']; //sfContext::getInstance()->getUser()->getAllPermissionNames();
    $permissions = sfContext::getInstance()->getUser()->getAllPermissionNames();
    $groups = sfContext::getInstance()->getUser()->getGroupNames();
    if (in_array('administrator', $groups)) {
        $permissions = sfContext::getInstance()->getUser()->getAllPermissionNames();
    } else if (isset($_SESSION['logedin_user_group']) && $_SESSION['logedin_user_group'] == 'merchant') {
        $permissions = Doctrine::getTable('sfGuardUser')->getPermissions(1);
    } else if (isset($_SESSION['logedin_user_group']) && $_SESSION['logedin_user_group'] == 'customer') {
        $permissions = Doctrine::getTable('sfGuardUser')->getPermissions(2);
    } else if (isset($_SESSION['logedin_user_group']) && $_SESSION['logedin_user_group'] == 'subAdmin1') {// added for sub admin on 2nd May 12 by Santosh Kumar
        $permissions = Doctrine::getTable('sfGuardUser')->getPermissions(4);
    } else if (isset($_SESSION['logedin_user_group']) && $_SESSION['logedin_user_group'] == 'subAdmin2') {// added for sub admin on 2nd May 12 by Santosh Kumar
        $permissions = Doctrine::getTable('sfGuardUser')->getPermissions(5);
    } else if (isset($_SESSION['logedin_user_group']) && $_SESSION['logedin_user_group'] == 'subAdmin') {// added for sub admin on 2nd May 12 by Santosh Kumar
        $permissions = Doctrine::getTable('sfGuardUser')->getSubAdminPermissions(array(4, 5));
    }

    if (count($elm)) {
        foreach ($elm as $node) {

            echo "<li";
            $perm = @$node['perm'];
            if (!in_array($perm, $permissions) && !empty($perm))
                continue;
            $url = @$node['url'];
            $requestParam = sfContext::getInstance()->getRequest();

            $requestUrl = $requestParam->getParameter('module') . "/" . $requestParam->getParameter('action');
            echo " id='" . str_replace(' ', '', $node['label']) . "'";
            if ($requestUrl == $url)
                echo " class='active'>";
            else
                echo ">";
            if (isset($node['child']) && count($node['child'])) {
                echo (!empty($url) && $url != '#') ? link_to($node['label'], $url) : "<a href='#'>" . $node['label'] . "</a>";
                //print_r(count($node['child']));
                renderChildMenuYAML($node['child']);
            } else {
                echo (!empty($url)) ? link_to($node['label'], $url) : $node['label'] . "";
            }
            echo "</li>";
        }
    }
}

#end of function

function renderChildMenuYAML($elem) {

    echo "<ul>";
    renderMenuYAML($elem);
    echo "</ul>";
}

function renderMenuYAMLPerms($elem) {

//      die('manu');
//    $userPerms = $_SESSION['symfony/user/sfUser/credentials'] ;// sfContext::getInstance()->getUser()->getAllPermissionNames();
//    $type = sfContext::getInstance()->getUser()->getAttribute('user_type');
    $groups = sfContext::getInstance()->getUser()->getGroupNames();
    $userPerms = array();
    if (in_array('administrator', $groups)) {
        unset($_SESSION['logedin_user_group']);
        $userPerms = sfContext::getInstance()->getUser()->getAllPermissionNames();
    } else if ((isset($_SESSION['logedin_user_group']) && $_SESSION['logedin_user_group'] == 'merchant')) {
        if (!in_array('merchant', $groups)) {
            if (isset($_SESSION['is_logout']) && ($_SESSION['is_logout'] == 1)) {
                $userPerms = Doctrine::getTable('sfGuardUser')->getPermissions(1);
            }
        } else {
            $userPerms = Doctrine::getTable('sfGuardUser')->getPermissions(1);
            $_SESSION['logedin_user_group'] = 'merchant';
//                sfContext::getInstance()->getController()->redirect('home/index');
        }
    } else if (isset($_SESSION['logedin_user_group']) && $_SESSION['logedin_user_group'] == 'customer') {
        if (!in_array('customer', $groups)) {
            if (isset($_SESSION['is_logout']) && ($_SESSION['is_logout'] == 1)) {
                $userPerms = Doctrine::getTable('sfGuardUser')->getPermissions(2);
            }
        } else {
            $userPerms = Doctrine::getTable('sfGuardUser')->getPermissions(2);
            $_SESSION['logedin_user_group'] = 'customer';
        }
    } else if (isset($_SESSION['logedin_user_group']) && $_SESSION['logedin_user_group'] == 'subAdmin') {// added for sub admin on 2nd May 12 by Santosh Kumar
        if (!in_array('report', $groups)) {
            if (isset($_SESSION['is_logout']) && ($_SESSION['is_logout'] == 1)) {
                $userPerms = Doctrine::getTable('sfGuardUser')->getSubAdminPermissions(array(4, 5));
            }
        } else {
            $userPerms = Doctrine::getTable('sfGuardUser')->getSubAdminPermissions(array(4, 5));
            $_SESSION['logedin_user_group'] = 'subAdmin';
        }
    } else if (isset($_SESSION['logedin_user_group']) && $_SESSION['logedin_user_group'] == 'subAdmin1') {// added for sub admin on 2nd May 12 by Santosh Kumar
        if (!in_array('report', $groups)) {
            if (isset($_SESSION['is_logout']) && ($_SESSION['is_logout'] == 1)) {
                $userPerms = Doctrine::getTable('sfGuardUser')->getPermissions(4);
            }
        } else {
            $userPerms = Doctrine::getTable('sfGuardUser')->getPermissions(4);
            $_SESSION['logedin_user_group'] = 'subAdmin1';
        }
    } else if (isset($_SESSION['logedin_user_group']) && $_SESSION['logedin_user_group'] == 'subAdmin2') {// added for sub admin on 2nd May 12 by Santosh Kumar
        if (!in_array('merchant_report', $groups)) {
            if (isset($_SESSION['is_logout']) && ($_SESSION['is_logout'] == 1)) {
                $userPerms = Doctrine::getTable('sfGuardUser')->getPermissions(5);
            }
        } else {
            $userPerms = Doctrine::getTable('sfGuardUser')->getPermissions(5);
            $_SESSION['logedin_user_group'] = 'subAdmin2';
        }
    }
    /* @Aditi- added for sign in as  new role of teller
     *  20- jun 2012
     */ else if (isset($_SESSION['logedin_user_group']) && $_SESSION['logedin_user_group'] == 'teller') {
        if (!in_array('teller', $groups)) {
            if (isset($_SESSION['is_logout']) && ($_SESSION['is_logout'] == 1)) {
                $userPerms = Doctrine::getTable('sfGuardUser')->getPermissions(6);
            }
        } else {
            $userPerms = Doctrine::getTable('sfGuardUser')->getPermissions(6);
            $_SESSION['logedin_user_group'] = 'teller';
        }
    } else if (isset($_SESSION['logedin_user_group']) && $_SESSION['logedin_user_group'] == 'support') {
        if (!in_array('support', $groups)) {
            if (isset($_SESSION['is_logout']) && ($_SESSION['is_logout'] == 1)) {
                $userPerms = Doctrine::getTable('sfGuardUser')->getPermissions(7);
            }
        } else {
            $userPerms = Doctrine::getTable('sfGuardUser')->getPermissions(7);
            $_SESSION['logedin_user_group'] = 'support';
        }
    }
    foreach ($elem as $k => $aElem) {
        $res = isValidMenuItem($aElem, $userPerms);
        if (!$res) {
            unset($elem[$k]);
        } else {
            $elem[$k] = $res;
        }
    }

    return $elem;
}

function isValidMenuItem($elem, $uperms) {
    if (!array_key_exists('perm', $elem)) {
        $myperm = '';
    } else {
        $myperm = $elem['perm'];
    }
    // check if my permission is there in current user permissions list
    if (!empty($myperm)) {
        if (!in_array($myperm, $uperms)) {
            return false;
        }
    }
    // I do have permission for myself at least
    // find out if I have child
    if (!array_key_exists('child', $elem)) {
        // no child - find out if I have url
        if (array_key_exists('url', $elem) && (!empty($elem['url']))) {
            // at least I have url - I am valid entity
            return $elem;
            ;
        }
        // I don't have url
    }
    // I do have children
//    echo "<pre>";
//    print_r($elem['child']);
//    exit;
    foreach ($elem['child'] as $k => $aChild) {
        $retVal = isValidMenuItem($aChild, $uperms);
        if (!$retVal) {
            // not a valid menu item - remove it
            unset($elem['child'][$k]);
        } else {
            $elem['child'][$k] = $retVal;
        }
    }
    // All children validated - see if I have any valid child
    if (count($elem['child'])) {
        // I have valid child(ren)
        return $elem;
    }
    // No valid child!!
    return false;
}

