<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of helper
 *
 * This helper is written for bulk upload through spreadsheets in Enterprise and Registration Module
 *
 * @author amalhotra
 */
//rename class to ExcelHelper
class csvHelper {

// This function checks the extension of the file(.xls).
    /*
     * Manu :Feedback item [A5] Merchant – Invoice upload via CSV-new column added
     * 13 Apr 2012
     */
    private $invoice_mandatory_columns = array("CustomerEmailAddress", "AdditionalInformation", "CurrencyType", "IssueDate", "DeliveryDate", "DueDate", "ItemQuantity1", "ItemDescription1", "ItemUnitPrice1");
    private $customer_mandatory_columns = array("EmailAddress", "Password", "Name", "Sex", "Address", "PhoneNumber");

    public function saveFileInTargetFolder($file, $targetPath) {
        if (move_uploaded_file($file, $targetPath)) {
            return true;
        } else {
            return false;
        }
    }

    public function validateFileType($fileType, $requiredFileType) {

        if ($fileType == $requiredFileType) {
            return true;
        } else {
            return false;
        }
    }

    public function findexts($filename) {
        $filename = strtolower($filename);
        $exts = preg_split("[/\\.]", $filename); // split replaced with preg_split
        $n = count($exts) - 1;
        $exts = $exts[$n];
        return $exts;
    }

    // this function validate and save the spreadsheet data.
    public function save_Excel_data($target_path, $minimum_Cols, $classNames, $limitsArray, $Tables_instances, $merchant_id) {
        set_time_limit(0);
        $det = array();


        $objReader = PHPExcel_IOFactory::createReader('CSV');

        $objPHPExcelFromCSV = $objReader->load($target_path);

        $message = '';

        $conn = Doctrine::getTable($Tables_instances)->getConnection();
        $conn->beginTransaction();

        $worksheet = $objPHPExcelFromCSV->getActiveSheet();


        if (PHPExcel_Cell::columnIndexFromString(($worksheet->getHighestColumn())) >= $minimum_Cols) {
            $number_of_rows = $worksheet->getHighestRow();
            if ($number_of_rows > 1) {

//                    switch (count($classNames)) {
//                        case 1:
//                            $limit_of_update = 6825;
//                            break;
//                        case 2:
//                            $limit_of_update = 2106;
//                            break;
//                    }
//                    if ((count($classNames) == 1 && $number_of_rows <= $limit_of_update) || (count($classNames) > 1 && $number_of_rows <= $limit_of_update)) {

                $is_first_row = 1;
                /* --  Iterating through each Row------ */
//                $i=0;
                $savingParametersForSendingMail = array();
                foreach ($worksheet->getRowIterator() as $row) {
                    $tax = 0;
                    $empty_mandatory_column_message = "";
                    if ($is_first_row) {

                        $is_first_row = 0;
                    } else {

                        $empty_row_check = 0;
                        for ($col = 0; $col < PHPExcel_Cell::columnIndexFromString($worksheet->getHighestColumn()); $col++) {

                            if (is_null(($worksheet->getCellByColumnAndRow($col, $row->getRowIndex())->getValue()))) {

                                $empty_row_check++;
                            }
                        }
                        if ($empty_row_check != PHPExcel_Cell::columnIndexFromString($worksheet->getHighestColumn())) {
                            $cellIterator = $row->getCellIterator();

                            $cellIterator->setIterateOnlyExistingCells(false); // Loop all cells, even if it is not set
                            /* --  Iterating each Cell in Row-------- */
                            $cellNo = 0;

                            for ($w = 1; $w <= PHPExcel_Cell::columnIndexFromString($worksheet->getHighestColumn()); $w++) {

                                $cell = $cellIterator->current();


                                try {
//                                            $row_no=0;
                                    if (!is_null($cell->getValue())) {

                                        $field[$worksheet->getCellByColumnAndRow($w - 1, 1)->getValue()] = ltrim(rtrim($cell->getValue()));
                                    } else {
                                        $field[$worksheet->getCellByColumnAndRow($w - 1, 1)->getValue()] = null;
                                        $mandatory = $this->chkIfMandatory($worksheet->getCellByColumnAndRow($w - 1, 1)->getValue(), $Tables_instances);

                                        if ($mandatory) {
                                            $empty_mandatory_column_message.= '<li>No Data at Row:' . $row->getRowIndex() . ',Column: ' . $worksheet->getCellByColumnAndRow($w - 1, 1)->getValue();
                                            $message.= '<li>No Data at Row:' . $row->getRowIndex() . ',Column: ' . $worksheet->getCellByColumnAndRow($w - 1, 1)->getValue();
                                        }
                                    }
                                    if (isset($field)) {

                                        $save_object = false;
                                        if (PHPExcel_Cell::columnIndexFromString($worksheet->getHighestColumn()) == count($field) && $empty_mandatory_column_message == '') {

                                            $save_object = true;
                                        }

                                        if ($save_object) {



                                            $temp_array = array();
                                            if ($Tables_instances == 'Invoice') {
                                                $is_part_payment_applicable = false;
                                                $amountDue = 0;
                                                $subject = "Regular Invoice";
                                                $temp_array['subject'] = "Regular Invoice";
                                                $temp_array['partial_name'] = 'emailContent';
                                                $temp_array['next_invoice'] = null;
                                            }
                                            for ($x = 0; $x < count($classNames); $x++) {
//                                                    echo $x."<br>";


                                                if ($x == 0) {
                                                    $ob = new $classNames[$x]();
                                                }
                                                for ($j = $limitsArray[$classNames[$x]][0]; $j <= $limitsArray[$classNames[$x]][1]; $j++) {

                                                    $columnName = $worksheet->getCellByColumnAndRow($j - 1, 1)->getValue();
                                                    if ($j <= $limitsArray[get_class($ob)][1]) {

                                                        //If parent Coulumn
//

                                                        $func = "set" . $columnName;
                                                        if (!is_null($field[$columnName])) {
                                                            $result = $this->dataValidation($columnName, $field[$columnName], &$temp_array, $row->getRowIndex(), $merchant_id);

                                                            if ($columnName == 'CustomerEmailAddress') {
                                                                if (is_integer($result)) {
                                                                    $func = "setCustomerId";

                                                                    $ob->$func($result);
                                                                    $ob->setMerchantId($merchant_id);
                                                                    /*                                                                     * ---------------------Invoice number generate Start---------------------------------- */
                                                                    $merchantName = &$temp_array['company_name'];
                                                                    $merchantId = $merchant_id;
                                                                    $invoiceNo = pay4billHelper::getInvoiceNo($merchantId, $merchantName);
                                                                    $temp_array['invoice_no'] = $invoiceNo;
                                                                    /*                                                                     * ---------------------Invoice number generate End---------------------------------- */

                                                                    $ob->setInvoiceNo($invoiceNo);
                                                                } else {
//                                                                    $message.='<li>'.$result."</li>";
                                                                    throw new Exception($result);
                                                                }
                                                            } else if ($columnName == 'PartPaymentPercentage') {
//                                                            print_r('Partpayment');
                                                                if ($result === $field[$columnName]) {
                                                                    $func = "setPartPayment";

                                                                    $is_part_payment_applicable = true;
                                                                    $ob->$func($result);
                                                                } else {
                                                                    $message.=$result;
                                                                }
                                                            } else if ($columnName == 'ItemQuantity1' || $columnName == 'ItemQuantity2' || $columnName == 'ItemQuantity3' || $columnName == 'ItemQuantity4' || $columnName == 'ItemDescription1' || $columnName == 'ItemDescription2' || $columnName == 'ItemDescription3' || $columnName == 'ItemDescription4' || $columnName == 'ItemUnitPrice1' || $columnName == 'ItemUnitPrice2' || $columnName == 'ItemUnitPrice3' || $columnName == 'ItemUnitPrice4' || $columnName == 'ItemTax') {
                                                                /*
                                                                 * Manu :Feedback item [A5] Merchant – Invoice upload via CSV-new column added
                                                                 * 13 Apr 2012
                                                                 */
                                                                if (isset($result['no'])) {
                                                                    $det['no'] = $result['no'];
                                                                }
                                                                if (isset($result['desc'])) {
                                                                    $det['desc'] = $result['desc'];
                                                                }
                                                                if (isset($result['price'])) {
                                                                    $det['price'] = $result['price'];
                                                                }
                                                                if (isset($result['tax'])) {
                                                                    $tax = $result['tax'];
                                                                }
                                                                if (is_array($result) && @count($det) == 3) {
                                                                    $details_ob = new InvoiceItemDetails();
                                                                    $details_ob->setInvoice($ob);
                                                                    $details_ob->setNoOfItems($det['no']);
                                                                    $details_ob->setItemDescription($det['desc']);
                                                                    $details_ob->setItemPrice($det['price']);
                                                                    $details_ob->setItemTax($tax);
                                                                    $details_ob->setSubTotal(round($details_ob->getItemPrice() * $details_ob->getNoOfItems(), 2));
                                                                    $details_ob->setTaxTotal(round(($details_ob->getSubTotal() * $details_ob->getItemTax() / 100), 2));
                                                                    $amountDue +=$details_ob->getSubTotal() + $details_ob->getTaxTotal();

                                                                    $details_ob->save();
                                                                    unset($det);

//                                                            } else if ($columnName == 'InvoiceItemDetails') {
//                                                                if (is_array($result)) {
//                                                                    foreach ($result as $eachDetail) {
//                                                                        $details_ob = new InvoiceItemDetails();
//                                                                        $details_ob->setInvoice($ob);
//                                                                        $details_ob->setNoOfItems($eachDetail['no']);
//                                                                        $details_ob->setItemDescription($eachDetail['desc']);
//                                                                        $details_ob->setItemPrice($eachDetail['price']);
//
//                                                                        $details_ob->setItemTax($eachDetail['tax']);
//                                                                        $details_ob->setSubTotal(round($details_ob->getItemPrice() * $details_ob->getNoOfItems(), 2));
//                                                                        $details_ob->setTaxTotal(round(($details_ob->getSubTotal() * $details_ob->getItemTax() / 100), 2));
//                                                                        $amountDue +=$details_ob->getSubTotal() + $details_ob->getTaxTotal();
//
//                                                                        $details_ob->save();
//                                                                    }
                                                                } else if (!is_array($result)) {
                                                                    $message.=$result;
                                                                }
//
                                                            } else if ($columnName == 'RecurrentBilling') {
//                                                            print_r('Recurring');
                                                                if (is_array($result)) {

//                                                                $partial_name = 'recurringInvoice';
                                                                    $temp_array['partial_name'] = 'recurringInvoice';
                                                                    $recurring_obj = new RecurrentBilling();
                                                                    $recurring_obj->setInvoice($ob);
                                                                    $recurring_obj->setType($result['plan']);

                                                                    $recurring_obj->setStatus('live');

                                                                    switch ($recurring_obj->getType()) {

                                                                        case 'weekly':
                                                                            $nextInvoice = date("d-M-Y", strtotime($ob->getDueDate() . "+1 week"));
                                                                            break;
                                                                        case 'monthly':
                                                                            $nextInvoice = date("d-M-Y", strtotime($ob->getDueDate() . "+1 month"));
                                                                            break;
                                                                        case 'yearly':
                                                                            $nextInvoice = date("d-M-Y", strtotime($ob->getDueDate() . "+1 year"));
                                                                            break;
                                                                        case 'quarterly':
                                                                            $nextInvoice = date("d-M-Y", strtotime($ob->getDueDate() . "+3 months"));
                                                                            break;
                                                                        case 'custom':
                                                                            $recurring_obj->setValue($result['value']);
                                                                            $nextInvoice = date("d-M-Y", strtotime($ob->getDueDate() . "+" . $result['value'] . " days"));
                                                                            break;
                                                                    }
                                                                    $recurring_obj->save();
                                                                    $temp_array['next_invoice'] = $nextInvoice;
                                                                    $partial_name = 'recurringInvoice';
                                                                    $temp_array['subject'] = "Re : " . $temp_array['company_name'] . " Invoice Reminder";
                                                                } else {
                                                                    $message.=$result;
                                                                }
                                                            } else {
                                                                if ($result === $field[$columnName]) {
                                                                    $ob->$func($field[$columnName]);
                                                                } else {
                                                                    if ($columnName == 'InvoiceNo') {
                                                                        throw new Exception($result);
                                                                    } else {
                                                                        $message.=$result;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    } else {
//                                                            echo  $columnName."<br>";
                                                        $result = $this->dataValidation($columnName, $field[$columnName], &$temp_array, $row->getRowIndex(), $merchant_id);
//
                                                        if ($result === $field[$columnName]) {
                                                            $a = $ob->$classNames[$x];
                                                            $func = "set" . $columnName;
                                                            $a->$func($field[$columnName]);
                                                        } else {
                                                            $message.=$result;
                                                        }
                                                    }
                                                }
                                            }
                                            if ($Tables_instances == 'sfGuardUser') {
                                                $this->setUsername_and_Merchant($ob, $merchant_id, $classNames);
                                            } else if ($Tables_instances == 'Invoice') {

                                                if ($is_part_payment_applicable == true) {
                                                    $ob->setAmountDue($ob->getPartPayment() * $amountDue / 100);
                                                } else {
                                                    $ob->setAmountDue($amountDue);
                                                }
                                                $this->check_Issue_Delivery_Due_date_validation($ob, &$message, $row->getRowIndex());
                                                $ob->setAmountPaid(0);
                                            }
                                            $ob->save();
                                            //Manu : Remove Merchant Id from customer table and mapping the customer and merchant MerchantCustomer table
                                            $temp_array['invoice_ob_id'] = $ob->getId();
                                            if ($Tables_instances == 'sfGuardUser') {
                                                $new_merchant_customer_obj = new MerchantCustomer();
                                                $new_merchant_customer_obj->setCustomerId($ob->getCustomerUser()->getId());
                                                $new_merchant_customer_obj->setMerchantId($merchant_id);
                                                $new_merchant_customer_obj->save();
                                            } else {
                                                $temp_array['company_url'] = $ob->Merchant->getCompanyUrl();
                                            }
                                            if (isset($temp_array)) {
                                                if (count($temp_array)) {

                                                    $savingParametersForSendingMail[] = $temp_array;
                                                }
                                            }

                                            unset($temp_array);


                                            unset($field);
                                        }
                                    }
                                }

                                /* --------If invalid Data Exception caught----- */ catch (Exception $e) {
//                                    echo $e->getMessage();
                                    if ($e->getCode() == 23000) {
                                        if (stripos($e->getMessage(), '1062')) {

                                            $pos = stripos($message, "<li>Row : " . $row->getRowIndex() . " already Exists</li>");

                                            if ($pos === false) {
                                                $message.= "<li>Row : " . $row->getRowIndex() . " already Exists</li>";
                                            }
                                        } elseif (stripos($e->getMessage(), '1452')) {

                                            $temp_string = explode("FOREIGN KEY (`", $e->getMessage());
                                            $field = explode("`)", $temp_string[1]);
                                            $pos = stripos($message, "<li>" . "Foreign Key Constraint: Invalid Data at Row " . $row->getRowIndex() . ",Column:" . str_replace(" ", "", ucwords(str_replace("_", " ", $field[0]))) . "</li>");


                                            if ($pos === false) {
                                                $message.="<li>" . "Foreign Key Constraint: Invalid Data at Row " . $row->getRowIndex() . ",Column:" . str_replace(" ", "", ucwords(str_replace("_", " ", $field[0]))) . "</li>";
                                            }
                                        }
                                    } elseif (substr_count($e->getMessage(), "Unknown")) {
                                        if ((strpos($message, "<li>Column '" . $columnName . "'</li>")) or $message == '') {
                                            if (empty($key)) {
                                                $message.= "<li>Column '" . $columnName . "' doesn't exist.Check the Excel Sheet Format</li>";
                                            } else {
                                                $message.= "<li>Column '" . $key . "' doesn't exist.Check your default values</li>";
                                            }
                                        }
                                    } else {

                                        $pos = stripos($message, $e->getMessage());


//
                                        if ($pos === false) {
                                            $message.= '<li>' . $e->getMessage() . "</li>";
//                                                                                $message.= '<li>Invalid RecruitmentId at Row:' . $row->getRowIndex() . "(".$this->is_valid_recruitmentId_for_newPersonnel($field[$columnName]).")";
                                        }
                                    }
                                }//End Catch
                                $cellIterator->next();
                            }//End Cell Iteration
                        }//If row not empty
                    }
                }//End Row Iteration
                /* --------If valid Data Commit the transaction------ */
                if (empty($message)) {
                    $conn->commit();


//                    $message = '                   success';
                } else {
                    $conn->rollback(); /* --------Else rollback----- */
                }
//                    } else {
//                        $message = '<li>Cannot Upload.Number of Rows should be less than :' . $limit_of_update . '</li>';
//                    }
            } else {
                $message = '<li>No Data in the Excel Sheet</li>';
            }
        } else {
            $message = '<li>Insufficient Number Of Columns.Check the Format</li>';
        }

        if ($message == '') {
            return $savingParametersForSendingMail;
        } else {
            $message = '<ul class="msg_ul">' . $message . '</ul>';
            return $message;
        }
    }

//


    private function setUsername_and_Merchant($ob, $merchant_id, $classNames) {
        $ob->setUsername($ob->getEmailAddress());
        $ob->link('Groups', array(2));
        //Manu : Remove Merchant Id from customer table and mapping the customer and merchant MerchantCustomer table
//        $ob->link($classNames[1]->setMerchantId($merchant_id));
    }

    private function chkEmail($value) {
        preg_match("/^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i", $value, $matches);
        if (count($matches)) {
            return true;
        } else {
            return false;
        }
    }

    private function chkPassword($value) {
        if (strlen($value) < 6) {
            return false;
        } else {
            return true;
        }
    }

    private function chkSex($value) {
        if (strtolower($value) == 'male' || strtolower($value) == 'female') {
            return true;
        } else {
            return false;
        }
    }

//Invoce No auto generated
//    private function chkInvoiceNo($value, $rowNo) {
//
//        $returnBoolean = Doctrine::getTable('Invoice')->chk_Invoice_number($value);
//        if ($returnBoolean) {
//            $pattern = '/^[0-9]{1,10}$/';
//            preg_match_all($pattern, $value, $matches);
//            if (count($matches[0]) == 0) {
//                $return = false;
//            } else {
//                $return = true;
//            }
//        } else {
//            $return = "Invoice Number at Row:" . $rowNo . " Already Exists";
//        }
//        return $return;
//    }

    private function chkPhoneNumber($value) {
        preg_match("/^[0-9]*$/", $value, $matches);
        if (count($matches) && strlen($value) == 10) {
            return true;
        } else {
            return false;
        }
    }

    private function chkCurrency($value) {
        if (strtolower($value) == 'naira') {
            return true;
        } else {
            return false;
        }
    }

    private function chkRecurrentBilling($value, $row_no) {
        $info = "";




        if (substr(ltrim(rtrim($value)), 0, 1) == "{" && substr(ltrim(rtrim($value)), strlen(ltrim(rtrim($value))) - 1, 1) == "}") {
            $individual_values = explode("|", substr(ltrim(rtrim($value)), 1, strlen(ltrim(rtrim($value))) - 2));

            if (count($individual_values) == 2 && $individual_values[0] == 'custom') {
                $pattern1 = "/^([1-9]){1,}[0-9]{0,2}$/";

                preg_match($pattern1, $individual_values[1], $matches1);
                if (count($matches1) == 0) {
                    $info.= "<li>Invalid No of Days for Recurring Plan at Row:" . $row_no . "(Max 999)</li>";
                } else {
                    if (intval($individual_values[1]) > 999) {
                        $info.= "<li>Invalid No of Days for Recurring Plan at Row:" . $row_no . "(Max 999)</li>";
                    } else {
                        $array_values['plan'] = $individual_values[0];
                        $array_values['value'] = $individual_values[1];
                    }
                }
            } else {


                switch ($individual_values[0]) {
                    case "weekly":

                        $array_values['plan'] = $individual_values[0];
                        $valid = true;
                        break;
                    case "yearly":
                        $array_values['plan'] = $individual_values[0];
                        break;
                    case "quarterly":
                        $array_values['plan'] = $individual_values[0];
                        break;
                    case "monthly":
                        $array_values['plan'] = $individual_values[0];
                        break;
                    default:
                        $info = "Invalid Recurring Plan at Row:" . $row_no;
                        break;
                }
            }
            if ($info == '') {
                return $array_values;
            } else {
                return $info;
            }
        } else {
            return "Incomplete/Malformed Recurring Plan Details at Row:" . $row_no;
        }
    }

    private function chkItemQuantity($value, $row_no) {
        $pattern1 = "/^([1-9]){1}([0-9]){0,2}$/";
        $info = '';
        preg_match($pattern1, $value, $matches1);
        //Varun : Bug FIx : FS#34918
        if (count($matches1) == 0) {
            $info.= "<li>Invalid Item Number(Max 999) at Row:" . $row_no . "</li>";
        } else {
            if (intval($value) > 999) {
                $info.= "<li>Item Number(Max 999) at Row:" . $row_no . "</li>"; //Varun : Bug FIx : FS#34918
            } else {
                $array_values['no'] = $value;
            }
        }
        if ($info == '') {
            return $array_values;
        } else {
            return $info;
        }
    }

    /*
     * Manu :Feedback item [A5] Merchant – Invoice upload via CSV-new column added
     * 13 Apr 2012
     */

    private function chkItemDescription($value, $row_no) {
        $pattern2 = "/^.+$/";
        $info = '';
        preg_match($pattern2, $value, $matches2);
        //Varun : Bug fix : FS#34912
        if (count($matches2) == 0) {
            $info.= "<li>Invalid Item Description at Row:" . $row_no . "</li>"; //Varun : Bug FIx : FS#34918
        } else {
            if (strlen($value) > 50) {
                $info.= "<li> Item Description cannot be more than 50 characters .String at Row:" . $row_no . "</li>";
            } else {
                $array_values['desc'] = $value;
            }
        }
        if ($info == '') {
            return $array_values;
        } else {
            return $info;
        }
    }

    /*
     * Manu :Feedback item [A5] Merchant – Invoice upload via CSV-new column added
     * 13 Apr 2012
     */

    private function chkItemUnitPrice($value, $row_no) {
        $pattern3 = "/^[1-9]+([0-9])*(?:\.[0-9]{1,3})?$/";
        $pattern4 = "/^(0){1}(\.){1}([1-9]){1,3}$/";
        $info = '';
        preg_match($pattern4, $value, $matches4);
        preg_match($pattern3, $value, $matches3);
        if (count($matches3) == 0 && count($matches4) == 0) {
            $info.= "<li>Invalid Item Price at Row:" . $row_no . "</li>";
        } else {
            //varun : Bug Fix : FS#34915
            if (floatval($value) > 99999999) {
                $info.= "<li>Invalid Item Price(Max 99999999) at Row:" . $row_no . "</li>"; //Varun : Bug FIx : FS#34918
            } else if (floatval($value) <= 0) { //Bug : FS#34913
                $info.= "<li>Invalid Item Price should be grater than 0 at Row:" . $row_no . "</li>"; //Varun : Bug FIx : FS#34918
            } else {
                $array_values['price'] = $value;
            }
        }
        if ($info == '') {
            return $array_values;
        } else {
            return $info;
        }
    }

    /*
     * Manu :Feedback item [A5] Merchant – Invoice upload via CSV-new column added
     * 13 Apr 2012
     */

    private function chkItemTax($value, $row_no) {
        $info = '';
        $pattern5 = "/^[1-9]+([0-9])*(?:\.[0-9]{1,3})?$/";
        $pattern6 = "/^(0){1}(\.){1}([1-9]){1,3}$/";

        preg_match($pattern5, $value, $matches5);
        preg_match($pattern6, $value, $matches6);
        if (count($matches6) == 0 && count($matches5) == 0) {
            $array_values ['tax'] = 0;
            //Manu : Tax is optional not mendatory
//                                        $info.= "<li>Invalid Tax % for Details(" . ($main_key + 1) . ") String at Row:" . $row_no . "</li>";
        } else {
            if (floatval($value) > 99.99) {
                $info.= "<li>Invalid Tax%(Max 99.99) at Row:" . $row_no . "</li>"; //Varun : Bug FIx : FS#34918
            } else {
                $array_values['tax'] = $value;
            }
        }
        if ($info == '') {
            return $array_values;
        } else {
            return $info;
        }
    }

    private function chkInvoiceItemDetails($value, $row_no) {

        $no_of_item_details = explode(";", substr(ltrim(rtrim($value)), 0, strlen(ltrim(rtrim($value)))));
        if (count($no_of_item_details) < 6) {
            $main_array_to_return = array();
            $info = "";
            foreach ($no_of_item_details as $main_key => $items_string) {

                if (count($items_string) != 0) {
                    $individual_values = explode("|", substr($items_string, 1, strlen($items_string) - 2));

                    if (count($individual_values) == 4 && substr(ltrim(rtrim($value)), 0, 1) == "{" && substr(ltrim(rtrim($value)), strlen(ltrim(rtrim($value))) - 1, 1) == "}") {

                        foreach ($individual_values as $key => $string) {

                            switch ($key) {
                                case 0:
                                    $pattern1 = "/^([1-9]){1}([0-9]){0,2}$/";

                                    preg_match($pattern1, $string, $matches1);
                                    if (count($matches1) == 0) {
                                        $info.= "<li>Invalid Item Number(Max 999) at Row:" . $row_no . "</li>"; //Varun : Bug FIx : FS#34918
                                    } else {
                                        if (intval($string) > 999) {
                                            $info.= "<li>Item Number(Max 999) at Row:" . $row_no . "</li>"; //Varun : Bug FIx : FS#34918
                                        } else {
                                            $array_values['no'] = $string;
                                        }
                                    }

                                    break;
                                case 1:
                                    $pattern2 = "/^.+$/";

                                    preg_match($pattern2, $string, $matches2);
                                    if (count($matches2) == 0) {
                                        $info.= "<li>Invalid Item Description at Row:" . $row_no . "</li>"; //Varun : Bug FIx : FS#34918
                                    } else {
                                        $array_values['desc'] = $string;
                                    }
                                    break;

                                case 2:
                                    $pattern3 = "/^[1-9]+([0-9])*(?:\.[0-9]{1,3})?$/";
                                    $pattern4 = "/^(0){1}(\.){1}([1-9]){1,3}$/";

                                    preg_match($pattern4, $string, $matches4);
                                    preg_match($pattern3, $string, $matches3);
                                    if (count($matches3) == 0 && count($matches4) == 0) {
                                        $info.= "<li>Invalid Item Price  at Row:" . $row_no . "</li>"; //Varun : Bug FIx : FS#34918
                                    } else {
                                        if (floatval($string) > 99999999) {
                                            $info.= "<li>Invalid Item Price(Max 99999999) at Row:" . $row_no . "</li>"; //Varun : Bug FIx : FS#34918
                                        } else {
                                            $array_values['price'] = $string;
                                        }
                                    }
                                    break;
                                case 3:
                                    $pattern5 = "/^[1-9]+([0-9])*(?:\.[0-9]{1,3})?$/";
                                    $pattern6 = "/^(0){1}(\.){1}([1-9]){1,3}$/";

                                    preg_match($pattern5, $string, $matches5);
                                    preg_match($pattern6, $string, $matches6);
                                    if (count($matches6) == 0 && count($matches5) == 0) {
                                        //Manu : Tax is optional not mendatory
//                                        $info.= "<li>Invalid Tax % for Details(" . ($main_key + 1) . ") String at Row:" . $row_no . "</li>";
                                    } else {
                                        if (floatval($string) > 99.99) {
                                            $info.= "<li>Invalid Tax%(Max 99.99) at Row:" . $row_no . "</li>"; //Varun : Bug FIx : FS#34918
                                        } else {
                                            $array_values['tax'] = $string;
                                        }
                                        break;
                                    }
                            }
                        }
                    } else {
                        $info = "<li>Incomplete/Malformed Item Details at Row:" . $row_no . "</li>";
                    }
                    if ($info == '') {
                        $main_array_to_return[] = $array_values;
                        unset($array_values);
                    }
                }
            }
        } else {
            $info = "<li>Maximum 5 Item Details String Accepted. Check Row:" . $row_no;
        }
        if ($info == '') {
            return $main_array_to_return;
        } else {
            return $info;
        }
    }

    private function chkPartPaymentPercentage($value) {
        preg_match("/^[1-9]{1,}([0-9])*(?:\.[0-9]{1,3})?$/", $value, $matches);
        if (count($matches) && floatval($value) < 100) {
            return true;
        } else {
            preg_match("/^[1-9]{1,}([0-9])*(?:\.[0-9]{1,3})?$/", $value, $matches);
            if (count($matches) && floatval($value) < 100) {
                return true;
            } else {
                return false;
            }
        }
    }

    private function chkDate($value, $str) {
        // Varun : Bug fix :FS#34904
        $date_format = explode('/', $value);
        if (count($date_format) == '1') {
            $return = "Invalid " . $str . "Format";
        } else {
            if ($this->is_date($value)) {

                if (strtotime($value) < strtotime(date('Y-m-d'))) {
                    $return = $str . " Can't be Past Date ";
                } else {
                    $return = true;
                }
            } else {
                $return = "Invalid " . $str;
            }
        }


        return $return;
    }

    private function is_date($str) {

        $stamp = strtotime(ltrim(rtrim($str)));

        if (!is_numeric($stamp)) {
            return FALSE;
        }
        $month = date('m', $stamp);
        $day = date('d', $stamp);
        $year = date('Y', $stamp);

        if (checkdate($month, $day, $year)) {
            return TRUE;
        }

        return FALSE;
    }

    private function check_Issue_Delivery_Due_date_validation($ob, $message, $row_no) {

        $issueDate = strtotime($ob->getIssueDate());
        $deliveryDate = strtotime($ob->getDeliveryDate());
        $due_date = strtotime($ob->getDueDate());
        $checkFurther = true;
        if (is_null($issueDate)) {
            $message.="<li>Issue Date Should be less than" . date("Y-m-d", strtotime($ob->getIssueDate() . "+10 years")) . " at Row:" . $row_no . "</li>";
            $checkFurther = false;
        }
        if (is_null($deliveryDate)) {
            $checkFurther = false;
            $message.="<li>Delivery Date Should be less than" . date("Y-m-d", strtotime($ob->getIssueDate() . "+10 years")) . " at Row:" . $row_no . "</li>";
        }
        if (is_null($due_date)) {
            $checkFurther = false;
            $message.="<li>Due Date Should be less than" . date("Y-m-d", strtotime($ob->getIssueDate() . "+10 years")) . " at Row:" . $row_no . "</li>";
        }
        if ($checkFurther) {
            if ($issueDate > $deliveryDate) {
                $message.="<li>Delivery  Date Should be greater than Issue Date at Row:" . $row_no . "</li>";
            }
            if ($deliveryDate > $due_date) {
                $message.="<li>Due  Date Should be greater than Delivery Date at Row:" . $row_no . "</li>";
            }
        }
    }

    private function ChkAllMandatoryColumns_filled($fields_array, $Tables_instances) {
        $return = true;

        switch ($Tables_instances) {
            case "Invoice":
                foreach ($this->invoice_mandatory_columns as $value) {
//                echo $value."<br>";
                    if (!array_key_exists($value, $fields_array)) {

                        $return = false;
                    }
                }
                break;


            case "sfGuardUser":
                foreach ($this->customer_mandatory_columns as $value) {
                    if (!array_key_exists($value, $fields_array)) {
                        $return = false;
                    }
                }
                break;
        }
        return $return;
    }

    private function chkIfMandatory($fieldName, $Tables_instances, $data=null, $is_filled=null) {
        $mandatory = false;
        switch ($Tables_instances) {
            case "Invoice":
                if (in_array($fieldName, $this->invoice_mandatory_columns)) {
                    $mandatory = true;
                }


            case "sfGuardUser":
                if (in_array($fieldName, $this->customer_mandatory_columns)) {
                    $mandatory = true;
                }
        }

        return $mandatory;
    }

    private function dataValidation($fieldName, $value, $temp_array, $row_no, $merchant_id) {
        $message = $value;
        switch ($fieldName) {
            case "EmailAddress":
                $chk = $this->chkEmail($value);

                if (!$chk) {
                    $message = '<li>Invalid Email Address at: Row:' . $row_no . "</li>";
                } else {
                    $temp_array['username'] = $value;
                }
                break;
            case "Password":
                $chk = $this->chkPassword($value);
                if (!$chk) {
                    $message = '<li>Invalid Password at: Row:' . $row_no . "(At least 6 characters)</li>";
                } else {

                    $temp_array['password'] = $value;
                }
                break;
            case "PhoneNumber":
                $chk = $this->chkPhoneNumber($value);
                if (!$chk) {
                    $message = '<li>Invalid PhoneNumber at: Row:' . $row_no . "(10 digits)</li>";
                }
                break;
            case "Sex":
                $chk = $this->chkSex($value);
                if (!$chk) {
                    $message.= '<li>Invalid Sex at: Row:' . $row_no . "  </li>";
                } else {

                }
                break;
            case "Name":

                $temp_array['customer_name'] = $value;
                break;
            case "AdditionalInformation":
                //Bug fix : FS#34908
                if (strlen($value) > 100) {
                    $message = 'Additional Information is more than 100 characters  at: Row:' . $row_no;
                } else {
                    $temp_array['additional_information'] = $value;
                }
                break;
            case "CustomerEmailAddress":
                $chk = $this->chkEmail($value);
                if (!$chk) {
                    $message = 'Invalid Email Address  at: Row:' . $row_no; // Bug : FS#34903
                } else {

                    $if_valid_customer = Doctrine::getTable('CustomerProfile')->isValidCustomerForMerchant($merchant_id, $value);

                    $customer_array = $if_valid_customer->toArray();

                    if (count($if_valid_customer->toArray()) == 1) {

                        $message = (integer) $customer_array[0]['id'];
                        $merchantInfo = Doctrine::getTable('MerchantProfile')->find($merchant_id, Doctrine::HYDRATE_ARRAY);
                        $temp_array['email'] = $value;
                        $temp_array['customer_name'] = $customer_array[0]['name']; //Manu : Remove Merchant Id from customer table and mapping the customer and merchant MerchantCustomer table
                        $temp_array['company_name'] = $merchantInfo['merchant_name'];
                        $temp_array['tiny_url'] = $merchantInfo['tiny_url'];
                    } else {

                        $message = 'Customer  at: Row:' . $row_no . " doesn't exist";
                    }
                }
                break;
            //Invoce No auto generated
//            case "InvoiceNo":
//                $chk = $this->chkInvoiceNo($value, $row_no);
//                if (is_bool($chk)) {
//                    if (!$chk) {
//                        $message = 'Invalid Invoice Number at: Row:' . $row_no;
//                    } else {
//                        $temp_array['invoice_no'] = $value;
//                    }
//                } else {
//                    $message = $chk;
//                }
//                break;
            case "IssueDate":
                $chk = $this->chkDate($value, "Issue Date");
                if (is_string($chk)) {
                    $message = '<li>' . $chk . ' at: Row:' . $row_no . "</li>";
                } else {
                    $temp_array['issue_date'] = $value;
                }
                break;
            case "DueDate":
                $chk = $this->chkDate($value, 'Due Date');
                if (is_string($chk)) {
                    $message = '<li>' . $chk . ' at: Row:' . $row_no . "</li>";
                } else {
                    $temp_array['due_date'] = $value;
                }
                break;
            case "DeliveryDate":
                $chk = $this->chkDate($value, 'Delivery Date');
                if (is_string($chk)) {
                    $message = '<li>' . $chk . ' at: Row:' . $row_no . "</li>";
                } else {
                    $temp_array['delivery_date'] = $value;
                }
                break;
            case "PartPaymentPercentage":

                $chk = $this->chkPartPaymentPercentage($value);

                if (!$chk) {
                    $message = '<li>Invalid Part Payment Percentage(Max 99.99) at: Row:' . $row_no . "</li>";
                }

                break;
//            case "InvoiceItemDetails":
//
//                $chk = $this->chkInvoiceItemDetails($value, $row_no);
//
//
//                $message = $chk;
//
//                break;
            /*
             * Manu :Feedback item [A5] Merchant – Invoice upload via CSV-new column added
             * 13 Apr 2012
             */
            case "ItemQuantity1":
                $chk = $this->chkItemQuantity($value, $row_no);
                $message = $chk;
                break;
            case "ItemQuantity2":
                $chk = $this->chkItemQuantity($value, $row_no);
                $message = $chk;
                break;
            case "ItemQuantity3":
                $chk = $this->chkItemQuantity($value, $row_no);
                $message = $chk;
                break;
            case "ItemQuantity4":
                $chk = $this->chkItemQuantity($value, $row_no);
                $message = $chk;
                break;
            case "ItemUnitPrice1":
                $chk = $this->chkItemUnitPrice($value, $row_no);
                $message = $chk;
                break;
            case "ItemUnitPrice2":
                $chk = $this->chkItemUnitPrice($value, $row_no);
                $message = $chk;
                break;
            case "ItemUnitPrice3":
                $chk = $this->chkItemUnitPrice($value, $row_no);
                $message = $chk;
                break;
            case "ItemUnitPrice4":
                $chk = $this->chkItemUnitPrice($value, $row_no);
                $message = $chk;
                break;
            case "ItemDescription1":
                $chk = $this->chkItemDescription($value, $row_no);
                $message = $chk;
                break;
            case "ItemDescription2":
                $chk = $this->chkItemDescription($value, $row_no);
                $message = $chk;
                break;
            case "ItemDescription3":
                $chk = $this->chkItemDescription($value, $row_no);
                $message = $chk;
                break;
            case "ItemDescription4":
                $chk = $this->chkItemDescription($value, $row_no);
                $message = $chk;
                break;
            case "ItemTax":
                $chk = $this->chkItemTax($value, $row_no);
                $message = $chk;
                break;


            case "RecurrentBilling":

                $chk = $this->chkRecurrentBilling($value, $row_no);

                $message = $chk;
                break;
            case "CurrencyType":

                $chk = $this->chkCurrency($value);

                if (!($chk)) {
                    $message = "<li>Invalid Currency Type at Row:" . $row_no . "(Only Naira Accepted)</li>";
                } else {
                    $message = $value;
                    $temp_array['currency_type'] = $value;
                }

                break;
        }
        return $message;
    }

    public function exportData($invoices=null, $item_details=null, $id_Items_invoiceId=null, $filename, $fileType) {
        //Varun : Bug fix FS#34705, folder shifted inside uploads as permission is not caused for uploads folder
        $folder = "uploads/exported_files/";
        $objPHPExcel = new sfPhpExcel();
//        $setUp= new PHPExcel_Worksheet_PageSetup();
//        $setUp->setFitToHeight(1);
//        $setUp->setFitToWidth(1);

        $textAlign_obj = new PHPExcel_Style_Alignment();
        $textAlign_obj->setShrinkToFit(false);
        $textAlign_obj->applyFromArray(array("horizontal" => 'right'));
//      $objPHPExcel->getActiveSheet()->setSharedStyle($textAlign_obj);
//      $objPHPExcel->dd
        $objPHPExcel->setActiveSheetIndex(0);
//        PHPExcel_Shared_Font::inchSizeToPixels(8);
        
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, "Invoice Number");
        $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setAutoSize(true);
        $userGroups = sfContext::getInstance()->getUser()->getGuardUser()->getGroups()->toArray();
        $adminLogin = false;
        $merchantLogin = false;
        $reportSubAdminLogin = false;

        $adminLogin = pay4billHelper::checkIfAdminUser($userGroups);
        if (!$adminLogin) {
            $reportSubAdminLogin = pay4billHelper::checkIfSubAdminReportUser($userGroups);
            if (!$reportSubAdminLogin) {
                $merchantLogin = pay4billHelper::checkIfMerchantUser($userGroups);
            }
        }

        if ($adminLogin || $reportSubAdminLogin) {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 1, "Merchant Name");
        }

        if ($merchantLogin) {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 1, "Customer Name");
        }
        $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(1)->setAutoSize(false);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 1, "Customer Id");
        $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setAutoSize(true);


        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 1, "Amount(NGN)"); // Varun : Customer Feedback A3 Bug Fix : FS#34858

        $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(3)->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 1, "Issue Date");
        $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(4)->setAutoSize(false);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 1, "Delivery Date");
        $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(5)->setAutoSize(false);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, 1, "Due Date");
        $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(6)->setAutoSize(false);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, 1, "Payment Date");
        $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(7)->setAutoSize(false);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, 1, "Status");
        $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(8)->setAutoSize(false);
//        $objPHPExcel->getActiveSheet()->setPageSetup($setUp);
        $row = 2;

        foreach ($invoices as $invoice_ob) {
//$objPHPExcel->getActiveSheet()->setPageSetup
            $cell = 0;

            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($cell++, $row, $invoice_ob->getInvoiceNo());
            if ($adminLogin || $reportSubAdminLogin) {
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($cell++, $row, $invoice_ob->Merchant->getMerchantName());
            }if ($merchantLogin) {
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($cell++, $row, $invoice_ob->Customer->getName());
            }
//            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($cell)->setAutoSize(true);

            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($cell++, $row, $invoice_ob->Customer->getCustomerId());
            foreach ($item_details as $ob) {

                if ($ob->getInvoiceId() == $id_Items_invoiceId[$invoice_ob->getId()]) {
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($cell++, $row, "NGN " . number_format(($ob->getPartialSum() + $ob->getTax()), 2, '.', ','));
                }
            }

            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($cell++, $row, $invoice_ob->getIssueDate());
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($cell++, $row, $invoice_ob->getDeliveryDate());
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($cell++, $row, $invoice_ob->getDueDate());
            $payment_date = $invoice_ob->getPaymentDate() != '' ? $invoice_ob->getPaymentDate() : "NA";
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($cell++, $row, $payment_date);
//            echo "<pre>";
//            print_r($cell++);
//            print_r($row);
//            exit;
            switch ($invoice_ob->getStatus()) {
                case 0:
                    if (strtotime($invoice_ob->getDueDate()) >= strtotime(date("Y-m-d"))) {
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($cell++, $row, "Pending");
                    } else {
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($cell++, $row, "Expired");
                    }
                    break;
                case 1:
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($cell++, $row, "Cancelled");
                    break;
                case 2;
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($cell++, $row, "Paid");

                    break;
                case 3:

                    if (strtotime($invoice_ob->getDueDate()) >= strtotime(date("Y-m-d"))) {
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($cell++, $row, "Partially Paid");
                    } else {
                        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($cell++, $row, "Partial/Expired");
                    }

                    break;
            }
//            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($cell)->setAutoSize(true);
            $row++;
        }

        $objPHPExcel->getActiveSheet()->setTitle('Invoices');
        $objPHPExcel->setActiveSheetIndex(0);



        switch ($fileType) {
            case 'xls':

                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
//                $objWriter->
                $objWriter->save($folder . $filename . "." . $fileType);

                break;
            case 'pdf':
                $objWriter = new PHPExcel_Writer_PDF($objPHPExcel);
                $objWriter->save($folder . $filename . "." . $fileType);
                break;
            case 'csv':
                /**
                 * Varun : comma removed , as csv not shown properly
                 */
                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
                $objWriter->setDelimiter(' ');
                $objWriter->setEnclosure('"');
                $objWriter->setSheetIndex(0);
                $objWriter->save($folder . $filename . "." . $fileType);
                break;
        }
        chmod($folder . $filename . "." . $fileType, 0777);
        $this->download($folder, $filename . "." . $fileType);
    }

// This function is written for download functionality
    public function download($path, $filename) {
        $file_extension = strtolower(substr(strrchr($filename, "."), 1));

        switch ($file_extension) {
            case "pdf": $ctype = "application/pdf";
                break;
            case "csv": $ctype = "application/csv";
                break;
            case "exe": $ctype = "application/octet-stream";
                break;
            case "zip": $ctype = "application/zip";
                break;
            case "doc": $ctype = "application/msword";
                break;
            case "xls": $ctype = "application/vnd.ms-excel";
                break;
            case "ppt": $ctype = "application/vnd.ms-powerpoint";
                break;
            case "gif": $ctype = "image/gif";
                break;
            case "png": $ctype = "image/png";
                break;
            case "jpeg":
            case "jpg": $ctype = "image/jpg";
                break;
            default: $ctype = "application/force-download";
        }
        header("Pragma:public"); // required
        header("Expires:0");
        header("Cache-Control:must-revalidate,post-check=0,pre-check=0");
        header("Cache-Control:private", false);
        header("Content-Type:$ctype");
        header("Content-Disposition:attachment;filename=\"" . basename($filename) . "\";");

        header("Content-Transfer-Encoding:binary");


        readfile($path . $filename);
    }

// Varun : New functions added for amount format
    public static function price_format($amount) {
        //return '<span class="naira">'.sfConfig::get('app_currency').' '.number_format($amount, 2, '.', '').'</span>';
        return '<span class="naira">' . image_tag('naira.gif', array()) . ' ' . number_format($amount, 2, '.', ',') . '</span>';
    }

    public static function price_format_only($amount) {
        //return '<span class="naira">'.sfConfig::get('app_currency').' '.number_format($amount, 2, '.', '').'</span>';
        return '<span class="naira">' . number_format($amount, 2, '.', ',') . '</span>';
    }

}

?>