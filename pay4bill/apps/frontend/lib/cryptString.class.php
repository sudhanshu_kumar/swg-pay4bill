<?php

/**
 * @package    pay$bill
 * @author     Manu Datta
 */
class cryptString{

    /**
     *@author:  Manu Datta
     *@param <type> $string
     *@purpose:    To cncypt string
     */
  static function encrypt($string) {
    $Len_Simbolos = strlen(sfConfig::get('app_inscrypt_key'));
    if($Len_Simbolos <26)
    {
      echo 'ERROR';
      die;
    }
    $Len_Str_Message = strlen($string);
    $Str_Encoded_Message = "";
    for ($Position = 0; $Position < $Len_Str_Message; $Position++) {
      $Byte_To_Be_Encoded = substr($string, $Position, 1);
      $Ascii_Num_Byte_To_Encode = ord($Byte_To_Be_Encoded);
      $Rest_Modulo_Simbolos = ($Ascii_Num_Byte_To_Encode + $Len_Simbolos) % $Len_Simbolos;
      $Plus_Modulo_Simbolos = (int)($Ascii_Num_Byte_To_Encode / $Len_Simbolos);
      $Encoded_Byte = substr(sfConfig::get('app_inscrypt_key'), $Rest_Modulo_Simbolos, 1);
      if ($Plus_Modulo_Simbolos == 0) {
        $Str_Encoded_Message .= $Encoded_Byte;
      }else {
        $Str_Encoded_Message .= $Plus_Modulo_Simbolos . $Encoded_Byte;
      }
    }
    return base64_encode($Str_Encoded_Message);
  }
    /**
     *@author:  Manu Datta
     *@param <type> $string
     *@purpose:    To decrypt string
     */
  static function decrypt($string) {
    $Len_Simbolos = strlen(sfConfig::get('app_inscrypt_key'));
    if($Len_Simbolos <26)
    {
      echo 'ERROR';
      die;
    }
    $string=base64_decode($string);
    $Len_Str_Message = strlen($string);
    $Str_Decoded_Message = "";
    for ($Position = 0; $Position < $Len_Str_Message; $Position++) {
      $Plus_Modulo_Simbolos = 0;
      $Byte_To_Be_Decoded = substr($string, $Position, 1);
      if ($Byte_To_Be_Decoded > 0) {
        $Plus_Modulo_Simbolos = $Byte_To_Be_Decoded;
        $Position++;
        $Byte_To_Be_Decoded = substr($string, $Position, 1);
      }
      //finding the position in the string
      // $SIMBOLOS
      $Byte_Decoded = 0;
      for ($SecondPosition = 0; $SecondPosition < $Len_Simbolos; $SecondPosition++) {
        $Byte_To_Be_Compared = substr(sfConfig::get('app_inscrypt_key'), $SecondPosition, 1);
        if ($Byte_To_Be_Decoded == $Byte_To_Be_Compared) {
          $Byte_Decoded = $SecondPosition;
        }
      }
      $Byte_Decoded = ($Plus_Modulo_Simbolos * $Len_Simbolos) + $Byte_Decoded;
      $Ascii_Num_Byte_To_Decode = chr($Byte_Decoded);
      $Str_Decoded_Message .= $Ascii_Num_Byte_To_Decode;
    }
    return $Str_Decoded_Message;
  }
}
?>