<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of resetTellerPasswordForcefullyFilter
 *
 * @author saurabhk
 */


/* Saurabh @26 Dec 2012
 * Below condition has been added if the teller password has been reset by support user
 * teller must have to change the password when he logged in - first time into portal
 */

class resetTellerPasswordForcefullyFilter {
        
    public function execute($filterChain) { 
        
         if ((sfContext::getInstance()->getUser()->isAuthenticated()) && (sfContext::getInstance()->getUser()->getAttribute('user_group') == 6 && $_SESSION['logedin_user_group'] == 'teller')) {
            $user_id       = sfContext::getInstance()->getUser()->getGuardUser()->getId();
            $tellerProfile = Doctrine::getTable("TellerProfile")->findByUserId($user_id)->getFirst();
            $varLogOut = substr(sfContext::getInstance()->getRequest()->getURI(), -6);
            $varSlash = substr(sfContext::getInstance()->getRequest()->getURI(), -1);
           
              if ($varLogOut != 'logout' && $varSlash != '/') {
                $redirect_request1 = substr(sfContext::getInstance()->getRequest()->getURI(), -14); // change password
                $redirect_request2 = substr(sfContext::getInstance()->getRequest()->getURI(), -10); // save change
                if($tellerProfile->getChangePasswordRequired() == 1 && $redirect_request1 !='changePassword' && $redirect_request2 !='savechange'){
                   sfContext::getInstance()->getUser()->setFlash('error','Your password has been changed by pay4bill support team. Please change your password for security reasons.');  
                   sfContext::getInstance()->getController()->redirect("change_password_teller_forcefully");
                }
            }
        }
          $filterChain->execute();
     }
}

?>
