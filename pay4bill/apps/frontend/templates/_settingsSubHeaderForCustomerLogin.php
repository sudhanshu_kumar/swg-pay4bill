<?php use_helper('ePortal') ?>
<script language="javascript" >
    $(document).ready(function(){
        $('#Settings').addClass('active');
    });
</script>
<?php
/*
 * Add More Profile link for this section (after customer log in ) would not be displayed 
 * if the user have both the groups i.e., Merchant And Customer as this customer is
 * already linked up with the merchant so,the same customer can not 'Add More Profile'
 * 
 */
$userGroup = sfContext::getInstance()->getUser()->getGuardUser()->getGroups()->count();
?>
<div id="pgMenu">
    <div id="wrappernew">
        <div class="pgTitle">
            <h2>Settings</h2>
        </div>
        <div class="pgNav">
            <ul>
                <li class="<?php if ($addClassTo == 'changePassword') {echo "current";} ?>"><a href="<?php echo url_for('userAdmin/changePassword') ?>">Change Password</a></li>
                <?php if($userGroup == 1){?>
                    <li class="<?php if ($addClassTo == 'addProfile') {echo "current";} ?>"><a href="<?php echo url_for('custMgt/addProfile') ?>">Add Customer Profile</a></li>
                <?php }?>
                <li class="<?php if ($addClassTo == 'viewProfiles') {echo "current";} ?>"><a href="<?php echo url_for('custMgt/viewProfiles') ?>">View Customer Profiles</a></li>
            </ul>
        </div>
    </div>
</div>