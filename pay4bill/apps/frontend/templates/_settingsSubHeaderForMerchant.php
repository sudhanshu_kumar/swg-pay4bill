<?php use_helper('ePortal') ?>
<script language="javascript" >
    $(document).ready(function(){
        $('#Settings').addClass('active');
    });
</script>
<div id="pgMenu">
    <div id="wrappernew">
        <div class="pgTitle">
            <h2>Settings</h2>
        </div>
        <div class="pgNav">
            <ul>
                    <li><a href="<?php echo url_for('userAdmin/changePassword') ?>">Change Password</a></li>
                    <li class=""><a href="<?php echo url_for('userAdmin/viewBank') ?>">View Bank</a></li>
                    <li class=""><a href="<?php echo url_for('merchantRegistration/uploadMerchantLogo') ?>">Upload Merchant Logo</a></li>
                    <li  class="current"><a href="<?php echo url_for('custMgt/addProfile') ?>">Add Customer Profile</a></li>
                    <li class=""><a href="<?php echo url_for('userAdmin/viewDetail') ?>">View Details</a></li>
            </ul>
        </div>
    </div>
</div>