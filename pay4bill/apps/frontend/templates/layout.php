<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <?php include_http_metas() ?>
        <?php include_metas() ?>
        <?php include_javascripts() ?>
        <?php include_stylesheets() ?>
        <?php include_title() ?>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>pay4bill - Giving you multiple options to shop and pay your bills </title>
        <script type="text/javascript">
            $(document).ready(function() {
                $("#slides").SexySlider({
                    width : 610,
                    height : 288,
                    delay : 4000,
                    strips : 25,
                    autopause : true,
                    navigation: '#navigation',
                    control : '#slideControl',
                    effect : 'curtain' }
            );
				$('.FromInputLabel input[type=text]').addClass('FormInput2');
				$('.FromInputLabel input[type=password]').addClass('FormInput2');
                                $('.FromInputLabe3 input[type=text]').addClass('FormInput2');
				$('.FromInputLabe2 input[type=text]').addClass('FormInput2');
				$('.FromInputLabe3 input[type=password]').addClass('FormInput2');
				$('.FromInputLabe2 input[type=password]').addClass('FormInput2');
				$('.FromInputLabel input[type=text]').addClass('FormInput2');
				$('.FromInputLabe2 select').addClass('Select2');
				$('.jquery-selectbox').css('width', '305px');
				$('.jquery-selectbox-list').css('height', 'auto');
				$('.jquery-selectbox-list').css('width', '100%');
				
                <?php if($sf_request->getAttribute('addClassTo')!=''){?>
                $('#<?php echo $sf_request->getAttribute('addClassTo')?>').addClass('MenuSelected');
                <?php } ?>
            });

        </script>
        <!--font replacement  config-->
        <script type="text/javascript">
            Cufon.replace('h3', { fontFamily: 'Bell Centennial Std' });
            //If you also include a library, you can add complex selectors like #content h1
        </script>
        <!-- -->


    </head>
    <body >
        <div id="main">
        <div id="curvetop"></div>
            <div id="wrapper">
                <?php include_partial('global/header') ?>
                <div></div>

                <?php echo $sf_data->getRaw('sf_content') ?>

                <div id="wrapMid">
                    <div class="clearfix"></div>
                </div>
                <div id="wrapFoot">

<?php include_partial('global/footer') ?>

                </div>

            </div>
              <div id="curvebottom"></div>
        </div>
        <p>&nbsp;</p>
</body></html>