<div id="mainnew" >

    <div id="wrappernew">
        <div id="content">
            <div class="sform">
                <!-- Manu Datt : Flash messaged added -->
<div class="clear_new"></div>
    <?php $sf = sfContext::getInstance()->getUser();
if($sf->hasFlash('notice')){
  ?><div id="flash_notice" class="error_serverside" ><?php //Varun : change in class FS#34947
  echo nl2br($sf->getFlash('notice'));

  ?></div><?php }?>

                <h3><?php echo $title;?></h3>
                <form name="upload_file" action="<?php echo url_for($action); ?>" method="post" enctype="multipart/form-data">   <div class="FormRepeatArea">

                        <div class="TableForm"><table><tr><td><input type="file" value="Upload CSV" name="file" class="FormInput2" /></td></tr></table></div>
                        <div class="clear_new"></div>
                        <div class="form_table">

                            <?php if(($valid > 0  OR $invalid > 0 OR $success)){ ?>
                            <table style="width:50%;" align="center" border="0">
                            <?php }else{ ?>
                            <table style="width:100%;" border="0">
                                <?php } ?>
                            <?php if(($valid > 0 && $success) OR ($invalid > 0 && $success)){ ?>
                                <tr>
                                    <td class="thankyou">Thank You for Uploading</td>
                                </tr>
                                <?php if(($valid > 0 && $invalid > 0) && $success){ ?>
                                <tr>
                                    <td class="statusBlock"><span class="green" >Status:</span> <span>Customer list Partially Imported</span></td>
                                </tr>
                                <tr> <!-- Varun : Bug fix :FS#34980 -->
                                    <td><div class="hyperlinksuccess"><?php echo link_to('Download Success List', 'custMgt/downloads?file='.$validFile, 'popup=false') ?></div></td>
                                </tr>
                                <tr>
                                    <td><div class="hyperlinkerror"><?php echo link_to('Download Failure List', 'custMgt/downloads?file='.$invalidFile, 'popup=false') ?></div></td>
                                </tr>
                                <?php } else if($valid > 0 && $success){ ?>
                                <tr>
                                    <td class="statusBlock"><span class="green" >Status:</span> <span class="green">Customer list Successfully Imported</span></td>
                                </tr>
                                <tr> <!-- Varun : Bug fix :FS#34980 -->
                                    <td><div class="hyperlinksuccess"><?php echo link_to('Download Success List', 'custMgt/downloads?file='.$validFile, 'popup=false') ?></div></td>
                                </tr>
                                <?php } else { ?>
                                <tr>
                                    <td class="statusBlock"><span class="green" >Status:</span> <span>Customer List Importing Failed</span></td>
                                </tr>
                                <tr>
                                    <td><div class="hyperlinkerror"><?php echo link_to('Download Failure List', 'custMgt/downloads?file='.$invalidFile, 'popup=false') ?></div></td>
                                </tr>
                                <?php }?>
                                <tr>
                                    <td><div class="hyperlink"><?php //echo link_to('Upload Another File', 'custMgt/upload', 'popup=false') ?></div></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>

                                    <?php } ?>

                            </table>
                        </div>



                        <input type="submit" name="submit" value="<?php echo $uploadButtonText;?>" class = "blueBtn" style="margin:0px!important;" ><!-- set the alignment only this page-->

                    </div>

                </form>


            </div>

<!--            <div class="download_link"><a href="#" onclick="window.open('<?php //echo _compute_public_path($sample_link_file_name . '.csv', 'download', '', true); ?>');return false;"><?php //echo $sample_link_text;?></a></div>            <p>&nbsp;</p>-->
            <div class="download_link"><a href="#" onclick="window.open('<?php echo _compute_public_path($sample_link_file_name, 'uploads/assets', '', true); ?>');return false;"><?php echo $sample_link_text;?></a></div>            <p>&nbsp;</p>
        </div>
    </div>
</div>

