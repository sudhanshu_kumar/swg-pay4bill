<?php //echo image_tag('logo-versign.jpg', array('alt' => "Versign", "height" => "42", "width" => "83", "align" => "right")); ?>
<div id="copyrightInfo" class="right">Copyright &copy; 2012 pay4bill. All rights reserved.</div>
<ul id="footnav">
    <li><a href="<?php
if (!$sf_user->isAuthenticated()) {
    echo url_for('home/index');
} else {


    foreach (sfContext::getInstance()->getUser()->getGuardUser()->getGroups()->toArray() as $group_array) {
        if(isset($_SESSION['logedin_user_group']) && $_SESSION['logedin_user_group']=='merchant') {
            $linkTo = '@merchant_login';
        }
        elseif ($group_array['id'] == 2) {
            $linkTo = '@customer_login';
        } else if ($group_array['id'] == 1) {
            $linkTo = '@merchant_login';
        } else if ($group_array['id'] == 3) {
            $linkTo = '@admin_login';
        } else if ($group_array['id'] == 4) { // added for sub admin on 2nd May 12 by Santosh Kumar
            $linkTo = '@report_admin';
        } else if ($group_array['id'] == 5) {
            $linkTo = '@merchant_admin';
        } else if ($group_array['id'] == 6) {
            $linkTo = '@teller_login';
        }
    }

    echo url_for($linkTo);
}
?>"title="Home">Home</a></li>
    <li><a href="<?php if (!$sf_user->isAuthenticated()) {
            echo url_for('mypages/index');
        } else {
            echo url_for('mypages/aboutus');
        } ?>" title="About pay4bill">About pay4bill</a></li>
    <li><a href="<?php if (!$sf_user->isAuthenticated()) {
            echo url_for('mypages/features');
        } else {
            echo url_for('mypages/featuresAfterLogin');
        } ?>" title="Features">Features</a></li>
    <li><a href="mailto:support@pay4bill.com" title="Contact Information" class="end">Contact</a></li>
</ul>

<div class="clearfix"></div>