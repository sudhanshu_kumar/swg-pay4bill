<div id="pgMenu">
    <div id="wrappernew">
        <div class="pgTitle"><h2>Reports</h2></div>
        <div class="pgNav">
            <ul>
                <li class="<?php if ($addClassTo == 'index') { echo "current";}?>">
                    <a href="<?php echo url_for('report/allInvoicesForMerchant') ?>">Invoices</a>
                </li>
                <li class="<?php if ($addClassTo == 'amountPaidByCustomers') {echo "current"; }?>">
                    <a href="<?php echo url_for('report/amountPaidByCustomers') ?>">Payment By Customer</a>
                </li>
                <li class="<?php if ($addClassTo == 'customersWithPendingInvoices') { echo "current";}?>">
                    <a href="<?php echo url_for('report/customersWithPendingInvoices') ?>">Customers With Pending Invoices</a>
                </li>
                <li class="<?php if ($addClassTo == 'bankCollection' || $addClassTo == 'allMerchantPerBank') { echo "current";}?>">
                    <a href="<?php echo url_for('report/bankCollection') ?>">Bank Collection</a>
                </li>
            </ul>
        </div>
    </div>
</div>