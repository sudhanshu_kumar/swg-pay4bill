<?php
header("Cache-Control: no-cache");
header("Expires: -1");
?> <div id="thetop">
    <div id="theLogo"> <?php
if (!$sf_user->isAuthenticated()) {
    echo link_to(image_tag('logo-pay4bill.gif', array('alt' => "pay4bill", "border" => "0", "width" => "164")), "home/index", array('title' => "pay4bill"));
} else {
    foreach (sfContext::getInstance()->getUser()->getGuardUser()->getGroups()->toArray() as $group_array) {
        if (isset($_SESSION['logedin_user_group']) && $_SESSION['logedin_user_group'] == 'merchant') {
            $linkTo = '@merchant_login';
        } elseif ($group_array['id'] == 2) {
            $linkTo = '@customer_login';
        } else if ($group_array['id'] == 1) {
            $linkTo = '@merchant_login';
        } else if ($group_array['id'] == 3) {
            $linkTo = '@admin_login';
        }
        //Varun : Conditions added for Report(4) and Merchant(5) Admin
        else if ($group_array['id'] == 4) {
            $linkTo = '@report_admin';
        } else if ($group_array['id'] == 5) {
            $linkTo = '@merchant_admin';
        } else if ($group_array['id'] == 6) {
            $linkTo = '@teller_login';
        } else if ($group_array['id'] == 7) {
            $linkTo = '@support_user_login';
        }
        
    }

    echo link_to(image_tag('logo-pay4bill.gif', array('alt' => "pay4bill", "border" => "0", "width" => "164")), url_for($linkTo), array('title' => "pay4bill"));
?>
        <?php } ?>
    </div>
        <div id="versignLogo"><?php echo image_tag('mail-icon.gif', array('alt' => "support", "align" => "absmiddle")); ?> <a href="mailto:support@pay4bill.com" title="Contact">support@pay4bill.com</a></div>
        <map name="Map" id="Map">
            <area shape="rect" coords="7,5,217,43" href="<?php //echo site_url() ;   ?>">
        </map>
    

    <div class="clearfix"></div>
    <div class="userId">
<?php echo sfContext::getInstance()->getUser()->getGuardUser()->getUsername(); ?>
<?php
        /*
         * For customer to merchant and Vice versa
         * 24 Apr 2012
         * By : Santosh Kumar
         */
        $arrUserGroup = sfContext::getInstance()->getUser()->getGroupNames();
        if ((isset($_SESSION['logedin_user_group']) && $_SESSION['logedin_user_group'] == 'merchant') && (in_array('customer', $arrUserGroup))) {
 ?> | <a href="<?php echo url_for('userAdmin/signInInfo?type=' . cryptString::encrypt($_SESSION['logedin_user_group']) . '&bankchk=' . cryptString::encrypt('escbank')) ?>" >Switch to Customer View</a> <?php } ?> <?php if ((isset($_SESSION['logedin_user_group']) && $_SESSION['logedin_user_group'] == 'customer') && (in_array('merchant', $arrUserGroup))) {
 ?> | <a href="<?php echo url_for('userAdmin/signInInfo?type=' . cryptString::encrypt($_SESSION['logedin_user_group']) . '&bankchk=0') ?>" >Switch to Merchant View</a> <?php } ?> | <a href="<?php echo url_for('@sf_guard_signout') ?>" >Sign Out</a></div>

    <div class="clearfix"></div>
</div>

<?php
        if ($sf_user->isAuthenticated()) {
?>
            <div id="wrapNav">
                <div id="thenav">
<?php
            include_partial('global/menuAdmin', array('authenticated' => true));
?>

        </div>
    </div>



    <!--</div>-->

<?php
        }
?>