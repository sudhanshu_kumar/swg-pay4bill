<?php
header("Cache-Control: no-cache");
header("Expires: -1");
?>    <div id="thetop">
    <div id="theLogo"> <?php echo link_to(image_tag('logo-pay4bill.gif', array('alt' => "pay4bill", "border" => "0", "width" => "164")), "home/index", array('title' => "pay4bill")); ?></div>
    <div id="versignLogo"><?php echo image_tag('mail-icon.gif', array('alt' => "support", "align" => "absmiddle")); ?> <a href="mailto:support@pay4bill.com" title="Contact">support@pay4bill.com</a></div>
        <map name="Map" id="Map">
            <area shape="rect" coords="7,5,217,43" href="<?php //echo site_url() ;  ?>">
        </map>
    
    
    <div class="clearfix"></div>
</div>
<div id="wrapNav"><!--
  <div id="thenav">
    <ul id="topnav"> class="MenuSelected"
      <li> <a href="<?php echo url_for('home/index'); ?>" title="Home" id='home'>Home</a> </li>
      <li> <a href="<?php echo url_for('mypages/index'); ?>" title="Learn More" id='learn_more'>Learn More</a> </li>
      <li> <a href="<?php echo url_for('mypages/features'); ?>" title="Features" id='features'>Features</a> </li>
      <li> <a href="<?php echo url_for('mypages/services'); ?>" title="Services" id='services'>Services</a> </li>
      <li> <a href="<?php echo url_for('mypages/faqs'); ?>" title="FAQs" id='faqs'>FAQs</a> </li>
      <li> <a href="<?php echo url_for('mypages/feedback'); ?>" title="Feedback" >Feedback</a> </li>
    </ul>
  </div>-->
    <?php
    if ($sf_user->isAuthenticated()) {
        include_partial('global/menuAdmin', array('authenticated' => true));
    } else {
    ?>
        <div id="thenav">
    <?php
        include_partial('global/menuAdmin', array('authenticated' => false));
    ?>
        </div>
        <?php
    }
        ?>
</div>

<!--<div id="copyrightInfo"><font color="red">Copyright &copy; 2011 pay4bill. All rights reserved.</font></div>-->
