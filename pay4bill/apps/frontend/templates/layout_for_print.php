<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=100"/> 
<?php include_stylesheets() ?>
<?php include_javascripts() ?>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>pay4bill - Giving you multiple options to shop and pay your bills</title>
<?php
header("Cache-Control: no-cache");
header("Expires: -1");
?>
<script type="text/javascript" src="<?php echo _compute_public_path("jquery.selectbox-0.6.1.js", 'js', '', true); ?>"></script>
<script type="text/javascript">
            Cufon.replace('#pgMenu .pgTitle h3, .pgTitle h2', { fontFamily: 'Bell Centennial Std'   });
            //If you also include a library, you can add complex selectors like #content h1
			//FormInput1 jquery-selectbox-list jquery-custom-selectboxes-replaced-list" style="width: 60px; height: 30px; display: block;"
			 //$('.FromInputLabe2, .FromInputLabe3,').css('border', 'solid 1px red;');
				 $(document).ready(function() {
				$('.FromInputLabe3 input[type=text]').addClass('FormInput2');
				$('.FromInputLabe2 input[type=text]').addClass('FormInput2');
				$('.FromInputLabe3 input[type=password]').addClass('FormInput2');
				$('.FromInputLabe2 input[type=password]').addClass('FormInput2');
				$('.FromInputLabel input[type=text]').addClass('FormInput2');
				$('.FromInputLabe2 select').addClass('Select3');
				$('.jquery-selectbox').css('width', '305px');
				$('.jquery-selectbox-list').css('height', 'auto');
				$('.jquery-selectbox-list').css('width', '100%');
				$('.TableForm table tr:even').addClass('AlterBG');
				});
        </script>
</head>
<body>
<noscript>
<p class="noscript">This Website has been designed to use Javascript. You will
  not be able to view the full content of this site with Javascript disabled.
  If you wish to continue, please enable Javascript in your Internet Browser. </p>
</noscript>
<!--<script type="text/javascript" src="<?php echo _compute_public_path("jquery-1.2.6.pack".'.js', 'js', '', true); ?>">
</script>-->


<div id="main">
  <div id="curvetop"></div>
  <div id="wrapper">
  
   <div id="theLogo"> <?php echo image_tag('logo-pay4bill.gif', array('alt' => "pay4bill", "border" => "0", "width" => "164")); ?>
        <map name="Map" id="Map">
            <area shape="rect" coords="7,5,217,43" href="<?php //echo site_url() ;  ?>">
        </map>
    </div>
   
    <?php echo $sf_data->getRaw('sf_content') ?>
    <?php //include_partial('global/permissionsFooter') ?>
        <div id="wrapMid">
      <div class="clearfix"></div>
    </div>
    <div id="wrapFoot">
	<div id="copyrightInfo" class="right">Copyright &copy; 2012 pay4bill. All rights reserved.</div>
        <div style="height:10px;"></div>
      <?php //include_partial('global/footer') ?>
    </div>
  </div>
  <div id="curvebottom"></div>
</div>
<p>&nbsp;</p>
<script type="text/javascript">
		//<![CDATA[
                if($.browser.msie) {
			$(".FromInputLabe2 select").selectbox().bind('change', function(){
				$('<div>Value of #default-usage-select changed to: '+$(this).val()+'</div>').appendTo('#demo-default-usage .demoTarget').fadeOut(5000, function(){
					$(this).remove();
				});
			});
                }
		//]]>
		</script>
</body>

</html>
