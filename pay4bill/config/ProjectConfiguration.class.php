<?php

require_once dirname(__FILE__) . '/../lib/vendor/symfony/lib/autoload/sfCoreAutoload.class.php';
sfCoreAutoload::register();

class ProjectConfiguration extends sfProjectConfiguration {

    public function setup() {

        $this->enablePlugins('sfDoctrinePlugin');
        $this->enablePlugins('sfFormExtraPlugin');
        $this->enablePlugins('sfDoctrineGuardPlugin');
        $this->enablePlugins('epJobSchedulerPlugin');
        $this->enablePlugins('sfCryptoCaptchaPlugin');
        $this->enablePlugins('sfJqueryFormValidationPlugin');
        $this->enablePlugins('sfPhpExcelPlugin');
        $this->enablePlugins('sfWebBrowserPlugin');
        $this->enablePlugins('epPay4MePlugin');
        $this->enablePlugins('epSessionManagerPlugin');
        sfWidgetFormSchema::setDefaultFormFormatterName('Custom');
        $this->enablePlugins('sfPHPUnit2Plugin');
        $this->enablePlugins('epJsperReportPlugin');
        $this->enablePlugins('epActionAuditPlugin');
    }

}
