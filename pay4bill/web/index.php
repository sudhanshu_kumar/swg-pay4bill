<?php
require_once(dirname(__FILE__).'/../config/ProjectConfiguration.class.php');
ini_set('memory_limit','192M');
$configuration = ProjectConfiguration::getApplicationConfiguration('frontend', 'prod', false);
sfContext::createInstance($configuration)->dispatch();
