<?php

require_once 'test/bootstrap/unit.php';
$configuration = ProjectConfiguration::getApplicationConfiguration('frontend', '', true);
new sfDatabaseManager($configuration);

class unit_InvoiceTableTest extends sfPHPUnitBaseTestCase {

    public function testIsValidInvoiceNumberForThsMerchant() {


        $t = $this->getTest();

        $invoice_no = "";
        $merchantServiceId = "";
        $result = Doctrine::getTable('Invoice')->isValidInvoiceNumberForThsMerchant($invoice_no, $merchantServiceId);

        $t->cmp_ok(gettype($result), "==", "boolean");

        $invoice_no = "";
        $merchantServiceId = 33;
        $result = Doctrine::getTable('Invoice')->isValidInvoiceNumberForThsMerchant($invoice_no, $merchantServiceId);

        $t->cmp_ok($result, "==", false);

        $invoice_no = 23344;
        $merchantServiceId = "";
        $result = Doctrine::getTable('Invoice')->isValidInvoiceNumberForThsMerchant($invoice_no, $merchantServiceId);

        $t->cmp_ok($result, "==", false);

        $invoice_no = 0;
        $merchantServiceId = 3124;
        $result = Doctrine::getTable('Invoice')->isValidInvoiceNumberForThsMerchant($invoice_no, $merchantServiceId);

        $t->cmp_ok($result, "==", false);
//        $invoice_no = 578678678;
//        $merchantServiceId = 3124;
//        $result = Doctrine::getTable('Invoice')->isValidInvoiceNumberForThsMerchant($invoice_no, $merchantServiceId);
//
//        $t->cmp_ok($result, "!=", false);
    }

    public function testPaymentHistoryForCustomer() {
        $t = $this->getTest();
        $customerProfileId = Doctrine::getTable('CustomerProfile')->findAll()->getLast();
        $offset = 0;
        $limit = 0;
        $com_name = '';
        $invoice_no = '';
        $customerIdSearch = '';
        $result = Doctrine::getTable('Invoice')->paymentHistoryForCustomer($customerProfileId, $offset = 0, $limit = 0, $com_name, $invoice_no, $customerIdSearch)->count();

        $t->cmp_ok($result, ">=", 0);
    }

}