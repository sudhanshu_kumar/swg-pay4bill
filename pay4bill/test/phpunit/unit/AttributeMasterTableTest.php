<?php
require_once 'test/bootstrap/unit.php';
$configuration = ProjectConfiguration::getApplicationConfiguration( 'frontend', '', true);
new sfDatabaseManager($configuration);
class unit_AttributeMasterTest extends sfPHPUnitBaseTestCase
{

    
    public function  testGetAllAttributeDetailsWithValue(){

        $t = $this->getTest();

        $merchantId='';
        $object_type='';
        $object_id='';
//
        $result = Doctrine::getTable('AttributeMaster')->getAllAttributeDetailsWithValue($merchantId, $object_type, $object_id);
        $t->is(0, count($result));


       $merchantId=1;
        $object_type=1;
        $object_id=4;

        $result = Doctrine::getTable('AttributeMaster')->getAllAttributeDetailsWithValue($merchantId, $object_type, $object_id);
//
        $t->is(0, count($result));

        $merchantId=2;
        $object_type=1;
        $object_id=5;
//
        $result = Doctrine::getTable('AttributeMaster')->getAllAttributeDetailsWithValue($merchantId, $object_type, $object_id);
        $t->is(2, count($result));
//
//
        $merchantId=2;
        $object_type=2;
        $object_id=6;

        $result = Doctrine::getTable('AttributeMaster')->getAllAttributeDetailsWithValue($merchantId, $object_type, $object_id);
        $t->is(0, count($result));
//
        $merchantId=2;
        $object_type=1;
        $object_id=7;

        $result = Doctrine::getTable('AttributeMaster')->getAllAttributeDetailsWithValue($merchantId, $object_type, $object_id);
        $t->is(2, count($result));
    }
}