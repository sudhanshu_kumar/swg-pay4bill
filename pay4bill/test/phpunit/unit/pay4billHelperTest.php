<?php

require_once 'test/bootstrap/unit.php';
$configuration = ProjectConfiguration::getApplicationConfiguration('frontend', '', true);
new sfDatabaseManager($configuration);

class unit_pay4billHelperTest extends sfPHPUnitBaseTestCase {

    public function testGetInvoiceNo() {

        $t = $this->getTest();

        $merchantId = '';
        $merchantName = '';

        $result = pay4billHelper::getInvoiceNo($merchantId, $merchantName);
        $t->is(true, is_null($result));

        $merchantId = '';
        $merchantName = 'fddfgdf';

        $result = pay4billHelper::getInvoiceNo($merchantId, $merchantName);
        $t->is(true, is_null($result));

        $merchantId = '';
        $merchantName = 'fddfgdf';

        $result = pay4billHelper::getInvoiceNo($merchantId, $merchantName);
        $t->is(true, is_null($result));

        $merchantId = Doctrine::getTable('Invoice')->findAll()->getLast()->getMerchantId(); //Valid
        $merchantName = Doctrine::getTable('MerchantProfile')->findAll()->getLast()->getMerchantName(); //Valid

        $result = pay4billHelper::getInvoiceNo($merchantId, $merchantName);

        $t->is(true, is_string($result));


        $merchantId = Doctrine::getTable('MerchantProfile')->findAll()->getLast()->getId(); //InValid
        $merchantName = ""; //Valid

        $result = pay4billHelper::getInvoiceNo($merchantId + 1, $merchantName);

        $t->is(true, is_null($result));


        $merchantId = Doctrine::getTable('MerchantProfile')->findAll()->getLast()->getId(); //Valid
        $merchantName = Doctrine::getTable('MerchantProfile')->findAll()->getLast()->getMerchantName(); //Valid

        $result = pay4billHelper::getInvoiceNo($merchantId + 1, $merchantName);

        $t->is(true, is_null($result));
    }

    public function testGenerateCustomerId() {
        $t = $this->getTest();


        $merchantId = ''; //Invalid
        $customerName = ''; //Invalid
        $result = pay4billHelper::generateCustomerId($merchantId, $customerName);
        $t->is(true, is_null($result));


        $merchantId = Doctrine::getTable('MerchantProfile')->findAll()->getLast()->getId(); //Valid
        $customerName = 'zxvcv'; //Valid
        $result = pay4billHelper::generateCustomerId($merchantId, $customerName);
        $t->is(true, is_string($result));

        $merchantId = 0; //Invalid
        $customerName = ''; //Invalid
        $result = pay4billHelper::generateCustomerId($merchantId, $customerName);
        $t->is(true, is_null($result));



        $merchantId = Doctrine::getTable('MerchantProfile')->findAll()->getLast()->getId(); //Valid
        $customerName = 'zxvcv'; //Valid
        $result = pay4billHelper::generateCustomerId($merchantId + 1, $customerName);
        $t->is(true, is_null($result));
    }

    public function testCheckMerchantPaymentType() {
        $t = $this->getTest();
        $helperObj = new pay4billHelper();
        $merchantProfileObj = Doctrine::getTable('MerchantProfile')->findAll()->getLast();
        $merchantProfileId = $merchantProfileObj->getId();
        $paymentType = $merchantProfileObj->getPaymentType();
        $result = $helperObj->checkMerchantPaymentType($merchantProfileId);
        $t->is($paymentType, $result);
    }

}