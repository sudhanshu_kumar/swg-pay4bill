<?php
require_once 'test/bootstrap/unit.php';
$configuration = ProjectConfiguration::getApplicationConfiguration('frontend', '', true);
new sfDatabaseManager($configuration);

class unit_SupportUserProfileTableTest extends sfPHPUnitBaseTestCase
{
  public function testDefault()
  {
    $t = $this->getTest();

    $username = "";
    $email_address = "";
    $result = Doctrine::getTable('SupportUserProfile')->getSupportUserList($username, $email_address,0,0);
    $t->cmp_ok(gettype($result), "==", "object");

    $username = "temp";
    $email_address = "test@abc.com";
    $result = Doctrine::getTable('SupportUserProfile')->getSupportUserInfoById($username, $email_address,0,0);
    $t->cmp_ok($result, "!=", 'null');

    $supportUserId = "";
    $result = Doctrine::getTable('SupportUserProfile')->getSupportUserInfoById($supportUserId);
    $t->cmp_ok($result, "==", false);

    $supportUserId = Doctrine::getTable('SupportUserProfile')->findAll()->getFirst()->getUserId();
    $result = Doctrine::getTable('SupportUserProfile')->getSupportUserInfoById($supportUserId);
    $t->cmp_ok(gettype($result), "==", "array");

    $supportUserId = 7654; //Any random value
    $result = Doctrine::getTable('SupportUserProfile')->getSupportUserInfoById($supportUserId);
    $t->cmp_ok(count($result), "==", 0);
    }
}


?>