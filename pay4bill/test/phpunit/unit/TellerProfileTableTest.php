<?php
require_once 'test/bootstrap/unit.php';
$configuration = ProjectConfiguration::getApplicationConfiguration('frontend', '', true);
new sfDatabaseManager($configuration);

class unit_TellerProfileTableTest extends sfPHPUnitBaseTestCase
{
  public function testDefault()
  {
    $t = $this->getTest();

    $username = "";
    $email_address = "";
    $result = Doctrine::getTable('TellerProfile')->getTellersList(0, 0, $username, $email_address);
    $t->cmp_ok(gettype($result), "==", "object");
    
    $username = "temp";
    $email_address = "test@abc.com";
    $result = Doctrine::getTable('TellerProfile')->getTellersList(0, 0, $username, $email_address);
    $t->cmp_ok($result, "!=", 'null');
     
    $tellerId = "";
    $result = Doctrine::getTable('TellerProfile')->getTellerInfoById($tellerId);
    $t->cmp_ok($result, "==", false);        
        
    $tellerId = 893;
    $result = Doctrine::getTable('TellerProfile')->getTellerInfoById($tellerId);
    $t->cmp_ok(gettype($result), "==", "array");      

        
    $tellerId = 12;
    $result = Doctrine::getTable('TellerProfile')->getTellerInfoById($tellerId);
    $t->cmp_ok(count($result), ">", 0); 
    }
}


