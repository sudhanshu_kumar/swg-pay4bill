<?php

require_once 'test/bootstrap/unit.php';
$configuration = ProjectConfiguration::getApplicationConfiguration('frontend', '', true);
new sfDatabaseManager($configuration);

class unit_merchantProfileTableTest extends sfPHPUnitBaseTestCase {

    public function testGetFullyConfiguredActiveMerchants() {

        $t = $this->getTest();

        $result = Doctrine::getTable('MerchantProfile')->getFullyConfiguredActiveMerchants()->execute()->count();

        $t->cmp_ok($result, ">=", 0);
    }

}