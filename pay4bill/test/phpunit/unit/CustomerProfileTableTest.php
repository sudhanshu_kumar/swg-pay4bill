<?php
require_once 'test/bootstrap/unit.php';
$configuration = ProjectConfiguration::getApplicationConfiguration('frontend', '', true);
new sfDatabaseManager($configuration);

class unit_CustomerProfileTableTest extends sfPHPUnitBaseTestCase {

    public function testGetCustomersListWithAttributeSearch() {
        $t = $this->getTest();
        $merchantsArray = array();
        $name = '';
        $customer_id = '';
        $email_address = '';
        $address = '';
        $phoneNumber = '';
        $dynamic_attributes_array = array();

        $result = Doctrine::getTable('CustomerProfile')->getCustomersListWithAttributeSearch($merchantsArray, $name, $customer_id, $email_address, $address, $phoneNumber, $dynamic_attributes_array = array());

        $t->cmp_ok($result->count(), ">", 0);
        $merchantsArray = array();
        $name = '';
        $customer_id = '';
        $email_address = '';
        $address = '';
        $phoneNumber = 'ghfhgfgh';
        $dynamic_attributes_array = array();

        $result = Doctrine::getTable('CustomerProfile')->getCustomersListWithAttributeSearch($merchantsArray, $name, $customer_id, $email_address, $address, $phoneNumber, $dynamic_attributes_array = array());

        $t->cmp_ok($result->count(), "==", 0);

        $merchantsArray = array();
        $name = '';
        $customer_id = '';
        $email_address = '';
        $address = '{';
        $phoneNumber = '';
        $dynamic_attributes_array = array();

        $result = Doctrine::getTable('CustomerProfile')->getCustomersListWithAttributeSearch($merchantsArray, $name, $customer_id, $email_address, $address, $phoneNumber, $dynamic_attributes_array = array());

        $t->cmp_ok($result->count(), "==", 0);

        $merchantsArray = array();
        $name = '';
        $customer_id = '';
        $email_address = '';
        $address = '{';
        $phoneNumber = '';
        $dynamic_attributes_array = array('utin' => '87');

        $result = Doctrine::getTable('CustomerProfile')->getCustomersListWithAttributeSearch($merchantsArray, $name, $customer_id, $email_address, $address, $phoneNumber, $dynamic_attributes_array = array());

        $t->cmp_ok($result->count(), "==", 0);

        $merchantsArray = array(77); //Merchant not in database
        $name = '';
        $customer_id = '';
        $email_address = '';
        $address = '';
        $phoneNumber = '';
        $dynamic_attributes_array = array('utin' => '87');

        $result = Doctrine::getTable('CustomerProfile')->getCustomersListWithAttributeSearch($merchantsArray, $name, $customer_id, $email_address, $address, $phoneNumber, $dynamic_attributes_array = array());

        $t->cmp_ok($result->count(), "==", 0);

        $merchantsArray = array(2); //Merchant not in database
        $name = '';
        $customer_id = '';
        $email_address = '';
        $address = '';
        $phoneNumber = '';
        $dynamic_attributes_array = array('utin' => '87');

        $result = Doctrine::getTable('CustomerProfile')->getCustomersListWithAttributeSearch($merchantsArray, $name, $customer_id, $email_address, $address, $phoneNumber, $dynamic_attributes_array = array());

        $t->cmp_ok($result->count(), ">", 0);
    }
    
    public function testCanDisable() {
        $t = $this->getTest();        
        
        $allCustomers = Doctrine::getTable('CustomerProfile')->findAll()->getFirst();        
        $id = $allCustomers->getId();
        $mid = $allCustomers->getMerchantProfileId();        
        $result = $cnt = Doctrine::getTable('CustomerProfile')->canDisable($id, $mid);
        $t->cmp_ok($result, ">", 0);
        
        
        $id = 0; // Not in the database
        $mid = 0;// Not in the database        
        $result = $cnt = Doctrine::getTable('CustomerProfile')->canDisable($id, $mid);
        $t->cmp_ok($result, "==", 0);
        
        $id = ''; // With blank values
        $mid = '';// With blank values
        $result = $cnt = Doctrine::getTable('CustomerProfile')->canDisable($id, $mid);
        $t->cmp_ok($result, "==", 0);
    }

}